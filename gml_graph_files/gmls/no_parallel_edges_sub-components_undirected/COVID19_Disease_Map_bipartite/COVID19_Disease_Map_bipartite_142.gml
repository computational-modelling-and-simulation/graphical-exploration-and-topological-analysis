# generated with VANTED V2.8.2 at Fri Mar 04 10:04:35 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:C3;urn:miriam:mesh:D003179;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000125730;urn:miriam:refseq:NM_000064;urn:miriam:uniprot:P01024;urn:miriam:uniprot:P01024;urn:miriam:hgnc:1318;urn:miriam:ncbigene:718;urn:miriam:ncbigene:718"
      hgnc "HGNC_SYMBOL:C3"
      map_id "M121_180"
      name "C3b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa310"
      uniprot "UNIPROT:P01024"
    ]
    graphics [
      x 872.5
      y 1928.590887274084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_180"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:17395591;PUBMED:427127"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_50"
      name "PMID:427127, PMID:17395591"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re261"
      uniprot "NA"
    ]
    graphics [
      x 1451.043215136001
      y 2542.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "PUBMED:26521297"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_61"
      name "PMID:26521297"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re285"
      uniprot "NA"
    ]
    graphics [
      x 452.5
      y 2147.0917536624975
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D003179;urn:miriam:brenda:3.4.21.47;urn:miriam:taxonomy:9606;urn:miriam:pubmed:12440962;urn:miriam:hgnc:1037;urn:miriam:mesh:D051561;urn:miriam:hgnc.symbol:C3;urn:miriam:mesh:D003179;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000125730;urn:miriam:refseq:NM_000064;urn:miriam:uniprot:P01024;urn:miriam:uniprot:P01024;urn:miriam:hgnc:1318;urn:miriam:ncbigene:718;urn:miriam:ncbigene:718;urn:miriam:ec-code:3.4.21.47;urn:miriam:ensembl:ENSG00000243649;urn:miriam:hgnc.symbol:CFB;urn:miriam:uniprot:P00751;urn:miriam:uniprot:P00751;urn:miriam:hgnc.symbol:CFB;urn:miriam:hgnc:1037;urn:miriam:refseq:NM_001710;urn:miriam:ncbigene:629;urn:miriam:ncbigene:629"
      hgnc "HGNC_SYMBOL:C3;HGNC_SYMBOL:CFB"
      map_id "M121_8"
      name "C3b:Bb"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa26"
      uniprot "UNIPROT:P01024;UNIPROT:P00751"
    ]
    graphics [
      x 1112.5
      y 2007.0116707084726
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:brenda:34.4.21.47;urn:miriam:mesh:D003179;urn:miriam:taxonomy:9606;urn:miriam:mesh:D051566;urn:miriam:pubmed:12440962;urn:miriam:hgnc:1037;urn:miriam:ec-code:3.4.21.47;urn:miriam:ensembl:ENSG00000243649;urn:miriam:hgnc.symbol:CFB;urn:miriam:uniprot:P00751;urn:miriam:uniprot:P00751;urn:miriam:hgnc.symbol:CFB;urn:miriam:hgnc:1037;urn:miriam:refseq:NM_001710;urn:miriam:ncbigene:629;urn:miriam:ncbigene:629;urn:miriam:hgnc.symbol:C3;urn:miriam:mesh:D003179;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000125730;urn:miriam:refseq:NM_000064;urn:miriam:uniprot:P01024;urn:miriam:uniprot:P01024;urn:miriam:hgnc:1318;urn:miriam:ncbigene:718;urn:miriam:ncbigene:718"
      hgnc "HGNC_SYMBOL:CFB;HGNC_SYMBOL:C3"
      map_id "M121_9"
      name "C3b:Bb:C3b"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa27"
      uniprot "UNIPROT:P00751;UNIPROT:P01024"
    ]
    graphics [
      x 812.5
      y 1340.3215371113788
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "PUBMED:30083158;PUBMED:12878586"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_51"
      name "PMID:30083158, PMID: 12878586"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re262"
      uniprot "NA"
    ]
    graphics [
      x 1532.5
      y 1259.738142841986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc:1331;urn:miriam:hgnc.symbol:C5;urn:miriam:hgnc.symbol:C5;urn:miriam:refseq:NM_001735;urn:miriam:ensembl:ENSG00000106804;urn:miriam:uniprot:P01031;urn:miriam:uniprot:P01031;urn:miriam:ncbigene:727;urn:miriam:ncbigene:727"
      hgnc "HGNC_SYMBOL:C5"
      map_id "M121_181"
      name "C5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa314"
      uniprot "UNIPROT:P01031"
    ]
    graphics [
      x 632.5
      y 1027.077935484341
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_181"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D051574;urn:miriam:taxonomy:9606;urn:miriam:brenda:3.4.21.43;urn:miriam:mesh:D050678;urn:miriam:hgnc:1324;urn:miriam:refseq:NM_001002029;urn:miriam:ensembl:ENSG00000224389;urn:miriam:taxonomy:9606;urn:miriam:ncbigene:100293534;urn:miriam:hgnc.symbol:C4B;urn:miriam:ncbigene:721;urn:miriam:hgnc.symbol:C4B;urn:miriam:hgnc:1324;urn:miriam:uniprot:P0C0L5;urn:miriam:uniprot:P0C0L5;urn:miriam:hgnc.symbol:C2;urn:miriam:uniprot:P06681;urn:miriam:uniprot:P06681;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000166278;urn:miriam:ec-code:3.4.21.43;urn:miriam:refseq:NM_000063;urn:miriam:mesh:D050678;urn:miriam:hgnc:1248;urn:miriam:ncbigene:717;urn:miriam:ncbigene:717"
      hgnc "HGNC_SYMBOL:C4B;HGNC_SYMBOL:C2"
      map_id "M121_15"
      name "C2a:C4b"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa35"
      uniprot "UNIPROT:P0C0L5;UNIPROT:P06681"
    ]
    graphics [
      x 1232.5
      y 1662.9973497539163
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc:1331;urn:miriam:hgnc.symbol:C5;urn:miriam:mesh:D050776;urn:miriam:refseq:NM_001735;urn:miriam:ensembl:ENSG00000106804;urn:miriam:uniprot:P01031;urn:miriam:uniprot:P01031;urn:miriam:ncbigene:727;urn:miriam:ncbigene:727"
      hgnc "HGNC_SYMBOL:C5"
      map_id "M121_182"
      name "C5b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa315"
      uniprot "UNIPROT:P01031"
    ]
    graphics [
      x 1698.7805189192002
      y 2577.1855687946227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_182"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc:1331;urn:miriam:hgnc.symbol:C5;urn:miriam:refseq:NM_001735;urn:miriam:mesh:D015936;urn:miriam:ensembl:ENSG00000106804;urn:miriam:uniprot:P01031;urn:miriam:uniprot:P01031;urn:miriam:ncbigene:727;urn:miriam:ncbigene:727"
      hgnc "HGNC_SYMBOL:C5"
      map_id "M121_174"
      name "C5a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa253"
      uniprot "UNIPROT:P01031"
    ]
    graphics [
      x 1562.5
      y 1035.4971941397312
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_174"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_86"
      name "PMCID:PMC7260598"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re334"
      uniprot "NA"
    ]
    graphics [
      x 2171.043215136001
      y 2693.713309864131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "PUBMED:27077125"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_94"
      name "PMID:27077125"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re343"
      uniprot "NA"
    ]
    graphics [
      x 1442.5
      y 754.2678925926325
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:27077125;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_237"
      name "s546"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa467"
      uniprot "NA"
    ]
    graphics [
      x 1522.0359904149498
      y 449.738142841986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_237"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:PLG;urn:miriam:ec-code:3.4.21.7;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000122194;urn:miriam:ncbigene:5340;urn:miriam:ncbigene:5340;urn:miriam:hgnc:9071;urn:miriam:refseq:NM_000301;urn:miriam:mesh:D005341;urn:miriam:brenda:3.4.21.7;urn:miriam:uniprot:P00747;urn:miriam:uniprot:P00747"
      hgnc "HGNC_SYMBOL:PLG"
      map_id "M121_238"
      name "Plasmin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa468"
      uniprot "UNIPROT:P00747"
    ]
    graphics [
      x 1382.5
      y 718.926824478399
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_238"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_235"
      name "s538"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa461"
      uniprot "NA"
    ]
    graphics [
      x 1938.7805189192002
      y 2342.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_235"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:2697049;urn:miriam:mesh:D012327"
      hgnc "NA"
      map_id "M121_158"
      name "SARS_minus_CoV_minus_2_space_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa207"
      uniprot "NA"
    ]
    graphics [
      x 2522.5
      y 2072.97150358576
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_158"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_78"
      name "PMICID:PMC7260598"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re324"
      uniprot "NA"
    ]
    graphics [
      x 1172.5
      y 1977.3156502059937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "PUBMED:32286245"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_85"
      name "PMID:32286245"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re333"
      uniprot "NA"
    ]
    graphics [
      x 632.5
      y 997.077935484341
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "PUBMED:32302438"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_77"
      name "PMID:32302438"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re323"
      uniprot "NA"
    ]
    graphics [
      x 3302.5
      y 1503.2836478803433
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "PUBMED:32504360"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_95"
      name "PMID:32504360"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re344"
      uniprot "NA"
    ]
    graphics [
      x 3302.5
      y 1981.9711191330853
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "PUBMED:32171076"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_73"
      name "PMID:32171076"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re315"
      uniprot "NA"
    ]
    graphics [
      x 1382.5
      y 1812.4847495740523
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "PUBMED:32286245"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_87"
      name "PMID:32286245"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re336"
      uniprot "NA"
    ]
    graphics [
      x 3332.5
      y 2043.2233332796325
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "PUBMED:32367170"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_65"
      name "PMID:32367170"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re295"
      uniprot "NA"
    ]
    graphics [
      x 1322.5
      y 882.6833324914029
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "PUBMED:32359396"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_84"
      name "PMID:32359396"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re332"
      uniprot "NA"
    ]
    graphics [
      x 2282.5
      y 819.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "PUBMED:32286245"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_88"
      name "PMID:32286245"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re337"
      uniprot "NA"
    ]
    graphics [
      x 1998.7805189192002
      y 2072.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32278764;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_225"
      name "s534"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa451"
      uniprot "NA"
    ]
    graphics [
      x 1800.6099514603573
      y 2942.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_225"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:2504360"
      hgnc "NA"
      map_id "M121_217"
      name "cytokine_space_storm"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa416"
      uniprot "NA"
    ]
    graphics [
      x 3212.5
      y 1639.6840355179397
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_217"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:uniprot:P01589;urn:miriam:uniprot:P01589;urn:miriam:ncbigene:3559;urn:miriam:ncbigene:3559;urn:miriam:hgnc:6008;urn:miriam:hgnc.symbol:IL2RA;urn:miriam:refseq:NM_000417;urn:miriam:hgnc.symbol:IL2RA;urn:miriam:ensembl:ENSG00000134460"
      hgnc "HGNC_SYMBOL:IL2RA"
      map_id "M121_178"
      name "IL2RA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa306"
      uniprot "UNIPROT:P01589"
    ]
    graphics [
      x 1382.5
      y 1872.4847495740523
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_178"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "PUBMED:20483636"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_49"
      name "PMID:20483636"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re260"
      uniprot "NA"
    ]
    graphics [
      x 1322.5
      y 1402.3432595389866
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:mesh:D013923;urn:miriam:mesh:D055806"
      hgnc "NA"
      map_id "M121_175"
      name "Thrombosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa271"
      uniprot "NA"
    ]
    graphics [
      x 632.5
      y 1214.2412105250232
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_175"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "PUBMED:27561337"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_43"
      name "PMID:27561337"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re227"
      uniprot "NA"
    ]
    graphics [
      x 812.5
      y 2095.4401700720628
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "PUBMED:27561337"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_45"
      name "PMID:27561337"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re231"
      uniprot "NA"
    ]
    graphics [
      x 572.5
      y 2247.2325393896285
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "PUBMED:27561337"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_44"
      name "PMID:27561337"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re229"
      uniprot "NA"
    ]
    graphics [
      x 722.5
      y 933.8850944639047
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "PUBMED:25573909"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_91"
      name "PMID:25573909"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re340"
      uniprot "NA"
    ]
    graphics [
      x 272.5
      y 1233.2330392074805
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "PUBMED:9490235"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_75"
      name "PMID:9490235"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re320"
      uniprot "NA"
    ]
    graphics [
      x 1202.5
      y 852.0185613371316
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "PUBMED:27561337"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_48"
      name "PMID:27561337"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re259"
      uniprot "NA"
    ]
    graphics [
      x 752.5
      y 2078.3665515504927
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "PUBMED:25573909"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_93"
      name "PMID:25573909"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re342"
      uniprot "NA"
    ]
    graphics [
      x 632.5
      y 1436.7702022570459
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "PUBMED:20975035"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_112"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re371"
      uniprot "NA"
    ]
    graphics [
      x 692.5
      y 1949.1308586148798
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "PUBMED:3500650"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_76"
      name "PMID:3500650"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re321"
      uniprot "NA"
    ]
    graphics [
      x 602.5
      y 1599.295395563012
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:12726;urn:miriam:taxonomy:9606;urn:miriam:refseq:NM_000552;urn:miriam:uniprot:P04275;urn:miriam:uniprot:P04275;urn:miriam:ncbigene:7450;urn:miriam:ncbigene:7450;urn:miriam:hgnc.symbol:VWF;urn:miriam:hgnc.symbol:VWF;urn:miriam:ensembl:ENSG00000110799"
      hgnc "HGNC_SYMBOL:VWF"
      map_id "M121_214"
      name "VWF"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa411"
      uniprot "UNIPROT:P04275"
    ]
    graphics [
      x 1742.5
      y 1911.755742606838
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_214"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "PUBMED:32367170"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_74"
      name "PMID:32367170"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re319"
      uniprot "NA"
    ]
    graphics [
      x 2026.5680063480233
      y 482.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "PUBMED:25051961"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_103"
      name "PMID:25051961"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re358"
      uniprot "NA"
    ]
    graphics [
      x 2282.5
      y 1666.1202267557142
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:6137;urn:miriam:pubmed:25051961;urn:miriam:taxonomy:10090;urn:miriam:hgnc:14338;urn:miriam:hgnc:6153;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:GP6;urn:miriam:hgnc.symbol:GP6;urn:miriam:hgnc:14388;urn:miriam:ensembl:ENSG00000088053;urn:miriam:ncbigene:51206;urn:miriam:ncbigene:51206;urn:miriam:refseq:NM_001083899;urn:miriam:uniprot:Q9HCN6;urn:miriam:uniprot:Q9HCN6;urn:miriam:hgnc:6137;urn:miriam:refseq:NM_002203;urn:miriam:ncbigene:3673;urn:miriam:ensembl:ENSG00000164171;urn:miriam:uniprot:P17301;urn:miriam:uniprot:P17301;urn:miriam:ncbigene:3673;urn:miriam:hgnc.symbol:ITGA2;urn:miriam:hgnc.symbol:ITGA2;urn:miriam:refseq:NM_002211;urn:miriam:uniprot:P05556;urn:miriam:uniprot:P05556;urn:miriam:hgnc.symbol:ITGB1;urn:miriam:hgnc.symbol:ITGB1;urn:miriam:ncbigene:3688;urn:miriam:ncbigene:3688;urn:miriam:hgnc:6153;urn:miriam:ensembl:ENSG00000150093"
      hgnc "HGNC_SYMBOL:GP6;HGNC_SYMBOL:ITGA2;HGNC_SYMBOL:ITGB1"
      map_id "M121_18"
      name "GP6:alpha2:beta1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa39"
      uniprot "UNIPROT:Q9HCN6;UNIPROT:P17301;UNIPROT:P05556"
    ]
    graphics [
      x 1892.5
      y 902.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:6137;urn:miriam:hgnc:12726;urn:miriam:pubmed:25051961;urn:miriam:taxonomy:10090;urn:miriam:hgnc:14338;urn:miriam:hgnc:6153;urn:miriam:hgnc:6137;urn:miriam:refseq:NM_002203;urn:miriam:ncbigene:3673;urn:miriam:ensembl:ENSG00000164171;urn:miriam:uniprot:P17301;urn:miriam:uniprot:P17301;urn:miriam:ncbigene:3673;urn:miriam:hgnc.symbol:ITGA2;urn:miriam:hgnc.symbol:ITGA2;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:GP6;urn:miriam:hgnc.symbol:GP6;urn:miriam:hgnc:14388;urn:miriam:ensembl:ENSG00000088053;urn:miriam:ncbigene:51206;urn:miriam:ncbigene:51206;urn:miriam:refseq:NM_001083899;urn:miriam:uniprot:Q9HCN6;urn:miriam:uniprot:Q9HCN6;urn:miriam:hgnc:12726;urn:miriam:taxonomy:9606;urn:miriam:refseq:NM_000552;urn:miriam:uniprot:P04275;urn:miriam:uniprot:P04275;urn:miriam:ncbigene:7450;urn:miriam:ncbigene:7450;urn:miriam:hgnc.symbol:VWF;urn:miriam:hgnc.symbol:VWF;urn:miriam:ensembl:ENSG00000110799;urn:miriam:refseq:NM_002211;urn:miriam:uniprot:P05556;urn:miriam:uniprot:P05556;urn:miriam:hgnc.symbol:ITGB1;urn:miriam:hgnc.symbol:ITGB1;urn:miriam:ncbigene:3688;urn:miriam:ncbigene:3688;urn:miriam:hgnc:6153;urn:miriam:ensembl:ENSG00000150093"
      hgnc "HGNC_SYMBOL:ITGA2;HGNC_SYMBOL:GP6;HGNC_SYMBOL:VWF;HGNC_SYMBOL:ITGB1"
      map_id "M121_19"
      name "GP6:alpha2beta1:VWF"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa40"
      uniprot "UNIPROT:P17301;UNIPROT:Q9HCN6;UNIPROT:P04275;UNIPROT:P05556"
    ]
    graphics [
      x 1742.5
      y 1941.755742606838
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "PUBMED:19286885"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_98"
      name "PMID:19286885"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re352"
      uniprot "NA"
    ]
    graphics [
      x 722.5
      y 1479.1638542042335
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      annotation "PUBMED:25051961"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_99"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re353"
      uniprot "NA"
    ]
    graphics [
      x 2162.5
      y 1793.7133098641311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:obo.go:GO%3A0030168"
      hgnc "NA"
      map_id "M121_221"
      name "platelet_space_activation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa430"
      uniprot "NA"
    ]
    graphics [
      x 1472.5
      y 1246.4685701968617
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_221"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      annotation "PUBMED:29472360"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_101"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re356"
      uniprot "NA"
    ]
    graphics [
      x 482.5
      y 2059.269634446779
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "PUBMED:25051961;PUBMED:19465929"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_100"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re355"
      uniprot "NA"
    ]
    graphics [
      x 2492.5
      y 841.4350242348289
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D013917;urn:miriam:taxonomy:9606"
      hgnc "NA"
      map_id "M121_224"
      name "thrombus_space_formation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa441"
      uniprot "NA"
    ]
    graphics [
      x 1870.9135463862924
      y 812.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_224"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "PUBMED:18026570;PUBMED:21789389"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_109"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re365"
      uniprot "NA"
    ]
    graphics [
      x 872.5
      y 1724.3683906431766
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "PUBMED:16391415"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_111"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re369"
      uniprot "NA"
    ]
    graphics [
      x 1922.5
      y 542.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:336;urn:miriam:refseq:NM_000685;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:uniprot:P30556;urn:miriam:uniprot:P30556;urn:miriam:ensembl:ENSG00000144891;urn:miriam:ncbigene:185;urn:miriam:ncbigene:185"
      hgnc "HGNC_SYMBOL:AGTR1"
      map_id "M121_246"
      name "AGTR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa484"
      uniprot "UNIPROT:P30556"
    ]
    graphics [
      x 842.5
      y 1524.6943946874349
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_246"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      annotation "PUBMED:8158359"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_110"
      name "PMID:8158359"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re366"
      uniprot "NA"
    ]
    graphics [
      x 1331.043215136001
      y 2543.3146446645715
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:336;urn:miriam:refseq:NM_000685;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:uniprot:P30556;urn:miriam:uniprot:P30556;urn:miriam:ensembl:ENSG00000144891;urn:miriam:ncbigene:185;urn:miriam:ncbigene:185"
      hgnc "HGNC_SYMBOL:AGTR1"
      map_id "M121_254"
      name "AGTR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa500"
      uniprot "UNIPROT:P30556"
    ]
    graphics [
      x 2088.7805189192004
      y 2255.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_254"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:obo.chebi:CHEBI%3A2718"
      hgnc "NA"
      map_id "M121_148"
      name "angiotensin_space_II"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa194"
      uniprot "NA"
    ]
    graphics [
      x 1181.043215136001
      y 2638.9883818562403
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_148"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      annotation "PUBMED:10969042;PUBMED:190881"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_25"
      name "PMID:19065996"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re157"
      uniprot "NA"
    ]
    graphics [
      x 1308.7805189192002
      y 2183.3146446645715
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      annotation "PUBMED:32048163"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_116"
      name "PMID:32048163"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re375"
      uniprot "NA"
    ]
    graphics [
      x 1915.5437299022901
      y 2822.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      annotation "PUBMED:32048163"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_40"
      name "PMID:32048163"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re183"
      uniprot "NA"
    ]
    graphics [
      x 722.5
      y 1605.5409586491112
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "PUBMED:20591974;PUBMED:8034668;PUBMED:11983698;PUBMED:2091055"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_118"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re377"
      uniprot "NA"
    ]
    graphics [
      x 1082.5
      y 1156.3529417498703
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      annotation "PUBMED:11983698"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_121"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re386"
      uniprot "NA"
    ]
    graphics [
      x 2291.043215136001
      y 2529.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "PUBMED:173529;PUBMED:8404594;PUBMED:32565254"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_117"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re376"
      uniprot "NA"
    ]
    graphics [
      x 3212.5
      y 1542.7257342680075
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "PUBMED:3124286;PUBMED:3096399;PUBMED:12091055;PUBMED:10373228"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_37"
      name "PMID:10373228, PMID:3124286"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re180"
      uniprot "NA"
    ]
    graphics [
      x 1592.5
      y 1643.119912305107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "PUBMED:23392115"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_70"
      name "PMID:23392115, PMID:32336612"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re307"
      uniprot "NA"
    ]
    graphics [
      x 782.5
      y 1147.835840060344
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000130234;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:ACE2"
      map_id "M121_203"
      name "ACE2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa394"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 2552.5
      y 1194.576720665756
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_203"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A55438;urn:miriam:taxonomy:9606"
      hgnc "NA"
      map_id "M121_208"
      name "angiotensin_space_I_minus_7"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa400"
      uniprot "NA"
    ]
    graphics [
      x 1712.5
      y 1227.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_208"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      annotation "PUBMED:18026570"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_108"
      name "PMID:18026570"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re364"
      uniprot "NA"
    ]
    graphics [
      x 1082.5
      y 976.3529417498704
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_002377;urn:miriam:ensembl:ENSG00000130368;urn:miriam:hgnc:6899;urn:miriam:ncbigene:4142;urn:miriam:ncbigene:4142;urn:miriam:hgnc.symbol:MAS1;urn:miriam:uniprot:P04201;urn:miriam:uniprot:P04201;urn:miriam:hgnc.symbol:MAS1"
      hgnc "HGNC_SYMBOL:MAS1"
      map_id "M121_250"
      name "MAS1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa496"
      uniprot "UNIPROT:P04201"
    ]
    graphics [
      x 482.5
      y 1022.6765796688758
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_250"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_002377;urn:miriam:ensembl:ENSG00000130368;urn:miriam:hgnc:6899;urn:miriam:ncbigene:4142;urn:miriam:ncbigene:4142;urn:miriam:hgnc.symbol:MAS1;urn:miriam:uniprot:P04201;urn:miriam:uniprot:P04201;urn:miriam:hgnc.symbol:MAS1"
      hgnc "HGNC_SYMBOL:MAS1"
      map_id "M121_245"
      name "MAS1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa483"
      uniprot "UNIPROT:P04201"
    ]
    graphics [
      x 1052.5
      y 1018.8948945564143
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_245"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      annotation "PUBMED:16008552"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_67"
      name "PMID:32336612, PMID:16008552"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re304"
      uniprot "NA"
    ]
    graphics [
      x 1292.5
      y 1913.3146446645712
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000130234;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:ACE2"
      map_id "M121_206"
      name "ACE2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa398"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1592.5
      y 983.1199123051069
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_206"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:2697049;urn:miriam:mesh:D012327"
      hgnc "NA"
      map_id "M121_242"
      name "SARS_minus_CoV_minus_2_space_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa480"
      uniprot "NA"
    ]
    graphics [
      x 1952.5
      y 1952.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_242"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      annotation "PUBMED:32525548"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_128"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re396"
      uniprot "NA"
    ]
    graphics [
      x 2372.5
      y 1552.0608348716244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      annotation "PUBMED:32299776"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_79"
      name "PMID:32299776"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re325"
      uniprot "NA"
    ]
    graphics [
      x 1982.5
      y 1502.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "PUBMED:32299776;PUBMED:11290788"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_57"
      name "PMID:11290788, PMID:32299776"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re274"
      uniprot "NA"
    ]
    graphics [
      x 2702.5
      y 1664.2824110750719
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:O00187;urn:miriam:uniprot:O00187;urn:miriam:taxonomy:9606;urn:miriam:refseq:NM_006610;urn:miriam:ensembl:ENSG00000009724;urn:miriam:ec-code:3.4.21.104;urn:miriam:hgnc:6902;urn:miriam:hgnc.symbol:MASP2;urn:miriam:ncbigene:10747;urn:miriam:hgnc.symbol:MASP2;urn:miriam:ncbigene:10747"
      hgnc "HGNC_SYMBOL:MASP2"
      map_id "M121_188"
      name "MASP2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa357"
      uniprot "UNIPROT:O00187"
    ]
    graphics [
      x 2402.5
      y 1456.0046064486714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_188"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:O00187;urn:miriam:uniprot:O00187;urn:miriam:taxonomy:9606;urn:miriam:refseq:NM_006610;urn:miriam:ensembl:ENSG00000009724;urn:miriam:ec-code:3.4.21.104;urn:miriam:hgnc:6902;urn:miriam:hgnc.symbol:MASP2;urn:miriam:ncbigene:10747;urn:miriam:hgnc.symbol:MASP2;urn:miriam:ncbigene:10747"
      hgnc "HGNC_SYMBOL:MASP2"
      map_id "M121_233"
      name "MASP2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa459"
      uniprot "UNIPROT:O00187"
    ]
    graphics [
      x 1082.5
      y 1366.3529417498703
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_233"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      annotation "PUBMED:32299776"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_90"
      name "PMID:32299776"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re339"
      uniprot "NA"
    ]
    graphics [
      x 1712.5
      y 957.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      annotation "PUBMED:21664989"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_59"
      name "PMID:22949645"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re276"
      uniprot "NA"
    ]
    graphics [
      x 1837.432708728739
      y 422.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:mesh:D00318"
      hgnc "NA"
      map_id "M121_191"
      name "C4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa363"
      uniprot "NA"
    ]
    graphics [
      x 2132.5
      y 1385.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_191"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_001002029;urn:miriam:ensembl:ENSG00000224389;urn:miriam:taxonomy:9606;urn:miriam:ncbigene:100293534;urn:miriam:hgnc.symbol:C4B;urn:miriam:ncbigene:721;urn:miriam:hgnc.symbol:C4B;urn:miriam:hgnc:1324;urn:miriam:uniprot:P0C0L5;urn:miriam:uniprot:P0C0L5"
      hgnc "HGNC_SYMBOL:C4B"
      map_id "M121_193"
      name "C4b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa365"
      uniprot "UNIPROT:P0C0L5"
    ]
    graphics [
      x 1682.5
      y 657.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_193"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:C4A;urn:miriam:ncbigene:720;urn:miriam:hgnc.symbol:C4A;urn:miriam:ncbigene:720;urn:miriam:hgnc:1323;urn:miriam:ensembl:ENSG00000244731;urn:miriam:uniprot:P0C0L4;urn:miriam:uniprot:P0C0L4;urn:miriam:refseq:NM_007293"
      hgnc "HGNC_SYMBOL:C4A"
      map_id "M121_192"
      name "C4a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa364"
      uniprot "UNIPROT:P0C0L4"
    ]
    graphics [
      x 2492.5
      y 781.4350242348289
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_192"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      annotation "PUBMED:26521297"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_60"
      name "PMID:26521297"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re278"
      uniprot "NA"
    ]
    graphics [
      x 2432.5
      y 1556.0089009888093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      annotation "PUBMED:19362461"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_62"
      name "PMID:19362461"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re288"
      uniprot "NA"
    ]
    graphics [
      x 1562.5
      y 651.3485724045411
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ec-code:3.4.21.46;urn:miriam:taxonomy:9606;urn:miriam:hgnc:2771;urn:miriam:hgnc.symbol:CFD;urn:miriam:hgnc.symbol:CFD;urn:miriam:refseq:NM_001928;urn:miriam:ensembl:ENSG00000197766;urn:miriam:uniprot:P00746;urn:miriam:uniprot:P00746;urn:miriam:ncbigene:1675;urn:miriam:ncbigene:1675"
      hgnc "HGNC_SYMBOL:CFD"
      map_id "M121_197"
      name "CFI"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa385"
      uniprot "UNIPROT:P00746"
    ]
    graphics [
      x 692.5
      y 1293.5606755964814
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_197"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_001002029;urn:miriam:ensembl:ENSG00000224389;urn:miriam:taxonomy:9606;urn:miriam:ncbigene:100293534;urn:miriam:hgnc.symbol:C4b;urn:miriam:mesh:C032261;urn:miriam:ncbigene:721;urn:miriam:hgnc.symbol:C4B;urn:miriam:hgnc:1324;urn:miriam:uniprot:P0C0L5;urn:miriam:uniprot:P0C0L5"
      hgnc "HGNC_SYMBOL:C4b;HGNC_SYMBOL:C4B"
      map_id "M121_198"
      name "C4d"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa387"
      uniprot "UNIPROT:P0C0L5"
    ]
    graphics [
      x 842.5
      y 859.0525412959387
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_198"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      annotation "PUBMED:32299776"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_71"
      name "PMID:32299776"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re308"
      uniprot "NA"
    ]
    graphics [
      x 2252.5
      y 1682.4583143700918
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "PUBMED:19362461"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_63"
      name "PMID: 32299776"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re289"
      uniprot "NA"
    ]
    graphics [
      x 1682.5
      y 1737.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32299776;urn:miriam:taxonomy:9606;urn:miriam:mesh:D018366"
      hgnc "NA"
      map_id "M121_199"
      name "C4d_space_deposition"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa388"
      uniprot "NA"
    ]
    graphics [
      x 932.5
      y 1236.2469660824179
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_199"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      annotation "PUBMED:19362461"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_106"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re361"
      uniprot "NA"
    ]
    graphics [
      x 1412.5
      y 682.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:mesh:D007681"
      hgnc "NA"
      map_id "M121_200"
      name "septal_space_capillary_space_necrosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa389"
      uniprot "NA"
    ]
    graphics [
      x 2312.5
      y 759.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_200"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:taxonomy:2697049;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "M121_202"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa391"
      uniprot "UNIPROT:P0DTC2;UNIPROT:P59594"
    ]
    graphics [
      x 1712.5
      y 1677.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_202"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      annotation "PUBMED:32275855"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_68"
      name "PMID:32275855"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re305"
      uniprot "NA"
    ]
    graphics [
      x 1142.5
      y 614.361809414056
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      annotation "PUBMED:32299776"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_72"
      name "PMID:32299776"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re309"
      uniprot "NA"
    ]
    graphics [
      x 962.5
      y 1747.6441692926126
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0005579;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1352;urn:miriam:hgnc:1353;urn:miriam:hgnc:1354;urn:miriam:hgnc:1346;urn:miriam:mesh:D050776;urn:miriam:hgnc:1358;urn:miriam:mesh:D015938;urn:miriam:hgnc:1339;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:C6;urn:miriam:hgnc.symbol:C6;urn:miriam:refseq:NM_000065;urn:miriam:ensembl:ENSG00000039537;urn:miriam:uniprot:P13671;urn:miriam:uniprot:P13671;urn:miriam:hgnc:1339;urn:miriam:ncbigene:729;urn:miriam:ncbigene:729;urn:miriam:hgnc.symbol:C8A;urn:miriam:refseq:NM_000562;urn:miriam:hgnc.symbol:C8A;urn:miriam:hgnc:1352;urn:miriam:uniprot:P07357;urn:miriam:uniprot:P07357;urn:miriam:ncbigene:731;urn:miriam:ncbigene:731;urn:miriam:ensembl:ENSG00000157131;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1331;urn:miriam:hgnc.symbol:C5;urn:miriam:mesh:D050776;urn:miriam:refseq:NM_001735;urn:miriam:ensembl:ENSG00000106804;urn:miriam:uniprot:P01031;urn:miriam:uniprot:P01031;urn:miriam:ncbigene:727;urn:miriam:ncbigene:727;urn:miriam:uniprot:P10643;urn:miriam:uniprot:P10643;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000112936;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc:1346;urn:miriam:refseq:NM_000587;urn:miriam:ensembl:ENSG00000176919;urn:miriam:refseq:NM_000606;urn:miriam:hgnc:1354;urn:miriam:hgnc.symbol:C8G;urn:miriam:ncbigene:733;urn:miriam:hgnc.symbol:C8G;urn:miriam:ncbigene:733;urn:miriam:uniprot:P07360;urn:miriam:uniprot:P07360;urn:miriam:ncbigene:735;urn:miriam:ncbigene:735;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1358;urn:miriam:ensembl:ENSG00000113600;urn:miriam:hgnc.symbol:C9;urn:miriam:hgnc.symbol:C9;urn:miriam:refseq:NM_001737;urn:miriam:uniprot:P02748;urn:miriam:uniprot:P02748;urn:miriam:refseq:NM_000066;urn:miriam:hgnc.symbol:C8B;urn:miriam:hgnc.symbol:C8B;urn:miriam:hgnc:1353;urn:miriam:ncbigene:732;urn:miriam:ensembl:ENSG00000021852;urn:miriam:ncbigene:732;urn:miriam:uniprot:P07358;urn:miriam:uniprot:P07358"
      hgnc "HGNC_SYMBOL:C6;HGNC_SYMBOL:C8A;HGNC_SYMBOL:C5;HGNC_SYMBOL:C7;HGNC_SYMBOL:C8G;HGNC_SYMBOL:C9;HGNC_SYMBOL:C8B"
      map_id "M121_14"
      name "C5b_minus_9"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa32"
      uniprot "UNIPROT:P13671;UNIPROT:P07357;UNIPROT:P01031;UNIPROT:P10643;UNIPROT:P07360;UNIPROT:P02748;UNIPROT:P07358"
    ]
    graphics [
      x 362.4999999999998
      y 1213.2995265724076
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0005579;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1352;urn:miriam:hgnc:1353;urn:miriam:hgnc:1354;urn:miriam:hgnc:1346;urn:miriam:mesh:D050776;urn:miriam:hgnc:1358;urn:miriam:mesh:D015938;urn:miriam:hgnc:1339;urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:taxonomy:2697049;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S;urn:miriam:refseq:NM_000066;urn:miriam:hgnc.symbol:C8B;urn:miriam:hgnc.symbol:C8B;urn:miriam:hgnc:1353;urn:miriam:ncbigene:732;urn:miriam:ensembl:ENSG00000021852;urn:miriam:ncbigene:732;urn:miriam:uniprot:P07358;urn:miriam:uniprot:P07358;urn:miriam:ncbigene:735;urn:miriam:ncbigene:735;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1358;urn:miriam:ensembl:ENSG00000113600;urn:miriam:hgnc.symbol:C9;urn:miriam:hgnc.symbol:C9;urn:miriam:refseq:NM_001737;urn:miriam:uniprot:P02748;urn:miriam:uniprot:P02748;urn:miriam:ensembl:ENSG00000176919;urn:miriam:refseq:NM_000606;urn:miriam:hgnc:1354;urn:miriam:hgnc.symbol:C8G;urn:miriam:ncbigene:733;urn:miriam:hgnc.symbol:C8G;urn:miriam:ncbigene:733;urn:miriam:uniprot:P07360;urn:miriam:uniprot:P07360;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:C6;urn:miriam:hgnc.symbol:C6;urn:miriam:refseq:NM_000065;urn:miriam:ensembl:ENSG00000039537;urn:miriam:uniprot:P13671;urn:miriam:uniprot:P13671;urn:miriam:hgnc:1339;urn:miriam:ncbigene:729;urn:miriam:ncbigene:729;urn:miriam:uniprot:P10643;urn:miriam:uniprot:P10643;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000112936;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc:1346;urn:miriam:refseq:NM_000587;urn:miriam:hgnc.symbol:C8A;urn:miriam:refseq:NM_000562;urn:miriam:hgnc.symbol:C8A;urn:miriam:hgnc:1352;urn:miriam:uniprot:P07357;urn:miriam:uniprot:P07357;urn:miriam:ncbigene:731;urn:miriam:ncbigene:731;urn:miriam:ensembl:ENSG00000157131;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1331;urn:miriam:hgnc.symbol:C5;urn:miriam:mesh:D050776;urn:miriam:refseq:NM_001735;urn:miriam:ensembl:ENSG00000106804;urn:miriam:uniprot:P01031;urn:miriam:uniprot:P01031;urn:miriam:ncbigene:727;urn:miriam:ncbigene:727"
      hgnc "HGNC_SYMBOL:S;HGNC_SYMBOL:C8B;HGNC_SYMBOL:C9;HGNC_SYMBOL:C8G;HGNC_SYMBOL:C6;HGNC_SYMBOL:C7;HGNC_SYMBOL:C8A;HGNC_SYMBOL:C5"
      map_id "M121_22"
      name "C5b_minus_9"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa43"
      uniprot "UNIPROT:P0DTC2;UNIPROT:P59594;UNIPROT:P07358;UNIPROT:P02748;UNIPROT:P07360;UNIPROT:P13671;UNIPROT:P10643;UNIPROT:P07357;UNIPROT:P01031"
    ]
    graphics [
      x 1472.5
      y 916.4685701968617
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      annotation "PUBMED:6796960"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_55"
      name "PMID:6796960"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re267"
      uniprot "NA"
    ]
    graphics [
      x 1502.5
      y 1717.3397236403869
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      annotation "PUBMED:9012652"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_92"
      name "PMID:9012652"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re341"
      uniprot "NA"
    ]
    graphics [
      x 1532.5
      y 2069.738142841986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      annotation "PUBMED:19362461"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_64"
      name "PMID:32299776"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re292"
      uniprot "NA"
    ]
    graphics [
      x 752.5
      y 1222.6097915613125
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32299776;urn:miriam:taxonomy:9606;urn:miriam:mesh:D018366"
      hgnc "NA"
      map_id "M121_201"
      name "C5b_minus_9_space_deposition"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa390"
      uniprot "NA"
    ]
    graphics [
      x 1172.5
      y 1647.3156502059937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_201"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      annotation "PUBMED:19362461"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_104"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re359"
      uniprot "NA"
    ]
    graphics [
      x 242.5
      y 1475.5390227688379
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      annotation "PUBMED:10807586"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_105"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re360"
      uniprot "NA"
    ]
    graphics [
      x 2372.5
      y 1972.0608348716244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:obo.go:GO%3A0006915"
      hgnc "NA"
      map_id "M121_211"
      name "apoptosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa408"
      uniprot "NA"
    ]
    graphics [
      x 2898.7805189192004
      y 2177.7034190231884
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_211"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:obo.go:GO%3A0030168"
      hgnc "NA"
      map_id "M121_212"
      name "platelet_space_aggregation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa409"
      uniprot "NA"
    ]
    graphics [
      x 1188.7805189192002
      y 2397.3156502059937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_212"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_249"
      name "s586"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa495"
      uniprot "NA"
    ]
    graphics [
      x 1464.1804181766208
      y 2782.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_249"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:PLG;urn:miriam:hgnc.symbol:PLG;urn:miriam:ec-code:3.4.21.7;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000122194;urn:miriam:ncbigene:5340;urn:miriam:ncbigene:5340;urn:miriam:hgnc:9071;urn:miriam:refseq:NM_000301;urn:miriam:uniprot:P00747;urn:miriam:uniprot:P00747"
      hgnc "HGNC_SYMBOL:PLG"
      map_id "M121_160"
      name "Plasminogen"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa211"
      uniprot "UNIPROT:P00747"
    ]
    graphics [
      x 1712.5
      y 1647.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_160"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      annotation "PUBMED:3850647;PUBMED:89876;PUBMED:6539333;PUBMED:2966802"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_35"
      name "PMID: 89876, PMID:385647"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re178"
      uniprot "NA"
    ]
    graphics [
      x 1862.5
      y 662.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ec-code:3.4.21.68;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000104368;urn:miriam:hgnc:9051;urn:miriam:hgnc.symbol:PLAT;urn:miriam:hgnc.symbol:PLAT;urn:miriam:uniprot:P00750;urn:miriam:uniprot:P00750;urn:miriam:ncbigene:5327;urn:miriam:ncbigene:5327;urn:miriam:refseq:NM_000930"
      hgnc "HGNC_SYMBOL:PLAT"
      map_id "M121_162"
      name "PLAT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa213"
      uniprot "UNIPROT:P00750"
    ]
    graphics [
      x 1706.1439857514508
      y 1407.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_162"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:ncbigene:5328;urn:miriam:ncbigene:5328;urn:miriam:hgnc.symbol:PLAU;urn:miriam:hgnc.symbol:PLAU;urn:miriam:ec-code:3.4.21.73;urn:miriam:ensembl:ENSG00000122861;urn:miriam:hgnc:9052;urn:miriam:uniprot:P00749;urn:miriam:uniprot:P00749;urn:miriam:refseq:NM_002658"
      hgnc "HGNC_SYMBOL:PLAU"
      map_id "M121_167"
      name "PLAU"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa236"
      uniprot "UNIPROT:P00749"
    ]
    graphics [
      x 1442.5
      y 2052.3750173377257
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_167"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:ncbigene:2160;urn:miriam:ncbigene:2160;urn:miriam:uniprot:P03951;urn:miriam:uniprot:P03951;urn:miriam:ensembl:ENSG00000088926;urn:miriam:mesh:D015945;urn:miriam:hgnc.symbol:F11;urn:miriam:brenda:3.4.21.27;urn:miriam:hgnc:3529;urn:miriam:ec-code:3.4.21.27;urn:miriam:refseq:NM_000128"
      hgnc "HGNC_SYMBOL:F11"
      map_id "M121_137"
      name "F11a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa173"
      uniprot "UNIPROT:P03951"
    ]
    graphics [
      x 2192.5
      y 1133.7133098641311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_137"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_000892;urn:miriam:ensembl:ENSG00000164344;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.21.34;urn:miriam:uniprot:P03952;urn:miriam:uniprot:P03952;urn:miriam:hgnc:6371;urn:miriam:ncbigene:3818;urn:miriam:ncbigene:3818;urn:miriam:hgnc.symbol:KLKB1;urn:miriam:hgnc.symbol:KLKB1"
      hgnc "HGNC_SYMBOL:KLKB1"
      map_id "M121_132"
      name "KLKB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa167"
      uniprot "UNIPROT:P03952"
    ]
    graphics [
      x 602.5
      y 1269.295395563012
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:PLG;urn:miriam:ec-code:3.4.21.7;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000122194;urn:miriam:ncbigene:5340;urn:miriam:ncbigene:5340;urn:miriam:hgnc:9071;urn:miriam:refseq:NM_000301;urn:miriam:mesh:D005341;urn:miriam:brenda:3.4.21.7;urn:miriam:uniprot:P00747;urn:miriam:uniprot:P00747"
      hgnc "HGNC_SYMBOL:PLG"
      map_id "M121_161"
      name "Plasmin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa212"
      uniprot "UNIPROT:P00747"
    ]
    graphics [
      x 1412.5
      y 592.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_161"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      annotation "PUBMED:29096812;PUBMED:10574983;PUBMED:32172226"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_97"
      name "PMID:29096812, PMID:10574983, PMID:32172226"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re346"
      uniprot "NA"
    ]
    graphics [
      x 212.5
      y 1660.4664758212084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      annotation "PUBMED:2437112"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_80"
      name "PMID:2437112, DOI:10.1101/2020.04.25.20077842"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re326"
      uniprot "NA"
    ]
    graphics [
      x 1251.4244996024302
      y 530.6357978615079
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P08697;urn:miriam:uniprot:P08697;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000167711;urn:miriam:ncbigene:5345;urn:miriam:ncbigene:5345;urn:miriam:hgnc:9075;urn:miriam:refseq:NM_000934;urn:miriam:hgnc.symbol:SERPINF2;urn:miriam:hgnc.symbol:SERPINF2"
      hgnc "HGNC_SYMBOL:SERPINF2"
      map_id "M121_215"
      name "SERPINF2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa412"
      uniprot "UNIPROT:P08697"
    ]
    graphics [
      x 1712.5
      y 1077.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_215"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:2697049;urn:miriam:mesh:D012327"
      hgnc "NA"
      map_id "M121_243"
      name "SARS_minus_CoV_minus_2_space_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa481"
      uniprot "NA"
    ]
    graphics [
      x 1232.5
      y 1242.9973497539163
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_243"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:2437112;urn:miriam:hgnc:9075;urn:miriam:mesh:D005341;urn:miriam:uniprot:P08697;urn:miriam:uniprot:P08697;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000167711;urn:miriam:ncbigene:5345;urn:miriam:ncbigene:5345;urn:miriam:hgnc:9075;urn:miriam:refseq:NM_000934;urn:miriam:hgnc.symbol:SERPINF2;urn:miriam:hgnc.symbol:SERPINF2;urn:miriam:hgnc.symbol:PLG;urn:miriam:ec-code:3.4.21.7;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000122194;urn:miriam:ncbigene:5340;urn:miriam:ncbigene:5340;urn:miriam:hgnc:9071;urn:miriam:refseq:NM_000301;urn:miriam:mesh:D005341;urn:miriam:brenda:3.4.21.7;urn:miriam:uniprot:P00747;urn:miriam:uniprot:P00747"
      hgnc "HGNC_SYMBOL:SERPINF2;HGNC_SYMBOL:PLG"
      map_id "M121_17"
      name "SERPINF2:Plasmin"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa38"
      uniprot "UNIPROT:P08697;UNIPROT:P00747"
    ]
    graphics [
      x 872.5
      y 1423.2281760082744
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      annotation "PUBMED:8136018"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_36"
      name "PMID: 8136018"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re179"
      uniprot "NA"
    ]
    graphics [
      x 1518.7805189192002
      y 2279.738142841986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P01008;urn:miriam:uniprot:P01008;urn:miriam:hgnc:775;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000117601;urn:miriam:ncbigene:462;urn:miriam:ncbigene:462;urn:miriam:refseq:NM_000488;urn:miriam:hgnc.symbol:SERPINC1;urn:miriam:hgnc.symbol:SERPINC1"
      hgnc "HGNC_SYMBOL:SERPINC1"
      map_id "M121_155"
      name "Antithrombin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa202"
      uniprot "UNIPROT:P01008"
    ]
    graphics [
      x 1038.7805189192002
      y 2451.9867962075773
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_155"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P00734;urn:miriam:uniprot:P00734;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.21.5;urn:miriam:hgnc:3535;urn:miriam:refseq:NM_000506;urn:miriam:ensembl:ENSG00000180210;urn:miriam:hgnc.symbol:F2;urn:miriam:hgnc.symbol:F2;urn:miriam:ncbigene:2147;urn:miriam:ncbigene:2147"
      hgnc "HGNC_SYMBOL:F2"
      map_id "M121_145"
      name "Thrombin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa181"
      uniprot "UNIPROT:P00734"
    ]
    graphics [
      x 2051.043215136001
      y 2672.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_145"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D013917;urn:miriam:hgnc:775;urn:miriam:taxonomy:9606;urn:miriam:mesh:C046193;urn:miriam:pubmed:22930518;urn:miriam:mesh:D013917;urn:miriam:uniprot:P00734;urn:miriam:uniprot:P00734;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.21.5;urn:miriam:hgnc:3535;urn:miriam:refseq:NM_000506;urn:miriam:ensembl:ENSG00000180210;urn:miriam:hgnc.symbol:F2;urn:miriam:ncbigene:2147;urn:miriam:ncbigene:2147;urn:miriam:uniprot:P01008;urn:miriam:uniprot:P01008;urn:miriam:hgnc:775;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000117601;urn:miriam:ncbigene:462;urn:miriam:ncbigene:462;urn:miriam:refseq:NM_000488;urn:miriam:hgnc.symbol:SERPINC1;urn:miriam:hgnc.symbol:SERPINC1"
      hgnc "HGNC_SYMBOL:F2;HGNC_SYMBOL:SERPINC1"
      map_id "M121_4"
      name "TAT_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa14"
      uniprot "UNIPROT:P00734;UNIPROT:P01008"
    ]
    graphics [
      x 2178.7805189192004
      y 2273.713309864131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      annotation "PUBMED:579490"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_34"
      name "PMID:579490"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re174"
      uniprot "NA"
    ]
    graphics [
      x 1652.5
      y 1437.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      annotation "PUBMED:8388351;PUBMED:6282863"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_47"
      name "PMID:8388351"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re256"
      uniprot "NA"
    ]
    graphics [
      x 1382.5
      y 1752.4847495740523
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      annotation "PUBMED:15746105"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_122"
      name "NA"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re390"
      uniprot "NA"
    ]
    graphics [
      x 632.5
      y 1496.7702022570459
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      annotation "PUBMED:23809134"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_38"
      name "PMID:23809134"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re181"
      uniprot "NA"
    ]
    graphics [
      x 1347.3095774771057
      y 2764.745367675022
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      annotation "PUBMED:21304106;PUBMED:8631976"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_27"
      name "PMID:21304106, PMID:8631976"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re160"
      uniprot "NA"
    ]
    graphics [
      x 1742.5
      y 1671.30614738341
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc:3530;urn:miriam:ensembl:ENSG00000131187;urn:miriam:ncbigene:2161;urn:miriam:ncbigene:2161;urn:miriam:mesh:D015956;urn:miriam:refseq:NM_000505;urn:miriam:hgnc.symbol:F12;urn:miriam:brenda:3.4.21.38;urn:miriam:uniprot:P00748;urn:miriam:uniprot:P00748;urn:miriam:ec-code:3.4.21.38"
      hgnc "HGNC_SYMBOL:F12"
      map_id "M121_131"
      name "F12a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa165"
      uniprot "UNIPROT:P00748"
    ]
    graphics [
      x 302.5
      y 1401.2592658750543
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_131"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:ncbigene:2160;urn:miriam:ncbigene:2160;urn:miriam:uniprot:P03951;urn:miriam:uniprot:P03951;urn:miriam:ensembl:ENSG00000088926;urn:miriam:hgnc.symbol:F11;urn:miriam:hgnc.symbol:F11;urn:miriam:hgnc:3529;urn:miriam:ec-code:3.4.21.27;urn:miriam:refseq:NM_000128"
      hgnc "HGNC_SYMBOL:F11"
      map_id "M121_136"
      name "F11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa172"
      uniprot "UNIPROT:P03951"
    ]
    graphics [
      x 2462.5
      y 2101.435024234829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      annotation "PUBMED:7391081;PUBMED:864009"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_26"
      name "PMID:7391081"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re159"
      uniprot "NA"
    ]
    graphics [
      x 362.5
      y 1119.852522579734
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      annotation "PUBMED:21304106"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_82"
      name "PMID:21304106"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re329"
      uniprot "NA"
    ]
    graphics [
      x 302.5
      y 971.2739961810923
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:brenda:3.4.21.34;urn:miriam:refseq:NM_000892;urn:miriam:ensembl:ENSG00000164344;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.21.34;urn:miriam:uniprot:P03952;urn:miriam:uniprot:P03952;urn:miriam:mesh:D020842;urn:miriam:hgnc:6371;urn:miriam:ncbigene:3818;urn:miriam:ncbigene:3818;urn:miriam:hgnc.symbol:KLKB1"
      hgnc "HGNC_SYMBOL:KLKB1"
      map_id "M121_172"
      name "Kallikrein"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa251"
      uniprot "UNIPROT:P03952"
    ]
    graphics [
      x 1592.5
      y 593.1199123051069
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_172"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      annotation "PUBMED:4627469;PUBMED:6768384"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_42"
      name "PMID:6768384, PMID:4627469"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re202"
      uniprot "NA"
    ]
    graphics [
      x 1382.5
      y 1269.3284249102053
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      annotation "PUBMED:692685"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_83"
      name "PMID:692685"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re330"
      uniprot "NA"
    ]
    graphics [
      x 422.5
      y 1414.5749572961008
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:REN;urn:miriam:hgnc.symbol:REN;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P00797;urn:miriam:uniprot:P00797;urn:miriam:hgnc:9958;urn:miriam:ensembl:ENSG00000143839;urn:miriam:ncbigene:5972;urn:miriam:refseq:NM_000537;urn:miriam:ncbigene:5972;urn:miriam:ec-code:3.4.23.15"
      hgnc "HGNC_SYMBOL:REN"
      map_id "M121_216"
      name "Prorenin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa415"
      uniprot "UNIPROT:P00797"
    ]
    graphics [
      x 918.7805189192002
      y 2294.647806417508
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_216"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:REN;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P00797;urn:miriam:uniprot:P00797;urn:miriam:hgnc:9958;urn:miriam:ensembl:ENSG00000143839;urn:miriam:ncbigene:5972;urn:miriam:refseq:NM_000537;urn:miriam:ncbigene:5972;urn:miriam:ec-code:3.4.23.15"
      hgnc "HGNC_SYMBOL:REN"
      map_id "M121_151"
      name "REN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa197"
      uniprot "UNIPROT:P00797"
    ]
    graphics [
      x 752.5
      y 2018.3665515504927
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_151"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      annotation "PUBMED:10585461;PUBMED:6172448;PUBMED:30934934"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_23"
      name "PMID:10585461"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re154"
      uniprot "NA"
    ]
    graphics [
      x 482.5
      y 2225.3602980587457
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P01019;urn:miriam:uniprot:P01019;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:AGT;urn:miriam:hgnc.symbol:AGT;urn:miriam:ensembl:ENSG00000135744;urn:miriam:hgnc:333;urn:miriam:ncbigene:183;urn:miriam:ncbigene:183;urn:miriam:refseq:NM_000029"
      hgnc "HGNC_SYMBOL:AGT"
      map_id "M121_150"
      name "AGT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa196"
      uniprot "UNIPROT:P01019"
    ]
    graphics [
      x 1437.3095774771057
      y 2812.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_150"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:obo.chebi:CHEBI%3A2718"
      hgnc "NA"
      map_id "M121_149"
      name "angiotensin_space_I"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa195"
      uniprot "NA"
    ]
    graphics [
      x 542.5
      y 1153.5654979241358
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_149"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:17598838;urn:miriam:taxonomy:9606;urn:miriam:intact:EBI-10087151;urn:miriam:hgnc:6371;urn:miriam:hgnc:6383;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:KNG1;urn:miriam:hgnc.symbol:KNG1;urn:miriam:hgnc.symbol:6383;urn:miriam:ncbigene:3827;urn:miriam:ncbigene:3827;urn:miriam:uniprot:P01042;urn:miriam:uniprot:P01042;urn:miriam:refseq:NM_001102416;urn:miriam:hgnc:6383;urn:miriam:ensembl:ENSG00000113889;urn:miriam:refseq:NM_000892;urn:miriam:ensembl:ENSG00000164344;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.21.34;urn:miriam:uniprot:P03952;urn:miriam:uniprot:P03952;urn:miriam:hgnc:6371;urn:miriam:ncbigene:3818;urn:miriam:ncbigene:3818;urn:miriam:hgnc.symbol:KLKB1;urn:miriam:hgnc.symbol:KLKB1"
      hgnc "HGNC_SYMBOL:KNG1;HGNC_SYMBOL:6383;HGNC_SYMBOL:KLKB1"
      map_id "M121_1"
      name "KNG1:KLKB1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa11"
      uniprot "UNIPROT:P01042;UNIPROT:P03952"
    ]
    graphics [
      x 602.5
      y 1239.295395563012
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D019679;urn:miriam:hgnc.symbol:KNG1;urn:miriam:taxonomy:9606;urn:miriam:ncbigene:3827;urn:miriam:ncbigene:3827;urn:miriam:uniprot:P01042;urn:miriam:uniprot:P01042;urn:miriam:refseq:NM_001102416;urn:miriam:hgnc:6383;urn:miriam:ensembl:ENSG00000113889"
      hgnc "HGNC_SYMBOL:KNG1"
      map_id "M121_171"
      name "Kininogen"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa250"
      uniprot "UNIPROT:P01042"
    ]
    graphics [
      x 662.5
      y 878.0142158729253
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_171"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:obo.chebi:CHEBI%3A3165"
      hgnc "NA"
      map_id "M121_209"
      name "Bradykinin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa402"
      uniprot "NA"
    ]
    graphics [
      x 1022.5
      y 1177.8045420565481
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_209"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      annotation "PUBMED:9066005"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_113"
      name "PMID:9066005"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re372"
      uniprot "NA"
    ]
    graphics [
      x 392.5
      y 1739.9630530945751
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      annotation "PUBMED:10969042"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_115"
      name "PMID:10749699"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re374"
      uniprot "NA"
    ]
    graphics [
      x 1592.5
      y 1463.119912305107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:2707;urn:miriam:uniprot:P12821;urn:miriam:uniprot:P12821;urn:miriam:ec-code:3.4.15.1;urn:miriam:taxonomy:9606;urn:miriam:ncbigene:1636;urn:miriam:ncbigene:1636;urn:miriam:hgnc.symbol:ACE;urn:miriam:refseq:NM_000789;urn:miriam:hgnc.symbol:ACE;urn:miriam:ec-code:3.2.1.-;urn:miriam:ensembl:ENSG00000159640"
      hgnc "HGNC_SYMBOL:ACE"
      map_id "M121_152"
      name "ACE"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa198"
      uniprot "UNIPROT:P12821"
    ]
    graphics [
      x 798.7805189192002
      y 2293.7234603533598
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_152"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:C079000;urn:miriam:taxonomy:9606"
      hgnc "NA"
      map_id "M121_210"
      name "Bradykinin(1_minus_5)"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa407"
      uniprot "NA"
    ]
    graphics [
      x 1472.5
      y 1066.4685701968617
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_210"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D010446"
      hgnc "NA"
      map_id "M121_205"
      name "Small_space_peptide"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa397"
      uniprot "NA"
    ]
    graphics [
      x 1804.5465868210804
      y 1652.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_205"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      annotation "PUBMED:5932931"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_123"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re391"
      uniprot "NA"
    ]
    graphics [
      x 782.5
      y 1994.1491183596902
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_266"
      name "s618"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa526"
      uniprot "NA"
    ]
    graphics [
      x 1578.7805189192002
      y 2093.119912305107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_266"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A27584"
      hgnc "NA"
      map_id "M121_256"
      name "aldosterone"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa503"
      uniprot "NA"
    ]
    graphics [
      x 1412.5
      y 532.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_256"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      annotation "PUBMED:8202152"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_119"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re379"
      uniprot "NA"
    ]
    graphics [
      x 512.5
      y 1370.0457199714222
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      annotation "PUBMED:32252108"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_130"
      name "NA"
      node_subtype "MODULATION"
      node_type "reaction"
      org_id "re399"
      uniprot "NA"
    ]
    graphics [
      x 2792.5
      y 907.8426717949401
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_130"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D007008;urn:miriam:taxonomy:9606"
      hgnc "NA"
      map_id "M121_270"
      name "Hypokalemia"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa531"
      uniprot "NA"
    ]
    graphics [
      x 2102.5
      y 1507.1941722470388
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_270"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      annotation "PUBMED:32525548"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_129"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re398"
      uniprot "NA"
    ]
    graphics [
      x 1592.5
      y 1403.119912305107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_129"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:20689271;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_271"
      name "s86"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa532"
      uniprot "NA"
    ]
    graphics [
      x 932.5
      y 786.0001579346998
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_271"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29103"
      hgnc "NA"
      map_id "M121_265"
      name "K_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa521"
      uniprot "NA"
    ]
    graphics [
      x 1368.7805189192002
      y 2146.912190820152
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_265"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A27584"
      hgnc "NA"
      map_id "M121_260"
      name "aldosterone"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa509"
      uniprot "NA"
    ]
    graphics [
      x 902.5
      y 1871.7973208312728
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_260"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      annotation "PUBMED:27045029"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_126"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re394"
      uniprot "NA"
    ]
    graphics [
      x 872.5
      y 2169.230133303526
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      annotation "PUBMED:27045029"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_127"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re395"
      uniprot "NA"
    ]
    graphics [
      x 752.5
      y 1776.579031299839
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_127"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      annotation "PUBMED:21349712;PUBMED:7045029"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_120"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re382"
      uniprot "NA"
    ]
    graphics [
      x 1172.5
      y 676.4696378562796
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:NR3C2;urn:miriam:uniprot:P08235;urn:miriam:uniprot:P08235;urn:miriam:hgnc.symbol:NR3C2;urn:miriam:ncbigene:4306;urn:miriam:ncbigene:4306;urn:miriam:refseq:NM_000901;urn:miriam:hgnc:7979;urn:miriam:ensembl:ENSG00000151623"
      hgnc "HGNC_SYMBOL:NR3C2"
      map_id "M121_259"
      name "NR3C2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa506"
      uniprot "UNIPROT:P08235"
    ]
    graphics [
      x 872.5
      y 1134.739628385086
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_259"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:336;urn:miriam:refseq:NM_000685;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:uniprot:P30556;urn:miriam:uniprot:P30556;urn:miriam:ensembl:ENSG00000144891;urn:miriam:ncbigene:185;urn:miriam:ncbigene:185"
      hgnc "HGNC_SYMBOL:AGTR1"
      map_id "M121_263"
      name "AGTR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa519"
      uniprot "UNIPROT:P30556"
    ]
    graphics [
      x 2672.5
      y 1517.1067434130223
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_263"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:NR3C2;urn:miriam:uniprot:P08235;urn:miriam:uniprot:P08235;urn:miriam:hgnc.symbol:NR3C2;urn:miriam:ncbigene:4306;urn:miriam:ncbigene:4306;urn:miriam:refseq:NM_000901;urn:miriam:hgnc:7979;urn:miriam:ensembl:ENSG00000151623"
      hgnc "HGNC_SYMBOL:NR3C2"
      map_id "M121_261"
      name "NR3C2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa516"
      uniprot "UNIPROT:P08235"
    ]
    graphics [
      x 1562.5
      y 1125.4971941397312
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_261"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      annotation "PUBMED:3165516"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_124"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re392"
      uniprot "NA"
    ]
    graphics [
      x 1052.5
      y 527.1551963465836
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_124"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:mesh:D013923;urn:miriam:mesh:D055806"
      hgnc "NA"
      map_id "M121_268"
      name "Thrombosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa529"
      uniprot "NA"
    ]
    graphics [
      x 1172.5
      y 783.7810490745335
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_268"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      annotation "PUBMED:26709040"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_125"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re393"
      uniprot "NA"
    ]
    graphics [
      x 2582.5
      y 2067.5083616454517
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_125"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_269"
      name "vascular_space_inflammation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa530"
      uniprot "NA"
    ]
    graphics [
      x 1082.5
      y 1456.3529417498703
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_269"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 167
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0001974"
      hgnc "NA"
      map_id "M121_262"
      name "vascular_space_remodeling"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa518"
      uniprot "NA"
    ]
    graphics [
      x 1352.5
      y 1989.311461366438
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_262"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 168
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_248"
      name "s585"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa494"
      uniprot "NA"
    ]
    graphics [
      x 738.7805189192002
      y 2450.074428910492
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_248"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 169
    zlevel -1

    cd19dm [
      annotation "PUBMED:7944388"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_28"
      name "PMID:7944388"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re163"
      uniprot "NA"
    ]
    graphics [
      x 1322.5
      y 1994.7238055463886
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 170
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:KNG1;urn:miriam:hgnc.symbol:KNG1;urn:miriam:hgnc.symbol:6383;urn:miriam:ncbigene:3827;urn:miriam:ncbigene:3827;urn:miriam:uniprot:P01042;urn:miriam:uniprot:P01042;urn:miriam:refseq:NM_001102416;urn:miriam:hgnc:6383;urn:miriam:ensembl:ENSG00000113889"
      hgnc "HGNC_SYMBOL:KNG1;HGNC_SYMBOL:6383"
      map_id "M121_133"
      name "KNG1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa169"
      uniprot "UNIPROT:P01042"
    ]
    graphics [
      x 2418.7805189192004
      y 2396.0089009888093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_133"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 171
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:3530;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000131187;urn:miriam:ncbigene:2161;urn:miriam:ncbigene:2161;urn:miriam:refseq:NM_000505;urn:miriam:hgnc.symbol:F12;urn:miriam:uniprot:P00748;urn:miriam:uniprot:P00748;urn:miriam:hgnc.symbol:F12;urn:miriam:ec-code:3.4.21.38"
      hgnc "HGNC_SYMBOL:F12"
      map_id "M121_134"
      name "F12"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa170"
      uniprot "UNIPROT:P00748"
    ]
    graphics [
      x 482.5
      y 1809.665177890063
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 172
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D010446"
      hgnc "NA"
      map_id "M121_135"
      name "Small_space_peptide"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa171"
      uniprot "NA"
    ]
    graphics [
      x 332.5
      y 1937.2402424061931
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 173
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000149257;urn:miriam:refseq:NM_004353;urn:miriam:hgnc.symbol:SERPINH1;urn:miriam:hgnc.symbol:SERPINH1;urn:miriam:hgnc:1546;urn:miriam:ncbigene:871;urn:miriam:ncbigene:871;urn:miriam:uniprot:P50454;urn:miriam:uniprot:P50454"
      hgnc "HGNC_SYMBOL:SERPINH1"
      map_id "M121_165"
      name "TAFI"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa226"
      uniprot "UNIPROT:P50454"
    ]
    graphics [
      x 2282.5
      y 1816.1202267557142
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_165"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 174
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000149257;urn:miriam:refseq:NM_004353;urn:miriam:hgnc.symbol:SERPINH1;urn:miriam:hgnc.symbol:SERPINH1;urn:miriam:hgnc:1546;urn:miriam:ncbigene:871;urn:miriam:ncbigene:871;urn:miriam:uniprot:P50454;urn:miriam:uniprot:P50454"
      hgnc "HGNC_SYMBOL:SERPINH1"
      map_id "M121_166"
      name "TAFI"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa227"
      uniprot "UNIPROT:P50454"
    ]
    graphics [
      x 738.7805189192002
      y 2390.074428910492
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_166"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 175
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_000132;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:F8;urn:miriam:hgnc.symbol:F8;urn:miriam:hgnc:3546;urn:miriam:uniprot:P00451;urn:miriam:uniprot:P00451;urn:miriam:ncbigene:2157;urn:miriam:ncbigene:2157;urn:miriam:ensembl:ENSG00000185010"
      hgnc "HGNC_SYMBOL:F8"
      map_id "M121_143"
      name "F8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa179"
      uniprot "UNIPROT:P00451"
    ]
    graphics [
      x 932.5
      y 1621.4892846436483
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_143"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 176
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ec-code:3.4.21.69;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000115718;urn:miriam:hgnc:9451;urn:miriam:refseq:NM_000312;urn:miriam:uniprot:P04070;urn:miriam:uniprot:P04070;urn:miriam:hgnc.symbol:PROC;urn:miriam:ncbigene:5624;urn:miriam:hgnc.symbol:PROC;urn:miriam:ncbigene:5624"
      hgnc "HGNC_SYMBOL:PROC"
      map_id "M121_157"
      name "PROC"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa205"
      uniprot "UNIPROT:P04070"
    ]
    graphics [
      x 1502.5
      y 1327.3397236403869
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_157"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 177
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D010446"
      hgnc "NA"
      map_id "M121_147"
      name "Small_space_peptide"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa183"
      uniprot "NA"
    ]
    graphics [
      x 1112.5
      y 689.1276992483592
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_147"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 178
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_000132;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:F8;urn:miriam:mesh:D015944;urn:miriam:hgnc:3546;urn:miriam:uniprot:P00451;urn:miriam:uniprot:P00451;urn:miriam:ncbigene:2157;urn:miriam:ncbigene:2157;urn:miriam:ensembl:ENSG00000185010"
      hgnc "HGNC_SYMBOL:F8"
      map_id "M121_144"
      name "F8a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa180"
      uniprot "UNIPROT:P00451"
    ]
    graphics [
      x 1237.432708728739
      y 451.4478390194806
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_144"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 179
    zlevel -1

    cd19dm [
      annotation "PUBMED:22471307"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_30"
      name "PMID:22471307"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re170"
      uniprot "NA"
    ]
    graphics [
      x 1747.432708728739
      y 452.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 180
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_000133;urn:miriam:taxonomy:9606;urn:miriam:hgnc:3551;urn:miriam:ensembl:ENSG00000101981;urn:miriam:ec-code:3.4.21.22;urn:miriam:uniprot:P00740;urn:miriam:uniprot:P00740;urn:miriam:hgnc.symbol:F9;urn:miriam:mesh:D015949;urn:miriam:ncbigene:2158;urn:miriam:ncbigene:2158"
      hgnc "HGNC_SYMBOL:F9"
      map_id "M121_139"
      name "F9a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa175"
      uniprot "UNIPROT:P00740"
    ]
    graphics [
      x 2017.432708728739
      y 452.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_139"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 181
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:22471307;urn:miriam:intact:EBI-11621595;urn:miriam:taxonomy:9606;urn:miriam:hgnc:3546;urn:miriam:hgnc:35531;urn:miriam:refseq:NM_000132;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:F8;urn:miriam:mesh:D015944;urn:miriam:hgnc:3546;urn:miriam:uniprot:P00451;urn:miriam:uniprot:P00451;urn:miriam:ncbigene:2157;urn:miriam:ncbigene:2157;urn:miriam:ensembl:ENSG00000185010;urn:miriam:refseq:NM_000133;urn:miriam:taxonomy:9606;urn:miriam:hgnc:3551;urn:miriam:ensembl:ENSG00000101981;urn:miriam:ec-code:3.4.21.22;urn:miriam:uniprot:P00740;urn:miriam:uniprot:P00740;urn:miriam:hgnc.symbol:F9;urn:miriam:mesh:D015949;urn:miriam:ncbigene:2158;urn:miriam:ncbigene:2158"
      hgnc "HGNC_SYMBOL:F8;HGNC_SYMBOL:F9"
      map_id "M121_2"
      name "F8:F9"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa12"
      uniprot "UNIPROT:P00451;UNIPROT:P00740"
    ]
    graphics [
      x 932.5
      y 876.0001579346998
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 182
    zlevel -1

    cd19dm [
      annotation "PUBMED:9100000"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_29"
      name "PMID:9100000"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re169"
      uniprot "NA"
    ]
    graphics [
      x 1142.5
      y 1374.9910422829782
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 183
    zlevel -1

    cd19dm [
      annotation "PUBMED:15853774;PUBMED:11551226"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_24"
      name "PMID:11551226"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re155"
      uniprot "NA"
    ]
    graphics [
      x 602.5
      y 2019.295395563012
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 184
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ec-code:3.4.21.6;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P00742;urn:miriam:uniprot:P00742;urn:miriam:refseq:NM_000504;urn:miriam:ensembl:ENSG00000126218;urn:miriam:hgnc.symbol:F10;urn:miriam:hgnc.symbol:F10;urn:miriam:hgnc:3528;urn:miriam:ncbigene:2159;urn:miriam:ncbigene:2159"
      hgnc "HGNC_SYMBOL:F10"
      map_id "M121_141"
      name "F10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa177"
      uniprot "UNIPROT:P00742"
    ]
    graphics [
      x 1361.043215136001
      y 2485.0755883950264
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_141"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 185
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ec-code:3.4.21.6;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P00742;urn:miriam:uniprot:P00742;urn:miriam:mesh:D015951;urn:miriam:refseq:NM_000504;urn:miriam:ensembl:ENSG00000126218;urn:miriam:hgnc.symbol:F10;urn:miriam:brenda:3.4.21.6;urn:miriam:hgnc:3528;urn:miriam:ncbigene:2159;urn:miriam:ncbigene:2159"
      hgnc "HGNC_SYMBOL:F10"
      map_id "M121_140"
      name "F10a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa176"
      uniprot "UNIPROT:P00742"
    ]
    graphics [
      x 1308.7805189192002
      y 2303.3146446645715
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_140"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 186
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D010446"
      hgnc "NA"
      map_id "M121_142"
      name "Small_space_peptide"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa178"
      uniprot "NA"
    ]
    graphics [
      x 1352.5
      y 818.2174785745879
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_142"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 187
    zlevel -1

    cd19dm [
      annotation "PUBMED:2303476"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_32"
      name "PMID:2303476"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re172"
      uniprot "NA"
    ]
    graphics [
      x 872.5
      y 1541.989346974373
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 188
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:F5;urn:miriam:uniprot:P12259;urn:miriam:uniprot:P12259;urn:miriam:taxonomy:9606;urn:miriam:hgnc:3542;urn:miriam:mesh:D015943;urn:miriam:refseq:NM_000130;urn:miriam:ncbigene:2153;urn:miriam:ncbigene:2153;urn:miriam:ensembl:ENSG00000198734"
      hgnc "HGNC_SYMBOL:F5"
      map_id "M121_154"
      name "F5a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa201"
      uniprot "UNIPROT:P12259"
    ]
    graphics [
      x 812.5
      y 870.0360504703245
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_154"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 189
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:mesh:D015951;urn:miriam:pubmed:2303476;urn:miriam:mesh:D15943;urn:miriam:mesh:C022475;urn:miriam:ec-code:3.4.21.6;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P00742;urn:miriam:uniprot:P00742;urn:miriam:mesh:D015951;urn:miriam:refseq:NM_000504;urn:miriam:ensembl:ENSG00000126218;urn:miriam:hgnc.symbol:F10;urn:miriam:brenda:3.4.21.6;urn:miriam:hgnc:3528;urn:miriam:ncbigene:2159;urn:miriam:ncbigene:2159;urn:miriam:hgnc.symbol:F5;urn:miriam:uniprot:P12259;urn:miriam:uniprot:P12259;urn:miriam:taxonomy:9606;urn:miriam:hgnc:3542;urn:miriam:mesh:D015943;urn:miriam:refseq:NM_000130;urn:miriam:ncbigene:2153;urn:miriam:ncbigene:2153;urn:miriam:ensembl:ENSG00000198734"
      hgnc "HGNC_SYMBOL:F10;HGNC_SYMBOL:F5"
      map_id "M121_3"
      name "F5a:F10a"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa13"
      uniprot "UNIPROT:P00742;UNIPROT:P12259"
    ]
    graphics [
      x 2222.5
      y 1196.7158381002334
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 190
    zlevel -1

    cd19dm [
      annotation "PUBMED:4430674;PUBMED:3818642"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_33"
      name "PMID:4430674,PMID:3818642"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re173"
      uniprot "NA"
    ]
    graphics [
      x 2058.7805189192004
      y 2165.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 191
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P00734;urn:miriam:uniprot:P00734;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.21.5;urn:miriam:hgnc:3535;urn:miriam:refseq:NM_000506;urn:miriam:ensembl:ENSG00000180210;urn:miriam:hgnc.symbol:F2;urn:miriam:hgnc.symbol:F2;urn:miriam:ncbigene:2147;urn:miriam:ncbigene:2147"
      hgnc "HGNC_SYMBOL:F2"
      map_id "M121_146"
      name "Prothrombin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa182"
      uniprot "UNIPROT:P00734"
    ]
    graphics [
      x 1652.5
      y 1977.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_146"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 192
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D013917;urn:miriam:uniprot:P00734;urn:miriam:uniprot:P00734;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.21.5;urn:miriam:hgnc:3535;urn:miriam:refseq:NM_000506;urn:miriam:ensembl:ENSG00000180210;urn:miriam:hgnc.symbol:F2;urn:miriam:ncbigene:2147;urn:miriam:ncbigene:2147"
      hgnc "HGNC_SYMBOL:F2"
      map_id "M121_156"
      name "Thrombin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa203"
      uniprot "UNIPROT:P00734"
    ]
    graphics [
      x 2852.5
      y 1663.9317909822832
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_156"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 193
    zlevel -1

    cd19dm [
      annotation "PUBMED:2322551;PUBMED:6572921;PUBMED:6282863"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_31"
      name "PMID:2322551, PMID:6282863, PMID:6572921"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re171"
      uniprot "NA"
    ]
    graphics [
      x 932.5
      y 1930.7631724663504
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 194
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:3541;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:F3;urn:miriam:refseq:NM_001993;urn:miriam:hgnc.symbol:F3;urn:miriam:ncbigene:2152;urn:miriam:ncbigene:2152;urn:miriam:ensembl:ENSG00000117525;urn:miriam:uniprot:P13726;urn:miriam:uniprot:P13726"
      hgnc "HGNC_SYMBOL:F3"
      map_id "M121_153"
      name "F5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa200"
      uniprot "UNIPROT:P13726"
    ]
    graphics [
      x 752.5
      y 1716.579031299839
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_153"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 195
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D013917;urn:miriam:hgnc:11784;urn:miriam:taxonomy:9986;urn:miriam:pubmed:6282863;urn:miriam:taxonomy:9606;urn:miriam:ncbigene:7056;urn:miriam:ncbigene:7056;urn:miriam:refseq:NM_000361;urn:miriam:uniprot:P07204;urn:miriam:uniprot:P07204;urn:miriam:hgnc:11784;urn:miriam:ensembl:ENSG00000178726;urn:miriam:hgnc.symbol:THBD;urn:miriam:hgnc.symbol:THBD;urn:miriam:mesh:D013917;urn:miriam:uniprot:P00734;urn:miriam:uniprot:P00734;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.21.5;urn:miriam:hgnc:3535;urn:miriam:refseq:NM_000506;urn:miriam:ensembl:ENSG00000180210;urn:miriam:hgnc.symbol:F2;urn:miriam:ncbigene:2147;urn:miriam:ncbigene:2147"
      hgnc "HGNC_SYMBOL:THBD;HGNC_SYMBOL:F2"
      map_id "M121_7"
      name "Thrombin:Thrombomodulin"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa25"
      uniprot "UNIPROT:P07204;UNIPROT:P00734"
    ]
    graphics [
      x 1202.5
      y 2127.3156502059937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 196
    zlevel -1

    cd19dm [
      annotation "PUBMED:28228446;PUBMED:6282863;PUBMED:2117226"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_46"
      name "PMID:28228446, PMID: 6282863, PMID:2117226 "
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re234"
      uniprot "NA"
    ]
    graphics [
      x 632.5
      y 1699.501819242279
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 197
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:3662;urn:miriam:taxonomy:9606;urn:miriam:hgnc:3661;urn:miriam:hgnc:3694;urn:miriam:pubmed:19296670;urn:miriam:obo.go:GO%3A0005577;urn:miriam:hgnc:3661;urn:miriam:hgnc.symbol:FGA;urn:miriam:refseq:NM_000508;urn:miriam:hgnc.symbol:FGA;urn:miriam:ncbigene:2243;urn:miriam:uniprot:P02671;urn:miriam:uniprot:P02671;urn:miriam:ncbigene:2243;urn:miriam:ensembl:ENSG00000171560;urn:miriam:uniprot:P02679;urn:miriam:uniprot:P02679;urn:miriam:hgnc:3694;urn:miriam:ensembl:ENSG00000171557;urn:miriam:refseq:NM_021870;urn:miriam:hgnc.symbol:FGG;urn:miriam:hgnc.symbol:FGG;urn:miriam:ncbigene:2266;urn:miriam:ncbigene:2266;urn:miriam:hgnc:3662;urn:miriam:hgnc.symbol:FGB;urn:miriam:uniprot:P02675;urn:miriam:uniprot:P02675;urn:miriam:hgnc.symbol:FGB;urn:miriam:ensembl:ENSG00000171564;urn:miriam:refseq:NM_005141;urn:miriam:ncbigene:2244;urn:miriam:ncbigene:2244"
      hgnc "HGNC_SYMBOL:FGA;HGNC_SYMBOL:FGG;HGNC_SYMBOL:FGB"
      map_id "M121_6"
      name "Fibrinogen"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa17"
      uniprot "UNIPROT:P02671;UNIPROT:P02679;UNIPROT:P02675"
    ]
    graphics [
      x 122.5
      y 1686.0884129110075
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 198
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D013917;urn:miriam:uniprot:P00734;urn:miriam:uniprot:P00734;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.21.5;urn:miriam:hgnc:3535;urn:miriam:refseq:NM_000506;urn:miriam:ensembl:ENSG00000180210;urn:miriam:hgnc.symbol:F2;urn:miriam:ncbigene:2147;urn:miriam:ncbigene:2147"
      hgnc "HGNC_SYMBOL:F2"
      map_id "M121_252"
      name "Thrombin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa498"
      uniprot "UNIPROT:P00734"
    ]
    graphics [
      x 1262.5
      y 1476.9780694277015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_252"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 199
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:2697049;urn:miriam:mesh:D012327"
      hgnc "NA"
      map_id "M121_253"
      name "SARS_minus_CoV_minus_2_space_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa499"
      uniprot "NA"
    ]
    graphics [
      x 1082.5
      y 796.3529417498704
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_253"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 200
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:C011468;urn:miriam:taxonomy:9606"
      hgnc "NA"
      map_id "M121_204"
      name "Fibrin_space_monomer"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa395"
      uniprot "NA"
    ]
    graphics [
      x 1511.043215136001
      y 2519.738142841986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_204"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 201
    zlevel -1

    cd19dm [
      annotation "PUBMED:29096812;PUBMED:7577232"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_96"
      name "PMID:29096812, PMID:7577232"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re345"
      uniprot "NA"
    ]
    graphics [
      x 2081.043215136001
      y 2495.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 202
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D026122;urn:miriam:taxonomy:9606;urn:miriam:brenda:2.3.2.13;urn:miriam:hgnc.symbol:F13"
      hgnc "HGNC_SYMBOL:F13"
      map_id "M121_220"
      name "F13a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa425"
      uniprot "NA"
    ]
    graphics [
      x 3078.7805189192004
      y 2320.1860833815003
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_220"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 203
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:mesh:C465961"
      hgnc "NA"
      map_id "M121_219"
      name "Fibrin_space_polymer"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa424"
      uniprot "NA"
    ]
    graphics [
      x 1121.043215136001
      y 2625.2198692568554
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_219"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 204
    zlevel -1

    cd19dm [
      annotation "PUBMED:22449964"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_39"
      name "PMID:22449964"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re182"
      uniprot "NA"
    ]
    graphics [
      x 2050.913546386292
      y 932.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 205
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_81"
      name "DOI:10.1101/2020.04.25.20077842"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re328"
      uniprot "NA"
    ]
    graphics [
      x 2582.5
      y 1669.6649132197108
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 206
    zlevel -1

    cd19dm [
      annotation "PUBMED:32172226"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_41"
      name "PMID:32172226"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re192"
      uniprot "NA"
    ]
    graphics [
      x 1262.5
      y 846.9780694277015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 207
    zlevel -1

    cd19dm [
      annotation "PUBMED:32302438"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_66"
      name "PMID:32302438"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re296"
      uniprot "NA"
    ]
    graphics [
      x 828.7805189192002
      y 2503.4007956574083
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 208
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32302438;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_241"
      name "s553"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa479"
      uniprot "NA"
    ]
    graphics [
      x 648.7805189192002
      y 2553.5844628674904
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_241"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 209
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_251"
      name "s589"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa497"
      uniprot "NA"
    ]
    graphics [
      x 512.5
      y 1544.3290598093936
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_251"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 210
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D062106;urn:miriam:doi:10.1101/2020.04.25.200"
      hgnc "NA"
      map_id "M121_240"
      name "s552"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa478"
      uniprot "NA"
    ]
    graphics [
      x 2402.5
      y 1186.0046064486714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_240"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 211
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:ncbigene:7056;urn:miriam:ncbigene:7056;urn:miriam:refseq:NM_000361;urn:miriam:uniprot:P07204;urn:miriam:uniprot:P07204;urn:miriam:hgnc:11784;urn:miriam:ensembl:ENSG00000178726;urn:miriam:hgnc.symbol:THBD;urn:miriam:hgnc.symbol:THBD"
      hgnc "HGNC_SYMBOL:THBD"
      map_id "M121_176"
      name "Thrombomodulin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa301"
      uniprot "UNIPROT:P07204"
    ]
    graphics [
      x 2372.5
      y 1462.0608348716244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_176"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 212
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:uniprot:P05121;urn:miriam:uniprot:P05121;urn:miriam:ncbigene:5054;urn:miriam:ncbigene:5054;urn:miriam:ensembl:ENSG00000106366;urn:miriam:hgnc:8593;urn:miriam:refseq:NM_000602;urn:miriam:hgnc.symbol:SERPINE1;urn:miriam:hgnc.symbol:SERPINE1;urn:miriam:hgnc:8583"
      hgnc "HGNC_SYMBOL:SERPINE1"
      map_id "M121_163"
      name "SERPINE1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa224"
      uniprot "UNIPROT:P05121"
    ]
    graphics [
      x 1938.7805189192002
      y 2162.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_163"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 213
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ec-code:3.4.21.68;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000104368;urn:miriam:hgnc:9051;urn:miriam:hgnc.symbol:PLAT;urn:miriam:hgnc.symbol:PLAT;urn:miriam:uniprot:P00750;urn:miriam:uniprot:P00750;urn:miriam:ncbigene:5327;urn:miriam:ncbigene:5327;urn:miriam:refseq:NM_000930"
      hgnc "HGNC_SYMBOL:PLAT"
      map_id "M121_164"
      name "PLAT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa225"
      uniprot "UNIPROT:P00750"
    ]
    graphics [
      x 1802.5
      y 2042.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_164"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 214
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc:9051;urn:miriam:pubmed:22449964;urn:miriam:hgnc:8593;urn:miriam:intact:EBI-7800882;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P05121;urn:miriam:uniprot:P05121;urn:miriam:ncbigene:5054;urn:miriam:ncbigene:5054;urn:miriam:ensembl:ENSG00000106366;urn:miriam:hgnc:8593;urn:miriam:refseq:NM_000602;urn:miriam:hgnc.symbol:SERPINE1;urn:miriam:hgnc.symbol:SERPINE1;urn:miriam:hgnc:8583;urn:miriam:ec-code:3.4.21.68;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000104368;urn:miriam:hgnc:9051;urn:miriam:hgnc.symbol:PLAT;urn:miriam:hgnc.symbol:PLAT;urn:miriam:uniprot:P00750;urn:miriam:uniprot:P00750;urn:miriam:ncbigene:5327;urn:miriam:ncbigene:5327;urn:miriam:refseq:NM_000930"
      hgnc "HGNC_SYMBOL:SERPINE1;HGNC_SYMBOL:PLAT"
      map_id "M121_5"
      name "PLAT:SERPINE1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa16"
      uniprot "UNIPROT:P05121;UNIPROT:P00750"
    ]
    graphics [
      x 2162.5
      y 1373.7133098641311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 215
    zlevel -1

    cd19dm [
      annotation "PUBMED:21199867"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_56"
      name "PMID:21199867"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re268"
      uniprot "NA"
    ]
    graphics [
      x 1571.043215136001
      y 2669.738142841986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 216
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:ncbigene:5328;urn:miriam:ncbigene:5328;urn:miriam:hgnc.symbol:PLAU;urn:miriam:hgnc.symbol:PLAU;urn:miriam:ec-code:3.4.21.73;urn:miriam:ensembl:ENSG00000122861;urn:miriam:hgnc:9052;urn:miriam:uniprot:P00749;urn:miriam:uniprot:P00749;urn:miriam:refseq:NM_002658"
      hgnc "HGNC_SYMBOL:PLAU"
      map_id "M121_187"
      name "PLAU"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa356"
      uniprot "UNIPROT:P00749"
    ]
    graphics [
      x 2162.5
      y 1223.7133098641311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_187"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 217
    zlevel -1

    cd19dm [
      annotation "PUBMED:29472360"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_107"
      name "PMID:29472360"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re363"
      uniprot "NA"
    ]
    graphics [
      x 948.7805189192002
      y 2252.5182557004428
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 218
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:GP6;urn:miriam:hgnc.symbol:GP6;urn:miriam:hgnc:14388;urn:miriam:ensembl:ENSG00000088053;urn:miriam:ncbigene:51206;urn:miriam:ncbigene:51206;urn:miriam:refseq:NM_001083899;urn:miriam:uniprot:Q9HCN6;urn:miriam:uniprot:Q9HCN6"
      hgnc "HGNC_SYMBOL:GP6"
      map_id "M121_222"
      name "GP6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa431"
      uniprot "UNIPROT:Q9HCN6"
    ]
    graphics [
      x 452.5
      y 2049.56913395263
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_222"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 219
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc:14388;urn:miriam:pubmed:19296670;urn:miriam:obo.go:GO%3A0005577;urn:miriam:hgnc:3661;urn:miriam:hgnc.symbol:FGA;urn:miriam:refseq:NM_000508;urn:miriam:hgnc.symbol:FGA;urn:miriam:ncbigene:2243;urn:miriam:uniprot:P02671;urn:miriam:uniprot:P02671;urn:miriam:ncbigene:2243;urn:miriam:ensembl:ENSG00000171560;urn:miriam:hgnc:3662;urn:miriam:hgnc.symbol:FGB;urn:miriam:uniprot:P02675;urn:miriam:uniprot:P02675;urn:miriam:hgnc.symbol:FGB;urn:miriam:ensembl:ENSG00000171564;urn:miriam:refseq:NM_005141;urn:miriam:ncbigene:2244;urn:miriam:ncbigene:2244;urn:miriam:uniprot:P02679;urn:miriam:uniprot:P02679;urn:miriam:hgnc:3694;urn:miriam:ensembl:ENSG00000171557;urn:miriam:refseq:NM_021870;urn:miriam:hgnc.symbol:FGG;urn:miriam:hgnc.symbol:FGG;urn:miriam:ncbigene:2266;urn:miriam:ncbigene:2266;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:GP6;urn:miriam:hgnc.symbol:GP6;urn:miriam:hgnc:14388;urn:miriam:ensembl:ENSG00000088053;urn:miriam:ncbigene:51206;urn:miriam:ncbigene:51206;urn:miriam:refseq:NM_001083899;urn:miriam:uniprot:Q9HCN6;urn:miriam:uniprot:Q9HCN6"
      hgnc "HGNC_SYMBOL:FGA;HGNC_SYMBOL:FGB;HGNC_SYMBOL:FGG;HGNC_SYMBOL:GP6"
      map_id "M121_21"
      name "Fibrinogen:GP6"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa42"
      uniprot "UNIPROT:P02671;UNIPROT:P02675;UNIPROT:P02679;UNIPROT:Q9HCN6"
    ]
    graphics [
      x 828.7805189192002
      y 2441.8664604847704
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 220
    zlevel -1

    cd19dm [
      annotation "PUBMED:25051961"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_102"
      name "PMID:25051961"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re357"
      uniprot "NA"
    ]
    graphics [
      x 1082.5
      y 1186.3529417498703
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 221
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:6137;urn:miriam:taxonomy:9606;urn:miriam:intact:EBI-16428357;urn:miriam:hgnc:6153;urn:miriam:hgnc:6137;urn:miriam:refseq:NM_002203;urn:miriam:ncbigene:3673;urn:miriam:ensembl:ENSG00000164171;urn:miriam:uniprot:P17301;urn:miriam:uniprot:P17301;urn:miriam:ncbigene:3673;urn:miriam:hgnc.symbol:ITGA2;urn:miriam:hgnc.symbol:ITGA2;urn:miriam:refseq:NM_002211;urn:miriam:uniprot:P05556;urn:miriam:uniprot:P05556;urn:miriam:hgnc.symbol:ITGB1;urn:miriam:hgnc.symbol:ITGB1;urn:miriam:ncbigene:3688;urn:miriam:ncbigene:3688;urn:miriam:hgnc:6153;urn:miriam:ensembl:ENSG00000150093"
      hgnc "HGNC_SYMBOL:ITGA2;HGNC_SYMBOL:ITGB1"
      map_id "M121_20"
      name "ITGA2:ITGAB1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa41"
      uniprot "UNIPROT:P17301;UNIPROT:P05556"
    ]
    graphics [
      x 1322.5
      y 1852.3432595389866
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 222
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:RPS3AP29;urn:miriam:refseq:NG_011230;urn:miriam:ensembl:ENSG00000237818;urn:miriam:hgnc:35531;urn:miriam:ncbigene:730861"
      hgnc "HGNC_SYMBOL:RPS3AP29"
      map_id "M121_138"
      name "F9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa174"
      uniprot "NA"
    ]
    graphics [
      x 962.5
      y 1627.6441692926126
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_138"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 223
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A28304;urn:miriam:taxonomy:9606;urn:miriam:pubmed:708377"
      hgnc "NA"
      map_id "M121_186"
      name "Heparin"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa355"
      uniprot "NA"
    ]
    graphics [
      x 932.5
      y 1296.2469660824179
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_186"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 224
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:mesh:C036309;urn:miriam:pubmed:19008457"
      hgnc "NA"
      map_id "M121_159"
      name "D_minus_dimer"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa210"
      uniprot "NA"
    ]
    graphics [
      x 362.5
      y 1440.7309670383409
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_159"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 225
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc:1352;urn:miriam:hgnc:1353;urn:miriam:pubmed:28630159;urn:miriam:hgnc:1354;urn:miriam:mesh:C042295;urn:miriam:mesh:D050776;urn:miriam:hgnc:1339;urn:miriam:refseq:NM_000066;urn:miriam:hgnc.symbol:C8B;urn:miriam:hgnc.symbol:C8B;urn:miriam:hgnc:1353;urn:miriam:ncbigene:732;urn:miriam:ensembl:ENSG00000021852;urn:miriam:ncbigene:732;urn:miriam:uniprot:P07358;urn:miriam:uniprot:P07358;urn:miriam:ensembl:ENSG00000176919;urn:miriam:refseq:NM_000606;urn:miriam:hgnc:1354;urn:miriam:hgnc.symbol:C8G;urn:miriam:ncbigene:733;urn:miriam:hgnc.symbol:C8G;urn:miriam:ncbigene:733;urn:miriam:uniprot:P07360;urn:miriam:uniprot:P07360;urn:miriam:hgnc.symbol:C8A;urn:miriam:refseq:NM_000562;urn:miriam:hgnc.symbol:C8A;urn:miriam:hgnc:1352;urn:miriam:uniprot:P07357;urn:miriam:uniprot:P07357;urn:miriam:ncbigene:731;urn:miriam:ncbigene:731;urn:miriam:ensembl:ENSG00000157131;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:C6;urn:miriam:hgnc.symbol:C6;urn:miriam:refseq:NM_000065;urn:miriam:ensembl:ENSG00000039537;urn:miriam:uniprot:P13671;urn:miriam:uniprot:P13671;urn:miriam:hgnc:1339;urn:miriam:ncbigene:729;urn:miriam:ncbigene:729;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1331;urn:miriam:hgnc.symbol:C5;urn:miriam:mesh:D050776;urn:miriam:refseq:NM_001735;urn:miriam:ensembl:ENSG00000106804;urn:miriam:uniprot:P01031;urn:miriam:uniprot:P01031;urn:miriam:ncbigene:727;urn:miriam:ncbigene:727;urn:miriam:uniprot:P10643;urn:miriam:uniprot:P10643;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000112936;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc:1346;urn:miriam:refseq:NM_000587"
      hgnc "HGNC_SYMBOL:C8B;HGNC_SYMBOL:C8G;HGNC_SYMBOL:C8A;HGNC_SYMBOL:C6;HGNC_SYMBOL:C5;HGNC_SYMBOL:C7"
      map_id "M121_13"
      name "C5b:C6:C7:C8A:C8B:C8G"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa31"
      uniprot "UNIPROT:P07358;UNIPROT:P07360;UNIPROT:P07357;UNIPROT:P13671;UNIPROT:P01031;UNIPROT:P10643"
    ]
    graphics [
      x 1982.5
      y 1892.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 226
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ncbigene:735;urn:miriam:ncbigene:735;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1358;urn:miriam:ensembl:ENSG00000113600;urn:miriam:hgnc.symbol:C9;urn:miriam:hgnc.symbol:C9;urn:miriam:refseq:NM_001737;urn:miriam:uniprot:P02748;urn:miriam:uniprot:P02748"
      hgnc "HGNC_SYMBOL:C9"
      map_id "M121_185"
      name "C9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa337"
      uniprot "UNIPROT:P02748"
    ]
    graphics [
      x 2132.5
      y 1775.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_185"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 227
    zlevel -1

    cd19dm [
      annotation "PUBMED:284414"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_54"
      name "PMID:284414"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re266"
      uniprot "NA"
    ]
    graphics [
      x 1052.5
      y 1748.5794444430944
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 228
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:28630159;urn:miriam:mesh:C037453;urn:miriam:hgnc:1346;urn:miriam:mesh:D050776;urn:miriam:hgnc:1339;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1331;urn:miriam:hgnc.symbol:C5;urn:miriam:mesh:D050776;urn:miriam:refseq:NM_001735;urn:miriam:ensembl:ENSG00000106804;urn:miriam:uniprot:P01031;urn:miriam:uniprot:P01031;urn:miriam:ncbigene:727;urn:miriam:ncbigene:727;urn:miriam:uniprot:P10643;urn:miriam:uniprot:P10643;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000112936;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc:1346;urn:miriam:refseq:NM_000587;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:C6;urn:miriam:hgnc.symbol:C6;urn:miriam:refseq:NM_000065;urn:miriam:ensembl:ENSG00000039537;urn:miriam:uniprot:P13671;urn:miriam:uniprot:P13671;urn:miriam:hgnc:1339;urn:miriam:ncbigene:729;urn:miriam:ncbigene:729"
      hgnc "HGNC_SYMBOL:C5;HGNC_SYMBOL:C7;HGNC_SYMBOL:C6"
      map_id "M121_11"
      name "C5b:C6:C7"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa29"
      uniprot "UNIPROT:P01031;UNIPROT:P10643;UNIPROT:P13671"
    ]
    graphics [
      x 1068.7805189192002
      y 2318.5794444430944
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 229
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc:1352;urn:miriam:hgnc:1353;urn:miriam:hgnc:1354;urn:miriam:mesh:D003185;urn:miriam:ensembl:ENSG00000176919;urn:miriam:refseq:NM_000606;urn:miriam:hgnc:1354;urn:miriam:hgnc.symbol:C8G;urn:miriam:ncbigene:733;urn:miriam:hgnc.symbol:C8G;urn:miriam:ncbigene:733;urn:miriam:uniprot:P07360;urn:miriam:uniprot:P07360;urn:miriam:hgnc.symbol:C8A;urn:miriam:refseq:NM_000562;urn:miriam:hgnc.symbol:C8A;urn:miriam:hgnc:1352;urn:miriam:uniprot:P07357;urn:miriam:uniprot:P07357;urn:miriam:ncbigene:731;urn:miriam:ncbigene:731;urn:miriam:ensembl:ENSG00000157131;urn:miriam:refseq:NM_000066;urn:miriam:hgnc.symbol:C8B;urn:miriam:hgnc.symbol:C8B;urn:miriam:hgnc:1353;urn:miriam:ncbigene:732;urn:miriam:ensembl:ENSG00000021852;urn:miriam:ncbigene:732;urn:miriam:uniprot:P07358;urn:miriam:uniprot:P07358"
      hgnc "HGNC_SYMBOL:C8G;HGNC_SYMBOL:C8A;HGNC_SYMBOL:C8B"
      map_id "M121_12"
      name "C8A:C8B:C8G"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa30"
      uniprot "UNIPROT:P07360;UNIPROT:P07357;UNIPROT:P07358"
    ]
    graphics [
      x 1322.5
      y 1882.3432595389866
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 230
    zlevel -1

    cd19dm [
      annotation "PUBMED:5058233"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_53"
      name "PMID:5058233"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re264"
      uniprot "NA"
    ]
    graphics [
      x 572.5
      y 1707.842676116287
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 231
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:mesh:D050776;urn:miriam:mesh:C050974;urn:miriam:hgnc:1339;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:C6;urn:miriam:hgnc.symbol:C6;urn:miriam:refseq:NM_000065;urn:miriam:ensembl:ENSG00000039537;urn:miriam:uniprot:P13671;urn:miriam:uniprot:P13671;urn:miriam:hgnc:1339;urn:miriam:ncbigene:729;urn:miriam:ncbigene:729;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1331;urn:miriam:hgnc.symbol:C5;urn:miriam:mesh:D050776;urn:miriam:refseq:NM_001735;urn:miriam:ensembl:ENSG00000106804;urn:miriam:uniprot:P01031;urn:miriam:uniprot:P01031;urn:miriam:ncbigene:727;urn:miriam:ncbigene:727"
      hgnc "HGNC_SYMBOL:C6;HGNC_SYMBOL:C5"
      map_id "M121_10"
      name "C5b:C6"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa28"
      uniprot "UNIPROT:P13671;UNIPROT:P01031"
    ]
    graphics [
      x 2492.5
      y 1981.435024234829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 232
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P10643;urn:miriam:uniprot:P10643;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000112936;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc:1346;urn:miriam:refseq:NM_000587"
      hgnc "HGNC_SYMBOL:C7"
      map_id "M121_184"
      name "C7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa321"
      uniprot "UNIPROT:P10643"
    ]
    graphics [
      x 978.7805189192002
      y 2370.6216893968285
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_184"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 233
    zlevel -1

    cd19dm [
      annotation "PUBMED:5058233"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_52"
      name "PMID:5058233"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re263"
      uniprot "NA"
    ]
    graphics [
      x 3302.5
      y 1682.1006432610989
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 234
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:C6;urn:miriam:hgnc.symbol:C6;urn:miriam:refseq:NM_000065;urn:miriam:ensembl:ENSG00000039537;urn:miriam:uniprot:P13671;urn:miriam:uniprot:P13671;urn:miriam:hgnc:1339;urn:miriam:ncbigene:729;urn:miriam:ncbigene:729"
      hgnc "HGNC_SYMBOL:C6"
      map_id "M121_183"
      name "C6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa318"
      uniprot "UNIPROT:P13671"
    ]
    graphics [
      x 2762.5
      y 1441.2535183076814
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_183"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 235
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:taxonomy:9606;urn:miriam:hgnc:13557;urn:miriam:taxonomy:2697049;urn:miriam:pdb:6CS2;urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:taxonomy:2697049;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S;urn:miriam:ensembl:ENSG00000130234;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:S;HGNC_SYMBOL:ACE2"
      map_id "M121_16"
      name "ACE2:Spike"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa37"
      uniprot "UNIPROT:P0DTC2;UNIPROT:P59594;UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1022.5
      y 937.804542056548
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 236
    zlevel -1

    cd19dm [
      annotation "PUBMED:32142651"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_114"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re373"
      uniprot "NA"
    ]
    graphics [
      x 2432.5
      y 776.0089009888093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 237
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:2697049;urn:miriam:mesh:D012327"
      hgnc "NA"
      map_id "M121_247"
      name "SARS_minus_CoV_minus_2_space_viral_space_entry"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa485"
      uniprot "NA"
    ]
    graphics [
      x 1622.5
      y 802.5365071385852
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_247"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 238
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:C2;urn:miriam:uniprot:P06681;urn:miriam:uniprot:P06681;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000166278;urn:miriam:ec-code:3.4.21.43;urn:miriam:refseq:NM_000063;urn:miriam:mesh:D050678;urn:miriam:hgnc:1248;urn:miriam:ncbigene:717;urn:miriam:ncbigene:717"
      hgnc "HGNC_SYMBOL:C2"
      map_id "M121_195"
      name "C2a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa367"
      uniprot "UNIPROT:P06681"
    ]
    graphics [
      x 692.5
      y 1179.27643439248
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_195"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 239
    zlevel -1

    cd19dm [
      annotation "PUBMED:10946292"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_89"
      name "PMID:10946292"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re338"
      uniprot "NA"
    ]
    graphics [
      x 512.5
      y 2014.8917823095192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 240
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:C2;urn:miriam:uniprot:P06681;urn:miriam:uniprot:P06681;urn:miriam:hgnc.symbol:C2;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000166278;urn:miriam:ec-code:3.4.21.43;urn:miriam:refseq:NM_000063;urn:miriam:hgnc:1248;urn:miriam:ncbigene:717;urn:miriam:ncbigene:717"
      hgnc "HGNC_SYMBOL:C2"
      map_id "M121_194"
      name "C2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa366"
      uniprot "UNIPROT:P06681"
    ]
    graphics [
      x 1278.7805189192002
      y 2466.9780694277015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_194"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 241
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000127241;urn:miriam:ncbigene:5648;urn:miriam:ncbigene:5648;urn:miriam:hgnc.symbol:MASP1;urn:miriam:refseq:NM_001879;urn:miriam:hgnc.symbol:MASP1;urn:miriam:hgnc:6901;urn:miriam:ec-code:3.4.21.-;urn:miriam:uniprot:P48740;urn:miriam:uniprot:P48740"
      hgnc "HGNC_SYMBOL:MASP1"
      map_id "M121_189"
      name "MASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa358"
      uniprot "UNIPROT:P48740"
    ]
    graphics [
      x 1398.7805189192002
      y 2302.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_189"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 242
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:C2;urn:miriam:uniprot:P06681;urn:miriam:uniprot:P06681;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000166278;urn:miriam:ec-code:3.4.21.43;urn:miriam:refseq:NM_000063;urn:miriam:mesh:D050679;urn:miriam:hgnc:1248;urn:miriam:ncbigene:717;urn:miriam:ncbigene:717"
      hgnc "HGNC_SYMBOL:C2"
      map_id "M121_196"
      name "C2b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa368"
      uniprot "UNIPROT:P06681"
    ]
    graphics [
      x 662.5
      y 1685.901254732075
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_196"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 243
    zlevel -1

    cd19dm [
      annotation "PUBMED:11290788"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_58"
      name "PMID:11290788"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re275"
      uniprot "NA"
    ]
    graphics [
      x 2162.5
      y 1133.7133098641311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 244
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000127241;urn:miriam:ncbigene:5648;urn:miriam:ncbigene:5648;urn:miriam:hgnc.symbol:MASP1;urn:miriam:refseq:NM_001879;urn:miriam:hgnc.symbol:MASP1;urn:miriam:hgnc:6901;urn:miriam:ec-code:3.4.21.-;urn:miriam:uniprot:P48740;urn:miriam:uniprot:P48740"
      hgnc "HGNC_SYMBOL:MASP1"
      map_id "M121_232"
      name "MASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa458"
      uniprot "UNIPROT:P48740"
    ]
    graphics [
      x 1232.5
      y 1452.9973497539163
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_232"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 245
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:O00187;urn:miriam:uniprot:O00187;urn:miriam:taxonomy:9606;urn:miriam:refseq:NM_006610;urn:miriam:ensembl:ENSG00000009724;urn:miriam:ec-code:3.4.21.104;urn:miriam:hgnc:6902;urn:miriam:hgnc.symbol:MASP2;urn:miriam:ncbigene:10747;urn:miriam:hgnc.symbol:MASP2;urn:miriam:ncbigene:10747"
      hgnc "HGNC_SYMBOL:MASP2"
      map_id "M121_190"
      name "MBL2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa362"
      uniprot "UNIPROT:O00187"
    ]
    graphics [
      x 2792.5
      y 1495.0576065390942
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_190"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 246
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32299776;urn:miriam:taxonomy:9606"
      hgnc "NA"
      map_id "M121_218"
      name "MASP2_space_deposition"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa417"
      uniprot "NA"
    ]
    graphics [
      x 1802.5
      y 752.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_218"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 247
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32299776;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_234"
      name "s537"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa460"
      uniprot "NA"
    ]
    graphics [
      x 2162.5
      y 1103.7133098641311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_234"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 248
    zlevel -1

    cd19dm [
      annotation "PUBMED:28116710"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_69"
      name "PMID:20689271"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re306"
      uniprot "NA"
    ]
    graphics [
      x 1357.432708728739
      y 605.6338011188755
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 249
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:20689271;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_207"
      name "s86"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa399"
      uniprot "NA"
    ]
    graphics [
      x 1142.5
      y 924.9910422829782
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_207"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 250
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:20689271;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_257"
      name "s86"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa504"
      uniprot "NA"
    ]
    graphics [
      x 2372.5
      y 1582.0608348716244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_257"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 251
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A3892;urn:miriam:hgnc:9201"
      hgnc "NA"
      map_id "M121_267"
      name "ACTH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa527"
      uniprot "NA"
    ]
    graphics [
      x 2912.5
      y 1119.67376082007
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_267"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 252
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:336;urn:miriam:refseq:NM_000685;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:uniprot:P30556;urn:miriam:uniprot:P30556;urn:miriam:ensembl:ENSG00000144891;urn:miriam:ncbigene:185;urn:miriam:ncbigene:185"
      hgnc "HGNC_SYMBOL:AGTR1"
      map_id "M121_264"
      name "AGTR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa520"
      uniprot "UNIPROT:P30556"
    ]
    graphics [
      x 1892.5
      y 1712.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_264"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 253
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:uniprot:P05121;urn:miriam:uniprot:P05121;urn:miriam:ncbigene:5054;urn:miriam:ncbigene:5054;urn:miriam:ensembl:ENSG00000106366;urn:miriam:hgnc:8593;urn:miriam:refseq:NM_000602;urn:miriam:hgnc.symbol:SERPINE1;urn:miriam:hgnc.symbol:SERPINE1;urn:miriam:hgnc:8583"
      hgnc "HGNC_SYMBOL:SERPINE1"
      map_id "M121_258"
      name "SERPINE1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa505"
      uniprot "UNIPROT:P05121"
    ]
    graphics [
      x 1592.5
      y 953.1199123051069
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_258"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 254
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ncbigene:3569;urn:miriam:ncbigene:3569;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P05231;urn:miriam:uniprot:P05231;urn:miriam:ensembl:ENSG00000136244;urn:miriam:hgnc:6018;urn:miriam:hgnc.symbol:IL6;urn:miriam:hgnc.symbol:IL6;urn:miriam:refseq:NM_000600"
      hgnc "HGNC_SYMBOL:IL6"
      map_id "M121_169"
      name "IL6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa242"
      uniprot "UNIPROT:P05231"
    ]
    graphics [
      x 962.5
      y 876.2731710667011
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_169"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 255
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:20689271;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_255"
      name "s86"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa501"
      uniprot "NA"
    ]
    graphics [
      x 2358.7805189192004
      y 2422.060834871624
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_255"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 256
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:19286885;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_223"
      name "s521"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa440"
      uniprot "NA"
    ]
    graphics [
      x 902.5
      y 1004.7914102512494
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_223"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 257
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:12726;urn:miriam:taxonomy:9606;urn:miriam:refseq:NM_000552;urn:miriam:uniprot:P04275;urn:miriam:uniprot:P04275;urn:miriam:ncbigene:7450;urn:miriam:ncbigene:7450;urn:miriam:hgnc.symbol:VWF;urn:miriam:hgnc.symbol:VWF;urn:miriam:ensembl:ENSG00000110799"
      hgnc "HGNC_SYMBOL:VWF"
      map_id "M121_239"
      name "VWF"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa472"
      uniprot "UNIPROT:P04275"
    ]
    graphics [
      x 1652.5
      y 1257.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_239"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 258
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_000584;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000169429;urn:miriam:hgnc:6025;urn:miriam:hgnc.symbol:CXCL8;urn:miriam:hgnc.symbol:CXCL8;urn:miriam:uniprot:P10145;urn:miriam:uniprot:P10145;urn:miriam:ncbigene:3576;urn:miriam:ncbigene:3576"
      hgnc "HGNC_SYMBOL:CXCL8"
      map_id "M121_177"
      name "IL8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa304"
      uniprot "UNIPROT:P10145"
    ]
    graphics [
      x 1901.043215136001
      y 2882.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_177"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 259
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ncbigene:1401;urn:miriam:ncbigene:1401;urn:miriam:taxonomy:9606;urn:miriam:hgnc:2367;urn:miriam:hgnc.symbol:CRP;urn:miriam:uniprot:P02741;urn:miriam:uniprot:P02741;urn:miriam:hgnc.symbol:CRP;urn:miriam:ensembl:ENSG00000132693;urn:miriam:refseq:NM_000567"
      hgnc "HGNC_SYMBOL:CRP"
      map_id "M121_213"
      name "CRP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa410"
      uniprot "UNIPROT:P02741"
    ]
    graphics [
      x 1172.5
      y 1257.3156502059937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_213"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 260
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_000594;urn:miriam:hgnc.symbol:TNF;urn:miriam:hgnc.symbol:TNF;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P01375;urn:miriam:uniprot:P01375;urn:miriam:hgnc:11892;urn:miriam:ncbigene:7124;urn:miriam:ncbigene:7124;urn:miriam:ensembl:ENSG00000232810"
      hgnc "HGNC_SYMBOL:TNF"
      map_id "M121_168"
      name "TNF"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa238"
      uniprot "UNIPROT:P01375"
    ]
    graphics [
      x 1398.7805189192002
      y 2152.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_168"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 261
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc:5992;urn:miriam:hgnc.symbol:IL1B;urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:uniprot:P01584;urn:miriam:refseq:NM_000576;urn:miriam:ncbigene:3553;urn:miriam:ncbigene:3553;urn:miriam:ensembl:ENSG00000125538"
      hgnc "HGNC_SYMBOL:IL1B"
      map_id "M121_170"
      name "IL1B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa244"
      uniprot "UNIPROT:P01584"
    ]
    graphics [
      x 1892.5
      y 1142.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_170"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 262
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32278764;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_227"
      name "s534"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa453"
      uniprot "NA"
    ]
    graphics [
      x 2612.5
      y 1068.7911939082426
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_227"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 263
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32278764;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_231"
      name "s534"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa457"
      uniprot "NA"
    ]
    graphics [
      x 1684.447673432587
      y 242.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_231"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 264
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32278764;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_226"
      name "s534"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa452"
      uniprot "NA"
    ]
    graphics [
      x 3062.5
      y 1617.9068770517201
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_226"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 265
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32278764;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_230"
      name "s534"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa456"
      uniprot "NA"
    ]
    graphics [
      x 1472.5
      y 1456.4685701968617
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_230"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 266
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32278764;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_229"
      name "s534"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa455"
      uniprot "NA"
    ]
    graphics [
      x 2088.7805189192004
      y 2285.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_229"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 267
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ec-code:3.4.21.69;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000115718;urn:miriam:hgnc:9451;urn:miriam:refseq:NM_000312;urn:miriam:uniprot:P04070;urn:miriam:uniprot:P04070;urn:miriam:hgnc.symbol:PROC;urn:miriam:ncbigene:5624;urn:miriam:hgnc.symbol:PROC;urn:miriam:ncbigene:5624"
      hgnc "HGNC_SYMBOL:PROC"
      map_id "M121_244"
      name "PROC"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa482"
      uniprot "UNIPROT:P04070"
    ]
    graphics [
      x 2192.5
      y 1253.7133098641311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_244"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 268
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32278764;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_228"
      name "s534"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa454"
      uniprot "NA"
    ]
    graphics [
      x 632.5
      y 879.0558040274691
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_228"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 269
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_236"
      name "s539"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa462"
      uniprot "NA"
    ]
    graphics [
      x 1502.5
      y 1417.3397236403869
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_236"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 270
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:C3;urn:miriam:hgnc.symbol:C3;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000125730;urn:miriam:refseq:NM_000064;urn:miriam:uniprot:P01024;urn:miriam:uniprot:P01024;urn:miriam:hgnc:1318;urn:miriam:ncbigene:718;urn:miriam:ncbigene:718"
      hgnc "HGNC_SYMBOL:C3"
      map_id "M121_173"
      name "C3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa252"
      uniprot "UNIPROT:P01024"
    ]
    graphics [
      x 2508.7805189192004
      y 2342.97150358576
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_173"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 271
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:C3;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000125730;urn:miriam:refseq:NM_000064;urn:miriam:uniprot:P01024;urn:miriam:uniprot:P01024;urn:miriam:mesh:D015926;urn:miriam:hgnc:1318;urn:miriam:ncbigene:718;urn:miriam:ncbigene:718"
      hgnc "HGNC_SYMBOL:C3"
      map_id "M121_179"
      name "C3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa308"
      uniprot "UNIPROT:P01024"
    ]
    graphics [
      x 2012.5
      y 1625.8526976066757
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_179"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 272
    source 2
    target 1
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_50"
      target_id "M121_180"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 273
    source 1
    target 3
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_180"
      target_id "M121_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 274
    source 270
    target 2
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_173"
      target_id "M121_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 275
    source 4
    target 2
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_8"
      target_id "M121_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 276
    source 8
    target 2
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_15"
      target_id "M121_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 277
    source 2
    target 271
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_50"
      target_id "M121_179"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 278
    source 4
    target 3
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_8"
      target_id "M121_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 3
    target 5
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_61"
      target_id "M121_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 5
    target 6
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_9"
      target_id "M121_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 7
    target 6
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_181"
      target_id "M121_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 8
    target 6
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_15"
      target_id "M121_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 6
    target 9
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_51"
      target_id "M121_182"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 6
    target 10
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_51"
      target_id "M121_174"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 83
    target 8
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_60"
      target_id "M121_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 9
    target 233
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_182"
      target_id "M121_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 11
    target 10
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_86"
      target_id "M121_174"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 12
    target 10
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_94"
      target_id "M121_174"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 15
    target 11
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_235"
      target_id "M121_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 16
    target 11
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "M121_158"
      target_id "M121_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 13
    target 12
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_237"
      target_id "M121_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 14
    target 12
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "M121_238"
      target_id "M121_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 16
    target 17
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "M121_158"
      target_id "M121_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 16
    target 18
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_158"
      target_id "M121_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 16
    target 19
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "M121_158"
      target_id "M121_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 16
    target 20
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_158"
      target_id "M121_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 16
    target 21
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "M121_158"
      target_id "M121_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 16
    target 22
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_158"
      target_id "M121_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 16
    target 23
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "M121_158"
      target_id "M121_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 16
    target 24
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_158"
      target_id "M121_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 16
    target 25
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_158"
      target_id "M121_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 269
    target 17
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_236"
      target_id "M121_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 17
    target 95
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_78"
      target_id "M121_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 304
    source 268
    target 18
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_228"
      target_id "M121_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 305
    source 27
    target 18
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_217"
      target_id "M121_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 306
    source 18
    target 254
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_85"
      target_id "M121_169"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 307
    source 267
    target 19
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_244"
      target_id "M121_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 308
    source 19
    target 176
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_77"
      target_id "M121_157"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 309
    source 266
    target 20
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_229"
      target_id "M121_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 310
    source 27
    target 20
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_217"
      target_id "M121_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 311
    source 20
    target 260
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_95"
      target_id "M121_168"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 312
    source 265
    target 21
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_230"
      target_id "M121_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 313
    source 21
    target 259
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_73"
      target_id "M121_213"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 314
    source 264
    target 22
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_226"
      target_id "M121_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 315
    source 27
    target 22
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_217"
      target_id "M121_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 316
    source 22
    target 258
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_87"
      target_id "M121_177"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 317
    source 263
    target 23
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_231"
      target_id "M121_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 318
    source 23
    target 175
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_65"
      target_id "M121_143"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 319
    source 262
    target 24
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_227"
      target_id "M121_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 320
    source 27
    target 24
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_217"
      target_id "M121_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 321
    source 24
    target 261
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_84"
      target_id "M121_170"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 322
    source 26
    target 25
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_225"
      target_id "M121_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 323
    source 27
    target 25
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_217"
      target_id "M121_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 324
    source 25
    target 28
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_88"
      target_id "M121_178"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 325
    source 28
    target 29
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_178"
      target_id "M121_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 326
    source 29
    target 30
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_49"
      target_id "M121_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 327
    source 31
    target 30
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_43"
      target_id "M121_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 328
    source 32
    target 30
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_45"
      target_id "M121_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 329
    source 33
    target 30
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_44"
      target_id "M121_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 330
    source 34
    target 30
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_91"
      target_id "M121_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 331
    source 35
    target 30
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_75"
      target_id "M121_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 332
    source 36
    target 30
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_48"
      target_id "M121_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 333
    source 37
    target 30
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_93"
      target_id "M121_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 334
    source 38
    target 30
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_112"
      target_id "M121_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 335
    source 39
    target 30
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_76"
      target_id "M121_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 336
    source 261
    target 31
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_170"
      target_id "M121_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 337
    source 260
    target 32
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_168"
      target_id "M121_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 338
    source 254
    target 33
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_169"
      target_id "M121_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 339
    source 86
    target 34
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_198"
      target_id "M121_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 340
    source 259
    target 35
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_213"
      target_id "M121_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 341
    source 258
    target 36
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_177"
      target_id "M121_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 342
    source 95
    target 37
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_14"
      target_id "M121_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 343
    source 56
    target 38
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_148"
      target_id "M121_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 344
    source 40
    target 39
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_214"
      target_id "M121_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 345
    source 41
    target 40
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_74"
      target_id "M121_214"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 346
    source 40
    target 42
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_214"
      target_id "M121_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 347
    source 257
    target 41
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_239"
      target_id "M121_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 348
    source 199
    target 41
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "M121_253"
      target_id "M121_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 349
    source 43
    target 42
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_18"
      target_id "M121_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 350
    source 42
    target 44
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_103"
      target_id "M121_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 351
    source 220
    target 43
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_102"
      target_id "M121_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 352
    source 44
    target 45
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_19"
      target_id "M121_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 353
    source 44
    target 46
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_19"
      target_id "M121_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 354
    source 45
    target 256
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_98"
      target_id "M121_223"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 355
    source 46
    target 47
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_99"
      target_id "M121_221"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 356
    source 48
    target 47
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_101"
      target_id "M121_221"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 357
    source 47
    target 49
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_221"
      target_id "M121_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 358
    source 219
    target 48
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_21"
      target_id "M121_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 359
    source 49
    target 50
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_100"
      target_id "M121_224"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 360
    source 51
    target 50
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_109"
      target_id "M121_224"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 361
    source 52
    target 50
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_111"
      target_id "M121_224"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 362
    source 69
    target 51
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_245"
      target_id "M121_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 363
    source 53
    target 52
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_246"
      target_id "M121_111"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 364
    source 54
    target 53
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_110"
      target_id "M121_246"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 365
    source 55
    target 54
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_254"
      target_id "M121_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 366
    source 56
    target 54
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_148"
      target_id "M121_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 367
    source 57
    target 56
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_25"
      target_id "M121_148"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 368
    source 58
    target 56
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_116"
      target_id "M121_148"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 369
    source 59
    target 56
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_40"
      target_id "M121_148"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 370
    source 56
    target 60
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_148"
      target_id "M121_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 371
    source 56
    target 61
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_148"
      target_id "M121_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 372
    source 56
    target 62
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_148"
      target_id "M121_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 373
    source 56
    target 63
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_148"
      target_id "M121_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 374
    source 56
    target 64
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_148"
      target_id "M121_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 375
    source 138
    target 57
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_149"
      target_id "M121_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 376
    source 144
    target 57
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_152"
      target_id "M121_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 377
    source 255
    target 58
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_255"
      target_id "M121_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 378
    source 72
    target 58
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_242"
      target_id "M121_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 379
    source 138
    target 59
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_149"
      target_id "M121_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 380
    source 253
    target 60
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_258"
      target_id "M121_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 381
    source 149
    target 60
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_256"
      target_id "M121_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 382
    source 66
    target 60
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "M121_208"
      target_id "M121_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 383
    source 254
    target 60
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_169"
      target_id "M121_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 384
    source 161
    target 60
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_263"
      target_id "M121_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 385
    source 60
    target 212
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_118"
      target_id "M121_163"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 386
    source 252
    target 61
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_264"
      target_id "M121_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 387
    source 61
    target 161
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_121"
      target_id "M121_263"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 388
    source 250
    target 62
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_257"
      target_id "M121_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 389
    source 72
    target 62
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "M121_242"
      target_id "M121_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 390
    source 155
    target 62
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_265"
      target_id "M121_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 391
    source 161
    target 62
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_263"
      target_id "M121_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 392
    source 251
    target 62
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_267"
      target_id "M121_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 393
    source 62
    target 149
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_117"
      target_id "M121_256"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 394
    source 213
    target 63
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_164"
      target_id "M121_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 395
    source 212
    target 63
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "M121_163"
      target_id "M121_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 396
    source 141
    target 63
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_209"
      target_id "M121_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 397
    source 66
    target 63
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "M121_208"
      target_id "M121_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 398
    source 176
    target 63
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "M121_157"
      target_id "M121_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 399
    source 63
    target 108
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_37"
      target_id "M121_162"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 400
    source 65
    target 64
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_203"
      target_id "M121_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 401
    source 64
    target 66
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_70"
      target_id "M121_208"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 402
    source 70
    target 65
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_67"
      target_id "M121_203"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 403
    source 66
    target 67
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_208"
      target_id "M121_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 404
    source 68
    target 67
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_250"
      target_id "M121_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 405
    source 67
    target 69
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_108"
      target_id "M121_245"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 406
    source 71
    target 70
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_206"
      target_id "M121_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 407
    source 72
    target 70
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "M121_242"
      target_id "M121_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 408
    source 248
    target 71
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_69"
      target_id "M121_206"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 409
    source 71
    target 93
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_206"
      target_id "M121_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 410
    source 72
    target 73
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_242"
      target_id "M121_128"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 411
    source 72
    target 74
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_242"
      target_id "M121_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 412
    source 72
    target 75
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_242"
      target_id "M121_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 413
    source 73
    target 152
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_128"
      target_id "M121_270"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 414
    source 247
    target 74
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_234"
      target_id "M121_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 415
    source 74
    target 86
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_79"
      target_id "M121_198"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 416
    source 76
    target 75
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_188"
      target_id "M121_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 417
    source 75
    target 77
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_57"
      target_id "M121_233"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 418
    source 77
    target 78
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_233"
      target_id "M121_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 419
    source 77
    target 79
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_233"
      target_id "M121_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 420
    source 78
    target 246
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_90"
      target_id "M121_218"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 421
    source 80
    target 79
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_191"
      target_id "M121_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 422
    source 79
    target 81
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_59"
      target_id "M121_193"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 423
    source 79
    target 82
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_59"
      target_id "M121_192"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 424
    source 81
    target 83
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_193"
      target_id "M121_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 425
    source 81
    target 84
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_193"
      target_id "M121_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 426
    source 238
    target 83
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_195"
      target_id "M121_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 427
    source 85
    target 84
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_197"
      target_id "M121_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 428
    source 84
    target 86
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_62"
      target_id "M121_198"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 429
    source 87
    target 86
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_71"
      target_id "M121_198"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 430
    source 86
    target 88
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_198"
      target_id "M121_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 431
    source 92
    target 87
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_202"
      target_id "M121_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 432
    source 88
    target 89
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_63"
      target_id "M121_199"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 433
    source 89
    target 90
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_199"
      target_id "M121_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 434
    source 90
    target 91
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_106"
      target_id "M121_200"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 435
    source 92
    target 93
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_202"
      target_id "M121_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 436
    source 92
    target 94
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_202"
      target_id "M121_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 437
    source 93
    target 235
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_68"
      target_id "M121_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 438
    source 95
    target 94
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_14"
      target_id "M121_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 439
    source 94
    target 96
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_72"
      target_id "M121_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 440
    source 97
    target 95
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_55"
      target_id "M121_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 441
    source 95
    target 98
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_14"
      target_id "M121_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 442
    source 95
    target 99
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_14"
      target_id "M121_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 443
    source 225
    target 97
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_13"
      target_id "M121_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 444
    source 226
    target 97
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_185"
      target_id "M121_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 445
    source 105
    target 98
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_249"
      target_id "M121_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 446
    source 98
    target 106
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_92"
      target_id "M121_160"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 447
    source 99
    target 100
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_64"
      target_id "M121_201"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 448
    source 100
    target 101
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_201"
      target_id "M121_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 449
    source 100
    target 102
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_201"
      target_id "M121_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 450
    source 101
    target 104
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_104"
      target_id "M121_212"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 451
    source 102
    target 103
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_105"
      target_id "M121_211"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 452
    source 106
    target 107
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_160"
      target_id "M121_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 453
    source 108
    target 107
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_162"
      target_id "M121_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 454
    source 109
    target 107
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_167"
      target_id "M121_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 455
    source 110
    target 107
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_137"
      target_id "M121_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 456
    source 111
    target 107
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_132"
      target_id "M121_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 457
    source 107
    target 112
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_35"
      target_id "M121_161"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 458
    source 142
    target 108
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_113"
      target_id "M121_162"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 459
    source 215
    target 109
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_56"
      target_id "M121_167"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 460
    source 126
    target 110
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_27"
      target_id "M121_137"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 461
    source 110
    target 182
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_137"
      target_id "M121_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 462
    source 111
    target 130
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_132"
      target_id "M121_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 463
    source 111
    target 129
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_132"
      target_id "M121_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 464
    source 111
    target 169
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_132"
      target_id "M121_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 465
    source 112
    target 113
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_161"
      target_id "M121_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 466
    source 112
    target 114
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_161"
      target_id "M121_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 467
    source 203
    target 113
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_219"
      target_id "M121_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 468
    source 174
    target 113
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_166"
      target_id "M121_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 469
    source 199
    target 113
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "M121_253"
      target_id "M121_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 470
    source 113
    target 224
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_97"
      target_id "M121_159"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 471
    source 115
    target 114
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_215"
      target_id "M121_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 472
    source 116
    target 114
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_243"
      target_id "M121_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 473
    source 114
    target 117
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_80"
      target_id "M121_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 474
    source 116
    target 118
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "M121_243"
      target_id "M121_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 475
    source 119
    target 118
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_155"
      target_id "M121_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 476
    source 120
    target 118
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_145"
      target_id "M121_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 477
    source 118
    target 121
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_36"
      target_id "M121_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 478
    source 207
    target 119
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_66"
      target_id "M121_155"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 479
    source 119
    target 122
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "M121_155"
      target_id "M121_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 480
    source 119
    target 183
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "M121_155"
      target_id "M121_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 481
    source 122
    target 120
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_34"
      target_id "M121_145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 482
    source 120
    target 123
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_145"
      target_id "M121_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 483
    source 120
    target 124
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_145"
      target_id "M121_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 484
    source 120
    target 125
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_145"
      target_id "M121_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 485
    source 120
    target 126
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_145"
      target_id "M121_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 486
    source 192
    target 122
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_156"
      target_id "M121_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 487
    source 223
    target 122
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_186"
      target_id "M121_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 488
    source 211
    target 123
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_176"
      target_id "M121_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 489
    source 123
    target 195
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_47"
      target_id "M121_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 490
    source 175
    target 124
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_143"
      target_id "M121_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 491
    source 176
    target 124
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "M121_157"
      target_id "M121_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 492
    source 124
    target 177
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_122"
      target_id "M121_147"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 493
    source 124
    target 178
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_122"
      target_id "M121_144"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 494
    source 173
    target 125
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_165"
      target_id "M121_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 495
    source 125
    target 174
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_38"
      target_id "M121_166"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 496
    source 127
    target 126
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_131"
      target_id "M121_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 497
    source 126
    target 128
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_27"
      target_id "M121_136"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 498
    source 129
    target 127
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_26"
      target_id "M121_131"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 499
    source 127
    target 130
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_131"
      target_id "M121_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 500
    source 171
    target 129
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_134"
      target_id "M121_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 501
    source 129
    target 172
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_26"
      target_id "M121_135"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 502
    source 130
    target 131
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_82"
      target_id "M121_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 503
    source 132
    target 131
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_42"
      target_id "M121_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 504
    source 131
    target 133
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_172"
      target_id "M121_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 505
    source 139
    target 132
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_1"
      target_id "M121_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 506
    source 132
    target 140
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_42"
      target_id "M121_171"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 507
    source 132
    target 141
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_42"
      target_id "M121_209"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 508
    source 134
    target 133
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_216"
      target_id "M121_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 509
    source 133
    target 135
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_83"
      target_id "M121_151"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 510
    source 135
    target 136
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_151"
      target_id "M121_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 511
    source 137
    target 136
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_150"
      target_id "M121_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 512
    source 136
    target 138
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_23"
      target_id "M121_149"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 513
    source 169
    target 139
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_28"
      target_id "M121_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 514
    source 141
    target 142
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_209"
      target_id "M121_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 515
    source 141
    target 143
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_209"
      target_id "M121_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 516
    source 168
    target 142
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_248"
      target_id "M121_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 517
    source 144
    target 143
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_152"
      target_id "M121_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 518
    source 143
    target 145
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_115"
      target_id "M121_210"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 519
    source 143
    target 146
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_115"
      target_id "M121_205"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 520
    source 147
    target 144
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_123"
      target_id "M121_152"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 521
    source 148
    target 147
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_266"
      target_id "M121_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 522
    source 149
    target 147
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_256"
      target_id "M121_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 523
    source 149
    target 150
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_256"
      target_id "M121_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 524
    source 149
    target 151
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_256"
      target_id "M121_130"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 525
    source 150
    target 156
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_119"
      target_id "M121_260"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 526
    source 151
    target 152
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_130"
      target_id "M121_270"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 527
    source 152
    target 153
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "M121_270"
      target_id "M121_129"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 528
    source 154
    target 153
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_271"
      target_id "M121_129"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 529
    source 153
    target 155
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_129"
      target_id "M121_265"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 530
    source 156
    target 157
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_260"
      target_id "M121_126"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 531
    source 156
    target 158
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_260"
      target_id "M121_127"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 532
    source 156
    target 159
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_260"
      target_id "M121_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 533
    source 157
    target 167
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_126"
      target_id "M121_262"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 534
    source 158
    target 166
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_127"
      target_id "M121_269"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 535
    source 160
    target 159
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_259"
      target_id "M121_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 536
    source 161
    target 159
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_263"
      target_id "M121_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 537
    source 159
    target 162
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_120"
      target_id "M121_261"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 538
    source 161
    target 165
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_263"
      target_id "M121_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 539
    source 162
    target 163
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_261"
      target_id "M121_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 540
    source 163
    target 164
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_124"
      target_id "M121_268"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 541
    source 165
    target 164
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_125"
      target_id "M121_268"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 542
    source 170
    target 169
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_133"
      target_id "M121_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 543
    source 176
    target 193
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "M121_157"
      target_id "M121_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 544
    source 178
    target 179
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_144"
      target_id "M121_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 545
    source 180
    target 179
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_139"
      target_id "M121_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 546
    source 179
    target 181
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_30"
      target_id "M121_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 547
    source 182
    target 180
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_29"
      target_id "M121_139"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 548
    source 180
    target 183
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_139"
      target_id "M121_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 549
    source 222
    target 182
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_138"
      target_id "M121_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 550
    source 184
    target 183
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_141"
      target_id "M121_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 551
    source 183
    target 185
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_24"
      target_id "M121_140"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 552
    source 183
    target 186
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_24"
      target_id "M121_142"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 553
    source 185
    target 187
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_140"
      target_id "M121_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 554
    source 188
    target 187
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_154"
      target_id "M121_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 555
    source 187
    target 189
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_32"
      target_id "M121_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 556
    source 193
    target 188
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_31"
      target_id "M121_154"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 557
    source 189
    target 190
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_3"
      target_id "M121_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 558
    source 191
    target 190
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_146"
      target_id "M121_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 559
    source 190
    target 192
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_33"
      target_id "M121_156"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 560
    source 194
    target 193
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_153"
      target_id "M121_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 561
    source 195
    target 193
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "M121_7"
      target_id "M121_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 562
    source 195
    target 196
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "M121_7"
      target_id "M121_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 563
    source 197
    target 196
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_6"
      target_id "M121_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 564
    source 198
    target 196
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_252"
      target_id "M121_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 565
    source 199
    target 196
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "M121_253"
      target_id "M121_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 566
    source 196
    target 200
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_46"
      target_id "M121_204"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 567
    source 206
    target 197
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_41"
      target_id "M121_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 568
    source 197
    target 217
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_6"
      target_id "M121_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 569
    source 199
    target 204
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "M121_253"
      target_id "M121_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 570
    source 199
    target 205
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "M121_253"
      target_id "M121_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 571
    source 199
    target 206
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "M121_253"
      target_id "M121_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 572
    source 199
    target 207
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "M121_253"
      target_id "M121_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 573
    source 200
    target 201
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_204"
      target_id "M121_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 574
    source 202
    target 201
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_220"
      target_id "M121_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 575
    source 201
    target 203
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_96"
      target_id "M121_219"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 576
    source 212
    target 204
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_163"
      target_id "M121_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 577
    source 213
    target 204
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_164"
      target_id "M121_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 578
    source 204
    target 214
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_39"
      target_id "M121_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 579
    source 210
    target 205
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_240"
      target_id "M121_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 580
    source 205
    target 211
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_81"
      target_id "M121_176"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 581
    source 209
    target 206
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_251"
      target_id "M121_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 582
    source 208
    target 207
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_241"
      target_id "M121_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 583
    source 212
    target 215
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "M121_163"
      target_id "M121_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 584
    source 216
    target 215
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_187"
      target_id "M121_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 585
    source 218
    target 217
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_222"
      target_id "M121_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 586
    source 217
    target 219
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_107"
      target_id "M121_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 587
    source 218
    target 220
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_222"
      target_id "M121_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 588
    source 221
    target 220
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_20"
      target_id "M121_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 589
    source 227
    target 225
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_54"
      target_id "M121_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 590
    source 228
    target 227
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_11"
      target_id "M121_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 591
    source 229
    target 227
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_12"
      target_id "M121_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 592
    source 230
    target 228
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_53"
      target_id "M121_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 593
    source 231
    target 230
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_10"
      target_id "M121_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 594
    source 232
    target 230
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_184"
      target_id "M121_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 595
    source 233
    target 231
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_52"
      target_id "M121_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 596
    source 234
    target 233
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_183"
      target_id "M121_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 597
    source 235
    target 236
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_16"
      target_id "M121_114"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 598
    source 236
    target 237
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_114"
      target_id "M121_247"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 599
    source 239
    target 238
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_89"
      target_id "M121_195"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 600
    source 240
    target 239
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_194"
      target_id "M121_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 601
    source 241
    target 239
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_189"
      target_id "M121_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 602
    source 239
    target 242
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_89"
      target_id "M121_196"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 603
    source 243
    target 241
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_58"
      target_id "M121_189"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 604
    source 244
    target 243
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_232"
      target_id "M121_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 605
    source 245
    target 243
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_190"
      target_id "M121_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 606
    source 249
    target 248
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_207"
      target_id "M121_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
