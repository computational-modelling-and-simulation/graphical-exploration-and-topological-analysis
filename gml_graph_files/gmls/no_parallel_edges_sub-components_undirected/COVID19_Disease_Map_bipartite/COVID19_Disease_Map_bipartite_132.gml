# generated with VANTED V2.8.2 at Fri Mar 04 10:04:35 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_369"
      name "Nsp10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1429"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1098.7805189192002
      y 2512.882220103153
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_369"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_222"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10201"
      uniprot "NA"
    ]
    graphics [
      x 1188.7805189192002
      y 2367.3156502059937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_222"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:ERGIC1;urn:miriam:ensembl:ENSG00000113719;urn:miriam:hgnc.symbol:ERGIC1;urn:miriam:hgnc:29205;urn:miriam:ncbigene:57222;urn:miriam:ncbigene:57222;urn:miriam:refseq:NM_020462;urn:miriam:uniprot:Q969X5"
      hgnc "HGNC_SYMBOL:ERGIC1"
      map_id "M115_349"
      name "ERGIC1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1383"
      uniprot "UNIPROT:Q969X5"
    ]
    graphics [
      x 1502.5
      y 1687.3397236403869
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_349"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:hgnc.symbol:ERGIC1;urn:miriam:ensembl:ENSG00000113719;urn:miriam:hgnc.symbol:ERGIC1;urn:miriam:hgnc:29205;urn:miriam:ncbigene:57222;urn:miriam:ncbigene:57222;urn:miriam:refseq:NM_020462;urn:miriam:uniprot:Q969X5"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:ERGIC1"
      map_id "M115_22"
      name "ERGIC1comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa119"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q969X5"
    ]
    graphics [
      x 452.5
      y 2207.0917536624975
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 5
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_369"
      target_id "M115_222"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 6
    source 3
    target 2
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_349"
      target_id "M115_222"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 7
    source 2
    target 4
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_222"
      target_id "M115_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
