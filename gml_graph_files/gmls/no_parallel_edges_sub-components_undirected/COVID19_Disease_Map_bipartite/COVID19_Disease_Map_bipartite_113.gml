# generated with VANTED V2.8.2 at Fri Mar 04 10:04:34 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc:20237;urn:miriam:ensembl:ENSG00000130545;urn:miriam:ncbigene:92359;urn:miriam:ncbigene:92359;urn:miriam:uniprot:Q9BUF7;urn:miriam:uniprot:Q9BUF7;urn:miriam:hgnc.symbol:CRB3;urn:miriam:hgnc.symbol:CRB3;urn:miriam:refseq:NM_139161"
      hgnc "HGNC_SYMBOL:CRB3"
      map_id "M17_55"
      name "CRB3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa29"
      uniprot "UNIPROT:Q9BUF7"
    ]
    graphics [
      x 1886.1439857514508
      y 1442.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:20861307"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_13"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re14"
      uniprot "NA"
    ]
    graphics [
      x 2162.5
      y 725.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc:20237;urn:miriam:ensembl:ENSG00000130545;urn:miriam:ncbigene:92359;urn:miriam:ncbigene:92359;urn:miriam:uniprot:Q9BUF7;urn:miriam:uniprot:Q9BUF7;urn:miriam:hgnc.symbol:CRB3;urn:miriam:hgnc.symbol:CRB3;urn:miriam:refseq:NM_139161"
      hgnc "HGNC_SYMBOL:CRB3"
      map_id "M17_61"
      name "CRB3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa34"
      uniprot "UNIPROT:Q9BUF7"
    ]
    graphics [
      x 2522.5
      y 698.4621842014352
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "PUBMED:20861307"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_25"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re31"
      uniprot "NA"
    ]
    graphics [
      x 2852.5
      y 1056.1195464255575
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc:18669;urn:miriam:ncbigene:64398;urn:miriam:refseq:NM_022474;urn:miriam:ncbigene:64398;urn:miriam:ensembl:ENSG00000072415;urn:miriam:hgnc.symbol:MPP5;urn:miriam:hgnc.symbol:MPP5;urn:miriam:uniprot:Q8N3R9;urn:miriam:uniprot:Q8N3R9"
      hgnc "HGNC_SYMBOL:MPP5"
      map_id "M17_62"
      name "MPP5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa35"
      uniprot "UNIPROT:Q8N3R9"
    ]
    graphics [
      x 3392.5
      y 1436.26874616626
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:refseq:NM_170605;urn:miriam:ncbigene:10207;urn:miriam:ensembl:ENSG00000132849;urn:miriam:uniprot:Q8NI35;urn:miriam:uniprot:Q8NI35;urn:miriam:hgnc.symbol:PATJ;urn:miriam:hgnc.symbol:PATJ;urn:miriam:hgnc:28881"
      hgnc "HGNC_SYMBOL:PATJ"
      map_id "M17_60"
      name "PATJ"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa33"
      uniprot "UNIPROT:Q8NI35"
    ]
    graphics [
      x 722.5
      y 2039.7438419327407
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:refseq:NM_170605;urn:miriam:ncbigene:10207;urn:miriam:ensembl:ENSG00000132849;urn:miriam:uniprot:Q8NI35;urn:miriam:uniprot:Q8NI35;urn:miriam:hgnc.symbol:PATJ;urn:miriam:hgnc.symbol:PATJ;urn:miriam:hgnc:28881;urn:miriam:hgnc:18669;urn:miriam:ncbigene:64398;urn:miriam:refseq:NM_022474;urn:miriam:ncbigene:64398;urn:miriam:ensembl:ENSG00000072415;urn:miriam:hgnc.symbol:MPP5;urn:miriam:hgnc.symbol:MPP5;urn:miriam:uniprot:Q8N3R9;urn:miriam:uniprot:Q8N3R9;urn:miriam:hgnc:20237;urn:miriam:ensembl:ENSG00000130545;urn:miriam:ncbigene:92359;urn:miriam:ncbigene:92359;urn:miriam:uniprot:Q9BUF7;urn:miriam:uniprot:Q9BUF7;urn:miriam:hgnc.symbol:CRB3;urn:miriam:hgnc.symbol:CRB3;urn:miriam:refseq:NM_139161"
      hgnc "HGNC_SYMBOL:PATJ;HGNC_SYMBOL:MPP5;HGNC_SYMBOL:CRB3"
      map_id "M17_7"
      name "CRB3:PALS1:PATJ_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa7"
      uniprot "UNIPROT:Q8NI35;UNIPROT:Q8N3R9;UNIPROT:Q9BUF7"
    ]
    graphics [
      x 2192.5
      y 1043.7133098641311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "PUBMED:20861307"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_26"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "re32"
      uniprot "NA"
    ]
    graphics [
      x 1592.5
      y 803.1199123051069
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_71"
      name "Maintenance_space_of_space_tight_space_junction"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa74"
      uniprot "NA"
    ]
    graphics [
      x 2762.5
      y 1825.8267364055584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "PUBMED:20861307"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_27"
      name "NA"
      node_subtype "UNKNOWN_NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re33"
      uniprot "NA"
    ]
    graphics [
      x 2418.7805189192004
      y 2186.0089009888093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ncbiprotein:BCD58755;urn:miriam:uniprot:E;urn:miriam:hgnc:18669;urn:miriam:ncbigene:64398;urn:miriam:refseq:NM_022474;urn:miriam:ncbigene:64398;urn:miriam:ensembl:ENSG00000072415;urn:miriam:hgnc.symbol:MPP5;urn:miriam:hgnc.symbol:PALS1;urn:miriam:uniprot:Q8N3R9;urn:miriam:uniprot:Q8N3R9"
      hgnc "HGNC_SYMBOL:MPP5;HGNC_SYMBOL:PALS1"
      map_id "M17_4"
      name "E_minus_PALS1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa3"
      uniprot "UNIPROT:E;UNIPROT:Q8N3R9"
    ]
    graphics [
      x 3198.7805189192004
      y 2300.5773127900743
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "PUBMED:20861307"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_10"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re11"
      uniprot "NA"
    ]
    graphics [
      x 3272.5
      y 1466.1844956582163
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "PUBMED:20861307"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_11"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re12"
      uniprot "NA"
    ]
    graphics [
      x 1801.4316445647612
      y 2972.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_58"
      name "csa6_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa31"
      uniprot "NA"
    ]
    graphics [
      x 1038.7805189192002
      y 2391.9867962075773
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ncbiprotein:BCD58755;urn:miriam:uniprot:E"
      hgnc "NA"
      map_id "M17_59"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa32"
      uniprot "UNIPROT:E"
    ]
    graphics [
      x 2372.5
      y 1672.0608348716244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc:18669;urn:miriam:ncbigene:64398;urn:miriam:refseq:NM_022474;urn:miriam:ncbigene:64398;urn:miriam:ensembl:ENSG00000072415;urn:miriam:hgnc.symbol:MPP5;urn:miriam:hgnc.symbol:MPP5;urn:miriam:uniprot:Q8N3R9;urn:miriam:uniprot:Q8N3R9"
      hgnc "HGNC_SYMBOL:MPP5"
      map_id "M17_57"
      name "MPP5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa30"
      uniprot "UNIPROT:Q8N3R9"
    ]
    graphics [
      x 3452.5
      y 2089.4311077605753
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "PUBMED:20861307"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_12"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re13"
      uniprot "NA"
    ]
    graphics [
      x 2231.043215136001
      y 2513.713309864131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "PUBMED:20861307"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_14"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re15"
      uniprot "NA"
    ]
    graphics [
      x 828.7805189192002
      y 2305.4401700720628
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:refseq:NM_170605;urn:miriam:ncbigene:10207;urn:miriam:ensembl:ENSG00000132849;urn:miriam:uniprot:Q8NI35;urn:miriam:uniprot:Q8NI35;urn:miriam:hgnc.symbol:PATJ;urn:miriam:hgnc.symbol:PATJ;urn:miriam:hgnc:28881"
      hgnc "HGNC_SYMBOL:PATJ"
      map_id "M17_54"
      name "PATJ"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa28"
      uniprot "UNIPROT:Q8NI35"
    ]
    graphics [
      x 1202.5
      y 1527.3156502059937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 20
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_55"
      target_id "M17_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 21
    source 2
    target 3
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_13"
      target_id "M17_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 22
    source 3
    target 4
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_61"
      target_id "M17_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 23
    source 5
    target 4
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_62"
      target_id "M17_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 24
    source 6
    target 4
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_60"
      target_id "M17_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 25
    source 4
    target 7
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_25"
      target_id "M17_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 26
    source 17
    target 5
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_12"
      target_id "M17_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 27
    source 18
    target 6
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_14"
      target_id "M17_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 28
    source 7
    target 8
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_7"
      target_id "M17_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 29
    source 8
    target 9
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_26"
      target_id "M17_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 30
    source 10
    target 9
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_27"
      target_id "M17_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 31
    source 11
    target 10
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_4"
      target_id "M17_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 32
    source 12
    target 11
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_10"
      target_id "M17_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 33
    source 11
    target 13
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_4"
      target_id "M17_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 34
    source 15
    target 12
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_59"
      target_id "M17_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 35
    source 16
    target 12
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_57"
      target_id "M17_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 36
    source 13
    target 14
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_11"
      target_id "M17_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 37
    source 16
    target 17
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_57"
      target_id "M17_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 38
    source 19
    target 18
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_54"
      target_id "M17_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
