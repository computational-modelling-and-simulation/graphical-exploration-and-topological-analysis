# generated with VANTED V2.8.2 at Fri Mar 04 10:04:34 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:refseq:NM_001880;urn:miriam:uniprot:P15336;urn:miriam:uniprot:P15336;urn:miriam:ncbigene:1386;urn:miriam:hgnc:784;urn:miriam:hgnc.symbol:ATF2;urn:miriam:hgnc.symbol:ATF2;urn:miriam:ensembl:ENSG00000115966"
      hgnc "HGNC_SYMBOL:ATF2"
      map_id "M110_37"
      name "ATF2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa71"
      uniprot "UNIPROT:P15336"
    ]
    graphics [
      x 1202.5
      y 1587.3156502059937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:7824938"
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M110_14"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re25"
      uniprot "NA"
    ]
    graphics [
      x 2492.5
      y 1111.435024234829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:ec-code:2.7.11.24;urn:miriam:refseq:NM_001278547;urn:miriam:ensembl:ENSG00000107643;urn:miriam:hgnc.symbol:MAPK8;urn:miriam:uniprot:P45983;urn:miriam:uniprot:P45983;urn:miriam:hgnc.symbol:MAPK8;urn:miriam:hgnc:6881;urn:miriam:ncbigene:5599;urn:miriam:ncbigene:5599;urn:miriam:ec-code:2.7.11.24;urn:miriam:ensembl:ENSG00000109339;urn:miriam:hgnc:6872;urn:miriam:uniprot:P53779;urn:miriam:uniprot:P53779;urn:miriam:refseq:NM_001318067;urn:miriam:hgnc.symbol:MAPK10;urn:miriam:hgnc.symbol:MAPK10;urn:miriam:ncbigene:5602;urn:miriam:ncbigene:5602;urn:miriam:ec-code:2.7.11.24;urn:miriam:hgnc.symbol:MAPK9;urn:miriam:ensembl:ENSG00000050748;urn:miriam:uniprot:P45984;urn:miriam:uniprot:P45984;urn:miriam:hgnc.symbol:MAPK9;urn:miriam:hgnc:6886;urn:miriam:ncbigene:5601;urn:miriam:ncbigene:5601;urn:miriam:refseq:NM_001135044"
      hgnc "HGNC_SYMBOL:MAPK8;HGNC_SYMBOL:MAPK10;HGNC_SYMBOL:MAPK9"
      map_id "M110_1"
      name "JNK"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa1"
      uniprot "UNIPROT:P45983;UNIPROT:P53779;UNIPROT:P45984"
    ]
    graphics [
      x 2162.5
      y 1613.7133098641311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:refseq:NM_001880;urn:miriam:uniprot:P15336;urn:miriam:uniprot:P15336;urn:miriam:ncbigene:1386;urn:miriam:hgnc:784;urn:miriam:hgnc.symbol:ATF2;urn:miriam:hgnc.symbol:ATF2;urn:miriam:ensembl:ENSG00000115966"
      hgnc "HGNC_SYMBOL:ATF2"
      map_id "M110_28"
      name "ATF2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa33"
      uniprot "UNIPROT:P15336"
    ]
    graphics [
      x 1052.5
      y 1048.8948945564143
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "PUBMED:17267381"
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M110_22"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re42"
      uniprot "NA"
    ]
    graphics [
      x 1815.2336320082195
      y 3062.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "PUBMED:17141229"
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M110_21"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re4"
      uniprot "NA"
    ]
    graphics [
      x 2522.5
      y 1532.9715035857598
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "PUBMED:10567572"
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M110_17"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re35"
      uniprot "NA"
    ]
    graphics [
      x 1832.5
      y 1082.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "PUBMED:9724739"
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M110_15"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re26"
      uniprot "NA"
    ]
    graphics [
      x 1720.9135463862924
      y 837.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "PUBMED:21561061"
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M110_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re36"
      uniprot "NA"
    ]
    graphics [
      x 542.5
      y 1243.5654979241358
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000130522;urn:miriam:hgnc:6206;urn:miriam:ncbigene:3727;urn:miriam:ncbigene:3727;urn:miriam:hgnc.symbol:JUND;urn:miriam:hgnc.symbol:JUND;urn:miriam:refseq:NM_005354;urn:miriam:uniprot:P17535;urn:miriam:uniprot:P17535;urn:miriam:ncbigene:3725;urn:miriam:ncbigene:3725;urn:miriam:hgnc:6204;urn:miriam:uniprot:P05412;urn:miriam:uniprot:P05412;urn:miriam:hgnc.symbol:JUN;urn:miriam:hgnc.symbol:JUN;urn:miriam:ensembl:ENSG00000177606;urn:miriam:refseq:NM_002228"
      hgnc "HGNC_SYMBOL:JUND;HGNC_SYMBOL:JUN"
      map_id "M110_9"
      name "AP_minus_1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa9"
      uniprot "UNIPROT:P17535;UNIPROT:P05412"
    ]
    graphics [
      x 1052.5
      y 1898.5794444430944
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:uniprot:P59633;urn:miriam:ncbigene:1489670"
      hgnc "NA"
      map_id "M110_38"
      name "3b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa75"
      uniprot "UNIPROT:P59633"
    ]
    graphics [
      x 1652.5
      y 2067.1855687946227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000130522;urn:miriam:hgnc:6206;urn:miriam:ncbigene:3727;urn:miriam:ncbigene:3727;urn:miriam:hgnc.symbol:JUND;urn:miriam:hgnc.symbol:JUND;urn:miriam:refseq:NM_005354;urn:miriam:uniprot:P17535;urn:miriam:uniprot:P17535;urn:miriam:ncbigene:3725;urn:miriam:ncbigene:3725;urn:miriam:hgnc:6204;urn:miriam:uniprot:P05412;urn:miriam:uniprot:P05412;urn:miriam:hgnc.symbol:JUN;urn:miriam:hgnc.symbol:JUN;urn:miriam:ensembl:ENSG00000177606;urn:miriam:refseq:NM_002228"
      hgnc "HGNC_SYMBOL:JUND;HGNC_SYMBOL:JUN"
      map_id "M110_6"
      name "AP_minus_1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa4"
      uniprot "UNIPROT:P17535;UNIPROT:P05412"
    ]
    graphics [
      x 1354.562918729742
      y 382.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M110_12"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re22"
      uniprot "NA"
    ]
    graphics [
      x 2192.5
      y 953.7133098641311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0045087"
      hgnc "NA"
      map_id "M110_25"
      name "Innate_space_Immunity"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa16"
      uniprot "NA"
    ]
    graphics [
      x 2822.5
      y 1718.4007591851837
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000141510;urn:miriam:hgnc:11998;urn:miriam:ncbigene:7157;urn:miriam:hgnc.symbol:TP53;urn:miriam:hgnc.symbol:TP53;urn:miriam:uniprot:P04637;urn:miriam:uniprot:P04637;urn:miriam:refseq:NM_000546"
      hgnc "HGNC_SYMBOL:TP53"
      map_id "M110_36"
      name "TP53"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa70"
      uniprot "UNIPROT:P04637"
    ]
    graphics [
      x 1862.5
      y 1712.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000141510;urn:miriam:hgnc:11998;urn:miriam:ncbigene:7157;urn:miriam:hgnc.symbol:TP53;urn:miriam:hgnc.symbol:TP53;urn:miriam:uniprot:P04637;urn:miriam:uniprot:P04637;urn:miriam:refseq:NM_000546"
      hgnc "HGNC_SYMBOL:TP53"
      map_id "M110_29"
      name "TP53"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa34"
      uniprot "UNIPROT:P04637"
    ]
    graphics [
      x 1412.5
      y 1162.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "PUBMED:9724739"
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M110_16"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re30"
      uniprot "NA"
    ]
    graphics [
      x 1412.5
      y 742.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0072331"
      hgnc "NA"
      map_id "M110_30"
      name "TP53_space_signalling"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa36"
      uniprot "NA"
    ]
    graphics [
      x 1777.432708728739
      y 452.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:hgnc.symbol:BCL2;urn:miriam:hgnc.symbol:BCL2;urn:miriam:refseq:NM_000657;urn:miriam:ncbigene:596;urn:miriam:uniprot:P10415;urn:miriam:uniprot:P10415;urn:miriam:ensembl:ENSG00000171791;urn:miriam:refseq:NM_000633;urn:miriam:hgnc:990"
      hgnc "HGNC_SYMBOL:BCL2"
      map_id "M110_34"
      name "BCL2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa53"
      uniprot "UNIPROT:P10415"
    ]
    graphics [
      x 2492.5
      y 1051.435024234829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:hgnc.symbol:BCL2;urn:miriam:hgnc.symbol:BCL2;urn:miriam:refseq:NM_000657;urn:miriam:ncbigene:596;urn:miriam:uniprot:P10415;urn:miriam:uniprot:P10415;urn:miriam:ensembl:ENSG00000171791;urn:miriam:refseq:NM_000633;urn:miriam:hgnc:990"
      hgnc "HGNC_SYMBOL:BCL2"
      map_id "M110_24"
      name "BCL2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa12"
      uniprot "UNIPROT:P10415"
    ]
    graphics [
      x 1412.5
      y 1822.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M110_23"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re43"
      uniprot "NA"
    ]
    graphics [
      x 1158.7805189192002
      y 2244.991042282978
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M110_10"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re14"
      uniprot "NA"
    ]
    graphics [
      x 782.5
      y 1507.835840060344
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0006914"
      hgnc "NA"
      map_id "M110_27"
      name "Autophagy"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa18"
      uniprot "NA"
    ]
    graphics [
      x 1952.5
      y 632.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0006915"
      hgnc "NA"
      map_id "M110_26"
      name "Apoptosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa17"
      uniprot "NA"
    ]
    graphics [
      x 1082.5
      y 2120.7023828005426
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:ec-code:2.7.11.24;urn:miriam:refseq:NM_001278547;urn:miriam:ensembl:ENSG00000107643;urn:miriam:hgnc.symbol:MAPK8;urn:miriam:uniprot:P45983;urn:miriam:uniprot:P45983;urn:miriam:hgnc.symbol:MAPK8;urn:miriam:hgnc:6881;urn:miriam:ncbigene:5599;urn:miriam:ncbigene:5599;urn:miriam:ec-code:2.7.11.24;urn:miriam:hgnc.symbol:MAPK9;urn:miriam:ensembl:ENSG00000050748;urn:miriam:uniprot:P45984;urn:miriam:uniprot:P45984;urn:miriam:hgnc.symbol:MAPK9;urn:miriam:hgnc:6886;urn:miriam:ncbigene:5601;urn:miriam:ncbigene:5601;urn:miriam:refseq:NM_001135044;urn:miriam:ec-code:2.7.11.24;urn:miriam:ensembl:ENSG00000109339;urn:miriam:hgnc:6872;urn:miriam:uniprot:P53779;urn:miriam:uniprot:P53779;urn:miriam:refseq:NM_001318067;urn:miriam:hgnc.symbol:MAPK10;urn:miriam:hgnc.symbol:MAPK10;urn:miriam:ncbigene:5602;urn:miriam:ncbigene:5602"
      hgnc "HGNC_SYMBOL:MAPK8;HGNC_SYMBOL:MAPK9;HGNC_SYMBOL:MAPK10"
      map_id "M110_3"
      name "JNK"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa12"
      uniprot "UNIPROT:P45983;UNIPROT:P45984;UNIPROT:P53779"
    ]
    graphics [
      x 948.7805189192002
      y 2402.265850066136
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:ec-code:2.7.12.2;urn:miriam:hgnc:6844;urn:miriam:ensembl:ENSG00000065559;urn:miriam:refseq:NM_001281435;urn:miriam:uniprot:P45985;urn:miriam:uniprot:P45985;urn:miriam:hgnc.symbol:MAP2K4;urn:miriam:hgnc.symbol:MAP2K4;urn:miriam:ncbigene:6416"
      hgnc "HGNC_SYMBOL:MAP2K4"
      map_id "M110_31"
      name "MAP2K4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa4"
      uniprot "UNIPROT:P45985"
    ]
    graphics [
      x 1082.5
      y 2026.3529417498703
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:ec-code:2.7.12.2;urn:miriam:ncbigene:5609;urn:miriam:uniprot:O14733;urn:miriam:uniprot:O14733;urn:miriam:ensembl:ENSG00000076984;urn:miriam:hgnc:6847;urn:miriam:refseq:NM_001297555;urn:miriam:hgnc.symbol:MAP2K7;urn:miriam:hgnc.symbol:MAP2K7"
      hgnc "HGNC_SYMBOL:MAP2K7"
      map_id "M110_33"
      name "MAP2K7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa5"
      uniprot "UNIPROT:O14733"
    ]
    graphics [
      x 2231.043215136001
      y 2573.713309864131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:uniprot:P59635;urn:miriam:ncbigene:1489674"
      hgnc "NA"
      map_id "M110_39"
      name "7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa76"
      uniprot "UNIPROT:P59635"
    ]
    graphics [
      x 1862.5
      y 572.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:ncbigene:1489669"
      hgnc "NA"
      map_id "M110_40"
      name "3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa77"
      uniprot "UNIPROT:P59632"
    ]
    graphics [
      x 2072.5
      y 1865.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M110_11"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re2"
      uniprot "NA"
    ]
    graphics [
      x 2135.1361657279376
      y 2923.5803415987098
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:ec-code:2.7.12.2;urn:miriam:ncbigene:5609;urn:miriam:uniprot:O14733;urn:miriam:uniprot:O14733;urn:miriam:ensembl:ENSG00000076984;urn:miriam:hgnc:6847;urn:miriam:refseq:NM_001297555;urn:miriam:hgnc.symbol:MAP2K7;urn:miriam:hgnc.symbol:MAP2K7"
      hgnc "HGNC_SYMBOL:MAP2K7"
      map_id "M110_32"
      name "MAP2K7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa44"
      uniprot "UNIPROT:O14733"
    ]
    graphics [
      x 1472.5
      y 1306.4685701968617
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:refseq:NM_001284230;urn:miriam:hgnc:6861;urn:miriam:hgnc.symbol:MAP3K9;urn:miriam:hgnc.symbol:MAP3K9;urn:miriam:ensembl:ENSG00000006432;urn:miriam:ncbigene:4293;urn:miriam:uniprot:P80192;urn:miriam:uniprot:P80192;urn:miriam:ncbigene:4293;urn:miriam:ec-code:2.7.11.25;urn:miriam:ncbigene:4294;urn:miriam:ncbigene:4294;urn:miriam:hgnc.symbol:MAP3K10;urn:miriam:hgnc.symbol:MAP3K10;urn:miriam:hgnc:6849;urn:miriam:uniprot:Q02779;urn:miriam:uniprot:Q02779;urn:miriam:refseq:NM_002446;urn:miriam:ensembl:ENSG00000130758;urn:miriam:ec-code:2.7.11.25;urn:miriam:hgnc:6850;urn:miriam:ncbigene:4296;urn:miriam:ensembl:ENSG00000173327;urn:miriam:ncbigene:4296;urn:miriam:uniprot:Q16584;urn:miriam:uniprot:Q16584;urn:miriam:hgnc.symbol:MAP3K11;urn:miriam:hgnc.symbol:MAP3K11;urn:miriam:refseq:NM_002419;urn:miriam:ec-code:2.7.11.25"
      hgnc "HGNC_SYMBOL:MAP3K9;HGNC_SYMBOL:MAP3K10;HGNC_SYMBOL:MAP3K11"
      map_id "M110_4"
      name "MLK1_slash_2_slash_3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa2"
      uniprot "UNIPROT:P80192;UNIPROT:Q02779;UNIPROT:Q16584"
    ]
    graphics [
      x 1931.043215136001
      y 2702.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:uniprot:P59635;urn:miriam:ncbigene:1489674;urn:miriam:uniprot:P59633;urn:miriam:ncbigene:1489670;urn:miriam:ncbigene:1489668;urn:miriam:uniprot:P59594;urn:miriam:hgnc.symbol:S;urn:miriam:uniprot:P59632;urn:miriam:ncbigene:1489669"
      hgnc "HGNC_SYMBOL:S"
      map_id "M110_7"
      name "SARS_minus_CoV_minus_1_space_proteins"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa5"
      uniprot "UNIPROT:P59635;UNIPROT:P59633;UNIPROT:P59594;UNIPROT:P59632"
    ]
    graphics [
      x 2335.842963598534
      y 2838.957494553204
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M110_20"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re39"
      uniprot "NA"
    ]
    graphics [
      x 872.5
      y 2201.24274230599
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M110_19"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re37"
      uniprot "NA"
    ]
    graphics [
      x 2912.5
      y 1993.3966563473575
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M110_13"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re23"
      uniprot "NA"
    ]
    graphics [
      x 2358.7805189192004
      y 2152.060834871624
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:hgnc:6850;urn:miriam:ncbigene:4296;urn:miriam:ensembl:ENSG00000173327;urn:miriam:ncbigene:4296;urn:miriam:uniprot:Q16584;urn:miriam:uniprot:Q16584;urn:miriam:hgnc.symbol:MAP3K11;urn:miriam:hgnc.symbol:MAP3K11;urn:miriam:refseq:NM_002419;urn:miriam:ec-code:2.7.11.25;urn:miriam:ncbigene:4294;urn:miriam:ncbigene:4294;urn:miriam:hgnc.symbol:MAP3K10;urn:miriam:hgnc.symbol:MAP3K10;urn:miriam:hgnc:6849;urn:miriam:uniprot:Q02779;urn:miriam:uniprot:Q02779;urn:miriam:refseq:NM_002446;urn:miriam:ensembl:ENSG00000130758;urn:miriam:ec-code:2.7.11.25;urn:miriam:refseq:NM_001284230;urn:miriam:hgnc:6861;urn:miriam:hgnc.symbol:MAP3K9;urn:miriam:hgnc.symbol:MAP3K9;urn:miriam:ensembl:ENSG00000006432;urn:miriam:ncbigene:4293;urn:miriam:uniprot:P80192;urn:miriam:uniprot:P80192;urn:miriam:ncbigene:4293;urn:miriam:ec-code:2.7.11.25"
      hgnc "HGNC_SYMBOL:MAP3K11;HGNC_SYMBOL:MAP3K10;HGNC_SYMBOL:MAP3K9"
      map_id "M110_8"
      name "MLK1_slash_2_slash_3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa6"
      uniprot "UNIPROT:Q16584;UNIPROT:Q02779;UNIPROT:P80192"
    ]
    graphics [
      x 2342.5
      y 1772.963502381953
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:refseq:NM_005921;urn:miriam:ensembl:ENSG00000095015;urn:miriam:uniprot:Q13233;urn:miriam:uniprot:Q13233;urn:miriam:hgnc:6848;urn:miriam:hgnc.symbol:MAP3K1;urn:miriam:hgnc.symbol:MAP3K1;urn:miriam:ncbigene:4214;urn:miriam:ncbigene:4214;urn:miriam:ec-code:2.7.11.25;urn:miriam:refseq:NM_001291958;urn:miriam:hgnc:6856;urn:miriam:hgnc.symbol:MAP3K4;urn:miriam:hgnc.symbol:MAP3K4;urn:miriam:uniprot:Q9Y6R4;urn:miriam:uniprot:Q9Y6R4;urn:miriam:ncbigene:4216;urn:miriam:ncbigene:4216;urn:miriam:ensembl:ENSG00000085511;urn:miriam:ec-code:2.7.11.25"
      hgnc "HGNC_SYMBOL:MAP3K1;HGNC_SYMBOL:MAP3K4"
      map_id "M110_2"
      name "MEKK1_slash_4"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa11"
      uniprot "UNIPROT:Q13233;UNIPROT:Q9Y6R4"
    ]
    graphics [
      x 2958.7805189192004
      y 2349.433992765119
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:refseq:NM_005921;urn:miriam:ensembl:ENSG00000095015;urn:miriam:uniprot:Q13233;urn:miriam:uniprot:Q13233;urn:miriam:hgnc:6848;urn:miriam:hgnc.symbol:MAP3K1;urn:miriam:hgnc.symbol:MAP3K1;urn:miriam:ncbigene:4214;urn:miriam:ncbigene:4214;urn:miriam:ec-code:2.7.11.25;urn:miriam:refseq:NM_001291958;urn:miriam:hgnc:6856;urn:miriam:hgnc.symbol:MAP3K4;urn:miriam:hgnc.symbol:MAP3K4;urn:miriam:uniprot:Q9Y6R4;urn:miriam:uniprot:Q9Y6R4;urn:miriam:ncbigene:4216;urn:miriam:ncbigene:4216;urn:miriam:ensembl:ENSG00000085511;urn:miriam:ec-code:2.7.11.25"
      hgnc "HGNC_SYMBOL:MAP3K1;HGNC_SYMBOL:MAP3K4"
      map_id "M110_5"
      name "MEKK1_slash_4"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa3"
      uniprot "UNIPROT:Q13233;UNIPROT:Q9Y6R4"
    ]
    graphics [
      x 1781.043215136001
      y 2612.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:ec-code:2.7.12.2;urn:miriam:hgnc:6844;urn:miriam:ensembl:ENSG00000065559;urn:miriam:refseq:NM_001281435;urn:miriam:uniprot:P45985;urn:miriam:uniprot:P45985;urn:miriam:hgnc.symbol:MAP2K4;urn:miriam:hgnc.symbol:MAP2K4;urn:miriam:ncbigene:6416"
      hgnc "HGNC_SYMBOL:MAP2K4"
      map_id "M110_35"
      name "MAP2K4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa69"
      uniprot "UNIPROT:P45985"
    ]
    graphics [
      x 1562.5
      y 1785.4971941397312
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:JNK pathway"
      full_annotation "urn:miriam:ncbigene:1489668;urn:miriam:uniprot:P59594;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "M110_41"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa78"
      uniprot "UNIPROT:P59594"
    ]
    graphics [
      x 2275.842963598534
      y 2829.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M110_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 42
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "CONSPUMPTION"
      source_id "M110_37"
      target_id "M110_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 43
    source 3
    target 2
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M110_1"
      target_id "M110_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 44
    source 2
    target 4
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PRODUCTION"
      source_id "M110_14"
      target_id "M110_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 45
    source 5
    target 3
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PRODUCTION"
      source_id "M110_22"
      target_id "M110_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 46
    source 6
    target 3
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PRODUCTION"
      source_id "M110_21"
      target_id "M110_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 47
    source 3
    target 7
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M110_1"
      target_id "M110_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 48
    source 3
    target 8
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M110_1"
      target_id "M110_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 49
    source 3
    target 9
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M110_1"
      target_id "M110_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 50
    source 25
    target 5
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "CONSPUMPTION"
      source_id "M110_3"
      target_id "M110_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 51
    source 41
    target 5
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M110_41"
      target_id "M110_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 52
    source 25
    target 6
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "CONSPUMPTION"
      source_id "M110_3"
      target_id "M110_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 53
    source 26
    target 6
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M110_31"
      target_id "M110_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 54
    source 27
    target 6
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M110_33"
      target_id "M110_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 55
    source 28
    target 6
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M110_39"
      target_id "M110_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 56
    source 29
    target 6
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M110_40"
      target_id "M110_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 57
    source 19
    target 7
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "CONSPUMPTION"
      source_id "M110_34"
      target_id "M110_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 58
    source 7
    target 20
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PRODUCTION"
      source_id "M110_17"
      target_id "M110_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 59
    source 15
    target 8
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "CONSPUMPTION"
      source_id "M110_36"
      target_id "M110_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 60
    source 8
    target 16
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PRODUCTION"
      source_id "M110_15"
      target_id "M110_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 61
    source 10
    target 9
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "CONSPUMPTION"
      source_id "M110_9"
      target_id "M110_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 62
    source 11
    target 9
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M110_38"
      target_id "M110_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 63
    source 9
    target 12
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PRODUCTION"
      source_id "M110_18"
      target_id "M110_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 64
    source 12
    target 13
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "CONSPUMPTION"
      source_id "M110_6"
      target_id "M110_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 13
    target 14
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PRODUCTION"
      source_id "M110_12"
      target_id "M110_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 16
    target 17
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "CONSPUMPTION"
      source_id "M110_29"
      target_id "M110_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 17
    target 18
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PRODUCTION"
      source_id "M110_16"
      target_id "M110_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 20
    target 21
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "CONSPUMPTION"
      source_id "M110_24"
      target_id "M110_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 69
    source 20
    target 22
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "CONSPUMPTION"
      source_id "M110_24"
      target_id "M110_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 70
    source 21
    target 24
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PRODUCTION"
      source_id "M110_23"
      target_id "M110_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 71
    source 22
    target 23
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PRODUCTION"
      source_id "M110_10"
      target_id "M110_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 72
    source 34
    target 26
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PRODUCTION"
      source_id "M110_20"
      target_id "M110_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 73
    source 30
    target 27
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PRODUCTION"
      source_id "M110_11"
      target_id "M110_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 74
    source 31
    target 30
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "CONSPUMPTION"
      source_id "M110_32"
      target_id "M110_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 75
    source 32
    target 30
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M110_4"
      target_id "M110_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 76
    source 33
    target 30
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M110_7"
      target_id "M110_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 77
    source 36
    target 32
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PRODUCTION"
      source_id "M110_13"
      target_id "M110_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 78
    source 33
    target 34
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M110_7"
      target_id "M110_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 79
    source 33
    target 35
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M110_7"
      target_id "M110_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 33
    target 36
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M110_7"
      target_id "M110_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 81
    source 40
    target 34
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "CONSPUMPTION"
      source_id "M110_35"
      target_id "M110_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 82
    source 39
    target 34
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M110_5"
      target_id "M110_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 83
    source 38
    target 35
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "CONSPUMPTION"
      source_id "M110_2"
      target_id "M110_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 84
    source 35
    target 39
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "PRODUCTION"
      source_id "M110_19"
      target_id "M110_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 85
    source 37
    target 36
    cd19dm [
      diagram "C19DMap:JNK pathway"
      edge_type "CONSPUMPTION"
      source_id "M110_8"
      target_id "M110_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
