# generated with VANTED V2.8.2 at Fri Mar 04 10:04:34 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000133657;urn:miriam:uniprot:Q9H7F0;urn:miriam:hgnc.symbol:ATP13A3;urn:miriam:hgnc.symbol:ATP13A3;urn:miriam:hgnc:24113;urn:miriam:refseq:NM_024524;urn:miriam:ec-code:7.2.2.-;urn:miriam:ncbigene:79572;urn:miriam:ncbigene:79572"
      hgnc "HGNC_SYMBOL:ATP13A3"
      map_id "M14_116"
      name "ATP13A3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa222"
      uniprot "UNIPROT:Q9H7F0"
    ]
    graphics [
      x 2028.7805189192002
      y 2315.852697606676
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_78"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re66"
      uniprot "NA"
    ]
    graphics [
      x 2321.043215136001
      y 2529.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_31"
      name "P_minus_ATPase"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa80"
      uniprot "NA"
    ]
    graphics [
      x 2642.5
      y 1397.7682304796128
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000133657;urn:miriam:uniprot:Q9H7F0;urn:miriam:hgnc.symbol:ATP13A3;urn:miriam:hgnc.symbol:ATP13A3;urn:miriam:ec-code:7.6.2.16;urn:miriam:hgnc:24113;urn:miriam:refseq:NM_024524;urn:miriam:ncbigene:79572;urn:miriam:ncbigene:79572"
      hgnc "HGNC_SYMBOL:ATP13A3"
      map_id "M14_20"
      name "P_minus_ATPase"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa41"
      uniprot "UNIPROT:Q9H7F0"
    ]
    graphics [
      x 1884.6052101739144
      y 1202.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_51"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re26"
      uniprot "NA"
    ]
    graphics [
      x 3482.5
      y 1303.970196409145
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049"
      hgnc "NA"
      map_id "M14_129"
      name "Nsp6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa48"
      uniprot "NA"
    ]
    graphics [
      x 2612.5
      y 1128.7911939082426
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_129"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000133657;urn:miriam:uniprot:Q9H7F0;urn:miriam:hgnc.symbol:ATP13A3;urn:miriam:hgnc.symbol:ATP13A3;urn:miriam:ec-code:7.6.2.16;urn:miriam:hgnc:24113;urn:miriam:refseq:NM_024524;urn:miriam:ncbigene:79572;urn:miriam:ncbigene:79572;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049"
      hgnc "HGNC_SYMBOL:ATP13A3"
      map_id "M14_19"
      name "P_minus_ATPase:Nsp6"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa40"
      uniprot "UNIPROT:Q9H7F0"
    ]
    graphics [
      x 3392.5
      y 1331.8884485524843
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re22"
      uniprot "NA"
    ]
    graphics [
      x 2942.5
      y 1702.6505321512923
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_48"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re23"
      uniprot "NA"
    ]
    graphics [
      x 3092.5
      y 1270.1860833815003
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_50"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re25"
      uniprot "NA"
    ]
    graphics [
      x 1608.7805189192002
      y 2423.119912305107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_69"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re54"
      uniprot "NA"
    ]
    graphics [
      x 2552.5
      y 885.722260552458
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_55"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re31"
      uniprot "NA"
    ]
    graphics [
      x 1652.5
      y 1407.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_52"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re27"
      uniprot "NA"
    ]
    graphics [
      x 3002.5
      y 1966.4435918251672
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859;PUBMED:29128390"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_67"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re51"
      uniprot "NA"
    ]
    graphics [
      x 2042.5
      y 1955.8526976066757
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049;urn:miriam:uniprot:Nsp3"
      hgnc "NA"
      map_id "M14_102"
      name "Nsp3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa162"
      uniprot "UNIPROT:Nsp3"
    ]
    graphics [
      x 3332.5
      y 2195.0946501883805
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761"
      hgnc "NA"
      map_id "M14_127"
      name "Nsp4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa4"
      uniprot "NA"
    ]
    graphics [
      x 632.5
      y 1249.1566491701199
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_127"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:doi:10.1016/j.virol.2017.07.019;urn:miriam:taxonomy:694009;urn:miriam:pubmed:29128390;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049;urn:miriam:uniprot:Nsp3;urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761"
      hgnc "NA"
      map_id "M14_17"
      name "Nsp3:Nsp4:Nsp6"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa39"
      uniprot "UNIPROT:Nsp3"
    ]
    graphics [
      x 2058.7805189192004
      y 2285.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_77"
      name "NA"
      node_subtype "POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "re64"
      uniprot "NA"
    ]
    graphics [
      x 902.5
      y 974.7914102512494
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:obo.go:GO%3A0007009;urn:miriam:taxonomy:694009;urn:miriam:pubmed:23943763"
      hgnc "NA"
      map_id "M14_104"
      name "Plasma_space_membrane_space_organization"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa164"
      uniprot "NA"
    ]
    graphics [
      x 1742.5
      y 657.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_44"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re2"
      uniprot "NA"
    ]
    graphics [
      x 1262.5
      y 1746.9780694277015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_94"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re9"
      uniprot "NA"
    ]
    graphics [
      x 2552.5
      y 1854.576720665756
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_73"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re6"
      uniprot "NA"
    ]
    graphics [
      x 1652.5
      y 1017.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_76"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re63"
      uniprot "NA"
    ]
    graphics [
      x 2075.815548054559
      y 665.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_40"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re16"
      uniprot "NA"
    ]
    graphics [
      x 1682.5
      y 1227.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re3"
      uniprot "NA"
    ]
    graphics [
      x 302.5
      y 1819.7372786211824
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_42"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re18"
      uniprot "NA"
    ]
    graphics [
      x 2732.5
      y 1701.5461143635134
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:hgnc:32456;urn:miriam:ec-code:2.4.1.131;urn:miriam:ncbigene:440138;urn:miriam:ncbigene:440138;urn:miriam:uniprot:Q2TAA5;urn:miriam:hgnc.symbol:ALG11;urn:miriam:hgnc.symbol:ALG11;urn:miriam:pubmed:20080937;urn:miriam:refseq:NM_001004127;urn:miriam:ensembl:ENSG00000253710"
      hgnc "HGNC_SYMBOL:ALG11"
      map_id "M14_96"
      name "ALG11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa14"
      uniprot "UNIPROT:Q2TAA5"
    ]
    graphics [
      x 1442.5
      y 1848.065842890646
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:20080937;urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761;urn:miriam:hgnc:32456;urn:miriam:ec-code:2.4.1.131;urn:miriam:ncbigene:440138;urn:miriam:ncbigene:440138;urn:miriam:uniprot:Q2TAA5;urn:miriam:hgnc.symbol:ALG11;urn:miriam:hgnc.symbol:ALG11;urn:miriam:pubmed:20080937;urn:miriam:refseq:NM_001004127;urn:miriam:ensembl:ENSG00000253710"
      hgnc "HGNC_SYMBOL:ALG11"
      map_id "M14_6"
      name "Nsp4_underscore_ALG11"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa15"
      uniprot "UNIPROT:Q2TAA5"
    ]
    graphics [
      x 1518.7805189192002
      y 2489.738142841986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_86"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re76"
      uniprot "NA"
    ]
    graphics [
      x 782.5
      y 1670.6643004405282
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:20080937;urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761;urn:miriam:hgnc:32456;urn:miriam:ec-code:2.4.1.131;urn:miriam:ncbigene:440138;urn:miriam:ncbigene:440138;urn:miriam:uniprot:Q2TAA5;urn:miriam:hgnc.symbol:ALG11;urn:miriam:hgnc.symbol:ALG11;urn:miriam:pubmed:20080937;urn:miriam:refseq:NM_001004127;urn:miriam:ensembl:ENSG00000253710"
      hgnc "HGNC_SYMBOL:ALG11"
      map_id "M14_35"
      name "Nsp4_underscore_ALG11"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa93"
      uniprot "UNIPROT:Q2TAA5"
    ]
    graphics [
      x 2432.5
      y 1646.0089009888093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_87"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re77"
      uniprot "NA"
    ]
    graphics [
      x 422.5
      y 1880.9830489895012
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:hgnc:32456;urn:miriam:ec-code:2.4.1.131;urn:miriam:ncbigene:440138;urn:miriam:ncbigene:440138;urn:miriam:uniprot:Q2TAA5;urn:miriam:hgnc.symbol:ALG11;urn:miriam:hgnc.symbol:ALG11;urn:miriam:pubmed:20080937;urn:miriam:refseq:NM_001004127;urn:miriam:ensembl:ENSG00000253710"
      hgnc "HGNC_SYMBOL:ALG11"
      map_id "M14_122"
      name "ALG11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa230"
      uniprot "UNIPROT:Q2TAA5"
    ]
    graphics [
      x 572.5
      y 2195.6317693644205
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_131"
      name "sa5_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa5"
      uniprot "NA"
    ]
    graphics [
      x 662.5
      y 1111.6414689325857
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_131"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:uniprot:Q9NVH1;urn:miriam:hgnc:25570;urn:miriam:ncbigene:55735;urn:miriam:ncbigene:55735;urn:miriam:pubmed:32353859;urn:miriam:pubmed:25997101;urn:miriam:ensembl:ENSG00000007923;urn:miriam:hgnc.symbol:DNAJC11;urn:miriam:refseq:NM_018198;urn:miriam:hgnc.symbol:DNAJC11"
      hgnc "HGNC_SYMBOL:DNAJC11"
      map_id "M14_95"
      name "DNAJC11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa13"
      uniprot "UNIPROT:Q9NVH1"
    ]
    graphics [
      x 1232.5
      y 1902.9973497539163
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:pubmed:25997101;urn:miriam:uniprot:Q9NVH1;urn:miriam:hgnc:25570;urn:miriam:ncbigene:55735;urn:miriam:ncbigene:55735;urn:miriam:pubmed:32353859;urn:miriam:pubmed:25997101;urn:miriam:ensembl:ENSG00000007923;urn:miriam:hgnc.symbol:DNAJC11;urn:miriam:refseq:NM_018198;urn:miriam:hgnc.symbol:DNAJC11;urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761"
      hgnc "HGNC_SYMBOL:DNAJC11"
      map_id "M14_3"
      name "Nsp4:DNAJC11"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa12"
      uniprot "UNIPROT:Q9NVH1"
    ]
    graphics [
      x 1682.5
      y 1707.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:uniprot:Q8N4H5;urn:miriam:uniprot:Q8N4H5;urn:miriam:hgnc:31369;urn:miriam:refseq:NM_001001790;urn:miriam:ensembl:ENSG00000175768;urn:miriam:hgnc.symbol:TOMM5;urn:miriam:hgnc.symbol:TOMM5;urn:miriam:ncbigene:401505;urn:miriam:ncbigene:401505;urn:miriam:ncbigene:10452;urn:miriam:ensembl:ENSG00000130204;urn:miriam:ncbigene:10452;urn:miriam:refseq:NM_001128916;urn:miriam:uniprot:O96008;urn:miriam:uniprot:O96008;urn:miriam:hgnc.symbol:TOMM40;urn:miriam:hgnc.symbol:TOMM40;urn:miriam:hgnc:18001;urn:miriam:ensembl:ENSG00000173726;urn:miriam:refseq:NM_014765;urn:miriam:uniprot:Q15388;urn:miriam:uniprot:Q15388;urn:miriam:ncbigene:9804;urn:miriam:ncbigene:9804;urn:miriam:hgnc.symbol:TOMM20;urn:miriam:hgnc.symbol:TOMM20;urn:miriam:hgnc:20947;urn:miriam:refseq:NM_020243;urn:miriam:uniprot:Q9NS69;urn:miriam:uniprot:Q9NS69;urn:miriam:ncbigene:56993;urn:miriam:ncbigene:56993;urn:miriam:hgnc:18002;urn:miriam:ensembl:ENSG00000100216;urn:miriam:hgnc.symbol:TOMM22;urn:miriam:hgnc.symbol:TOMM22;urn:miriam:refseq:NM_014820;urn:miriam:uniprot:O94826;urn:miriam:uniprot:O94826;urn:miriam:hgnc:11985;urn:miriam:hgnc.symbol:TOMM70;urn:miriam:hgnc.symbol:TOMM70;urn:miriam:ncbigene:9868;urn:miriam:ncbigene:9868;urn:miriam:ensembl:ENSG00000154174;urn:miriam:hgnc:21648;urn:miriam:hgnc.symbol:TOMM7;urn:miriam:hgnc.symbol:TOMM7;urn:miriam:refseq:NM_019059;urn:miriam:ncbigene:54543;urn:miriam:ncbigene:54543;urn:miriam:uniprot:Q9P0U1;urn:miriam:uniprot:Q9P0U1;urn:miriam:ensembl:ENSG00000196683"
      hgnc "HGNC_SYMBOL:TOMM5;HGNC_SYMBOL:TOMM40;HGNC_SYMBOL:TOMM20;HGNC_SYMBOL:TOMM22;HGNC_SYMBOL:TOMM70;HGNC_SYMBOL:TOMM7"
      map_id "M14_10"
      name "TOM_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa24"
      uniprot "UNIPROT:Q8N4H5;UNIPROT:O96008;UNIPROT:Q15388;UNIPROT:Q9NS69;UNIPROT:O94826;UNIPROT:Q9P0U1"
    ]
    graphics [
      x 3002.5
      y 1615.0031419964191
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761"
      hgnc "NA"
      map_id "M14_112"
      name "Nsp4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa217"
      uniprot "NA"
    ]
    graphics [
      x 2822.5
      y 1265.985912633978
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_58"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re37"
      uniprot "NA"
    ]
    graphics [
      x 3332.5
      y 1383.4293560947754
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000142444;urn:miriam:hgnc:25152;urn:miriam:hgnc.symbol:TIMM29;urn:miriam:hgnc.symbol:TIMM29;urn:miriam:refseq:NM_138358;urn:miriam:uniprot:Q9BSF4;urn:miriam:uniprot:Q9BSF4;urn:miriam:ncbigene:90580;urn:miriam:ncbigene:90580;urn:miriam:hgnc:4022;urn:miriam:uniprot:Q9Y5J6;urn:miriam:uniprot:Q9Y5J6;urn:miriam:hgnc.symbol:TIMM10B;urn:miriam:hgnc.symbol:TIMM10B;urn:miriam:ensembl:ENSG00000132286;urn:miriam:ncbigene:26515;urn:miriam:ncbigene:26515;urn:miriam:refseq:NM_012192;urn:miriam:uniprot:Q9Y584;urn:miriam:uniprot:Q9Y584;urn:miriam:hgnc.symbol:TIMM22;urn:miriam:hgnc.symbol:TIMM22;urn:miriam:hgnc:17317;urn:miriam:ncbigene:29928;urn:miriam:ncbigene:29928;urn:miriam:ensembl:ENSG00000177370;urn:miriam:refseq:NM_013337;urn:miriam:hgnc:11814;urn:miriam:uniprot:P62072;urn:miriam:uniprot:P62072;urn:miriam:ncbigene:26519;urn:miriam:ncbigene:26519;urn:miriam:ensembl:ENSG00000134809;urn:miriam:hgnc.symbol:TIMM10;urn:miriam:hgnc.symbol:TIMM10;urn:miriam:refseq:NM_012456;urn:miriam:refseq:NM_001304485;urn:miriam:ncbigene:26520;urn:miriam:ncbigene:26520;urn:miriam:hgnc.symbol:TIMM9;urn:miriam:ensembl:ENSG00000100575;urn:miriam:hgnc.symbol:TIMM9;urn:miriam:hgnc:11819;urn:miriam:uniprot:Q9Y5J7;urn:miriam:uniprot:Q9Y5J7"
      hgnc "HGNC_SYMBOL:TIMM29;HGNC_SYMBOL:TIMM10B;HGNC_SYMBOL:TIMM22;HGNC_SYMBOL:TIMM10;HGNC_SYMBOL:TIMM9"
      map_id "M14_12"
      name "TIM_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa29"
      uniprot "UNIPROT:Q9BSF4;UNIPROT:Q9Y5J6;UNIPROT:Q9Y584;UNIPROT:P62072;UNIPROT:Q9Y5J7"
    ]
    graphics [
      x 3512.5
      y 1511.3795629191009
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:uniprot:Q9Y584;urn:miriam:uniprot:Q9Y584;urn:miriam:hgnc.symbol:TIMM22;urn:miriam:hgnc.symbol:TIMM22;urn:miriam:hgnc:17317;urn:miriam:ncbigene:29928;urn:miriam:ncbigene:29928;urn:miriam:ensembl:ENSG00000177370;urn:miriam:refseq:NM_013337;urn:miriam:hgnc:4022;urn:miriam:uniprot:Q9Y5J6;urn:miriam:uniprot:Q9Y5J6;urn:miriam:hgnc.symbol:TIMM10B;urn:miriam:hgnc.symbol:TIMM10B;urn:miriam:ensembl:ENSG00000132286;urn:miriam:ncbigene:26515;urn:miriam:ncbigene:26515;urn:miriam:refseq:NM_012192;urn:miriam:ensembl:ENSG00000142444;urn:miriam:hgnc:25152;urn:miriam:hgnc.symbol:TIMM29;urn:miriam:hgnc.symbol:TIMM29;urn:miriam:refseq:NM_138358;urn:miriam:uniprot:Q9BSF4;urn:miriam:uniprot:Q9BSF4;urn:miriam:ncbigene:90580;urn:miriam:ncbigene:90580;urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761;urn:miriam:hgnc:11814;urn:miriam:uniprot:P62072;urn:miriam:uniprot:P62072;urn:miriam:ncbigene:26519;urn:miriam:ncbigene:26519;urn:miriam:ensembl:ENSG00000134809;urn:miriam:hgnc.symbol:TIMM10;urn:miriam:hgnc.symbol:TIMM10;urn:miriam:refseq:NM_012456;urn:miriam:refseq:NM_001304485;urn:miriam:ncbigene:26520;urn:miriam:ncbigene:26520;urn:miriam:hgnc.symbol:TIMM9;urn:miriam:ensembl:ENSG00000100575;urn:miriam:hgnc.symbol:TIMM9;urn:miriam:hgnc:11819;urn:miriam:uniprot:Q9Y5J7;urn:miriam:uniprot:Q9Y5J7"
      hgnc "HGNC_SYMBOL:TIMM22;HGNC_SYMBOL:TIMM10B;HGNC_SYMBOL:TIMM29;HGNC_SYMBOL:TIMM10;HGNC_SYMBOL:TIMM9"
      map_id "M14_11"
      name "TIM_space_complex:Nsp4"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa27"
      uniprot "UNIPROT:Q9Y584;UNIPROT:Q9Y5J6;UNIPROT:Q9BSF4;UNIPROT:P62072;UNIPROT:Q9Y5J7"
    ]
    graphics [
      x 1292.5
      y 1583.3146446645712
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:uniprot:P14735;urn:miriam:ncbigene:3416;urn:miriam:ncbigene:3416;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1210/mend-4-8-1125;urn:miriam:hgnc:5381;urn:miriam:refseq:NM_004969;urn:miriam:ec-code:3.4.24.56;urn:miriam:ensembl:ENSG00000119912;urn:miriam:hgnc.symbol:IDE;urn:miriam:hgnc.symbol:IDE"
      hgnc "HGNC_SYMBOL:IDE"
      map_id "M14_136"
      name "IDE"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa8"
      uniprot "UNIPROT:P14735"
    ]
    graphics [
      x 2162.5
      y 1433.7133098641311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:taxonomy:10116;urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761;urn:miriam:uniprot:P14735;urn:miriam:ncbigene:3416;urn:miriam:ncbigene:3416;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1210/mend-4-8-1125;urn:miriam:taxonomy:10116;urn:miriam:hgnc:5381;urn:miriam:refseq:NM_004969;urn:miriam:ec-code:3.4.24.56;urn:miriam:ensembl:ENSG00000119912;urn:miriam:hgnc.symbol:IDE;urn:miriam:hgnc.symbol:IDE"
      hgnc "HGNC_SYMBOL:IDE"
      map_id "M14_9"
      name "Nsp4:IDE"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa2"
      uniprot "UNIPROT:P14735"
    ]
    graphics [
      x 2822.5
      y 2101.553293828624
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_39"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10"
      uniprot "NA"
    ]
    graphics [
      x 2411.043215136001
      y 2726.0089009888093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_80"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re7"
      uniprot "NA"
    ]
    graphics [
      x 3152.5
      y 1854.3536941863422
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:taxonomy:10116;urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761;urn:miriam:uniprot:P14735;urn:miriam:ncbigene:3416;urn:miriam:ncbigene:3416;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1210/mend-4-8-1125;urn:miriam:taxonomy:10116;urn:miriam:hgnc:5381;urn:miriam:refseq:NM_004969;urn:miriam:ec-code:3.4.24.56;urn:miriam:ensembl:ENSG00000119912;urn:miriam:hgnc.symbol:IDE;urn:miriam:hgnc.symbol:IDE"
      hgnc "HGNC_SYMBOL:IDE"
      map_id "M14_18"
      name "Nsp4:IDE"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa4"
      uniprot "UNIPROT:P14735"
    ]
    graphics [
      x 2471.043215136001
      y 2491.435024234829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761;urn:miriam:uniprot:P14735;urn:miriam:ncbigene:3416;urn:miriam:ncbigene:3416;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1210/mend-4-8-1125;urn:miriam:taxonomy:10116;urn:miriam:hgnc:5381;urn:miriam:refseq:NM_004969;urn:miriam:ec-code:3.4.24.56;urn:miriam:ensembl:ENSG00000119912;urn:miriam:hgnc.symbol:IDE;urn:miriam:hgnc.symbol:IDE"
      hgnc "HGNC_SYMBOL:IDE"
      map_id "M14_28"
      name "Nsp4_underscore_IDE"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa5"
      uniprot "UNIPROT:P14735"
    ]
    graphics [
      x 2882.5
      y 1983.858034494286
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "PUBMED:9830016"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_60"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re40"
      uniprot "NA"
    ]
    graphics [
      x 2192.5
      y 1703.7133098641311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_82"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re72"
      uniprot "NA"
    ]
    graphics [
      x 1712.5
      y 1197.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:uniprot:P14735;urn:miriam:ncbigene:3416;urn:miriam:ncbigene:3416;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1210/mend-4-8-1125;urn:miriam:hgnc:5381;urn:miriam:refseq:NM_004969;urn:miriam:ec-code:3.4.24.56;urn:miriam:ensembl:ENSG00000119912;urn:miriam:hgnc.symbol:IDE;urn:miriam:hgnc.symbol:IDE"
      hgnc "HGNC_SYMBOL:IDE"
      map_id "M14_119"
      name "IDE"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa225"
      uniprot "UNIPROT:P14735"
    ]
    graphics [
      x 1652.5
      y 987.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:uniprot:P14735;urn:miriam:ncbigene:3416;urn:miriam:ncbigene:3416;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1210/mend-4-8-1125;urn:miriam:taxonomy:10116;urn:miriam:hgnc:5381;urn:miriam:refseq:NM_004969;urn:miriam:ec-code:3.4.24.56;urn:miriam:ensembl:ENSG00000119912;urn:miriam:hgnc.symbol:IDE;urn:miriam:hgnc.symbol:IDE"
      hgnc "HGNC_SYMBOL:IDE"
      map_id "M14_97"
      name "IDE"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa154"
      uniprot "UNIPROT:P14735"
    ]
    graphics [
      x 962.5
      y 1839.3153383642393
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:NUP210;urn:miriam:hgnc.symbol:NUP210;urn:miriam:pubmed:14517331;urn:miriam:uniprot:Q8TEM1;urn:miriam:ncbigene:23225;urn:miriam:ensembl:ENSG00000132182;urn:miriam:ncbigene:23225;urn:miriam:hgnc:30052;urn:miriam:refseq:NM_024923"
      hgnc "HGNC_SYMBOL:NUP210"
      map_id "M14_137"
      name "NUP210"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa9"
      uniprot "UNIPROT:Q8TEM1"
    ]
    graphics [
      x 2612.5
      y 937.9221021880783
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_137"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:14517331;urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761;urn:miriam:hgnc.symbol:NUP210;urn:miriam:hgnc.symbol:NUP210;urn:miriam:pubmed:14517331;urn:miriam:uniprot:Q8TEM1;urn:miriam:ncbigene:23225;urn:miriam:ensembl:ENSG00000132182;urn:miriam:ncbigene:23225;urn:miriam:hgnc:30052;urn:miriam:refseq:NM_024923"
      hgnc "HGNC_SYMBOL:NUP210"
      map_id "M14_5"
      name "Nsp4:NUP210"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa14"
      uniprot "UNIPROT:Q8TEM1"
    ]
    graphics [
      x 1814.292827536552
      y 2822.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "PUBMED:14517331"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_41"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re17"
      uniprot "NA"
    ]
    graphics [
      x 2162.5
      y 1313.7133098641311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:14517331;urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761;urn:miriam:hgnc.symbol:NUP210;urn:miriam:hgnc.symbol:NUP210;urn:miriam:pubmed:14517331;urn:miriam:uniprot:Q8TEM1;urn:miriam:ncbigene:23225;urn:miriam:ensembl:ENSG00000132182;urn:miriam:ncbigene:23225;urn:miriam:hgnc:30052;urn:miriam:refseq:NM_024923"
      hgnc "HGNC_SYMBOL:NUP210"
      map_id "M14_4"
      name "Nsp4:NUP210"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa13"
      uniprot "UNIPROT:Q8TEM1"
    ]
    graphics [
      x 1292.5
      y 983.3146446645712
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_68"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re53"
      uniprot "NA"
    ]
    graphics [
      x 2200.913546386292
      y 1013.7133098641311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "PUBMED:14517331"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_81"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re71"
      uniprot "NA"
    ]
    graphics [
      x 2012.5
      y 812.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:NUP210;urn:miriam:hgnc.symbol:NUP210;urn:miriam:pubmed:14517331;urn:miriam:uniprot:Q8TEM1;urn:miriam:ncbigene:23225;urn:miriam:ensembl:ENSG00000132182;urn:miriam:ncbigene:23225;urn:miriam:hgnc:30052;urn:miriam:refseq:NM_024923"
      hgnc "HGNC_SYMBOL:NUP210"
      map_id "M14_118"
      name "NUP210"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa224"
      uniprot "UNIPROT:Q8TEM1"
    ]
    graphics [
      x 2882.5
      y 1623.858034494286
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859"
      hgnc "NA"
      map_id "M14_105"
      name "Selinexor"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa187"
      uniprot "NA"
    ]
    graphics [
      x 1849.1891006464293
      y 242.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:hgnc.symbol:NUP210;urn:miriam:hgnc.symbol:NUP210;urn:miriam:pubmed:14517331;urn:miriam:uniprot:Q8TEM1;urn:miriam:ncbigene:23225;urn:miriam:ensembl:ENSG00000132182;urn:miriam:ncbigene:23225;urn:miriam:hgnc:30052;urn:miriam:refseq:NM_024923;urn:miriam:pubmed:32353859"
      hgnc "HGNC_SYMBOL:NUP210"
      map_id "M14_23"
      name "NUP210:Selinexor"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa44"
      uniprot "UNIPROT:Q8TEM1"
    ]
    graphics [
      x 2328.7805189192004
      y 2259.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859"
      hgnc "NA"
      map_id "M14_126"
      name "Nsp4_space_(_plus_)"
      node_subtype "RNA"
      node_type "species"
      org_id "sa3"
      uniprot "NA"
    ]
    graphics [
      x 1728.7805189192002
      y 2397.1855687946227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_38"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1"
      uniprot "NA"
    ]
    graphics [
      x 902.5
      y 1124.7914102512495
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_43"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re19"
      uniprot "NA"
    ]
    graphics [
      x 2252.5
      y 1742.4583143700918
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_59"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re4"
      uniprot "NA"
    ]
    graphics [
      x 678.7805189192002
      y 2495.7587894764206
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_135"
      name "sa2_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa6"
      uniprot "NA"
    ]
    graphics [
      x 872.5
      y 1754.3683906431766
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859"
      hgnc "NA"
      map_id "M14_115"
      name "Nsp4_space_(_minus_)"
      node_subtype "RNA"
      node_type "species"
      org_id "sa220"
      uniprot "NA"
    ]
    graphics [
      x 2102.5
      y 905.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re20"
      uniprot "NA"
    ]
    graphics [
      x 2178.7805189192004
      y 2363.713309864131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_99"
      name "sa3_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa16"
      uniprot "NA"
    ]
    graphics [
      x 1458.7805189192002
      y 2372.184438344133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_63"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re47"
      uniprot "NA"
    ]
    graphics [
      x 1833.6439781816332
      y 2222.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_66"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re50"
      uniprot "NA"
    ]
    graphics [
      x 3482.5
      y 2055.273613704569
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_103"
      name "sa171_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa163"
      uniprot "NA"
    ]
    graphics [
      x 2522.5
      y 1502.9715035857598
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859"
      hgnc "NA"
      map_id "M14_100"
      name "Nsp3_space_(_plus_)"
      node_subtype "RNA"
      node_type "species"
      org_id "sa160"
      uniprot "NA"
    ]
    graphics [
      x 1802.5
      y 1382.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_61"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re45"
      uniprot "NA"
    ]
    graphics [
      x 1412.5
      y 1372.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_65"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re49"
      uniprot "NA"
    ]
    graphics [
      x 2942.5
      y 1252.6505321512923
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_62"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re46"
      uniprot "NA"
    ]
    graphics [
      x 2492.5
      y 1261.435024234829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859"
      hgnc "NA"
      map_id "M14_114"
      name "Nsp3_space_(_minus_)"
      node_subtype "RNA"
      node_type "species"
      org_id "sa219"
      uniprot "NA"
    ]
    graphics [
      x 1652.5
      y 1647.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_64"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re48"
      uniprot "NA"
    ]
    graphics [
      x 2552.5
      y 718.8179889343239
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_98"
      name "sa169_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa159"
      uniprot "NA"
    ]
    graphics [
      x 2617.9989242256197
      y 455.83149007527027
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_101"
      name "sa170_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa161"
      uniprot "NA"
    ]
    graphics [
      x 2822.5
      y 871.7380400148135
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:10406945;urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1074/jbc.272.43.27107;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:uniprot:Q99720"
      hgnc "HGNC_SYMBOL:SIGMAR1"
      map_id "M14_133"
      name "SIGMAR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa53"
      uniprot "UNIPROT:Q99720"
    ]
    graphics [
      x 2282.5
      y 1416.211238590613
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_133"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:pubmed:10406945;urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1074/jbc.272.43.27107;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:uniprot:Q99720;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049"
      hgnc "HGNC_SYMBOL:SIGMAR1"
      map_id "M14_7"
      name "Nsp6:SIGMAR1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa18"
      uniprot "UNIPROT:Q99720"
    ]
    graphics [
      x 3122.5
      y 2074.0724790427703
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      annotation "PUBMED:10406945"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_53"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re28"
      uniprot "NA"
    ]
    graphics [
      x 3482.5
      y 1729.7870947233264
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      annotation "PUBMED:10406945"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_85"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re75"
      uniprot "NA"
    ]
    graphics [
      x 3362.5
      y 2234.6666469704237
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:10406945;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049;urn:miriam:pubmed:10406945;urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1074/jbc.272.43.27107;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:uniprot:Q99720"
      hgnc "HGNC_SYMBOL:SIGMAR1"
      map_id "M14_34"
      name "Nsp6:SIGMAR1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa92"
      uniprot "UNIPROT:Q99720"
    ]
    graphics [
      x 2792.5
      y 1762.3725415703839
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:10406945;urn:miriam:pubmed:10406945;urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1074/jbc.272.43.27107;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:uniprot:Q99720;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049"
      hgnc "HGNC_SYMBOL:SIGMAR1"
      map_id "M14_8"
      name "Nsp6:SIGMAR1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa19"
      uniprot "UNIPROT:Q99720"
    ]
    graphics [
      x 2838.7805189192004
      y 2212.7169405554178
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      annotation "PUBMED:10406945"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_83"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re73"
      uniprot "NA"
    ]
    graphics [
      x 3092.5
      y 1210.1860833815003
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      annotation "PUBMED:10406945"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_84"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re74"
      uniprot "NA"
    ]
    graphics [
      x 2222.5
      y 1376.7158381002334
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_75"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re62"
      uniprot "NA"
    ]
    graphics [
      x 3272.5
      y 1189.9470895919978
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_111"
      name "Several_space_drugs"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa212"
      uniprot "NA"
    ]
    graphics [
      x 2762.5
      y 1381.2535183076814
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:pubmed:10406945;urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1074/jbc.272.43.27107;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:uniprot:Q99720"
      hgnc "HGNC_SYMBOL:SIGMAR1"
      map_id "M14_30"
      name "SIGMAR1:Drugs"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa53"
      uniprot "UNIPROT:Q99720"
    ]
    graphics [
      x 1802.5
      y 1352.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:10406945;urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1074/jbc.272.43.27107;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:uniprot:Q99720"
      hgnc "HGNC_SYMBOL:SIGMAR1"
      map_id "M14_121"
      name "SIGMAR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa227"
      uniprot "UNIPROT:Q99720"
    ]
    graphics [
      x 1922.5
      y 1022.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:10406945;urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1074/jbc.272.43.27107;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:uniprot:Q99720"
      hgnc "HGNC_SYMBOL:SIGMAR1"
      map_id "M14_120"
      name "SIGMAR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa226"
      uniprot "UNIPROT:Q99720"
    ]
    graphics [
      x 1930.9135463862924
      y 842.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:uniprot:Q15904;urn:miriam:ncbigene:537;urn:miriam:ensembl:ENSG00000071553;urn:miriam:ncbigene:537;urn:miriam:hgnc:868;urn:miriam:refseq:NM_001183;urn:miriam:uniprot:Q15904;urn:miriam:pubmed:27231034;urn:miriam:hgnc.symbol:ATP6AP1;urn:miriam:hgnc.symbol:ATP6AP1"
      hgnc "HGNC_SYMBOL:ATP6AP1"
      map_id "M14_13"
      name "V_minus_ATPase"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa30"
      uniprot "UNIPROT:Q15904"
    ]
    graphics [
      x 1668.7805189192002
      y 2217.1855687946227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:uniprot:Q15904;urn:miriam:ncbigene:537;urn:miriam:ensembl:ENSG00000071553;urn:miriam:ncbigene:537;urn:miriam:hgnc:868;urn:miriam:refseq:NM_001183;urn:miriam:uniprot:Q15904;urn:miriam:pubmed:27231034;urn:miriam:hgnc.symbol:ATP6AP1;urn:miriam:hgnc.symbol:ATP6AP1;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049"
      hgnc "HGNC_SYMBOL:ATP6AP1"
      map_id "M14_14"
      name "V_minus_ATPase:Nsp6"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa32"
      uniprot "UNIPROT:Q15904"
    ]
    graphics [
      x 1442.5
      y 1235.1961954122032
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_91"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re81"
      uniprot "NA"
    ]
    graphics [
      x 2381.043215136001
      y 2602.060834871624
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:uniprot:Q15904;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049;urn:miriam:ncbigene:537;urn:miriam:ensembl:ENSG00000071553;urn:miriam:ncbigene:537;urn:miriam:hgnc:868;urn:miriam:refseq:NM_001183;urn:miriam:uniprot:Q15904;urn:miriam:pubmed:27231034;urn:miriam:hgnc.symbol:ATP6AP1;urn:miriam:hgnc.symbol:ATP6AP1"
      hgnc "HGNC_SYMBOL:ATP6AP1"
      map_id "M14_16"
      name "V_minus_ATPase:Nsp6"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa34"
      uniprot "UNIPROT:Q15904"
    ]
    graphics [
      x 2988.7805189192004
      y 2354.936290989684
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_89"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re79"
      uniprot "NA"
    ]
    graphics [
      x 1987.6260314874903
      y 3182.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_90"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re80"
      uniprot "NA"
    ]
    graphics [
      x 2531.043215136001
      y 2642.97150358576
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:uniprot:Q15904;urn:miriam:ncbigene:537;urn:miriam:ensembl:ENSG00000071553;urn:miriam:ncbigene:537;urn:miriam:hgnc:868;urn:miriam:refseq:NM_001183;urn:miriam:uniprot:Q15904;urn:miriam:pubmed:27231034;urn:miriam:hgnc.symbol:ATP6AP1;urn:miriam:hgnc.symbol:ATP6AP1"
      hgnc "HGNC_SYMBOL:ATP6AP1"
      map_id "M14_15"
      name "V_minus_ATPase"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa33"
      uniprot "UNIPROT:Q15904"
    ]
    graphics [
      x 1816.955934719773
      y 2522.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:obo.go:GO%3A0046611"
      hgnc "NA"
      map_id "M14_36"
      name "V_minus_type_space_proton_space_ATPase"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa94"
      uniprot "NA"
    ]
    graphics [
      x 752.5
      y 1898.3665515504927
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:ncbigene:537;urn:miriam:ensembl:ENSG00000071553;urn:miriam:ncbigene:537;urn:miriam:hgnc:868;urn:miriam:refseq:NM_001183;urn:miriam:uniprot:Q15904;urn:miriam:pubmed:27231034;urn:miriam:hgnc.symbol:ATP6AP1;urn:miriam:hgnc.symbol:ATP6AP1"
      hgnc "HGNC_SYMBOL:ATP6AP1"
      map_id "M14_117"
      name "ATP6AP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa223"
      uniprot "UNIPROT:Q15904"
    ]
    graphics [
      x 692.5
      y 1870.7442248293596
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_74"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re60"
      uniprot "NA"
    ]
    graphics [
      x 1592.5
      y 833.1199123051069
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_106"
      name "Bafilomycin_space_A1"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa188"
      uniprot "NA"
    ]
    graphics [
      x 1142.5
      y 1194.9910422829782
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:ncbigene:537;urn:miriam:ensembl:ENSG00000071553;urn:miriam:ncbigene:537;urn:miriam:hgnc:868;urn:miriam:refseq:NM_001183;urn:miriam:uniprot:Q15904;urn:miriam:pubmed:27231034;urn:miriam:hgnc.symbol:ATP6AP1;urn:miriam:hgnc.symbol:ATP6AP1"
      hgnc "HGNC_SYMBOL:ATP6AP1"
      map_id "M14_29"
      name "ATP6AP1:Bafilomycin_space_A1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa50"
      uniprot "UNIPROT:Q15904"
    ]
    graphics [
      x 1022.5
      y 697.804542056548
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      annotation "PUBMED:22335796"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_88"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re78"
      uniprot "NA"
    ]
    graphics [
      x 1172.5
      y 850.9505467799008
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "M14_124"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa234"
      uniprot "NA"
    ]
    graphics [
      x 1892.5
      y 962.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_124"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "M14_123"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa233"
      uniprot "NA"
    ]
    graphics [
      x 1922.5
      y 932.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:uniprot:Q8IY34;urn:miriam:pubmed:32353859;urn:miriam:ncbigene:55117;urn:miriam:ncbigene:55117;urn:miriam:hgnc.symbol:SLC15A3;urn:miriam:hgnc:13621;urn:miriam:refseq:NM_182767;urn:miriam:uniprot:Q9H2J7;urn:miriam:ncbigene:51296;urn:miriam:uniprot:Q9H2J7;urn:miriam:hgnc.symbol:SLC6A15;urn:miriam:hgnc.symbol:SLC6A15;urn:miriam:refseq:NM_018057;urn:miriam:ensembl:ENSG00000072041"
      hgnc "HGNC_SYMBOL:SLC15A3;HGNC_SYMBOL:SLC6A15"
      map_id "M14_107"
      name "SLC6A15"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa191"
      uniprot "UNIPROT:Q8IY34;UNIPROT:Q9H2J7"
    ]
    graphics [
      x 3272.5
      y 2028.438607253741
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:uniprot:Q8IY34;urn:miriam:pubmed:32353859;urn:miriam:ncbigene:55117;urn:miriam:ncbigene:55117;urn:miriam:hgnc.symbol:SLC15A3;urn:miriam:hgnc:13621;urn:miriam:refseq:NM_182767;urn:miriam:uniprot:Q9H2J7;urn:miriam:ncbigene:51296;urn:miriam:uniprot:Q9H2J7;urn:miriam:hgnc.symbol:SLC6A15;urn:miriam:hgnc.symbol:SLC6A15;urn:miriam:refseq:NM_018057;urn:miriam:ensembl:ENSG00000072041;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049"
      hgnc "HGNC_SYMBOL:SLC15A3;HGNC_SYMBOL:SLC6A15"
      map_id "M14_24"
      name "SLC6A15:Nsp6"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa45"
      uniprot "UNIPROT:Q8IY34;UNIPROT:Q9H2J7"
    ]
    graphics [
      x 2642.5
      y 587.7682304796128
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_71"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re56"
      uniprot "NA"
    ]
    graphics [
      x 2942.5
      y 2002.6505321512923
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_70"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re55"
      uniprot "NA"
    ]
    graphics [
      x 1938.7805189192002
      y 2432.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_72"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re57"
      uniprot "NA"
    ]
    graphics [
      x 2441.043215136001
      y 2726.0089009888093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:123134323"
      hgnc "NA"
      map_id "M14_110"
      name "Loratadine"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa200"
      uniprot "NA"
    ]
    graphics [
      x 2261.043215136001
      y 2582.458314370092
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:123134323;urn:miriam:uniprot:Q8IY34;urn:miriam:pubmed:32353859;urn:miriam:ncbigene:55117;urn:miriam:ncbigene:55117;urn:miriam:hgnc.symbol:SLC15A3;urn:miriam:hgnc:13621;urn:miriam:refseq:NM_182767;urn:miriam:uniprot:Q9H2J7;urn:miriam:ncbigene:51296;urn:miriam:uniprot:Q9H2J7;urn:miriam:hgnc.symbol:SLC6A15;urn:miriam:hgnc.symbol:SLC6A15;urn:miriam:refseq:NM_018057;urn:miriam:ensembl:ENSG00000072041"
      hgnc "HGNC_SYMBOL:SLC15A3;HGNC_SYMBOL:SLC6A15"
      map_id "M14_27"
      name "SLC6A15:Loratadine"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa49"
      uniprot "UNIPROT:Q8IY34;UNIPROT:Q9H2J7"
    ]
    graphics [
      x 1921.8191902032843
      y 1292.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:taxonomy:2697049"
      hgnc "NA"
      map_id "M14_108"
      name "Orf9c"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa192"
      uniprot "NA"
    ]
    graphics [
      x 1863.3339469581056
      y 3182.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049;urn:miriam:uniprot:Q8IY34;urn:miriam:pubmed:32353859;urn:miriam:ncbigene:55117;urn:miriam:ncbigene:55117;urn:miriam:hgnc.symbol:SLC15A3;urn:miriam:hgnc:13621;urn:miriam:refseq:NM_182767;urn:miriam:uniprot:Q9H2J7;urn:miriam:ncbigene:51296;urn:miriam:uniprot:Q9H2J7;urn:miriam:hgnc.symbol:SLC6A15;urn:miriam:hgnc.symbol:SLC6A15;urn:miriam:refseq:NM_018057;urn:miriam:ensembl:ENSG00000072041"
      hgnc "HGNC_SYMBOL:SLC15A3;HGNC_SYMBOL:SLC6A15"
      map_id "M14_25"
      name "SLC6A15:Orf9c"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa47"
      uniprot "UNIPROT:Q8IY34;UNIPROT:Q9H2J7"
    ]
    graphics [
      x 2822.5
      y 1589.7685509960293
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:taxonomy:2697049;urn:miriam:uniprot:M"
      hgnc "NA"
      map_id "M14_109"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa193"
      uniprot "UNIPROT:M"
    ]
    graphics [
      x 3392.5
      y 2021.640347711018
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:uniprot:Q8IY34;urn:miriam:pubmed:32353859;urn:miriam:ncbigene:55117;urn:miriam:ncbigene:55117;urn:miriam:hgnc.symbol:SLC15A3;urn:miriam:hgnc:13621;urn:miriam:refseq:NM_182767;urn:miriam:uniprot:Q9H2J7;urn:miriam:ncbigene:51296;urn:miriam:uniprot:Q9H2J7;urn:miriam:hgnc.symbol:SLC6A15;urn:miriam:hgnc.symbol:SLC6A15;urn:miriam:refseq:NM_018057;urn:miriam:ensembl:ENSG00000072041;urn:miriam:taxonomy:2697049;urn:miriam:uniprot:M"
      hgnc "HGNC_SYMBOL:SLC15A3;HGNC_SYMBOL:SLC6A15"
      map_id "M14_26"
      name "SLC6A15:M"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa48"
      uniprot "UNIPROT:Q8IY34;UNIPROT:Q9H2J7;UNIPROT:M"
    ]
    graphics [
      x 2492.5
      y 1201.435024234829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:uniprot:O75964;urn:miriam:hgnc:14247;urn:miriam:refseq:NM_006476;urn:miriam:hgnc.symbol:ATP5MG;urn:miriam:hgnc.symbol:ATP5MG;urn:miriam:ensembl:ENSG00000167283;urn:miriam:ncbigene:10632;urn:miriam:ncbigene:10632;urn:miriam:uniprot:O75964"
      hgnc "HGNC_SYMBOL:ATP5MG"
      map_id "M14_21"
      name "F_minus_ATPase"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa42"
      uniprot "UNIPROT:O75964"
    ]
    graphics [
      x 2171.043215136001
      y 2513.713309864131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049;urn:miriam:hgnc:14247;urn:miriam:refseq:NM_006476;urn:miriam:hgnc.symbol:ATP5MG;urn:miriam:hgnc.symbol:ATP5MG;urn:miriam:ensembl:ENSG00000167283;urn:miriam:ncbigene:10632;urn:miriam:ncbigene:10632;urn:miriam:uniprot:O75964"
      hgnc "HGNC_SYMBOL:ATP5MG"
      map_id "M14_22"
      name "F_minus_ATPase:Nsp6"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa43"
      uniprot "UNIPROT:O75964"
    ]
    graphics [
      x 602.5
      y 1959.295395563012
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_92"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re82"
      uniprot "NA"
    ]
    graphics [
      x 2598.7805189192004
      y 2198.4291303703953
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:hgnc:14247;urn:miriam:refseq:NM_006476;urn:miriam:hgnc.symbol:ATP5MG;urn:miriam:hgnc.symbol:ATP5MG;urn:miriam:ensembl:ENSG00000167283;urn:miriam:ncbigene:10632;urn:miriam:ncbigene:10632;urn:miriam:uniprot:O75964"
      hgnc "HGNC_SYMBOL:ATP5MG"
      map_id "M14_125"
      name "ATP5MG"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa255"
      uniprot "UNIPROT:O75964"
    ]
    graphics [
      x 1780.9135463862924
      y 872.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_125"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:uniprot:O75964"
      hgnc "NA"
      map_id "M14_37"
      name "F_minus_ATPase"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa97"
      uniprot "UNIPROT:O75964"
    ]
    graphics [
      x 3002.5
      y 800.4783956429558
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_93"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re83"
      uniprot "NA"
    ]
    graphics [
      x 2860.140916020091
      y 632.5045131438069
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_2"
      name "F1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa101"
      uniprot "NA"
    ]
    graphics [
      x 1957.432708728739
      y 452.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_1"
      name "F0"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa100"
      uniprot "NA"
    ]
    graphics [
      x 3422.5
      y 1663.8107765906786
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_130"
      name "sa5_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa49"
      uniprot "NA"
    ]
    graphics [
      x 1712.5
      y 1977.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_130"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859"
      hgnc "NA"
      map_id "M14_128"
      name "Nsp6_space_(_plus_)"
      node_subtype "RNA"
      node_type "species"
      org_id "sa47"
      uniprot "NA"
    ]
    graphics [
      x 2208.7805189192004
      y 2303.713309864131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_46"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re21"
      uniprot "NA"
    ]
    graphics [
      x 1922.5
      y 1622.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_56"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re32"
      uniprot "NA"
    ]
    graphics [
      x 1352.5
      y 879.3114613664379
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_49"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re24"
      uniprot "NA"
    ]
    graphics [
      x 3332.5
      y 1420.8614167199048
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_132"
      name "sa2_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa50"
      uniprot "NA"
    ]
    graphics [
      x 2672.5
      y 816.3996183854767
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859"
      hgnc "NA"
      map_id "M14_113"
      name "Nsp6_space_(_minus_)"
      node_subtype "RNA"
      node_type "species"
      org_id "sa218"
      uniprot "NA"
    ]
    graphics [
      x 1652.5
      y 1107.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_57"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re33"
      uniprot "NA"
    ]
    graphics [
      x 752.5
      y 1472.6540775161152
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_134"
      name "sa3_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa58"
      uniprot "NA"
    ]
    graphics [
      x 948.7805189192002
      y 2192.5182557004428
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_79"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re67"
      uniprot "NA"
    ]
    graphics [
      x 2792.5
      y 1343.3496847756978
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_33"
      name "P1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa84"
      uniprot "NA"
    ]
    graphics [
      x 1322.5
      y 1137.8321207248905
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_32"
      name "P0"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa83"
      uniprot "NA"
    ]
    graphics [
      x 2822.5
      y 1688.4007591851837
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 138
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_116"
      target_id "M14_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 139
    source 3
    target 2
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_31"
      target_id "M14_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 2
    target 4
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_78"
      target_id "M14_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 135
    target 3
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_79"
      target_id "M14_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 4
    target 5
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_20"
      target_id "M14_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 6
    target 5
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_129"
      target_id "M14_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 144
    source 5
    target 7
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_51"
      target_id "M14_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 145
    source 8
    target 6
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_47"
      target_id "M14_129"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 146
    source 6
    target 9
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_129"
      target_id "M14_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 147
    source 6
    target 10
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_129"
      target_id "M14_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 148
    source 6
    target 11
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_129"
      target_id "M14_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 149
    source 6
    target 12
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_129"
      target_id "M14_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 150
    source 6
    target 13
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_129"
      target_id "M14_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 151
    source 6
    target 14
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_129"
      target_id "M14_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 152
    source 127
    target 8
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_128"
      target_id "M14_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 153
    source 9
    target 126
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_48"
      target_id "M14_130"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 154
    source 118
    target 10
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_21"
      target_id "M14_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 155
    source 10
    target 119
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_50"
      target_id "M14_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 156
    source 107
    target 11
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_107"
      target_id "M14_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 157
    source 11
    target 108
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_69"
      target_id "M14_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 158
    source 92
    target 12
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_13"
      target_id "M14_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 159
    source 12
    target 93
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_55"
      target_id "M14_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 160
    source 79
    target 13
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_133"
      target_id "M14_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 161
    source 13
    target 80
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_52"
      target_id "M14_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 162
    source 15
    target 14
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_102"
      target_id "M14_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 163
    source 16
    target 14
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_127"
      target_id "M14_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 164
    source 14
    target 17
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_67"
      target_id "M14_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 165
    source 68
    target 15
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_63"
      target_id "M14_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 166
    source 15
    target 69
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_102"
      target_id "M14_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 167
    source 20
    target 16
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_44"
      target_id "M14_127"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 168
    source 16
    target 21
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_127"
      target_id "M14_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 169
    source 16
    target 22
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_127"
      target_id "M14_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 170
    source 16
    target 23
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_127"
      target_id "M14_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 171
    source 16
    target 24
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_127"
      target_id "M14_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 172
    source 16
    target 25
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_127"
      target_id "M14_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 173
    source 16
    target 26
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_127"
      target_id "M14_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 174
    source 17
    target 18
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_17"
      target_id "M14_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 175
    source 18
    target 19
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_77"
      target_id "M14_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 176
    source 60
    target 20
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_126"
      target_id "M14_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 177
    source 51
    target 21
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_137"
      target_id "M14_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 178
    source 21
    target 52
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_94"
      target_id "M14_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 179
    source 41
    target 22
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_136"
      target_id "M14_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 180
    source 22
    target 42
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_73"
      target_id "M14_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 181
    source 36
    target 23
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M14_10"
      target_id "M14_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 182
    source 23
    target 37
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_76"
      target_id "M14_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 34
    target 24
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_95"
      target_id "M14_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 24
    target 35
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_40"
      target_id "M14_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 25
    target 33
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_54"
      target_id "M14_131"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 27
    target 26
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_96"
      target_id "M14_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 26
    target 28
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_42"
      target_id "M14_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 27
    target 31
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_96"
      target_id "M14_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 28
    target 29
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_6"
      target_id "M14_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 29
    target 30
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_86"
      target_id "M14_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 31
    target 32
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_87"
      target_id "M14_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 37
    target 38
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_112"
      target_id "M14_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 39
    target 38
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_12"
      target_id "M14_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 38
    target 40
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_58"
      target_id "M14_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 41
    target 47
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_136"
      target_id "M14_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 41
    target 48
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_136"
      target_id "M14_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 42
    target 43
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_9"
      target_id "M14_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 42
    target 44
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_9"
      target_id "M14_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 43
    target 46
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_39"
      target_id "M14_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 44
    target 45
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_80"
      target_id "M14_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 47
    target 50
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_60"
      target_id "M14_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 48
    target 49
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_82"
      target_id "M14_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 51
    target 55
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_137"
      target_id "M14_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 51
    target 56
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_137"
      target_id "M14_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 52
    target 53
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_5"
      target_id "M14_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 53
    target 54
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_41"
      target_id "M14_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 58
    target 55
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_105"
      target_id "M14_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 55
    target 59
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_68"
      target_id "M14_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 56
    target 57
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_81"
      target_id "M14_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 61
    target 60
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_38"
      target_id "M14_126"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 60
    target 62
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_126"
      target_id "M14_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 60
    target 63
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_126"
      target_id "M14_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 65
    target 61
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_115"
      target_id "M14_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 62
    target 65
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_43"
      target_id "M14_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 63
    target 64
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_59"
      target_id "M14_135"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 65
    target 66
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_115"
      target_id "M14_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 66
    target 67
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_45"
      target_id "M14_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 71
    target 68
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_100"
      target_id "M14_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 69
    target 70
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_66"
      target_id "M14_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 72
    target 71
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_61"
      target_id "M14_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 71
    target 73
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_100"
      target_id "M14_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 71
    target 74
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_100"
      target_id "M14_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 75
    target 72
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_114"
      target_id "M14_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 73
    target 78
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_65"
      target_id "M14_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 74
    target 75
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_62"
      target_id "M14_114"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 75
    target 76
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_114"
      target_id "M14_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 76
    target 77
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_64"
      target_id "M14_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 79
    target 85
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_133"
      target_id "M14_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 79
    target 86
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_133"
      target_id "M14_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 79
    target 87
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_133"
      target_id "M14_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 80
    target 81
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_7"
      target_id "M14_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 80
    target 82
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_7"
      target_id "M14_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 81
    target 84
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_53"
      target_id "M14_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 82
    target 83
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_85"
      target_id "M14_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 85
    target 91
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_83"
      target_id "M14_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 86
    target 90
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_84"
      target_id "M14_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 88
    target 87
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_111"
      target_id "M14_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 87
    target 89
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_75"
      target_id "M14_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 239
    source 96
    target 92
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_89"
      target_id "M14_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 240
    source 92
    target 97
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_13"
      target_id "M14_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 241
    source 93
    target 94
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_14"
      target_id "M14_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 242
    source 94
    target 95
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_91"
      target_id "M14_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 243
    source 99
    target 96
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_36"
      target_id "M14_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 244
    source 100
    target 96
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_117"
      target_id "M14_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 245
    source 97
    target 98
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_90"
      target_id "M14_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 246
    source 99
    target 104
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CATALYSIS"
      source_id "M14_36"
      target_id "M14_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 247
    source 100
    target 101
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_117"
      target_id "M14_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 248
    source 102
    target 101
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_106"
      target_id "M14_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 249
    source 101
    target 103
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_74"
      target_id "M14_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 250
    source 105
    target 104
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_124"
      target_id "M14_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 251
    source 104
    target 106
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_88"
      target_id "M14_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 252
    source 107
    target 109
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_107"
      target_id "M14_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 253
    source 107
    target 110
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_107"
      target_id "M14_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 254
    source 107
    target 111
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_107"
      target_id "M14_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 255
    source 116
    target 109
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_109"
      target_id "M14_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 256
    source 109
    target 117
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_71"
      target_id "M14_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 257
    source 114
    target 110
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_108"
      target_id "M14_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 258
    source 110
    target 115
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_70"
      target_id "M14_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 259
    source 112
    target 111
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_110"
      target_id "M14_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 260
    source 111
    target 113
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_72"
      target_id "M14_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 261
    source 120
    target 118
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_92"
      target_id "M14_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 262
    source 121
    target 120
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_125"
      target_id "M14_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 263
    source 122
    target 120
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_37"
      target_id "M14_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 264
    source 123
    target 122
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_93"
      target_id "M14_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 265
    source 124
    target 123
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_2"
      target_id "M14_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 266
    source 125
    target 123
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_1"
      target_id "M14_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 267
    source 128
    target 127
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_46"
      target_id "M14_128"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 268
    source 127
    target 129
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_128"
      target_id "M14_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 269
    source 127
    target 130
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_128"
      target_id "M14_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 270
    source 132
    target 128
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_113"
      target_id "M14_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 271
    source 129
    target 132
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_56"
      target_id "M14_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 272
    source 130
    target 131
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_49"
      target_id "M14_132"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 273
    source 132
    target 133
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_113"
      target_id "M14_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 274
    source 133
    target 134
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_57"
      target_id "M14_134"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 275
    source 136
    target 135
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_33"
      target_id "M14_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 276
    source 137
    target 135
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_32"
      target_id "M14_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
