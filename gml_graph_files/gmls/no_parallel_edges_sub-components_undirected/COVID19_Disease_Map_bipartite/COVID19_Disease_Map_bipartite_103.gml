# generated with VANTED V2.8.2 at Fri Mar 04 10:04:34 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:ensembl:ENSG00000163513;urn:miriam:ensembl:ENSG00000106799"
      hgnc "NA"
      map_id "W23_12"
      name "e3254"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e3254"
      uniprot "NA"
    ]
    graphics [
      x 3362.5
      y 1399.8402358087055
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:29945215;PUBMED:24627487;PUBMED:24438557"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_24"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ide33a2255"
      uniprot "NA"
    ]
    graphics [
      x 2702.5
      y 1004.6209790551018
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:ensembl:ENSG00000124225;urn:miriam:ensembl:ENSG00000168675"
      hgnc "NA"
      map_id "W23_2"
      name "b0004"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b0004"
      uniprot "NA"
    ]
    graphics [
      x 3002.5
      y 1087.7426257424934
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:ensembl:ENSG00000175387"
      hgnc "NA"
      map_id "W23_10"
      name "df78d"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "df78d"
      uniprot "NA"
    ]
    graphics [
      x 2435.035345548027
      y 451.4350242348289
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 5
    source 1
    target 2
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "W23_12"
      target_id "W23_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 6
    source 3
    target 2
    cd19dm [
      diagram "WP4904"
      edge_type "INHIBITION"
      source_id "W23_2"
      target_id "W23_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 7
    source 2
    target 4
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_24"
      target_id "W23_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
