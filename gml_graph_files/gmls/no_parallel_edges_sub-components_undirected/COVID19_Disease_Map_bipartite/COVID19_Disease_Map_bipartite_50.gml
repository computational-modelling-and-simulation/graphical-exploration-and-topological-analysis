# generated with VANTED V2.8.2 at Fri Mar 04 10:04:33 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_13"
      name "EDEMosome"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "c4176"
      uniprot "NA"
    ]
    graphics [
      x 2222.5
      y 1586.7158381002334
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_29"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id1d777c00"
      uniprot "NA"
    ]
    graphics [
      x 1502.5
      y 1807.3397236403869
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_30"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id26b440cd"
      uniprot "NA"
    ]
    graphics [
      x 2372.5
      y 815.3879277149115
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_21"
      name "DMV_space__br_double_space_membrane_space__br_vesicle"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "e5087"
      uniprot "NA"
    ]
    graphics [
      x 1743.7069790936666
      y 182.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_16"
      name "Endoplasmic_space_Reticulum"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "ceb35"
      uniprot "NA"
    ]
    graphics [
      x 1442.5
      y 995.1961954122032
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 6
    source 2
    target 1
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_29"
      target_id "W7_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 7
    source 1
    target 3
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "W7_13"
      target_id "W7_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 8
    source 5
    target 2
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "W7_16"
      target_id "W7_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 9
    source 3
    target 4
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_30"
      target_id "W7_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
