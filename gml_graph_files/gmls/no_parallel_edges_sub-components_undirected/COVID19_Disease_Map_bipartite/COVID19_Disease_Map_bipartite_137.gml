# generated with VANTED V2.8.2 at Fri Mar 04 10:04:35 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:23526;urn:miriam:taxonomy:9606;urn:miriam:uniprot:Q8NE86;urn:miriam:uniprot:Q8NE86;urn:miriam:hgnc.symbol:MCU;urn:miriam:refseq:NM_138357;urn:miriam:hgnc.symbol:MCU;urn:miriam:ensembl:ENSG00000156026;urn:miriam:pubmed:24231807;urn:miriam:ncbigene:90550;urn:miriam:ncbigene:90550"
      hgnc "HGNC_SYMBOL:MCU"
      map_id "M118_315"
      name "MCU"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa34"
      uniprot "UNIPROT:Q8NE86"
    ]
    graphics [
      x 2462.5
      y 1471.435024234829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_315"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:24231807"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_270"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_1_re46"
      uniprot "NA"
    ]
    graphics [
      x 2388.7805189192004
      y 2332.060834871624
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_270"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000183172;urn:miriam:pubmed:24231807;urn:miriam:ncbigene:91689;urn:miriam:uniprot:Q9H4I9;urn:miriam:uniprot:Q9H4I9;urn:miriam:ncbigene:91689;urn:miriam:hgnc.symbol:SMDT1;urn:miriam:hgnc.symbol:SMDT1;urn:miriam:refseq:NM_033318;urn:miriam:hgnc:25055"
      hgnc "HGNC_SYMBOL:SMDT1"
      map_id "M118_316"
      name "SMDT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa35"
      uniprot "UNIPROT:Q9H4I9"
    ]
    graphics [
      x 2642.5
      y 1967.7682304796128
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_316"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:24231807;urn:miriam:ensembl:ENSG00000107745;urn:miriam:uniprot:Q9BPX6;urn:miriam:uniprot:Q9BPX6;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1530;urn:miriam:pubmed:24231807;urn:miriam:hgnc.symbol:MICU1;urn:miriam:hgnc.symbol:MICU1;urn:miriam:refseq:NM_006077;urn:miriam:ncbigene:10367;urn:miriam:ncbigene:10367;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000183172;urn:miriam:pubmed:24231807;urn:miriam:ncbigene:91689;urn:miriam:uniprot:Q9H4I9;urn:miriam:uniprot:Q9H4I9;urn:miriam:ncbigene:91689;urn:miriam:hgnc.symbol:SMDT1;urn:miriam:hgnc.symbol:SMDT1;urn:miriam:refseq:NM_033318;urn:miriam:hgnc:25055;urn:miriam:hgnc:23526;urn:miriam:taxonomy:9606;urn:miriam:uniprot:Q8NE86;urn:miriam:uniprot:Q8NE86;urn:miriam:hgnc.symbol:MCU;urn:miriam:refseq:NM_138357;urn:miriam:hgnc.symbol:MCU;urn:miriam:ensembl:ENSG00000156026;urn:miriam:pubmed:24231807;urn:miriam:ncbigene:90550;urn:miriam:ncbigene:90550;urn:miriam:hgnc:31830;urn:miriam:ensembl:ENSG00000165487;urn:miriam:refseq:NM_152726;urn:miriam:taxonomy:9606;urn:miriam:pubmed:24231807;urn:miriam:hgnc.symbol:MICU2;urn:miriam:hgnc.symbol:MICU2;urn:miriam:ncbigene:221154;urn:miriam:ncbigene:221154;urn:miriam:uniprot:Q8IYU8;urn:miriam:uniprot:Q8IYU8"
      hgnc "HGNC_SYMBOL:MICU1;HGNC_SYMBOL:SMDT1;HGNC_SYMBOL:MCU;HGNC_SYMBOL:MICU2"
      map_id "M118_230"
      name "MCU:MICU1:MICU2:SMDT1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_1_csa16"
      uniprot "UNIPROT:Q9BPX6;UNIPROT:Q9H4I9;UNIPROT:Q8NE86;UNIPROT:Q8IYU8"
    ]
    graphics [
      x 722.5
      y 1979.7438419327407
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_230"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "PUBMED:24231807"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_269"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_1_re44"
      uniprot "NA"
    ]
    graphics [
      x 1128.7805189192002
      y 2184.991042282978
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_269"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "PUBMED:24231807"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_278"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_1_re58"
      uniprot "NA"
    ]
    graphics [
      x 812.5
      y 1402.6626209543215
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_278"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:26968367;urn:miriam:ensembl:ENSG00000107745;urn:miriam:uniprot:Q9BPX6;urn:miriam:uniprot:Q9BPX6;urn:miriam:hgnc:1530;urn:miriam:hgnc.symbol:MICU1;urn:miriam:hgnc.symbol:MICU1;urn:miriam:refseq:NM_006077;urn:miriam:ncbigene:10367;urn:miriam:ncbigene:10367;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000183172;urn:miriam:pubmed:24231807;urn:miriam:ncbigene:91689;urn:miriam:uniprot:Q9H4I9;urn:miriam:uniprot:Q9H4I9;urn:miriam:ncbigene:91689;urn:miriam:hgnc.symbol:SMDT1;urn:miriam:hgnc.symbol:SMDT1;urn:miriam:refseq:NM_033318;urn:miriam:hgnc:25055;urn:miriam:hgnc:26076;urn:miriam:hgnc.symbol:MCUB;urn:miriam:hgnc.symbol:MCUB;urn:miriam:ensembl:ENSG00000005059;urn:miriam:refseq:NM_017918;urn:miriam:ncbigene:55013;urn:miriam:ncbigene:55013;urn:miriam:uniprot:Q9NWR8;urn:miriam:uniprot:Q9NWR8;urn:miriam:hgnc:31830;urn:miriam:ensembl:ENSG00000165487;urn:miriam:refseq:NM_152726;urn:miriam:hgnc.symbol:MICU2;urn:miriam:hgnc.symbol:MICU2;urn:miriam:ncbigene:221154;urn:miriam:ncbigene:221154;urn:miriam:uniprot:Q8IYU8;urn:miriam:uniprot:Q8IYU8;urn:miriam:hgnc:23526;urn:miriam:taxonomy:9606;urn:miriam:uniprot:Q8NE86;urn:miriam:uniprot:Q8NE86;urn:miriam:hgnc.symbol:MCU;urn:miriam:refseq:NM_138357;urn:miriam:hgnc.symbol:MCU;urn:miriam:ensembl:ENSG00000156026;urn:miriam:pubmed:24231807;urn:miriam:ncbigene:90550;urn:miriam:ncbigene:90550"
      hgnc "HGNC_SYMBOL:MICU1;HGNC_SYMBOL:SMDT1;HGNC_SYMBOL:MCUB;HGNC_SYMBOL:MICU2;HGNC_SYMBOL:MCU"
      map_id "M118_236"
      name "Mitochondrial_space_calcium_space_uniporter_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_1_csa6"
      uniprot "UNIPROT:Q9BPX6;UNIPROT:Q9H4I9;UNIPROT:Q9NWR8;UNIPROT:Q8IYU8;UNIPROT:Q8NE86"
    ]
    graphics [
      x 2012.5
      y 1112.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_236"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "PUBMED:24231807"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_266"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "path_1_re25"
      uniprot "NA"
    ]
    graphics [
      x 3182.5
      y 1339.8713265536658
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_266"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "PUBMED:24231807"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_275"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re51"
      uniprot "NA"
    ]
    graphics [
      x 2402.5
      y 1126.0046064486714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_275"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:24231807;urn:miriam:obo.go:GO%3A0006851"
      hgnc "NA"
      map_id "M118_312"
      name "mitochondrial_space_calcium_space_ion_space_transmembrane_space_transport"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa31"
      uniprot "NA"
    ]
    graphics [
      x 1952.5
      y 1622.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_312"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29108"
      hgnc "NA"
      map_id "M118_320"
      name "Ca2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "path_1_sa63"
      uniprot "NA"
    ]
    graphics [
      x 3542.5
      y 1828.9633390353715
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_320"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:24231807;urn:miriam:obo.chebi:CHEBI%3A29108"
      hgnc "NA"
      map_id "M118_321"
      name "Ca2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "path_1_sa64"
      uniprot "NA"
    ]
    graphics [
      x 1862.5
      y 1862.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_321"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:31830;urn:miriam:ensembl:ENSG00000165487;urn:miriam:refseq:NM_152726;urn:miriam:taxonomy:9606;urn:miriam:pubmed:24231807;urn:miriam:hgnc.symbol:MICU2;urn:miriam:hgnc.symbol:MICU2;urn:miriam:ncbigene:221154;urn:miriam:ncbigene:221154;urn:miriam:uniprot:Q8IYU8;urn:miriam:uniprot:Q8IYU8"
      hgnc "HGNC_SYMBOL:MICU2"
      map_id "M118_314"
      name "MCU2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa33"
      uniprot "UNIPROT:Q8IYU8"
    ]
    graphics [
      x 2552.5
      y 2064.576720665756
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_314"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000107745;urn:miriam:uniprot:Q9BPX6;urn:miriam:uniprot:Q9BPX6;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1530;urn:miriam:pubmed:24231807;urn:miriam:hgnc.symbol:MICU1;urn:miriam:hgnc.symbol:MICU1;urn:miriam:refseq:NM_006077;urn:miriam:ncbigene:10367;urn:miriam:ncbigene:10367"
      hgnc "HGNC_SYMBOL:MICU1"
      map_id "M118_313"
      name "MCU1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa32"
      uniprot "UNIPROT:Q9BPX6"
    ]
    graphics [
      x 182.5
      y 1824.8599656103618
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_313"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 15
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_315"
      target_id "M118_270"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 16
    source 3
    target 2
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_316"
      target_id "M118_270"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 17
    source 2
    target 4
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_270"
      target_id "M118_230"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 18
    source 5
    target 4
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_269"
      target_id "M118_230"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 19
    source 4
    target 6
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_230"
      target_id "M118_278"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 20
    source 13
    target 5
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_314"
      target_id "M118_269"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 21
    source 14
    target 5
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_313"
      target_id "M118_269"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 22
    source 6
    target 7
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_278"
      target_id "M118_236"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 23
    source 7
    target 8
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "TRIGGER"
      source_id "M118_236"
      target_id "M118_266"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 24
    source 7
    target 9
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_236"
      target_id "M118_275"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 25
    source 11
    target 8
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_320"
      target_id "M118_266"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 26
    source 8
    target 12
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_266"
      target_id "M118_321"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 27
    source 9
    target 10
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_275"
      target_id "M118_312"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
