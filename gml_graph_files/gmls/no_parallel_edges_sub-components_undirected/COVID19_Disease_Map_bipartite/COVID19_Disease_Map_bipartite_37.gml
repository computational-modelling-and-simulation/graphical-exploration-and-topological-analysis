# generated with VANTED V2.8.2 at Fri Mar 04 10:04:33 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "urn:miriam:uniprot:P18850;urn:miriam:uniprot:P11021"
      hgnc "NA"
      map_id "W3_15"
      name "beb8c"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "beb8c"
      uniprot "UNIPROT:P18850;UNIPROT:P11021"
    ]
    graphics [
      x 1452.8920057158782
      y 419.738142841986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_60"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id3e604b90"
      uniprot "NA"
    ]
    graphics [
      x 1412.5
      y 892.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "urn:miriam:uniprot:P11021"
      hgnc "NA"
      map_id "W3_40"
      name "e10e4"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e10e4"
      uniprot "UNIPROT:P11021"
    ]
    graphics [
      x 2107.4327087287393
      y 575.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 4
    source 1
    target 2
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "W3_15"
      target_id "W3_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 5
    source 2
    target 3
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_60"
      target_id "W3_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
