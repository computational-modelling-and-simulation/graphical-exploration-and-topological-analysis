# generated with VANTED V2.8.2 at Fri Mar 04 10:04:33 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:pubmed:32654247;urn:miriam:reactome:R-COV-9694702;urn:miriam:uniprot:P0DTC9"
      hgnc "NA"
      map_id "R2_104"
      name "N_space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2361"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 572.5
      y 1907.9181734535155
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694345;PUBMED:15848177"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_322"
      name "Nucleoprotein translocates to the nucleolus"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694345__layout_2360"
      uniprot "NA"
    ]
    graphics [
      x 1691.043215136001
      y 2757.1855687946227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_322"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9683761;urn:miriam:pubmed:15094372;urn:miriam:uniprot:P0DTC9;urn:miriam:reactome:R-COV-9694659"
      hgnc "NA"
      map_id "R2_105"
      name "N_space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2362"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 2418.7805189192004
      y 2126.0089009888093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 4
    source 1
    target 2
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_104"
      target_id "R2_322"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 5
    source 2
    target 3
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_322"
      target_id "R2_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
