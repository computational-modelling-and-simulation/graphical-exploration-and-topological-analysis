# generated with VANTED V2.8.2 at Fri Mar 04 10:04:33 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "urn:miriam:ncbigene:4142"
      hgnc "NA"
      map_id "W11_10"
      name "MAS1"
      node_subtype "GENE"
      node_type "species"
      org_id "c93ee"
      uniprot "NA"
    ]
    graphics [
      x 2077.4327087287393
      y 452.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:32125455"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_30"
      name "NA"
      node_subtype "UNKNOWN_POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "id501a4bdc"
      uniprot "NA"
    ]
    graphics [
      x 1172.5
      y 736.4696378562796
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "PUBMED:32125455"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_36"
      name "NA"
      node_subtype "UNKNOWN_POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "id965b8019"
      uniprot "NA"
    ]
    graphics [
      x 1592.5
      y 1943.119912305107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "PUBMED:32125455"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_27"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id2c3a414b"
      uniprot "NA"
    ]
    graphics [
      x 3122.5
      y 1572.2345746030637
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_4"
      name "Anti_minus_Inflammation_br_Anti_minus_oxidant_br_Anti_minus_fibrosis_br_Vasodilation_br_Anti_minus_atrophy"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "a8dcd"
      uniprot "NA"
    ]
    graphics [
      x 1818.7805189192002
      y 2282.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "PUBMED:32125455"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_25"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id2338925"
      uniprot "NA"
    ]
    graphics [
      x 2088.7805189192004
      y 2135.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_17"
      name "Tissue_space_production"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "d3c68"
      uniprot "NA"
    ]
    graphics [
      x 2688.7805189192004
      y 2223.3940982191457
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A55438"
      hgnc "NA"
      map_id "W11_13"
      name "Angiotensin_space_1_minus_7"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "cfdf4"
      uniprot "NA"
    ]
    graphics [
      x 752.5
      y 1868.3665515504927
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A80128"
      hgnc "NA"
      map_id "W11_1"
      name "Angiotensin_space_1_minus_9"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "a0555"
      uniprot "NA"
    ]
    graphics [
      x 1842.2236828530072
      y 212.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 10
    source 2
    target 1
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_30"
      target_id "W11_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 11
    source 3
    target 1
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_36"
      target_id "W11_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 12
    source 1
    target 4
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "W11_10"
      target_id "W11_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 13
    source 9
    target 2
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "W11_1"
      target_id "W11_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 14
    source 8
    target 3
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "W11_13"
      target_id "W11_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 15
    source 4
    target 5
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_27"
      target_id "W11_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 16
    source 5
    target 6
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "W11_4"
      target_id "W11_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 17
    source 6
    target 7
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_25"
      target_id "W11_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
