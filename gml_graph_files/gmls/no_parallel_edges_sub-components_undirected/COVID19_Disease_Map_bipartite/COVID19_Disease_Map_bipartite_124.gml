# generated with VANTED V2.8.2 at Fri Mar 04 10:04:35 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:10428810;urn:miriam:ncbigene:2963;urn:miriam:refseq:NM_004128;urn:miriam:ncbigene:2963;urn:miriam:hgnc:4653;urn:miriam:uniprot:P13984;urn:miriam:ec-code:3.6.4.12;urn:miriam:hgnc.symbol:GTF2F2;urn:miriam:hgnc.symbol:GTF2F2;urn:miriam:ensembl:ENSG00000188342"
      hgnc "HGNC_SYMBOL:GTF2F2"
      map_id "M115_382"
      name "GTF2F2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa981"
      uniprot "UNIPROT:P13984"
    ]
    graphics [
      x 2792.5
      y 942.8678886993378
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_382"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:3860504;PUBMED:16878124;PUBMED:26496610;PUBMED:8662660;PUBMED:26344197;PUBMED:17643375;PUBMED:11278533"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_119"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10082"
      uniprot "NA"
    ]
    graphics [
      x 2312.5
      y 1149.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "PUBMED:8934526"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_138"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10114"
      uniprot "NA"
    ]
    graphics [
      x 2282.5
      y 669.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_138"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_150"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10126"
      uniprot "NA"
    ]
    graphics [
      x 1412.5
      y 952.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_150"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_365"
      name "Nsp9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1424"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1232.5
      y 1062.9973497539163
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_365"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:pubmed:10428810;urn:miriam:ncbigene:2963;urn:miriam:refseq:NM_004128;urn:miriam:ncbigene:2963;urn:miriam:hgnc:4653;urn:miriam:uniprot:P13984;urn:miriam:ec-code:3.6.4.12;urn:miriam:hgnc.symbol:GTF2F2;urn:miriam:hgnc.symbol:GTF2F2;urn:miriam:ensembl:ENSG00000188342"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:GTF2F2"
      map_id "M115_37"
      name "gtf2f2comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa16"
      uniprot "UNIPROT:P0DTD1;UNIPROT:P13984"
    ]
    graphics [
      x 1592.5
      y 1493.119912305107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_146"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10122"
      uniprot "NA"
    ]
    graphics [
      x 1522.514108617874
      y 419.738142841986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_146"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_144"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10120"
      uniprot "NA"
    ]
    graphics [
      x 962.5
      y 2030.4574439370535
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_144"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_145"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10121"
      uniprot "NA"
    ]
    graphics [
      x 662.5
      y 2151.224286585304
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_145"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_148"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10124"
      uniprot "NA"
    ]
    graphics [
      x 722.5
      y 1845.5409586491112
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_148"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:Q8TD19;urn:miriam:ec-code:2.7.11.1;urn:miriam:ncbigene:91754;urn:miriam:ncbigene:91754;urn:miriam:hgnc:18591;urn:miriam:refseq:NM_033116;urn:miriam:hgnc.symbol:NEK9;urn:miriam:hgnc.symbol:NEK9;urn:miriam:ensembl:ENSG00000119638"
      hgnc "HGNC_SYMBOL:NEK9"
      map_id "M115_381"
      name "NEK9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa979"
      uniprot "UNIPROT:Q8TD19"
    ]
    graphics [
      x 2282.5
      y 1846.1202267557142
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_381"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:uniprot:Q8TD19;urn:miriam:ec-code:2.7.11.1;urn:miriam:ncbigene:91754;urn:miriam:ncbigene:91754;urn:miriam:hgnc:18591;urn:miriam:refseq:NM_033116;urn:miriam:hgnc.symbol:NEK9;urn:miriam:hgnc.symbol:NEK9;urn:miriam:ensembl:ENSG00000119638;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:NEK9;HGNC_SYMBOL:rep"
      map_id "M115_39"
      name "nek9comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa18"
      uniprot "UNIPROT:Q8TD19;UNIPROT:P0DTD1"
    ]
    graphics [
      x 2012.5
      y 1715.8526976066757
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "PUBMED:17353931;PUBMED:26186194;PUBMED:20873783;PUBMED:12101123;PUBMED:28514442;PUBMED:12840024;PUBMED:20562859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_120"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10084"
      uniprot "NA"
    ]
    graphics [
      x 2222.5
      y 1286.7158381002334
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:NEK7;urn:miriam:hgnc.symbol:NEK7;urn:miriam:refseq:NM_133494;urn:miriam:uniprot:Q8TDX7;urn:miriam:ncbigene:140609;urn:miriam:ncbigene:140609;urn:miriam:ensembl:ENSG00000151414;urn:miriam:hgnc:13386"
      hgnc "HGNC_SYMBOL:NEK7"
      map_id "M115_252"
      name "NEK7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1067"
      uniprot "UNIPROT:Q8TDX7"
    ]
    graphics [
      x 2642.5
      y 1487.7682304796128
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_252"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:10783;urn:miriam:ncbigene:10783;urn:miriam:ec-code:2.7.11.1;urn:miriam:uniprot:Q9HC98;urn:miriam:hgnc.symbol:NEK6;urn:miriam:refseq:NM_014397;urn:miriam:hgnc.symbol:NEK6;urn:miriam:hgnc:7749;urn:miriam:ensembl:ENSG00000119408"
      hgnc "HGNC_SYMBOL:NEK6"
      map_id "M115_251"
      name "NEK6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1066"
      uniprot "UNIPROT:Q9HC98"
    ]
    graphics [
      x 3032.5
      y 1357.8843495551378
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_251"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:28514442;urn:miriam:ncbigene:10783;urn:miriam:ncbigene:10783;urn:miriam:ec-code:2.7.11.1;urn:miriam:uniprot:Q9HC98;urn:miriam:hgnc.symbol:NEK6;urn:miriam:refseq:NM_014397;urn:miriam:hgnc.symbol:NEK6;urn:miriam:hgnc:7749;urn:miriam:ensembl:ENSG00000119408;urn:miriam:uniprot:Q8TD19;urn:miriam:ec-code:2.7.11.1;urn:miriam:ncbigene:91754;urn:miriam:ncbigene:91754;urn:miriam:hgnc:18591;urn:miriam:refseq:NM_033116;urn:miriam:hgnc.symbol:NEK9;urn:miriam:hgnc.symbol:NEK9;urn:miriam:ensembl:ENSG00000119638;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:NEK7;urn:miriam:hgnc.symbol:NEK7;urn:miriam:refseq:NM_133494;urn:miriam:uniprot:Q8TDX7;urn:miriam:ncbigene:140609;urn:miriam:ncbigene:140609;urn:miriam:ensembl:ENSG00000151414;urn:miriam:hgnc:13386"
      hgnc "HGNC_SYMBOL:NEK6;HGNC_SYMBOL:NEK9;HGNC_SYMBOL:NEK7"
      map_id "M115_50"
      name "NEKs"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa34"
      uniprot "UNIPROT:Q9HC98;UNIPROT:Q8TD19;UNIPROT:Q8TDX7"
    ]
    graphics [
      x 1507.432708728739
      y 539.738142841986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:P61962;urn:miriam:pubmed:16887337;urn:miriam:ensembl:ENSG00000136485;urn:miriam:hgnc.symbol:DCAF7;urn:miriam:hgnc.symbol:DCAF7;urn:miriam:ncbigene:10238;urn:miriam:ncbigene:10238;urn:miriam:refseq:NM_005828;urn:miriam:hgnc:30915;urn:miriam:pubmed:16949367"
      hgnc "HGNC_SYMBOL:DCAF7"
      map_id "M115_383"
      name "DCAF7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa983"
      uniprot "UNIPROT:P61962"
    ]
    graphics [
      x 452.5
      y 1716.404539742084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_383"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:P61962;urn:miriam:pubmed:16887337;urn:miriam:ensembl:ENSG00000136485;urn:miriam:hgnc.symbol:DCAF7;urn:miriam:hgnc.symbol:DCAF7;urn:miriam:ncbigene:10238;urn:miriam:ncbigene:10238;urn:miriam:refseq:NM_005828;urn:miriam:hgnc:30915;urn:miriam:pubmed:16949367"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:DCAF7"
      map_id "M115_43"
      name "dcafcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa26"
      uniprot "UNIPROT:P0DTD1;UNIPROT:P61962"
    ]
    graphics [
      x 1908.7805189192002
      y 2642.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "PUBMED:27705803"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_142"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10118"
      uniprot "NA"
    ]
    graphics [
      x 1352.5
      y 1749.311461366438
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_142"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "PUBMED:27880803"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_116"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10061"
      uniprot "NA"
    ]
    graphics [
      x 122.5
      y 1842.8130271979446
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:3176;urn:miriam:refseq:NM_001955;urn:miriam:ncbigene:1906;urn:miriam:hgnc.symbol:EDN1;urn:miriam:uniprot:P05305;urn:miriam:ensembl:ENSG00000078401"
      hgnc "HGNC_SYMBOL:EDN1"
      map_id "M115_240"
      name "EDN1"
      node_subtype "GENE"
      node_type "species"
      org_id "sa1007"
      uniprot "UNIPROT:P05305"
    ]
    graphics [
      x 1022.5
      y 1846.7111332684874
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_240"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:3176;urn:miriam:refseq:NM_001955;urn:miriam:ncbigene:1906;urn:miriam:hgnc.symbol:EDN1;urn:miriam:uniprot:P05305;urn:miriam:ensembl:ENSG00000078401"
      hgnc "HGNC_SYMBOL:EDN1"
      map_id "M115_239"
      name "EDN1"
      node_subtype "RNA"
      node_type "species"
      org_id "sa1006"
      uniprot "UNIPROT:P05305"
    ]
    graphics [
      x 1262.5
      y 2016.9780694277015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_239"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "PUBMED:20837776"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_117"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10062"
      uniprot "NA"
    ]
    graphics [
      x 1652.5
      y 1527.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "PUBMED:20837776"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_121"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10087"
      uniprot "NA"
    ]
    graphics [
      x 2162.5
      y 1823.7133098641311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:3176;urn:miriam:refseq:NM_001955;urn:miriam:uniprot:P05305;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:ensembl:ENSG00000078401"
      hgnc "HGNC_SYMBOL:EDN1"
      map_id "M115_254"
      name "EDN1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1074"
      uniprot "UNIPROT:P05305"
    ]
    graphics [
      x 3422.5
      y 1870.069977491511
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_254"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "PUBMED:15568807"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_137"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10113"
      uniprot "NA"
    ]
    graphics [
      x 3152.5
      y 1884.3536941863422
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_137"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:3176;urn:miriam:refseq:NM_001955;urn:miriam:uniprot:P05305;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:ensembl:ENSG00000078401"
      hgnc "HGNC_SYMBOL:EDN1"
      map_id "M115_238"
      name "EDN1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1003"
      uniprot "UNIPROT:P05305"
    ]
    graphics [
      x 3362.5
      y 1763.3387688589096
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_238"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:28514442;urn:miriam:hgnc:3176;urn:miriam:refseq:NM_001955;urn:miriam:uniprot:P05305;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:ensembl:ENSG00000078401"
      hgnc "HGNC_SYMBOL:EDN1"
      map_id "M115_51"
      name "EDN1_minus_homo"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa36"
      uniprot "UNIPROT:P05305"
    ]
    graphics [
      x 2822.5
      y 1852.097842491094
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "PUBMED:16713569"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_133"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10109"
      uniprot "NA"
    ]
    graphics [
      x 1248.7805189192002
      y 2196.9780694277015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_133"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "PUBMED:16169070"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_134"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10110"
      uniprot "NA"
    ]
    graphics [
      x 2081.043215136001
      y 2645.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "PUBMED:17678888;PUBMED:16741042;PUBMED:20837776;PUBMED:11603923"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_122"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10089"
      uniprot "NA"
    ]
    graphics [
      x 2190.2870010858446
      y 2783.713309864131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "PUBMED:16713569"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_123"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10090"
      uniprot "NA"
    ]
    graphics [
      x 2882.5
      y 1593.858034494286
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "PUBMED:16169070"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_136"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10112"
      uniprot "NA"
    ]
    graphics [
      x 3302.5
      y 1212.6294574380252
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:P46379;urn:miriam:refseq:NM_080703;urn:miriam:ncbigene:7917;urn:miriam:ncbigene:7917;urn:miriam:ensembl:ENSG00000204463;urn:miriam:hgnc:13919;urn:miriam:hgnc.symbol:BAG6;urn:miriam:hgnc.symbol:BAG6"
      hgnc "HGNC_SYMBOL:BAG6"
      map_id "M115_255"
      name "BAG6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1077"
      uniprot "UNIPROT:P46379"
    ]
    graphics [
      x 2852.5
      y 1603.9317909822832
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_255"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:16169070;urn:miriam:hgnc:3176;urn:miriam:refseq:NM_001955;urn:miriam:uniprot:P05305;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:ensembl:ENSG00000078401;urn:miriam:uniprot:P46379;urn:miriam:refseq:NM_080703;urn:miriam:ncbigene:7917;urn:miriam:ncbigene:7917;urn:miriam:ensembl:ENSG00000204463;urn:miriam:hgnc:13919;urn:miriam:hgnc.symbol:BAG6;urn:miriam:hgnc.symbol:BAG6"
      hgnc "HGNC_SYMBOL:EDN1;HGNC_SYMBOL:BAG6"
      map_id "M115_52"
      name "EDN1_minus_homo"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa37"
      uniprot "UNIPROT:P05305;UNIPROT:P46379"
    ]
    graphics [
      x 3332.5
      y 1236.2015552786877
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:16713569;urn:miriam:hgnc:3176;urn:miriam:refseq:NM_001955;urn:miriam:uniprot:P05305;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:ensembl:ENSG00000078401"
      hgnc "HGNC_SYMBOL:EDN1"
      map_id "M115_54"
      name "EDN1_minus_homo"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa39"
      uniprot "UNIPROT:P05305"
    ]
    graphics [
      x 2591.043215136001
      y 2621.6943451463712
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:17678888;urn:miriam:hgnc:3176;urn:miriam:refseq:NM_001955;urn:miriam:uniprot:P05305;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:ensembl:ENSG00000078401"
      hgnc "HGNC_SYMBOL:EDN1"
      map_id "M115_53"
      name "EDN1_minus_homo"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa38"
      uniprot "UNIPROT:P05305"
    ]
    graphics [
      x 2628.7805189192004
      y 2507.7682304796126
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "PUBMED:7509919"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_132"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10108"
      uniprot "NA"
    ]
    graphics [
      x 2882.5
      y 1199.3089157085717
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:refseq:NM_001166055;urn:miriam:hgnc:3179;urn:miriam:uniprot:P25101;urn:miriam:ensembl:ENSG00000151617;urn:miriam:ncbigene:1909;urn:miriam:ncbigene:1909;urn:miriam:hgnc.symbol:EDNRA;urn:miriam:hgnc.symbol:EDNRA"
      hgnc "HGNC_SYMBOL:EDNRA"
      map_id "M115_264"
      name "EDNRA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1116"
      uniprot "UNIPROT:P25101"
    ]
    graphics [
      x 1992.2236828530072
      y 272.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_264"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:7509919;urn:miriam:refseq:NM_001166055;urn:miriam:hgnc:3179;urn:miriam:uniprot:P25101;urn:miriam:ensembl:ENSG00000151617;urn:miriam:ncbigene:1909;urn:miriam:ncbigene:1909;urn:miriam:hgnc.symbol:EDNRA;urn:miriam:hgnc.symbol:EDNRA;urn:miriam:hgnc:3176;urn:miriam:refseq:NM_001955;urn:miriam:uniprot:P05305;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:ensembl:ENSG00000078401"
      hgnc "HGNC_SYMBOL:EDNRA;HGNC_SYMBOL:EDN1"
      map_id "M115_60"
      name "EDN1_minus_homo"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa45"
      uniprot "UNIPROT:P25101;UNIPROT:P05305"
    ]
    graphics [
      x 1925.8155480545588
      y 482.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "PUBMED:17472992;PUBMED:10961375;PUBMED:11849873;PUBMED:11704565;PUBMED:11752352;PUBMED:11728166;PUBMED:11447307"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_159"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10136"
      uniprot "NA"
    ]
    graphics [
      x 3062.5
      y 1367.540546233551
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_159"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "PUBMED:20811346;PUBMED:19601701;PUBMED:19389876;PUBMED:19920913"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_160"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10137"
      uniprot "NA"
    ]
    graphics [
      x 2151.4064326594944
      y 233.71330986413113
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_160"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "PUBMED:10727528"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_162"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10139"
      uniprot "NA"
    ]
    graphics [
      x 332.5
      y 1124.9968698481289
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_162"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "PUBMED:24261583;PUBMED:22862294;PUBMED:22458347"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_161"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10138"
      uniprot "NA"
    ]
    graphics [
      x 2012.5
      y 992.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_161"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:16004692"
      hgnc "NA"
      map_id "M115_281"
      name "Macitentan"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1177"
      uniprot "NA"
    ]
    graphics [
      x 2612.5
      y 1158.7911939082426
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_281"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:24261583;urn:miriam:pubmed:22862294;urn:miriam:pubmed:22458347;urn:miriam:refseq:NM_001166055;urn:miriam:hgnc:3179;urn:miriam:uniprot:P25101;urn:miriam:ensembl:ENSG00000151617;urn:miriam:ncbigene:1909;urn:miriam:ncbigene:1909;urn:miriam:hgnc.symbol:EDNRA;urn:miriam:hgnc.symbol:EDNRA;urn:miriam:pubchem.compound:16004692"
      hgnc "HGNC_SYMBOL:EDNRA"
      map_id "M115_76"
      name "EDNRMacComp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa61"
      uniprot "UNIPROT:P25101"
    ]
    graphics [
      x 1292.5
      y 1523.3146446645712
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:2244"
      hgnc "NA"
      map_id "M115_282"
      name "Acetylsalicylic_space_acid"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1180"
      uniprot "NA"
    ]
    graphics [
      x 1952.5
      y 1802.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_282"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:10727528;urn:miriam:refseq:NM_001166055;urn:miriam:hgnc:3179;urn:miriam:uniprot:P25101;urn:miriam:ensembl:ENSG00000151617;urn:miriam:ncbigene:1909;urn:miriam:ncbigene:1909;urn:miriam:hgnc.symbol:EDNRA;urn:miriam:hgnc.symbol:EDNRA;urn:miriam:pubchem.compound:2244"
      hgnc "HGNC_SYMBOL:EDNRA"
      map_id "M115_77"
      name "EDNRAcetComp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa62"
      uniprot "UNIPROT:P25101"
    ]
    graphics [
      x 542.5
      y 1213.5654979241358
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:6918493"
      hgnc "NA"
      map_id "M115_280"
      name "Ambrisentan"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1172"
      uniprot "NA"
    ]
    graphics [
      x 2732.5
      y 644.238921635267
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_280"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:20811346;urn:miriam:pubchem.compound:6918493;urn:miriam:refseq:NM_001166055;urn:miriam:hgnc:3179;urn:miriam:uniprot:P25101;urn:miriam:ensembl:ENSG00000151617;urn:miriam:ncbigene:1909;urn:miriam:ncbigene:1909;urn:miriam:hgnc.symbol:EDNRA;urn:miriam:hgnc.symbol:EDNRA"
      hgnc "HGNC_SYMBOL:EDNRA"
      map_id "M115_75"
      name "EDNRAmbComp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa60"
      uniprot "UNIPROT:P25101"
    ]
    graphics [
      x 3362.5
      y 1159.8199918847067
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:643975"
      hgnc "NA"
      map_id "M115_279"
      name "Sitaxentan"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1169"
      uniprot "NA"
    ]
    graphics [
      x 2118.7805189192004
      y 2345.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_279"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:17472992;urn:miriam:pubchem.compound:643975;urn:miriam:refseq:NM_001166055;urn:miriam:hgnc:3179;urn:miriam:uniprot:P25101;urn:miriam:ensembl:ENSG00000151617;urn:miriam:ncbigene:1909;urn:miriam:ncbigene:1909;urn:miriam:hgnc.symbol:EDNRA;urn:miriam:hgnc.symbol:EDNRA"
      hgnc "HGNC_SYMBOL:EDNRA"
      map_id "M115_74"
      name "EDNRASitaComp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa59"
      uniprot "UNIPROT:P25101"
    ]
    graphics [
      x 2868.7805189192004
      y 2219.4322662939235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000168090;urn:miriam:refseq:NM_006833;urn:miriam:ncbigene:10980;urn:miriam:ncbigene:10980;urn:miriam:hgnc.symbol:COPS6;urn:miriam:hgnc.symbol:COPS6;urn:miriam:uniprot:Q7L5N1;urn:miriam:hgnc:21749"
      hgnc "HGNC_SYMBOL:COPS6"
      map_id "M115_257"
      name "COPS6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1086"
      uniprot "UNIPROT:Q7L5N1"
    ]
    graphics [
      x 2432.5
      y 1796.0089009888093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_257"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:16169070;urn:miriam:hgnc:3176;urn:miriam:refseq:NM_001955;urn:miriam:uniprot:P05305;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:ensembl:ENSG00000078401;urn:miriam:ensembl:ENSG00000168090;urn:miriam:refseq:NM_006833;urn:miriam:ncbigene:10980;urn:miriam:ncbigene:10980;urn:miriam:hgnc.symbol:COPS6;urn:miriam:hgnc.symbol:COPS6;urn:miriam:uniprot:Q7L5N1;urn:miriam:hgnc:21749"
      hgnc "HGNC_SYMBOL:EDN1;HGNC_SYMBOL:COPS6"
      map_id "M115_57"
      name "EDN1_minus_homo"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa42"
      uniprot "UNIPROT:P05305;UNIPROT:Q7L5N1"
    ]
    graphics [
      x 752.5
      y 1988.3665515504927
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "PUBMED:10903862;PUBMED:26186194;PUBMED:23408908;PUBMED:11967155;PUBMED:26344197;PUBMED:27173435;PUBMED:16045761;PUBMED:22939629;PUBMED:26496610;PUBMED:21145461;PUBMED:27833851;PUBMED:18850735;PUBMED:26976604;PUBMED:9707402;PUBMED:28514442;PUBMED:16624822;PUBMED:19295130;PUBMED:19615732;PUBMED:22863883;PUBMED:12628923"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_124"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10091"
      uniprot "NA"
    ]
    graphics [
      x 2492.5
      y 2071.435024234829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_124"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000166200;urn:miriam:refseq:NM_004236;urn:miriam:ncbigene:9318;urn:miriam:ncbigene:9318;urn:miriam:hgnc:30747;urn:miriam:uniprot:P61201;urn:miriam:hgnc.symbol:COPS2;urn:miriam:hgnc.symbol:COPS2"
      hgnc "HGNC_SYMBOL:COPS2"
      map_id "M115_261"
      name "COPS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1090"
      uniprot "UNIPROT:P61201"
    ]
    graphics [
      x 1502.5
      y 1537.3397236403869
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_261"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:51138;urn:miriam:ncbigene:51138;urn:miriam:ensembl:ENSG00000138663;urn:miriam:refseq:NM_001258006;urn:miriam:uniprot:Q9BT78;urn:miriam:uniprot:Q9BT78;urn:miriam:hgnc:16702;urn:miriam:hgnc.symbol:COPS4;urn:miriam:hgnc.symbol:COPS4;urn:miriam:hgnc.symbol:COPS7A;urn:miriam:uniprot:Q9UBW8;urn:miriam:ncbigene:50813"
      hgnc "HGNC_SYMBOL:COPS4;HGNC_SYMBOL:COPS7A"
      map_id "M115_260"
      name "COPS4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1089"
      uniprot "UNIPROT:Q9BT78;UNIPROT:Q9UBW8"
    ]
    graphics [
      x 1172.5
      y 1557.3156502059937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_260"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:2240;urn:miriam:ec-code:3.4.-.-;urn:miriam:ensembl:ENSG00000121022;urn:miriam:uniprot:Q92905;urn:miriam:ncbigene:10987;urn:miriam:ncbigene:10987;urn:miriam:hgnc.symbol:COPS5;urn:miriam:refseq:NM_006837;urn:miriam:hgnc.symbol:COPS5"
      hgnc "HGNC_SYMBOL:COPS5"
      map_id "M115_258"
      name "COPS5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1087"
      uniprot "UNIPROT:Q92905"
    ]
    graphics [
      x 2898.7805189192004
      y 2394.6572753284927
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_258"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:refseq:NM_001164093;urn:miriam:hgnc:16758;urn:miriam:ensembl:ENSG00000111652;urn:miriam:hgnc.symbol:COPS7A;urn:miriam:hgnc.symbol:COPS7A;urn:miriam:uniprot:Q9UBW8;urn:miriam:ncbigene:50813;urn:miriam:ncbigene:50813"
      hgnc "HGNC_SYMBOL:COPS7A"
      map_id "M115_259"
      name "COPS7A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1088"
      uniprot "UNIPROT:Q9UBW8"
    ]
    graphics [
      x 1382.5
      y 1992.4847495740523
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_259"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:refseq:NM_006710;urn:miriam:uniprot:Q99627;urn:miriam:hgnc.symbol:COPS8;urn:miriam:ncbigene:10920;urn:miriam:ensembl:ENSG00000198612;urn:miriam:hgnc.symbol:COPS8;urn:miriam:ncbigene:10920;urn:miriam:hgnc:24335"
      hgnc "HGNC_SYMBOL:COPS8"
      map_id "M115_262"
      name "COPS8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1091"
      uniprot "UNIPROT:Q99627"
    ]
    graphics [
      x 2852.5
      y 1879.2792916844683
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_262"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:16045761;urn:miriam:ncbigene:51138;urn:miriam:ncbigene:51138;urn:miriam:ensembl:ENSG00000138663;urn:miriam:refseq:NM_001258006;urn:miriam:uniprot:Q9BT78;urn:miriam:uniprot:Q9BT78;urn:miriam:hgnc:16702;urn:miriam:hgnc.symbol:COPS4;urn:miriam:hgnc.symbol:COPS4;urn:miriam:hgnc.symbol:COPS7A;urn:miriam:uniprot:Q9UBW8;urn:miriam:ncbigene:50813;urn:miriam:ensembl:ENSG00000168090;urn:miriam:refseq:NM_006833;urn:miriam:ncbigene:10980;urn:miriam:ncbigene:10980;urn:miriam:hgnc.symbol:COPS6;urn:miriam:hgnc.symbol:COPS6;urn:miriam:uniprot:Q7L5N1;urn:miriam:hgnc:21749;urn:miriam:hgnc:2240;urn:miriam:ec-code:3.4.-.-;urn:miriam:ensembl:ENSG00000121022;urn:miriam:uniprot:Q92905;urn:miriam:ncbigene:10987;urn:miriam:ncbigene:10987;urn:miriam:hgnc.symbol:COPS5;urn:miriam:refseq:NM_006837;urn:miriam:hgnc.symbol:COPS5;urn:miriam:refseq:NM_001164093;urn:miriam:hgnc:16758;urn:miriam:ensembl:ENSG00000111652;urn:miriam:hgnc.symbol:COPS7A;urn:miriam:hgnc.symbol:COPS7A;urn:miriam:uniprot:Q9UBW8;urn:miriam:ncbigene:50813;urn:miriam:ncbigene:50813;urn:miriam:ensembl:ENSG00000166200;urn:miriam:refseq:NM_004236;urn:miriam:ncbigene:9318;urn:miriam:ncbigene:9318;urn:miriam:hgnc:30747;urn:miriam:uniprot:P61201;urn:miriam:hgnc.symbol:COPS2;urn:miriam:hgnc.symbol:COPS2;urn:miriam:refseq:NM_006710;urn:miriam:uniprot:Q99627;urn:miriam:hgnc.symbol:COPS8;urn:miriam:ncbigene:10920;urn:miriam:ensembl:ENSG00000198612;urn:miriam:hgnc.symbol:COPS8;urn:miriam:ncbigene:10920;urn:miriam:hgnc:24335"
      hgnc "HGNC_SYMBOL:COPS4;HGNC_SYMBOL:COPS7A;HGNC_SYMBOL:COPS6;HGNC_SYMBOL:COPS5;HGNC_SYMBOL:COPS2;HGNC_SYMBOL:COPS8"
      map_id "M115_55"
      name "COPS"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa40"
      uniprot "UNIPROT:Q9BT78;UNIPROT:Q9UBW8;UNIPROT:Q7L5N1;UNIPROT:Q92905;UNIPROT:P61201;UNIPROT:Q99627"
    ]
    graphics [
      x 2252.5
      y 842.4583143700918
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "PUBMED:22190034;PUBMED:12237292"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_135"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10111"
      uniprot "NA"
    ]
    graphics [
      x 2167.4327087287393
      y 413.71330986413113
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:I2A5W5;urn:miriam:taxonomy:11676;urn:miriam:hgnc.symbol:vpr"
      hgnc "HGNC_SYMBOL:vpr"
      map_id "M115_256"
      name "Vpr"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1085"
      uniprot "UNIPROT:I2A5W5"
    ]
    graphics [
      x 2102.5
      y 2001.6051299571266
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_256"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:22190034;urn:miriam:ensembl:ENSG00000168090;urn:miriam:refseq:NM_006833;urn:miriam:ncbigene:10980;urn:miriam:ncbigene:10980;urn:miriam:hgnc.symbol:COPS6;urn:miriam:hgnc.symbol:COPS6;urn:miriam:uniprot:Q7L5N1;urn:miriam:hgnc:21749;urn:miriam:refseq:NM_001164093;urn:miriam:hgnc:16758;urn:miriam:ensembl:ENSG00000111652;urn:miriam:hgnc.symbol:COPS7A;urn:miriam:hgnc.symbol:COPS7A;urn:miriam:uniprot:Q9UBW8;urn:miriam:ncbigene:50813;urn:miriam:ncbigene:50813;urn:miriam:refseq:NM_006710;urn:miriam:uniprot:Q99627;urn:miriam:hgnc.symbol:COPS8;urn:miriam:ncbigene:10920;urn:miriam:ensembl:ENSG00000198612;urn:miriam:hgnc.symbol:COPS8;urn:miriam:ncbigene:10920;urn:miriam:hgnc:24335;urn:miriam:ensembl:ENSG00000166200;urn:miriam:refseq:NM_004236;urn:miriam:ncbigene:9318;urn:miriam:ncbigene:9318;urn:miriam:hgnc:30747;urn:miriam:uniprot:P61201;urn:miriam:hgnc.symbol:COPS2;urn:miriam:hgnc.symbol:COPS2;urn:miriam:ncbigene:51138;urn:miriam:ncbigene:51138;urn:miriam:ensembl:ENSG00000138663;urn:miriam:refseq:NM_001258006;urn:miriam:uniprot:Q9BT78;urn:miriam:uniprot:Q9BT78;urn:miriam:hgnc:16702;urn:miriam:hgnc.symbol:COPS4;urn:miriam:hgnc.symbol:COPS4;urn:miriam:hgnc.symbol:COPS7A;urn:miriam:uniprot:Q9UBW8;urn:miriam:ncbigene:50813;urn:miriam:uniprot:I2A5W5;urn:miriam:taxonomy:11676;urn:miriam:hgnc.symbol:vpr;urn:miriam:hgnc:2240;urn:miriam:ec-code:3.4.-.-;urn:miriam:ensembl:ENSG00000121022;urn:miriam:uniprot:Q92905;urn:miriam:ncbigene:10987;urn:miriam:ncbigene:10987;urn:miriam:hgnc.symbol:COPS5;urn:miriam:refseq:NM_006837;urn:miriam:hgnc.symbol:COPS5"
      hgnc "HGNC_SYMBOL:COPS6;HGNC_SYMBOL:COPS7A;HGNC_SYMBOL:COPS8;HGNC_SYMBOL:COPS2;HGNC_SYMBOL:COPS4;HGNC_SYMBOL:vpr;HGNC_SYMBOL:COPS5"
      map_id "M115_56"
      name "COPS"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa41"
      uniprot "UNIPROT:Q7L5N1;UNIPROT:Q9UBW8;UNIPROT:Q99627;UNIPROT:P61201;UNIPROT:Q9BT78;UNIPROT:I2A5W5;UNIPROT:Q92905"
    ]
    graphics [
      x 2672.5
      y 686.1931880766348
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:UBQLN4;urn:miriam:hgnc.symbol:UBQLN4;urn:miriam:refseq:NM_020131;urn:miriam:uniprot:Q9NRR5;urn:miriam:hgnc:1237;urn:miriam:ncbigene:56893;urn:miriam:ncbigene:56893;urn:miriam:ensembl:ENSG00000160803"
      hgnc "HGNC_SYMBOL:UBQLN4"
      map_id "M115_263"
      name "UBQLN4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1108"
      uniprot "UNIPROT:Q9NRR5"
    ]
    graphics [
      x 1592.4025537851148
      y 2729.738142841986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_263"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:16713569;urn:miriam:hgnc.symbol:UBQLN4;urn:miriam:hgnc.symbol:UBQLN4;urn:miriam:refseq:NM_020131;urn:miriam:uniprot:Q9NRR5;urn:miriam:hgnc:1237;urn:miriam:ncbigene:56893;urn:miriam:ncbigene:56893;urn:miriam:ensembl:ENSG00000160803;urn:miriam:hgnc:3176;urn:miriam:refseq:NM_001955;urn:miriam:uniprot:P05305;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:ensembl:ENSG00000078401"
      hgnc "HGNC_SYMBOL:UBQLN4;HGNC_SYMBOL:EDN1"
      map_id "M115_58"
      name "EDN1_minus_homo"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa43"
      uniprot "UNIPROT:Q9NRR5;UNIPROT:P05305"
    ]
    graphics [
      x 1202.5
      y 1767.3156502059937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      annotation "PUBMED:16713569"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_151"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10127"
      uniprot "NA"
    ]
    graphics [
      x 2748.7805189192004
      y 2162.4310147534843
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_151"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:16713569;urn:miriam:hgnc:3176;urn:miriam:refseq:NM_001955;urn:miriam:uniprot:P05305;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:ensembl:ENSG00000078401;urn:miriam:hgnc.symbol:UBQLN4;urn:miriam:hgnc.symbol:UBQLN4;urn:miriam:refseq:NM_020131;urn:miriam:uniprot:Q9NRR5;urn:miriam:hgnc:1237;urn:miriam:ncbigene:56893;urn:miriam:ncbigene:56893;urn:miriam:ensembl:ENSG00000160803"
      hgnc "HGNC_SYMBOL:EDN1;HGNC_SYMBOL:UBQLN4"
      map_id "M115_59"
      name "EDN1_minus_homo"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa44"
      uniprot "UNIPROT:P05305;UNIPROT:Q9NRR5"
    ]
    graphics [
      x 1651.4316445647612
      y 2817.1855687946227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:23589;urn:miriam:hgnc.symbol:ZNF503;urn:miriam:hgnc.symbol:ZNF503;urn:miriam:uniprot:Q96F45;urn:miriam:ensembl:ENSG00000165655;urn:miriam:refseq:NM_032772;urn:miriam:ncbigene:84858;urn:miriam:ncbigene:84858"
      hgnc "HGNC_SYMBOL:ZNF503"
      map_id "M115_377"
      name "ZNF503"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa964"
      uniprot "UNIPROT:Q96F45"
    ]
    graphics [
      x 1308.7805189192002
      y 2093.3146446645715
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_377"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:27705803;urn:miriam:hgnc:23589;urn:miriam:hgnc.symbol:ZNF503;urn:miriam:hgnc.symbol:ZNF503;urn:miriam:uniprot:Q96F45;urn:miriam:ensembl:ENSG00000165655;urn:miriam:refseq:NM_032772;urn:miriam:ncbigene:84858;urn:miriam:ncbigene:84858;urn:miriam:uniprot:P61962;urn:miriam:pubmed:16887337;urn:miriam:ensembl:ENSG00000136485;urn:miriam:hgnc.symbol:DCAF7;urn:miriam:hgnc.symbol:DCAF7;urn:miriam:ncbigene:10238;urn:miriam:ncbigene:10238;urn:miriam:refseq:NM_005828;urn:miriam:hgnc:30915;urn:miriam:pubmed:16949367"
      hgnc "HGNC_SYMBOL:ZNF503;HGNC_SYMBOL:DCAF7"
      map_id "M115_46"
      name "dcafznf"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa29"
      uniprot "UNIPROT:Q96F45;UNIPROT:P61962"
    ]
    graphics [
      x 1622.5
      y 1312.5365071385852
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:hgnc:23589;urn:miriam:hgnc.symbol:ZNF503;urn:miriam:hgnc.symbol:ZNF503;urn:miriam:uniprot:Q96F45;urn:miriam:ensembl:ENSG00000165655;urn:miriam:refseq:NM_032772;urn:miriam:ncbigene:84858;urn:miriam:ncbigene:84858"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:ZNF503"
      map_id "M115_44"
      name "znfcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa27"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q96F45"
    ]
    graphics [
      x 1848.7805189192002
      y 2522.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:Q15056;urn:miriam:pubmed:10585411;urn:miriam:hgnc.symbol:EIF4H;urn:miriam:hgnc.symbol:EIF4H;urn:miriam:ensembl:ENSG00000106682;urn:miriam:hgnc:12741;urn:miriam:pubmed:11418588;urn:miriam:ncbigene:7458;urn:miriam:refseq:NM_022170;urn:miriam:ncbigene:7458"
      hgnc "HGNC_SYMBOL:EIF4H"
      map_id "M115_384"
      name "EIF4H"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa985"
      uniprot "UNIPROT:Q15056"
    ]
    graphics [
      x 1082.5
      y 886.3529417498704
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_384"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:Q15056;urn:miriam:pubmed:10585411;urn:miriam:hgnc.symbol:EIF4H;urn:miriam:hgnc.symbol:EIF4H;urn:miriam:ensembl:ENSG00000106682;urn:miriam:hgnc:12741;urn:miriam:pubmed:11418588;urn:miriam:ncbigene:7458;urn:miriam:refseq:NM_022170;urn:miriam:ncbigene:7458"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:EIF4H"
      map_id "M115_42"
      name "eifcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa25"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q15056"
    ]
    graphics [
      x 2042.5
      y 1985.8526976066757
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:2.6.1.5;urn:miriam:hgnc:11573;urn:miriam:hgnc.symbol:TAT;urn:miriam:hgnc.symbol:TAT;urn:miriam:hgnc.symbol:tat;urn:miriam:ncbigene:6898;urn:miriam:ensembl:ENSG00000198650;urn:miriam:ncbigene:6898;urn:miriam:uniprot:A6MI22;urn:miriam:refseq:NM_000353;urn:miriam:uniprot:P17735;urn:miriam:uniprot:P17735;urn:miriam:taxonomy:11676"
      hgnc "HGNC_SYMBOL:TAT;HGNC_SYMBOL:tat"
      map_id "M115_250"
      name "TAT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1063"
      uniprot "UNIPROT:A6MI22;UNIPROT:P17735"
    ]
    graphics [
      x 2822.5
      y 947.3665828718408
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_250"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:8934526;urn:miriam:ec-code:2.6.1.5;urn:miriam:hgnc:11573;urn:miriam:hgnc.symbol:TAT;urn:miriam:hgnc.symbol:TAT;urn:miriam:hgnc.symbol:tat;urn:miriam:ncbigene:6898;urn:miriam:ensembl:ENSG00000198650;urn:miriam:ncbigene:6898;urn:miriam:uniprot:A6MI22;urn:miriam:refseq:NM_000353;urn:miriam:uniprot:P17735;urn:miriam:uniprot:P17735;urn:miriam:taxonomy:11676;urn:miriam:pubmed:10428810;urn:miriam:ncbigene:2963;urn:miriam:refseq:NM_004128;urn:miriam:ncbigene:2963;urn:miriam:hgnc:4653;urn:miriam:uniprot:P13984;urn:miriam:ec-code:3.6.4.12;urn:miriam:hgnc.symbol:GTF2F2;urn:miriam:hgnc.symbol:GTF2F2;urn:miriam:ensembl:ENSG00000188342"
      hgnc "HGNC_SYMBOL:TAT;HGNC_SYMBOL:tat;HGNC_SYMBOL:GTF2F2"
      map_id "M115_49"
      name "TAT_minus_HIV"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa33"
      uniprot "UNIPROT:A6MI22;UNIPROT:P17735;UNIPROT:P13984"
    ]
    graphics [
      x 3152.5
      y 1503.699630803535
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:2.3.1.48;urn:miriam:ncbigene:2959;urn:miriam:ncbigene:2959;urn:miriam:hgnc:4648;urn:miriam:uniprot:Q00403;urn:miriam:hgnc.symbol:GTF2B;urn:miriam:refseq:NM_001514;urn:miriam:hgnc.symbol:GTF2B;urn:miriam:ensembl:ENSG00000137947"
      hgnc "HGNC_SYMBOL:GTF2B"
      map_id "M115_249"
      name "GTF2B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1058"
      uniprot "UNIPROT:Q00403"
    ]
    graphics [
      x 2478.7805189192004
      y 2281.435024234829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_249"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:refseq:NM_000937;urn:miriam:ec-code:2.7.7.48;urn:miriam:uniprot:P30876;urn:miriam:ncbigene:5430;urn:miriam:ncbigene:5430;urn:miriam:ncbigene:5431;urn:miriam:hgnc.symbol:POLR2B;urn:miriam:hgnc:9187;urn:miriam:uniprot:P24928;urn:miriam:uniprot:P24928;urn:miriam:hgnc.symbol:POLR2A;urn:miriam:hgnc.symbol:POLR2A;urn:miriam:ensembl:ENSG00000181222;urn:miriam:ec-code:2.7.7.6"
      hgnc "HGNC_SYMBOL:POLR2B;HGNC_SYMBOL:POLR2A"
      map_id "M115_237"
      name "POLR2A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1000"
      uniprot "UNIPROT:P30876;UNIPROT:P24928"
    ]
    graphics [
      x 1892.5
      y 2102.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_237"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000047315;urn:miriam:hgnc.symbol:POLR2B;urn:miriam:refseq:NM_000938;urn:miriam:hgnc.symbol:POLR2B;urn:miriam:uniprot:P30876;urn:miriam:ncbigene:5431;urn:miriam:ncbigene:5431;urn:miriam:ec-code:2.7.7.6;urn:miriam:hgnc:9188"
      hgnc "HGNC_SYMBOL:POLR2B"
      map_id "M115_246"
      name "POLR2B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1055"
      uniprot "UNIPROT:P30876"
    ]
    graphics [
      x 2762.5
      y 1663.1797235393926
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_246"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:refseq:NM_002695;urn:miriam:uniprot:P19388;urn:miriam:ensembl:ENSG00000099817;urn:miriam:hgnc.symbol:POLR2E;urn:miriam:hgnc.symbol:POLR2E;urn:miriam:hgnc:9192;urn:miriam:ncbigene:5434;urn:miriam:ncbigene:5434"
      hgnc "HGNC_SYMBOL:POLR2E"
      map_id "M115_247"
      name "POLR2E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1056"
      uniprot "UNIPROT:P19388"
    ]
    graphics [
      x 2822.5
      y 1059.4923030083019
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_247"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:refseq:NM_002696;urn:miriam:hgnc.symbol:POLR2G;urn:miriam:hgnc.symbol:POLR2G;urn:miriam:hgnc:9194;urn:miriam:ncbigene:5436;urn:miriam:ncbigene:5436;urn:miriam:uniprot:P62487;urn:miriam:ensembl:ENSG00000168002"
      hgnc "HGNC_SYMBOL:POLR2G"
      map_id "M115_248"
      name "POLR2G"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1057"
      uniprot "UNIPROT:P62487"
    ]
    graphics [
      x 1532.5
      y 599.738142841986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_248"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:26344197;urn:miriam:refseq:NM_002695;urn:miriam:uniprot:P19388;urn:miriam:ensembl:ENSG00000099817;urn:miriam:hgnc.symbol:POLR2E;urn:miriam:hgnc.symbol:POLR2E;urn:miriam:hgnc:9192;urn:miriam:ncbigene:5434;urn:miriam:ncbigene:5434;urn:miriam:refseq:NM_002696;urn:miriam:hgnc.symbol:POLR2G;urn:miriam:hgnc.symbol:POLR2G;urn:miriam:hgnc:9194;urn:miriam:ncbigene:5436;urn:miriam:ncbigene:5436;urn:miriam:uniprot:P62487;urn:miriam:ensembl:ENSG00000168002;urn:miriam:ec-code:2.3.1.48;urn:miriam:ncbigene:2959;urn:miriam:ncbigene:2959;urn:miriam:hgnc:4648;urn:miriam:uniprot:Q00403;urn:miriam:hgnc.symbol:GTF2B;urn:miriam:refseq:NM_001514;urn:miriam:hgnc.symbol:GTF2B;urn:miriam:ensembl:ENSG00000137947;urn:miriam:pubmed:10428810;urn:miriam:ncbigene:2963;urn:miriam:refseq:NM_004128;urn:miriam:ncbigene:2963;urn:miriam:hgnc:4653;urn:miriam:uniprot:P13984;urn:miriam:ec-code:3.6.4.12;urn:miriam:hgnc.symbol:GTF2F2;urn:miriam:hgnc.symbol:GTF2F2;urn:miriam:ensembl:ENSG00000188342;urn:miriam:ensembl:ENSG00000047315;urn:miriam:hgnc.symbol:POLR2B;urn:miriam:refseq:NM_000938;urn:miriam:hgnc.symbol:POLR2B;urn:miriam:uniprot:P30876;urn:miriam:ncbigene:5431;urn:miriam:ncbigene:5431;urn:miriam:ec-code:2.7.7.6;urn:miriam:hgnc:9188;urn:miriam:refseq:NM_000937;urn:miriam:ec-code:2.7.7.48;urn:miriam:uniprot:P30876;urn:miriam:ncbigene:5430;urn:miriam:ncbigene:5430;urn:miriam:ncbigene:5431;urn:miriam:hgnc.symbol:POLR2B;urn:miriam:hgnc:9187;urn:miriam:uniprot:P24928;urn:miriam:uniprot:P24928;urn:miriam:hgnc.symbol:POLR2A;urn:miriam:hgnc.symbol:POLR2A;urn:miriam:ensembl:ENSG00000181222;urn:miriam:ec-code:2.7.7.6"
      hgnc "HGNC_SYMBOL:POLR2E;HGNC_SYMBOL:POLR2G;HGNC_SYMBOL:GTF2B;HGNC_SYMBOL:GTF2F2;HGNC_SYMBOL:POLR2B;HGNC_SYMBOL:POLR2A"
      map_id "M115_34"
      name "gtfrnapoly"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa13"
      uniprot "UNIPROT:P19388;UNIPROT:P62487;UNIPROT:Q00403;UNIPROT:P13984;UNIPROT:P30876;UNIPROT:P24928"
    ]
    graphics [
      x 902.5
      y 1401.3808877744325
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 82
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_382"
      target_id "M115_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 83
    source 1
    target 3
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_382"
      target_id "M115_138"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 84
    source 1
    target 4
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_382"
      target_id "M115_150"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 85
    source 76
    target 2
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_249"
      target_id "M115_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 86
    source 77
    target 2
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_237"
      target_id "M115_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 87
    source 78
    target 2
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_246"
      target_id "M115_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 88
    source 79
    target 2
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_247"
      target_id "M115_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 89
    source 80
    target 2
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_248"
      target_id "M115_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 90
    source 2
    target 81
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_119"
      target_id "M115_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 91
    source 74
    target 3
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_250"
      target_id "M115_138"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 92
    source 3
    target 75
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_138"
      target_id "M115_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 93
    source 5
    target 4
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_365"
      target_id "M115_150"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 94
    source 4
    target 6
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_150"
      target_id "M115_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 95
    source 5
    target 7
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_365"
      target_id "M115_146"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 96
    source 5
    target 8
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_365"
      target_id "M115_144"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 97
    source 5
    target 9
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_365"
      target_id "M115_145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 98
    source 5
    target 10
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_365"
      target_id "M115_148"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 99
    source 72
    target 7
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_384"
      target_id "M115_146"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 100
    source 7
    target 73
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_146"
      target_id "M115_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 69
    target 8
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_377"
      target_id "M115_144"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 8
    target 71
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_144"
      target_id "M115_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 17
    target 9
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_383"
      target_id "M115_145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 9
    target 18
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_145"
      target_id "M115_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 11
    target 10
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_381"
      target_id "M115_148"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 106
    source 10
    target 12
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_148"
      target_id "M115_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 107
    source 11
    target 13
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_381"
      target_id "M115_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 108
    source 14
    target 13
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_252"
      target_id "M115_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 109
    source 15
    target 13
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_251"
      target_id "M115_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 110
    source 13
    target 16
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_120"
      target_id "M115_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 111
    source 17
    target 19
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_383"
      target_id "M115_142"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 112
    source 17
    target 20
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CATALYSIS"
      source_id "M115_383"
      target_id "M115_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 113
    source 69
    target 19
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_377"
      target_id "M115_142"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 114
    source 19
    target 70
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_142"
      target_id "M115_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 21
    target 20
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_240"
      target_id "M115_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 20
    target 22
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_116"
      target_id "M115_239"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 22
    target 23
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_239"
      target_id "M115_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 22
    target 24
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_239"
      target_id "M115_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 23
    target 27
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_117"
      target_id "M115_238"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 24
    target 25
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_121"
      target_id "M115_254"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 25
    target 26
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_254"
      target_id "M115_137"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 27
    target 26
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_238"
      target_id "M115_137"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 26
    target 28
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_137"
      target_id "M115_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 28
    target 29
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_51"
      target_id "M115_133"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 28
    target 30
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_51"
      target_id "M115_134"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 126
    source 28
    target 31
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_51"
      target_id "M115_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 127
    source 28
    target 32
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_51"
      target_id "M115_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 128
    source 28
    target 33
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_51"
      target_id "M115_136"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 65
    target 29
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_263"
      target_id "M115_133"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 130
    source 29
    target 66
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_133"
      target_id "M115_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 131
    source 53
    target 30
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_257"
      target_id "M115_134"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 132
    source 30
    target 54
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_134"
      target_id "M115_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 133
    source 31
    target 37
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_122"
      target_id "M115_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 134
    source 32
    target 36
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_123"
      target_id "M115_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 135
    source 34
    target 33
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_255"
      target_id "M115_136"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 136
    source 33
    target 35
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_136"
      target_id "M115_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 137
    source 37
    target 38
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_53"
      target_id "M115_132"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 138
    source 39
    target 38
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_264"
      target_id "M115_132"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 139
    source 38
    target 40
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_132"
      target_id "M115_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 39
    target 41
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_264"
      target_id "M115_159"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 39
    target 42
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_264"
      target_id "M115_160"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 39
    target 43
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_264"
      target_id "M115_162"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 39
    target 44
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_264"
      target_id "M115_161"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 144
    source 51
    target 41
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_279"
      target_id "M115_159"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 145
    source 41
    target 52
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_159"
      target_id "M115_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 146
    source 49
    target 42
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_280"
      target_id "M115_160"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 147
    source 42
    target 50
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_160"
      target_id "M115_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 148
    source 47
    target 43
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_282"
      target_id "M115_162"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 149
    source 43
    target 48
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_162"
      target_id "M115_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 150
    source 45
    target 44
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_281"
      target_id "M115_161"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 151
    source 44
    target 46
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_161"
      target_id "M115_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 152
    source 53
    target 55
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_257"
      target_id "M115_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 153
    source 56
    target 55
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_261"
      target_id "M115_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 154
    source 57
    target 55
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_260"
      target_id "M115_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 155
    source 58
    target 55
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_258"
      target_id "M115_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 156
    source 59
    target 55
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_259"
      target_id "M115_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 157
    source 60
    target 55
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_262"
      target_id "M115_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 158
    source 55
    target 61
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_124"
      target_id "M115_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 159
    source 61
    target 62
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_55"
      target_id "M115_135"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 160
    source 63
    target 62
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_256"
      target_id "M115_135"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 161
    source 62
    target 64
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_135"
      target_id "M115_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 162
    source 66
    target 67
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_58"
      target_id "M115_151"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 163
    source 67
    target 68
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_151"
      target_id "M115_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
