# generated with VANTED V2.8.2 at Fri Mar 04 10:04:34 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_20"
      name "nsp8_space_(I)"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph49"
      uniprot "NA"
    ]
    graphics [
      x 2342.5
      y 2102.963502381953
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_48"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "glyph82"
      uniprot "NA"
    ]
    graphics [
      x 3092.5
      y 752.9047032334182
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_41"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph76"
      uniprot "NA"
    ]
    graphics [
      x 1398.7805189192002
      y 2362.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_22"
      name "nsp8_space_(II)"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph50"
      uniprot "NA"
    ]
    graphics [
      x 2372.5
      y 952.0608348716244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_9"
      name "nsp7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph33"
      uniprot "NA"
    ]
    graphics [
      x 1142.5
      y 1464.9910422829782
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_45"
      name "complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "glyph8"
      uniprot "NA"
    ]
    graphics [
      x 2508.7805189192004
      y 2312.97150358576
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_42"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph77"
      uniprot "NA"
    ]
    graphics [
      x 2321.043215136001
      y 2649.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_21"
      name "CoV_space_poly_minus__br_merase_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "glyph5"
      uniprot "NA"
    ]
    graphics [
      x 1571.043215136001
      y 2519.738142841986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_23"
      name "nsp9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph51"
      uniprot "NA"
    ]
    graphics [
      x 2552.5
      y 1314.576720665756
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_1"
      name "(_plus_)gRNA_space_(including_space_ORF1a_space_ORF1b)"
      node_subtype "GENE"
      node_type "species"
      org_id "glyph13"
      uniprot "NA"
    ]
    graphics [
      x 1668.7805189192002
      y 2247.1855687946227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_8"
      name "Replication_space_transcription_space_complex_space_"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "glyph3"
      uniprot "NA"
    ]
    graphics [
      x 2372.5
      y 2062.060834871624
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph81"
      uniprot "NA"
    ]
    graphics [
      x 3032.5
      y 2011.2922471284464
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_52"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph86"
      uniprot "NA"
    ]
    graphics [
      x 2252.5
      y 1652.4583143700918
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_36"
      name "deg"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "glyph71"
      uniprot "NA"
    ]
    graphics [
      x 2208.7805189192004
      y 2333.713309864131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_6"
      name "Replication_space_transcription_space_complex_space_"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "glyph2"
      uniprot "NA"
    ]
    graphics [
      x 2822.5
      y 1748.4007591851837
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_46"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph80"
      uniprot "NA"
    ]
    graphics [
      x 1502.5
      y 1897.3397236403869
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_14"
      name "CoV_space_poly_minus__br_merase_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "glyph4"
      uniprot "NA"
    ]
    graphics [
      x 632.5
      y 1117.077935484341
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_28"
      name "complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "glyph6"
      uniprot "NA"
    ]
    graphics [
      x 1218.7805189192002
      y 2337.3156502059937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_17"
      name "nsp9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph42"
      uniprot "NA"
    ]
    graphics [
      x 2552.5
      y 2154.576720665756
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_3"
      name "(_plus_)gRNA_space_(including_space_ORF1a_space_ORF1b)"
      node_subtype "GENE"
      node_type "species"
      org_id "glyph17"
      uniprot "NA"
    ]
    graphics [
      x 1742.5
      y 747.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_2"
      name "(_minus_)gRNA_space_(including_space_ORF1a_space_ORF1b)"
      node_subtype "GENE"
      node_type "species"
      org_id "glyph16"
      uniprot "NA"
    ]
    graphics [
      x 2372.5
      y 1192.0608348716244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_43"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph78"
      uniprot "NA"
    ]
    graphics [
      x 1502.5
      y 836.4103423706738
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_49"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph83"
      uniprot "NA"
    ]
    graphics [
      x 1142.5
      y 584.361809414056
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_34"
      name "deg"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "glyph69"
      uniprot "NA"
    ]
    graphics [
      x 2255.815548054559
      y 639.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_19"
      name "pp1a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph46"
      uniprot "NA"
    ]
    graphics [
      x 422.5
      y 1090.716725563188
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_37"
      name "deg"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "glyph72"
      uniprot "NA"
    ]
    graphics [
      x 1022.5
      y 1387.8045420565481
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_25"
      name "pp1ab"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph53"
      uniprot "NA"
    ]
    graphics [
      x 1202.5
      y 1197.3156502059937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_39"
      name "deg"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "glyph74"
      uniprot "NA"
    ]
    graphics [
      x 2208.7805189192004
      y 2153.713309864131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_7"
      name "(_plus_)sgRNA_space_(2_minus_9)"
      node_subtype "GENE"
      node_type "species"
      org_id "glyph20"
      uniprot "NA"
    ]
    graphics [
      x 2162.5
      y 1763.7133098641311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph88"
      uniprot "NA"
    ]
    graphics [
      x 1899.238984217479
      y 2042.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_40"
      name "deg"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "glyph75"
      uniprot "NA"
    ]
    graphics [
      x 992.5
      y 1353.2744018749
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_5"
      name "viral_space_accessory"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph19"
      uniprot "NA"
    ]
    graphics [
      x 1412.5
      y 982.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_4"
      name "structural_space_proteins"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph18"
      uniprot "NA"
    ]
    graphics [
      x 1451.043215136001
      y 2722.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_51"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph85"
      uniprot "NA"
    ]
    graphics [
      x 572.5
      y 1739.0990577074945
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_53"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph87"
      uniprot "NA"
    ]
    graphics [
      x 2552.5
      y 1254.576720665756
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_35"
      name "deg"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "glyph70"
      uniprot "NA"
    ]
    graphics [
      x 2312.5
      y 1269.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_15"
      name "pp1ab"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph40"
      uniprot "NA"
    ]
    graphics [
      x 2762.5
      y 1795.8267364055584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_44"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "glyph79"
      uniprot "NA"
    ]
    graphics [
      x 2612.5
      y 2048.4291303703953
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_30"
      name "nsp12"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph62"
      uniprot "NA"
    ]
    graphics [
      x 3242.5
      y 2170.296191534207
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_26"
      name "nsp14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph54"
      uniprot "NA"
    ]
    graphics [
      x 2538.7805189192004
      y 2402.97150358576
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_10"
      name "nsp13"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph34"
      uniprot "NA"
    ]
    graphics [
      x 1412.5
      y 1852.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_11"
      name "nsp16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph36"
      uniprot "NA"
    ]
    graphics [
      x 2522.5
      y 992.9715035857598
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_24"
      name "nsp15"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph52"
      uniprot "NA"
    ]
    graphics [
      x 2882.5
      y 1743.858034494286
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_50"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph84"
      uniprot "NA"
    ]
    graphics [
      x 2612.5
      y 2138.4291303703953
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_29"
      name "nsp10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph61"
      uniprot "NA"
    ]
    graphics [
      x 1292.5
      y 1193.3146446645712
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_38"
      name "deg"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "glyph73"
      uniprot "NA"
    ]
    graphics [
      x 1682.5
      y 1587.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_31"
      name "pp1a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph63"
      uniprot "NA"
    ]
    graphics [
      x 2102.5
      y 785.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_13"
      name "nsp1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph39"
      uniprot "NA"
    ]
    graphics [
      x 2012.5
      y 1052.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_12"
      name "nsp2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph37"
      uniprot "NA"
    ]
    graphics [
      x 2642.5
      y 647.7682304796128
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_18"
      name "nsp3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph45"
      uniprot "NA"
    ]
    graphics [
      x 3482.5
      y 1273.970196409145
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_32"
      name "nsp11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph66"
      uniprot "NA"
    ]
    graphics [
      x 2492.5
      y 901.4350242348289
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_27"
      name "nsp6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph56"
      uniprot "NA"
    ]
    graphics [
      x 2419.8951650312483
      y 481.4350242348289
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_33"
      name "nsp5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph67"
      uniprot "NA"
    ]
    graphics [
      x 3272.5
      y 977.2363202812028
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_16"
      name "nsp4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph41"
      uniprot "NA"
    ]
    graphics [
      x 2737.4327087287393
      y 576.1910186689809
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 55
    source 2
    target 1
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "M15_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 56
    source 1
    target 3
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_20"
      target_id "M15_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 57
    source 47
    target 2
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_31"
      target_id "M15_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 58
    source 2
    target 4
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "M15_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 59
    source 2
    target 5
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "M15_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 60
    source 2
    target 9
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "M15_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 61
    source 2
    target 48
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "M15_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 62
    source 2
    target 49
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "M15_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 63
    source 2
    target 50
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "M15_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 64
    source 2
    target 51
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "M15_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 2
    target 52
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "M15_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 2
    target 53
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "M15_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 2
    target 54
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "M15_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 2
    target 45
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "M15_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 69
    source 4
    target 3
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_22"
      target_id "M15_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 70
    source 5
    target 3
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_9"
      target_id "M15_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 71
    source 3
    target 6
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_41"
      target_id "M15_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 72
    source 6
    target 7
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_45"
      target_id "M15_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 73
    source 8
    target 7
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_21"
      target_id "M15_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 74
    source 9
    target 7
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_23"
      target_id "M15_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 75
    source 10
    target 7
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_1"
      target_id "M15_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 76
    source 7
    target 11
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_42"
      target_id "M15_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 77
    source 44
    target 8
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_50"
      target_id "M15_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 78
    source 10
    target 34
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "TRIGGER"
      source_id "M15_1"
      target_id "M15_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 79
    source 10
    target 35
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "TRIGGER"
      source_id "M15_1"
      target_id "M15_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 11
    target 12
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "TRIGGER"
      source_id "M15_8"
      target_id "M15_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 81
    source 11
    target 13
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_8"
      target_id "M15_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 82
    source 28
    target 12
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_39"
      target_id "M15_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 83
    source 12
    target 29
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_47"
      target_id "M15_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 84
    source 14
    target 13
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_36"
      target_id "M15_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 85
    source 13
    target 15
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_52"
      target_id "M15_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 86
    source 15
    target 16
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_6"
      target_id "M15_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 87
    source 16
    target 17
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_46"
      target_id "M15_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 88
    source 16
    target 18
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_46"
      target_id "M15_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 89
    source 16
    target 19
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_46"
      target_id "M15_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 90
    source 16
    target 20
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_46"
      target_id "M15_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 91
    source 16
    target 21
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_46"
      target_id "M15_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 92
    source 20
    target 22
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "TRIGGER"
      source_id "M15_3"
      target_id "M15_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 93
    source 20
    target 23
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "TRIGGER"
      source_id "M15_3"
      target_id "M15_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 94
    source 26
    target 22
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_37"
      target_id "M15_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 95
    source 22
    target 27
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_43"
      target_id "M15_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 96
    source 24
    target 23
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_34"
      target_id "M15_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 97
    source 23
    target 25
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_49"
      target_id "M15_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 98
    source 29
    target 30
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "TRIGGER"
      source_id "M15_7"
      target_id "M15_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 99
    source 31
    target 30
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_40"
      target_id "M15_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 100
    source 30
    target 32
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_54"
      target_id "M15_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 30
    target 33
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_54"
      target_id "M15_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 46
    target 34
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_38"
      target_id "M15_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 34
    target 47
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_51"
      target_id "M15_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 36
    target 35
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_35"
      target_id "M15_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 35
    target 37
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_53"
      target_id "M15_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 106
    source 37
    target 38
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_15"
      target_id "M15_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 107
    source 38
    target 39
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_44"
      target_id "M15_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 108
    source 38
    target 40
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_44"
      target_id "M15_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 109
    source 38
    target 41
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_44"
      target_id "M15_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 110
    source 38
    target 42
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_44"
      target_id "M15_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 111
    source 38
    target 43
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_44"
      target_id "M15_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 112
    source 39
    target 44
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_30"
      target_id "M15_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 113
    source 40
    target 44
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_26"
      target_id "M15_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 114
    source 45
    target 44
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_29"
      target_id "M15_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
