# generated with VANTED V2.8.2 at Fri Mar 04 10:04:33 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30616;urn:miriam:reactome:R-ALL-113592"
      hgnc "NA"
      map_id "R2_82"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2307"
      uniprot "NA"
    ]
    graphics [
      x 1022.5
      y 1666.7111332684874
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694471;PUBMED:30918070;PUBMED:10799579;PUBMED:32330414;PUBMED:27760233"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_345"
      name "Polyadenylation of SARS-CoV-2 genomic RNA (plus strand)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694471__layout_2306"
      uniprot "NA"
    ]
    graphics [
      x 1592.5
      y 1613.119912305107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_345"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9694619;urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9684636"
      hgnc "NA"
      map_id "R2_81"
      name "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_2305"
      uniprot "NA"
    ]
    graphics [
      x 332.5
      y 2075.9106481566537
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9685518;urn:miriam:reactome:R-COV-9694393"
      hgnc "NA"
      map_id "R2_63"
      name "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_2277"
      uniprot "NA"
    ]
    graphics [
      x 1082.5
      y 1426.3529417498703
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A33019;urn:miriam:reactome:R-ALL-111294"
      hgnc "NA"
      map_id "R2_83"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2308"
      uniprot "NA"
    ]
    graphics [
      x 1578.7805189192002
      y 2333.119912305107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694549;PUBMED:25197083"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_361"
      name "RTC completes synthesis of the minus strand genomic RNA complement"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694549__layout_2275"
      uniprot "NA"
    ]
    graphics [
      x 2192.5
      y 1373.7133098641311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_361"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694455;PUBMED:24418573;PUBMED:17002283;PUBMED:15147189;PUBMED:17379242;PUBMED:15849181;PUBMED:18456656;PUBMED:15094372;PUBMED:19052082;PUBMED:16214138;PUBMED:16103198;PUBMED:15020242;PUBMED:18561946;PUBMED:23717688;PUBMED:17881296;PUBMED:18631359;PUBMED:16228284"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_343"
      name "SUMO-p-N protein dimer binds genomic RNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694455__layout_2464"
      uniprot "NA"
    ]
    graphics [
      x 1082.5
      y 1246.3529417498703
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_343"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9686056;urn:miriam:reactome:R-COV-9694464;urn:miriam:uniprot:P0DTC9"
      hgnc "NA"
      map_id "R2_106"
      name "N_space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2364"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 1878.7805189192002
      y 2522.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9684230;urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9694402;urn:miriam:uniprot:P0DTC9"
      hgnc "NA"
      map_id "R2_161"
      name "SUMO_minus_p_minus_N_space_dimer:SARS_minus_CoV_minus_2_space_genomic_space_RNA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2465"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 1622.5
      y 1852.5365071385852
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_161"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694281;PUBMED:24418573;PUBMED:23717688;PUBMED:17229691;PUBMED:17379242;PUBMED:16873249"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_309"
      name "Encapsidation of SARS coronavirus genomic RNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694281__layout_2466"
      uniprot "NA"
    ]
    graphics [
      x 1886.1439857514508
      y 1412.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_309"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9684199;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:P0DTC9;urn:miriam:reactome:R-COV-9694612"
      hgnc "NA"
      map_id "R2_162"
      name "encapsidated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2467"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 1952.5
      y 572.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_162"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694444;PUBMED:16684538;PUBMED:18703211;PUBMED:19322648;PUBMED:16254320;PUBMED:15474033;PUBMED:15147946;PUBMED:18792806;PUBMED:31226023;PUBMED:16877062;PUBMED:16507314;PUBMED:17530462;PUBMED:15713601;PUBMED:15351485;PUBMED:31133031;PUBMED:25855243;PUBMED:18753196;PUBMED:16343974;PUBMED:15507643"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_338"
      name "E and N are recruited to the M lattice"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694444__layout_2470"
      uniprot "NA"
    ]
    graphics [
      x 2672.5
      y 1047.0720280092366
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_338"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9684216;urn:miriam:reactome:R-COV-9694371;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "R2_163"
      name "M_space_lattice"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2469"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 2612.5
      y 1838.4291303703953
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_163"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9683621;urn:miriam:reactome:R-COV-9694408;urn:miriam:uniprot:P0DTC4"
      hgnc "NA"
      map_id "R2_132"
      name "Ub_minus_3xPalmC_minus_E_space_pentamer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2414"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 1532.5
      y 929.738142841986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9684226;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:P0DTC9;urn:miriam:reactome:R-COV-9694491;urn:miriam:uniprot:P0DTC5;urn:miriam:uniprot:P0DTC4"
      hgnc "NA"
      map_id "R2_164"
      name "M_space_lattice:E_space_protein:encapsidated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2472"
      uniprot "UNIPROT:P0DTC9;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      x 1982.5
      y 1382.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_164"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694553;PUBMED:24418573;PUBMED:22238235;PUBMED:17166901;PUBMED:27145752;PUBMED:25855243;PUBMED:20580052;PUBMED:20007283;PUBMED:18792806;PUBMED:16873249"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_363"
      name "Recruitment of Spike trimer to assembling virion"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694553__layout_2473"
      uniprot "NA"
    ]
    graphics [
      x 1502.5
      y 967.3397236403869
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_363"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9698334;urn:miriam:pubmed:32587972"
      hgnc "NA"
      map_id "R2_242"
      name "fully_space_glycosylated_space_Spike_space_trimer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3099"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 1112.5
      y 542.8215826773569
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_242"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9686310;urn:miriam:reactome:R-COV-9694321;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:P0DTC9;urn:miriam:uniprot:P0DTC5;urn:miriam:uniprot:P0DTC4"
      hgnc "NA"
      map_id "R2_165"
      name "S3:M:E:encapsidated_space__space_SARS_minus_CoV_minus_2_space_genomic_space_RNA:O_minus_glycosyl_space_3a_space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2475"
      uniprot "UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      x 1232.5
      y 901.6411877776235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_165"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694528;PUBMED:16840309;PUBMED:15807784;PUBMED:16894145;PUBMED:15781262;PUBMED:23202509;PUBMED:15194747"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_356"
      name "Accessory proteins are recruited to the maturing virion"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694528__layout_2476"
      uniprot "NA"
    ]
    graphics [
      x 1442.5
      y 694.2678925926325
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_356"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9694781;urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9686674"
      hgnc "NA"
      map_id "R2_126"
      name "O_minus_glycosyl_space_3a_space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2403"
      uniprot "UNIPROT:P0DTC3"
    ]
    graphics [
      x 2132.5
      y 935.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9694750;urn:miriam:uniprot:P0DTC7"
      hgnc "NA"
      map_id "R2_166"
      name "7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2477"
      uniprot "UNIPROT:P0DTC7"
    ]
    graphics [
      x 542.5
      y 1380.679699410001
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_166"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC3;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9684225;urn:miriam:reactome:R-COV-9694324;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:P0DTC9;urn:miriam:uniprot:P0DTC7;urn:miriam:uniprot:P0DTC5;urn:miriam:uniprot:P0DTC4"
      hgnc "NA"
      map_id "R2_167"
      name "S3:M:E:encapsidated_space__space_SARS_minus_CoV_minus_2_space_genomic_space_RNA:7a:O_minus_glycosyl_space_3a_space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2478"
      uniprot "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      x 572.5
      y 994.6867331485753
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_167"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694633;PUBMED:16877062;PUBMED:9658133;PUBMED:10799570;PUBMED:31133031;PUBMED:16254320;PUBMED:25855243;PUBMED:18792806"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_380"
      name "SARS virus buds into ERGIC lumen"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694633__layout_3799"
      uniprot "NA"
    ]
    graphics [
      x 1382.5
      y 1089.3284249102053
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_380"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC3;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9752958;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:P0DTC9;urn:miriam:uniprot:P0DTC7;urn:miriam:uniprot:P0DTC5;urn:miriam:uniprot:P0DTC4;urn:miriam:reactome:R-COV-9685539"
      hgnc "NA"
      map_id "R2_295"
      name "S3:M:E:encapsidated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA:_space_7a:O_minus_glycosyl_space_3a_space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3798"
      uniprot "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      x 2587.4327087287393
      y 558.5302073251539
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_295"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694641;PUBMED:31226023;PUBMED:16877062;PUBMED:25855243"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_381"
      name "Viral release"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694641__layout_2481"
      uniprot "NA"
    ]
    graphics [
      x 2240.1259087313247
      y 399.06618499081924
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_381"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC3;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9694500;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:P0DTC9;urn:miriam:uniprot:P0DTC7;urn:miriam:uniprot:P0DTC5;urn:miriam:uniprot:P0DTC4;urn:miriam:reactome:R-COV-9685506"
      hgnc "NA"
      map_id "R2_168"
      name "S3:M:E:encapsidated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA:_space_7a:O_minus_glycosyl_space_3a_space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2482"
      uniprot "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      x 992.5
      y 1070.4120397530464
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_168"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694662;PUBMED:32587976"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_384"
      name "Protein 3a forms a homotetramer"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694662__layout_2402"
      uniprot "NA"
    ]
    graphics [
      x 2462.5
      y 1381.435024234829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_384"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694572;PUBMED:19398035;PUBMED:15194747"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_367"
      name "3a localizes to the cell membrane"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694572__layout_2454"
      uniprot "NA"
    ]
    graphics [
      x 1365.4722923592326
      y 442.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_367"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9683640;urn:miriam:reactome:R-COV-9694386;urn:miriam:pubmed:16474139"
      hgnc "NA"
      map_id "R2_155"
      name "O_minus_glycosyl_space_3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2455"
      uniprot "UNIPROT:P0DTC3"
    ]
    graphics [
      x 1412.5
      y 622.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_155"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9683640;urn:miriam:reactome:R-COV-9694386;urn:miriam:pubmed:16474139"
      hgnc "NA"
      map_id "R2_124"
      name "O_minus_glycosyl_space_3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2399"
      uniprot "UNIPROT:P0DTC3"
    ]
    graphics [
      x 2312.5
      y 1569.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_124"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694718;PUBMED:16474139"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_389"
      name "O-glycosylation of 3a is terminated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694718__layout_2397"
      uniprot "NA"
    ]
    graphics [
      x 3092.5
      y 920.7966731765532
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_389"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9699093;urn:miriam:obo.chebi:CHEBI%3A16556"
      hgnc "NA"
      map_id "R2_249"
      name "CMP_minus_Neu5Ac"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_3350"
      uniprot "NA"
    ]
    graphics [
      x 2222.5
      y 2036.7158381002334
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_249"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9683709;urn:miriam:reactome:R-COV-9694658;urn:miriam:pubmed:16474139"
      hgnc "NA"
      map_id "R2_122"
      name "GalNAc_minus_O_minus_3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2395"
      uniprot "UNIPROT:P0DTC3"
    ]
    graphics [
      x 2912.5
      y 848.4085582493395
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:Q16842;urn:miriam:reactome:R-HSA-9683042;urn:miriam:uniprot:Q11201;urn:miriam:uniprot:Q11203;urn:miriam:uniprot:Q9UJ37;urn:miriam:uniprot:Q9H4F1;urn:miriam:uniprot:P15907;urn:miriam:uniprot:Q8NDV1;urn:miriam:uniprot:Q11206"
      hgnc "NA"
      map_id "R2_125"
      name "sialyltransferases"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2401"
      uniprot "UNIPROT:Q16842;UNIPROT:Q11201;UNIPROT:Q11203;UNIPROT:Q9UJ37;UNIPROT:Q9H4F1;UNIPROT:P15907;UNIPROT:Q8NDV1;UNIPROT:Q11206"
    ]
    graphics [
      x 2792.5
      y 1642.3725415703839
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_125"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17361;urn:miriam:reactome:R-ALL-9699094"
      hgnc "NA"
      map_id "R2_250"
      name "CMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_3351"
      uniprot "NA"
    ]
    graphics [
      x 1817.823920805403
      y 1442.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_250"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9697018;PUBMED:32366695"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_403"
      name "Addition of sialic acids on some Spike glycosyl sidechains"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9697018__layout_3054"
      uniprot "NA"
    ]
    graphics [
      x 1712.5
      y 1047.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_403"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9697194;urn:miriam:pubmed:32587972"
      hgnc "NA"
      map_id "R2_231"
      name "tri_minus_antennary_space_N_minus_glycan_minus_PALM_minus_Spike_space_trimer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3050"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 2282.5
      y 1116.211238590613
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_231"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:pubmed:32587972;urn:miriam:reactome:R-COV-9697197"
      hgnc "NA"
      map_id "R2_235"
      name "fully_space_glycosylated_space_Spike_space_trimer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3055"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 1922.5
      y 1652.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_235"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9696980;PUBMED:32366695"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_402"
      name "Spike trimer glycoside chains get additional branches"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9696980__layout_3049"
      uniprot "NA"
    ]
    graphics [
      x 872.5
      y 1571.989346974373
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_402"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:pubmed:32587972;urn:miriam:reactome:R-COV-9697195"
      hgnc "NA"
      map_id "R2_226"
      name "di_minus_antennary_space_N_minus_glycan_minus_PALM_minus_Spike_space_trimer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2956"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 1582.514108617874
      y 413.11991230510694
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_226"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-GGA-1028782;urn:miriam:reactome:R-HSA-1028782;urn:miriam:reactome:R-XTR-1028782;urn:miriam:reactome:R-DME-1028782;urn:miriam:uniprot:Q9BYC5;urn:miriam:reactome:R-DDI-1028782;urn:miriam:reactome:R-MMU-1028782;urn:miriam:reactome:R-DRE-1028782;urn:miriam:reactome:R-CEL-1028782;urn:miriam:reactome:R-RNO-1028782"
      hgnc "NA"
      map_id "R2_232"
      name "FUT8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_3051"
      uniprot "UNIPROT:Q9BYC5"
    ]
    graphics [
      x 1772.5
      y 1442.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_232"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-DME-975913;urn:miriam:reactome:R-BTA-975913;urn:miriam:reactome:R-HSA-975913;urn:miriam:reactome:R-GGA-975913;urn:miriam:reactome:R-DRE-975913;urn:miriam:reactome:R-RNO-975913;urn:miriam:reactome:R-SSC-975913;urn:miriam:uniprot:Q9UBM8;urn:miriam:uniprot:Q9UM21;urn:miriam:reactome:R-XTR-975913;urn:miriam:uniprot:Q9UQ53;urn:miriam:reactome:R-CFA-975913;urn:miriam:reactome:R-MMU-975913"
      hgnc "NA"
      map_id "R2_233"
      name "MGAT4s"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3052"
      uniprot "UNIPROT:Q9UBM8;UNIPROT:Q9UM21;UNIPROT:Q9UQ53"
    ]
    graphics [
      x 2058.7805189192004
      y 2225.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_233"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:Q09328;urn:miriam:reactome:R-CEL-975896;urn:miriam:reactome:R-RNO-975896;urn:miriam:reactome:R-GGA-975896;urn:miriam:reactome:R-DRE-975896;urn:miriam:reactome:R-MMU-975896;urn:miriam:reactome:R-SSC-975896;urn:miriam:reactome:R-CFA-975896;urn:miriam:reactome:R-BTA-975896;urn:miriam:reactome:R-HSA-975896;urn:miriam:reactome:R-XTR-975896"
      hgnc "NA"
      map_id "R2_234"
      name "MGAT5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_3053"
      uniprot "UNIPROT:Q09328"
    ]
    graphics [
      x 2342.5
      y 1172.963502381953
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_234"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694656;PUBMED:32366695"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_382"
      name "Spike trimer glycoside chains are extended"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694656__layout_2386"
      uniprot "NA"
    ]
    graphics [
      x 332.5
      y 1053.2589678558525
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_382"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696901;urn:miriam:pubmed:32587972"
      hgnc "NA"
      map_id "R2_208"
      name "high_minus_mannose_space_N_minus_glycan_minus_PALM_minus_Spike_space_trimer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2897"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 902.5
      y 1461.3808877744325
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_208"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-MMU-964750;urn:miriam:reactome:R-XTR-964750;urn:miriam:reactome:R-GGA-964750;urn:miriam:reactome:R-DME-964750;urn:miriam:reactome:R-CEL-964750;urn:miriam:reactome:R-DDI-964750;urn:miriam:uniprot:P26572;urn:miriam:reactome:R-BTA-964750;urn:miriam:reactome:R-CFA-964750;urn:miriam:reactome:R-DRE-964750;urn:miriam:reactome:R-RNO-964750;urn:miriam:reactome:R-SSC-964750;urn:miriam:reactome:R-HSA-964750"
      hgnc "NA"
      map_id "R2_119"
      name "MGAT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2390"
      uniprot "UNIPROT:P26572"
    ]
    graphics [
      x 752.5
      y 977.3084581890098
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-RNO-975823;urn:miriam:reactome:R-CFA-975823;urn:miriam:reactome:R-GGA-975823;urn:miriam:uniprot:Q16706;urn:miriam:reactome:R-HSA-975823;urn:miriam:reactome:R-DRE-975823;urn:miriam:reactome:R-XTR-975823;urn:miriam:reactome:R-MMU-975823;urn:miriam:reactome:R-SSC-975823"
      hgnc "NA"
      map_id "R2_215"
      name "MAN2A1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2905"
      uniprot "UNIPROT:Q16706"
    ]
    graphics [
      x 1052.5
      y 645.0278620931559
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_215"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-CEL-975830;urn:miriam:reactome:R-RNO-975830;urn:miriam:reactome:R-GGA-975830;urn:miriam:reactome:R-DRE-975830;urn:miriam:reactome:R-DME-975830;urn:miriam:reactome:R-MMU-975830;urn:miriam:reactome:R-CFA-975830;urn:miriam:reactome:R-BTA-975830;urn:miriam:reactome:R-HSA-975830;urn:miriam:reactome:R-XTR-975830;urn:miriam:uniprot:Q10469"
      hgnc "NA"
      map_id "R2_216"
      name "MGAT2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2906"
      uniprot "UNIPROT:Q10469"
    ]
    graphics [
      x 1382.5
      y 2026.9121908201519
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_216"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694780;PUBMED:31226023"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_396"
      name "Spike trimer translocates to ERGIC"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694780__layout_2384"
      uniprot "NA"
    ]
    graphics [
      x 2342.5
      y 722.963502381953
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_396"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9696883;urn:miriam:uniprot:P0DTC2;urn:miriam:pubmed:32587972"
      hgnc "NA"
      map_id "R2_207"
      name "high_minus_mannose_space_N_minus_glycan_minus_PALM_minus_Spike_space_trimer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2896"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 1742.5
      y 957.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_207"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694467;PUBMED:32587972"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_344"
      name "Spike protein forms a homotrimer"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694467__layout_2382"
      uniprot "NA"
    ]
    graphics [
      x 812.5
      y 900.0360504703245
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_344"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:pubmed:32366695;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696917"
      hgnc "NA"
      map_id "R2_206"
      name "high_minus_mannose_space_N_minus_glycan_minus_PALM_minus_Spike"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2895"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 1472.5
      y 1006.4685701968617
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_206"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694341;PUBMED:17134730;PUBMED:33310888;PUBMED:20580052"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_320"
      name "Spike protein gets palmitoylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694341__layout_2378"
      uniprot "NA"
    ]
    graphics [
      x 1878.7805189192002
      y 2642.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_320"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:pubmed:32366695;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696880"
      hgnc "NA"
      map_id "R2_205"
      name "high_minus_mannose_space_N_minus_glycan_space_folded_space_Spike"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2894"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 872.5
      y 1848.6818958846175
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_205"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9683736;urn:miriam:obo.chebi:CHEBI%3A15525"
      hgnc "NA"
      map_id "R2_115"
      name "palmitoyl_minus_CoA"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2379"
      uniprot "NA"
    ]
    graphics [
      x 2972.5
      y 1797.4318615488276
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-162743;urn:miriam:obo.chebi:CHEBI%3A57287"
      hgnc "NA"
      map_id "R2_116"
      name "CoA_minus_SH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2380"
      uniprot "NA"
    ]
    graphics [
      x 3182.5
      y 1697.7501341961922
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694401;PUBMED:22548323;PUBMED:16507314"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_332"
      name "E protein gets palmitoylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694401__layout_2406"
      uniprot "NA"
    ]
    graphics [
      x 1952.5
      y 782.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_332"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9683684;urn:miriam:reactome:R-COV-9694312;urn:miriam:uniprot:P0DTC4"
      hgnc "NA"
      map_id "R2_94"
      name "nascent_space_E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2335"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 3182.5
      y 1517.7501341961922
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9683597;urn:miriam:reactome:R-COV-9694305;urn:miriam:uniprot:P0DTC4"
      hgnc "NA"
      map_id "R2_128"
      name "3xPalmC_minus_E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2407"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 1595.7815919415286
      y 353.11991230510694
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694529;PUBMED:20409569"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_357"
      name "Ubiquination of protein E"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694529__layout_2408"
      uniprot "NA"
    ]
    graphics [
      x 1472.5
      y 1186.4685701968617
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_357"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P62987;urn:miriam:uniprot:P62979;urn:miriam:uniprot:P0CG48;urn:miriam:uniprot:P0CG47;urn:miriam:reactome:R-HSA-8943136"
      hgnc "NA"
      map_id "R2_129"
      name "Ub"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2409"
      uniprot "UNIPROT:P62987;UNIPROT:P62979;UNIPROT:P0CG48;UNIPROT:P0CG47"
    ]
    graphics [
      x 542.5
      y 1943.333672274809
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_129"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9694423;urn:miriam:reactome:R-COV-9683626;urn:miriam:uniprot:P0DTC4"
      hgnc "NA"
      map_id "R2_130"
      name "Ub_minus_3xPalmC_minus_E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2410"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 2012.5
      y 752.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_130"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694452;PUBMED:15522242;PUBMED:22819936"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_341"
      name "Protein E forms a homopentamer"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694452__layout_2411"
      uniprot "NA"
    ]
    graphics [
      x 3452.5
      y 1302.2295567798512
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_341"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9683621;urn:miriam:reactome:R-COV-9694408;urn:miriam:uniprot:P0DTC4"
      hgnc "NA"
      map_id "R2_131"
      name "Ub_minus_3xPalmC_minus_E_space_pentamer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2412"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 2942.5
      y 1282.6505321512923
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_131"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694294;PUBMED:34237302;PUBMED:21524776;PUBMED:33709461;PUBMED:21450821"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_311"
      name "E pentamer is transported to the Golgi"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694294__layout_2413"
      uniprot "NA"
    ]
    graphics [
      x 2282.5
      y 1513.5647643715495
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_311"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694280;PUBMED:32015508"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_308"
      name "mRNA4 is translated to protein E"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694280__layout_2333"
      uniprot "NA"
    ]
    graphics [
      x 1698.7805189192002
      y 2487.1855687946227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_308"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694790;PUBMED:16684538;PUBMED:20129637"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_397"
      name "E protein gets N-glycosylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694790__layout_2404"
      uniprot "NA"
    ]
    graphics [
      x 3002.5
      y 1161.7899809652147
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_397"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9683033"
      hgnc "NA"
      map_id "R2_107"
      name "nucleotide_minus_sugar"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "layout_2366"
      uniprot "NA"
    ]
    graphics [
      x 2227.4327087287393
      y 429.06618499081924
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-156540"
      hgnc "NA"
      map_id "R2_109"
      name "H_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2369"
      uniprot "NA"
    ]
    graphics [
      x 1202.5
      y 822.0185613371316
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:pubmed:16684538;urn:miriam:reactome:R-COV-9683652;urn:miriam:reactome:R-COV-9694754;urn:miriam:uniprot:P0DTC4"
      hgnc "NA"
      map_id "R2_127"
      name "N_minus_glycan_space_E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2405"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 2082.223682853007
      y 212.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_127"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9683046"
      hgnc "NA"
      map_id "R2_108"
      name "nucleoside_space_5'_minus_diphosphate(3âˆ’)"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "layout_2368"
      uniprot "NA"
    ]
    graphics [
      x 1802.5
      y 1052.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9694622;urn:miriam:reactome:R-COV-9685917"
      hgnc "NA"
      map_id "R2_93"
      name "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA4"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_2334"
      uniprot "NA"
    ]
    graphics [
      x 2462.5
      y 2041.435024234829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694337;PUBMED:22915798"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_318"
      name "Trimmed spike protein binds to calnexin"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694337__layout_2375"
      uniprot "NA"
    ]
    graphics [
      x 1142.5
      y 1854.9910422829782
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_318"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:pubmed:32366695;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696875"
      hgnc "NA"
      map_id "R2_213"
      name "high_minus_mannose_space_N_minus_glycan_space_unfolded_space_Spike"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2903"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 392.5
      y 1117.104474237206
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_213"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-195906;urn:miriam:uniprot:P27824;urn:miriam:reactome:R-BTA-195906;urn:miriam:reactome:R-CFA-195906;urn:miriam:reactome:R-SPO-195906;urn:miriam:reactome:R-SSC-195906;urn:miriam:reactome:R-RNO-195906;urn:miriam:reactome:R-GGA-195906;urn:miriam:reactome:R-DRE-195906;urn:miriam:reactome:R-CEL-195906;urn:miriam:reactome:R-DDI-195906;urn:miriam:reactome:R-MMU-195906;urn:miriam:reactome:R-DME-195906;urn:miriam:reactome:R-XTR-195906;urn:miriam:reactome:R-DME-195906-2;urn:miriam:reactome:R-SCE-195906;urn:miriam:reactome:R-SPO-195906-2;urn:miriam:reactome:R-DME-195906-3;urn:miriam:reactome:R-SPO-195906-3;urn:miriam:reactome:R-DME-195906-4"
      hgnc "NA"
      map_id "R2_114"
      name "CANX"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2377"
      uniprot "UNIPROT:P27824"
    ]
    graphics [
      x 872.5
      y 1383.0012911041538
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9696807;PUBMED:32366695;PUBMED:18003979"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_401"
      name "N-glycan mannose trimming of Spike"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9696807__layout_3043"
      uniprot "NA"
    ]
    graphics [
      x 902.5
      y 1249.451558880041
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_401"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9696892"
      hgnc "NA"
      map_id "R2_210"
      name "Man(9)_space_N_minus_glycan_space_unfolded_space_Spike"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2899"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 2132.5
      y 1055.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_210"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:Q9BV94;urn:miriam:reactome:R-HSA-6782581;urn:miriam:uniprot:Q9UKM7"
      hgnc "NA"
      map_id "R2_230"
      name "MAN1B1,EDEM2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3044"
      uniprot "UNIPROT:Q9BV94;UNIPROT:Q9UKM7"
    ]
    graphics [
      x 1922.5
      y 2102.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_230"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694364;PUBMED:32366695;PUBMED:10929008;PUBMED:12145188"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_324"
      name "N-glycan glucose trimming of Spike"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694364__layout_2370"
      uniprot "NA"
    ]
    graphics [
      x 632.5
      y 1087.077935484341
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_324"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9694459"
      hgnc "NA"
      map_id "R2_113"
      name "14_minus_sugar_space_N_minus_glycan_space_unfolded_space_Spike"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2376"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 1922.5
      y 1952.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-113519"
      hgnc "NA"
      map_id "R2_209"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2898"
      uniprot "NA"
    ]
    graphics [
      x 962.5
      y 1687.6441692926126
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_209"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-9682983;urn:miriam:uniprot:Q14697;urn:miriam:uniprot:P14314;urn:miriam:pubmed:25348530;urn:miriam:uniprot:Q13724"
      hgnc "NA"
      map_id "R2_111"
      name "ER_space_alpha_minus_glucosidases"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2373"
      uniprot "UNIPROT:Q14697;UNIPROT:P14314;UNIPROT:Q13724"
    ]
    graphics [
      x 1878.7805189192002
      y 2372.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-532676;urn:miriam:uniprot:Q13724"
      hgnc "NA"
      map_id "R2_214"
      name "MOGS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2904"
      uniprot "UNIPROT:Q13724"
    ]
    graphics [
      x 632.5
      y 1939.6861762223555
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_214"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-9686842;urn:miriam:uniprot:Q14697;urn:miriam:uniprot:P14314;urn:miriam:uniprot:Q13724"
      hgnc "NA"
      map_id "R2_112"
      name "ER_minus_alpha_space_glucosidases:ER_minus_alpha_space_glucosidase_space_inhibitors"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2374"
      uniprot "UNIPROT:Q14697;UNIPROT:P14314;UNIPROT:Q13724"
    ]
    graphics [
      x 842.5
      y 1494.6943946874349
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15903;urn:miriam:reactome:R-ALL-9683087"
      hgnc "NA"
      map_id "R2_110"
      name "beta_minus_D_minus_glucose"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2372"
      uniprot "NA"
    ]
    graphics [
      x 752.5
      y 1132.6097915613125
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9686790;PUBMED:16188993;PUBMED:24716661;PUBMED:23816430;PUBMED:19223639;PUBMED:23503623;PUBMED:7986008"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_300"
      name "ER-alpha glucosidases bind ER-alpha glucosidase inhibitors"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9686790__layout_2954"
      uniprot "NA"
    ]
    graphics [
      x 992.5
      y 860.4120397530463
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_300"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9686811"
      hgnc "NA"
      map_id "R2_225"
      name "ER_minus_alpha_minus_glucosidase_space_inhibitors"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_2955"
      uniprot "NA"
    ]
    graphics [
      x 2137.4327087287393
      y 503.71330986413113
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_225"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694793;PUBMED:11470266;PUBMED:32366695;PUBMED:32363391;PUBMED:32676595;PUBMED:32518941"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_399"
      name "Spike protein gets N-glycosylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694793__layout_2365"
      uniprot "NA"
    ]
    graphics [
      x 872.5
      y 1623.4129773986483
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_399"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-449651;urn:miriam:obo.chebi:CHEBI%3A53019"
      hgnc "NA"
      map_id "R2_211"
      name "(Glc)3_space_(GlcNAc)2_space_(Man)9_space_(PP_minus_Dol)1"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2901"
      uniprot "NA"
    ]
    graphics [
      x 648.7805189192002
      y 2342.1098071894694
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_211"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9694796"
      hgnc "NA"
      map_id "R2_90"
      name "nascent_space_Spike"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2329"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 2582.5
      y 2127.5083616454517
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P61803;urn:miriam:uniprot:P39656;urn:miriam:uniprot:Q13454;urn:miriam:uniprot:P46977;urn:miriam:reactome:R-HSA-532516;urn:miriam:uniprot:Q9H0U3;urn:miriam:uniprot:P04843;urn:miriam:uniprot:P04844"
      hgnc "NA"
      map_id "R2_217"
      name "OST_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2907"
      uniprot "UNIPROT:P61803;UNIPROT:P39656;UNIPROT:Q13454;UNIPROT:P46977;UNIPROT:Q9H0U3;UNIPROT:P04843;UNIPROT:P04844"
    ]
    graphics [
      x 1772.5
      y 662.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_217"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16214;urn:miriam:reactome:R-ALL-449311"
      hgnc "NA"
      map_id "R2_212"
      name "DOLP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2902"
      uniprot "NA"
    ]
    graphics [
      x 1668.7805189192002
      y 2127.1855687946227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_212"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694447;PUBMED:32015508"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_340"
      name "mRNA2 is translated to Spike"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694447__layout_2327"
      uniprot "NA"
    ]
    graphics [
      x 3272.5
      y 1286.1844956582163
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_340"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9694503;urn:miriam:reactome:R-COV-9685916"
      hgnc "NA"
      map_id "R2_89"
      name "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA2"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_2328"
      uniprot "NA"
    ]
    graphics [
      x 2792.5
      y 811.9085631847952
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694438;PUBMED:16474139"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_336"
      name "GalNAc is transferred onto 3a"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694438__layout_2393"
      uniprot "NA"
    ]
    graphics [
      x 2822.5
      y 2162.0569251114393
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_336"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9683691;urn:miriam:reactome:R-COV-9694716;urn:miriam:pubmed:16474139"
      hgnc "NA"
      map_id "R2_120"
      name "3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2392"
      uniprot "UNIPROT:P0DTC3"
    ]
    graphics [
      x 2001.0104666921623
      y 2942.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16846;urn:miriam:reactome:R-ALL-9683025"
      hgnc "NA"
      map_id "R2_121"
      name "UDP_minus_GalNAc"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2394"
      uniprot "NA"
    ]
    graphics [
      x 1671.3386501059647
      y 2787.1855687946227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:Q10472;urn:miriam:reactome:R-HSA-9682906"
      hgnc "NA"
      map_id "R2_123"
      name "GALNT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2396"
      uniprot "UNIPROT:Q10472"
    ]
    graphics [
      x 2538.7805189192004
      y 2312.97150358576
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-9683057"
      hgnc "NA"
      map_id "R2_117"
      name "H_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2388"
      uniprot "NA"
    ]
    graphics [
      x 2170.913546386292
      y 1013.7133098641311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17659;urn:miriam:reactome:R-ALL-9683078"
      hgnc "NA"
      map_id "R2_118"
      name "UDP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2389"
      uniprot "NA"
    ]
    graphics [
      x 2462.5
      y 1051.435024234829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694392;PUBMED:16474139"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_331"
      name "3a translocates to the ERGIC"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694392__layout_2391"
      uniprot "NA"
    ]
    graphics [
      x 1742.5
      y 1521.30614738341
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_331"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC3;urn:miriam:reactome:R-COV-9694584"
      hgnc "NA"
      map_id "R2_92"
      name "3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2332"
      uniprot "UNIPROT:P0DTC3"
    ]
    graphics [
      x 1442.5
      y 1715.1961954122032
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694794;PUBMED:32015508"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_400"
      name "mRNA3 is translated to protein 3a"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694794__layout_2330"
      uniprot "NA"
    ]
    graphics [
      x 1592.5
      y 1583.119912305107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_400"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9685914;urn:miriam:reactome:R-COV-9694325"
      hgnc "NA"
      map_id "R2_91"
      name "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA3"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_2331"
      uniprot "NA"
    ]
    graphics [
      x 2132.5
      y 1175.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694487;PUBMED:16442106;PUBMED:18703211;PUBMED:9658133;PUBMED:16254320;PUBMED:20154085;PUBMED:7721788;PUBMED:15474033;PUBMED:19534833;PUBMED:15147946;PUBMED:16877062;PUBMED:10799570;PUBMED:18753196;PUBMED:23700447;PUBMED:15507643"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_347"
      name "M protein oligomerization"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694487__layout_2468"
      uniprot "NA"
    ]
    graphics [
      x 2132.5
      y 1115.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_347"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9684213;urn:miriam:uniprot:P0DTC5;urn:miriam:reactome:R-COV-9694439"
      hgnc "NA"
      map_id "R2_133"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2416"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 1477.432708728739
      y 586.4685701968617
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_133"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9694446;urn:miriam:reactome:R-COV-9684206;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "R2_160"
      name "N_minus_glycan_space_M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2463"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 2282.5
      y 759.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_160"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694367;PUBMED:33203855;PUBMED:19534833"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_325"
      name "Glycosylated M localizes to the Golgi membrane"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694367__layout_2462"
      uniprot "NA"
    ]
    graphics [
      x 1901.9505261727695
      y 242.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_325"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9694446;urn:miriam:reactome:R-COV-9684206;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "R2_135"
      name "N_minus_glycan_space_M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2419"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 842.5
      y 739.9078969287905
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694525;PUBMED:16442106"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_355"
      name "M protein gets N-glycosylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694525__layout_2417"
      uniprot "NA"
    ]
    graphics [
      x 1644.5824145209654
      y 353.11991230510694
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_355"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9683586;urn:miriam:reactome:R-COV-9694279;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "R2_96"
      name "nascent_space_M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2338"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 2407.4327087287393
      y 662.469736994576
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9683033"
      hgnc "NA"
      map_id "R2_134"
      name "nucleotide_minus_sugar"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "layout_2418"
      uniprot "NA"
    ]
    graphics [
      x 2102.5
      y 1661.3440132206817
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-156540"
      hgnc "NA"
      map_id "R2_137"
      name "H_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2421"
      uniprot "NA"
    ]
    graphics [
      x 1502.5
      y 686.4103423706738
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_137"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9683046"
      hgnc "NA"
      map_id "R2_136"
      name "nucleoside_space_5'_minus_diphosphate(3âˆ’)"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "layout_2420"
      uniprot "NA"
    ]
    graphics [
      x 1382.5
      y 1602.4847495740523
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694681;PUBMED:32015508"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_387"
      name "mRNA5 is translated to protein M"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694681__layout_2336"
      uniprot "NA"
    ]
    graphics [
      x 1412.5
      y 1792.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_387"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694555;PUBMED:32198291;PUBMED:19534833"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_364"
      name "Protein M localizes to the Golgi membrane"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694555__layout_2415"
      uniprot "NA"
    ]
    graphics [
      x 2432.5
      y 1256.0089009888093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_364"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9685920;urn:miriam:reactome:R-COV-9694456;urn:miriam:refseq:MN908947.3"
      hgnc "NA"
      map_id "R2_95"
      name "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA5"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_2337"
      uniprot "NA"
    ]
    graphics [
      x 1202.5
      y 1347.3156502059937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694568;PUBMED:17210170"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_366"
      name "Nucleoprotein translocates to the ERGIC outer membrane"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694568__layout_2363"
      uniprot "NA"
    ]
    graphics [
      x 2072.5
      y 1025.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_366"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:pubmed:32654247;urn:miriam:reactome:R-COV-9694702;urn:miriam:uniprot:P0DTC9"
      hgnc "NA"
      map_id "R2_102"
      name "N_space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2352"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 572.5
      y 1509.9219450611738
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694363;PUBMED:32654247;PUBMED:33794152;PUBMED:33594360;PUBMED:32637943"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_323"
      name "Tetramerisation of nucleoprotein"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694363__layout_3642"
      uniprot "NA"
    ]
    graphics [
      x 1472.5
      y 946.4685701968617
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_323"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694575;PUBMED:16103198"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_368"
      name "Nucleoprotein translocates to the plasma membrane"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694575__layout_2351"
      uniprot "NA"
    ]
    graphics [
      x 1742.5
      y 1317.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_368"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9686056;urn:miriam:reactome:R-COV-9694464;urn:miriam:uniprot:P0DTC9"
      hgnc "NA"
      map_id "R2_103"
      name "N_space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2353"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 2222.5
      y 1526.7158381002334
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:pubmed:15848177;urn:miriam:reactome:R-COV-9682916;urn:miriam:reactome:R-COV-9729340;urn:miriam:uniprot:P0DTC9"
      hgnc "NA"
      map_id "R2_275"
      name "SUMO1_minus_K62_minus_ADPr_minus_p_minus_11S,2T_minus_metR95,177_minus_N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_3639"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 1982.5
      y 1682.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_275"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9729307;PUBMED:15848177"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_412"
      name "Nucleoprotein is SUMOylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9729307__layout_3638"
      uniprot "NA"
    ]
    graphics [
      x 2792.5
      y 1732.3725415703839
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_412"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-4656922;urn:miriam:reactome:R-XTR-4656922;urn:miriam:reactome:R-RNO-4656922;urn:miriam:reactome:R-MMU-4656922;urn:miriam:uniprot:P63279;urn:miriam:reactome:R-DRE-4656922;urn:miriam:uniprot:P63165;urn:miriam:reactome:R-DRE-4655342;urn:miriam:reactome:R-MMU-4655403;urn:miriam:uniprot:P63279;urn:miriam:reactome:R-DME-4655342;urn:miriam:reactome:R-RNO-4655342;urn:miriam:reactome:R-HSA-4655342;urn:miriam:reactome:R-XTR-4655342"
      hgnc "NA"
      map_id "R2_274"
      name "SUMO1:C93_minus_UBE2I"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3633"
      uniprot "UNIPROT:P63279;UNIPROT:P63165"
    ]
    graphics [
      x 1352.5
      y 1899.311461366438
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_274"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9729275;urn:miriam:reactome:R-COV-9686058;urn:miriam:uniprot:P0DTC9"
      hgnc "NA"
      map_id "R2_272"
      name "ADPr_minus_p_minus_11S,2T_minus_metR95,177_minus_N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_3616"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 992.5
      y 1483.7014358937463
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_272"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-DRE-4655342;urn:miriam:reactome:R-MMU-4655403;urn:miriam:uniprot:P63279;urn:miriam:reactome:R-DME-4655342;urn:miriam:reactome:R-RNO-4655342;urn:miriam:reactome:R-HSA-4655342;urn:miriam:reactome:R-XTR-4655342"
      hgnc "NA"
      map_id "R2_293"
      name "UBE2I"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_3792"
      uniprot "UNIPROT:P63279"
    ]
    graphics [
      x 3332.5
      y 1071.1794378682557
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_293"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9729279;PUBMED:29199039;PUBMED:32029454"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_409"
      name "Nucleoprotein is ADP-ribosylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9729279__layout_3615"
      uniprot "NA"
    ]
    graphics [
      x 1562.5
      y 945.4971941397312
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_409"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9729308;urn:miriam:uniprot:P0DTC9"
      hgnc "NA"
      map_id "R2_261"
      name "p_minus_11S,2T_minus_metR95,177_minus_N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_3529"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 812.5
      y 1785.825388295384
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_261"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57540;urn:miriam:reactome:R-ALL-29360"
      hgnc "NA"
      map_id "R2_149"
      name "NAD_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2443"
      uniprot "NA"
    ]
    graphics [
      x 2402.5
      y 976.0046064486716
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_149"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-CFA-8938273;urn:miriam:uniprot:Q53GL7;urn:miriam:reactome:R-HSA-8938273;urn:miriam:reactome:R-XTR-8938273;urn:miriam:uniprot:Q2NL67;urn:miriam:reactome:R-DDI-8938273;urn:miriam:uniprot:Q8N3A8;urn:miriam:reactome:R-GGA-8938273;urn:miriam:reactome:R-RNO-8938273;urn:miriam:reactome:R-MMU-8938273;urn:miriam:reactome:R-BTA-8938273;urn:miriam:reactome:R-DRE-8938273;urn:miriam:reactome:R-DME-8938259;urn:miriam:uniprot:Q8N5Y8;urn:miriam:uniprot:Q460N5;urn:miriam:uniprot:Q8IXQ6;urn:miriam:uniprot:Q9UKK3;urn:miriam:reactome:R-SSC-8938273"
      hgnc "NA"
      map_id "R2_151"
      name "PARPs"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2446"
      uniprot "UNIPROT:Q53GL7;UNIPROT:Q2NL67;UNIPROT:Q8N3A8;UNIPROT:Q8N5Y8;UNIPROT:Q460N5;UNIPROT:Q8IXQ6;UNIPROT:Q9UKK3"
    ]
    graphics [
      x 1530.6423670710196
      y 319.1765963310454
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_151"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17154;urn:miriam:reactome:R-ALL-197277"
      hgnc "NA"
      map_id "R2_150"
      name "NAM"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2444"
      uniprot "NA"
    ]
    graphics [
      x 1682.5
      y 1047.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_150"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-70106"
      hgnc "NA"
      map_id "R2_273"
      name "H_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_3624"
      uniprot "NA"
    ]
    graphics [
      x 1712.5
      y 1707.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_273"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9729283"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_410"
      name "Nucleoprotein is methylated by PRMT1"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9729283__layout_3539"
      uniprot "NA"
    ]
    graphics [
      x 1382.5
      y 1359.3284249102053
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_410"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9729264;urn:miriam:uniprot:P0DTC9;urn:miriam:pubmed:19106108"
      hgnc "NA"
      map_id "R2_260"
      name "p_minus_11S,2T_minus_N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_3528"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 1682.5
      y 987.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_260"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9639461;urn:miriam:obo.chebi:CHEBI%3A15414"
      hgnc "NA"
      map_id "R2_288"
      name "S_minus_adenosyl_minus_L_minus_methionine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_3787"
      uniprot "NA"
    ]
    graphics [
      x 1608.7805189192002
      y 2153.119912305107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_288"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-MMU-9632173;urn:miriam:reactome:R-CFA-9632173;urn:miriam:reactome:R-HSA-9632173;urn:miriam:reactome:R-DRE-9632173;urn:miriam:reactome:R-XTR-9632173;urn:miriam:uniprot:Q99873;urn:miriam:reactome:R-DME-9632173;urn:miriam:reactome:R-RNO-9632173"
      hgnc "NA"
      map_id "R2_289"
      name "PRMT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_3788"
      uniprot "UNIPROT:Q99873"
    ]
    graphics [
      x 2522.5
      y 1622.9715035857598
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_289"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16680;urn:miriam:reactome:R-ALL-9639443"
      hgnc "NA"
      map_id "R2_262"
      name "S_minus_adenosyl_minus_L_minus_homocysteine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_3538"
      uniprot "NA"
    ]
    graphics [
      x 2462.5
      y 1831.435024234829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_262"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-70106"
      hgnc "NA"
      map_id "R2_100"
      name "H_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2348"
      uniprot "NA"
    ]
    graphics [
      x 2285.815548054559
      y 579.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9729300;PUBMED:32817937;PUBMED:32645325;PUBMED:32877642;PUBMED:32723359"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_411"
      name "Unknown kinase phosphorylates nucleoprotein"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9729300__layout_3541"
      uniprot "NA"
    ]
    graphics [
      x 332.5
      y 993.2589678558526
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_411"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30616;urn:miriam:reactome:R-ALL-113592"
      hgnc "NA"
      map_id "R2_281"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_3773"
      uniprot "NA"
    ]
    graphics [
      x 812.5
      y 777.2808309408925
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_281"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9729277;urn:miriam:uniprot:P0DTC9;urn:miriam:pubmed:19106108"
      hgnc "NA"
      map_id "R2_263"
      name "p_minus_8S,2T_minus_N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_3542"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 1750.9135463862924
      y 902.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_263"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A456216;urn:miriam:reactome:R-ALL-29370"
      hgnc "NA"
      map_id "R2_282"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_3774"
      uniprot "NA"
    ]
    graphics [
      x 842.5
      y 1404.6943946874349
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_282"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-70106"
      hgnc "NA"
      map_id "R2_264"
      name "H_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_3547"
      uniprot "NA"
    ]
    graphics [
      x 362.5000000000002
      y 1373.0730545379486
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_264"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9729318;PUBMED:32817937;PUBMED:32645325;PUBMED:32877642;PUBMED:32723359"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_413"
      name "CSNK1A1 phosphorylates nucleoprotein"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9729318__layout_3550"
      uniprot "NA"
    ]
    graphics [
      x 3242.5
      y 1016.0426117444084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_413"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC9;urn:miriam:reactome:R-COV-9729335;urn:miriam:pubmed:19106108"
      hgnc "NA"
      map_id "R2_269"
      name "p_minus_8S_minus_N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_3574"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 1832.5
      y 1772.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_269"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30616;urn:miriam:reactome:R-ALL-113592"
      hgnc "NA"
      map_id "R2_283"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_3775"
      uniprot "NA"
    ]
    graphics [
      x 1532.5
      y 1019.738142841986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_283"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-BTA-195263-4;urn:miriam:reactome:R-BTA-195263-5;urn:miriam:reactome:R-BTA-195263-2;urn:miriam:reactome:R-BTA-195263-3;urn:miriam:reactome:R-CEL-195263;urn:miriam:reactome:R-MMU-195263;urn:miriam:reactome:R-DME-195263;urn:miriam:reactome:R-XTR-195263;urn:miriam:reactome:R-DME-195263-2;urn:miriam:reactome:R-DME-195263-5;urn:miriam:reactome:R-CEL-195263-5;urn:miriam:reactome:R-DME-195263-3;urn:miriam:reactome:R-DME-195263-4;urn:miriam:reactome:R-HSA-195263;urn:miriam:reactome:R-CEL-195263-2;urn:miriam:reactome:R-CEL-195263-4;urn:miriam:reactome:R-CEL-195263-3;urn:miriam:reactome:R-BTA-195263;urn:miriam:reactome:R-CFA-195263;urn:miriam:reactome:R-SSC-195263;urn:miriam:uniprot:P48729;urn:miriam:reactome:R-RNO-195263;urn:miriam:reactome:R-GGA-195263;urn:miriam:reactome:R-DRE-195263"
      hgnc "NA"
      map_id "R2_290"
      name "CSNK1A1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_3789"
      uniprot "UNIPROT:P48729"
    ]
    graphics [
      x 3092.5
      y 2140.1860833815003
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_290"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A456216;urn:miriam:reactome:R-ALL-29370"
      hgnc "NA"
      map_id "R2_284"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_3776"
      uniprot "NA"
    ]
    graphics [
      x 2402.5
      y 1096.0046064486714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_284"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-70106"
      hgnc "NA"
      map_id "R2_265"
      name "H_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_3554"
      uniprot "NA"
    ]
    graphics [
      x 2822.5
      y 1220.8713978203377
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_265"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9729260;PUBMED:32817937;PUBMED:32645325;PUBMED:32877642;PUBMED:32723359;PUBMED:32637943"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_408"
      name "GSK3 phosphorylates nucleoprotein"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9729260__layout_3598"
      uniprot "NA"
    ]
    graphics [
      x 962.5
      y 2130.6216893968285
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_408"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC9;urn:miriam:reactome:R-COV-9729324"
      hgnc "NA"
      map_id "R2_266"
      name "p_minus_S188,206_minus_N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_3558"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 422.5
      y 1622.2366478906015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_266"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30616;urn:miriam:reactome:R-ALL-113592"
      hgnc "NA"
      map_id "R2_286"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_3779"
      uniprot "NA"
    ]
    graphics [
      x 1412.5
      y 1252.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_286"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P49840;urn:miriam:reactome:R-HSA-198358;urn:miriam:uniprot:P49841"
      hgnc "NA"
      map_id "R2_84"
      name "GSK3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2314"
      uniprot "UNIPROT:P49840;UNIPROT:P49841"
    ]
    graphics [
      x 2792.5
      y 1702.3725415703839
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-9687663;urn:miriam:uniprot:P49841"
      hgnc "NA"
      map_id "R2_270"
      name "GSK3B:GSKi"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3593"
      uniprot "UNIPROT:P49841"
    ]
    graphics [
      x 1661.043215136001
      y 2615.6068976483975
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_270"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A456216;urn:miriam:reactome:R-ALL-29370"
      hgnc "NA"
      map_id "R2_287"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_3780"
      uniprot "NA"
    ]
    graphics [
      x 722.5
      y 1539.7660496881804
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_287"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-70106"
      hgnc "NA"
      map_id "R2_271"
      name "H_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_3602"
      uniprot "NA"
    ]
    graphics [
      x 1142.5
      y 1524.9910422829782
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_271"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9687724;PUBMED:34593624;PUBMED:19666099;PUBMED:19389332;PUBMED:11162580;PUBMED:24931005;PUBMED:18977324;PUBMED:18938143;PUBMED:19106108;PUBMED:18806775"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_301"
      name "GSK3B binds GSKi"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9687724__layout_3596"
      uniprot "NA"
    ]
    graphics [
      x 1112.5
      y 2097.0116707084726
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_301"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-SPO-2997551-3;urn:miriam:reactome:R-XTR-2997551;urn:miriam:reactome:R-SPO-2997551-2;urn:miriam:reactome:R-CEL-2997551;urn:miriam:reactome:R-SSC-2997551;urn:miriam:uniprot:P49841;urn:miriam:reactome:R-BTA-2997551;urn:miriam:reactome:R-SCE-2997551-2;urn:miriam:reactome:R-SCE-2997551-3;urn:miriam:reactome:R-SCE-2997551-4;urn:miriam:reactome:R-DRE-2997551;urn:miriam:reactome:R-SPO-2997551;urn:miriam:reactome:R-HSA-2997551;urn:miriam:reactome:R-DME-2997551;urn:miriam:reactome:R-RNO-198619;urn:miriam:reactome:R-SCE-2997551;urn:miriam:reactome:R-DME-2997551-2;urn:miriam:reactome:R-CEL-2997551-3;urn:miriam:reactome:R-DME-2997551-3;urn:miriam:reactome:R-CEL-2997551-4;urn:miriam:reactome:R-GGA-2997551;urn:miriam:reactome:R-CEL-2997551-2;urn:miriam:reactome:R-CFA-2997551;urn:miriam:reactome:R-MMU-2997551;urn:miriam:reactome:R-DDI-2997551"
      hgnc "NA"
      map_id "R2_101"
      name "GSK3B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2349"
      uniprot "UNIPROT:P49841"
    ]
    graphics [
      x 1908.7805189192002
      y 2462.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9687688;urn:miriam:pubmed:19106108"
      hgnc "NA"
      map_id "R2_224"
      name "GSKi"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_2949"
      uniprot "NA"
    ]
    graphics [
      x 602.5
      y 1179.295395563012
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_224"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9729330;PUBMED:12134018;PUBMED:32817937;PUBMED:32637943;PUBMED:33248025"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_414"
      name "SRPK1/2 phosphorylates nucleoprotein"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9729330__layout_3564"
      uniprot "NA"
    ]
    graphics [
      x 2042.5
      y 1408.5318911777056
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_414"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30616;urn:miriam:reactome:R-ALL-113592"
      hgnc "NA"
      map_id "R2_285"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_3777"
      uniprot "NA"
    ]
    graphics [
      x 1622.5
      y 862.5365071385852
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_285"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9694300;urn:miriam:uniprot:P0DTC9;urn:miriam:reactome:R-COV-9683625"
      hgnc "NA"
      map_id "R2_98"
      name "N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2341"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 962.5
      y 2000.4574439370535
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:pubmed:12134018;urn:miriam:uniprot:P78362;urn:miriam:reactome:R-HSA-9729312;urn:miriam:pubmed:12565829;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:uniprot:Q96SB4"
      hgnc "NA"
      map_id "R2_292"
      name "SRPK1_slash_2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3791"
      uniprot "UNIPROT:P78362;UNIPROT:Q96SB4"
    ]
    graphics [
      x 1802.5
      y 692.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_292"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A456216;urn:miriam:reactome:R-ALL-29370"
      hgnc "NA"
      map_id "R2_291"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_3790"
      uniprot "NA"
    ]
    graphics [
      x 2898.7805189192004
      y 2237.7034190231884
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_291"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-70106"
      hgnc "NA"
      map_id "R2_267"
      name "H_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_3569"
      uniprot "NA"
    ]
    graphics [
      x 3272.5
      y 1256.1844956582163
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_267"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 167
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694370;PUBMED:32015508"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_326"
      name "mRNA9a is translated to Nucleoprotein"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694370__layout_2339"
      uniprot "NA"
    ]
    graphics [
      x 1729.0132710924058
      y 3122.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_326"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 168
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694373;PUBMED:16103198"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_327"
      name "Unphosphorylated nucleoprotein translocates to the plasma membrane"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694373__layout_2342"
      uniprot "NA"
    ]
    graphics [
      x 1772.5
      y 1292.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_327"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 169
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9694356;urn:miriam:uniprot:P0DTC9;urn:miriam:reactome:R-COV-9683611;urn:miriam:pubmed:12775768"
      hgnc "NA"
      map_id "R2_99"
      name "N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2343"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 1867.432708728739
      y 362.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 170
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9694513;urn:miriam:reactome:R-COV-9685921"
      hgnc "NA"
      map_id "R2_97"
      name "m7G(5')pppAm_minus_capped,polyadenylated_minus_mRNA9"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_2340"
      uniprot "NA"
    ]
    graphics [
      x 2187.740229171312
      y 2813.713309864131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 171
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-COV-9682469;urn:miriam:reactome:R-COV-9694618"
      hgnc "NA"
      map_id "R2_55"
      name "SARS_minus_CoV_minus_2_space_gRNA:RTC:nascent_space_RNA_space_minus_space_strand"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2266"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1442.5
      y 1415.1961954122032
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 172
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-6806881;urn:miriam:obo.chebi:CHEBI%3A61557"
      hgnc "NA"
      map_id "R2_62"
      name "NTP(4_minus_)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2276"
      uniprot "NA"
    ]
    graphics [
      x 1412.5
      y 802.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 173
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:reactome:R-COV-9694327;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-COV-9687385"
      hgnc "NA"
      map_id "R2_64"
      name "SARS_minus_CoV_minus_2_space_gRNA:RTC:nascent_space_RNA_space_minus_space_strand:RTC_space_inhibitors"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2279"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 2042.5
      y 1919.8792239533784
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 174
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9713316;urn:miriam:reactome:R-COV-9713636;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420"
      hgnc "NA"
      map_id "R2_251"
      name "SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_complement_space_(minus_space_strand):RTC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3361"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 3152.5
      y 1734.3536941863422
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_251"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 175
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A33019;urn:miriam:reactome:R-ALL-111294"
      hgnc "NA"
      map_id "R2_56"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2267"
      uniprot "NA"
    ]
    graphics [
      x 1112.5
      y 1677.0116707084728
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 176
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694605;PUBMED:25197083;PUBMED:32358203;PUBMED:22791111"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_375"
      name "nsp12 synthesizes minus strand SARS-CoV-2 genomic RNA complement"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694605__layout_2264"
      uniprot "NA"
    ]
    graphics [
      x 2732.5
      y 2132.4310147534843
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_375"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 177
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-6806881;urn:miriam:obo.chebi:CHEBI%3A61557"
      hgnc "NA"
      map_id "R2_54"
      name "NTP(4_minus_)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2265"
      uniprot "NA"
    ]
    graphics [
      x 2291.043215136001
      y 2679.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 178
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9681663;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-COV-9694725"
      hgnc "NA"
      map_id "R2_52"
      name "SARS_minus_CoV_minus_2_space_gRNA:RTC:RNA_space_primer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2262"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 2762.5
      y 878.1323001980676
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 179
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694255;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-COV-9680476"
      hgnc "NA"
      map_id "R2_57"
      name "SARS_minus_CoV_minus_2_space_gRNA:RTC:RNA_space_primer:RTC_space_inhibitors"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2268"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 3182.5
      y 1002.533005889245
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 180
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694541;PUBMED:31645453;PUBMED:32258351;PUBMED:32284326;PUBMED:29511076;PUBMED:33264556;PUBMED:32253226;PUBMED:31233808;PUBMED:32094225;PUBMED:28124907"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_359"
      name "SARS-CoV-2 gRNA:RTC:RNA primer binds RTC inhibitors"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694541__layout_2502"
      uniprot "NA"
    ]
    graphics [
      x 1738.7570867047787
      y 302.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_359"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 181
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694792;PUBMED:20463816"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_398"
      name "nsp12 misincorporates a nucleotide in nascent RNA minus strand"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694792__layout_2269"
      uniprot "NA"
    ]
    graphics [
      x 2568.7805189192004
      y 2334.576720665756
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_398"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 182
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694269;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-COV-9682566"
      hgnc "NA"
      map_id "R2_58"
      name "SARS_space_coronavirus_space_gRNA:RTC:nascent_space_RNA_space_minus_space_strand_space_with_space_mismatched_space_nucleotide"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2270"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1451.043215136001
      y 2692.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 183
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A33019;urn:miriam:reactome:R-ALL-111294"
      hgnc "NA"
      map_id "R2_59"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2271"
      uniprot "NA"
    ]
    graphics [
      x 3062.5
      y 1120.8789316657976
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 184
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694632;PUBMED:25197083;PUBMED:17927896;PUBMED:32938769;PUBMED:20463816;PUBMED:22635272;PUBMED:16549795;PUBMED:25074927"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_379"
      name "nsp14 acts as a 3'-to-5' exonuclease to remove misincorporated nucleotides from nascent RNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694632__layout_2272"
      uniprot "NA"
    ]
    graphics [
      x 1952.5
      y 1712.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_379"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 185
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-29356"
      hgnc "NA"
      map_id "R2_60"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2273"
      uniprot "NA"
    ]
    graphics [
      x 1142.5
      y 1914.9910422829782
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 186
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9689912;urn:miriam:obo.chebi:CHEBI%3A26558"
      hgnc "NA"
      map_id "R2_61"
      name "NMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2274"
      uniprot "NA"
    ]
    graphics [
      x 2792.5
      y 1942.3725415703839
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 187
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9687408"
      hgnc "NA"
      map_id "R2_180"
      name "RTC_space_inhibitors"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_2503"
      uniprot "NA"
    ]
    graphics [
      x 992.5
      y 1822.2331028429844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_180"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 188
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694665;PUBMED:31645453;PUBMED:32258351;PUBMED:32284326;PUBMED:29511076;PUBMED:32253226;PUBMED:31233808;PUBMED:32094225;PUBMED:28124907"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_385"
      name "SARS-CoV-2 gRNA:RTC:nascent RNA minus strand binds RTC inhibitors"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694665__layout_2504"
      uniprot "NA"
    ]
    graphics [
      x 1952.5
      y 1592.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_385"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 189
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694277;PUBMED:17024178;PUBMED:22039154"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_307"
      name "nsp8 generates RNA primers"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694277__layout_2260"
      uniprot "NA"
    ]
    graphics [
      x 2972.5
      y 1527.4318615488276
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_307"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 190
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-6806881;urn:miriam:obo.chebi:CHEBI%3A61557"
      hgnc "NA"
      map_id "R2_278"
      name "NTP(4_minus_)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_3761"
      uniprot "NA"
    ]
    graphics [
      x 1262.5
      y 1386.9780694277015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_278"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 191
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-COV-9694458;urn:miriam:reactome:R-COV-9681659"
      hgnc "NA"
      map_id "R2_49"
      name "SARS_minus_CoV_minus_2_space_gRNA:RTC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2256"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 2598.7805189192004
      y 2477.2860175199776
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 192
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A33019;urn:miriam:reactome:R-ALL-111294"
      hgnc "NA"
      map_id "R2_53"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2263"
      uniprot "NA"
    ]
    graphics [
      x 1668.7805189192002
      y 2307.1855687946227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 193
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694344;PUBMED:12917450;PUBMED:16928755;PUBMED:12927536;PUBMED:14569023;PUBMED:25736566;PUBMED:26919232"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_321"
      name "Synthesis of SARS-CoV-2 minus strand subgenomic mRNAs by template switching"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694344__layout_2422"
      uniprot "NA"
    ]
    graphics [
      x 2203.868711874887
      y 2753.713309864131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_321"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 194
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:pubmed:32654247;urn:miriam:reactome:R-COV-9694702;urn:miriam:uniprot:P0DTC9"
      hgnc "NA"
      map_id "R2_268"
      name "N_space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3571"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 2762.5
      y 1531.2535183076814
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_268"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 195
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9713317;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-COV-9713652"
      hgnc "NA"
      map_id "R2_256"
      name "SARS_minus_CoV_minus_2_space_minus_space_strand_space_subgenomic_space_mRNAs:RTC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3408"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1578.7805189192002
      y 2303.119912305107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_256"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 196
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694506;PUBMED:12917450;PUBMED:12927536;PUBMED:32330414;PUBMED:14569023;PUBMED:26919232"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_351"
      name "Synthesis of SARS-CoV-2 plus strand subgenomic mRNAs"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694506__layout_2427"
      uniprot "NA"
    ]
    graphics [
      x 1873.7434535708758
      y 2942.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_351"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 197
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-6806881;urn:miriam:obo.chebi:CHEBI%3A61557"
      hgnc "NA"
      map_id "R2_138"
      name "NTP(4_minus_)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2423"
      uniprot "NA"
    ]
    graphics [
      x 2462.5
      y 1681.435024234829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_138"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 198
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:reactome:R-COV-9713314;urn:miriam:reactome:R-COV-9713653;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420"
      hgnc "NA"
      map_id "R2_257"
      name "SARS_minus_CoV_minus_2_space_plus_space_strand_space_subgenomic_space_mRNAs:RTC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3410"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1562.5
      y 1905.4971941397312
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_257"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 199
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A33019;urn:miriam:reactome:R-ALL-111294"
      hgnc "NA"
      map_id "R2_139"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2425"
      uniprot "NA"
    ]
    graphics [
      x 1682.5
      y 1557.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_139"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 200
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694476;PUBMED:15220459;PUBMED:19208801;PUBMED:12456663;PUBMED:6165837"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_346"
      name "nsp14 acts as a cap N7 methyltransferase to modify SARS-CoV-2 mRNAs"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694476__layout_2429"
      uniprot "NA"
    ]
    graphics [
      x 3152.5
      y 1007.6810339779831
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_346"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 201
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-29356"
      hgnc "NA"
      map_id "R2_140"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2430"
      uniprot "NA"
    ]
    graphics [
      x 2582.5
      y 1228.7265736750567
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_140"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 202
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A37565;urn:miriam:reactome:R-ALL-29438"
      hgnc "NA"
      map_id "R2_142"
      name "GTP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2432"
      uniprot "NA"
    ]
    graphics [
      x 2852.5
      y 939.8252654416042
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_142"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 203
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9639461;urn:miriam:obo.chebi:CHEBI%3A15414"
      hgnc "NA"
      map_id "R2_141"
      name "S_minus_adenosyl_minus_L_minus_methionine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2431"
      uniprot "NA"
    ]
    graphics [
      x 2102.5
      y 845.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_141"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 204
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A43474;urn:miriam:reactome:R-ALL-29372"
      hgnc "NA"
      map_id "R2_144"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2434"
      uniprot "NA"
    ]
    graphics [
      x 3362.5
      y 1296.3039990154389
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_144"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 205
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-COV-9713302;urn:miriam:reactome:R-COV-9713651"
      hgnc "NA"
      map_id "R2_258"
      name "m7GpppA_minus_SARS_minus_CoV_minus_2_space_plus_space_strand_space_subgenomic_space_mRNAs:RTC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3417"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 3512.5
      y 1028.1381124657096
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_258"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 206
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16680;urn:miriam:reactome:R-ALL-9639443"
      hgnc "NA"
      map_id "R2_143"
      name "S_minus_adenosyl_minus_L_minus_homocysteine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2433"
      uniprot "NA"
    ]
    graphics [
      x 2702.5
      y 1786.8021149866954
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_143"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 207
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A33019;urn:miriam:reactome:R-ALL-111294"
      hgnc "NA"
      map_id "R2_279"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_3764"
      uniprot "NA"
    ]
    graphics [
      x 3452.5
      y 1905.1001068356327
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_279"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 208
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694499;PUBMED:18417574;PUBMED:21637813;PUBMED:32709886;PUBMED:24478444;PUBMED:20421945;PUBMED:34131072;PUBMED:12456663;PUBMED:6165837"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_350"
      name "nsp16 acts as a cap 2'-O-methyltransferase to modify SARS-CoV-2 mRNAs"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694499__layout_2435"
      uniprot "NA"
    ]
    graphics [
      x 2702.5
      y 1604.2824110750719
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_350"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 209
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9639461;urn:miriam:obo.chebi:CHEBI%3A15414"
      hgnc "NA"
      map_id "R2_280"
      name "S_minus_adenosyl_minus_L_minus_methionine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_3766"
      uniprot "NA"
    ]
    graphics [
      x 3512.5
      y 1590.1223224818948
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_280"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 210
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9686016;urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694302;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:obo.chebi:CHEBI%3A18420"
      hgnc "NA"
      map_id "R2_46"
      name "RTC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2251"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 2072.5
      y 1685.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 211
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9685891;urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9694259"
      hgnc "NA"
      map_id "R2_145"
      name "m7G(5')pppAm_minus_SARS_minus_CoV_minus_2_space_plus_space_strand_space_subgenomic_space_mRNAs"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2437"
      uniprot "NA"
    ]
    graphics [
      x 1421.043215136001
      y 2632.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_145"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 212
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694733;PUBMED:30918070;PUBMED:10799579;PUBMED:32330414;PUBMED:32511382;PUBMED:27760233"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_394"
      name "Polyadenylation of SARS-CoV-2 subgenomic mRNAs (plus strand)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694733__layout_2438"
      uniprot "NA"
    ]
    graphics [
      x 1541.043215136001
      y 2609.738142841986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_394"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 213
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30616;urn:miriam:reactome:R-ALL-113592"
      hgnc "NA"
      map_id "R2_146"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2439"
      uniprot "NA"
    ]
    graphics [
      x 1292.5
      y 1673.3146446645712
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_146"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 214
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9694561;urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9685910"
      hgnc "NA"
      map_id "R2_147"
      name "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_subgenomic_space_mRNAs_space_(plus_space_strand)"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2440"
      uniprot "NA"
    ]
    graphics [
      x 1899.379876527966
      y 1832.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_147"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 215
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A33019;urn:miriam:reactome:R-ALL-111294"
      hgnc "NA"
      map_id "R2_148"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2441"
      uniprot "NA"
    ]
    graphics [
      x 1562.5
      y 1875.4971941397312
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_148"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 216
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694521;PUBMED:18417574;PUBMED:21637813;PUBMED:32709886;PUBMED:24478444;PUBMED:20421945;PUBMED:34131072;PUBMED:12456663;PUBMED:6165837"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_353"
      name "nsp16 acts as a cap 2'-O-methyltransferase to modify SARS-CoV-2 gRNA complement (minus strand)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694521__layout_2288"
      uniprot "NA"
    ]
    graphics [
      x 1866.248669174655
      y 2042.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_353"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 217
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694576;PUBMED:18827877;PUBMED:18255185"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_369"
      name "nsp3 binds to nsp7-8 and nsp12-16"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694576__layout_2451"
      uniprot "NA"
    ]
    graphics [
      x 1802.5
      y 932.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_369"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 218
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694721;PUBMED:18417574;PUBMED:21637813;PUBMED:32709886;PUBMED:24478444;PUBMED:20421945;PUBMED:34131072;PUBMED:12456663;PUBMED:6165837"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_390"
      name "nsp16 acts as a cap 2'-O-methyltransferase to modify SARS-CoV-2 gRNA (plus strand)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694721__layout_2304"
      uniprot "NA"
    ]
    graphics [
      x 858.7805189192002
      y 2241.1015810722574
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_390"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 219
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694454;PUBMED:32358203;PUBMED:32526208;PUBMED:32438371;PUBMED:22791111"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_342"
      name "Replication transcription complex binds SARS-CoV-2 genomic RNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694454__layout_2250"
      uniprot "NA"
    ]
    graphics [
      x 2912.5
      y 1299.67376082007
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_342"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 220
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694495;PUBMED:22791111"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_349"
      name "RTC binds SARS-CoV-2 genomic RNA complement (minus strand)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694495__layout_2290"
      uniprot "NA"
    ]
    graphics [
      x 2042.5
      y 1468.5318911777056
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_349"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 221
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9685513;urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9694603"
      hgnc "NA"
      map_id "R2_71"
      name "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_complement_space_(minus_space_strand)"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_2289"
      uniprot "NA"
    ]
    graphics [
      x 1352.5
      y 849.3114613664379
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 222
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9682253;urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:reactome:R-COV-9694404;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420"
      hgnc "NA"
      map_id "R2_72"
      name "SARS_minus_CoV_minus_2_space_gRNA_space_complement_space_(minus_space_strand):RTC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2291"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 3392.5
      y 1180.0344030971005
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 223
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694419;PUBMED:31645453;PUBMED:32258351;PUBMED:32284326;PUBMED:29511076;PUBMED:32253226;PUBMED:31233808;PUBMED:32094225;PUBMED:28124907"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_334"
      name "SARS-CoV-2 gRNA complement (minus strand):RTC binds RTC inhibitors"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694419__layout_2505"
      uniprot "NA"
    ]
    graphics [
      x 2178.7805189192004
      y 2123.713309864131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_334"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 224
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694581;PUBMED:25197083;PUBMED:32358203;PUBMED:22791111"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_372"
      name "RTC synthesizes SARS-CoV-2 plus strand genomic RNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694581__layout_2292"
      uniprot "NA"
    ]
    graphics [
      x 2942.5
      y 1222.6505321512923
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_372"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 225
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-6806881;urn:miriam:obo.chebi:CHEBI%3A61557"
      hgnc "NA"
      map_id "R2_73"
      name "NTP(4_minus_)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2293"
      uniprot "NA"
    ]
    graphics [
      x 2852.5
      y 1026.1195464255575
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 226
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-COV-9694690;urn:miriam:reactome:R-COV-9687382"
      hgnc "NA"
      map_id "R2_75"
      name "SARS_minus_CoV_minus_2_space_gRNA_space_complement_space_(minus_space_strand):RTC:RTC_space_inhibitors"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2296"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1862.5
      y 632.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 227
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9713644;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-COV-9713311"
      hgnc "NA"
      map_id "R2_253"
      name "SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand):RTC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3381"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1368.7805189192002
      y 2206.912190820152
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_253"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 228
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A33019;urn:miriam:reactome:R-ALL-111294"
      hgnc "NA"
      map_id "R2_74"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2294"
      uniprot "NA"
    ]
    graphics [
      x 1472.5
      y 1096.4685701968617
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 229
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694737;PUBMED:15220459;PUBMED:19208801;PUBMED:12456663;PUBMED:6165837"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_395"
      name "nsp14 acts as a cap N7 methyltransferase to modify SARS-CoV-2 gRNA (plus strand)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694737__layout_2297"
      uniprot "NA"
    ]
    graphics [
      x 782.5
      y 995.939330668649
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_395"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 230
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-29356"
      hgnc "NA"
      map_id "R2_254"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_3385"
      uniprot "NA"
    ]
    graphics [
      x 2222.5
      y 1436.7158381002334
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_254"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 231
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A37565;urn:miriam:reactome:R-ALL-29438"
      hgnc "NA"
      map_id "R2_77"
      name "GTP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2300"
      uniprot "NA"
    ]
    graphics [
      x 602.5
      y 1119.295395563012
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 232
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9639461;urn:miriam:obo.chebi:CHEBI%3A15414"
      hgnc "NA"
      map_id "R2_76"
      name "S_minus_adenosyl_minus_L_minus_methionine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2299"
      uniprot "NA"
    ]
    graphics [
      x 738.7805189192002
      y 2325.1905259513114
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 233
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9713643;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-COV-9713313;urn:miriam:obo.chebi:CHEBI%3A33019;urn:miriam:reactome:R-ALL-111294"
      hgnc "NA"
      map_id "R2_255"
      name "m7GpppA_minus_capped_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand):RTC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3386"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 768.7805189192002
      y 2415.2255563407352
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_255"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 234
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A43474;urn:miriam:reactome:R-ALL-29372"
      hgnc "NA"
      map_id "R2_78"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2301"
      uniprot "NA"
    ]
    graphics [
      x 782.5
      y 2054.14911835969
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 235
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16680;urn:miriam:reactome:R-ALL-9639443"
      hgnc "NA"
      map_id "R2_79"
      name "S_minus_adenosyl_minus_L_minus_homocysteine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2302"
      uniprot "NA"
    ]
    graphics [
      x 1615.4220780557016
      y 473.11991230510694
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 236
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A33019;urn:miriam:reactome:R-ALL-111294"
      hgnc "NA"
      map_id "R2_80"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2303"
      uniprot "NA"
    ]
    graphics [
      x 452.5
      y 956.6834762305416
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 237
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9687408"
      hgnc "NA"
      map_id "R2_181"
      name "RTC_space_inhibitors"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_2506"
      uniprot "NA"
    ]
    graphics [
      x 1631.043215136001
      y 2633.119912305107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_181"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 238
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9685518;urn:miriam:reactome:R-COV-9694393"
      hgnc "NA"
      map_id "R2_276"
      name "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_3664"
      uniprot "NA"
    ]
    graphics [
      x 2672.5
      y 1983.7928516911866
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_276"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 239
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694731;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9682668;urn:miriam:obo.chebi:CHEBI%3A18420"
      hgnc "NA"
      map_id "R2_47"
      name "SARS_space_coronavirus_space_2_space_gRNA_space_with_space_secondary_space_structure:RTC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2253"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1952.5
      y 1172.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 240
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694265;PUBMED:32783916;PUBMED:32484220;PUBMED:19224332;PUBMED:16579970;PUBMED:12917423;PUBMED:15140959;PUBMED:32500504;PUBMED:20671029;PUBMED:22615777"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_305"
      name "nsp13 helicase melts secondary structures in SARS-CoV-2 genomic RNA template"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694265__layout_3127"
      uniprot "NA"
    ]
    graphics [
      x 3242.5
      y 907.3186590187348
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_305"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 241
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30616;urn:miriam:reactome:R-ALL-113592"
      hgnc "NA"
      map_id "R2_48"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2255"
      uniprot "NA"
    ]
    graphics [
      x 2762.5
      y 818.1323001980676
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 242
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A43474;urn:miriam:reactome:R-ALL-29372"
      hgnc "NA"
      map_id "R2_51"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2258"
      uniprot "NA"
    ]
    graphics [
      x 3302.5
      y 1242.6294574380252
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 243
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A456216;urn:miriam:reactome:R-ALL-29370"
      hgnc "NA"
      map_id "R2_50"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2257"
      uniprot "NA"
    ]
    graphics [
      x 1870.9135463862924
      y 782.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 244
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16680;urn:miriam:reactome:R-ALL-9639443"
      hgnc "NA"
      map_id "R2_68"
      name "S_minus_adenosyl_minus_L_minus_homocysteine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2284"
      uniprot "NA"
    ]
    graphics [
      x 512.5
      y 1631.6882910201155
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 245
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694492;PUBMED:15220459;PUBMED:19208801;PUBMED:12456663;PUBMED:6165837"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_348"
      name "nsp14 acts as a cap N7 methyltransferase to modify SARS-CoV-2 gRNA complement (minus strand)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694492__layout_2280"
      uniprot "NA"
    ]
    graphics [
      x 1592.5
      y 1073.119912305107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_348"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 246
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-29356"
      hgnc "NA"
      map_id "R2_67"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2283"
      uniprot "NA"
    ]
    graphics [
      x 842.5
      y 700.9796434332732
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 247
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A37565;urn:miriam:reactome:R-ALL-29438"
      hgnc "NA"
      map_id "R2_65"
      name "GTP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2281"
      uniprot "NA"
    ]
    graphics [
      x 1292.5
      y 2003.3146446645712
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 248
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9639461;urn:miriam:obo.chebi:CHEBI%3A15414"
      hgnc "NA"
      map_id "R2_66"
      name "S_minus_adenosyl_minus_L_minus_methionine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2282"
      uniprot "NA"
    ]
    graphics [
      x 2358.7805189192004
      y 2302.060834871624
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 249
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A43474;urn:miriam:reactome:R-ALL-29372"
      hgnc "NA"
      map_id "R2_70"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2286"
      uniprot "NA"
    ]
    graphics [
      x 1292.5
      y 1013.3146446645712
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 250
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9713306;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:reactome:R-COV-9713634;urn:miriam:refseq:MN908947.3;urn:miriam:obo.chebi:CHEBI%3A18420"
      hgnc "NA"
      map_id "R2_252"
      name "m7GpppA_minus_capped_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_complement_space_(minus_space_strand):RTC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3366"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 662.5
      y 2085.102589067452
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_252"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 251
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A33019;urn:miriam:reactome:R-ALL-111294"
      hgnc "NA"
      map_id "R2_69"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2285"
      uniprot "NA"
    ]
    graphics [
      x 2192.5
      y 773.7133098641311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 252
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694740;urn:miriam:reactome:R-COV-9683403"
      hgnc "NA"
      map_id "R2_44"
      name "nsp7:nsp8:nsp12:nsp14:nsp10:nsp13:nsp15"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2247"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 842.5
      y 1088.434726735171
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 253
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9686008;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694728"
      hgnc "NA"
      map_id "R2_154"
      name "nsp3:nsp4:nsp6"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2450"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 692.5
      y 1979.1308586148798
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_154"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 254
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:pubmed:32511376;urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:pubmed:32709886;urn:miriam:reactome:R-COV-9694416;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:pubmed:32838362;urn:miriam:pubmed:34131072"
      hgnc "NA"
      map_id "R2_45"
      name "nsp16:nsp10"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2249"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1772.5
      y 1112.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 255
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694445;PUBMED:20699222;PUBMED:22022266;PUBMED:32511376;PUBMED:32709886;PUBMED:32838362;PUBMED:16873246;PUBMED:20421945;PUBMED:16873247;PUBMED:34131072;PUBMED:25074927;PUBMED:26041293;PUBMED:21637813;PUBMED:21393853;PUBMED:22635272"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_339"
      name "nsp16 binds nsp10"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694445__layout_2248"
      uniprot "NA"
    ]
    graphics [
      x 1202.5
      y 1437.3156502059937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_339"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 256
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694358;urn:miriam:reactome:R-COV-9682215"
      hgnc "NA"
      map_id "R2_39"
      name "nsp10"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2238"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 482.5
      y 1582.1825831110364
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 257
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29035;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:reactome:R-ALL-1500648"
      hgnc "NA"
      map_id "R2_294"
      name "Mg2_plus__slash_Mn2_plus_"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3797"
      uniprot "NA"
    ]
    graphics [
      x 1691.043215136001
      y 2607.1855687946227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_294"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 258
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9678289;urn:miriam:reactome:R-COV-9694647"
      hgnc "NA"
      map_id "R2_14"
      name "pp1ab_minus_nsp16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2198"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 2342.5
      y 2072.963502381953
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 259
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694732;PUBMED:14561748"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_393"
      name "3CLp cleaves pp1ab"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694732__layout_2188"
      uniprot "NA"
    ]
    graphics [
      x 692.5
      y 1147.9447581609927
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_393"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 260
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694520;PUBMED:25732088"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_352"
      name "nsp16 binds VHL"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694520__layout_2324"
      uniprot "NA"
    ]
    graphics [
      x 3092.5
      y 1750.1860833815003
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_352"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 261
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-CEL-391423;urn:miriam:reactome:R-DDI-391423;urn:miriam:reactome:R-MMU-391423;urn:miriam:reactome:R-DME-391423;urn:miriam:reactome:R-XTR-391423;urn:miriam:reactome:R-SSC-391423;urn:miriam:reactome:R-RNO-391423;urn:miriam:reactome:R-GGA-391423;urn:miriam:reactome:R-DRE-391423;urn:miriam:uniprot:P40337;urn:miriam:reactome:R-BTA-391423;urn:miriam:reactome:R-HSA-391423;urn:miriam:reactome:R-CFA-391423"
      hgnc "NA"
      map_id "R2_88"
      name "VHL"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2325"
      uniprot "UNIPROT:P40337"
    ]
    graphics [
      x 1548.7805189192002
      y 2159.738142841986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 262
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-9694398;urn:miriam:uniprot:P0DTD1;urn:miriam:uniprot:P40337;urn:miriam:reactome:R-HSA-9683453;urn:miriam:pubmed:25732088"
      hgnc "NA"
      map_id "R2_237"
      name "nsp16:VHL"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3093"
      uniprot "UNIPROT:P0DTD1;UNIPROT:P40337"
    ]
    graphics [
      x 2432.5
      y 806.0089009888093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_237"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 263
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682068"
      hgnc "NA"
      map_id "R2_201"
      name "pp1ab"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2763"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1412.5
      y 1582.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_201"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 264
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-29356"
      hgnc "NA"
      map_id "R2_202"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2764"
      uniprot "NA"
    ]
    graphics [
      x 212.5
      y 1354.7381506303675
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_202"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 265
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9684343;urn:miriam:reactome:R-COV-9694407"
      hgnc "NA"
      map_id "R2_5"
      name "3CLp_space_dimer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2176"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 482.5
      y 1089.5426747442716
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 266
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9694375;urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:pubmed:32541865;urn:miriam:obo.chebi:CHEBI%3A2981;urn:miriam:pubmed:32272481"
      hgnc "NA"
      map_id "R2_6"
      name "3CLp_space_dimer:3CLp_space_inhibitors"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2179"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1202.5
      y 1827.3156502059937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 267
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9678285;urn:miriam:reactome:R-COV-9694537"
      hgnc "NA"
      map_id "R2_10"
      name "pp1ab_minus_nsp14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2192"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 692.5
      y 1612.1232976122167
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 268
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682052"
      hgnc "NA"
      map_id "R2_204"
      name "pp1ab_minus_nsp8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2893"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 2162.5
      y 2003.7133098641311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_204"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 269
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694642;urn:miriam:reactome:R-COV-9678281"
      hgnc "NA"
      map_id "R2_12"
      name "pp1ab_minus_nsp13"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2195"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1652.5
      y 1827.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 270
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694697;urn:miriam:reactome:R-COV-9682232"
      hgnc "NA"
      map_id "R2_13"
      name "pp1ab_minus_nsp9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2196"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1892.5
      y 512.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 271
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682057"
      hgnc "NA"
      map_id "R2_196"
      name "pp1ab_minus_nsp12"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2575"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1949.2795783000176
      y 272.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_196"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 272
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682233;urn:miriam:reactome:R-COV-9694425"
      hgnc "NA"
      map_id "R2_15"
      name "pp1ab_minus_nsp6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2199"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 602.5
      y 1869.295395563012
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 273
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694335;urn:miriam:reactome:R-COV-9678288"
      hgnc "NA"
      map_id "R2_11"
      name "pp1ab_minus_nsp15"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2193"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 782.5
      y 1237.835840060344
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 274
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682061"
      hgnc "NA"
      map_id "R2_203"
      name "pp1ab_minus_nsp7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2848"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 662.5
      y 946.1962192099609
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_203"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 275
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9694372;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682216"
      hgnc "NA"
      map_id "R2_16"
      name "pp1ab_minus_nsp10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2201"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1921.8191902032843
      y 1322.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 276
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9684861;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694695"
      hgnc "NA"
      map_id "R2_8"
      name "pp1ab_minus_nsp1_minus_4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2190"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 422.5
      y 1545.4403911061856
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 277
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9682196;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694504"
      hgnc "NA"
      map_id "R2_9"
      name "pp1ab_minus_nsp5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2191"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 692.5
      y 1383.5606755964814
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 278
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694436;PUBMED:32304108;PUBMED:32803198"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_335"
      name "nsp15 forms a hexamer"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694436__layout_2244"
      uniprot "NA"
    ]
    graphics [
      x 978.7805189192002
      y 2400.6216893968285
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_335"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 279
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9694570;urn:miriam:pubmed:18045871;urn:miriam:pubmed:16882730;urn:miriam:pubmed:16828802;urn:miriam:uniprot:P0DTD1;urn:miriam:pubmed:16216269;urn:miriam:reactome:R-COV-9682715;urn:miriam:pubmed:22301153;urn:miriam:pubmed:17409150"
      hgnc "NA"
      map_id "R2_43"
      name "nsp15_space_hexamer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2245"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 962.5
      y 1925.8115459828596
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 280
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694539;PUBMED:28143984;PUBMED:16828802;PUBMED:16882730;PUBMED:18045871;PUBMED:16216269;PUBMED:17409150;PUBMED:18255185;PUBMED:22301153"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_358"
      name "nsp15 binds nsp8"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694539__layout_2246"
      uniprot "NA"
    ]
    graphics [
      x 662.5
      y 1390.2200465763535
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_358"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 281
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694390;PUBMED:22301153"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_330"
      name "nsp15 binds RB1"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694390__layout_2321"
      uniprot "NA"
    ]
    graphics [
      x 2462.5
      y 691.4350242348289
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_330"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 282
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-SSC-9660621;urn:miriam:reactome:R-DME-9660621-2;urn:miriam:reactome:R-CFA-9660621;urn:miriam:reactome:R-DRE-9660621;urn:miriam:reactome:R-MMU-9660621;urn:miriam:reactome:R-DDI-9660621;urn:miriam:reactome:R-HSA-9660621;urn:miriam:reactome:R-XTR-9660621;urn:miriam:reactome:R-CEL-9660621;urn:miriam:uniprot:P06400;urn:miriam:reactome:R-RNO-9660621;urn:miriam:reactome:R-DME-9660621;urn:miriam:reactome:R-DME-9660621-3"
      hgnc "NA"
      map_id "R2_87"
      name "RB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2322"
      uniprot "UNIPROT:P06400"
    ]
    graphics [
      x 3152.5
      y 1704.3536941863422
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 283
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-9694258;urn:miriam:uniprot:P0DTD1;urn:miriam:uniprot:P06400;urn:miriam:reactome:R-HSA-9682726;urn:miriam:pubmed:22301153"
      hgnc "NA"
      map_id "R2_238"
      name "nsp15:RB1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3094"
      uniprot "UNIPROT:P0DTD1;UNIPROT:P06400"
    ]
    graphics [
      x 2424.660732432475
      y 323.1730363852655
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_238"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 284
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:pubmed:33208736;urn:miriam:reactome:R-COV-9694717"
      hgnc "NA"
      map_id "R2_42"
      name "nsp7:nsp8:nsp12:nsp14:nsp10:nsp13"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2243"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1292.5
      y 1613.3146446645712
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 285
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694524;PUBMED:32783916;PUBMED:31131400;PUBMED:33208736;PUBMED:33232691;PUBMED:22615777;PUBMED:17520018"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_354"
      name "nsp13 binds nsp12"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694524__layout_2242"
      uniprot "NA"
    ]
    graphics [
      x 1878.7805189192002
      y 2192.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_354"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 286
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9682451;urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694597"
      hgnc "NA"
      map_id "R2_41"
      name "nsp7:nsp8:nsp12:nsp14:nsp10"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2241"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 2432.5
      y 1316.0089009888093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 287
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694304;PUBMED:25197083;PUBMED:16549795"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_312"
      name "nsp14 binds nsp12"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694304__layout_2240"
      uniprot "NA"
    ]
    graphics [
      x 2342.5
      y 1382.963502381953
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_312"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 288
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694688;urn:miriam:reactome:R-COV-9682545"
      hgnc "NA"
      map_id "R2_40"
      name "nsp10:nsp14"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2239"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 3182.5
      y 1929.1187697791565
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 289
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9691364;urn:miriam:pubmed:32277040"
      hgnc "NA"
      map_id "R2_197"
      name "nsp7:nsp8:nsp12"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2576"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 2287.4327087287393
      y 489.06618499081924
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_197"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 290
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9691363;PUBMED:32358203;PUBMED:32438371;PUBMED:31138817;PUBMED:32838362;PUBMED:32531208;PUBMED:32277040"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_303"
      name "nsp12 binds nsp7 and nsp8"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9691363__layout_2574"
      uniprot "NA"
    ]
    graphics [
      x 2621.3843143779663
      y 425.83149007527027
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_303"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 291
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9691351"
      hgnc "NA"
      map_id "R2_193"
      name "nsp8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2559"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 3572.5
      y 1565.0114241332692
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_193"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 292
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9691348;urn:miriam:pubmed:32277040"
      hgnc "NA"
      map_id "R2_195"
      name "nsp7:nsp8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2565"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 3242.5
      y 1195.9916113401819
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_195"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 293
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9691335;PUBMED:32535228;PUBMED:32438371;PUBMED:32838362;PUBMED:32531208;PUBMED:32277040"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_302"
      name "nsp7 binds nsp8"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9691335__layout_2558"
      uniprot "NA"
    ]
    graphics [
      x 3482.5
      y 2103.2948504496417
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_302"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 294
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9691334"
      hgnc "NA"
      map_id "R2_194"
      name "nsp7"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2562"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 3422.5
      y 1633.8107765906786
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_194"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 295
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694542;PUBMED:25197083;PUBMED:32838362;PUBMED:22635272;PUBMED:25074927"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_360"
      name "nsp14 binds nsp10"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694542__layout_2237"
      uniprot "NA"
    ]
    graphics [
      x 1832.5
      y 1622.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_360"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 296
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694406;PUBMED:19224332"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_333"
      name "nsp13 binds DDX5"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694406__layout_2318"
      uniprot "NA"
    ]
    graphics [
      x 2257.4327087287393
      y 459.06618499081924
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_333"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 297
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P17844;urn:miriam:reactome:R-HSA-9682643"
      hgnc "NA"
      map_id "R2_86"
      name "DDX5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2319"
      uniprot "UNIPROT:P17844"
    ]
    graphics [
      x 3032.5
      y 1168.6015259804678
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 298
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:pubmed:19224332;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-HSA-9682634;urn:miriam:uniprot:P17844;urn:miriam:reactome:R-HSA-9694692"
      hgnc "NA"
      map_id "R2_236"
      name "nsp13:DDX5"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3082"
      uniprot "UNIPROT:P0DTD1;UNIPROT:P17844"
    ]
    graphics [
      x 1782.6086432296322
      y 182.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_236"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 299
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694592;PUBMED:32045235;PUBMED:33452205;PUBMED:32541865;PUBMED:33984267;PUBMED:33062953;PUBMED:32272481;PUBMED:32374457;PUBMED:32896566;PUBMED:33152262;PUBMED:32737471;PUBMED:34726479;PUBMED:15507456;PUBMED:33574416"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_373"
      name "3CLp dimer binds 3CLp inhibitors"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694592__layout_2452"
      uniprot "NA"
    ]
    graphics [
      x 1832.5
      y 542.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_373"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 300
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694551;PUBMED:14561748"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_362"
      name "3CLp cleaves nsp6-11"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694551__layout_2180"
      uniprot "NA"
    ]
    graphics [
      x 1301.043215136001
      y 2603.3146446645715
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_362"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 301
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694441;PUBMED:14561748"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_337"
      name "3CLp cleaves pp1a"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694441__layout_2177"
      uniprot "NA"
    ]
    graphics [
      x 752.5
      y 1442.6540775161152
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_337"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 302
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9681986"
      hgnc "NA"
      map_id "R2_198"
      name "pp1a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2719"
      uniprot "UNIPROT:P0DTC1"
    ]
    graphics [
      x 632.5
      y 1147.077935484341
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_198"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 303
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-29356"
      hgnc "NA"
      map_id "R2_199"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2725"
      uniprot "NA"
    ]
    graphics [
      x 1742.5
      y 1257.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_199"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 304
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9681994"
      hgnc "NA"
      map_id "R2_192"
      name "pp1a_minus_3CL"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2547"
      uniprot "UNIPROT:P0DTC1"
    ]
    graphics [
      x 1825.5437299022901
      y 2882.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_192"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 305
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9684311;urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9694778"
      hgnc "NA"
      map_id "R2_22"
      name "pp1a_minus_nsp1_minus_4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2208"
      uniprot "UNIPROT:P0DTC1"
    ]
    graphics [
      x 1472.5
      y 1636.4685701968617
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 306
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9684354;urn:miriam:reactome:R-COV-9694668"
      hgnc "NA"
      map_id "R2_3"
      name "pp1a_minus_nsp6_minus_11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2172"
      uniprot "UNIPROT:P0DTC1"
    ]
    graphics [
      x 1292.5
      y 1163.3146446645712
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 307
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694377;PUBMED:15788388;PUBMED:21203998;PUBMED:27799534"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_328"
      name "pp1a cleaves itself"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694377__layout_2169"
      uniprot "NA"
    ]
    graphics [
      x 1571.043215136001
      y 2699.738142841986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_328"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 308
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9698265;PUBMED:31231549;PUBMED:15331731;PUBMED:21799305;PUBMED:25135833"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_405"
      name "Enhanced autophagosome formation"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9698265__layout_3102"
      uniprot "NA"
    ]
    graphics [
      x 1661.043215136001
      y 2519.0434319294973
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_405"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 309
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-GGA-5205651;urn:miriam:uniprot:Q9GZQ8;urn:miriam:reactome:R-DDI-5205651-2;urn:miriam:reactome:R-DDI-5205651-3;urn:miriam:reactome:R-RNO-5205651;urn:miriam:reactome:R-CEL-5205651;urn:miriam:reactome:R-PFA-5205651;urn:miriam:reactome:R-BTA-5205651;urn:miriam:reactome:R-HSA-5205651;urn:miriam:reactome:R-DDI-5205651;urn:miriam:reactome:R-SCE-5205651;urn:miriam:reactome:R-SSC-5205651;urn:miriam:reactome:R-MMU-5205651;urn:miriam:reactome:R-DRE-5205651;urn:miriam:reactome:R-CFA-5205651;urn:miriam:reactome:R-SPO-5205651"
      hgnc "NA"
      map_id "R2_183"
      name "MAP1LC3B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2510"
      uniprot "UNIPROT:Q9GZQ8"
    ]
    graphics [
      x 1091.043215136001
      y 2545.990081674636
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_183"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 310
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:Q99570;urn:miriam:reactome:R-CEL-5683632;urn:miriam:reactome:R-HSA-5683632;urn:miriam:reactome:R-XTR-5683632;urn:miriam:reactome:R-DME-5683632;urn:miriam:reactome:R-RNO-5683632;urn:miriam:reactome:R-SPO-5683632;urn:miriam:reactome:R-MMU-5683632;urn:miriam:reactome:R-DDI-5683632;urn:miriam:reactome:R-CFA-5683632;urn:miriam:reactome:R-GGA-5683632;urn:miriam:reactome:R-DRE-5683632;urn:miriam:reactome:R-SCE-5683632;urn:miriam:uniprot:Q14457;urn:miriam:reactome:R-SSC-5683632;urn:miriam:uniprot:Q9P2Y5;urn:miriam:uniprot:Q8NEB9"
      hgnc "NA"
      map_id "R2_186"
      name "UVRAG_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2515"
      uniprot "UNIPROT:Q99570;UNIPROT:Q14457;UNIPROT:Q9P2Y5;UNIPROT:Q8NEB9"
    ]
    graphics [
      x 1488.7805189192002
      y 2252.5133596239853
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_186"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 311
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-XTR-917723;urn:miriam:uniprot:Q9UQN3;urn:miriam:reactome:R-DDI-917723;urn:miriam:reactome:R-DME-917723;urn:miriam:uniprot:Q8WUX9;urn:miriam:reactome:R-SSC-917723;urn:miriam:reactome:R-GGA-917723;urn:miriam:uniprot:Q96CF2;urn:miriam:reactome:R-RNO-917723;urn:miriam:uniprot:Q9Y3E7;urn:miriam:uniprot:Q9BY43;urn:miriam:reactome:R-DRE-917723;urn:miriam:reactome:R-HSA-917723;urn:miriam:reactome:R-CFA-917723;urn:miriam:reactome:R-BTA-917723;urn:miriam:uniprot:Q9H444;urn:miriam:uniprot:Q96FZ7;urn:miriam:reactome:R-SPO-917723;urn:miriam:reactome:R-MMU-917723;urn:miriam:uniprot:O43633;urn:miriam:reactome:R-SCE-917723;urn:miriam:reactome:R-CEL-917723"
      hgnc "NA"
      map_id "R2_185"
      name "ESCRT_minus_III"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2514"
      uniprot "UNIPROT:Q9UQN3;UNIPROT:Q8WUX9;UNIPROT:Q96CF2;UNIPROT:Q9Y3E7;UNIPROT:Q9BY43;UNIPROT:Q9H444;UNIPROT:Q96FZ7;UNIPROT:O43633"
    ]
    graphics [
      x 2441.043215136001
      y 2636.0089009888093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_185"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 312
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9727285;urn:miriam:uniprot:P0DTD2"
      hgnc "NA"
      map_id "R2_259"
      name "9b_space_homodimer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3481"
      uniprot "UNIPROT:P0DTD2"
    ]
    graphics [
      x 2083.403181474062
      y 2822.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_259"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 313
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-CEL-5683641;urn:miriam:reactome:R-DDI-5683641-3;urn:miriam:reactome:R-DDI-5683641-2;urn:miriam:reactome:R-RNO-5683641;urn:miriam:uniprot:Q9GZQ8;urn:miriam:reactome:R-HSA-5683641;urn:miriam:reactome:R-SPO-5683641;urn:miriam:reactome:R-CFA-5683641;urn:miriam:reactome:R-GGA-5683641;urn:miriam:reactome:R-MMU-5683641;urn:miriam:reactome:R-DRE-5683641;urn:miriam:reactome:R-DDI-5683641;urn:miriam:reactome:R-PFA-5683641;urn:miriam:reactome:R-SCE-5683641;urn:miriam:reactome:R-SSC-5683641;urn:miriam:reactome:R-BTA-5683641"
      hgnc "NA"
      map_id "R2_184"
      name "MAP1LC3B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2513"
      uniprot "UNIPROT:Q9GZQ8"
    ]
    graphics [
      x 768.7805189192002
      y 2470.6512952763524
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_184"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 314
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694580;PUBMED:15331731"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_371"
      name "nsp8 binds MAP1LC3B"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694580__layout_2509"
      uniprot "NA"
    ]
    graphics [
      x 1502.5
      y 2047.3397236403869
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_371"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 315
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9691351"
      hgnc "NA"
      map_id "R2_241"
      name "nsp8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3098"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 512.5
      y 1730.340593846861
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_241"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 316
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-9694566;urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:uniprot:Q9GZQ8;urn:miriam:reactome:R-HSA-9687117"
      hgnc "NA"
      map_id "R2_240"
      name "nsp8:MAP1LC3B"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3096"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1;UNIPROT:Q9GZQ8"
    ]
    graphics [
      x 2342.5
      y 1922.963502381953
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_240"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 317
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-29356"
      hgnc "NA"
      map_id "R2_200"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2727"
      uniprot "NA"
    ]
    graphics [
      x 2261.043215136001
      y 2612.458314370092
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_200"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 318
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9694743;urn:miriam:reactome:R-COV-9684326"
      hgnc "NA"
      map_id "R2_2"
      name "pp1a_space_dimer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2168"
      uniprot "UNIPROT:P0DTC1"
    ]
    graphics [
      x 932.5
      y 1651.4892846436483
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 319
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694567;PUBMED:27799534;PUBMED:21203998"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_365"
      name "pp1a forms a dimer"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694567__layout_2167"
      uniprot "NA"
    ]
    graphics [
      x 1098.7805189192002
      y 2413.184617392878
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_365"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 320
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694338;PUBMED:15564471"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_319"
      name "nsp1-4 cleaves itself"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694338__layout_2202"
      uniprot "NA"
    ]
    graphics [
      x 1022.5
      y 1147.8045420565481
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_319"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 321
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-29356"
      hgnc "NA"
      map_id "R2_17"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2203"
      uniprot "NA"
    ]
    graphics [
      x 1832.5
      y 1022.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 322
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694784;urn:miriam:reactome:R-COV-9684866"
      hgnc "NA"
      map_id "R2_18"
      name "nsp1_minus_4"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2204"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 2058.7805189192004
      y 2105.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 323
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694741;urn:miriam:reactome:R-COV-9682210"
      hgnc "NA"
      map_id "R2_19"
      name "nsp1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2205"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 2822.5
      y 981.8989058414568
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 324
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9694760;urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682222"
      hgnc "NA"
      map_id "R2_21"
      name "nsp2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2207"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 2762.5
      y 725.3458248118548
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 325
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694554;urn:miriam:reactome:R-COV-9684869"
      hgnc "NA"
      map_id "R2_20"
      name "nsp3_minus_4"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2206"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1919.2795783000176
      y 212.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 326
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694331;PUBMED:15564471"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_315"
      name "nsp3-4 is glycosylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694331__layout_2209"
      uniprot "NA"
    ]
    graphics [
      x 1412.5
      y 1282.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_315"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 327
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694601;PUBMED:15564471"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_374"
      name "nsp3-4 cleaves itself"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694601__layout_2214"
      uniprot "NA"
    ]
    graphics [
      x 3302.5
      y 1115.5067724401504
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_374"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 328
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-29356"
      hgnc "NA"
      map_id "R2_27"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2215"
      uniprot "NA"
    ]
    graphics [
      x 2762.5
      y 1915.8267364055584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 329
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9684874;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694600"
      hgnc "NA"
      map_id "R2_30"
      name "N_minus_glycan_space_pp1ab_minus_nsp3_minus_4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2218"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 3392.5
      y 2059.61914717945
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 330
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9684877;urn:miriam:reactome:R-COV-9694428"
      hgnc "NA"
      map_id "R2_28"
      name "nsp3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2216"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 2912.5
      y 1599.67376082007
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 331
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694577;urn:miriam:reactome:R-COV-9684876"
      hgnc "NA"
      map_id "R2_29"
      name "nsp4"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2217"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 2020.9135463862924
      y 902.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 332
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694625;PUBMED:12917450;PUBMED:15564471"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_377"
      name "nsp3 cleaves nsp1-4"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694625__layout_2221"
      uniprot "NA"
    ]
    graphics [
      x 3422.5
      y 1509.4381065711991
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_377"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 333
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694611;PUBMED:17855519"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_376"
      name "nsp4 is glycosylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694611__layout_2223"
      uniprot "NA"
    ]
    graphics [
      x 482.5
      y 1274.6023667197312
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_376"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 334
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-1131311;urn:miriam:obo.chebi:CHEBI%3A25609"
      hgnc "NA"
      map_id "R2_33"
      name "a_space_nucleotide_space_sugar"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2224"
      uniprot "NA"
    ]
    graphics [
      x 1412.5
      y 2032.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 335
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-70106"
      hgnc "NA"
      map_id "R2_35"
      name "H_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2226"
      uniprot "NA"
    ]
    graphics [
      x 1442.5
      y 1779.0599084538621
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 336
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57930;urn:miriam:reactome:R-ALL-9684302"
      hgnc "NA"
      map_id "R2_36"
      name "nucleoside_space_5'_minus_diphosphate(3_minus_)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2227"
      uniprot "NA"
    ]
    graphics [
      x 1112.5
      y 1917.0116707084726
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 337
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9684875;urn:miriam:reactome:R-COV-9694569"
      hgnc "NA"
      map_id "R2_34"
      name "N_minus_glycan_space_nsp4"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2225"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 2132.5
      y 1415.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 338
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694317;PUBMED:20542253;PUBMED:24991833;PUBMED:28738245;PUBMED:23943763"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_314"
      name "Nsp3, nsp4, and nsp6 produce replicative organelles"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694317__layout_2507"
      uniprot "NA"
    ]
    graphics [
      x 2568.7805189192004
      y 2244.576720665756
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_314"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 339
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694735;urn:miriam:reactome:R-COV-9682205"
      hgnc "NA"
      map_id "R2_31"
      name "N_minus_glycan_space_nsp3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2220"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 2732.5
      y 1064.6714792204612
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 340
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9694361;urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682227"
      hgnc "NA"
      map_id "R2_182"
      name "nsp6"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2508"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1869.379876527966
      y 1892.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_182"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 341
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694366;urn:miriam:reactome:R-COV-9686003"
      hgnc "NA"
      map_id "R2_152"
      name "nsp3:nsp4"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2448"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 2192.5
      y 1673.7133098641311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_152"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 342
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9694770;urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9686011"
      hgnc "NA"
      map_id "R2_153"
      name "nsp6"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2449"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1031.043215136001
      y 2579.498627734156
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_153"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 343
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694630;PUBMED:18367524"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_378"
      name "Nsp3:nsp4 binds to nsp6"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694630__layout_2447"
      uniprot "NA"
    ]
    graphics [
      x 1061.043215136001
      y 2600.2805759893836
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_378"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 344
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694389;PUBMED:15564471"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_329"
      name "nsp3 is glycosylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694389__layout_2219"
      uniprot "NA"
    ]
    graphics [
      x 2432.5
      y 1286.0089009888093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_329"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 345
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-1131311;urn:miriam:obo.chebi:CHEBI%3A25609"
      hgnc "NA"
      map_id "R2_23"
      name "a_space_nucleotide_space_sugar"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2210"
      uniprot "NA"
    ]
    graphics [
      x 1652.5
      y 957.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 346
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-70106"
      hgnc "NA"
      map_id "R2_24"
      name "H_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2211"
      uniprot "NA"
    ]
    graphics [
      x 2282.5
      y 699.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 347
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57930;urn:miriam:reactome:R-ALL-9684302"
      hgnc "NA"
      map_id "R2_26"
      name "nucleoside_space_5'_minus_diphosphate(3_minus_)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2213"
      uniprot "NA"
    ]
    graphics [
      x 1262.5
      y 1206.9780694277015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 348
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694735;urn:miriam:reactome:R-COV-9682205"
      hgnc "NA"
      map_id "R2_32"
      name "N_minus_glycan_space_nsp3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2222"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 3482.5
      y 1487.953226682637
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 349
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9684863;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694479"
      hgnc "NA"
      map_id "R2_25"
      name "N_minus_glycan_space_nsp3_minus_4"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2212"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 662.5
      y 1745.901254732075
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 350
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694334;PUBMED:15680415"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_317"
      name "mRNA1 is translated to pp1a"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694334__layout_2162"
      uniprot "NA"
    ]
    graphics [
      x 1895.8155480545588
      y 452.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_317"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 351
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-COV-9685518;urn:miriam:reactome:R-COV-9694393"
      hgnc "NA"
      map_id "R2_1"
      name "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_2163"
      uniprot "NA"
    ]
    graphics [
      x 2372.5
      y 892.0608348716244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 352
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694723;PUBMED:31226023"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_391"
      name "Uncoating of SARS-CoV-2 Genome"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694723__layout_2501"
      uniprot "NA"
    ]
    graphics [
      x 1652.5
      y 623.6639077537443
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_391"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 353
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694274;PUBMED:15680415"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_306"
      name "mRNA1 is translated to pp1ab"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694274__layout_2165"
      uniprot "NA"
    ]
    graphics [
      x 1987.432708728739
      y 482.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_306"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 354
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694677;PUBMED:22362731"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_386"
      name "ZCRB1 binds 5'UTR of SARS-CoV-2 genomic RNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694677__layout_2315"
      uniprot "NA"
    ]
    graphics [
      x 2568.7805189192004
      y 2394.576720665756
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_386"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 355
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-9682010;urn:miriam:uniprot:Q8TBF4"
      hgnc "NA"
      map_id "R2_85"
      name "ZCRB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2316"
      uniprot "UNIPROT:Q8TBF4"
    ]
    graphics [
      x 2972.5
      y 1587.4318615488276
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 356
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-9698376;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:Q8TBF4"
      hgnc "NA"
      map_id "R2_243"
      name "ZCRB1:m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3236"
      uniprot "UNIPROT:Q8TBF4"
    ]
    graphics [
      x 2522.5
      y 1892.9715035857598
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_243"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 357
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9694461;urn:miriam:reactome:R-COV-9686697;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:P0DTC9"
      hgnc "NA"
      map_id "R2_176"
      name "encapsidated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA_space_(plus_space_strand)"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2496"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 3242.5
      y 1762.3406631757666
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_176"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 358
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:pubmed:32654247;urn:miriam:reactome:R-COV-9694702;urn:miriam:uniprot:P0DTC9"
      hgnc "NA"
      map_id "R2_227"
      name "N_space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2960"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 1987.432708728739
      y 392.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_227"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 359
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694689;PUBMED:26311884"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_388"
      name "Fusion and Release of SARS-CoV-2 Nucleocapsid"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694689__layout_2494"
      uniprot "NA"
    ]
    graphics [
      x 1428.7805189192002
      y 2452.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_388"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 360
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9698988;PUBMED:21068237;PUBMED:21325420;PUBMED:20926566;PUBMED:15474033;PUBMED:32532959;PUBMED:32362314"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_406"
      name "Direct Host Cell Membrane Membrane Fusion and Release of SARS-CoV-2 Nucleocapsid"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9698988__layout_3336"
      uniprot "NA"
    ]
    graphics [
      x 3452.5
      y 1479.444263232392
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_406"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 361
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-9694785;urn:miriam:uniprot:P0DTC3;urn:miriam:uniprot:P0DTC2;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:P0DTC9;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:P0DTC7;urn:miriam:uniprot:P0DTC5;urn:miriam:uniprot:P0DTC4"
      hgnc "NA"
      map_id "R2_228"
      name "S3:M:E:encapsidated_space__space_SARS_minus_CoV_minus_2_space_genomic_space_RNA:_space_7a:O_minus_glycosyl_space_3a_space_tetramer:glycosylated_minus_ACE2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2971"
      uniprot "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:Q9BYF1;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      x 3512.5
      y 1541.3795629191009
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_228"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 362
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-CFA-2159856;urn:miriam:reactome:R-DRE-2159856;urn:miriam:reactome:R-HSA-2159856;urn:miriam:reactome:R-XTR-2159856;urn:miriam:reactome:R-GGA-2159856;urn:miriam:pubmed:1629222;urn:miriam:uniprot:P09958;urn:miriam:reactome:R-MMU-2159856;urn:miriam:reactome:R-SSC-2159856;urn:miriam:reactome:R-RNO-2159856"
      hgnc "NA"
      map_id "R2_245"
      name "FURIN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_3309"
      uniprot "UNIPROT:P09958"
    ]
    graphics [
      x 3002.5
      y 1206.327521062367
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_245"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 363
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:O15393;urn:miriam:reactome:R-HSA-9686707"
      hgnc "NA"
      map_id "R2_248"
      name "TMPRSS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_3349"
      uniprot "UNIPROT:O15393"
    ]
    graphics [
      x 1712.5
      y 807.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_248"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 364
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:pubmed:14754895;urn:miriam:uniprot:Q9BYF1;urn:miriam:reactome:R-HSA-9683480"
      hgnc "NA"
      map_id "R2_247"
      name "glycosylated_minus_ACE2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_3347"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 3182.5
      y 1769.6963981767485
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_247"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 365
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9686688;urn:miriam:reactome:R-COV-9698997;urn:miriam:uniprot:P0DTC2;urn:miriam:uniprot:P0DTC5;urn:miriam:uniprot:P0DTC4"
      hgnc "NA"
      map_id "R2_246"
      name "S1:S2:M_space_lattice:E_space_protein"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_3345"
      uniprot "UNIPROT:P0DTC2;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      x 3452.5
      y 1815.1001068356327
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_246"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 366
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9699007;PUBMED:32532959;PUBMED:32362314;PUBMED:21068237"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_407"
      name "FURIN Mediated SARS-CoV-2 Spike Protein Cleavage and Endocytosis"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9699007__layout_3308"
      uniprot "NA"
    ]
    graphics [
      x 3332.5
      y 967.5069924106831
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_407"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 367
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-9698958;urn:miriam:uniprot:Q9BYF1"
      hgnc "NA"
      map_id "R2_244"
      name "ACE2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_3279"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1082.5
      y 856.3529417498704
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_244"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 368
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC3;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9694585;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:P0DTC9;urn:miriam:uniprot:P0DTC7;urn:miriam:reactome:R-COV-9686703;urn:miriam:uniprot:P0DTC5;urn:miriam:uniprot:P0DTC4"
      hgnc "NA"
      map_id "R2_171"
      name "S1:S2:M:E:encapsidated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA:_space_7a:O_minus_glycosyl_space_3a_space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2490"
      uniprot "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      x 2012.5
      y 1295.8526976066757
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_171"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 369
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694661;PUBMED:32532959;PUBMED:20926566;PUBMED:21325420;PUBMED:21068237"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_383"
      name "TMPRSS2 Mediated SARS-CoV-2 Spike Protein Cleavage and Endocytosis"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694661__layout_2498"
      uniprot "NA"
    ]
    graphics [
      x 2222.5
      y 1226.7158381002334
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_383"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 370
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694287;PUBMED:22816037;PUBMED:19321428"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_310"
      name "Cleavage of S protein into S1:S2"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694287__layout_2489"
      uniprot "NA"
    ]
    graphics [
      x 2732.5
      y 704.238921635267
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_310"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 371
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC3;urn:miriam:uniprot:P0DTC2;urn:miriam:refseq:MN908947.3;urn:miriam:reactome:R-HSA-9694758;urn:miriam:uniprot:P0DTC9;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:P0DTC7;urn:miriam:uniprot:P0DTC5;urn:miriam:uniprot:P0DTC4;urn:miriam:reactome:R-HSA-9686692"
      hgnc "NA"
      map_id "R2_229"
      name "S3:M:E:encapsidated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA:_space_7a:O_minus_glycosyl_space_3a_space_tetramer:glycosylated_minus_ACE2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2972"
      uniprot "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:Q9BYF1;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      x 2222.5
      y 1886.7158381002334
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_229"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 372
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P07711;urn:miriam:reactome:R-HSA-9686717"
      hgnc "NA"
      map_id "R2_173"
      name "Cathepsin_space_L1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2492"
      uniprot "UNIPROT:P07711"
    ]
    graphics [
      x 2372.5
      y 1912.0608348716244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_173"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 373
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-9683316;urn:miriam:uniprot:P07711;urn:miriam:pubchem.compound:3767"
      hgnc "NA"
      map_id "R2_174"
      name "CTSL:CTSL_space_inhibitors"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2493"
      uniprot "UNIPROT:P07711"
    ]
    graphics [
      x 2167.4327087287393
      y 563.7133098641311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_174"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 374
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:pubmed:14754895;urn:miriam:uniprot:Q9BYF1;urn:miriam:reactome:R-HSA-9683480"
      hgnc "NA"
      map_id "R2_172"
      name "glycosylated_minus_ACE2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2491"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 2492.5
      y 721.4350242348289
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_172"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 375
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9685655;PUBMED:32179150;PUBMED:23105391;PUBMED:16962401;PUBMED:26335104;PUBMED:26953343"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_299"
      name "CTSL bind CTSL inhibitors"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9685655__layout_2922"
      uniprot "NA"
    ]
    graphics [
      x 812.5
      y 1552.6626209543215
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_299"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 376
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9693170;urn:miriam:pubchem.compound:3767"
      hgnc "NA"
      map_id "R2_218"
      name "CTSL_space_inhibitors"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_2923"
      uniprot "NA"
    ]
    graphics [
      x 1292.5
      y 1973.3146446645712
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_218"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 377
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9697423;PUBMED:15163706;PUBMED:15010527;PUBMED:22816037;PUBMED:15140961"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_404"
      name "Endocytosis of SARS-CoV-2 Virion"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9697423__layout_2970"
      uniprot "NA"
    ]
    graphics [
      x 3332.5
      y 1744.3257498350802
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_404"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 378
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:O15393;urn:miriam:reactome:R-HSA-9686707"
      hgnc "NA"
      map_id "R2_178"
      name "TMPRSS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2499"
      uniprot "UNIPROT:O15393"
    ]
    graphics [
      x 2432.5
      y 1076.0089009888093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_178"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 379
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-HSA-9681532;urn:miriam:uniprot:O15393"
      hgnc "NA"
      map_id "R2_179"
      name "TMPRSS2:TMPRSS2_space_inhibitors"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2500"
      uniprot "UNIPROT:O15393"
    ]
    graphics [
      x 2702.5
      y 647.5262206052779
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_179"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 380
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9681514;PUBMED:14647384;PUBMED:32142651;PUBMED:21068237;PUBMED:27550352;PUBMED:27277342;PUBMED:24227843;PUBMED:28414992;PUBMED:22496216"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_296"
      name "TMPRSS2 binds TMPRSS2 inhibitors"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9681514__layout_2924"
      uniprot "NA"
    ]
    graphics [
      x 2042.5
      y 662.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_296"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 381
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-ALL-9682035"
      hgnc "NA"
      map_id "R2_219"
      name "TMPRSS2_space_inhibitors"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_2925"
      uniprot "NA"
    ]
    graphics [
      x 2522.5
      y 1172.9715035857598
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_219"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 382
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694579;PUBMED:14647384;PUBMED:16166518;PUBMED:32125455;PUBMED:22816037"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_370"
      name "Spike glycoprotein of SARS-CoV-2 binds ACE2 on host cell"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694579__layout_2483"
      uniprot "NA"
    ]
    graphics [
      x 1352.5
      y 1569.311461366438
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_370"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 383
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:pubmed:14754895;urn:miriam:uniprot:Q9BYF1;urn:miriam:reactome:R-HSA-9683480"
      hgnc "NA"
      map_id "R2_169"
      name "glycosylated_minus_ACE2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2484"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1172.5
      y 880.9505467799008
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_169"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 384
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC3;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9694500;urn:miriam:refseq:MN908947.3;urn:miriam:uniprot:P0DTC9;urn:miriam:uniprot:P0DTC7;urn:miriam:uniprot:P0DTC5;urn:miriam:uniprot:P0DTC4;urn:miriam:reactome:R-COV-9685506"
      hgnc "NA"
      map_id "R2_170"
      name "S3:M:E:encapsidated_space_SARS_minus_CoV_minus_2_space_genomic_space_RNA:_space_7a:O_minus_glycosyl_space_3a_space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2485"
      uniprot "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC9;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      x 632.5
      y 1845.5228459007562
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_170"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 385
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-CEL-1181251;urn:miriam:reactome:R-HSA-1181251;urn:miriam:reactome:R-XTR-1181251;urn:miriam:reactome:R-CFA-1181251;urn:miriam:reactome:R-GGA-1181251;urn:miriam:reactome:R-MMU-1181251;urn:miriam:reactome:R-RNO-1181251;urn:miriam:reactome:R-DRE-1181251;urn:miriam:reactome:R-DDI-1181251;urn:miriam:reactome:R-PFA-1181251;urn:miriam:uniprot:P55072;urn:miriam:reactome:R-SPO-1181251;urn:miriam:reactome:R-SSC-1181251;urn:miriam:reactome:R-SCE-1181251;urn:miriam:reactome:R-DME-1181251"
      hgnc "NA"
      map_id "R2_177"
      name "VCP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2497"
      uniprot "UNIPROT:P55072"
    ]
    graphics [
      x 1651.33449019633
      y 2847.1855687946227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_177"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 386
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC3;urn:miriam:uniprot:P0DTC2;urn:miriam:reactome:R-COV-9694534;urn:miriam:uniprot:P0DTC7;urn:miriam:uniprot:P0DTC5;urn:miriam:reactome:R-COV-9686705;urn:miriam:uniprot:P0DTC4"
      hgnc "NA"
      map_id "R2_175"
      name "S1:S2:M:E:_space_7a:O_minus_glycosyl_space_3a_space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2495"
      uniprot "UNIPROT:P0DTC3;UNIPROT:P0DTC2;UNIPROT:P0DTC7;UNIPROT:P0DTC5;UNIPROT:P0DTC4"
    ]
    graphics [
      x 1592.5
      y 1433.119912305107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_175"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 387
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-29356"
      hgnc "NA"
      map_id "R2_239"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_3095"
      uniprot "NA"
    ]
    graphics [
      x 842.5
      y 1899.3397606190515
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_239"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 388
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9681991"
      hgnc "NA"
      map_id "R2_191"
      name "pp1a_minus_nsp9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2546"
      uniprot "UNIPROT:P0DTC1"
    ]
    graphics [
      x 1998.7805189192002
      y 2282.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_191"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 389
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9681992"
      hgnc "NA"
      map_id "R2_187"
      name "pp1a_minus_nsp8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2542"
      uniprot "UNIPROT:P0DTC1"
    ]
    graphics [
      x 2732.5
      y 2096.5950284240166
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_187"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 390
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9681990"
      hgnc "NA"
      map_id "R2_188"
      name "pp1a_minus_nsp11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2543"
      uniprot "UNIPROT:P0DTC1"
    ]
    graphics [
      x 1458.7805189192002
      y 2312.184438344133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_188"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 391
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9681995"
      hgnc "NA"
      map_id "R2_189"
      name "pp1a_minus_nsp7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2544"
      uniprot "UNIPROT:P0DTC1"
    ]
    graphics [
      x 1211.043215136001
      y 2634.418854811679
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_189"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 392
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9678265;urn:miriam:reactome:R-COV-9694256"
      hgnc "NA"
      map_id "R2_7"
      name "pp1a_minus_nsp6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2184"
      uniprot "UNIPROT:P0DTC1"
    ]
    graphics [
      x 1391.043215136001
      y 2609.0818025008366
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 393
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:reactome:R-COV-9681983"
      hgnc "NA"
      map_id "R2_190"
      name "pp1a_minus_nsp10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2545"
      uniprot "UNIPROT:P0DTC1"
    ]
    graphics [
      x 2111.043215136001
      y 2675.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_190"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 394
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A2981;urn:miriam:reactome:R-ALL-9731705"
      hgnc "NA"
      map_id "R2_277"
      name "3CLp_space_inhibitors"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_3674"
      uniprot "NA"
    ]
    graphics [
      x 1562.5
      y 1005.4971941397312
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_277"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 395
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694333;PUBMED:32198291"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_316"
      name "3CLp forms a homodimer"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694333__layout_2174"
      uniprot "NA"
    ]
    graphics [
      x 452.5
      y 2079.56913395263
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_316"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 396
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9682197;urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9694748"
      hgnc "NA"
      map_id "R2_4"
      name "nsp5"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2175"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 902.5
      y 1841.7973208312728
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 397
    source 1
    target 2
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_82"
      target_id "R2_345"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 398
    source 3
    target 2
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_81"
      target_id "R2_345"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 399
    source 2
    target 4
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_345"
      target_id "R2_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 400
    source 2
    target 5
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_345"
      target_id "R2_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 401
    source 218
    target 3
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_390"
      target_id "R2_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 402
    source 6
    target 4
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_361"
      target_id "R2_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 403
    source 4
    target 7
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_63"
      target_id "R2_343"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 404
    source 171
    target 6
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_55"
      target_id "R2_361"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 405
    source 172
    target 6
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_62"
      target_id "R2_361"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 406
    source 173
    target 6
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "MODULATION"
      source_id "R2_64"
      target_id "R2_361"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 407
    source 6
    target 174
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_361"
      target_id "R2_251"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 408
    source 6
    target 175
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_361"
      target_id "R2_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 409
    source 8
    target 7
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_106"
      target_id "R2_343"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 410
    source 7
    target 9
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_343"
      target_id "R2_161"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 411
    source 118
    target 8
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_366"
      target_id "R2_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 412
    source 8
    target 10
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_106"
      target_id "R2_309"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 413
    source 9
    target 10
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_161"
      target_id "R2_309"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 414
    source 10
    target 11
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_309"
      target_id "R2_162"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 415
    source 11
    target 12
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_162"
      target_id "R2_338"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 416
    source 13
    target 12
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_163"
      target_id "R2_338"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 417
    source 14
    target 12
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_132"
      target_id "R2_338"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 418
    source 12
    target 15
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_338"
      target_id "R2_164"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 419
    source 105
    target 13
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_347"
      target_id "R2_163"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 420
    source 65
    target 14
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_311"
      target_id "R2_132"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 421
    source 15
    target 16
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_164"
      target_id "R2_363"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 422
    source 17
    target 16
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_242"
      target_id "R2_363"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 423
    source 16
    target 18
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_363"
      target_id "R2_165"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 424
    source 18
    target 19
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_165"
      target_id "R2_356"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 425
    source 20
    target 19
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_126"
      target_id "R2_356"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 426
    source 21
    target 19
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_166"
      target_id "R2_356"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 427
    source 19
    target 22
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_356"
      target_id "R2_167"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 428
    source 27
    target 20
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_384"
      target_id "R2_126"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 429
    source 20
    target 28
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_126"
      target_id "R2_367"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 430
    source 22
    target 23
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_167"
      target_id "R2_380"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 431
    source 23
    target 24
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_380"
      target_id "R2_295"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 432
    source 24
    target 25
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_295"
      target_id "R2_381"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 433
    source 25
    target 26
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_381"
      target_id "R2_168"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 434
    source 30
    target 27
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_124"
      target_id "R2_384"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 435
    source 28
    target 29
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_367"
      target_id "R2_155"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 436
    source 31
    target 30
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_389"
      target_id "R2_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 437
    source 32
    target 31
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_249"
      target_id "R2_389"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 438
    source 33
    target 31
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_122"
      target_id "R2_389"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 439
    source 34
    target 31
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "R2_125"
      target_id "R2_389"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 440
    source 31
    target 35
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_389"
      target_id "R2_250"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 441
    source 95
    target 33
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_336"
      target_id "R2_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 442
    source 34
    target 36
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "R2_125"
      target_id "R2_403"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 443
    source 37
    target 36
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_231"
      target_id "R2_403"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 444
    source 36
    target 38
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_403"
      target_id "R2_235"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 445
    source 39
    target 37
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_402"
      target_id "R2_231"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 446
    source 40
    target 39
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_226"
      target_id "R2_402"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 447
    source 41
    target 39
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "R2_232"
      target_id "R2_402"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 448
    source 42
    target 39
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "R2_233"
      target_id "R2_402"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 449
    source 43
    target 39
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "R2_234"
      target_id "R2_402"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 450
    source 44
    target 40
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_382"
      target_id "R2_226"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 451
    source 45
    target 44
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_208"
      target_id "R2_382"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 452
    source 46
    target 44
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "R2_119"
      target_id "R2_382"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 453
    source 47
    target 44
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "R2_215"
      target_id "R2_382"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 454
    source 48
    target 44
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "R2_216"
      target_id "R2_382"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 455
    source 49
    target 45
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_396"
      target_id "R2_208"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 456
    source 50
    target 49
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_207"
      target_id "R2_396"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 457
    source 51
    target 50
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_344"
      target_id "R2_207"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 458
    source 52
    target 51
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_206"
      target_id "R2_344"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 459
    source 53
    target 52
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_320"
      target_id "R2_206"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 460
    source 54
    target 53
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_205"
      target_id "R2_320"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 461
    source 55
    target 53
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_115"
      target_id "R2_320"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 462
    source 53
    target 56
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_320"
      target_id "R2_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 463
    source 73
    target 54
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_318"
      target_id "R2_205"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 464
    source 55
    target 57
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_115"
      target_id "R2_332"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 465
    source 57
    target 56
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_332"
      target_id "R2_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 466
    source 58
    target 57
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_94"
      target_id "R2_332"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 467
    source 57
    target 59
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_332"
      target_id "R2_128"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 468
    source 66
    target 58
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_308"
      target_id "R2_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 469
    source 58
    target 67
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_94"
      target_id "R2_397"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 470
    source 59
    target 60
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_128"
      target_id "R2_357"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 471
    source 61
    target 60
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_129"
      target_id "R2_357"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 472
    source 60
    target 62
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_357"
      target_id "R2_130"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 473
    source 62
    target 63
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_130"
      target_id "R2_341"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 474
    source 63
    target 64
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_341"
      target_id "R2_131"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 475
    source 64
    target 65
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_131"
      target_id "R2_311"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 476
    source 72
    target 66
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_93"
      target_id "R2_308"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 477
    source 68
    target 67
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_107"
      target_id "R2_397"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 478
    source 67
    target 69
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_397"
      target_id "R2_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 479
    source 67
    target 70
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_397"
      target_id "R2_127"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 480
    source 67
    target 71
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_397"
      target_id "R2_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 481
    source 74
    target 73
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_213"
      target_id "R2_318"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 482
    source 75
    target 73
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "R2_114"
      target_id "R2_318"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 483
    source 76
    target 74
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_401"
      target_id "R2_213"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 484
    source 77
    target 76
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_210"
      target_id "R2_401"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 485
    source 78
    target 76
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "R2_230"
      target_id "R2_401"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 486
    source 79
    target 77
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_324"
      target_id "R2_210"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 487
    source 80
    target 79
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_113"
      target_id "R2_324"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 488
    source 81
    target 79
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_209"
      target_id "R2_324"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 489
    source 82
    target 79
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "R2_111"
      target_id "R2_324"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 490
    source 83
    target 79
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "R2_214"
      target_id "R2_324"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 491
    source 84
    target 79
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "MODULATION"
      source_id "R2_112"
      target_id "R2_324"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 492
    source 79
    target 85
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_324"
      target_id "R2_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 493
    source 88
    target 80
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_399"
      target_id "R2_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 494
    source 82
    target 86
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_111"
      target_id "R2_300"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 495
    source 86
    target 84
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_300"
      target_id "R2_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 496
    source 87
    target 86
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_225"
      target_id "R2_300"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 497
    source 89
    target 88
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_211"
      target_id "R2_399"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 498
    source 90
    target 88
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_90"
      target_id "R2_399"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 499
    source 91
    target 88
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "R2_217"
      target_id "R2_399"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 500
    source 88
    target 92
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_399"
      target_id "R2_212"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 501
    source 93
    target 90
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_340"
      target_id "R2_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 502
    source 94
    target 93
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_89"
      target_id "R2_340"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 503
    source 96
    target 95
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_120"
      target_id "R2_336"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 504
    source 97
    target 95
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_121"
      target_id "R2_336"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 505
    source 98
    target 95
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "R2_123"
      target_id "R2_336"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 506
    source 95
    target 99
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_336"
      target_id "R2_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 507
    source 95
    target 100
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_336"
      target_id "R2_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 508
    source 101
    target 96
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_331"
      target_id "R2_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 509
    source 102
    target 101
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_92"
      target_id "R2_331"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 510
    source 103
    target 102
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_400"
      target_id "R2_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 511
    source 104
    target 103
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_91"
      target_id "R2_400"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 512
    source 106
    target 105
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_133"
      target_id "R2_347"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 513
    source 107
    target 105
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_160"
      target_id "R2_347"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 514
    source 116
    target 106
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_364"
      target_id "R2_133"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 515
    source 108
    target 107
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_325"
      target_id "R2_160"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 516
    source 109
    target 108
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_135"
      target_id "R2_325"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 517
    source 110
    target 109
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_355"
      target_id "R2_135"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 518
    source 111
    target 110
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_96"
      target_id "R2_355"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 519
    source 112
    target 110
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_134"
      target_id "R2_355"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 520
    source 110
    target 113
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_355"
      target_id "R2_137"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 521
    source 110
    target 114
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_355"
      target_id "R2_136"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 522
    source 115
    target 111
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_387"
      target_id "R2_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 523
    source 111
    target 116
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_96"
      target_id "R2_364"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 524
    source 117
    target 115
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_95"
      target_id "R2_387"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 525
    source 119
    target 118
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_102"
      target_id "R2_366"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 526
    source 120
    target 119
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_323"
      target_id "R2_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 527
    source 119
    target 121
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_102"
      target_id "R2_368"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 528
    source 123
    target 120
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_275"
      target_id "R2_323"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 529
    source 121
    target 122
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_368"
      target_id "R2_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 530
    source 124
    target 123
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_412"
      target_id "R2_275"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 531
    source 125
    target 124
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_274"
      target_id "R2_412"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 532
    source 126
    target 124
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_272"
      target_id "R2_412"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 533
    source 124
    target 127
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_412"
      target_id "R2_293"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 534
    source 128
    target 126
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_409"
      target_id "R2_272"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 535
    source 129
    target 128
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_261"
      target_id "R2_409"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 536
    source 130
    target 128
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_149"
      target_id "R2_409"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 537
    source 131
    target 128
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "R2_151"
      target_id "R2_409"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 538
    source 128
    target 132
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_409"
      target_id "R2_150"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 539
    source 128
    target 133
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_409"
      target_id "R2_273"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 540
    source 134
    target 129
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_410"
      target_id "R2_261"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 541
    source 135
    target 134
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_260"
      target_id "R2_410"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 542
    source 136
    target 134
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_288"
      target_id "R2_410"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 543
    source 137
    target 134
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "R2_289"
      target_id "R2_410"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 544
    source 134
    target 138
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_410"
      target_id "R2_262"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 545
    source 134
    target 139
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_410"
      target_id "R2_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 546
    source 140
    target 135
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_411"
      target_id "R2_260"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 547
    source 141
    target 140
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_281"
      target_id "R2_411"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 548
    source 142
    target 140
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_263"
      target_id "R2_411"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 549
    source 140
    target 143
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_411"
      target_id "R2_282"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 550
    source 140
    target 144
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_411"
      target_id "R2_264"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 551
    source 145
    target 142
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_413"
      target_id "R2_263"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 552
    source 146
    target 145
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_269"
      target_id "R2_413"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 553
    source 147
    target 145
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_283"
      target_id "R2_413"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 554
    source 148
    target 145
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "R2_290"
      target_id "R2_413"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 555
    source 145
    target 149
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_413"
      target_id "R2_284"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 556
    source 145
    target 150
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_413"
      target_id "R2_265"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 557
    source 151
    target 146
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_408"
      target_id "R2_269"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 558
    source 152
    target 151
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_266"
      target_id "R2_408"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 559
    source 153
    target 151
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_286"
      target_id "R2_408"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 560
    source 154
    target 151
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "R2_84"
      target_id "R2_408"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 561
    source 155
    target 151
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "MODULATION"
      source_id "R2_270"
      target_id "R2_408"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 562
    source 151
    target 156
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_408"
      target_id "R2_287"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 563
    source 151
    target 157
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_408"
      target_id "R2_271"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 564
    source 161
    target 152
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_414"
      target_id "R2_266"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 565
    source 158
    target 155
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_301"
      target_id "R2_270"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 566
    source 159
    target 158
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_101"
      target_id "R2_301"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 567
    source 160
    target 158
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_224"
      target_id "R2_301"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 568
    source 162
    target 161
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_285"
      target_id "R2_414"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 569
    source 163
    target 161
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_98"
      target_id "R2_414"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 570
    source 164
    target 161
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "R2_292"
      target_id "R2_414"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 571
    source 161
    target 165
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_414"
      target_id "R2_291"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 572
    source 161
    target 166
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_414"
      target_id "R2_267"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 573
    source 167
    target 163
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_326"
      target_id "R2_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 574
    source 163
    target 168
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_98"
      target_id "R2_327"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 575
    source 170
    target 167
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_97"
      target_id "R2_326"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 576
    source 168
    target 169
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_327"
      target_id "R2_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 577
    source 176
    target 171
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_375"
      target_id "R2_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 578
    source 184
    target 171
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_379"
      target_id "R2_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 579
    source 171
    target 188
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_55"
      target_id "R2_385"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 580
    source 171
    target 181
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_55"
      target_id "R2_398"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 581
    source 188
    target 173
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_385"
      target_id "R2_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 582
    source 174
    target 245
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_251"
      target_id "R2_348"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 583
    source 176
    target 175
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_375"
      target_id "R2_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 584
    source 177
    target 176
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_54"
      target_id "R2_375"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 585
    source 178
    target 176
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_52"
      target_id "R2_375"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 586
    source 179
    target 176
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "MODULATION"
      source_id "R2_57"
      target_id "R2_375"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 587
    source 177
    target 181
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_54"
      target_id "R2_398"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 588
    source 189
    target 178
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_307"
      target_id "R2_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 589
    source 178
    target 180
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_52"
      target_id "R2_359"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 590
    source 180
    target 179
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_359"
      target_id "R2_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 591
    source 179
    target 181
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "MODULATION"
      source_id "R2_57"
      target_id "R2_398"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 592
    source 187
    target 180
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_180"
      target_id "R2_359"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 593
    source 181
    target 182
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_398"
      target_id "R2_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 594
    source 181
    target 183
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_398"
      target_id "R2_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 595
    source 182
    target 184
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_58"
      target_id "R2_379"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 596
    source 185
    target 184
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_60"
      target_id "R2_379"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 597
    source 184
    target 186
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_379"
      target_id "R2_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 598
    source 187
    target 188
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_180"
      target_id "R2_385"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 599
    source 190
    target 189
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_278"
      target_id "R2_307"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 600
    source 191
    target 189
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_49"
      target_id "R2_307"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 601
    source 189
    target 192
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_307"
      target_id "R2_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 602
    source 190
    target 193
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_278"
      target_id "R2_321"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 603
    source 240
    target 191
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_305"
      target_id "R2_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 604
    source 191
    target 193
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_49"
      target_id "R2_321"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 605
    source 193
    target 192
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_321"
      target_id "R2_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 606
    source 194
    target 193
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PHYSICAL_STIMULATION"
      source_id "R2_268"
      target_id "R2_321"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 607
    source 193
    target 195
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_321"
      target_id "R2_256"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 608
    source 195
    target 196
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_256"
      target_id "R2_351"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 609
    source 197
    target 196
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_138"
      target_id "R2_351"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 610
    source 196
    target 198
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_351"
      target_id "R2_257"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 611
    source 196
    target 199
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_351"
      target_id "R2_139"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 612
    source 198
    target 200
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_257"
      target_id "R2_346"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 613
    source 201
    target 200
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_140"
      target_id "R2_346"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 614
    source 202
    target 200
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_142"
      target_id "R2_346"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 615
    source 203
    target 200
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_141"
      target_id "R2_346"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 616
    source 200
    target 204
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_346"
      target_id "R2_144"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 617
    source 200
    target 205
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_346"
      target_id "R2_258"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 618
    source 200
    target 206
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_346"
      target_id "R2_143"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 619
    source 200
    target 207
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_346"
      target_id "R2_279"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 620
    source 205
    target 208
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_258"
      target_id "R2_350"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 621
    source 208
    target 206
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_350"
      target_id "R2_143"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 622
    source 209
    target 208
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_280"
      target_id "R2_350"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 623
    source 208
    target 210
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_350"
      target_id "R2_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 624
    source 208
    target 211
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_350"
      target_id "R2_145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 625
    source 216
    target 210
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_353"
      target_id "R2_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 626
    source 217
    target 210
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_369"
      target_id "R2_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 627
    source 218
    target 210
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_390"
      target_id "R2_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 628
    source 210
    target 219
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_46"
      target_id "R2_342"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 629
    source 210
    target 220
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_46"
      target_id "R2_349"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 630
    source 211
    target 212
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_145"
      target_id "R2_394"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 631
    source 213
    target 212
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_146"
      target_id "R2_394"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 632
    source 212
    target 214
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_394"
      target_id "R2_147"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 633
    source 212
    target 215
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_394"
      target_id "R2_148"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 634
    source 250
    target 216
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_252"
      target_id "R2_353"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 635
    source 248
    target 216
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_66"
      target_id "R2_353"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 636
    source 216
    target 221
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_353"
      target_id "R2_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 637
    source 216
    target 244
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_353"
      target_id "R2_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 638
    source 252
    target 217
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_44"
      target_id "R2_369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 639
    source 253
    target 217
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_154"
      target_id "R2_369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 640
    source 254
    target 217
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_45"
      target_id "R2_369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 641
    source 233
    target 218
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_255"
      target_id "R2_390"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 642
    source 232
    target 218
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_76"
      target_id "R2_390"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 643
    source 218
    target 244
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_390"
      target_id "R2_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 644
    source 238
    target 219
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_276"
      target_id "R2_342"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 645
    source 219
    target 239
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_342"
      target_id "R2_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 646
    source 221
    target 220
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_71"
      target_id "R2_349"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 647
    source 220
    target 222
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_349"
      target_id "R2_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 648
    source 224
    target 221
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_372"
      target_id "R2_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 649
    source 222
    target 223
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_72"
      target_id "R2_334"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 650
    source 222
    target 224
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_72"
      target_id "R2_372"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 651
    source 237
    target 223
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_181"
      target_id "R2_334"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 652
    source 223
    target 226
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_334"
      target_id "R2_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 653
    source 225
    target 224
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_73"
      target_id "R2_372"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 654
    source 226
    target 224
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "MODULATION"
      source_id "R2_75"
      target_id "R2_372"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 655
    source 224
    target 227
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_372"
      target_id "R2_253"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 656
    source 224
    target 228
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_372"
      target_id "R2_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 657
    source 227
    target 229
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_253"
      target_id "R2_395"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 658
    source 230
    target 229
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_254"
      target_id "R2_395"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 659
    source 231
    target 229
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_77"
      target_id "R2_395"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 660
    source 232
    target 229
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_76"
      target_id "R2_395"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 661
    source 229
    target 233
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_395"
      target_id "R2_255"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 662
    source 229
    target 234
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_395"
      target_id "R2_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 663
    source 229
    target 235
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_395"
      target_id "R2_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 664
    source 229
    target 236
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_395"
      target_id "R2_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 665
    source 239
    target 240
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_47"
      target_id "R2_305"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 666
    source 241
    target 240
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_48"
      target_id "R2_305"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 667
    source 240
    target 242
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_305"
      target_id "R2_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 668
    source 240
    target 243
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_305"
      target_id "R2_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 669
    source 245
    target 244
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_348"
      target_id "R2_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 670
    source 246
    target 245
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_67"
      target_id "R2_348"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 671
    source 247
    target 245
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_65"
      target_id "R2_348"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 672
    source 248
    target 245
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_66"
      target_id "R2_348"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 673
    source 245
    target 249
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_348"
      target_id "R2_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 674
    source 245
    target 250
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_348"
      target_id "R2_252"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 675
    source 245
    target 251
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_348"
      target_id "R2_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 676
    source 280
    target 252
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_358"
      target_id "R2_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 677
    source 343
    target 253
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_378"
      target_id "R2_154"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 678
    source 255
    target 254
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_339"
      target_id "R2_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 679
    source 256
    target 255
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_39"
      target_id "R2_339"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 680
    source 257
    target 255
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_294"
      target_id "R2_339"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 681
    source 258
    target 255
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_14"
      target_id "R2_339"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 682
    source 256
    target 295
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_39"
      target_id "R2_360"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 683
    source 259
    target 258
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_393"
      target_id "R2_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 684
    source 258
    target 260
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_14"
      target_id "R2_352"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 685
    source 263
    target 259
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_201"
      target_id "R2_393"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 686
    source 264
    target 259
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_202"
      target_id "R2_393"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 687
    source 265
    target 259
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "R2_5"
      target_id "R2_393"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 688
    source 266
    target 259
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "MODULATION"
      source_id "R2_6"
      target_id "R2_393"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 689
    source 259
    target 267
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_393"
      target_id "R2_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 690
    source 259
    target 268
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_393"
      target_id "R2_204"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 691
    source 259
    target 269
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_393"
      target_id "R2_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 692
    source 259
    target 270
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_393"
      target_id "R2_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 693
    source 259
    target 271
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_393"
      target_id "R2_196"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 694
    source 259
    target 272
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_393"
      target_id "R2_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 695
    source 259
    target 273
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_393"
      target_id "R2_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 696
    source 259
    target 274
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_393"
      target_id "R2_203"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 697
    source 259
    target 275
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_393"
      target_id "R2_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 698
    source 259
    target 276
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_393"
      target_id "R2_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 699
    source 259
    target 277
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_393"
      target_id "R2_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 700
    source 261
    target 260
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_88"
      target_id "R2_352"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 701
    source 260
    target 262
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_352"
      target_id "R2_237"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 702
    source 353
    target 263
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_306"
      target_id "R2_201"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 703
    source 395
    target 265
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_316"
      target_id "R2_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 704
    source 265
    target 299
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_5"
      target_id "R2_373"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 705
    source 265
    target 300
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "R2_5"
      target_id "R2_362"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 706
    source 265
    target 301
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "R2_5"
      target_id "R2_337"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 707
    source 299
    target 266
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_373"
      target_id "R2_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 708
    source 266
    target 300
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "MODULATION"
      source_id "R2_6"
      target_id "R2_362"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 709
    source 266
    target 301
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "MODULATION"
      source_id "R2_6"
      target_id "R2_337"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 710
    source 267
    target 295
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_10"
      target_id "R2_360"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 711
    source 269
    target 296
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_12"
      target_id "R2_333"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 712
    source 269
    target 285
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_12"
      target_id "R2_354"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 713
    source 271
    target 290
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_196"
      target_id "R2_303"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 714
    source 273
    target 278
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_11"
      target_id "R2_335"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 715
    source 278
    target 279
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_335"
      target_id "R2_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 716
    source 279
    target 280
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_43"
      target_id "R2_358"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 717
    source 279
    target 281
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_43"
      target_id "R2_330"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 718
    source 284
    target 280
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_42"
      target_id "R2_358"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 719
    source 282
    target 281
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_87"
      target_id "R2_330"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 720
    source 281
    target 283
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_330"
      target_id "R2_238"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 721
    source 285
    target 284
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_354"
      target_id "R2_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 722
    source 286
    target 285
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_41"
      target_id "R2_354"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 723
    source 287
    target 286
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_312"
      target_id "R2_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 724
    source 288
    target 287
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_40"
      target_id "R2_312"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 725
    source 289
    target 287
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_197"
      target_id "R2_312"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 726
    source 295
    target 288
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_360"
      target_id "R2_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 727
    source 290
    target 289
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_303"
      target_id "R2_197"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 728
    source 291
    target 290
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_193"
      target_id "R2_303"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 729
    source 292
    target 290
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_195"
      target_id "R2_303"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 730
    source 291
    target 293
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_193"
      target_id "R2_302"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 731
    source 293
    target 292
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_302"
      target_id "R2_195"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 732
    source 294
    target 293
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_194"
      target_id "R2_302"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 733
    source 297
    target 296
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_86"
      target_id "R2_333"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 734
    source 296
    target 298
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_333"
      target_id "R2_236"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 735
    source 394
    target 299
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_277"
      target_id "R2_373"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 736
    source 387
    target 300
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_239"
      target_id "R2_362"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 737
    source 306
    target 300
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_3"
      target_id "R2_362"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 738
    source 300
    target 388
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_362"
      target_id "R2_191"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 739
    source 300
    target 389
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_362"
      target_id "R2_187"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 740
    source 300
    target 390
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_362"
      target_id "R2_188"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 741
    source 300
    target 391
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_362"
      target_id "R2_189"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 742
    source 300
    target 392
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_362"
      target_id "R2_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 743
    source 300
    target 393
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_362"
      target_id "R2_190"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 744
    source 302
    target 301
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_198"
      target_id "R2_337"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 745
    source 303
    target 301
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_199"
      target_id "R2_337"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 746
    source 301
    target 304
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_337"
      target_id "R2_192"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 747
    source 301
    target 305
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_337"
      target_id "R2_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 748
    source 301
    target 306
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_337"
      target_id "R2_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 749
    source 350
    target 302
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_317"
      target_id "R2_198"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 750
    source 302
    target 307
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_198"
      target_id "R2_328"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 751
    source 302
    target 319
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_198"
      target_id "R2_365"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 752
    source 307
    target 304
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_328"
      target_id "R2_192"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 753
    source 307
    target 305
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_328"
      target_id "R2_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 754
    source 305
    target 320
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "R2_22"
      target_id "R2_319"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 755
    source 307
    target 306
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_328"
      target_id "R2_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 756
    source 306
    target 308
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PHYSICAL_STIMULATION"
      source_id "R2_3"
      target_id "R2_405"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 757
    source 317
    target 307
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_200"
      target_id "R2_328"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 758
    source 318
    target 307
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "R2_2"
      target_id "R2_328"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 759
    source 309
    target 308
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_183"
      target_id "R2_405"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 760
    source 310
    target 308
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PHYSICAL_STIMULATION"
      source_id "R2_186"
      target_id "R2_405"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 761
    source 311
    target 308
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PHYSICAL_STIMULATION"
      source_id "R2_185"
      target_id "R2_405"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 762
    source 312
    target 308
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PHYSICAL_STIMULATION"
      source_id "R2_259"
      target_id "R2_405"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 763
    source 308
    target 313
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_405"
      target_id "R2_184"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 764
    source 309
    target 314
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_183"
      target_id "R2_371"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 765
    source 315
    target 314
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_241"
      target_id "R2_371"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 766
    source 314
    target 316
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_371"
      target_id "R2_240"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 767
    source 319
    target 318
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_365"
      target_id "R2_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 768
    source 321
    target 320
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_17"
      target_id "R2_319"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 769
    source 322
    target 320
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_18"
      target_id "R2_319"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 770
    source 320
    target 323
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_319"
      target_id "R2_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 771
    source 320
    target 324
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_319"
      target_id "R2_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 772
    source 320
    target 325
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_319"
      target_id "R2_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 773
    source 321
    target 332
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_17"
      target_id "R2_377"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 774
    source 322
    target 332
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_18"
      target_id "R2_377"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 775
    source 332
    target 323
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_377"
      target_id "R2_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 776
    source 332
    target 324
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_377"
      target_id "R2_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 777
    source 325
    target 326
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_20"
      target_id "R2_315"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 778
    source 325
    target 327
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_20"
      target_id "R2_374"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 779
    source 345
    target 326
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_23"
      target_id "R2_315"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 780
    source 326
    target 349
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_315"
      target_id "R2_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 781
    source 326
    target 346
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_315"
      target_id "R2_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 782
    source 326
    target 347
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_315"
      target_id "R2_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 783
    source 328
    target 327
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_27"
      target_id "R2_374"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 784
    source 329
    target 327
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "R2_30"
      target_id "R2_374"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 785
    source 327
    target 330
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_374"
      target_id "R2_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 786
    source 327
    target 331
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_374"
      target_id "R2_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 787
    source 332
    target 330
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_377"
      target_id "R2_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 788
    source 330
    target 344
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_28"
      target_id "R2_329"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 789
    source 332
    target 331
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_377"
      target_id "R2_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 790
    source 331
    target 333
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_29"
      target_id "R2_376"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 791
    source 348
    target 332
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "R2_32"
      target_id "R2_377"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 792
    source 334
    target 333
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_33"
      target_id "R2_376"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 793
    source 333
    target 335
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_376"
      target_id "R2_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 794
    source 333
    target 336
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_376"
      target_id "R2_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 795
    source 333
    target 337
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_376"
      target_id "R2_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 796
    source 337
    target 338
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_34"
      target_id "R2_314"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 797
    source 339
    target 338
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_31"
      target_id "R2_314"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 798
    source 340
    target 338
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_182"
      target_id "R2_314"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 799
    source 338
    target 341
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_314"
      target_id "R2_152"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 800
    source 338
    target 342
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_314"
      target_id "R2_153"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 801
    source 344
    target 339
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_329"
      target_id "R2_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 802
    source 341
    target 343
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_152"
      target_id "R2_378"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 803
    source 342
    target 343
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_153"
      target_id "R2_378"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 804
    source 345
    target 344
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_23"
      target_id "R2_329"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 805
    source 344
    target 346
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_329"
      target_id "R2_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 806
    source 344
    target 347
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_329"
      target_id "R2_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 807
    source 351
    target 350
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_1"
      target_id "R2_317"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 808
    source 352
    target 351
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_391"
      target_id "R2_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 809
    source 351
    target 353
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_1"
      target_id "R2_306"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 810
    source 351
    target 354
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_1"
      target_id "R2_386"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 811
    source 357
    target 352
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_176"
      target_id "R2_391"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 812
    source 352
    target 358
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_391"
      target_id "R2_227"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 813
    source 355
    target 354
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_85"
      target_id "R2_386"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 814
    source 354
    target 356
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_386"
      target_id "R2_243"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 815
    source 359
    target 357
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_388"
      target_id "R2_176"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 816
    source 360
    target 357
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_406"
      target_id "R2_176"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 817
    source 368
    target 359
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_171"
      target_id "R2_388"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 818
    source 385
    target 359
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PHYSICAL_STIMULATION"
      source_id "R2_177"
      target_id "R2_388"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 819
    source 359
    target 386
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_388"
      target_id "R2_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 820
    source 361
    target 360
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_228"
      target_id "R2_406"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 821
    source 362
    target 360
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PHYSICAL_STIMULATION"
      source_id "R2_245"
      target_id "R2_406"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 822
    source 363
    target 360
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PHYSICAL_STIMULATION"
      source_id "R2_248"
      target_id "R2_406"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 823
    source 360
    target 364
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_406"
      target_id "R2_247"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 824
    source 360
    target 365
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_406"
      target_id "R2_246"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 825
    source 382
    target 361
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_370"
      target_id "R2_228"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 826
    source 361
    target 377
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_228"
      target_id "R2_404"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 827
    source 361
    target 366
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_228"
      target_id "R2_407"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 828
    source 361
    target 369
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_228"
      target_id "R2_383"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 829
    source 362
    target 366
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "R2_245"
      target_id "R2_407"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 830
    source 366
    target 367
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_407"
      target_id "R2_244"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 831
    source 366
    target 368
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_407"
      target_id "R2_171"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 832
    source 369
    target 367
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_383"
      target_id "R2_244"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 833
    source 369
    target 368
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_383"
      target_id "R2_171"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 834
    source 370
    target 368
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_310"
      target_id "R2_171"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 835
    source 378
    target 369
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "R2_178"
      target_id "R2_383"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 836
    source 379
    target 369
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "MODULATION"
      source_id "R2_179"
      target_id "R2_383"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 837
    source 371
    target 370
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_229"
      target_id "R2_310"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 838
    source 372
    target 370
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CATALYSIS"
      source_id "R2_173"
      target_id "R2_310"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 839
    source 373
    target 370
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "MODULATION"
      source_id "R2_174"
      target_id "R2_310"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 840
    source 370
    target 374
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_310"
      target_id "R2_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 841
    source 377
    target 371
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_404"
      target_id "R2_229"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 842
    source 372
    target 375
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_173"
      target_id "R2_299"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 843
    source 375
    target 373
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_299"
      target_id "R2_174"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 844
    source 376
    target 375
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_218"
      target_id "R2_299"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 845
    source 378
    target 380
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_178"
      target_id "R2_296"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 846
    source 380
    target 379
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_296"
      target_id "R2_179"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 847
    source 381
    target 380
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_219"
      target_id "R2_296"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 848
    source 383
    target 382
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_169"
      target_id "R2_370"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 849
    source 384
    target 382
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_170"
      target_id "R2_370"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 850
    source 396
    target 395
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_4"
      target_id "R2_316"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
