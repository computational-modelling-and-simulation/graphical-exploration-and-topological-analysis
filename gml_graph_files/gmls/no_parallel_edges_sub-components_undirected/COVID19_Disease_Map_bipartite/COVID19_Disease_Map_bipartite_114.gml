# generated with VANTED V2.8.2 at Fri Mar 04 10:04:34 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:interpro:IPR002552"
      hgnc "NA"
      map_id "M18_188"
      name "S2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2188"
      uniprot "NA"
    ]
    graphics [
      x 3152.5
      y 1222.6115175143973
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_188"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:32142651"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_107"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re843"
      uniprot "NA"
    ]
    graphics [
      x 1802.5
      y 1802.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "PUBMED:32047258"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_121"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re981"
      uniprot "NA"
    ]
    graphics [
      x 3272.5
      y 1069.3290600025891
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:interpro:IPR002552"
      hgnc "NA"
      map_id "M18_164"
      name "S2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2052"
      uniprot "NA"
    ]
    graphics [
      x 3242.5
      y 2200.296191534207
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_164"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "PUBMED:32047258;PUBMED:32142651"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_120"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re979"
      uniprot "NA"
    ]
    graphics [
      x 3302.5
      y 1897.1565950810616
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_3"
      name "Nucleocapsid"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa365"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 2501.043215136001
      y 2668.6566737692056
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570;urn:miriam:hgnc.symbol:E;urn:miriam:uniprot:P0DTC4"
      hgnc "HGNC_SYMBOL:E"
      map_id "M18_167"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2062"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 2028.7805189192002
      y 2135.852697606676
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_167"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740573;urn:miriam:uniprot:P0DTC7"
      hgnc "NA"
      map_id "M18_165"
      name "Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2060"
      uniprot "UNIPROT:P0DTC7"
    ]
    graphics [
      x 2988.7805189192004
      y 2264.936290989684
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_165"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "M18_166"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2061"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 2372.5
      y 1372.0608348716244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_166"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_5"
      name "Nucleocapsid"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa369"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 2928.7805189192004
      y 2282.014859733602
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740573;urn:miriam:uniprot:P0DTC7"
      hgnc "NA"
      map_id "M18_182"
      name "Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2114"
      uniprot "UNIPROT:P0DTC7"
    ]
    graphics [
      x 2312.5
      y 729.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_182"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570;urn:miriam:hgnc.symbol:E;urn:miriam:uniprot:P0DTC4"
      hgnc "HGNC_SYMBOL:E"
      map_id "M18_183"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2115"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 2568.7805189192004
      y 2424.576720665756
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_183"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "M18_184"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2116"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 1938.7805189192002
      y 2582.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_184"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "PUBMED:32047258;PUBMED:32142651;PUBMED:32944968;PUBMED:32094589"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_109"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re855"
      uniprot "NA"
    ]
    graphics [
      x 3002.5
      y 1695.9723789255502
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_110"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re859"
      uniprot "NA"
    ]
    graphics [
      x 3422.5
      y 1473.7848897986798
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:refseq:NC_045512"
      hgnc "NA"
      map_id "M18_134"
      name "(_plus_)ss_space_gRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa1675"
      uniprot "NA"
    ]
    graphics [
      x 2612.5
      y 730.8049225365115
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_133"
      name "N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1667"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 2732.5
      y 2006.5950284240166
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_133"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_113"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re908"
      uniprot "NA"
    ]
    graphics [
      x 1472.5
      y 1426.4685701968617
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_155"
      name "s2919"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa1920"
      uniprot "NA"
    ]
    graphics [
      x 1352.5
      y 1689.311461366438
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_155"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_99"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1115"
      uniprot "NA"
    ]
    graphics [
      x 1322.5
      y 690.1482085105855
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_98"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1113"
      uniprot "NA"
    ]
    graphics [
      x 3032.5
      y 1701.1000550923975
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_53"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1063"
      uniprot "NA"
    ]
    graphics [
      x 2162.5
      y 1193.7133098641311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_12"
      name "Replication_space_transcription_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa410"
      uniprot "NA"
    ]
    graphics [
      x 2732.5
      y 998.9883079255927
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:refseq:NC_045512"
      hgnc "NA"
      map_id "M18_245"
      name "(_minus_)ss_space_gRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2328"
      uniprot "NA"
    ]
    graphics [
      x 2162.5
      y 905.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_245"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_64"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1076"
      uniprot "NA"
    ]
    graphics [
      x 932.5
      y 1711.4892846436483
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "PUBMED:8830530"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_103"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1123"
      uniprot "NA"
    ]
    graphics [
      x 2492.5
      y 1531.435024234829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_235"
      name "Orf7b_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2318"
      uniprot "NA"
    ]
    graphics [
      x 2702.5
      y 1367.0678799150003
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_235"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740575"
      hgnc "NA"
      map_id "M18_267"
      name "N_space__space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2353"
      uniprot "NA"
    ]
    graphics [
      x 3332.5
      y 1539.2060711780277
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_267"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571"
      hgnc "NA"
      map_id "M18_244"
      name "M_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2327"
      uniprot "NA"
    ]
    graphics [
      x 1691.043215136001
      y 2727.1855687946227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_244"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570"
      hgnc "NA"
      map_id "M18_243"
      name "E_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2326"
      uniprot "NA"
    ]
    graphics [
      x 2852.5
      y 819.8252654416042
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_243"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_242"
      name "S_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2325"
      uniprot "NA"
    ]
    graphics [
      x 812.5
      y 2035.4401700720625
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_242"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_241"
      name "Orf7a_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2324"
      uniprot "NA"
    ]
    graphics [
      x 1802.5
      y 542.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_241"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_240"
      name "Orf6_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2323"
      uniprot "NA"
    ]
    graphics [
      x 1622.5
      y 1822.5365071385852
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_240"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_239"
      name "Orf3a_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2322"
      uniprot "NA"
    ]
    graphics [
      x 3452.5
      y 1659.444263232392
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_239"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_238"
      name "Orf8_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2321"
      uniprot "NA"
    ]
    graphics [
      x 2688.7805189192004
      y 2168.8467133692384
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_238"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_237"
      name "Orf9b_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2320"
      uniprot "NA"
    ]
    graphics [
      x 1352.5
      y 1839.311461366438
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_237"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_236"
      name "Orf14_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2319"
      uniprot "NA"
    ]
    graphics [
      x 1352.5
      y 939.3114613664379
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_236"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_73"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1085"
      uniprot "NA"
    ]
    graphics [
      x 2210.095385997597
      y 399.06618499081924
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_265"
      name "Orf14_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2351"
      uniprot "NA"
    ]
    graphics [
      x 2012.5
      y 722.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_265"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_84"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1097"
      uniprot "NA"
    ]
    graphics [
      x 3392.5
      y 1032.4630189434856
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_231"
      name "Orf14_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2301"
      uniprot "NA"
    ]
    graphics [
      x 3542.5
      y 1794.2528232882507
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_231"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_178"
      name "Orf14_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2110"
      uniprot "NA"
    ]
    graphics [
      x 2402.5
      y 702.9806537083268
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_178"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_49"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1030"
      uniprot "NA"
    ]
    graphics [
      x 2942.5
      y 1372.6505321512923
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0070992"
      hgnc "NA"
      map_id "M18_14"
      name "Host_space_translation_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa428"
      uniprot "NA"
    ]
    graphics [
      x 1968.7805189192002
      y 2282.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTD3"
      hgnc "NA"
      map_id "M18_148"
      name "Orf14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1877"
      uniprot "UNIPROT:P0DTD3"
    ]
    graphics [
      x 3062.5
      y 1786.0648090877098
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_148"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_38"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1019"
      uniprot "NA"
    ]
    graphics [
      x 3272.5
      y 1782.3067640942568
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTD3"
      hgnc "NA"
      map_id "M18_220"
      name "Orf14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2250"
      uniprot "UNIPROT:P0DTD3"
    ]
    graphics [
      x 3032.5
      y 2207.1723107651524
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_220"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_44"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1025"
      uniprot "NA"
    ]
    graphics [
      x 738.7805189192002
      y 2492.9775029608195
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_50"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1031"
      uniprot "NA"
    ]
    graphics [
      x 2342.5
      y 1262.963502381953
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_41"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1022"
      uniprot "NA"
    ]
    graphics [
      x 362.5
      y 1779.3036330292925
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_42"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1023"
      uniprot "NA"
    ]
    graphics [
      x 482.5
      y 1700.7170144315944
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_48"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1029"
      uniprot "NA"
    ]
    graphics [
      x 1292.5
      y 1643.3146446645712
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1026"
      uniprot "NA"
    ]
    graphics [
      x 2882.5
      y 1713.858034494286
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_46"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1027"
      uniprot "NA"
    ]
    graphics [
      x 3002.5
      y 913.4106042632473
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1028"
      uniprot "NA"
    ]
    graphics [
      x 1915.5437299022901
      y 2852.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_43"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1024"
      uniprot "NA"
    ]
    graphics [
      x 3122.5
      y 2006.9605253255231
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_180"
      name "S_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2112"
      uniprot "NA"
    ]
    graphics [
      x 1788.7805189192002
      y 2222.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_180"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "M18_138"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1688"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 2492.5
      y 1141.435024234829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_138"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_32"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1013"
      uniprot "NA"
    ]
    graphics [
      x 1082.5
      y 1276.3529417498703
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "M18_154"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1893"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 842.5
      y 1342.9240290532734
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_154"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_29"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1010"
      uniprot "NA"
    ]
    graphics [
      x 1837.432708728739
      y 332.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "M18_143"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1859"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 1622.5
      y 1012.5365071385852
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_143"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_78"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1091"
      uniprot "NA"
    ]
    graphics [
      x 888.7805189192002
      y 2375.750201068447
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_259"
      name "S_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2345"
      uniprot "NA"
    ]
    graphics [
      x 971.043215136001
      y 2619.554151105054
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_259"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_225"
      name "S_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2295"
      uniprot "NA"
    ]
    graphics [
      x 1781.043215136001
      y 2912.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_225"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_56"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1067"
      uniprot "NA"
    ]
    graphics [
      x 2589.009256179148
      y 2839.2507314226996
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_248"
      name "sa2295_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2332"
      uniprot "NA"
    ]
    graphics [
      x 2350.501318138762
      y 2799.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_248"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      annotation "PUBMED:11142;PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_67"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1079"
      uniprot "NA"
    ]
    graphics [
      x 1308.7805189192002
      y 2153.3146446645715
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_175"
      name "Orf8_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2107"
      uniprot "NA"
    ]
    graphics [
      x 1098.7805189192002
      y 2323.184617392878
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_175"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC8;urn:miriam:ncbigene:43740577"
      hgnc "NA"
      map_id "M18_150"
      name "Orf8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1879"
      uniprot "UNIPROT:P0DTC8"
    ]
    graphics [
      x 2462.5
      y 1141.435024234829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_150"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1017"
      uniprot "NA"
    ]
    graphics [
      x 1562.5
      y 825.4971941397312
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC8;urn:miriam:ncbigene:43740577"
      hgnc "NA"
      map_id "M18_218"
      name "Orf8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2248"
      uniprot "UNIPROT:P0DTC8"
    ]
    graphics [
      x 2642.5
      y 1547.7682304796128
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_218"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_82"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1095"
      uniprot "NA"
    ]
    graphics [
      x 812.5
      y 970.6005374060633
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_263"
      name "Orf8_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2349"
      uniprot "NA"
    ]
    graphics [
      x 1832.5
      y 1501.5418469724498
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_263"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_229"
      name "Orf8_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2299"
      uniprot "NA"
    ]
    graphics [
      x 1202.5
      y 591.3264172216933
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_229"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_60"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1072"
      uniprot "NA"
    ]
    graphics [
      x 1712.5
      y 1287.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_252"
      name "sa2299_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2337"
      uniprot "NA"
    ]
    graphics [
      x 1781.043215136001
      y 2582.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_252"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_71"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1083"
      uniprot "NA"
    ]
    graphics [
      x 2852.5
      y 789.8252654416042
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_172"
      name "Orf3a_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2104"
      uniprot "NA"
    ]
    graphics [
      x 2522.5
      y 902.9715035857598
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_172"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC3;urn:miriam:ncbigene:43740569"
      hgnc "NA"
      map_id "M18_144"
      name "Orf3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1873"
      uniprot "UNIPROT:P0DTC3"
    ]
    graphics [
      x 3002.5
      y 1433.793921636545
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_144"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_35"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1016"
      uniprot "NA"
    ]
    graphics [
      x 1717.432708728739
      y 507.18556879462244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC3;urn:miriam:ncbigene:43740569"
      hgnc "NA"
      map_id "M18_217"
      name "Orf3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2247"
      uniprot "UNIPROT:P0DTC3"
    ]
    graphics [
      x 1112.5
      y 606.5935187823518
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_217"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_81"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1094"
      uniprot "NA"
    ]
    graphics [
      x 2167.5144558658467
      y 653.7133098641311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_262"
      name "Orf3a_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2348"
      uniprot "NA"
    ]
    graphics [
      x 2292.223682853007
      y 369.06618499081924
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_262"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_228"
      name "Orf3a_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2298"
      uniprot "NA"
    ]
    graphics [
      x 692.5
      y 1582.1232976122167
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_228"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_59"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1070"
      uniprot "NA"
    ]
    graphics [
      x 1472.5
      y 1336.4685701968617
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_251"
      name "sa2298_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2335"
      uniprot "NA"
    ]
    graphics [
      x 1352.5
      y 999.3114613664379
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_251"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_70"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1082"
      uniprot "NA"
    ]
    graphics [
      x 3302.5
      y 1837.8301897599697
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_176"
      name "Orf6_space__space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2108"
      uniprot "NA"
    ]
    graphics [
      x 1712.5
      y 777.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_176"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740572;urn:miriam:uniprot:P0DTC6"
      hgnc "NA"
      map_id "M18_145"
      name "Orf6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1874"
      uniprot "UNIPROT:P0DTC6"
    ]
    graphics [
      x 2105.815548054559
      y 665.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_145"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1015"
      uniprot "NA"
    ]
    graphics [
      x 692.5
      y 931.4686045727693
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740572;urn:miriam:uniprot:P0DTC6"
      hgnc "NA"
      map_id "M18_216"
      name "Orf6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2246"
      uniprot "UNIPROT:P0DTC6"
    ]
    graphics [
      x 1502.5
      y 1027.3397236403869
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_216"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_80"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1093"
      uniprot "NA"
    ]
    graphics [
      x 2282.5
      y 1446.211238590613
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_261"
      name "Orf6_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2347"
      uniprot "NA"
    ]
    graphics [
      x 1322.5
      y 1612.3432595389866
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_261"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_227"
      name "Orf6_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2297"
      uniprot "NA"
    ]
    graphics [
      x 1008.7805189192002
      y 2396.354750723753
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_227"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_58"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1069"
      uniprot "NA"
    ]
    graphics [
      x 602.5
      y 2079.295395563012
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_250"
      name "sa2297_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2334"
      uniprot "NA"
    ]
    graphics [
      x 618.7805189192002
      y 2513.038700694234
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_250"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_69"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1081"
      uniprot "NA"
    ]
    graphics [
      x 3002.5
      y 1297.4122316665537
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_177"
      name "Orf9b_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2109"
      uniprot "NA"
    ]
    graphics [
      x 842.5
      y 1118.434726735171
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_177"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTD2"
      hgnc "NA"
      map_id "M18_149"
      name "Orf9b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1878"
      uniprot "UNIPROT:P0DTD2"
    ]
    graphics [
      x 1061.043215136001
      y 2570.2805759893836
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_149"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_37"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1018"
      uniprot "NA"
    ]
    graphics [
      x 1811.043215136001
      y 2912.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTD2"
      hgnc "NA"
      map_id "M18_219"
      name "Orf9b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2249"
      uniprot "UNIPROT:P0DTD2"
    ]
    graphics [
      x 2042.5
      y 1228.5318911777056
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_219"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_83"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1096"
      uniprot "NA"
    ]
    graphics [
      x 482.5
      y 1899.665177890063
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_264"
      name "Orf9b_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2350"
      uniprot "NA"
    ]
    graphics [
      x 542.5
      y 980.5613607011237
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_264"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_230"
      name "Orf9b_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2300"
      uniprot "NA"
    ]
    graphics [
      x 1832.5
      y 722.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_230"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_61"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1073"
      uniprot "NA"
    ]
    graphics [
      x 2386.411591395815
      y 391.4350242348289
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_253"
      name "sa2300_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2338"
      uniprot "NA"
    ]
    graphics [
      x 3062.5
      y 825.8181106932224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_253"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_72"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1084"
      uniprot "NA"
    ]
    graphics [
      x 2162.5
      y 845.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_179"
      name "E_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2111"
      uniprot "NA"
    ]
    graphics [
      x 482.5
      y 2178.771903109522
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_179"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570;urn:miriam:hgnc.symbol:E;urn:miriam:uniprot:P0DTC4"
      hgnc "HGNC_SYMBOL:E"
      map_id "M18_137"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1687"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 1271.043215136001
      y 2570.4558091340696
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_137"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1012"
      uniprot "NA"
    ]
    graphics [
      x 2411.043215136001
      y 2696.0089009888093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570;urn:miriam:hgnc.symbol:E;urn:miriam:uniprot:P0DTC4"
      hgnc "HGNC_SYMBOL:E"
      map_id "M18_153"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1892"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 1771.4316445647612
      y 3002.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_153"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_28"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1009"
      uniprot "NA"
    ]
    graphics [
      x 858.7805189192002
      y 2415.956451910424
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570;urn:miriam:hgnc.symbol:E;urn:miriam:uniprot:P0DTC4"
      hgnc "HGNC_SYMBOL:E"
      map_id "M18_142"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1858"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 1172.5
      y 1797.3156502059937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_142"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_77"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1090"
      uniprot "NA"
    ]
    graphics [
      x 242.5
      y 1507.130969511257
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_258"
      name "E_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2344"
      uniprot "NA"
    ]
    graphics [
      x 1188.7805189192002
      y 2457.3156502059937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_258"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570"
      hgnc "NA"
      map_id "M18_224"
      name "E_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2294"
      uniprot "NA"
    ]
    graphics [
      x 1232.5
      y 1572.9973497539163
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_224"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_55"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1066"
      uniprot "NA"
    ]
    graphics [
      x 182.5
      y 1854.8599656103618
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_247"
      name "sa2294_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2331"
      uniprot "NA"
    ]
    graphics [
      x 738.7805189192002
      y 2358.126675774194
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_247"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      annotation "PUBMED:11142;PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_66"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1078"
      uniprot "NA"
    ]
    graphics [
      x 1892.5
      y 992.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_181"
      name "M_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2113"
      uniprot "NA"
    ]
    graphics [
      x 392.5
      y 1917.5827250944008
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_181"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "M18_136"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1686"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 2148.7805189192004
      y 2363.713309864131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_30"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1011"
      uniprot "NA"
    ]
    graphics [
      x 2282.5
      y 2059.2260305918107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "M18_152"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1891"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 2162.5
      y 1883.7133098641311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_152"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_27"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1008"
      uniprot "NA"
    ]
    graphics [
      x 1172.5
      y 2007.3156502059937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "M18_141"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1857"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 212.5
      y 1705.4623180844383
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_141"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_76"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1089"
      uniprot "NA"
    ]
    graphics [
      x 842.5
      y 971.0684958977382
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_257"
      name "M_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2343"
      uniprot "NA"
    ]
    graphics [
      x 962.5
      y 1507.6441692926126
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_257"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571"
      hgnc "NA"
      map_id "M18_223"
      name "M_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2292"
      uniprot "NA"
    ]
    graphics [
      x 1202.5
      y 792.0185613371316
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_223"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1064"
      uniprot "NA"
    ]
    graphics [
      x 2312.5
      y 789.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_246"
      name "sa2292_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2329"
      uniprot "NA"
    ]
    graphics [
      x 2767.4327087287393
      y 576.9197253703976
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_246"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_65"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1077"
      uniprot "NA"
    ]
    graphics [
      x 1262.5
      y 1896.9780694277015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_174"
      name "Orf7b_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2106"
      uniprot "NA"
    ]
    graphics [
      x 3212.5
      y 1746.3442194486067
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_174"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740574;urn:miriam:uniprot:P0DTD8"
      hgnc "NA"
      map_id "M18_147"
      name "Orf7b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1876"
      uniprot "UNIPROT:P0DTD8"
    ]
    graphics [
      x 2522.5
      y 962.9715035857598
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_147"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_39"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1020"
      uniprot "NA"
    ]
    graphics [
      x 1412.5
      y 832.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740574;urn:miriam:uniprot:P0DTD8"
      hgnc "NA"
      map_id "M18_221"
      name "Orf7b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2251"
      uniprot "UNIPROT:P0DTD8"
    ]
    graphics [
      x 782.5
      y 908.0998133070935
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_221"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_85"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1098"
      uniprot "NA"
    ]
    graphics [
      x 2042.5
      y 602.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_266"
      name "Orf7b_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2352"
      uniprot "NA"
    ]
    graphics [
      x 2377.4327087287393
      y 619.6609545171913
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_266"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_232"
      name "Orf7b_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2302"
      uniprot "NA"
    ]
    graphics [
      x 1172.5
      y 586.4696378562796
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_232"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_63"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1075"
      uniprot "NA"
    ]
    graphics [
      x 1082.5
      y 1696.3529417498703
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_255"
      name "sa2302_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2340"
      uniprot "NA"
    ]
    graphics [
      x 962.5
      y 2160.6216893968285
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_255"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_74"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1086"
      uniprot "NA"
    ]
    graphics [
      x 3242.5
      y 1343.410114080192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_173"
      name "Orf7a_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2105"
      uniprot "NA"
    ]
    graphics [
      x 1112.5
      y 1347.0116707084728
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_173"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740573;urn:miriam:uniprot:P0DTC7"
      hgnc "NA"
      map_id "M18_146"
      name "Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1875"
      uniprot "UNIPROT:P0DTC7"
    ]
    graphics [
      x 662.5
      y 2055.102589067452
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_146"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_33"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1014"
      uniprot "NA"
    ]
    graphics [
      x 992.5
      y 2050.5392529791284
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740573;urn:miriam:uniprot:P0DTC7"
      hgnc "NA"
      map_id "M18_215"
      name "Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2245"
      uniprot "UNIPROT:P0DTC7"
    ]
    graphics [
      x 1098.7805189192002
      y 2199.3853339635925
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_215"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_40"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1021"
      uniprot "NA"
    ]
    graphics [
      x 1532.5
      y 1769.738142841986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740573;urn:miriam:uniprot:P0DTC7"
      hgnc "NA"
      map_id "M18_158"
      name "Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1986"
      uniprot "UNIPROT:P0DTC7"
    ]
    graphics [
      x 1698.7805189192002
      y 2367.1855687946227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_158"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_79"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1092"
      uniprot "NA"
    ]
    graphics [
      x 2492.5
      y 1321.435024234829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_260"
      name "Orf7a_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2346"
      uniprot "NA"
    ]
    graphics [
      x 3152.5
      y 1252.6115175143973
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_260"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_226"
      name "Orf7a_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2296"
      uniprot "NA"
    ]
    graphics [
      x 1202.5
      y 700.1341956569238
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_226"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_57"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1068"
      uniprot "NA"
    ]
    graphics [
      x 2052.223682853007
      y 272.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_249"
      name "sa2296_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2333"
      uniprot "NA"
    ]
    graphics [
      x 2522.5
      y 1202.9715035857598
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_249"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_68"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1080"
      uniprot "NA"
    ]
    graphics [
      x 2492.5
      y 1561.435024234829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_62"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1074"
      uniprot "NA"
    ]
    graphics [
      x 2102.5
      y 1879.3787754406758
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_254"
      name "sa2301_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2339"
      uniprot "NA"
    ]
    graphics [
      x 752.5
      y 1656.138274694718
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_254"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_86"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1099"
      uniprot "NA"
    ]
    graphics [
      x 2552.5
      y 825.7005370980255
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_268"
      name "N_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2354"
      uniprot "NA"
    ]
    graphics [
      x 1938.7805189192002
      y 2282.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_268"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_87"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1100"
      uniprot "NA"
    ]
    graphics [
      x 872.5
      y 2131.839801025266
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_269"
      name "N_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2355"
      uniprot "NA"
    ]
    graphics [
      x 1532.5
      y 1169.738142841986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_269"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740575"
      hgnc "NA"
      map_id "M18_157"
      name "N_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa1962"
      uniprot "NA"
    ]
    graphics [
      x 1802.5
      y 1772.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_157"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_115"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re940"
      uniprot "NA"
    ]
    graphics [
      x 1472.5
      y 1216.4685701968617
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0070992"
      hgnc "NA"
      map_id "M18_15"
      name "Host_space_translation_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa429"
      uniprot "NA"
    ]
    graphics [
      x 2162.5
      y 875.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_135"
      name "N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1685"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 2402.5
      y 795.2178973508608
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      annotation "PUBMED:28720894"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_114"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re932"
      uniprot "NA"
    ]
    graphics [
      x 3182.5
      y 1092.533005889245
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_151"
      name "N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1887"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 3362.5
      y 1244.0913295120756
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_151"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 167
    zlevel -1

    cd19dm [
      annotation "PUBMED:28720894"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_52"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1061"
      uniprot "NA"
    ]
    graphics [
      x 2342.5
      y 1982.963502381953
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 168
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_11"
      name "Replication_space_transcription_space_complex:N_space_oligomer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa398"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 2298.7805189192004
      y 2229.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 169
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_102"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1120"
      uniprot "NA"
    ]
    graphics [
      x 3002.5
      y 1125.3708365422253
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 170
    zlevel -1

    cd19dm [
      annotation "PUBMED:28720894"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_119"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re972"
      uniprot "NA"
    ]
    graphics [
      x 2972.5
      y 2174.936290989684
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 171
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:refseq:NC_045512"
      hgnc "NA"
      map_id "M18_185"
      name "(_plus_)ss_space_gRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2153"
      uniprot "NA"
    ]
    graphics [
      x 2702.5
      y 1832.4301566673087
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_185"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 172
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_10"
      name "Nucleocapsid"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa397"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 1278.7805189192002
      y 2226.9780694277015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 173
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_20"
      name "Replication_space_transcription_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa440"
      uniprot "NA"
    ]
    graphics [
      x 2552.5
      y 2034.576720665756
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 174
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_101"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1119"
      uniprot "NA"
    ]
    graphics [
      x 2028.7805189192002
      y 2285.852697606676
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 175
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_116"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re948"
      uniprot "NA"
    ]
    graphics [
      x 2552.5
      y 1164.576720665756
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 176
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_9"
      name "Nucleocapsid"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa391"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 3332.5
      y 1206.2015552786877
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 177
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_112"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re895"
      uniprot "NA"
    ]
    graphics [
      x 2972.5
      y 1737.4318615488276
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 178
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_8"
      name "Nucleocapsid"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa389"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 2852.5
      y 1844.9316229072606
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 179
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_111"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re894"
      uniprot "NA"
    ]
    graphics [
      x 2928.7805189192004
      y 2242.6505321512923
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 180
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_7"
      name "Nucleocapsid"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa387"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 2245.842963598534
      y 2829.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 181
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_75"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1088"
      uniprot "NA"
    ]
    graphics [
      x 1142.5
      y 1554.9910422829782
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 182
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:refseq:NC_045512"
      hgnc "NA"
      map_id "M18_256"
      name "ds_space_gRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2342"
      uniprot "NA"
    ]
    graphics [
      x 1188.7805189192002
      y 2157.3156502059937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_256"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 183
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:refseq:NC_045512"
      hgnc "NA"
      map_id "M18_233"
      name "(_minus_)ss_space_gRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2314"
      uniprot "NA"
    ]
    graphics [
      x 1022.5
      y 727.804542056548
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_233"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 184
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_51"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1057"
      uniprot "NA"
    ]
    graphics [
      x 632.5
      y 1563.3058405199877
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 185
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_234"
      name "sa2314_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2315"
      uniprot "NA"
    ]
    graphics [
      x 1022.5
      y 1636.7111332684874
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_234"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 186
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_100"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1118"
      uniprot "NA"
    ]
    graphics [
      x 1832.5
      y 2012.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 187
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_282"
      name "sa2355_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2368"
      uniprot "NA"
    ]
    graphics [
      x 2942.5
      y 1582.6505321512923
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_282"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 188
    zlevel -1

    cd19dm [
      annotation "PUBMED:11907209;PUBMED:30632963"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_97"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1112"
      uniprot "NA"
    ]
    graphics [
      x 918.7805189192002
      y 2446.6950219408322
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 189
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725306"
      hgnc "NA"
      map_id "M18_192"
      name "Nsp10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2200"
      uniprot "NA"
    ]
    graphics [
      x 902.5
      y 1901.7973208312728
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_192"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 190
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725311"
      hgnc "NA"
      map_id "M18_191"
      name "Nsp16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2199"
      uniprot "NA"
    ]
    graphics [
      x 1945.5437299022901
      y 2822.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_191"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 191
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725310"
      hgnc "NA"
      map_id "M18_197"
      name "Nsp15"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2205"
      uniprot "NA"
    ]
    graphics [
      x 2688.7805189192004
      y 2384.5317366253753
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_197"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 192
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725309"
      hgnc "NA"
      map_id "M18_198"
      name "Nsp14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2206"
      uniprot "NA"
    ]
    graphics [
      x 2132.5
      y 1715.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_198"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 193
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725308"
      hgnc "NA"
      map_id "M18_190"
      name "Nsp13"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2198"
      uniprot "NA"
    ]
    graphics [
      x 1922.5
      y 1232.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_190"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 194
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725307"
      hgnc "NA"
      map_id "M18_189"
      name "Nsp12"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2197"
      uniprot "NA"
    ]
    graphics [
      x 1541.043215136001
      y 2549.738142841986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_189"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 195
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725305"
      hgnc "NA"
      map_id "M18_193"
      name "Nsp9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2201"
      uniprot "NA"
    ]
    graphics [
      x 1502.5
      y 1777.3397236403869
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_193"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 196
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725304"
      hgnc "NA"
      map_id "M18_194"
      name "Nsp8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2202"
      uniprot "NA"
    ]
    graphics [
      x 992.5
      y 1722.999485893885
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_194"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 197
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725303"
      hgnc "NA"
      map_id "M18_195"
      name "Nsp7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2203"
      uniprot "NA"
    ]
    graphics [
      x 2291.043215136001
      y 2499.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_195"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 198
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725302"
      hgnc "NA"
      map_id "M18_196"
      name "Nsp6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2204"
      uniprot "NA"
    ]
    graphics [
      x 2171.043215136001
      y 2603.713309864131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_196"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 199
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725299;urn:miriam:uniprot:Nsp3"
      hgnc "NA"
      map_id "M18_203"
      name "Nsp3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2222"
      uniprot "UNIPROT:Nsp3"
    ]
    graphics [
      x 1592.5
      y 1823.119912305107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_203"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 200
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725300"
      hgnc "NA"
      map_id "M18_205"
      name "Nsp4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2228"
      uniprot "NA"
    ]
    graphics [
      x 1698.7805189192002
      y 2517.1855687946227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_205"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 201
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725302"
      hgnc "NA"
      map_id "M18_281"
      name "Nsp6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2367"
      uniprot "NA"
    ]
    graphics [
      x 2642.5
      y 917.7682304796128
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_281"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 202
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725303"
      hgnc "NA"
      map_id "M18_277"
      name "Nsp7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2363"
      uniprot "NA"
    ]
    graphics [
      x 1841.043215136001
      y 2912.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_277"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 203
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725304"
      hgnc "NA"
      map_id "M18_276"
      name "Nsp8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2362"
      uniprot "NA"
    ]
    graphics [
      x 1292.5
      y 1493.3146446645712
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_276"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 204
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725305"
      hgnc "NA"
      map_id "M18_275"
      name "Nsp9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2361"
      uniprot "NA"
    ]
    graphics [
      x 842.5
      y 2050.6505922279507
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_275"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 205
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725306"
      hgnc "NA"
      map_id "M18_274"
      name "Nsp10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2360"
      uniprot "NA"
    ]
    graphics [
      x 678.7805189192002
      y 2305.659967359952
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_274"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 206
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725298"
      hgnc "NA"
      map_id "M18_200"
      name "Nsp2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2217"
      uniprot "NA"
    ]
    graphics [
      x 2372.5
      y 1612.0608348716244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_200"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 207
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725298"
      hgnc "NA"
      map_id "M18_271"
      name "Nsp2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2357"
      uniprot "NA"
    ]
    graphics [
      x 3152.5
      y 2034.3536941863422
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_271"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 208
    zlevel -1

    cd19dm [
      annotation "PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_89"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1104"
      uniprot "NA"
    ]
    graphics [
      x 3362.5
      y 1129.8199918847067
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 209
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTC1;urn:miriam:ec-code:3.4.22.-;urn:miriam:ncbigene:43740578"
      hgnc "NA"
      map_id "M18_139"
      name "pp1a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1789"
      uniprot "UNIPROT:P0DTC1"
    ]
    graphics [
      x 3272.5
      y 2172.9002822464467
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_139"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 210
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTC1;urn:miriam:ec-code:3.4.22.-;urn:miriam:ncbigene:43740578"
      hgnc "NA"
      map_id "M18_201"
      name "pp1a_space_Nsp3_minus_11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2220"
      uniprot "UNIPROT:P0DTC1"
    ]
    graphics [
      x 2497.4327087287393
      y 599.2524234976722
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_201"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 211
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725297"
      hgnc "NA"
      map_id "M18_270"
      name "Nsp1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2356"
      uniprot "NA"
    ]
    graphics [
      x 1832.5
      y 1712.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_270"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 212
    zlevel -1

    cd19dm [
      annotation "PUBMED:23035226"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_90"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re1105"
      uniprot "NA"
    ]
    graphics [
      x 1608.7805189192002
      y 2333.119912305107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 213
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0006412"
      hgnc "NA"
      map_id "M18_272"
      name "Host_space_translation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa2358"
      uniprot "NA"
    ]
    graphics [
      x 1181.043215136001
      y 2713.5263713449904
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_272"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 214
    zlevel -1

    cd19dm [
      annotation "PUBMED:21203998"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_23"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1001"
      uniprot "NA"
    ]
    graphics [
      x 1702.514108617874
      y 362.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 215
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M18_210"
      name "pp1a_space_Nsp3_minus_11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2240"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 722.5
      y 1068.582096328788
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_210"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 216
    zlevel -1

    cd19dm [
      annotation "PUBMED:21203998;PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_122"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re989"
      uniprot "NA"
    ]
    graphics [
      x 1638.7805189192002
      y 2157.1855687946227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 217
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTC1;urn:miriam:ec-code:3.4.22.-;urn:miriam:ncbigene:43740578"
      hgnc "NA"
      map_id "M18_204"
      name "pp1a_space_Nsp6_minus_11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2224"
      uniprot "UNIPROT:P0DTC1"
    ]
    graphics [
      x 2012.5
      y 1142.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_204"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 218
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725300;urn:miriam:ncbiprotein:YP_009725299"
      hgnc "NA"
      map_id "M18_273"
      name "Nsp3_minus_4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2359"
      uniprot "NA"
    ]
    graphics [
      x 1652.5
      y 1917.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_273"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 219
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725301"
      hgnc "NA"
      map_id "M18_280"
      name "Nsp5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2366"
      uniprot "NA"
    ]
    graphics [
      x 1502.5
      y 1747.3397236403869
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_280"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 220
    zlevel -1

    cd19dm [
      annotation "PUBMED:11907209"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_94"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1109"
      uniprot "NA"
    ]
    graphics [
      x 1622.5
      y 1402.5365071385852
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 221
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725312"
      hgnc "NA"
      map_id "M18_278"
      name "Nsp11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2364"
      uniprot "NA"
    ]
    graphics [
      x 1562.5
      y 915.4971941397312
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_278"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 222
    zlevel -1

    cd19dm [
      annotation "PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_91"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1106"
      uniprot "NA"
    ]
    graphics [
      x 2178.7805189192004
      y 2333.713309864131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 223
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725300;urn:miriam:ncbiprotein:YP_009725299"
      hgnc "NA"
      map_id "M18_279"
      name "Nsp3_minus_4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2365"
      uniprot "NA"
    ]
    graphics [
      x 2882.5
      y 1893.858034494286
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_279"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 224
    zlevel -1

    cd19dm [
      annotation "PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_92"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1107"
      uniprot "NA"
    ]
    graphics [
      x 1638.7805189192002
      y 2097.1855687946227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 225
    zlevel -1

    cd19dm [
      annotation "PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_88"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1102"
      uniprot "NA"
    ]
    graphics [
      x 1472.5
      y 826.4685701968617
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 226
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M18_140"
      name "pp1ab"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1790"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1082.5
      y 1096.3529417498703
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_140"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 227
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M18_202"
      name "pp1ab_space_Nsp3_minus_16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2221"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 2167.4327087287393
      y 383.7133098641309
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_202"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 228
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725297"
      hgnc "NA"
      map_id "M18_199"
      name "Nsp1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2216"
      uniprot "NA"
    ]
    graphics [
      x 1968.7805189192002
      y 2522.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_199"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 229
    zlevel -1

    cd19dm [
      annotation "PUBMED:23035226"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_22"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re1000"
      uniprot "NA"
    ]
    graphics [
      x 2642.5
      y 2147.7682304796126
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 230
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0006412"
      hgnc "NA"
      map_id "M18_207"
      name "Host_space_translation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa2237"
      uniprot "NA"
    ]
    graphics [
      x 1601.043215136001
      y 2663.119912305107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_207"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 231
    zlevel -1

    cd19dm [
      annotation "PUBMED:21203998"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_25"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1004"
      uniprot "NA"
    ]
    graphics [
      x 2456.3748126233327
      y 421.4350242348289
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 232
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M18_214"
      name "pp1ab_space_Nsp3_minus_16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2244"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 2882.5
      y 1352.7054530749897
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_214"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 233
    zlevel -1

    cd19dm [
      annotation "PUBMED:21203998;PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_26"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1006"
      uniprot "NA"
    ]
    graphics [
      x 2058.7805189192004
      y 2255.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 234
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M18_206"
      name "pp1ab_space_nsp6_minus_16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2229"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 3272.5
      y 1653.9230160479606
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_206"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 235
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725301"
      hgnc "NA"
      map_id "M18_211"
      name "Nsp5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2241"
      uniprot "NA"
    ]
    graphics [
      x 1518.7805189192002
      y 2219.738142841986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_211"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 236
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725300;urn:miriam:ncbiprotein:YP_009725299"
      hgnc "NA"
      map_id "M18_212"
      name "Nsp3_minus_4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2242"
      uniprot "NA"
    ]
    graphics [
      x 1322.5
      y 1552.3432595389866
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_212"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 237
    zlevel -1

    cd19dm [
      annotation "PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_24"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1002"
      uniprot "NA"
    ]
    graphics [
      x 2762.5
      y 2005.8267364055584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 238
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725300;urn:miriam:ncbiprotein:YP_009725299"
      hgnc "NA"
      map_id "M18_213"
      name "Nsp3_minus_4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2243"
      uniprot "NA"
    ]
    graphics [
      x 2582.5
      y 1849.6649132197108
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_213"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 239
    zlevel -1

    cd19dm [
      annotation "PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_93"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1108"
      uniprot "NA"
    ]
    graphics [
      x 2972.5
      y 1310.7746017483123
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 240
    zlevel -1

    cd19dm [
      annotation "PUBMED:11907209"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_95"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1110"
      uniprot "NA"
    ]
    graphics [
      x 2852.5
      y 1243.9317909822832
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 241
    zlevel -1

    cd19dm [
      annotation "PUBMED:23943763"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_123"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re999"
      uniprot "NA"
    ]
    graphics [
      x 2342.5
      y 1502.963502381953
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 242
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0005783"
      hgnc "NA"
      map_id "M18_19"
      name "Endoplasmic_space_reticulum"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa439"
      uniprot "NA"
    ]
    graphics [
      x 1262.5
      y 1446.9780694277015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 243
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0039718"
      hgnc "NA"
      map_id "M18_18"
      name "Double_minus_membrane_space_vesicle"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa438"
      uniprot "NA"
    ]
    graphics [
      x 2402.5
      y 1066.0046064486714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 244
    zlevel -1

    cd19dm [
      annotation "PUBMED:28484023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_96"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re1111"
      uniprot "NA"
    ]
    graphics [
      x 2024.292827536552
      y 2642.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 245
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0071360"
      hgnc "NA"
      map_id "M18_222"
      name "cellular_space_response_space_to_space_exogenous_space_dsRNA"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa2291"
      uniprot "NA"
    ]
    graphics [
      x 2162.5
      y 1853.7133098641311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_222"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 246
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0070992"
      hgnc "NA"
      map_id "M18_13"
      name "Host_space_translation_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa427"
      uniprot "NA"
    ]
    graphics [
      x 1442.5
      y 1685.1961954122032
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 247
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_6"
      name "Nucleocapsid"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa374"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 1022.5
      y 1207.8045420565481
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 248
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570;urn:miriam:hgnc.symbol:E;urn:miriam:uniprot:P0DTC4"
      hgnc "HGNC_SYMBOL:E"
      map_id "M18_160"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2023"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 2852.5
      y 1543.9317909822832
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_160"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 249
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "M18_161"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2024"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 2582.5
      y 1699.6649132197108
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_161"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 250
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740573;urn:miriam:uniprot:P0DTC7"
      hgnc "NA"
      map_id "M18_162"
      name "Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2025"
      uniprot "UNIPROT:P0DTC7"
    ]
    graphics [
      x 3422.5
      y 1323.6172558704602
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_162"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 251
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:interpro:IPR002552"
      hgnc "NA"
      map_id "M18_132"
      name "S2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1601"
      uniprot "NA"
    ]
    graphics [
      x 2628.7805189192004
      y 2567.7682304796126
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 252
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16113"
      hgnc "NA"
      map_id "M18_286"
      name "cholesterol"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa2372"
      uniprot "NA"
    ]
    graphics [
      x 3302.5
      y 2236.979820722916
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_286"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 253
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:interpro:IPR002551;urn:miriam:ncbigene:8829;urn:miriam:ncbigene:8829;urn:miriam:hgnc:8004;urn:miriam:uniprot:O14786;urn:miriam:uniprot:O14786;urn:miriam:ensembl:ENSG00000099250;urn:miriam:refseq:NM_001024628;urn:miriam:hgnc.symbol:NRP1;urn:miriam:hgnc.symbol:NRP1"
      hgnc "HGNC_SYMBOL:NRP1"
      map_id "M18_21"
      name "S1:NRP1_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa441"
      uniprot "UNIPROT:O14786"
    ]
    graphics [
      x 2462.5
      y 1201.435024234829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 254
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570;urn:miriam:hgnc.symbol:E;urn:miriam:uniprot:P0DTC4"
      hgnc "HGNC_SYMBOL:E"
      map_id "M18_169"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2065"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 2222.5
      y 1106.7158381002334
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_169"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 255
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "M18_170"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2066"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 3362.5
      y 1200.594585777549
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_170"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 256
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740573;urn:miriam:uniprot:P0DTC7"
      hgnc "NA"
      map_id "M18_171"
      name "Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2067"
      uniprot "UNIPROT:P0DTC7"
    ]
    graphics [
      x 2972.5
      y 2144.936290989684
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_171"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 257
    zlevel -1

    cd19dm [
      annotation "PUBMED:33082294;PUBMED:33082293"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_104"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1124"
      uniprot "NA"
    ]
    graphics [
      x 2102.5
      y 755.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 258
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:interpro:IPR002551"
      hgnc "NA"
      map_id "M18_129"
      name "S1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1539"
      uniprot "NA"
    ]
    graphics [
      x 3272.5
      y 1346.1844956582163
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_129"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 259
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:8829;urn:miriam:ncbigene:8829;urn:miriam:hgnc:8004;urn:miriam:uniprot:O14786;urn:miriam:uniprot:O14786;urn:miriam:ensembl:ENSG00000099250;urn:miriam:refseq:NM_001024628;urn:miriam:hgnc.symbol:NRP1;urn:miriam:hgnc.symbol:NRP1"
      hgnc "HGNC_SYMBOL:NRP1"
      map_id "M18_285"
      name "NRP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2371"
      uniprot "UNIPROT:O14786"
    ]
    graphics [
      x 902.5
      y 1219.451558880041
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_285"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 260
    zlevel -1

    cd19dm [
      annotation "PUBMED:32142651;PUBMED:32362314"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_117"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re950"
      uniprot "NA"
    ]
    graphics [
      x 3362.5
      y 2113.270850164884
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 261
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S;urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:S;HGNC_SYMBOL:ACE2"
      map_id "M18_4"
      name "ACE2:SPIKE_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa368"
      uniprot "UNIPROT:P0DTC2;UNIPROT:Q9BYF1"
    ]
    graphics [
      x 3122.5
      y 1871.471224340805
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 262
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:hgnc:8568;urn:miriam:ensembl:ENSG00000140564;urn:miriam:ec-code:3.4.21.75;urn:miriam:uniprot:P09958;urn:miriam:uniprot:P09958;urn:miriam:ncbigene:5045;urn:miriam:ncbigene:5045;urn:miriam:hgnc.symbol:FURIN;urn:miriam:hgnc.symbol:FURIN;urn:miriam:refseq:NM_002569"
      hgnc "HGNC_SYMBOL:FURIN"
      map_id "M18_156"
      name "FURIN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1921"
      uniprot "UNIPROT:P09958"
    ]
    graphics [
      x 3122.5
      y 1605.2087137406706
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_156"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 263
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:hgnc:11876;urn:miriam:hgnc.symbol:TMPRSS2;urn:miriam:uniprot:O15393;urn:miriam:uniprot:O15393;urn:miriam:hgnc.symbol:TMPRSS2;urn:miriam:ncbigene:7113;urn:miriam:ncbigene:7113;urn:miriam:ec-code:3.4.21.-;urn:miriam:ensembl:ENSG00000184012;urn:miriam:refseq:NM_001135099"
      hgnc "HGNC_SYMBOL:TMPRSS2"
      map_id "M18_128"
      name "TMPRSS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1537"
      uniprot "UNIPROT:O15393"
    ]
    graphics [
      x 1502.5
      y 1357.3397236403869
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 264
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:pubchem.compound:2536"
      hgnc "NA"
      map_id "M18_130"
      name "Camostat_space_mesylate"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1544"
      uniprot "NA"
    ]
    graphics [
      x 2351.043215136001
      y 2589.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_130"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 265
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:interpro:IPR002552"
      hgnc "NA"
      map_id "M18_168"
      name "S2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2063"
      uniprot "NA"
    ]
    graphics [
      x 3572.5
      y 1727.4927836444674
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_168"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 266
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:ACE2"
      map_id "M18_209"
      name "ACE2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2239"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 2492.5
      y 1651.435024234829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_209"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 267
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:ACE2"
      map_id "M18_208"
      name "ACE2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2238"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1712.5
      y 1827.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_208"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 268
    zlevel -1

    cd19dm [
      annotation "PUBMED:32047258"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_118"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re952"
      uniprot "NA"
    ]
    graphics [
      x 2958.7805189192004
      y 2409.433992765119
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 269
    zlevel -1

    cd19dm [
      annotation "PUBMED:32970989;PUBMED:32142651;PUBMED:32155444;PUBMED:32094589"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_108"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re852"
      uniprot "NA"
    ]
    graphics [
      x 1682.5
      y 1677.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 270
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "M18_159"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2009"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 2111.043215136001
      y 2585.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_159"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 271
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:ACE2"
      map_id "M18_131"
      name "ACE2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1545"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 722.5
      y 1441.2414382460581
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_131"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 272
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "M18_186"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2173"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 1562.5
      y 1995.4971941397312
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_186"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 273
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A28815"
      hgnc "NA"
      map_id "M18_284"
      name "Heparan_space_sulfate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa2370"
      uniprot "NA"
    ]
    graphics [
      x 2312.5
      y 639.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_284"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 274
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S;urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:S;HGNC_SYMBOL:ACE2"
      map_id "M18_17"
      name "ACE2:SPIKE_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa431"
      uniprot "UNIPROT:P0DTC2;UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1668.7805189192002
      y 2400.5573378046906
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 275
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ensembl:ENSG00000164733;urn:miriam:ncbigene:1508;urn:miriam:ncbigene:1508;urn:miriam:refseq:NM_147780;urn:miriam:uniprot:P07858;urn:miriam:uniprot:P07858;urn:miriam:hgnc:2527;urn:miriam:ec-code:3.4.22.1;urn:miriam:hgnc.symbol:CTSB;urn:miriam:hgnc.symbol:CTSB"
      hgnc "HGNC_SYMBOL:CTSB"
      map_id "M18_126"
      name "CTSB"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1524"
      uniprot "UNIPROT:P07858"
    ]
    graphics [
      x 1248.7805189192002
      y 2226.9780694277015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 276
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.22.15;urn:miriam:hgnc.symbol:CTSL;urn:miriam:ncbigene:1514;urn:miriam:hgnc.symbol:CTSL;urn:miriam:ncbigene:1514;urn:miriam:uniprot:P07711;urn:miriam:uniprot:P07711;urn:miriam:ensembl:ENSG00000135047;urn:miriam:refseq:NM_001912;urn:miriam:hgnc:2537"
      hgnc "HGNC_SYMBOL:CTSL"
      map_id "M18_127"
      name "CTSL"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1525"
      uniprot "UNIPROT:P07711"
    ]
    graphics [
      x 2358.7805189192004
      y 2452.060834871624
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_127"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 277
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:interpro:IPR002551"
      hgnc "NA"
      map_id "M18_125"
      name "S1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1516"
      uniprot "NA"
    ]
    graphics [
      x 1592.5
      y 773.1199123051069
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_125"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 278
    source 2
    target 1
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_107"
      target_id "M18_188"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 1
    target 3
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_188"
      target_id "M18_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 274
    target 2
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_17"
      target_id "M18_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 275
    target 2
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "M18_126"
      target_id "M18_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 276
    target 2
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "M18_127"
      target_id "M18_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 2
    target 277
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_107"
      target_id "M18_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 3
    target 4
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_121"
      target_id "M18_164"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 4
    target 5
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M18_164"
      target_id "M18_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 6
    target 5
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_3"
      target_id "M18_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 7
    target 5
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_167"
      target_id "M18_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 8
    target 5
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_165"
      target_id "M18_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 9
    target 5
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_166"
      target_id "M18_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 5
    target 10
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_120"
      target_id "M18_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 5
    target 11
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_120"
      target_id "M18_182"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 5
    target 12
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_120"
      target_id "M18_183"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 5
    target 13
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_120"
      target_id "M18_184"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 14
    target 10
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_109"
      target_id "M18_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 10
    target 15
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_5"
      target_id "M18_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 247
    target 14
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_6"
      target_id "M18_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 248
    target 14
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_160"
      target_id "M18_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 249
    target 14
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_161"
      target_id "M18_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 250
    target 14
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_162"
      target_id "M18_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 251
    target 14
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M18_132"
      target_id "M18_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 252
    target 14
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M18_286"
      target_id "M18_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 253
    target 14
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M18_21"
      target_id "M18_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 14
    target 254
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_109"
      target_id "M18_169"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 304
    source 14
    target 255
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_109"
      target_id "M18_170"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 305
    source 14
    target 256
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_109"
      target_id "M18_171"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 306
    source 15
    target 16
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_110"
      target_id "M18_134"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 307
    source 15
    target 17
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_110"
      target_id "M18_133"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 308
    source 16
    target 20
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_134"
      target_id "M18_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 309
    source 16
    target 21
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_134"
      target_id "M18_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 310
    source 16
    target 22
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_134"
      target_id "M18_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 311
    source 17
    target 18
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_133"
      target_id "M18_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 312
    source 18
    target 19
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_113"
      target_id "M18_155"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 313
    source 246
    target 20
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_13"
      target_id "M18_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 314
    source 20
    target 226
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_99"
      target_id "M18_140"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 315
    source 246
    target 21
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_13"
      target_id "M18_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 316
    source 21
    target 209
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_98"
      target_id "M18_139"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 317
    source 23
    target 22
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 318
    source 22
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_53"
      target_id "M18_245"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 319
    source 188
    target 23
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_97"
      target_id "M18_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 320
    source 174
    target 23
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_101"
      target_id "M18_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 321
    source 23
    target 108
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 322
    source 23
    target 169
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_12"
      target_id "M18_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 323
    source 23
    target 120
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 324
    source 23
    target 88
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 325
    source 23
    target 132
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 326
    source 23
    target 157
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 327
    source 23
    target 98
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 328
    source 23
    target 25
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 329
    source 23
    target 78
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 330
    source 23
    target 38
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 331
    source 23
    target 154
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 332
    source 23
    target 26
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 333
    source 23
    target 68
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 334
    source 23
    target 142
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 335
    source 24
    target 25
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_245"
      target_id "M18_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 336
    source 24
    target 26
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_245"
      target_id "M18_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 337
    source 25
    target 182
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_64"
      target_id "M18_256"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 338
    source 26
    target 27
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_235"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 339
    source 26
    target 28
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_267"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 340
    source 26
    target 29
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_244"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 341
    source 26
    target 30
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_243"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 342
    source 26
    target 31
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_242"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 343
    source 26
    target 32
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_241"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 344
    source 26
    target 33
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_240"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 345
    source 26
    target 34
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_239"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 346
    source 26
    target 35
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_238"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 347
    source 26
    target 36
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_237"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 348
    source 26
    target 37
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_236"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 349
    source 27
    target 142
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_235"
      target_id "M18_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 350
    source 28
    target 157
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_267"
      target_id "M18_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 351
    source 29
    target 132
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_244"
      target_id "M18_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 352
    source 30
    target 120
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_243"
      target_id "M18_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 353
    source 31
    target 68
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_242"
      target_id "M18_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 354
    source 32
    target 154
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_241"
      target_id "M18_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 355
    source 33
    target 98
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_240"
      target_id "M18_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 356
    source 34
    target 88
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_239"
      target_id "M18_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 357
    source 35
    target 78
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_238"
      target_id "M18_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 358
    source 36
    target 108
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_237"
      target_id "M18_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 359
    source 37
    target 38
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_236"
      target_id "M18_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 360
    source 38
    target 39
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_73"
      target_id "M18_265"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 361
    source 39
    target 40
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_265"
      target_id "M18_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 362
    source 40
    target 41
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_84"
      target_id "M18_231"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 363
    source 40
    target 42
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_84"
      target_id "M18_178"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 364
    source 41
    target 155
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_231"
      target_id "M18_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 365
    source 42
    target 43
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_178"
      target_id "M18_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 366
    source 44
    target 43
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 367
    source 43
    target 45
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_49"
      target_id "M18_148"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 368
    source 44
    target 48
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 369
    source 44
    target 49
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 370
    source 44
    target 50
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 371
    source 44
    target 51
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 372
    source 44
    target 52
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 373
    source 44
    target 53
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 374
    source 44
    target 54
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 375
    source 44
    target 55
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 376
    source 44
    target 56
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 377
    source 45
    target 46
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_148"
      target_id "M18_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 378
    source 46
    target 47
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_38"
      target_id "M18_220"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 379
    source 143
    target 48
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_173"
      target_id "M18_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 380
    source 48
    target 144
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_44"
      target_id "M18_146"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 381
    source 133
    target 49
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_174"
      target_id "M18_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 382
    source 49
    target 134
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_50"
      target_id "M18_147"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 383
    source 121
    target 50
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_181"
      target_id "M18_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 384
    source 50
    target 122
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_41"
      target_id "M18_136"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 385
    source 109
    target 51
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_179"
      target_id "M18_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 386
    source 51
    target 110
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_42"
      target_id "M18_137"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 387
    source 99
    target 52
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_177"
      target_id "M18_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 388
    source 52
    target 100
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_48"
      target_id "M18_149"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 389
    source 89
    target 53
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_176"
      target_id "M18_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 390
    source 53
    target 90
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_45"
      target_id "M18_145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 391
    source 79
    target 54
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_172"
      target_id "M18_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 392
    source 54
    target 80
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_46"
      target_id "M18_144"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 393
    source 69
    target 55
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_175"
      target_id "M18_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 394
    source 55
    target 70
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_47"
      target_id "M18_150"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 395
    source 57
    target 56
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_180"
      target_id "M18_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 396
    source 56
    target 58
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_43"
      target_id "M18_138"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 397
    source 63
    target 57
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_78"
      target_id "M18_180"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 398
    source 58
    target 59
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_138"
      target_id "M18_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 399
    source 59
    target 60
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_32"
      target_id "M18_154"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 400
    source 60
    target 61
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_154"
      target_id "M18_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 401
    source 61
    target 62
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_29"
      target_id "M18_143"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 402
    source 64
    target 63
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_259"
      target_id "M18_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 403
    source 63
    target 65
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_78"
      target_id "M18_225"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 404
    source 68
    target 64
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_67"
      target_id "M18_259"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 405
    source 65
    target 66
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_225"
      target_id "M18_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 406
    source 66
    target 67
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_56"
      target_id "M18_248"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 407
    source 73
    target 69
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_82"
      target_id "M18_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 408
    source 70
    target 71
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_150"
      target_id "M18_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 409
    source 71
    target 72
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_36"
      target_id "M18_218"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 410
    source 74
    target 73
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_263"
      target_id "M18_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 411
    source 73
    target 75
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_82"
      target_id "M18_229"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 412
    source 78
    target 74
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_71"
      target_id "M18_263"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 413
    source 75
    target 76
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_229"
      target_id "M18_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 414
    source 76
    target 77
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_60"
      target_id "M18_252"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 415
    source 83
    target 79
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_81"
      target_id "M18_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 416
    source 80
    target 81
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_144"
      target_id "M18_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 417
    source 81
    target 82
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_35"
      target_id "M18_217"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 418
    source 84
    target 83
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_262"
      target_id "M18_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 419
    source 83
    target 85
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_81"
      target_id "M18_228"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 420
    source 88
    target 84
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_70"
      target_id "M18_262"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 421
    source 85
    target 86
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_228"
      target_id "M18_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 422
    source 86
    target 87
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_59"
      target_id "M18_251"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 423
    source 93
    target 89
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_80"
      target_id "M18_176"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 424
    source 90
    target 91
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_145"
      target_id "M18_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 425
    source 91
    target 92
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_34"
      target_id "M18_216"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 426
    source 94
    target 93
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_261"
      target_id "M18_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 427
    source 93
    target 95
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_80"
      target_id "M18_227"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 428
    source 98
    target 94
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_69"
      target_id "M18_261"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 429
    source 95
    target 96
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_227"
      target_id "M18_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 430
    source 96
    target 97
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_58"
      target_id "M18_250"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 431
    source 103
    target 99
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_83"
      target_id "M18_177"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 432
    source 100
    target 101
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_149"
      target_id "M18_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 433
    source 101
    target 102
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_37"
      target_id "M18_219"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 434
    source 104
    target 103
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_264"
      target_id "M18_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 435
    source 103
    target 105
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_83"
      target_id "M18_230"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 436
    source 108
    target 104
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_72"
      target_id "M18_264"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 437
    source 105
    target 106
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_230"
      target_id "M18_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 438
    source 106
    target 107
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_61"
      target_id "M18_253"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 439
    source 115
    target 109
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_77"
      target_id "M18_179"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 440
    source 110
    target 111
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_137"
      target_id "M18_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 441
    source 111
    target 112
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_31"
      target_id "M18_153"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 442
    source 112
    target 113
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_153"
      target_id "M18_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 443
    source 113
    target 114
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_28"
      target_id "M18_142"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 444
    source 116
    target 115
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_258"
      target_id "M18_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 445
    source 115
    target 117
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_77"
      target_id "M18_224"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 446
    source 120
    target 116
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_66"
      target_id "M18_258"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 447
    source 117
    target 118
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_224"
      target_id "M18_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 448
    source 118
    target 119
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_55"
      target_id "M18_247"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 449
    source 127
    target 121
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_76"
      target_id "M18_181"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 450
    source 122
    target 123
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_136"
      target_id "M18_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 451
    source 123
    target 124
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_30"
      target_id "M18_152"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 452
    source 124
    target 125
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_152"
      target_id "M18_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 453
    source 125
    target 126
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_27"
      target_id "M18_141"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 454
    source 128
    target 127
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_257"
      target_id "M18_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 455
    source 127
    target 129
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_76"
      target_id "M18_223"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 456
    source 132
    target 128
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_65"
      target_id "M18_257"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 457
    source 129
    target 130
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_223"
      target_id "M18_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 458
    source 130
    target 131
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_54"
      target_id "M18_246"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 459
    source 137
    target 133
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_85"
      target_id "M18_174"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 460
    source 134
    target 135
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_147"
      target_id "M18_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 461
    source 135
    target 136
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_39"
      target_id "M18_221"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 462
    source 138
    target 137
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_266"
      target_id "M18_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 463
    source 137
    target 139
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_85"
      target_id "M18_232"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 464
    source 142
    target 138
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_74"
      target_id "M18_266"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 465
    source 139
    target 140
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_232"
      target_id "M18_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 466
    source 140
    target 141
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_63"
      target_id "M18_255"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 467
    source 149
    target 143
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_79"
      target_id "M18_173"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 468
    source 144
    target 145
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_146"
      target_id "M18_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 469
    source 145
    target 146
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_33"
      target_id "M18_215"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 470
    source 146
    target 147
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_215"
      target_id "M18_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 471
    source 147
    target 148
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_40"
      target_id "M18_158"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 472
    source 150
    target 149
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_260"
      target_id "M18_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 473
    source 149
    target 151
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_79"
      target_id "M18_226"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 474
    source 154
    target 150
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_68"
      target_id "M18_260"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 475
    source 151
    target 152
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_226"
      target_id "M18_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 476
    source 152
    target 153
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_57"
      target_id "M18_249"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 477
    source 155
    target 156
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_62"
      target_id "M18_254"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 478
    source 157
    target 158
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_86"
      target_id "M18_268"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 479
    source 158
    target 159
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_268"
      target_id "M18_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 480
    source 159
    target 160
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_87"
      target_id "M18_269"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 481
    source 159
    target 161
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_87"
      target_id "M18_157"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 482
    source 160
    target 186
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_269"
      target_id "M18_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 483
    source 161
    target 162
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_157"
      target_id "M18_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 484
    source 163
    target 162
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_15"
      target_id "M18_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 485
    source 162
    target 164
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_115"
      target_id "M18_135"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 486
    source 164
    target 165
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_135"
      target_id "M18_114"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 487
    source 165
    target 166
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_114"
      target_id "M18_151"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 488
    source 166
    target 167
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_151"
      target_id "M18_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 489
    source 167
    target 168
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_52"
      target_id "M18_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 490
    source 169
    target 168
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_102"
      target_id "M18_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 491
    source 168
    target 170
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_11"
      target_id "M18_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 492
    source 171
    target 170
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_185"
      target_id "M18_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 493
    source 170
    target 172
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_119"
      target_id "M18_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 494
    source 170
    target 173
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_119"
      target_id "M18_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 495
    source 181
    target 171
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_75"
      target_id "M18_185"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 496
    source 172
    target 175
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_10"
      target_id "M18_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 497
    source 173
    target 174
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_20"
      target_id "M18_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 498
    source 175
    target 176
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_116"
      target_id "M18_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 499
    source 176
    target 177
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_9"
      target_id "M18_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 500
    source 177
    target 178
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_112"
      target_id "M18_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 501
    source 178
    target 179
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_8"
      target_id "M18_111"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 502
    source 179
    target 180
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_111"
      target_id "M18_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 503
    source 182
    target 181
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_256"
      target_id "M18_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 504
    source 181
    target 183
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_75"
      target_id "M18_233"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 505
    source 183
    target 184
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_233"
      target_id "M18_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 506
    source 184
    target 185
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_51"
      target_id "M18_234"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 507
    source 186
    target 187
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_100"
      target_id "M18_282"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 508
    source 189
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_192"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 509
    source 190
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_191"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 510
    source 191
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_197"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 511
    source 192
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_198"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 512
    source 193
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_190"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 513
    source 194
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_189"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 514
    source 195
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_193"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 515
    source 196
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_194"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 516
    source 197
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_195"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 517
    source 198
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_196"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 518
    source 199
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_203"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 519
    source 200
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_205"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 520
    source 201
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_281"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 521
    source 202
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_277"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 522
    source 203
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_276"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 523
    source 204
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_275"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 524
    source 205
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_274"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 525
    source 206
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_200"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 526
    source 207
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_271"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 527
    source 240
    target 189
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_192"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 528
    source 240
    target 190
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_191"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 529
    source 240
    target 191
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_197"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 530
    source 191
    target 244
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_197"
      target_id "M18_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 531
    source 240
    target 192
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_198"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 532
    source 240
    target 193
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_190"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 533
    source 240
    target 194
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_189"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 534
    source 240
    target 195
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_193"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 535
    source 240
    target 196
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_194"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 536
    source 240
    target 197
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_195"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 537
    source 240
    target 198
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_196"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 538
    source 198
    target 241
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "MODULATION"
      source_id "M18_196"
      target_id "M18_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 539
    source 239
    target 199
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_93"
      target_id "M18_203"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 540
    source 224
    target 199
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_92"
      target_id "M18_203"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 541
    source 199
    target 241
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "MODULATION"
      source_id "M18_203"
      target_id "M18_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 542
    source 239
    target 200
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_93"
      target_id "M18_205"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 543
    source 224
    target 200
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_92"
      target_id "M18_205"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 544
    source 200
    target 241
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "MODULATION"
      source_id "M18_205"
      target_id "M18_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 545
    source 220
    target 201
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_94"
      target_id "M18_281"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 546
    source 201
    target 241
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "MODULATION"
      source_id "M18_281"
      target_id "M18_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 547
    source 220
    target 202
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_94"
      target_id "M18_277"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 548
    source 220
    target 203
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_94"
      target_id "M18_276"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 549
    source 220
    target 204
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_94"
      target_id "M18_275"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 550
    source 220
    target 205
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_94"
      target_id "M18_274"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 551
    source 225
    target 206
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_88"
      target_id "M18_200"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 552
    source 208
    target 207
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_89"
      target_id "M18_271"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 553
    source 209
    target 208
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_139"
      target_id "M18_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 554
    source 208
    target 210
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_89"
      target_id "M18_201"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 555
    source 208
    target 211
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_89"
      target_id "M18_270"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 556
    source 210
    target 214
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_201"
      target_id "M18_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 557
    source 211
    target 212
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_270"
      target_id "M18_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 558
    source 212
    target 213
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_90"
      target_id "M18_272"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 559
    source 214
    target 215
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_23"
      target_id "M18_210"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 560
    source 215
    target 216
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_210"
      target_id "M18_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 561
    source 216
    target 217
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_122"
      target_id "M18_204"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 562
    source 216
    target 218
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_122"
      target_id "M18_273"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 563
    source 216
    target 219
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_122"
      target_id "M18_280"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 564
    source 217
    target 220
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_204"
      target_id "M18_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 565
    source 218
    target 222
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_273"
      target_id "M18_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 566
    source 219
    target 220
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "M18_280"
      target_id "M18_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 567
    source 220
    target 221
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_94"
      target_id "M18_278"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 568
    source 222
    target 223
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_91"
      target_id "M18_279"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 569
    source 223
    target 224
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_279"
      target_id "M18_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 570
    source 226
    target 225
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_140"
      target_id "M18_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 571
    source 225
    target 227
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_88"
      target_id "M18_202"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 572
    source 225
    target 228
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_88"
      target_id "M18_199"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 573
    source 227
    target 231
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_202"
      target_id "M18_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 574
    source 228
    target 229
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_199"
      target_id "M18_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 575
    source 229
    target 230
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_22"
      target_id "M18_207"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 576
    source 231
    target 232
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_25"
      target_id "M18_214"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 577
    source 232
    target 233
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_214"
      target_id "M18_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 578
    source 233
    target 234
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_26"
      target_id "M18_206"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 579
    source 233
    target 235
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_26"
      target_id "M18_211"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 580
    source 233
    target 236
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_26"
      target_id "M18_212"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 581
    source 234
    target 240
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_206"
      target_id "M18_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 582
    source 235
    target 240
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "M18_211"
      target_id "M18_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 583
    source 236
    target 237
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_212"
      target_id "M18_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 584
    source 237
    target 238
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_24"
      target_id "M18_213"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 585
    source 238
    target 239
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_213"
      target_id "M18_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 586
    source 242
    target 241
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_19"
      target_id "M18_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 587
    source 241
    target 243
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_123"
      target_id "M18_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 588
    source 244
    target 245
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_96"
      target_id "M18_222"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 589
    source 268
    target 251
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_118"
      target_id "M18_132"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 590
    source 257
    target 253
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_104"
      target_id "M18_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 591
    source 258
    target 257
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_129"
      target_id "M18_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 592
    source 259
    target 257
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_285"
      target_id "M18_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 593
    source 260
    target 258
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_117"
      target_id "M18_129"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 594
    source 261
    target 260
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_4"
      target_id "M18_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 595
    source 262
    target 260
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "M18_156"
      target_id "M18_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 596
    source 263
    target 260
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "M18_128"
      target_id "M18_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 597
    source 264
    target 260
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "INHIBITION"
      source_id "M18_130"
      target_id "M18_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 598
    source 260
    target 265
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_117"
      target_id "M18_168"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 599
    source 260
    target 266
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_117"
      target_id "M18_209"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 600
    source 260
    target 267
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_117"
      target_id "M18_208"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 601
    source 269
    target 261
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_108"
      target_id "M18_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 602
    source 265
    target 268
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_168"
      target_id "M18_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 603
    source 270
    target 269
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_159"
      target_id "M18_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 604
    source 271
    target 269
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_131"
      target_id "M18_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 605
    source 272
    target 269
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_186"
      target_id "M18_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 606
    source 273
    target 269
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M18_284"
      target_id "M18_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
