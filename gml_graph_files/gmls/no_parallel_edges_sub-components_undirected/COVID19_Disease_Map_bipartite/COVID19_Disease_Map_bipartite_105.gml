# generated with VANTED V2.8.2 at Fri Mar 04 10:04:34 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_162"
      name "s1132"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa664"
      uniprot "NA"
    ]
    graphics [
      x 2261.043215136001
      y 2552.458314370092
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_162"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_42"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re152"
      uniprot "NA"
    ]
    graphics [
      x 2598.7805189192004
      y 2320.9512101709474
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ncbiprotein:AIA62288"
      hgnc "NA"
      map_id "M13_147"
      name "Orf9c"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa498"
      uniprot "NA"
    ]
    graphics [
      x 2252.5
      y 1622.4583143700918
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_147"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ncbigene:4715;urn:miriam:ncbigene:4715;urn:miriam:refseq:NM_005005;urn:miriam:ensembl:ENSG00000147684;urn:miriam:hgnc:7704;urn:miriam:hgnc.symbol:NDUFB9;urn:miriam:uniprot:Q9Y6M9;urn:miriam:uniprot:Q9Y6M9;urn:miriam:hgnc.symbol:NDUFB9"
      hgnc "HGNC_SYMBOL:NDUFB9"
      map_id "M13_160"
      name "NDUFB9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa661"
      uniprot "UNIPROT:Q9Y6M9"
    ]
    graphics [
      x 3272.5
      y 1563.477689119798
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_160"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385;PUBMED:30030361"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_29"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re105"
      uniprot "NA"
    ]
    graphics [
      x 1172.5
      y 1677.3156502059937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_159"
      name "s1127"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa659"
      uniprot "NA"
    ]
    graphics [
      x 1262.5
      y 1026.9780694277015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_159"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:refseq:NM_004541;urn:miriam:ensembl:ENSG00000125356;urn:miriam:uniprot:O15239;urn:miriam:uniprot:O15239;urn:miriam:hgnc.symbol:NDUFA1;urn:miriam:hgnc.symbol:NDUFA1;urn:miriam:ncbigene:4694;urn:miriam:ncbigene:4694;urn:miriam:hgnc:7683"
      hgnc "HGNC_SYMBOL:NDUFA1"
      map_id "M13_163"
      name "NDUFA1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa665"
      uniprot "UNIPROT:O15239"
    ]
    graphics [
      x 1622.5
      y 738.9254149382612
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_163"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:pubmed:23149385;urn:miriam:pubmed:30030361"
      hgnc "NA"
      map_id "M13_28"
      name "mtDNA_space_encoded_space_OXPHOS_space_units"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa98"
      uniprot "NA"
    ]
    graphics [
      x 1112.5
      y 1160.894430449393
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:pubmed:23149385;urn:miriam:pubmed:30030361"
      hgnc "NA"
      map_id "M13_7"
      name "OXPHOS_space_factors"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa44"
      uniprot "NA"
    ]
    graphics [
      x 2162.5
      y 815.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.go:GO%3A0045271;urn:miriam:doi:10.1016/j.bbabio.2011.08.010;urn:miriam:pubmed:19355884;urn:miriam:ensembl:ENSG00000023228;urn:miriam:refseq:NM_005006;urn:miriam:hgnc.symbol:NDUFS1;urn:miriam:ncbigene:4719;urn:miriam:hgnc.symbol:NDUFS1;urn:miriam:ncbigene:4719;urn:miriam:hgnc:7707;urn:miriam:uniprot:P28331;urn:miriam:uniprot:P28331;urn:miriam:ec-code:7.1.1.2;urn:miriam:ncbigene:4723;urn:miriam:ncbigene:4723;urn:miriam:uniprot:P49821;urn:miriam:uniprot:P49821;urn:miriam:refseq:NM_007103;urn:miriam:hgnc.symbol:NDUFV1;urn:miriam:hgnc.symbol:NDUFV1;urn:miriam:hgnc:7716;urn:miriam:ec-code:7.1.1.2;urn:miriam:ensembl:ENSG00000167792;urn:miriam:ncbigene:4537;urn:miriam:ncbigene:4537;urn:miriam:uniprot:P03897;urn:miriam:uniprot:P03897;urn:miriam:hgnc:7458;urn:miriam:refseq:YP_003024033;urn:miriam:hgnc.symbol:MT-ND3;urn:miriam:ensembl:ENSG00000198840;urn:miriam:hgnc.symbol:MT-ND3;urn:miriam:ec-code:7.1.1.2;urn:miriam:uniprot:P03923;urn:miriam:uniprot:P03923;urn:miriam:refseq:YP_003024037;urn:miriam:ncbigene:4541;urn:miriam:ncbigene:4541;urn:miriam:hgnc.symbol:MT-ND6;urn:miriam:hgnc.symbol:MT-ND6;urn:miriam:ec-code:7.1.1.2;urn:miriam:hgnc:7462;urn:miriam:ensembl:ENSG00000198695;urn:miriam:ensembl:ENSG00000178127;urn:miriam:refseq:NM_021074;urn:miriam:ncbigene:4729;urn:miriam:ncbigene:4729;urn:miriam:uniprot:P19404;urn:miriam:uniprot:P19404;urn:miriam:hgnc:7717;urn:miriam:hgnc.symbol:NDUFV2;urn:miriam:hgnc.symbol:NDUFV2;urn:miriam:ec-code:7.1.1.2;urn:miriam:hgnc.symbol:NDUFS8;urn:miriam:hgnc.symbol:NDUFS8;urn:miriam:ensembl:ENSG00000110717;urn:miriam:refseq:NM_002496;urn:miriam:ncbigene:4728;urn:miriam:ncbigene:4728;urn:miriam:uniprot:O00217;urn:miriam:uniprot:O00217;urn:miriam:hgnc:7715;urn:miriam:ec-code:7.1.1.2;urn:miriam:ncbigene:4539;urn:miriam:uniprot:P03901;urn:miriam:uniprot:P03901;urn:miriam:ncbigene:4539;urn:miriam:ensembl:ENSG00000212907;urn:miriam:hgnc.symbol:MT-ND4L;urn:miriam:hgnc.symbol:MT-ND4L;urn:miriam:hgnc:7460;urn:miriam:refseq:YP_003024034;urn:miriam:ec-code:7.1.1.2;urn:miriam:hgnc:7456;urn:miriam:ncbigene:4536;urn:miriam:ncbigene:4536;urn:miriam:refseq:YP_003024027;urn:miriam:uniprot:P03891;urn:miriam:uniprot:P03891;urn:miriam:ensembl:ENSG00000198763;urn:miriam:ec-code:7.1.1.2;urn:miriam:hgnc.symbol:MT-ND2;urn:miriam:hgnc.symbol:MT-ND2;urn:miriam:hgnc:7710;urn:miriam:uniprot:O75489;urn:miriam:uniprot:O75489;urn:miriam:ensembl:ENSG00000213619;urn:miriam:hgnc.symbol:NDUFS3;urn:miriam:refseq:NM_004551;urn:miriam:hgnc.symbol:NDUFS3;urn:miriam:ncbigene:4722;urn:miriam:ncbigene:4722;urn:miriam:ec-code:7.1.1.2;urn:miriam:refseq:YP_003024036;urn:miriam:ensembl:ENSG00000198786;urn:miriam:hgnc.symbol:MT-ND5;urn:miriam:ncbigene:4540;urn:miriam:hgnc.symbol:MT-ND5;urn:miriam:ncbigene:4540;urn:miriam:ec-code:7.1.1.2;urn:miriam:hgnc:7461;urn:miriam:uniprot:P03915;urn:miriam:uniprot:P03915;urn:miriam:ncbigene:4538;urn:miriam:ncbigene:4538;urn:miriam:hgnc:7459;urn:miriam:refseq:YP_003024035;urn:miriam:ensembl:ENSG00000198886;urn:miriam:hgnc.symbol:MT-ND4;urn:miriam:hgnc.symbol:MT-ND4;urn:miriam:uniprot:P03905;urn:miriam:uniprot:P03905;urn:miriam:ec-code:7.1.1.2;urn:miriam:ensembl:ENSG00000115286;urn:miriam:hgnc.symbol:NDUFS7;urn:miriam:hgnc.symbol:NDUFS7;urn:miriam:hgnc:7714;urn:miriam:uniprot:O75251;urn:miriam:uniprot:O75251;urn:miriam:ncbigene:374291;urn:miriam:ncbigene:374291;urn:miriam:ec-code:7.1.1.2;urn:miriam:refseq:NM_024407;urn:miriam:hgnc.symbol:NDUFS2;urn:miriam:ensembl:ENSG00000158864;urn:miriam:hgnc.symbol:NDUFS2;urn:miriam:refseq:NM_004550;urn:miriam:uniprot:O75306;urn:miriam:uniprot:O75306;urn:miriam:ec-code:7.1.1.2;urn:miriam:hgnc:7708;urn:miriam:ncbigene:4720;urn:miriam:ncbigene:4720;urn:miriam:ensembl:ENSG00000160194;urn:miriam:refseq:NM_001001503;urn:miriam:uniprot:P56181;urn:miriam:uniprot:P56181;urn:miriam:hgnc.symbol:NDUFV3;urn:miriam:hgnc.symbol:NDUFV3;urn:miriam:hgnc:7719;urn:miriam:ncbigene:4731;urn:miriam:ncbigene:4731;urn:miriam:hgnc:7455;urn:miriam:refseq:YP_003024026;urn:miriam:uniprot:P03886;urn:miriam:uniprot:P03886;urn:miriam:ensembl:ENSG00000198888;urn:miriam:ncbigene:4535;urn:miriam:ncbigene:4535;urn:miriam:ec-code:7.1.1.2;urn:miriam:hgnc.symbol:MT-ND1;urn:miriam:hgnc.symbol:MT-ND1"
      hgnc "HGNC_SYMBOL:NDUFS1;HGNC_SYMBOL:NDUFV1;HGNC_SYMBOL:MT-ND3;HGNC_SYMBOL:MT-ND6;HGNC_SYMBOL:NDUFV2;HGNC_SYMBOL:NDUFS8;HGNC_SYMBOL:MT-ND4L;HGNC_SYMBOL:MT-ND2;HGNC_SYMBOL:NDUFS3;HGNC_SYMBOL:MT-ND5;HGNC_SYMBOL:MT-ND4;HGNC_SYMBOL:NDUFS7;HGNC_SYMBOL:NDUFS2;HGNC_SYMBOL:NDUFV3;HGNC_SYMBOL:MT-ND1"
      map_id "M13_2"
      name "Complex_space_1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa20"
      uniprot "UNIPROT:P28331;UNIPROT:P49821;UNIPROT:P03897;UNIPROT:P03923;UNIPROT:P19404;UNIPROT:O00217;UNIPROT:P03901;UNIPROT:P03891;UNIPROT:O75489;UNIPROT:P03915;UNIPROT:P03905;UNIPROT:O75251;UNIPROT:O75306;UNIPROT:P56181;UNIPROT:P03886"
    ]
    graphics [
      x 782.5
      y 1477.835840060344
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "PUBMED:29464561"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_57"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re41"
      uniprot "NA"
    ]
    graphics [
      x 272.5
      y 1793.593315923612
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "PUBMED:25991374"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_56"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re40"
      uniprot "NA"
    ]
    graphics [
      x 1361.043215136001
      y 2625.7848127261113
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "PUBMED:18039652;PUBMED:26336579"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_78"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re78"
      uniprot "NA"
    ]
    graphics [
      x 2882.5
      y 947.330861409245
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "PUBMED:19355884"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re138"
      uniprot "NA"
    ]
    graphics [
      x 2148.7805189192004
      y 2213.713309864131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16908"
      hgnc "NA"
      map_id "M13_90"
      name "NADH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa2"
      uniprot "NA"
    ]
    graphics [
      x 1142.5
      y 2154.991042282978
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15846"
      hgnc "NA"
      map_id "M13_99"
      name "NAD_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa3"
      uniprot "NA"
    ]
    graphics [
      x 3212.5
      y 1422.7257342680077
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29235"
      hgnc "NA"
      map_id "M13_153"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa649"
      uniprot "NA"
    ]
    graphics [
      x 2432.5
      y 1106.0089009888093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_153"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_152"
      name "e_minus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa645"
      uniprot "NA"
    ]
    graphics [
      x 2282.5
      y 1969.6539311332745
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_152"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_37"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re139"
      uniprot "NA"
    ]
    graphics [
      x 2402.5
      y 2096.0089009888093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16389"
      hgnc "NA"
      map_id "M13_93"
      name "Q"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa219"
      uniprot "NA"
    ]
    graphics [
      x 1232.5
      y 2065.876634351126
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_154"
      name "e_minus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa650"
      uniprot "NA"
    ]
    graphics [
      x 872.5
      y 1484.8169333503001
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_154"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_38"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re140"
      uniprot "NA"
    ]
    graphics [
      x 2028.7805189192002
      y 2225.852697606676
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:uniprot:P99999;urn:miriam:ncbigene:54205;urn:miriam:hgnc.symbol:CYCS"
      hgnc "HGNC_SYMBOL:CYCS"
      map_id "M13_97"
      name "Cyt_space_C"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa253"
      uniprot "UNIPROT:P99999"
    ]
    graphics [
      x 2718.7805189192004
      y 2493.6109600044088
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_155"
      name "e_minus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa651"
      uniprot "NA"
    ]
    graphics [
      x 1751.043215136001
      y 2517.1855687946227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_155"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "PUBMED:25991374"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_83"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re9"
      uniprot "NA"
    ]
    graphics [
      x 3212.5
      y 1780.8355243840494
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15379"
      hgnc "NA"
      map_id "M13_96"
      name "O_underscore_sub_underscore_2_underscore_endsub_underscore_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa24"
      uniprot "NA"
    ]
    graphics [
      x 2522.5
      y 1952.9715035857598
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:uniprot:P99999;urn:miriam:ncbigene:54205;urn:miriam:hgnc.symbol:CYCS"
      hgnc "HGNC_SYMBOL:CYCS"
      map_id "M13_87"
      name "Cyt_space_C"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa13"
      uniprot "UNIPROT:P99999"
    ]
    graphics [
      x 2282.5
      y 1756.1202267557142
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29235"
      hgnc "NA"
      map_id "M13_89"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa19"
      uniprot "NA"
    ]
    graphics [
      x 2042.5
      y 1528.5318911777056
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17976"
      hgnc "NA"
      map_id "M13_98"
      name "QH_underscore_sub_underscore_2_underscore_endsub_underscore_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa255"
      uniprot "NA"
    ]
    graphics [
      x 2201.043215136001
      y 2603.713309864131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.go:GO%3A0045275;urn:miriam:ensembl:ENSG00000140740;urn:miriam:refseq:NM_003366;urn:miriam:uniprot:P22695;urn:miriam:uniprot:P22695;urn:miriam:hgnc.symbol:UQCRC2;urn:miriam:hgnc.symbol:UQCRC2;urn:miriam:ncbigene:7385;urn:miriam:ncbigene:7385;urn:miriam:hgnc:12586;urn:miriam:hgnc.symbol:UQCR11;urn:miriam:hgnc.symbol:UQCR11;urn:miriam:hgnc:30862;urn:miriam:ensembl:ENSG00000127540;urn:miriam:refseq:NM_006830;urn:miriam:ncbigene:10975;urn:miriam:ncbigene:10975;urn:miriam:uniprot:O14957;urn:miriam:uniprot:O14957;urn:miriam:refseq:NM_014402;urn:miriam:ncbigene:27089;urn:miriam:ncbigene:27089;urn:miriam:uniprot:O14949;urn:miriam:uniprot:O14949;urn:miriam:ensembl:ENSG00000164405;urn:miriam:hgnc:29594;urn:miriam:hgnc.symbol:UQCRQ;urn:miriam:hgnc.symbol:UQCRQ;urn:miriam:hgnc.symbol:UQCRC1;urn:miriam:hgnc.symbol:UQCRC1;urn:miriam:refseq:NM_003365;urn:miriam:uniprot:P31930;urn:miriam:uniprot:P31930;urn:miriam:ncbigene:7384;urn:miriam:ensembl:ENSG00000010256;urn:miriam:ncbigene:7384;urn:miriam:hgnc:12585;urn:miriam:hgnc.symbol:BCS1L;urn:miriam:hgnc.symbol:BCS1L;urn:miriam:hgnc:1020;urn:miriam:uniprot:Q9Y276;urn:miriam:uniprot:Q9Y276;urn:miriam:ensembl:ENSG00000074582;urn:miriam:ncbigene:617;urn:miriam:ncbigene:617;urn:miriam:refseq:NM_004328;urn:miriam:ncbigene:100128525;urn:miriam:hgnc:12588;urn:miriam:uniprot:P0C7P4;urn:miriam:uniprot:P0C7P4;urn:miriam:refseq:NG_009458;urn:miriam:ensembl:ENSG00000226085;urn:miriam:hgnc.symbol:UQCRFS1P1;urn:miriam:hgnc.symbol:UQCRFS1P1;urn:miriam:ncbigene:7381;urn:miriam:ncbigene:7381;urn:miriam:ensembl:ENSG00000156467;urn:miriam:hgnc.symbol:UQCRB;urn:miriam:hgnc.symbol:UQCRB;urn:miriam:refseq:NM_006294;urn:miriam:uniprot:P14927;urn:miriam:uniprot:P14927;urn:miriam:hgnc:12582;urn:miriam:hgnc:12590;urn:miriam:uniprot:P07919;urn:miriam:uniprot:P07919;urn:miriam:refseq:NM_006004;urn:miriam:hgnc.symbol:UQCRH;urn:miriam:hgnc.symbol:UQCRH;urn:miriam:ncbigene:7388;urn:miriam:ncbigene:7388;urn:miriam:ensembl:ENSG00000173660;urn:miriam:refseq:NM_013387;urn:miriam:hgnc.symbol:UQCR10;urn:miriam:hgnc.symbol:UQCR10;urn:miriam:hgnc:30863;urn:miriam:ncbigene:29796;urn:miriam:ncbigene:29796;urn:miriam:uniprot:Q9UDW1;urn:miriam:uniprot:Q9UDW1;urn:miriam:ensembl:ENSG00000184076"
      hgnc "HGNC_SYMBOL:UQCRC2;HGNC_SYMBOL:UQCR11;HGNC_SYMBOL:UQCRQ;HGNC_SYMBOL:UQCRC1;HGNC_SYMBOL:BCS1L;HGNC_SYMBOL:UQCRFS1P1;HGNC_SYMBOL:UQCRB;HGNC_SYMBOL:UQCRH;HGNC_SYMBOL:UQCR10"
      map_id "M13_5"
      name "complex_space_3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa34"
      uniprot "UNIPROT:P22695;UNIPROT:O14957;UNIPROT:O14949;UNIPROT:P31930;UNIPROT:Q9Y276;UNIPROT:P0C7P4;UNIPROT:P14927;UNIPROT:P07919;UNIPROT:Q9UDW1"
    ]
    graphics [
      x 2942.5
      y 837.6825784648988
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29356"
      hgnc "NA"
      map_id "M13_95"
      name "O_underscore_super_underscore_2_minus__underscore_endsuper_underscore_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa23"
      uniprot "NA"
    ]
    graphics [
      x 2162.5
      y 1973.7133098641311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16389"
      hgnc "NA"
      map_id "M13_150"
      name "Q"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa6"
      uniprot "NA"
    ]
    graphics [
      x 1982.5
      y 1832.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_150"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29235"
      hgnc "NA"
      map_id "M13_91"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa20"
      uniprot "NA"
    ]
    graphics [
      x 2612.5
      y 1988.4291303703953
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_31"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re132"
      uniprot "NA"
    ]
    graphics [
      x 3182.5
      y 1152.533005889245
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.go:GO%3A0005753;urn:miriam:hgnc.symbol:MT-ATP6;urn:miriam:hgnc.symbol:MT-ATP6;urn:miriam:hgnc:7414;urn:miriam:ncbigene:4508;urn:miriam:ncbigene:4508;urn:miriam:ensembl:ENSG00000198899;urn:miriam:refseq:YP_003024031;urn:miriam:uniprot:P00846;urn:miriam:uniprot:P00846;urn:miriam:hgnc.symbol:ATP5IF1;urn:miriam:hgnc.symbol:ATP5IF1;urn:miriam:ensembl:ENSG00000130770;urn:miriam:ncbigene:93974;urn:miriam:ncbigene:93974;urn:miriam:refseq:NM_016311;urn:miriam:hgnc:871;urn:miriam:uniprot:Q9UII2;urn:miriam:uniprot:Q9UII2"
      hgnc "HGNC_SYMBOL:MT-ATP6;HGNC_SYMBOL:ATP5IF1"
      map_id "M13_8"
      name "ATP_space_Synthase"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa47"
      uniprot "UNIPROT:P00846;UNIPROT:Q9UII2"
    ]
    graphics [
      x 1922.5
      y 632.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_53"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re164"
      uniprot "NA"
    ]
    graphics [
      x 2852.5
      y 1783.9317909822832
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_35"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re136"
      uniprot "NA"
    ]
    graphics [
      x 812.5
      y 1150.6005374060633
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "PUBMED:31115493"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_34"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re135"
      uniprot "NA"
    ]
    graphics [
      x 3212.5
      y 1694.452852247842
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "PUBMED:25991374"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_49"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re16"
      uniprot "NA"
    ]
    graphics [
      x 2261.043215136001
      y 2702.458314370092
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_32"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re133"
      uniprot "NA"
    ]
    graphics [
      x 1832.5
      y 2072.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29235"
      hgnc "NA"
      map_id "M13_103"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa335"
      uniprot "NA"
    ]
    graphics [
      x 2081.043215136001
      y 2675.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16761"
      hgnc "NA"
      map_id "M13_100"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa30"
      uniprot "NA"
    ]
    graphics [
      x 1742.5
      y 1641.30614738341
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "M13_101"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa32"
      uniprot "NA"
    ]
    graphics [
      x 2261.043215136001
      y 2492.458314370092
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15422"
      hgnc "NA"
      map_id "M13_102"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa33"
      uniprot "NA"
    ]
    graphics [
      x 3092.5
      y 2200.1860833815003
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29235"
      hgnc "NA"
      map_id "M13_151"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa644"
      uniprot "NA"
    ]
    graphics [
      x 2718.7805189192004
      y 2192.4310147534843
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_151"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      annotation "PUBMED:31115493"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_33"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re134"
      uniprot "NA"
    ]
    graphics [
      x 2432.5
      y 2096.0089009888093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29235"
      hgnc "NA"
      map_id "M13_88"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa18"
      uniprot "NA"
    ]
    graphics [
      x 1202.5
      y 1317.3156502059937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_180"
      name "s1190"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa712"
      uniprot "NA"
    ]
    graphics [
      x 3062.5
      y 1403.942057954809
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_180"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:hgnc:14247;urn:miriam:refseq:NM_006476;urn:miriam:hgnc.symbol:ATP5MG;urn:miriam:hgnc.symbol:ATP5MG;urn:miriam:ensembl:ENSG00000167283;urn:miriam:ncbigene:10632;urn:miriam:ncbigene:10632;urn:miriam:uniprot:O75964;urn:miriam:uniprot:O75964"
      hgnc "HGNC_SYMBOL:ATP5MG"
      map_id "M13_179"
      name "ATP5MG"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa710"
      uniprot "UNIPROT:O75964"
    ]
    graphics [
      x 1982.5
      y 1592.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_179"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ncbiprotein:YP_009742613"
      hgnc "NA"
      map_id "M13_181"
      name "Nsp6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa713"
      uniprot "NA"
    ]
    graphics [
      x 1652.5
      y 927.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_181"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385;PUBMED:30030361"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_58"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re43"
      uniprot "NA"
    ]
    graphics [
      x 1770.3026255485454
      y 242.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_184"
      name "s1196"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa716"
      uniprot "NA"
    ]
    graphics [
      x 2102.5
      y 1477.1941722470388
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_184"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:pubmed:23149385;urn:miriam:pubmed:30030361"
      hgnc "NA"
      map_id "M13_1"
      name "mtDNA_space_encoded_space_OXPHOS_space_units"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa103"
      uniprot "NA"
    ]
    graphics [
      x 302.5
      y 1304.2652768547302
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385;PUBMED:30030361"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_59"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re44"
      uniprot "NA"
    ]
    graphics [
      x 1690.9135463862924
      y 837.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_185"
      name "s1197"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa717"
      uniprot "NA"
    ]
    graphics [
      x 2102.5
      y 875.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_185"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.go:GO%3A0045277;urn:miriam:uniprot:P00414;urn:miriam:uniprot:P00414;urn:miriam:ncbigene:4514;urn:miriam:ncbigene:4514;urn:miriam:hgnc:7422;urn:miriam:hgnc.symbol:MT-CO3;urn:miriam:ensembl:ENSG00000198938;urn:miriam:hgnc.symbol:MT-CO3;urn:miriam:ec-code:7.1.1.9;urn:miriam:refseq:YP_003024032;urn:miriam:uniprot:P10176;urn:miriam:uniprot:P10176;urn:miriam:ncbigene:1351;urn:miriam:ncbigene:1351;urn:miriam:refseq:NM_004074;urn:miriam:hgnc.symbol:COX8A;urn:miriam:hgnc.symbol:COX8A;urn:miriam:hgnc:2294;urn:miriam:ensembl:ENSG00000176340;urn:miriam:uniprot:P00403;urn:miriam:uniprot:P00403;urn:miriam:refseq:YP_003024029;urn:miriam:hgnc:7421;urn:miriam:hgnc.symbol:MT-CO2;urn:miriam:hgnc.symbol:MT-CO2;urn:miriam:ec-code:7.1.1.9;urn:miriam:ensembl:ENSG00000198712;urn:miriam:ncbigene:4513;urn:miriam:ncbigene:4513;urn:miriam:ncbigene:1337;urn:miriam:ncbigene:1337;urn:miriam:hgnc:2277;urn:miriam:hgnc.symbol:COX6A1;urn:miriam:refseq:NM_004373;urn:miriam:ensembl:ENSG00000111775;urn:miriam:hgnc.symbol:COX6A1;urn:miriam:uniprot:P12074;urn:miriam:uniprot:P12074;urn:miriam:uniprot:P14854;urn:miriam:uniprot:P14854;urn:miriam:ensembl:ENSG00000126267;urn:miriam:hgnc.symbol:COX6B1;urn:miriam:hgnc.symbol:COX6B1;urn:miriam:ncbigene:1340;urn:miriam:ncbigene:1340;urn:miriam:hgnc:2280;urn:miriam:refseq:NM_001863;urn:miriam:refseq:NM_001862;urn:miriam:ncbigene:1329;urn:miriam:ncbigene:1329;urn:miriam:hgnc:2269;urn:miriam:hgnc.symbol:COX5B;urn:miriam:uniprot:P10606;urn:miriam:uniprot:P10606;urn:miriam:hgnc.symbol:COX5B;urn:miriam:ensembl:ENSG00000135940;urn:miriam:uniprot:P15954;urn:miriam:uniprot:P15954;urn:miriam:ncbigene:1350;urn:miriam:refseq:NM_001867;urn:miriam:ncbigene:1350;urn:miriam:hgnc.symbol:COX7C;urn:miriam:hgnc.symbol:COX7C;urn:miriam:ensembl:ENSG00000127184;urn:miriam:hgnc:2292;urn:miriam:refseq:NM_005205;urn:miriam:uniprot:Q02221;urn:miriam:uniprot:Q02221;urn:miriam:ncbigene:1339;urn:miriam:ncbigene:1339;urn:miriam:hgnc:2279;urn:miriam:ensembl:ENSG00000156885;urn:miriam:hgnc.symbol:COX6A2;urn:miriam:hgnc.symbol:COX6A2;urn:miriam:ncbigene:1349;urn:miriam:ncbigene:1349;urn:miriam:uniprot:P24311;urn:miriam:uniprot:P24311;urn:miriam:ensembl:ENSG00000131174;urn:miriam:hgnc.symbol:COX7B;urn:miriam:hgnc.symbol:COX7B;urn:miriam:hgnc:2291;urn:miriam:refseq:NM_001866;urn:miriam:hgnc:24381;urn:miriam:ensembl:ENSG00000170516;urn:miriam:refseq:NM_130902;urn:miriam:hgnc.symbol:COX7B2;urn:miriam:hgnc.symbol:COX7B2;urn:miriam:ncbigene:170712;urn:miriam:ncbigene:170712;urn:miriam:uniprot:Q8TF08;urn:miriam:uniprot:Q8TF08;urn:miriam:uniprot:Q6YFQ2;urn:miriam:uniprot:Q6YFQ2;urn:miriam:ensembl:ENSG00000160471;urn:miriam:hgnc:24380;urn:miriam:refseq:NM_144613;urn:miriam:hgnc.symbol:COX6B2;urn:miriam:hgnc.symbol:COX6B2;urn:miriam:ncbigene:125965;urn:miriam:ncbigene:125965;urn:miriam:hgnc:2265;urn:miriam:ncbigene:1327;urn:miriam:refseq:NM_001861;urn:miriam:ncbigene:1327;urn:miriam:ensembl:ENSG00000131143;urn:miriam:hgnc.symbol:COX4I1;urn:miriam:hgnc.symbol:COX4I1;urn:miriam:uniprot:P13073;urn:miriam:uniprot:P13073;urn:miriam:ncbigene:1346;urn:miriam:ncbigene:1346;urn:miriam:hgnc:2287;urn:miriam:hgnc.symbol:COX7A1;urn:miriam:hgnc.symbol:COX7A1;urn:miriam:uniprot:P24310;urn:miriam:uniprot:P24310;urn:miriam:refseq:NM_001864;urn:miriam:ensembl:ENSG00000161281;urn:miriam:uniprot:P14406;urn:miriam:uniprot:P14406;urn:miriam:ncbigene:1347;urn:miriam:ncbigene:1347;urn:miriam:hgnc:2288;urn:miriam:ensembl:ENSG00000112695;urn:miriam:hgnc.symbol:COX7A2;urn:miriam:hgnc.symbol:COX7A2;urn:miriam:refseq:NM_001865;urn:miriam:ensembl:ENSG00000178741;urn:miriam:ncbigene:9377;urn:miriam:ncbigene:9377;urn:miriam:refseq:NM_004255;urn:miriam:hgnc:2267;urn:miriam:uniprot:P20674;urn:miriam:uniprot:P20674;urn:miriam:hgnc.symbol:COX5A;urn:miriam:hgnc.symbol:COX5A;urn:miriam:hgnc.symbol:COX4I2;urn:miriam:hgnc.symbol:COX4I2;urn:miriam:hgnc:16232;urn:miriam:ensembl:ENSG00000131055;urn:miriam:uniprot:Q96KJ9;urn:miriam:uniprot:Q96KJ9;urn:miriam:ncbigene:84701;urn:miriam:ncbigene:84701;urn:miriam:refseq:NM_032609;urn:miriam:hgnc:24382;urn:miriam:ensembl:ENSG00000187581;urn:miriam:uniprot:Q7Z4L0;urn:miriam:uniprot:Q7Z4L0;urn:miriam:refseq:NM_182971;urn:miriam:ncbigene:341947;urn:miriam:ncbigene:341947;urn:miriam:hgnc.symbol:COX8C;urn:miriam:hgnc.symbol:COX8C;urn:miriam:hgnc:2285;urn:miriam:ensembl:ENSG00000164919;urn:miriam:uniprot:P09669;urn:miriam:uniprot:P09669;urn:miriam:refseq:NM_004374;urn:miriam:hgnc.symbol:COX6C;urn:miriam:hgnc.symbol:COX6C;urn:miriam:ncbigene:1345;urn:miriam:ncbigene:1345;urn:miriam:ec-code:7.1.1.9;urn:miriam:hgnc.symbol:MT-CO1;urn:miriam:ensembl:ENSG00000198804;urn:miriam:hgnc.symbol:MT-CO1;urn:miriam:refseq:YP_003024028;urn:miriam:uniprot:P00395;urn:miriam:uniprot:P00395;urn:miriam:hgnc:7419;urn:miriam:ncbigene:4512;urn:miriam:ncbigene:4512"
      hgnc "HGNC_SYMBOL:MT-CO3;HGNC_SYMBOL:COX8A;HGNC_SYMBOL:MT-CO2;HGNC_SYMBOL:COX6A1;HGNC_SYMBOL:COX6B1;HGNC_SYMBOL:COX5B;HGNC_SYMBOL:COX7C;HGNC_SYMBOL:COX6A2;HGNC_SYMBOL:COX7B;HGNC_SYMBOL:COX7B2;HGNC_SYMBOL:COX6B2;HGNC_SYMBOL:COX4I1;HGNC_SYMBOL:COX7A1;HGNC_SYMBOL:COX7A2;HGNC_SYMBOL:COX5A;HGNC_SYMBOL:COX4I2;HGNC_SYMBOL:COX8C;HGNC_SYMBOL:COX6C;HGNC_SYMBOL:MT-CO1"
      map_id "M13_6"
      name "complex_space_4"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa35"
      uniprot "UNIPROT:P00414;UNIPROT:P10176;UNIPROT:P00403;UNIPROT:P12074;UNIPROT:P14854;UNIPROT:P10606;UNIPROT:P15954;UNIPROT:Q02221;UNIPROT:P24311;UNIPROT:Q8TF08;UNIPROT:Q6YFQ2;UNIPROT:P13073;UNIPROT:P24310;UNIPROT:P14406;UNIPROT:P20674;UNIPROT:Q96KJ9;UNIPROT:Q7Z4L0;UNIPROT:P09669;UNIPROT:P00395"
    ]
    graphics [
      x 962.5
      y 846.2731710667011
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A34905;urn:miriam:obo.chebi:CHEBI%3A29235"
      hgnc "NA"
      map_id "M13_9"
      name "paraquat_space_dication"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa50"
      uniprot "NA"
    ]
    graphics [
      x 2132.5
      y 755.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18421;urn:miriam:obo.chebi:CHEBI%3A1842"
      hgnc "NA"
      map_id "M13_110"
      name "superoxide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa360"
      uniprot "NA"
    ]
    graphics [
      x 2612.5
      y 1359.9502320886959
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A34905"
      hgnc "NA"
      map_id "M13_141"
      name "paraquat"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa397"
      uniprot "NA"
    ]
    graphics [
      x 1772.5
      y 1832.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_141"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "PUBMED:26336579"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_77"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re77"
      uniprot "NA"
    ]
    graphics [
      x 602.5
      y 1689.295395563012
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15379"
      hgnc "NA"
      map_id "M13_111"
      name "O_underscore_sub_underscore_2_underscore_endsub_underscore_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa361"
      uniprot "NA"
    ]
    graphics [
      x 1592.5
      y 1973.119912305107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_65"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re64"
      uniprot "NA"
    ]
    graphics [
      x 2312.5
      y 1479.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_64"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re63"
      uniprot "NA"
    ]
    graphics [
      x 1777.432708728739
      y 482.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_66"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re65"
      uniprot "NA"
    ]
    graphics [
      x 3212.5
      y 2013.3500826904597
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A26523"
      hgnc "NA"
      map_id "M13_112"
      name "ROS"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa362"
      uniprot "NA"
    ]
    graphics [
      x 1668.7805189192002
      y 2337.1855687946227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_69"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re69"
      uniprot "NA"
    ]
    graphics [
      x 1712.5
      y 1317.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_67"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re66"
      uniprot "NA"
    ]
    graphics [
      x 2882.5
      y 1409.4229994287773
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_71"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re71"
      uniprot "NA"
    ]
    graphics [
      x 1548.7805189192002
      y 2339.738142841986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16240"
      hgnc "NA"
      map_id "M13_105"
      name "H_underscore_sub_underscore_2_underscore_endsub_underscore_O_underscore_sub_underscore_2_underscore_endsub_underscore_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa355"
      uniprot "NA"
    ]
    graphics [
      x 932.5
      y 1591.4892846436483
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_60"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re56"
      uniprot "NA"
    ]
    graphics [
      x 1428.7805189192002
      y 2272.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_74"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re74"
      uniprot "NA"
    ]
    graphics [
      x 1562.5
      y 1155.4971941397312
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_61"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re58"
      uniprot "NA"
    ]
    graphics [
      x 2402.5
      y 1216.0046064486714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_62"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re60"
      uniprot "NA"
    ]
    graphics [
      x 2852.5
      y 849.8252654416042
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_68"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re67"
      uniprot "NA"
    ]
    graphics [
      x 2132.5
      y 1025.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "PUBMED:29464561"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_30"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re120"
      uniprot "NA"
    ]
    graphics [
      x 512.5
      y 1420.3067138906858
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:refseq:NM_001752;urn:miriam:ncbigene:847;urn:miriam:ncbigene:847;urn:miriam:ec-code:1.11.1.6;urn:miriam:ensembl:ENSG00000121691;urn:miriam:hgnc.symbol:CAT;urn:miriam:hgnc.symbol:CAT;urn:miriam:hgnc:1516;urn:miriam:uniprot:P04040"
      hgnc "HGNC_SYMBOL:CAT"
      map_id "M13_135"
      name "CAT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa385"
      uniprot "UNIPROT:P04040"
    ]
    graphics [
      x 932.5
      y 1681.4892846436483
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "M13_106"
      name "H_underscore_sub_underscore_2_underscore_endsub_underscore_O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa356"
      uniprot "NA"
    ]
    graphics [
      x 932.5
      y 816.0001579346998
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29033"
      hgnc "NA"
      map_id "M13_115"
      name "Fe2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa365"
      uniprot "NA"
    ]
    graphics [
      x 1982.5
      y 1112.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A49648"
      hgnc "NA"
      map_id "M13_113"
      name "HO"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa363"
      uniprot "NA"
    ]
    graphics [
      x 2882.5
      y 1262.7054530749897
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29034"
      hgnc "NA"
      map_id "M13_116"
      name "Fe3_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa366"
      uniprot "NA"
    ]
    graphics [
      x 2342.5
      y 1082.963502381953
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16234"
      hgnc "NA"
      map_id "M13_114"
      name "hydroxide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa364"
      uniprot "NA"
    ]
    graphics [
      x 932.5
      y 1561.4892846436483
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_70"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re70"
      uniprot "NA"
    ]
    graphics [
      x 2582.5
      y 1909.6649132197108
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29033"
      hgnc "NA"
      map_id "M13_117"
      name "Fe2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa367"
      uniprot "NA"
    ]
    graphics [
      x 3362.5
      y 2083.270850164884
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29034"
      hgnc "NA"
      map_id "M13_118"
      name "Fe3_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa368"
      uniprot "NA"
    ]
    graphics [
      x 2808.7805189192004
      y 2425.149927253783
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:refseq:NM_012473;urn:miriam:hgnc:17772;urn:miriam:uniprot:Q99757;urn:miriam:ncbigene:25828;urn:miriam:ncbigene:25828;urn:miriam:hgnc.symbol:TXN2;urn:miriam:hgnc.symbol:TXN2;urn:miriam:ensembl:ENSG00000100348"
      hgnc "HGNC_SYMBOL:TXN2"
      map_id "M13_137"
      name "TXN2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa392"
      uniprot "UNIPROT:Q99757"
    ]
    graphics [
      x 2242.1908575608613
      y 549.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_137"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:pubmed:26067716;urn:miriam:uniprot:P30044;urn:miriam:uniprot:P30044;urn:miriam:refseq:NM_181651;urn:miriam:ensembl:ENSG00000126432;urn:miriam:ec-code:1.11.1.24;urn:miriam:hgnc.symbol:PRDX5;urn:miriam:hgnc.symbol:PRDX5;urn:miriam:hgnc:9355;urn:miriam:ncbigene:25824;urn:miriam:ncbigene:25824;urn:miriam:ec-code:3.1.1.4;urn:miriam:hgnc:16753;urn:miriam:ec-code:2.3.1.23;urn:miriam:refseq:NM_004905;urn:miriam:ensembl:ENSG00000117592;urn:miriam:hgnc.symbol:PRDX6;urn:miriam:hgnc.symbol:PRDX6;urn:miriam:ncbigene:9588;urn:miriam:ncbigene:9588;urn:miriam:uniprot:P30041;urn:miriam:uniprot:P30041;urn:miriam:ec-code:1.11.1.27;urn:miriam:uniprot:Q06830;urn:miriam:uniprot:Q06830;urn:miriam:refseq:NM_181697;urn:miriam:ncbigene:5052;urn:miriam:ncbigene:5052;urn:miriam:hgnc:9352;urn:miriam:ec-code:1.11.1.24;urn:miriam:ensembl:ENSG00000117450;urn:miriam:hgnc.symbol:PRDX1;urn:miriam:hgnc.symbol:PRDX1;urn:miriam:ncbigene:7001;urn:miriam:ncbigene:7001;urn:miriam:refseq:NM_005809;urn:miriam:uniprot:P32119;urn:miriam:uniprot:P32119;urn:miriam:ec-code:1.11.1.24;urn:miriam:ensembl:ENSG00000167815;urn:miriam:hgnc:9353;urn:miriam:hgnc.symbol:PRDX2;urn:miriam:hgnc.symbol:PRDX2;urn:miriam:ensembl:ENSG00000165672;urn:miriam:ncbigene:10935;urn:miriam:ncbigene:10935;urn:miriam:uniprot:P30048;urn:miriam:uniprot:P30048;urn:miriam:refseq:NM_006793;urn:miriam:ec-code:1.11.1.24;urn:miriam:hgnc.symbol:PRDX3;urn:miriam:hgnc.symbol:PRDX3;urn:miriam:hgnc:9354"
      hgnc "HGNC_SYMBOL:PRDX5;HGNC_SYMBOL:PRDX6;HGNC_SYMBOL:PRDX1;HGNC_SYMBOL:PRDX2;HGNC_SYMBOL:PRDX3"
      map_id "M13_27"
      name "PRDX"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa97"
      uniprot "UNIPROT:P30044;UNIPROT:P30041;UNIPROT:Q06830;UNIPROT:P32119;UNIPROT:P30048"
    ]
    graphics [
      x 2162.5
      y 755.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:refseq:NM_012473;urn:miriam:hgnc:17772;urn:miriam:uniprot:Q99757;urn:miriam:ncbigene:25828;urn:miriam:ncbigene:25828;urn:miriam:hgnc.symbol:TXN2;urn:miriam:hgnc.symbol:TXN2;urn:miriam:ensembl:ENSG00000100348"
      hgnc "HGNC_SYMBOL:TXN2"
      map_id "M13_136"
      name "TXN2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa391"
      uniprot "UNIPROT:Q99757"
    ]
    graphics [
      x 3212.5
      y 1512.7257342680077
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "PUBMED:12032145"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_76"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re76"
      uniprot "NA"
    ]
    graphics [
      x 1892.5
      y 932.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16474"
      hgnc "NA"
      map_id "M13_140"
      name "NADPH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa395"
      uniprot "NA"
    ]
    graphics [
      x 902.5
      y 877.2024583221429
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_140"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ec-code:1.8.1.9;urn:miriam:ensembl:ENSG00000184470;urn:miriam:hgnc.symbol:TXNRD2;urn:miriam:hgnc.symbol:TXNRD2;urn:miriam:refseq:NM_006440;urn:miriam:hgnc:18155;urn:miriam:ncbigene:10587;urn:miriam:ncbigene:10587;urn:miriam:uniprot:Q9NNW7"
      hgnc "HGNC_SYMBOL:TXNRD2"
      map_id "M13_138"
      name "TXNRD2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa393"
      uniprot "UNIPROT:Q9NNW7"
    ]
    graphics [
      x 2942.5
      y 1882.6505321512923
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_138"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18009"
      hgnc "NA"
      map_id "M13_139"
      name "NADP(_plus_)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa394"
      uniprot "NA"
    ]
    graphics [
      x 1951.8191902032843
      y 1322.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_139"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18421;urn:miriam:obo.chebi:CHEBI%3A1842"
      hgnc "NA"
      map_id "M13_104"
      name "superoxide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa354"
      uniprot "NA"
    ]
    graphics [
      x 2732.5
      y 1331.4928470671368
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29235"
      hgnc "NA"
      map_id "M13_120"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa370"
      uniprot "NA"
    ]
    graphics [
      x 2312.5
      y 1119.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "M13_119"
      name "H_underscore_sub_underscore_2_underscore_endsub_underscore_O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa369"
      uniprot "NA"
    ]
    graphics [
      x 2882.5
      y 1953.858034494286
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_73"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re73"
      uniprot "NA"
    ]
    graphics [
      x 3122.5
      y 1147.69481713623
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_72"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re72"
      uniprot "NA"
    ]
    graphics [
      x 2557.4327087287393
      y 540.2336873325008
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29034"
      hgnc "NA"
      map_id "M13_126"
      name "Fe3_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa376"
      uniprot "NA"
    ]
    graphics [
      x 2552.5
      y 1014.5767206657562
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15379"
      hgnc "NA"
      map_id "M13_124"
      name "O_underscore_sub_underscore_2_underscore_endsub_underscore_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa374"
      uniprot "NA"
    ]
    graphics [
      x 2026.5680063480233
      y 512.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_124"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29235"
      hgnc "NA"
      map_id "M13_127"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa377"
      uniprot "NA"
    ]
    graphics [
      x 2942.5
      y 982.6505321512923
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_127"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29033"
      hgnc "NA"
      map_id "M13_125"
      name "Fe2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa375"
      uniprot "NA"
    ]
    graphics [
      x 1772.5
      y 1802.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_125"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16856"
      hgnc "NA"
      map_id "M13_131"
      name "glutathione"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa381"
      uniprot "NA"
    ]
    graphics [
      x 3062.5
      y 1677.9068770517201
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_131"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ensembl:ENSG00000167468;urn:miriam:ncbigene:2879;urn:miriam:ncbigene:2879;urn:miriam:hgnc:4556;urn:miriam:hgnc.symbol:GPX4;urn:miriam:hgnc.symbol:GPX4;urn:miriam:refseq:NM_002085;urn:miriam:uniprot:P36969;urn:miriam:ec-code:1.11.1.12"
      hgnc "HGNC_SYMBOL:GPX4"
      map_id "M13_129"
      name "GPX4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa379"
      uniprot "UNIPROT:P36969"
    ]
    graphics [
      x 992.5
      y 1100.4120397530464
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_129"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ncbigene:2876;urn:miriam:ncbigene:2876;urn:miriam:hgnc:4553;urn:miriam:ensembl:ENSG00000233276;urn:miriam:uniprot:P07203;urn:miriam:hgnc.symbol:GPX1;urn:miriam:hgnc.symbol:GPX1;urn:miriam:refseq:NM_000581;urn:miriam:ec-code:1.11.1.9"
      hgnc "HGNC_SYMBOL:GPX1"
      map_id "M13_128"
      name "GPX1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa378"
      uniprot "UNIPROT:P07203"
    ]
    graphics [
      x 2238.7805189192004
      y 2192.458314370092
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A58297"
      hgnc "NA"
      map_id "M13_130"
      name "glutathione_space_disulfide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa380"
      uniprot "NA"
    ]
    graphics [
      x 842.5
      y 1822.0043330213798
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_130"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_75"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re75"
      uniprot "NA"
    ]
    graphics [
      x 2702.5
      y 1274.6209790551018
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16474"
      hgnc "NA"
      map_id "M13_132"
      name "NADPH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa382"
      uniprot "NA"
    ]
    graphics [
      x 3452.5
      y 1845.1001068356327
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ensembl:ENSG00000104687;urn:miriam:ec-code:1.8.1.7;urn:miriam:hgnc:4623;urn:miriam:ncbigene:2936;urn:miriam:ncbigene:2936;urn:miriam:refseq:NM_000637;urn:miriam:hgnc.symbol:GSR;urn:miriam:hgnc.symbol:GSR;urn:miriam:uniprot:P00390"
      hgnc "HGNC_SYMBOL:GSR"
      map_id "M13_134"
      name "GSR"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa384"
      uniprot "UNIPROT:P00390"
    ]
    graphics [
      x 2882.5
      y 917.330861409245
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18009"
      hgnc "NA"
      map_id "M13_133"
      name "NADP(_plus_)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa383"
      uniprot "NA"
    ]
    graphics [
      x 2762.5
      y 2035.8267364055584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_133"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29235"
      hgnc "NA"
      map_id "M13_123"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa373"
      uniprot "NA"
    ]
    graphics [
      x 2372.5
      y 1702.0608348716244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:refseq:NM_000454;urn:miriam:uniprot:P00441;urn:miriam:ensembl:ENSG00000142168;urn:miriam:hgnc:11179;urn:miriam:hgnc.symbol:SOD1;urn:miriam:hgnc.symbol:SOD1;urn:miriam:ncbigene:6647;urn:miriam:ncbigene:6647;urn:miriam:ec-code:1.15.1.1"
      hgnc "HGNC_SYMBOL:SOD1"
      map_id "M13_122"
      name "SOD1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa372"
      uniprot "UNIPROT:P00441"
    ]
    graphics [
      x 2492.5
      y 2191.435024234829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29235"
      hgnc "NA"
      map_id "M13_121"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa371"
      uniprot "NA"
    ]
    graphics [
      x 2167.4327087287393
      y 443.71330986413113
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ensembl:ENSG00000112096;urn:miriam:hgnc:11180;urn:miriam:ncbigene:6648;urn:miriam:ncbigene:6648;urn:miriam:uniprot:P04179;urn:miriam:refseq:NM_000636;urn:miriam:hgnc.symbol:SOD2;urn:miriam:hgnc.symbol:SOD2;urn:miriam:ec-code:1.15.1.1"
      hgnc "HGNC_SYMBOL:SOD2"
      map_id "M13_108"
      name "SOD2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa358"
      uniprot "UNIPROT:P04179"
    ]
    graphics [
      x 1682.5
      y 627.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      annotation "PUBMED:26071769"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_63"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re61"
      uniprot "NA"
    ]
    graphics [
      x 1571.043215136001
      y 2549.738142841986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ensembl:ENSG00000112096;urn:miriam:hgnc:11180;urn:miriam:ncbigene:6648;urn:miriam:ncbigene:6648;urn:miriam:uniprot:P04179;urn:miriam:refseq:NM_000636;urn:miriam:hgnc.symbol:SOD2;urn:miriam:hgnc.symbol:SOD2;urn:miriam:ec-code:1.15.1.1"
      hgnc "HGNC_SYMBOL:SOD2"
      map_id "M13_107"
      name "SOD2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa357"
      uniprot "UNIPROT:P04179"
    ]
    graphics [
      x 1908.7805189192002
      y 2222.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ncbigene:23410;urn:miriam:ncbigene:23410;urn:miriam:uniprot:Q9NTG7;urn:miriam:hgnc.symbol:SIRT3;urn:miriam:hgnc.symbol:SIRT3;urn:miriam:hgnc:14931;urn:miriam:refseq:NM_001017524;urn:miriam:ec-code:2.3.1.286;urn:miriam:ensembl:ENSG00000142082"
      hgnc "HGNC_SYMBOL:SIRT3"
      map_id "M13_109"
      name "SIRT3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa359"
      uniprot "UNIPROT:Q9NTG7"
    ]
    graphics [
      x 2238.7805189192004
      y 2462.458314370092
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.go:GO%3A0045273;urn:miriam:doi:10.1021/bi901627u;urn:miriam:ncbigene:6391;urn:miriam:ncbigene:6391;urn:miriam:refseq:NM_003001;urn:miriam:hgnc.symbol:SDHC;urn:miriam:hgnc.symbol:SDHC;urn:miriam:hgnc:10682;urn:miriam:ensembl:ENSG00000143252;urn:miriam:uniprot:Q99643;urn:miriam:uniprot:Q99643;urn:miriam:ncbigene:6392;urn:miriam:uniprot:O14521;urn:miriam:uniprot:O14521;urn:miriam:ncbigene:6392;urn:miriam:hgnc.symbol:SDHD;urn:miriam:refseq:NM_003002;urn:miriam:hgnc.symbol:SDHD;urn:miriam:ensembl:ENSG00000204370;urn:miriam:hgnc:10683"
      hgnc "HGNC_SYMBOL:SDHC;HGNC_SYMBOL:SDHD"
      map_id "M13_19"
      name "complex_space_2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa84"
      uniprot "UNIPROT:Q99643;UNIPROT:O14521"
    ]
    graphics [
      x 2291.4241222035007
      y 2984.2764026524364
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.go:GO%3A0045273;urn:miriam:doi:10.1021/bi901627u;urn:miriam:ncbigene:6392;urn:miriam:uniprot:O14521;urn:miriam:uniprot:O14521;urn:miriam:ncbigene:6392;urn:miriam:hgnc.symbol:SDHD;urn:miriam:refseq:NM_003002;urn:miriam:hgnc.symbol:SDHD;urn:miriam:ensembl:ENSG00000204370;urn:miriam:hgnc:10683;urn:miriam:ncbigene:6391;urn:miriam:ncbigene:6391;urn:miriam:refseq:NM_003001;urn:miriam:hgnc.symbol:SDHC;urn:miriam:hgnc.symbol:SDHC;urn:miriam:hgnc:10682;urn:miriam:ensembl:ENSG00000143252;urn:miriam:uniprot:Q99643;urn:miriam:uniprot:Q99643"
      hgnc "HGNC_SYMBOL:SDHD;HGNC_SYMBOL:SDHC"
      map_id "M13_4"
      name "complex_space_2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa24"
      uniprot "UNIPROT:O14521;UNIPROT:Q99643"
    ]
    graphics [
      x 2522.5
      y 932.9715035857598
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17976"
      hgnc "NA"
      map_id "M13_94"
      name "QH_underscore_sub_underscore_2_underscore_endsub_underscore_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa220"
      uniprot "NA"
    ]
    graphics [
      x 1921.4316445647612
      y 3062.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re34"
      uniprot "NA"
    ]
    graphics [
      x 1712.5
      y 1527.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      annotation "PUBMED:31082116"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_55"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re38"
      uniprot "NA"
    ]
    graphics [
      x 2591.043215136001
      y 2587.4407707221744
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_149"
      name "TCA"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa500"
      uniprot "NA"
    ]
    graphics [
      x 2972.5
      y 1934.936290989684
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_149"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.go:GO%3A0045273;urn:miriam:doi:10.1021/bi901627u;urn:miriam:ncbigene:6392;urn:miriam:uniprot:O14521;urn:miriam:uniprot:O14521;urn:miriam:ncbigene:6392;urn:miriam:hgnc.symbol:SDHD;urn:miriam:refseq:NM_003002;urn:miriam:hgnc.symbol:SDHD;urn:miriam:ensembl:ENSG00000204370;urn:miriam:hgnc:10683;urn:miriam:ncbigene:6391;urn:miriam:ncbigene:6391;urn:miriam:refseq:NM_003001;urn:miriam:hgnc.symbol:SDHC;urn:miriam:hgnc.symbol:SDHC;urn:miriam:hgnc:10682;urn:miriam:ensembl:ENSG00000143252;urn:miriam:uniprot:Q99643;urn:miriam:uniprot:Q99643"
      hgnc "HGNC_SYMBOL:SDHD;HGNC_SYMBOL:SDHC"
      map_id "M13_3"
      name "complex_space_2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa23"
      uniprot "UNIPROT:O14521;UNIPROT:Q99643"
    ]
    graphics [
      x 2021.043215136001
      y 2582.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ncbigene:23410;urn:miriam:ncbigene:23410;urn:miriam:uniprot:Q9NTG7;urn:miriam:hgnc.symbol:SIRT3;urn:miriam:hgnc.symbol:SIRT3;urn:miriam:hgnc:14931;urn:miriam:refseq:NM_001017524;urn:miriam:ec-code:2.3.1.286;urn:miriam:ensembl:ENSG00000142082"
      hgnc "HGNC_SYMBOL:SIRT3"
      map_id "M13_92"
      name "SIRT3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa205"
      uniprot "UNIPROT:Q9NTG7"
    ]
    graphics [
      x 2732.5
      y 1886.5950284240166
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29235"
      hgnc "NA"
      map_id "M13_183"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa715"
      uniprot "NA"
    ]
    graphics [
      x 1262.5
      y 774.4666355513621
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_183"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385;PUBMED:30030361"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_44"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re155"
      uniprot "NA"
    ]
    graphics [
      x 3152.5
      y 1292.467395221327
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_182"
      name "s1195"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa714"
      uniprot "NA"
    ]
    graphics [
      x 3092.5
      y 717.0340754435495
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_182"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ncbigene:28976;urn:miriam:ncbigene:28976;urn:miriam:refseq:NM_014049;urn:miriam:ec-code:1.3.8.-;urn:miriam:ensembl:ENSG00000177646;urn:miriam:hgnc:21497;urn:miriam:hgnc.symbol:ACAD9;urn:miriam:uniprot:Q9H845;urn:miriam:uniprot:Q9H845;urn:miriam:hgnc.symbol:ACAD9"
      hgnc "HGNC_SYMBOL:ACAD9"
      map_id "M13_165"
      name "ACAD9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa668"
      uniprot "UNIPROT:Q9H845"
    ]
    graphics [
      x 3212.5
      y 1220.8910219476425
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_165"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:uniprot:Q9BQ95;urn:miriam:uniprot:Q9BQ95;urn:miriam:ensembl:ENSG00000130159;urn:miriam:hgnc:29548;urn:miriam:hgnc.symbol:ECSIT;urn:miriam:refseq:NM_016581;urn:miriam:hgnc.symbol:ECSIT;urn:miriam:ncbigene:51295;urn:miriam:ncbigene:51295"
      hgnc "HGNC_SYMBOL:ECSIT"
      map_id "M13_164"
      name "ECSIT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa667"
      uniprot "UNIPROT:Q9BQ95"
    ]
    graphics [
      x 2762.5
      y 1501.2535183076814
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_164"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ensembl:ENSG00000003509;urn:miriam:hgnc.symbol:NDUFAF7;urn:miriam:hgnc.symbol:NDUFAF7;urn:miriam:ec-code:2.1.1.320;urn:miriam:hgnc:28816;urn:miriam:ncbigene:55471;urn:miriam:uniprot:Q7L592;urn:miriam:uniprot:Q7L592;urn:miriam:ncbigene:55471;urn:miriam:refseq:NM_144736"
      hgnc "HGNC_SYMBOL:NDUFAF7"
      map_id "M13_166"
      name "NDUFAF7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa669"
      uniprot "UNIPROT:Q7L592"
    ]
    graphics [
      x 1682.5
      y 1167.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_166"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re158"
      uniprot "NA"
    ]
    graphics [
      x 992.5
      y 1514.8939431416584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_167"
      name "s1139"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa670"
      uniprot "NA"
    ]
    graphics [
      x 1142.5
      y 1314.9910422829782
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_167"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ncbiprotein:YP_009725303"
      hgnc "NA"
      map_id "M13_148"
      name "Nsp7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa499"
      uniprot "NA"
    ]
    graphics [
      x 1202.5
      y 1977.3156502059937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_148"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_46"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re157"
      uniprot "NA"
    ]
    graphics [
      x 1933.1138130093018
      y 1262.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_168"
      name "s1139"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa671"
      uniprot "NA"
    ]
    graphics [
      x 2282.5
      y 1056.211238590613
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_168"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re156"
      uniprot "NA"
    ]
    graphics [
      x 1742.5
      y 1287.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_169"
      name "s1139"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa672"
      uniprot "NA"
    ]
    graphics [
      x 1082.5
      y 946.3529417498704
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_169"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_43"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re154"
      uniprot "NA"
    ]
    graphics [
      x 752.5
      y 1162.6097915613125
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_161"
      name "s1131"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa663"
      uniprot "NA"
    ]
    graphics [
      x 1472.5
      y 1366.4685701968617
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_161"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 139
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_162"
      target_id "M13_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 3
    target 2
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "INHIBITION"
      source_id "M13_147"
      target_id "M13_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 2
    target 4
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_42"
      target_id "M13_160"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 3
    target 135
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "INHIBITION"
      source_id "M13_147"
      target_id "M13_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 3
    target 137
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "INHIBITION"
      source_id "M13_147"
      target_id "M13_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 144
    source 3
    target 133
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "INHIBITION"
      source_id "M13_147"
      target_id "M13_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 145
    source 4
    target 5
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_160"
      target_id "M13_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 146
    source 6
    target 5
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_159"
      target_id "M13_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 147
    source 7
    target 5
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_163"
      target_id "M13_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 148
    source 8
    target 5
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_28"
      target_id "M13_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 149
    source 9
    target 5
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_7"
      target_id "M13_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 150
    source 5
    target 10
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_29"
      target_id "M13_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 151
    source 137
    target 7
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_43"
      target_id "M13_163"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 152
    source 125
    target 9
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_44"
      target_id "M13_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 153
    source 9
    target 119
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_7"
      target_id "M13_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 154
    source 9
    target 54
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_7"
      target_id "M13_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 155
    source 9
    target 51
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_7"
      target_id "M13_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 156
    source 10
    target 11
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_2"
      target_id "M13_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 157
    source 10
    target 12
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_2"
      target_id "M13_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 158
    source 10
    target 13
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_2"
      target_id "M13_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 159
    source 10
    target 14
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_2"
      target_id "M13_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 160
    source 124
    target 11
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_183"
      target_id "M13_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 161
    source 11
    target 47
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_57"
      target_id "M13_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 162
    source 116
    target 12
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_19"
      target_id "M13_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 163
    source 20
    target 12
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_93"
      target_id "M13_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 164
    source 12
    target 117
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_56"
      target_id "M13_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 165
    source 12
    target 118
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_56"
      target_id "M13_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 166
    source 57
    target 13
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_9"
      target_id "M13_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 167
    source 58
    target 13
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_110"
      target_id "M13_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 168
    source 13
    target 59
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_78"
      target_id "M13_141"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 169
    source 15
    target 14
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_90"
      target_id "M13_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 170
    source 14
    target 16
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_36"
      target_id "M13_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 171
    source 14
    target 17
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_36"
      target_id "M13_153"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 172
    source 14
    target 18
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_36"
      target_id "M13_152"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 173
    source 18
    target 19
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_152"
      target_id "M13_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 174
    source 20
    target 19
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_93"
      target_id "M13_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 175
    source 19
    target 21
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_37"
      target_id "M13_154"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 176
    source 21
    target 22
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_154"
      target_id "M13_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 177
    source 23
    target 22
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_97"
      target_id "M13_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 178
    source 22
    target 24
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_38"
      target_id "M13_155"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 179
    source 25
    target 23
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_83"
      target_id "M13_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 180
    source 26
    target 25
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_96"
      target_id "M13_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 181
    source 27
    target 25
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_87"
      target_id "M13_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 182
    source 28
    target 25
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_89"
      target_id "M13_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 29
    target 25
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_98"
      target_id "M13_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 30
    target 25
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_5"
      target_id "M13_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 25
    target 31
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_83"
      target_id "M13_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 25
    target 32
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_83"
      target_id "M13_150"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 25
    target 33
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_83"
      target_id "M13_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 51
    target 30
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_58"
      target_id "M13_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 30
    target 46
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_5"
      target_id "M13_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 33
    target 34
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_91"
      target_id "M13_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 34
    target 35
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_31"
      target_id "M13_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 36
    target 35
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_53"
      target_id "M13_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 37
    target 35
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_35"
      target_id "M13_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 38
    target 35
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_34"
      target_id "M13_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 35
    target 39
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_8"
      target_id "M13_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 35
    target 40
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_8"
      target_id "M13_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 48
    target 36
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_180"
      target_id "M13_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 49
    target 36
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_179"
      target_id "M13_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 50
    target 36
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "INHIBITION"
      source_id "M13_181"
      target_id "M13_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 47
    target 37
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_88"
      target_id "M13_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 45
    target 38
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_151"
      target_id "M13_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 42
    target 39
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_100"
      target_id "M13_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 43
    target 39
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_101"
      target_id "M13_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 41
    target 39
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M13_103"
      target_id "M13_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 39
    target 44
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_49"
      target_id "M13_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 40
    target 41
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_32"
      target_id "M13_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 46
    target 45
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_33"
      target_id "M13_151"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 52
    target 51
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_184"
      target_id "M13_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 53
    target 51
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_1"
      target_id "M13_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 53
    target 54
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_1"
      target_id "M13_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 55
    target 54
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_185"
      target_id "M13_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 54
    target 56
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_59"
      target_id "M13_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 60
    target 57
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_77"
      target_id "M13_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 62
    target 58
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_65"
      target_id "M13_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 60
    target 58
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_77"
      target_id "M13_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 58
    target 63
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_110"
      target_id "M13_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 58
    target 64
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_110"
      target_id "M13_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 59
    target 60
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_141"
      target_id "M13_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 61
    target 60
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_111"
      target_id "M13_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 61
    target 62
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_111"
      target_id "M13_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 111
    target 63
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_121"
      target_id "M13_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 112
    target 63
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_108"
      target_id "M13_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 63
    target 69
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_64"
      target_id "M13_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 64
    target 65
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_66"
      target_id "M13_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 66
    target 65
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_69"
      target_id "M13_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 67
    target 65
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_67"
      target_id "M13_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 68
    target 65
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_71"
      target_id "M13_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 81
    target 66
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_114"
      target_id "M13_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 92
    target 67
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_104"
      target_id "M13_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 69
    target 68
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_105"
      target_id "M13_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 70
    target 69
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_60"
      target_id "M13_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 69
    target 71
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_105"
      target_id "M13_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 69
    target 72
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_105"
      target_id "M13_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 69
    target 73
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_105"
      target_id "M13_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 69
    target 74
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_105"
      target_id "M13_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 69
    target 75
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_105"
      target_id "M13_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 92
    target 70
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_104"
      target_id "M13_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 109
    target 70
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_123"
      target_id "M13_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 239
    source 110
    target 70
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_122"
      target_id "M13_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 240
    source 101
    target 71
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_131"
      target_id "M13_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 241
    source 102
    target 71
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_129"
      target_id "M13_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 242
    source 103
    target 71
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_128"
      target_id "M13_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 243
    source 71
    target 77
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_74"
      target_id "M13_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 244
    source 71
    target 104
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_74"
      target_id "M13_130"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 245
    source 81
    target 72
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_114"
      target_id "M13_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 246
    source 72
    target 92
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_61"
      target_id "M13_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 247
    source 72
    target 93
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_61"
      target_id "M13_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 248
    source 72
    target 94
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_61"
      target_id "M13_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 249
    source 85
    target 73
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_137"
      target_id "M13_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 250
    source 86
    target 73
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_27"
      target_id "M13_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 251
    source 73
    target 77
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_62"
      target_id "M13_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 252
    source 73
    target 87
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_62"
      target_id "M13_136"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 253
    source 78
    target 74
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_115"
      target_id "M13_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 254
    source 74
    target 79
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_68"
      target_id "M13_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 255
    source 74
    target 80
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_68"
      target_id "M13_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 256
    source 74
    target 81
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_68"
      target_id "M13_114"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 257
    source 76
    target 75
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_135"
      target_id "M13_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 258
    source 75
    target 77
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_30"
      target_id "M13_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 259
    source 82
    target 79
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_70"
      target_id "M13_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 260
    source 81
    target 82
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_114"
      target_id "M13_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 261
    source 83
    target 82
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_117"
      target_id "M13_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 262
    source 82
    target 84
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_70"
      target_id "M13_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 263
    source 88
    target 85
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_76"
      target_id "M13_137"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 264
    source 87
    target 88
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_136"
      target_id "M13_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 265
    source 89
    target 88
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_140"
      target_id "M13_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 266
    source 90
    target 88
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_138"
      target_id "M13_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 267
    source 88
    target 91
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_76"
      target_id "M13_139"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 268
    source 95
    target 92
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_73"
      target_id "M13_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 269
    source 92
    target 96
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_104"
      target_id "M13_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 270
    source 98
    target 95
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_124"
      target_id "M13_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 271
    source 97
    target 96
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_126"
      target_id "M13_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 272
    source 96
    target 98
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_72"
      target_id "M13_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 273
    source 96
    target 99
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_72"
      target_id "M13_127"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 274
    source 96
    target 100
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_72"
      target_id "M13_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 275
    source 105
    target 101
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_75"
      target_id "M13_131"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 276
    source 104
    target 105
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_130"
      target_id "M13_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 277
    source 106
    target 105
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_132"
      target_id "M13_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 278
    source 107
    target 105
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_134"
      target_id "M13_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 105
    target 108
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_75"
      target_id "M13_133"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 113
    target 112
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_63"
      target_id "M13_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 114
    target 113
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_107"
      target_id "M13_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 115
    target 113
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_109"
      target_id "M13_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 120
    target 116
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_55"
      target_id "M13_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 119
    target 117
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_54"
      target_id "M13_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 117
    target 120
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_4"
      target_id "M13_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 122
    target 119
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_3"
      target_id "M13_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 123
    target 119
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_92"
      target_id "M13_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 121
    target 120
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_149"
      target_id "M13_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 126
    target 125
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_182"
      target_id "M13_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 127
    target 125
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_165"
      target_id "M13_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 128
    target 125
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_164"
      target_id "M13_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 129
    target 125
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_166"
      target_id "M13_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 135
    target 127
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_45"
      target_id "M13_165"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 133
    target 128
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_46"
      target_id "M13_164"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 130
    target 129
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_47"
      target_id "M13_166"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 131
    target 130
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_167"
      target_id "M13_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 132
    target 130
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "INHIBITION"
      source_id "M13_148"
      target_id "M13_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 134
    target 133
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_168"
      target_id "M13_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 136
    target 135
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_169"
      target_id "M13_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 138
    target 137
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_161"
      target_id "M13_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
