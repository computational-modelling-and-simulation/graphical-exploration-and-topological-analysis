# generated with VANTED V2.8.2 at Fri Mar 04 10:04:33 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000204673"
      hgnc "NA"
      map_id "W6_29"
      name "AKT1S1"
      node_subtype "GENE"
      node_type "species"
      org_id "c9d11"
      uniprot "NA"
    ]
    graphics [
      x 1622.5
      y 1072.5365071385852
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_57"
      name "NA"
      node_subtype "UNKNOWN_POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "id28c533ea"
      uniprot "NA"
    ]
    graphics [
      x 302.5
      y 1919.4859836461537
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_62"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id63264543"
      uniprot "NA"
    ]
    graphics [
      x 662.5
      y 832.1311545710906
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:2475;urn:miriam:ncbiprotein:64223;urn:miriam:ncbiprotein:57521;urn:miriam:ncbiprotein:64798"
      hgnc "NA"
      map_id "W6_27"
      name "c5e75"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c5e75"
      uniprot "NA"
    ]
    graphics [
      x 392.5
      y 1457.3429094579028
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_58"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id35a4115e"
      uniprot "NA"
    ]
    graphics [
      x 452.5
      y 1297.404927148254
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_60"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id3a40ef1e"
      uniprot "NA"
    ]
    graphics [
      x 1721.043215136001
      y 2787.1855687946227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_66"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id853c2e26"
      uniprot "NA"
    ]
    graphics [
      x 2072.5
      y 812.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_73"
      name "NA"
      node_subtype "UNKNOWN_POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "ide513164c"
      uniprot "NA"
    ]
    graphics [
      x 1873.7877673644155
      y 1382.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_65"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id7a84a2"
      uniprot "NA"
    ]
    graphics [
      x 632.5
      y 967.077935484341
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_71"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idd277ed15"
      uniprot "NA"
    ]
    graphics [
      x 992.5
      y 890.4120397530463
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000023287;urn:miriam:ncbiprotein:9776;urn:miriam:ncbiprotein:8408"
      hgnc "NA"
      map_id "W6_31"
      name "cf768"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "cf768"
      uniprot "NA"
    ]
    graphics [
      x 2942.5
      y 934.4123061771328
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_68"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id949da026"
      uniprot "NA"
    ]
    graphics [
      x 1742.5
      y 1227.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:wikidata:Q34360359"
      hgnc "NA"
      map_id "W6_2"
      name "Phagophore"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "a1420"
      uniprot "NA"
    ]
    graphics [
      x 2342.5
      y 2012.963502381953
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000116209"
      hgnc "NA"
      map_id "W6_39"
      name "TMEM59"
      node_subtype "GENE"
      node_type "species"
      org_id "e10fa"
      uniprot "NA"
    ]
    graphics [
      x 2227.4327087287393
      y 459.06618499081924
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000101844"
      hgnc "NA"
      map_id "W6_18"
      name "ATG4A"
      node_subtype "GENE"
      node_type "species"
      org_id "bc0e6"
      uniprot "NA"
    ]
    graphics [
      x 2462.5
      y 751.4350242348289
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:9140;urn:miriam:ncbiprotein:55054;urn:miriam:ncbiprotein:9474"
      hgnc "NA"
      map_id "W6_37"
      name "ddc93"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ddc93"
      uniprot "NA"
    ]
    graphics [
      x 2552.5
      y 1884.576720665756
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:10533"
      hgnc "NA"
      map_id "W6_13"
      name "ATG7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b8149"
      uniprot "NA"
    ]
    graphics [
      x 1472.5
      y 796.4685701968617
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:64422"
      hgnc "NA"
      map_id "W6_22"
      name "ATG3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bfd6f"
      uniprot "NA"
    ]
    graphics [
      x 1052.5
      y 1928.5794444430944
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:10133"
      hgnc "NA"
      map_id "W6_11"
      name "OPTN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b3adb"
      uniprot "NA"
    ]
    graphics [
      x 602.5
      y 1719.295395563012
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000066739"
      hgnc "NA"
      map_id "W6_4"
      name "ATG2B"
      node_subtype "GENE"
      node_type "species"
      org_id "a3222"
      uniprot "NA"
    ]
    graphics [
      x 902.5
      y 1508.7939123070432
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:8878"
      hgnc "NA"
      map_id "W6_9"
      name "SQSTM1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "acae9"
      uniprot "NA"
    ]
    graphics [
      x 812.5
      y 1310.3215371113788
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:6194"
      hgnc "NA"
      map_id "W6_24"
      name "RPS6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c1a5f"
      uniprot "NA"
    ]
    graphics [
      x 1668.7805189192002
      y 2097.1855687946227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000158528"
      hgnc "NA"
      map_id "W6_10"
      name "PPP1R9A"
      node_subtype "GENE"
      node_type "species"
      org_id "ad6e9"
      uniprot "NA"
    ]
    graphics [
      x 2552.5
      y 1644.576720665756
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:10241"
      hgnc "NA"
      map_id "W6_16"
      name "CALCOCO2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bbbe9"
      uniprot "NA"
    ]
    graphics [
      x 1742.5
      y 1791.755742606838
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_8"
      name "Autophagosome_slash__br_Late_space_endosome"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "aa8e9"
      uniprot "NA"
    ]
    graphics [
      x 482.5
      y 1304.6023667197312
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:11345"
      hgnc "NA"
      map_id "W6_17"
      name "GABARAPL2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bc090"
      uniprot "NA"
    ]
    graphics [
      x 2672.5
      y 1347.0720280092366
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_36"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "db1e2"
      uniprot "NA"
    ]
    graphics [
      x 3362.5
      y 1793.3387688589096
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_25"
      name "ORF3"
      node_subtype "GENE"
      node_type "species"
      org_id "c28d5"
      uniprot "NA"
    ]
    graphics [
      x 2208.7805189192004
      y 2243.713309864131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_20"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "bf121"
      uniprot "NA"
    ]
    graphics [
      x 2552.5
      y 1734.576720665756
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:8887"
      hgnc "NA"
      map_id "W6_6"
      name "TAX1BP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a579d"
      uniprot "NA"
    ]
    graphics [
      x 2402.5
      y 1156.0046064486714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_55"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id20e40bd3"
      uniprot "NA"
    ]
    graphics [
      x 422.5
      y 1158.6264880398348
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_52"
      name "Lysosome"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "f8183"
      uniprot "NA"
    ]
    graphics [
      x 602.5
      y 931.9876498886439
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000118640"
      hgnc "NA"
      map_id "W6_40"
      name "VAMP8"
      node_subtype "GENE"
      node_type "species"
      org_id "e1e54"
      uniprot "NA"
    ]
    graphics [
      x 302.5
      y 1116.2573143157751
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_33"
      name "Autophago_br_lysosome"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "d106b"
      uniprot "NA"
    ]
    graphics [
      x 1022.5
      y 847.804542056548
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id180e5540"
      uniprot "NA"
    ]
    graphics [
      x 1022.5
      y 1546.7111332684874
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:27072;urn:miriam:ncbiprotein:55823;urn:miriam:ensembl:ENSG00000166887;urn:miriam:ensembl:ENSG00000104142;urn:miriam:ncbiprotein:64601;urn:miriam:ensembl:ENSG00000139719"
      hgnc "NA"
      map_id "W6_51"
      name "HOPS_space_COMPLEX_br_"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f7bb6"
      uniprot "NA"
    ]
    graphics [
      x 1652.5
      y 867.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_3"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "a2165"
      uniprot "NA"
    ]
    graphics [
      x 3152.5
      y 2064.353694186342
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_47"
      name "ORF3"
      node_subtype "GENE"
      node_type "species"
      org_id "ec84a"
      uniprot "NA"
    ]
    graphics [
      x 3452.5
      y 1599.444263232392
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_50"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "f725b"
      uniprot "NA"
    ]
    graphics [
      x 3002.5
      y 1725.9723789255502
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:YP_009725302;urn:miriam:pubmed:33845483"
      hgnc "NA"
      map_id "W6_12"
      name "NSP6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b6a2a"
      uniprot "NA"
    ]
    graphics [
      x 2072.5
      y 1955.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_53"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "fbe32"
      uniprot "NA"
    ]
    graphics [
      x 1736.1439857514508
      y 1377.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_38"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "e048e"
      uniprot "NA"
    ]
    graphics [
      x 1532.5
      y 1229.738142841986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000188554"
      hgnc "NA"
      map_id "W6_32"
      name "NBR1"
      node_subtype "GENE"
      node_type "species"
      org_id "d05df"
      uniprot "NA"
    ]
    graphics [
      x 1802.5
      y 572.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:79065"
      hgnc "NA"
      map_id "W6_43"
      name "ATG9A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e6873"
      uniprot "NA"
    ]
    graphics [
      x 2012.5
      y 1325.8526976066757
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_61"
      name "NA"
      node_subtype "UNKNOWN_POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "id47e9ee53"
      uniprot "NA"
    ]
    graphics [
      x 1382.5
      y 1329.3284249102053
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:wikidata:Q34360359"
      hgnc "NA"
      map_id "W6_41"
      name "e44d3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e44d3"
      uniprot "NA"
    ]
    graphics [
      x 1082.5
      y 1666.3529417498703
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_67"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id8825c1d0"
      uniprot "NA"
    ]
    graphics [
      x 1848.7805189192002
      y 2432.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000108443"
      hgnc "NA"
      map_id "W6_21"
      name "P70S6K"
      node_subtype "GENE"
      node_type "species"
      org_id "bfbce"
      uniprot "NA"
    ]
    graphics [
      x 1488.7805189192002
      y 2174.360221091171
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_56"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id259f64e"
      uniprot "NA"
    ]
    graphics [
      x 2118.7805189192004
      y 2435.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000063046"
      hgnc "NA"
      map_id "W6_28"
      name "EIF4B"
      node_subtype "GENE"
      node_type "species"
      org_id "c8253"
      uniprot "NA"
    ]
    graphics [
      x 2402.5
      y 1726.0046064486714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000155506"
      hgnc "NA"
      map_id "W6_1"
      name "LARP1"
      node_subtype "GENE"
      node_type "species"
      org_id "a13c2"
      uniprot "NA"
    ]
    graphics [
      x 1712.5
      y 1767.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ensembl:ENSG00000149357"
      hgnc "NA"
      map_id "W6_44"
      name "LAMTOR1"
      node_subtype "GENE"
      node_type "species"
      org_id "e6980"
      uniprot "NA"
    ]
    graphics [
      x 2377.4327087287393
      y 512.963502381953
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:5563"
      hgnc "NA"
      map_id "W6_5"
      name "AMPK"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a5318"
      uniprot "NA"
    ]
    graphics [
      x 2628.7805189192004
      y 2537.7682304796126
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:6009"
      hgnc "NA"
      map_id "W6_34"
      name "RHEB"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d10c6"
      uniprot "NA"
    ]
    graphics [
      x 1952.5
      y 1772.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_69"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id9545f48f"
      uniprot "NA"
    ]
    graphics [
      x 2561.043215136001
      y 2701.401936169212
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_63"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id67a2f315"
      uniprot "NA"
    ]
    graphics [
      x 1682.5
      y 1287.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:P31749"
      hgnc "NA"
      map_id "W6_46"
      name "GSK3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ec6c1"
      uniprot "NA"
    ]
    graphics [
      x 3302.5
      y 1058.7586668196132
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_64"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id788d6f1c"
      uniprot "NA"
    ]
    graphics [
      x 2732.5
      y 743.9490706397426
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:P31749"
      hgnc "NA"
      map_id "W6_48"
      name "TSC2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ed654"
      uniprot "NA"
    ]
    graphics [
      x 1202.5
      y 1137.3156502059937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_74"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "ide9784478"
      uniprot "NA"
    ]
    graphics [
      x 1172.5
      y 1857.3156502059937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:uniprot:P31749"
      hgnc "NA"
      map_id "W6_26"
      name "AKT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c34e3"
      uniprot "UNIPROT:P31749"
    ]
    graphics [
      x 1892.5
      y 1472.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "NA"
      hgnc "NA"
      map_id "W6_30"
      name "ORF3"
      node_subtype "GENE"
      node_type "species"
      org_id "cbf03"
      uniprot "NA"
    ]
    graphics [
      x 2688.7805189192004
      y 2264.5317366253753
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4936"
      full_annotation "urn:miriam:ncbiprotein:5315"
      hgnc "NA"
      map_id "W6_7"
      name "PKM"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a628b"
      uniprot "NA"
    ]
    graphics [
      x 888.7805189192002
      y 2345.750201068447
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W6_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 64
    source 2
    target 1
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_57"
      target_id "W6_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 1
    target 3
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_29"
      target_id "W6_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 63
    target 2
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_7"
      target_id "W6_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 3
    target 4
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_62"
      target_id "W6_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 5
    target 4
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_58"
      target_id "W6_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 69
    source 6
    target 4
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_60"
      target_id "W6_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 70
    source 7
    target 4
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_66"
      target_id "W6_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 71
    source 4
    target 8
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_27"
      target_id "W6_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 72
    source 4
    target 9
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_27"
      target_id "W6_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 73
    source 4
    target 10
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_27"
      target_id "W6_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 74
    source 54
    target 5
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_34"
      target_id "W6_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 75
    source 53
    target 6
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_5"
      target_id "W6_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 76
    source 52
    target 7
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_44"
      target_id "W6_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 77
    source 8
    target 48
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_73"
      target_id "W6_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 78
    source 9
    target 51
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_65"
      target_id "W6_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 79
    source 10
    target 11
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_71"
      target_id "W6_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 11
    target 12
    cd19dm [
      diagram "WP4936"
      edge_type "MODULATION"
      source_id "W6_31"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 81
    source 13
    target 12
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_2"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 82
    source 14
    target 12
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_39"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 83
    source 15
    target 12
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_18"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 84
    source 16
    target 12
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_37"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 85
    source 17
    target 12
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_13"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 86
    source 18
    target 12
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_22"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 87
    source 19
    target 12
    cd19dm [
      diagram "WP4936"
      edge_type "MODULATION"
      source_id "W6_11"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 88
    source 20
    target 12
    cd19dm [
      diagram "WP4936"
      edge_type "MODULATION"
      source_id "W6_4"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 89
    source 21
    target 12
    cd19dm [
      diagram "WP4936"
      edge_type "MODULATION"
      source_id "W6_9"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 90
    source 22
    target 12
    cd19dm [
      diagram "WP4936"
      edge_type "INHIBITION"
      source_id "W6_24"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 91
    source 23
    target 12
    cd19dm [
      diagram "WP4936"
      edge_type "MODULATION"
      source_id "W6_10"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 92
    source 24
    target 12
    cd19dm [
      diagram "WP4936"
      edge_type "MODULATION"
      source_id "W6_16"
      target_id "W6_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 93
    source 12
    target 25
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_68"
      target_id "W6_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 94
    source 12
    target 26
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_68"
      target_id "W6_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 95
    source 47
    target 22
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_67"
      target_id "W6_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 96
    source 39
    target 23
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_50"
      target_id "W6_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 97
    source 25
    target 31
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_8"
      target_id "W6_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 98
    source 27
    target 26
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_36"
      target_id "W6_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 99
    source 28
    target 27
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_25"
      target_id "W6_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 100
    source 28
    target 29
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_25"
      target_id "W6_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 29
    target 30
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_20"
      target_id "W6_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 32
    target 31
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_52"
      target_id "W6_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 33
    target 31
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_40"
      target_id "W6_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 31
    target 34
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_55"
      target_id "W6_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 35
    target 33
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_54"
      target_id "W6_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 106
    source 36
    target 35
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_51"
      target_id "W6_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 107
    source 37
    target 36
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_3"
      target_id "W6_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 108
    source 38
    target 37
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_47"
      target_id "W6_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 109
    source 40
    target 39
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_12"
      target_id "W6_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 110
    source 40
    target 41
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_12"
      target_id "W6_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 111
    source 40
    target 42
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_12"
      target_id "W6_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 112
    source 41
    target 44
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_53"
      target_id "W6_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 113
    source 42
    target 43
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_38"
      target_id "W6_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 114
    source 44
    target 45
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_43"
      target_id "W6_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 45
    target 46
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_61"
      target_id "W6_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 48
    target 47
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_21"
      target_id "W6_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 48
    target 49
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_21"
      target_id "W6_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 49
    target 50
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_56"
      target_id "W6_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 55
    target 54
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_69"
      target_id "W6_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 56
    target 54
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_63"
      target_id "W6_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 62
    target 55
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_30"
      target_id "W6_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 57
    target 56
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_46"
      target_id "W6_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 57
    target 58
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_46"
      target_id "W6_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 58
    target 59
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_64"
      target_id "W6_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 60
    target 59
    cd19dm [
      diagram "WP4936"
      edge_type "PRODUCTION"
      source_id "W6_74"
      target_id "W6_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 126
    source 61
    target 60
    cd19dm [
      diagram "WP4936"
      edge_type "CONSPUMPTION"
      source_id "W6_26"
      target_id "W6_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
