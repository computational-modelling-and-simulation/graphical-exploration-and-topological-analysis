# generated with VANTED V2.8.2 at Fri Mar 04 10:04:33 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:wikidata:Q381899"
      hgnc "NA"
      map_id "W14_35"
      name "Fibrinogen"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "d0b3d"
      uniprot "NA"
    ]
    graphics [
      x 2342.5
      y 1862.963502381953
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "c4da9"
      uniprot "NA"
    ]
    graphics [
      x 2252.5
      y 902.4583143700918
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_11"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ad0b7"
      uniprot "NA"
    ]
    graphics [
      x 2942.5
      y 1072.6505321512923
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_50"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "e100c"
      uniprot "NA"
    ]
    graphics [
      x 1915.2777090652366
      y 2612.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:wikidata:Q381899"
      hgnc "NA"
      map_id "W14_62"
      name "Fibrinogen"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "f3c75"
      uniprot "NA"
    ]
    graphics [
      x 1128.7805189192002
      y 2250.504789527062
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "PUBMED:2856554"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_42"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "d83d6"
      uniprot "NA"
    ]
    graphics [
      x 1407.3095774771057
      y 2782.4136664038665
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:uniprot:P00734"
      hgnc "NA"
      map_id "W14_12"
      name "Thrombin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "af109"
      uniprot "UNIPROT:P00734"
    ]
    graphics [
      x 1128.7805189192002
      y 2280.504789527062
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:wikidata:Q27205"
      hgnc "NA"
      map_id "W14_38"
      name "d483c"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d483c"
      uniprot "NA"
    ]
    graphics [
      x 1862.5
      y 2072.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "PUBMED:21742792;PUBMED:27363989"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_2"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "a3d56"
      uniprot "NA"
    ]
    graphics [
      x 1502.5
      y 1507.3397236403869
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:uniprot:P00488;urn:miriam:pubmed:27363989"
      hgnc "NA"
      map_id "W14_25"
      name "Factor_space_XIII_space_A_space_chain"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c1095"
      uniprot "UNIPROT:P00488"
    ]
    graphics [
      x 482.5
      y 1169.4984145220399
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:wikidata:Q27205"
      hgnc "NA"
      map_id "W14_8"
      name "a8861"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "a8861"
      uniprot "NA"
    ]
    graphics [
      x 2317.4327087287393
      y 579.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "PUBMED:2143188"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_64"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f5786"
      uniprot "NA"
    ]
    graphics [
      x 2642.5
      y 1247.7682304796128
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "PUBMED:2143188"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "d27d4"
      uniprot "NA"
    ]
    graphics [
      x 1202.5
      y 635.4370357294522
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:uniprot:P00747"
      hgnc "NA"
      map_id "W14_61"
      name "Plasmin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f1440"
      uniprot "UNIPROT:P00747"
    ]
    graphics [
      x 1802.5
      y 1832.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:wikidata:Q198526;urn:miriam:pubmed:32344011;urn:miriam:pubmed:19008457"
      hgnc "NA"
      map_id "W14_71"
      name "D_minus_Dimer"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "fc137"
      uniprot "NA"
    ]
    graphics [
      x 1807.432708728739
      y 392.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:wikidata:Q2162109"
      hgnc "NA"
      map_id "W14_14"
      name "Fibrin_space_degradation_space__br_products_space_(FDPs)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "b02e2"
      uniprot "NA"
    ]
    graphics [
      x 2012.5
      y 632.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_27"
      name "E"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "c1ef1"
      uniprot "NA"
    ]
    graphics [
      x 3302.5
      y 1442.7997843841147
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_29"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "c3f7f"
      uniprot "NA"
    ]
    graphics [
      x 3422.5
      y 2132.7790747364807
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_48"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "ddc79"
      uniprot "NA"
    ]
    graphics [
      x 1202.5
      y 1737.3156502059937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_66"
      name "D"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "f6688"
      uniprot "NA"
    ]
    graphics [
      x 1592.5
      y 1763.119912305107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_17"
      name "D"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "b0fab"
      uniprot "NA"
    ]
    graphics [
      x 2988.7805189192004
      y 2444.936290989684
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:uniprot:P02679;urn:miriam:uniprot:P02671;urn:miriam:uniprot:P02675"
      hgnc "NA"
      map_id "W14_13"
      name "afcd3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "afcd3"
      uniprot "UNIPROT:P02679;UNIPROT:P02671;UNIPROT:P02675"
    ]
    graphics [
      x 1622.5
      y 648.9254149382612
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_68"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "f9232"
      uniprot "NA"
    ]
    graphics [
      x 2312.5
      y 909.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:uniprot:P02675;urn:miriam:uniprot:P02671;urn:miriam:uniprot:P02679"
      hgnc "NA"
      map_id "W14_7"
      name "a870c"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "a870c"
      uniprot "UNIPROT:P02675;UNIPROT:P02671;UNIPROT:P02679"
    ]
    graphics [
      x 2312.5
      y 1509.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_5"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "a51f3"
      uniprot "NA"
    ]
    graphics [
      x 2972.5
      y 1994.936290989684
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_39"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "d69c7"
      uniprot "NA"
    ]
    graphics [
      x 2732.5
      y 1510.590404477927
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:uniprot:P02679;urn:miriam:uniprot:P02671;urn:miriam:uniprot:P02675"
      hgnc "NA"
      map_id "W14_45"
      name "d9474"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d9474"
      uniprot "UNIPROT:P02679;UNIPROT:P02671;UNIPROT:P02675"
    ]
    graphics [
      x 1873.6642091719086
      y 2162.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_28"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "c203e"
      uniprot "NA"
    ]
    graphics [
      x 1442.5
      y 1295.1961954122032
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_44"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "d91d8"
      uniprot "NA"
    ]
    graphics [
      x 1901.043215136001
      y 2912.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:uniprot:P02675"
      hgnc "NA"
      map_id "W14_22"
      name "FGB_space_(B_beta_)"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bd1cb"
      uniprot "UNIPROT:P02675"
    ]
    graphics [
      x 3092.5
      y 1690.1860833815003
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_60"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "f02cd"
      uniprot "NA"
    ]
    graphics [
      x 3078.7805189192004
      y 2453.9737184211685
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_58"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "efc91"
      uniprot "NA"
    ]
    graphics [
      x 3302.5
      y 1272.6294574380252
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:uniprot:P02679;urn:miriam:uniprot:P02675"
      hgnc "NA"
      map_id "W14_20"
      name "bb70e"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "bb70e"
      uniprot "UNIPROT:P02679;UNIPROT:P02675"
    ]
    graphics [
      x 2222.5
      y 760.2640672722596
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_59"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "efe48"
      uniprot "NA"
    ]
    graphics [
      x 1712.5
      y 1617.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_53"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "eabf8"
      uniprot "NA"
    ]
    graphics [
      x 3542.5
      y 1538.5558453851231
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:uniprot:P02679;urn:miriam:uniprot:P02675;urn:miriam:uniprot:P02671"
      hgnc "NA"
      map_id "W14_21"
      name "bbecc"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "bbecc"
      uniprot "UNIPROT:P02679;UNIPROT:P02675;UNIPROT:P02671"
    ]
    graphics [
      x 3182.5
      y 2162.4674336193925
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_23"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "bf206"
      uniprot "NA"
    ]
    graphics [
      x 2792.5
      y 1912.3725415703839
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:uniprot:P02671"
      hgnc "NA"
      map_id "W14_41"
      name "FGA_space_(A_alpha_)"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d7d71"
      uniprot "UNIPROT:P02671"
    ]
    graphics [
      x 1731.3386501059647
      y 2877.1855687946227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_40"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "d7615"
      uniprot "NA"
    ]
    graphics [
      x 2312.5
      y 2109.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_6"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "a6e53"
      uniprot "NA"
    ]
    graphics [
      x 2402.5
      y 1916.0089009888093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:uniprot:P02671;urn:miriam:uniprot:P02679"
      hgnc "NA"
      map_id "W14_33"
      name "c579f"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c579f"
      uniprot "UNIPROT:P02671;UNIPROT:P02679"
    ]
    graphics [
      x 1322.5
      y 1522.3432595389866
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_54"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "eb5be"
      uniprot "NA"
    ]
    graphics [
      x 992.5
      y 950.4120397530463
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:uniprot:P02679"
      hgnc "NA"
      map_id "W14_26"
      name "FGG_space_(_gamma_)"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c1685"
      uniprot "UNIPROT:P02679"
    ]
    graphics [
      x 2732.5
      y 849.3702052372457
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_46"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "db5f6"
      uniprot "NA"
    ]
    graphics [
      x 2672.5
      y 1015.8948985849099
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:uniprot:P02679"
      hgnc "NA"
      map_id "W14_32"
      name "FGG_space_(_gamma_)"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c5467"
      uniprot "UNIPROT:P02679"
    ]
    graphics [
      x 1412.5
      y 1042.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_9"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "a9600"
      uniprot "NA"
    ]
    graphics [
      x 2522.5
      y 2042.9715035857598
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:ncbigene:2266"
      hgnc "NA"
      map_id "W14_16"
      name "FGG"
      node_subtype "GENE"
      node_type "species"
      org_id "b0c94"
      uniprot "NA"
    ]
    graphics [
      x 2042.5
      y 1768.5318911777056
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:uniprot:P02671"
      hgnc "NA"
      map_id "W14_65"
      name "FGA_space_(A_alpha_)"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f5a20"
      uniprot "UNIPROT:P02671"
    ]
    graphics [
      x 3242.5
      y 1313.410114080192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_10"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "acd27"
      uniprot "NA"
    ]
    graphics [
      x 3032.5
      y 1950.7962381347772
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:ncbigene:2243"
      hgnc "NA"
      map_id "W14_3"
      name "FGA"
      node_subtype "GENE"
      node_type "species"
      org_id "a4aba"
      uniprot "NA"
    ]
    graphics [
      x 3512.5
      y 1481.3795629191009
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:uniprot:P02675"
      hgnc "NA"
      map_id "W14_43"
      name "FGB_space_(B_beta_)"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d8eb7"
      uniprot "UNIPROT:P02675"
    ]
    graphics [
      x 1788.7805189192002
      y 2492.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_63"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f47dc"
      uniprot "NA"
    ]
    graphics [
      x 572.5
      y 1872.95801449575
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:ncbigene:2244"
      hgnc "NA"
      map_id "W14_47"
      name "FGB"
      node_subtype "GENE"
      node_type "species"
      org_id "dd369"
      uniprot "NA"
    ]
    graphics [
      x 1352.5
      y 1059.311461366438
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 54
    source 2
    target 1
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_31"
      target_id "W14_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 55
    source 1
    target 3
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "W14_35"
      target_id "W14_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 56
    source 1
    target 4
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "W14_35"
      target_id "W14_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 57
    source 22
    target 2
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "W14_13"
      target_id "W14_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 58
    source 3
    target 17
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_11"
      target_id "W14_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 59
    source 4
    target 5
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_50"
      target_id "W14_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 60
    source 5
    target 6
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "W14_62"
      target_id "W14_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 61
    source 7
    target 6
    cd19dm [
      diagram "WP4927"
      edge_type "CATALYSIS"
      source_id "W14_12"
      target_id "W14_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 62
    source 6
    target 8
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_42"
      target_id "W14_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 63
    source 8
    target 9
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "W14_38"
      target_id "W14_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 64
    source 10
    target 9
    cd19dm [
      diagram "WP4927"
      edge_type "CATALYSIS"
      source_id "W14_25"
      target_id "W14_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 9
    target 11
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_2"
      target_id "W14_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 11
    target 12
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "W14_8"
      target_id "W14_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 11
    target 13
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "W14_8"
      target_id "W14_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 14
    target 12
    cd19dm [
      diagram "WP4927"
      edge_type "CATALYSIS"
      source_id "W14_61"
      target_id "W14_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 69
    source 12
    target 16
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_64"
      target_id "W14_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 70
    source 14
    target 13
    cd19dm [
      diagram "WP4927"
      edge_type "CATALYSIS"
      source_id "W14_61"
      target_id "W14_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 71
    source 13
    target 15
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_36"
      target_id "W14_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 72
    source 18
    target 17
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_29"
      target_id "W14_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 73
    source 17
    target 19
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "W14_27"
      target_id "W14_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 74
    source 21
    target 18
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "W14_17"
      target_id "W14_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 75
    source 19
    target 20
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_48"
      target_id "W14_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 76
    source 23
    target 22
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_68"
      target_id "W14_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 77
    source 24
    target 23
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "W14_7"
      target_id "W14_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 78
    source 25
    target 24
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_5"
      target_id "W14_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 79
    source 26
    target 24
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_39"
      target_id "W14_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 36
    target 25
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "W14_21"
      target_id "W14_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 81
    source 27
    target 26
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "W14_45"
      target_id "W14_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 82
    source 28
    target 27
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_28"
      target_id "W14_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 83
    source 29
    target 27
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_44"
      target_id "W14_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 84
    source 41
    target 28
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "W14_33"
      target_id "W14_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 85
    source 30
    target 29
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "W14_22"
      target_id "W14_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 86
    source 31
    target 30
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_60"
      target_id "W14_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 87
    source 30
    target 32
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "W14_22"
      target_id "W14_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 88
    source 51
    target 31
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "W14_43"
      target_id "W14_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 89
    source 32
    target 33
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_58"
      target_id "W14_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 90
    source 34
    target 33
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_59"
      target_id "W14_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 91
    source 33
    target 35
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "W14_20"
      target_id "W14_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 92
    source 43
    target 34
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "W14_26"
      target_id "W14_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 93
    source 35
    target 36
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_53"
      target_id "W14_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 94
    source 37
    target 36
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_23"
      target_id "W14_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 95
    source 38
    target 37
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "W14_41"
      target_id "W14_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 96
    source 39
    target 38
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_40"
      target_id "W14_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 97
    source 38
    target 40
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "W14_41"
      target_id "W14_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 98
    source 48
    target 39
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "W14_65"
      target_id "W14_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 99
    source 40
    target 41
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_6"
      target_id "W14_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 100
    source 42
    target 41
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_54"
      target_id "W14_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 43
    target 42
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "W14_26"
      target_id "W14_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 44
    target 43
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_46"
      target_id "W14_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 45
    target 44
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "W14_32"
      target_id "W14_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 46
    target 45
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_9"
      target_id "W14_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 47
    target 46
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "W14_16"
      target_id "W14_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 106
    source 49
    target 48
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_10"
      target_id "W14_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 107
    source 50
    target 49
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "W14_3"
      target_id "W14_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 108
    source 52
    target 51
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_63"
      target_id "W14_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 109
    source 53
    target 52
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "W14_47"
      target_id "W14_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
