# generated with VANTED V2.8.2 at Fri Mar 04 10:04:35 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A14649"
      hgnc "NA"
      map_id "M123_163"
      name "NMN"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa260"
      uniprot "NA"
    ]
    graphics [
      x 1052.5
      y 773.1373437817828
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_163"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_68"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re85"
      uniprot "NA"
    ]
    graphics [
      x 1861.8191902032843
      y 1292.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_69"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re86"
      uniprot "NA"
    ]
    graphics [
      x 692.5
      y 1418.55599096428
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "M123_164"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa261"
      uniprot "NA"
    ]
    graphics [
      x 1052.5
      y 1255.3271467622153
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_164"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29105;urn:miriam:uniprot:P21589;urn:miriam:obo.chebi:CHEBI%3A29105;urn:miriam:ec-code:3.1.3.5;urn:miriam:uniprot:P21589;urn:miriam:uniprot:P21589;urn:miriam:hgnc:8021;urn:miriam:hgnc.symbol:NT5E;urn:miriam:ncbigene:4907;urn:miriam:hgnc.symbol:NT5E;urn:miriam:ncbigene:4907;urn:miriam:ensembl:ENSG00000135318;urn:miriam:refseq:NM_001204813"
      hgnc "HGNC_SYMBOL:NT5E"
      map_id "M123_9"
      name "NT5E:Zn2_plus_"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa35"
      uniprot "UNIPROT:P21589"
    ]
    graphics [
      x 1518.7805189192002
      y 2189.738142841986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15927"
      hgnc "NA"
      map_id "M123_166"
      name "NRNAM"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa263"
      uniprot "NA"
    ]
    graphics [
      x 212.5
      y 1765.4623180844383
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_166"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "M123_165"
      name "Pi"
      node_subtype "ION"
      node_type "species"
      org_id "sa262"
      uniprot "NA"
    ]
    graphics [
      x 1442.5
      y 1205.1961954122032
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_165"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57540"
      hgnc "NA"
      map_id "M123_158"
      name "NAD_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa256"
      uniprot "NA"
    ]
    graphics [
      x 1857.4264683201452
      y 1322.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_158"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "M123_159"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa257"
      uniprot "NA"
    ]
    graphics [
      x 2792.5
      y 1313.3496847756978
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_159"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "M123_161"
      name "Pi"
      node_subtype "ION"
      node_type "species"
      org_id "sa259"
      uniprot "NA"
    ]
    graphics [
      x 2042.5
      y 632.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_161"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_67"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re84"
      uniprot "NA"
    ]
    graphics [
      x 2882.5
      y 1533.858034494286
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_65"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re82"
      uniprot "NA"
    ]
    graphics [
      x 1562.5
      y 1185.4971941397312
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_66"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re83"
      uniprot "NA"
    ]
    graphics [
      x 2972.5
      y 1904.936290989684
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "M123_157"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa255"
      uniprot "NA"
    ]
    graphics [
      x 1472.5
      y 1696.4685701968617
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_157"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:ncbigene:952;urn:miriam:ncbigene:952;urn:miriam:ec-code:3.2.2.6;urn:miriam:hgnc.symbol:CD38;urn:miriam:hgnc.symbol:CD38;urn:miriam:ensembl:ENSG00000004468;urn:miriam:ec-code:2.4.99.20;urn:miriam:hgnc:1667;urn:miriam:refseq:NM_001775;urn:miriam:uniprot:P28907"
      hgnc "HGNC_SYMBOL:CD38"
      map_id "M123_168"
      name "CD38"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa265"
      uniprot "UNIPROT:P28907"
    ]
    graphics [
      x 2231.043215136001
      y 2543.713309864131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_168"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17154"
      hgnc "NA"
      map_id "M123_155"
      name "NAM"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa253"
      uniprot "NA"
    ]
    graphics [
      x 2132.5
      y 1595.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_155"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57967"
      hgnc "NA"
      map_id "M123_160"
      name "ADP_minus_ribose"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa258"
      uniprot "NA"
    ]
    graphics [
      x 1622.5
      y 1942.5365071385852
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_160"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "M123_154"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa252"
      uniprot "NA"
    ]
    graphics [
      x 1292.5
      y 1793.3146446645712
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_154"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:refseq:NM_004334;urn:miriam:ensembl:ENSG00000109743;urn:miriam:ec-code:3.2.2.6;urn:miriam:ncbigene:683;urn:miriam:ncbigene:683;urn:miriam:hgnc:1118;urn:miriam:uniprot:Q10588;urn:miriam:hgnc.symbol:BST1;urn:miriam:hgnc.symbol:BST1"
      hgnc "HGNC_SYMBOL:BST1"
      map_id "M123_167"
      name "BST1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa264"
      uniprot "UNIPROT:Q10588"
    ]
    graphics [
      x 1232.5
      y 2095.876634351126
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_167"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A90174;urn:miriam:obo.chebi:CHEBI%3A90171"
      hgnc "NA"
      map_id "M123_150"
      name "dh_minus_beta_minus_NAD"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa249"
      uniprot "NA"
    ]
    graphics [
      x 3032.5
      y 2079.700836187499
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_150"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15379"
      hgnc "NA"
      map_id "M123_152"
      name "O2"
      node_subtype "ION"
      node_type "species"
      org_id "sa250"
      uniprot "NA"
    ]
    graphics [
      x 1998.7805189192002
      y 2162.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_152"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "M123_153"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa251"
      uniprot "NA"
    ]
    graphics [
      x 2462.5
      y 1651.435024234829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_153"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:uniprot:Q5VYX0;urn:miriam:obo.chebi:CHEBI%3A16238;urn:miriam:ncbigene:55328;urn:miriam:ncbigene:55328;urn:miriam:hgnc.symbol:RNLS;urn:miriam:hgnc.symbol:RNLS;urn:miriam:ec-code:1.6.3.5;urn:miriam:hgnc:25641;urn:miriam:refseq:NM_018363;urn:miriam:uniprot:Q5VYX0;urn:miriam:uniprot:Q5VYX0;urn:miriam:ensembl:ENSG00000184719;urn:miriam:obo.chebi:CHEBI%3A16238"
      hgnc "HGNC_SYMBOL:RNLS"
      map_id "M123_5"
      name "RNLS:FAD"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa20"
      uniprot "UNIPROT:Q5VYX0"
    ]
    graphics [
      x 1262.5
      y 1266.9780694277015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16240"
      hgnc "NA"
      map_id "M123_156"
      name "H2O2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa254"
      uniprot "NA"
    ]
    graphics [
      x 2942.5
      y 876.0691386886826
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_156"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 25
    source 2
    target 1
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_68"
      target_id "M123_163"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 26
    source 1
    target 3
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_163"
      target_id "M123_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 27
    source 8
    target 2
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_158"
      target_id "M123_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 28
    source 9
    target 2
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_159"
      target_id "M123_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 29
    source 5
    target 2
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_9"
      target_id "M123_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 30
    source 2
    target 10
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_68"
      target_id "M123_161"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 31
    source 4
    target 3
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_164"
      target_id "M123_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 32
    source 5
    target 3
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_9"
      target_id "M123_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 33
    source 3
    target 6
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_69"
      target_id "M123_166"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 34
    source 3
    target 7
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_69"
      target_id "M123_165"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 35
    source 11
    target 8
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_67"
      target_id "M123_158"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 36
    source 8
    target 12
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_158"
      target_id "M123_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 37
    source 8
    target 13
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_158"
      target_id "M123_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 38
    source 20
    target 11
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_150"
      target_id "M123_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 39
    source 21
    target 11
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_152"
      target_id "M123_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 40
    source 22
    target 11
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_153"
      target_id "M123_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 41
    source 23
    target 11
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_5"
      target_id "M123_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 42
    source 11
    target 24
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_67"
      target_id "M123_156"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 43
    source 14
    target 12
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_157"
      target_id "M123_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 44
    source 19
    target 12
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_167"
      target_id "M123_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 45
    source 12
    target 17
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_65"
      target_id "M123_160"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 46
    source 12
    target 16
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_65"
      target_id "M123_155"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 47
    source 12
    target 18
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_65"
      target_id "M123_154"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 48
    source 14
    target 13
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_157"
      target_id "M123_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 49
    source 15
    target 13
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_168"
      target_id "M123_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 50
    source 13
    target 16
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_66"
      target_id "M123_155"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 51
    source 13
    target 17
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_66"
      target_id "M123_160"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 52
    source 13
    target 18
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_66"
      target_id "M123_154"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
