# generated with VANTED V2.8.2 at Fri Mar 04 10:04:34 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:wikidata:Q106020256"
      hgnc "NA"
      map_id "W21_53"
      name "S1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bcd14"
      uniprot "NA"
    ]
    graphics [
      x 2552.5
      y 1824.576720665756
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:33391280"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_63"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "c830f"
      uniprot "NA"
    ]
    graphics [
      x 3452.5
      y 1186.2860778188074
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_154"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "fdd8b"
      uniprot "NA"
    ]
    graphics [
      x 1652.5
      y 1677.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_154"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_38"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "b66f3"
      uniprot "NA"
    ]
    graphics [
      x 2077.4327087287393
      y 482.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306;PUBMED:19430490;PUBMED:33335518"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_27"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "b21db"
      uniprot "NA"
    ]
    graphics [
      x 3062.5
      y 2195.341905548022
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000130234"
      hgnc "NA"
      map_id "W21_122"
      name "ACE2"
      node_subtype "GENE"
      node_type "species"
      org_id "f1b6b"
      uniprot "NA"
    ]
    graphics [
      x 2478.7805189192004
      y 2401.435024234829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:P35613"
      hgnc "NA"
      map_id "W21_15"
      name "BSG"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "aa59a"
      uniprot "UNIPROT:P35613"
    ]
    graphics [
      x 2612.5
      y 862.4622902725786
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_143"
      name "Viral_space_particle"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "fbdf9"
      uniprot "NA"
    ]
    graphics [
      x 2822.5
      y 1912.097842491094
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_143"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "PUBMED:33391280"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_56"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "bfa83"
      uniprot "NA"
    ]
    graphics [
      x 3332.5
      y 1176.2015552786877
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000105329"
      hgnc "NA"
      map_id "W21_120"
      name "TGFB1"
      node_subtype "GENE"
      node_type "species"
      org_id "f062c"
      uniprot "NA"
    ]
    graphics [
      x 2822.5
      y 1942.097842491094
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "PUBMED:33391280"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_52"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "bcb8e"
      uniprot "NA"
    ]
    graphics [
      x 2088.7805189192004
      y 2315.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:wikidata:Q22327223"
      hgnc "NA"
      map_id "W21_123"
      name "IgA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f3c02"
      uniprot "NA"
    ]
    graphics [
      x 3122.5
      y 1087.69481713623
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 13
    source 2
    target 1
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_63"
      target_id "W21_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 14
    source 3
    target 1
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_154"
      target_id "W21_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 15
    source 1
    target 4
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_53"
      target_id "W21_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 16
    source 1
    target 5
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_53"
      target_id "W21_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 17
    source 12
    target 2
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_123"
      target_id "W21_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 18
    source 8
    target 3
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_143"
      target_id "W21_154"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 19
    source 4
    target 7
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_38"
      target_id "W21_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 20
    source 5
    target 6
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_27"
      target_id "W21_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 21
    source 8
    target 9
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_143"
      target_id "W21_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 22
    source 9
    target 10
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_56"
      target_id "W21_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 23
    source 10
    target 11
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_120"
      target_id "W21_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 24
    source 11
    target 12
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_52"
      target_id "W21_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
