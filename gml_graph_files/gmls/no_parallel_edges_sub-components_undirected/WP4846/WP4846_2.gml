# generated with VANTED V2.8.2 at Fri Mar 04 10:04:39 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_62"
      name "Endocytosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "d77e4"
      uniprot "NA"
    ]
    graphics [
      x 1442.624726009289
      y 1506.7438968397194
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_165"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ide4f56776"
      uniprot "NA"
    ]
    graphics [
      x 1628.5053598927097
      y 1592.4369672058442
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_165"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "PUBMED:33244168"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_118"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id230f84bb"
      uniprot "NA"
    ]
    graphics [
      x 1318.1423333398493
      y 1640.7520413187995
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_128"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id5dadb7eb"
      uniprot "NA"
    ]
    graphics [
      x 1359.696577131729
      y 1319.5648017957942
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_3"
      name "NA"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "a479d"
      uniprot "NA"
    ]
    graphics [
      x 1251.352519552795
      y 1183.438315225911
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:ensembl:ENSG00000073060;urn:miriam:obo.chebi:CHEBI%3A39025;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:P0DTC2"
      hgnc "NA"
      map_id "W1_106"
      name "recognition_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f9872"
      uniprot "UNIPROT:Q9BYF1;UNIPROT:P0DTC2"
    ]
    graphics [
      x 1158.2201678415547
      y 1742.436922298084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "PUBMED:33244168"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_135"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id7acf7b3"
      uniprot "NA"
    ]
    graphics [
      x 1030.5246581812878
      y 1839.6663738605735
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:obo.chebi:CHEBI%3A39025"
      hgnc "NA"
      map_id "W1_26"
      name "b76b3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b76b3"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 894.3492687689765
      y 1901.1071321864629
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "PUBMED:33244168"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_157"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idce2a065d"
      uniprot "NA"
    ]
    graphics [
      x 753.369675659679
      y 1990.2672182223805
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_157"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:uniprot:P0DTC2"
      hgnc "NA"
      map_id "W1_5"
      name "a4fdf"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "a4fdf"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 635.3302218277004
      y 2052.5397905170344
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:uniprot:Q9BYF1"
      hgnc "NA"
      map_id "W1_96"
      name "recognition_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f1172"
      uniprot "UNIPROT:P0DTC2;UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1783.219101983494
      y 1669.370344043104
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_159"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "idd6e4d05b"
      uniprot "NA"
    ]
    graphics [
      x 1879.2800551392834
      y 1771.2889541343075
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_159"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:uniprot:P0DTC2"
      hgnc "NA"
      map_id "W1_36"
      name "trimer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c25c7"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 1842.6891129030623
      y 1886.6445970176655
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 14
    source 2
    target 1
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_165"
      target_id "W1_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 15
    source 3
    target 1
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_118"
      target_id "W1_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 16
    source 1
    target 4
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_62"
      target_id "W1_128"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 17
    source 11
    target 2
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_96"
      target_id "W1_165"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 18
    source 6
    target 3
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_106"
      target_id "W1_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 19
    source 4
    target 5
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_128"
      target_id "W1_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 20
    source 7
    target 6
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_135"
      target_id "W1_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 21
    source 8
    target 7
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_26"
      target_id "W1_135"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 22
    source 9
    target 8
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_157"
      target_id "W1_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 23
    source 10
    target 9
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_5"
      target_id "W1_157"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 24
    source 12
    target 11
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_159"
      target_id "W1_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 25
    source 13
    target 12
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_36"
      target_id "W1_159"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
