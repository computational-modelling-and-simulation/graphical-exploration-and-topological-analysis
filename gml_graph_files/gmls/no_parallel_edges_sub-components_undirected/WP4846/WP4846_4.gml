# generated with VANTED V2.8.2 at Fri Mar 04 10:04:39 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:uniprot:P0DTD1"
      hgnc "NA"
      map_id "W1_59"
      name "orf1ab"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d244b"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 907.1327513263012
      y 1405.6152119257877
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_98"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f30e4"
      uniprot "NA"
    ]
    graphics [
      x 1197.0881108357953
      y 1273.5176807187881
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_97"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f262e"
      uniprot "NA"
    ]
    graphics [
      x 721.2861957278737
      y 1622.571160743554
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_22"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "aec54"
      uniprot "NA"
    ]
    graphics [
      x 682.4785239200348
      y 1365.6457532345082
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_63"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "d78c0"
      uniprot "NA"
    ]
    graphics [
      x 969.427082757294
      y 1187.8550176364622
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_45"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "c849c"
      uniprot "NA"
    ]
    graphics [
      x 769.3593447647718
      y 1361.82721605342
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_103"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f8ee8"
      uniprot "NA"
    ]
    graphics [
      x 769.6929209469188
      y 1456.076701147888
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_38"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "c3f71"
      uniprot "NA"
    ]
    graphics [
      x 1167.5526140769275
      y 1403.3509106536371
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_20"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "aceb2"
      uniprot "NA"
    ]
    graphics [
      x 1018.5523472585865
      y 1470.2957350544361
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_55"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "cec01"
      uniprot "NA"
    ]
    graphics [
      x 903.4974940029872
      y 1716.4011061519486
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_88"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ec0be"
      uniprot "NA"
    ]
    graphics [
      x 874.5242956956321
      y 1588.0622962005923
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_25"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "b6f1b"
      uniprot "NA"
    ]
    graphics [
      x 1130.5364960218797
      y 1491.8186725071666
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "c9b9f"
      uniprot "NA"
    ]
    graphics [
      x 845.9193437003925
      y 1508.7975055252468
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_152"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idba2d7d98"
      uniprot "NA"
    ]
    graphics [
      x 1056.9275177080528
      y 1579.379716843
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_152"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_21"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ae185"
      uniprot "NA"
    ]
    graphics [
      x 818.0869280994254
      y 1300.8062892850787
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_65"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "d8831"
      uniprot "NA"
    ]
    graphics [
      x 694.8134013455157
      y 1482.749308867054
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_102"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f81a2"
      uniprot "NA"
    ]
    graphics [
      x 1086.0899430756695
      y 1202.9765707645101
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_92"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "efcb8"
      uniprot "NA"
    ]
    graphics [
      x 853.9397003268914
      y 1173.6557624067293
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_68"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "dd252"
      uniprot "NA"
    ]
    graphics [
      x 790.0231505541453
      y 1216.148550691501
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q87917579"
      hgnc "NA"
      map_id "W1_12"
      name "nsp15"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a9205"
      uniprot "NA"
    ]
    graphics [
      x 716.8960723883586
      y 1048.951920637028
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_115"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id126968be"
      uniprot "NA"
    ]
    graphics [
      x 707.6341699482712
      y 897.2039644516879
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikipathways:WP4861"
      hgnc "NA"
      map_id "W1_8"
      name "Integrative_space_stress_br_response"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "a679a"
      uniprot "NA"
    ]
    graphics [
      x 720.1475288815043
      y 773.9474133861775
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:pubmed:32321524;urn:miriam:pubmed:32529116;urn:miriam:pubmed:32283108;urn:miriam:wikidata:Q94647436"
      hgnc "NA"
      map_id "W1_58"
      name "nsp12"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d0526"
      uniprot "NA"
    ]
    graphics [
      x 834.1527972500292
      y 995.1449698633963
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "PUBMED:32438371"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_132"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id768dd6a5"
      uniprot "NA"
    ]
    graphics [
      x 845.8184195925512
      y 842.8057919456488
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:pubmed:32321524;urn:miriam:pubmed:32529116;urn:miriam:pubmed:32283108;urn:miriam:wikidata:Q94647436"
      hgnc "NA"
      map_id "W1_4"
      name "nsp12"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a4ef4"
      uniprot "NA"
    ]
    graphics [
      x 862.1605621806087
      y 718.0410051153115
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q90038963"
      hgnc "NA"
      map_id "W1_66"
      name "nsp7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "db214"
      uniprot "NA"
    ]
    graphics [
      x 1207.1262526930047
      y 1015.4035748285108
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "PUBMED:32438371"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_137"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id858197a5"
      uniprot "NA"
    ]
    graphics [
      x 1326.8008589871945
      y 853.9683104522124
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_137"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q90038963"
      hgnc "NA"
      map_id "W1_81"
      name "nsp7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e64da"
      uniprot "NA"
    ]
    graphics [
      x 1431.188148673222
      y 787.4465242993926
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q87917579"
      hgnc "NA"
      map_id "W1_40"
      name "nsp16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c4ba4"
      uniprot "NA"
    ]
    graphics [
      x 501.3388128112938
      y 1482.139657399565
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "PUBMED:32511376"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_125"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id54006fd1"
      uniprot "NA"
    ]
    graphics [
      x 346.90750483692125
      y 1463.4051281407933
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_125"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q87917579"
      hgnc "NA"
      map_id "W1_18"
      name "nsp16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "abf4b"
      uniprot "NA"
    ]
    graphics [
      x 222.2380511220025
      y 1455.4318821382049
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q87917582"
      hgnc "NA"
      map_id "W1_24"
      name "nsp5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b5d9f"
      uniprot "NA"
    ]
    graphics [
      x 711.0452902750205
      y 1217.093404106471
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q90038952;urn:miriam:pubmed:32680882"
      hgnc "NA"
      map_id "W1_29"
      name "nsp1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bb606"
      uniprot "NA"
    ]
    graphics [
      x 1211.043499240736
      y 1690.2454406805252
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "PUBMED:32680882"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_164"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "ide3b4c27b"
      uniprot "NA"
    ]
    graphics [
      x 1319.341450032519
      y 1799.5546487569595
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_164"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q90038952"
      hgnc "NA"
      map_id "W1_104"
      name "nsp1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f9094"
      uniprot "NA"
    ]
    graphics [
      x 1423.9996489773564
      y 1882.6658272243412
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q94648377"
      hgnc "NA"
      map_id "W1_37"
      name "nsp13"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c38dc"
      uniprot "NA"
    ]
    graphics [
      x 950.444425419934
      y 1503.2509445193946
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_147"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idaf62af2b"
      uniprot "NA"
    ]
    graphics [
      x 994.3346488537643
      y 1370.2353306174182
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_147"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikipathways:WP4868"
      hgnc "NA"
      map_id "W1_75"
      name "Type_space_I_space_interferon_br_induction_space_and_br_signaling"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "e1943"
      uniprot "NA"
    ]
    graphics [
      x 1042.601261983982
      y 1252.564371554588
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_148"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idaf9c6f8"
      uniprot "NA"
    ]
    graphics [
      x 1067.6066829073602
      y 1096.9249740120492
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_148"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q87917581"
      hgnc "NA"
      map_id "W1_84"
      name "PL2_minus_PRO"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e9d37"
      uniprot "NA"
    ]
    graphics [
      x 990.3511902215009
      y 988.4898008599112
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "PUBMED:23943763"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_155"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "idc71222d4"
      uniprot "NA"
    ]
    graphics [
      x 1035.3068303209
      y 824.1356217797133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_155"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q87917581"
      hgnc "NA"
      map_id "W1_93"
      name "PL2_minus_PRO"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f03df"
      uniprot "NA"
    ]
    graphics [
      x 1059.9948365090938
      y 703.3869150876401
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q94648393;urn:miriam:pubmed:32283108"
      hgnc "NA"
      map_id "W1_90"
      name "ExoN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "eed95"
      uniprot "NA"
    ]
    graphics [
      x 1327.4723712538837
      y 1514.0890593869974
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "PUBMED:32938769"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_163"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "ide22650b2"
      uniprot "NA"
    ]
    graphics [
      x 1518.3322751527119
      y 1511.1980969910724
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_163"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_48"
      name "Viral_space_RNA_space_synthesis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "ca04b"
      uniprot "NA"
    ]
    graphics [
      x 1652.6107450174363
      y 1500.4285332418733
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q88656943"
      hgnc "NA"
      map_id "W1_83"
      name "nsp6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e7ad1"
      uniprot "NA"
    ]
    graphics [
      x 783.0810662196836
      y 1705.9922479056545
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "PUBMED:23943763"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_153"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "idbb7865a7"
      uniprot "NA"
    ]
    graphics [
      x 662.2746309393578
      y 1743.080391643357
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_153"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q88656943"
      hgnc "NA"
      map_id "W1_35"
      name "nsp6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c2256"
      uniprot "NA"
    ]
    graphics [
      x 570.8247061279683
      y 1673.1412483449046
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q89686805;urn:miriam:pubmed:32592996"
      hgnc "NA"
      map_id "W1_9"
      name "nsp9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a7c94"
      uniprot "NA"
    ]
    graphics [
      x 885.5666051112826
      y 1997.8750597083535
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_134"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id7a78fc75"
      uniprot "NA"
    ]
    graphics [
      x 804.3798991363543
      y 2121.0036868530933
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_117"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id1f86b6c5"
      uniprot "NA"
    ]
    graphics [
      x 864.3455280940915
      y 2176.8235758641467
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q89686805"
      hgnc "NA"
      map_id "W1_71"
      name "nsp9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "de7fa"
      uniprot "NA"
    ]
    graphics [
      x 786.9132441263876
      y 2283.6535190253335
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q89686805"
      hgnc "NA"
      map_id "W1_56"
      name "nsp9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "cf2d5"
      uniprot "NA"
    ]
    graphics [
      x 708.8273995287207
      y 2082.266246204843
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q89006922"
      hgnc "NA"
      map_id "W1_43"
      name "nsp2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c6991"
      uniprot "NA"
    ]
    graphics [
      x 1093.816937927615
      y 1396.0771676168229
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q90038956"
      hgnc "NA"
      map_id "W1_72"
      name "nsp4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e019b"
      uniprot "NA"
    ]
    graphics [
      x 1370.8355910842788
      y 1395.7184829535606
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "PUBMED:23943763"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_123"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id52141a70"
      uniprot "NA"
    ]
    graphics [
      x 1535.1356465455283
      y 1367.6295597861997
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q90038956"
      hgnc "NA"
      map_id "W1_78"
      name "nsp4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e5589"
      uniprot "NA"
    ]
    graphics [
      x 1666.786659191697
      y 1357.8173351001174
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q87917582;urn:miriam:pubmed:32529116"
      hgnc "NA"
      map_id "W1_80"
      name "3CL_minus_PRO"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e5b99"
      uniprot "NA"
    ]
    graphics [
      x 632.720142800421
      y 1456.6818297784862
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:uniprot:P0DTD1"
      hgnc "NA"
      map_id "W1_34"
      name "orf1ab"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c185d"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 655.9829514665878
      y 1292.7150220124704
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q88659350"
      hgnc "NA"
      map_id "W1_39"
      name "nsp8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c43c8"
      uniprot "NA"
    ]
    graphics [
      x 509.9733452052702
      y 1308.8361119888111
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      annotation "PUBMED:32438371"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_160"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "idd97096a5"
      uniprot "NA"
    ]
    graphics [
      x 372.8897450336434
      y 1268.8317616427364
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_160"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q88659350"
      hgnc "NA"
      map_id "W1_79"
      name "nsp8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e5b7f"
      uniprot "NA"
    ]
    graphics [
      x 255.31339532898835
      y 1256.2522079614166
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q87917572"
      hgnc "NA"
      map_id "W1_70"
      name "nsp10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "de394"
      uniprot "NA"
    ]
    graphics [
      x 567.7176713312764
      y 1832.6493075868343
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_151"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "idb8ba3d51"
      uniprot "NA"
    ]
    graphics [
      x 681.0717534327766
      y 1886.5711088689277
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_151"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      annotation "PUBMED:32511376"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_150"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "idb2e3b478"
      uniprot "NA"
    ]
    graphics [
      x 466.04990071025736
      y 1959.6345630992391
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_150"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_122"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id4f170add"
      uniprot "NA"
    ]
    graphics [
      x 432.20453644248755
      y 1858.658799333487
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikipathways:WP4860"
      hgnc "NA"
      map_id "W1_91"
      name "Hijack_space_of_br_ubiquitination"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "ef7e5"
      uniprot "NA"
    ]
    graphics [
      x 490.45467749570844
      y 1763.472258623808
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q87917572"
      hgnc "NA"
      map_id "W1_11"
      name "nsp10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a8b3c"
      uniprot "NA"
    ]
    graphics [
      x 357.86175952398605
      y 2008.0339907387704
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q87917572"
      hgnc "NA"
      map_id "W1_14"
      name "nsp10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a99b9"
      uniprot "NA"
    ]
    graphics [
      x 798.8507266206984
      y 1919.5210102175806
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:ncbigene:43740578"
      hgnc "NA"
      map_id "W1_54"
      name "orf1"
      node_subtype "GENE"
      node_type "species"
      org_id "ce268"
      uniprot "NA"
    ]
    graphics [
      x 1461.4932749972627
      y 1128.216649275813
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_121"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id3d108ea2"
      uniprot "NA"
    ]
    graphics [
      x 1584.752662307267
      y 1180.4189758056996
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_138"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id88a323b4"
      uniprot "NA"
    ]
    graphics [
      x 1615.984683115993
      y 1069.0033640665133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_138"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      annotation "PUBMED:32706371"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_161"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "iddb77d7c7"
      uniprot "NA"
    ]
    graphics [
      x 1545.4890020583566
      y 915.3794449470045
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_161"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:uniprot:Q9NYK1"
      hgnc "NA"
      map_id "W1_60"
      name "TLR7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d516b"
      uniprot "UNIPROT:Q9NYK1"
    ]
    graphics [
      x 1535.0873740261563
      y 738.8499774503114
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "PUBMED:32706371"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_2"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "a273b"
      uniprot "NA"
    ]
    graphics [
      x 1445.5277122045045
      y 624.592949781695
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikipathways:WP4868"
      hgnc "NA"
      map_id "W1_13"
      name "Type_space_I_space_interferon_br_induction_space_and_br_signaling"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "a989f"
      uniprot "NA"
    ]
    graphics [
      x 1317.2966775241048
      y 653.284886482362
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:ncbigene:43740578"
      hgnc "NA"
      map_id "W1_100"
      name "orf1"
      node_subtype "GENE"
      node_type "species"
      org_id "f7787"
      uniprot "NA"
    ]
    graphics [
      x 1721.7264393396144
      y 1037.4091653093085
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_10"
      name "Membrane_br_fusion"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "a7d58"
      uniprot "NA"
    ]
    graphics [
      x 1541.4237379545486
      y 1074.9689570898468
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      annotation "PUBMED:32944968"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_149"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idb04f1828"
      uniprot "NA"
    ]
    graphics [
      x 1479.9698000264168
      y 959.6257502098553
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_149"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q106020384"
      hgnc "NA"
      map_id "W1_33"
      name "S2_space_subunit"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bdee3"
      uniprot "NA"
    ]
    graphics [
      x 1353.765366144422
      y 915.3900460025174
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      annotation "PUBMED:32142651"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_130"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id66bc3866"
      uniprot "NA"
    ]
    graphics [
      x 1215.6927037715323
      y 875.6623411243351
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_130"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:uniprot:P0DTC2"
      hgnc "NA"
      map_id "W1_82"
      name "surface_br_glycoprotein_space_S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e7798"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 1130.419798159681
      y 1003.047719517929
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:pubmed:32142651;urn:miriam:pubmed:32662421;urn:miriam:uniprot:O15393"
      hgnc "NA"
      map_id "W1_57"
      name "TMPRSS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "cf321"
      uniprot "UNIPROT:O15393"
    ]
    graphics [
      x 1106.2107964438378
      y 871.5655040909724
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      annotation "PUBMED:32142651;PUBMED:32917722;PUBMED:32125455;PUBMED:32132184"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_126"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id596b2488"
      uniprot "NA"
    ]
    graphics [
      x 1278.2613144273835
      y 1043.7395806164545
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      annotation "PUBMED:32653452;PUBMED:32970989"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_139"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id8acd3f8f"
      uniprot "NA"
    ]
    graphics [
      x 994.8686841104108
      y 1111.3625579893057
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_139"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A28815"
      hgnc "NA"
      map_id "W1_107"
      name "heparan_space_sulfate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "fa9de"
      uniprot "NA"
    ]
    graphics [
      x 922.6261464106425
      y 1219.0522854765272
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16393"
      hgnc "NA"
      map_id "W1_114"
      name "sphingosine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "ffcdb"
      uniprot "NA"
    ]
    graphics [
      x 1357.0277187975817
      y 1124.820303080509
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:uniprot:Q9BYF1"
      hgnc "NA"
      map_id "W1_74"
      name "ACE2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e154d"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1383.793795249375
      y 1009.4085178736115
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 89
    source 2
    target 1
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_98"
      target_id "W1_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 90
    source 1
    target 3
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_59"
      target_id "W1_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 91
    source 1
    target 4
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_59"
      target_id "W1_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 92
    source 1
    target 5
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_59"
      target_id "W1_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 93
    source 1
    target 6
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_59"
      target_id "W1_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 94
    source 1
    target 7
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_59"
      target_id "W1_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 95
    source 1
    target 8
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_59"
      target_id "W1_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 96
    source 1
    target 9
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_59"
      target_id "W1_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 97
    source 1
    target 10
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_59"
      target_id "W1_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 98
    source 1
    target 11
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_59"
      target_id "W1_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 99
    source 1
    target 12
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_59"
      target_id "W1_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 100
    source 1
    target 13
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_59"
      target_id "W1_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 1
    target 14
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_59"
      target_id "W1_152"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 1
    target 15
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_59"
      target_id "W1_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 1
    target 16
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_59"
      target_id "W1_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 1
    target 17
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_59"
      target_id "W1_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 1
    target 18
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_59"
      target_id "W1_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 106
    source 1
    target 19
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_59"
      target_id "W1_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 107
    source 70
    target 2
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_54"
      target_id "W1_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 108
    source 3
    target 63
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_97"
      target_id "W1_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 109
    source 4
    target 60
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_22"
      target_id "W1_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 110
    source 5
    target 40
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_63"
      target_id "W1_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 111
    source 6
    target 59
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_45"
      target_id "W1_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 112
    source 7
    target 58
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_103"
      target_id "W1_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 113
    source 8
    target 55
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_38"
      target_id "W1_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 114
    source 9
    target 54
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_20"
      target_id "W1_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 10
    target 49
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_55"
      target_id "W1_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 11
    target 46
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_88"
      target_id "W1_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 12
    target 43
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_25"
      target_id "W1_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 13
    target 36
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_47"
      target_id "W1_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 14
    target 33
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_152"
      target_id "W1_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 15
    target 32
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_21"
      target_id "W1_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 16
    target 29
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_65"
      target_id "W1_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 17
    target 26
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_102"
      target_id "W1_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 18
    target 23
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_92"
      target_id "W1_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 19
    target 20
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_68"
      target_id "W1_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 20
    target 21
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_12"
      target_id "W1_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 126
    source 21
    target 22
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_115"
      target_id "W1_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 127
    source 23
    target 24
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_58"
      target_id "W1_132"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 128
    source 24
    target 25
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_132"
      target_id "W1_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 26
    target 27
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_66"
      target_id "W1_137"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 130
    source 27
    target 28
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_137"
      target_id "W1_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 131
    source 29
    target 30
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_40"
      target_id "W1_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 132
    source 30
    target 31
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_125"
      target_id "W1_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 133
    source 33
    target 34
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_29"
      target_id "W1_164"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 134
    source 34
    target 35
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_164"
      target_id "W1_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 135
    source 36
    target 37
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_37"
      target_id "W1_147"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 136
    source 37
    target 38
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_147"
      target_id "W1_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 137
    source 39
    target 38
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_148"
      target_id "W1_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 138
    source 40
    target 39
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_84"
      target_id "W1_148"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 139
    source 40
    target 41
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_84"
      target_id "W1_155"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 41
    target 42
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_155"
      target_id "W1_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 43
    target 44
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_90"
      target_id "W1_163"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 44
    target 45
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_163"
      target_id "W1_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 46
    target 47
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_83"
      target_id "W1_153"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 144
    source 47
    target 48
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_153"
      target_id "W1_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 145
    source 49
    target 50
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_9"
      target_id "W1_134"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 146
    source 49
    target 51
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_9"
      target_id "W1_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 147
    source 50
    target 53
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_134"
      target_id "W1_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 148
    source 51
    target 52
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_117"
      target_id "W1_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 149
    source 55
    target 56
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_72"
      target_id "W1_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 150
    source 56
    target 57
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_123"
      target_id "W1_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 151
    source 60
    target 61
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_39"
      target_id "W1_160"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 152
    source 61
    target 62
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_160"
      target_id "W1_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 153
    source 63
    target 64
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_70"
      target_id "W1_151"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 154
    source 63
    target 65
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_70"
      target_id "W1_150"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 155
    source 63
    target 66
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_70"
      target_id "W1_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 156
    source 64
    target 69
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_151"
      target_id "W1_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 157
    source 65
    target 68
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_150"
      target_id "W1_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 158
    source 66
    target 67
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_122"
      target_id "W1_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 159
    source 71
    target 70
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_121"
      target_id "W1_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 160
    source 72
    target 70
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_138"
      target_id "W1_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 161
    source 70
    target 73
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_54"
      target_id "W1_161"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 162
    source 78
    target 71
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_10"
      target_id "W1_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 163
    source 77
    target 72
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_100"
      target_id "W1_138"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 164
    source 73
    target 74
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_161"
      target_id "W1_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 165
    source 74
    target 75
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_60"
      target_id "W1_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 166
    source 75
    target 76
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_2"
      target_id "W1_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 167
    source 79
    target 78
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_149"
      target_id "W1_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 168
    source 80
    target 79
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_33"
      target_id "W1_149"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 169
    source 81
    target 80
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_130"
      target_id "W1_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 170
    source 82
    target 81
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_82"
      target_id "W1_130"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 171
    source 83
    target 81
    cd19dm [
      diagram "WP4846"
      edge_type "CATALYSIS"
      source_id "W1_57"
      target_id "W1_130"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 172
    source 82
    target 84
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_82"
      target_id "W1_126"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 173
    source 82
    target 85
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_82"
      target_id "W1_139"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 174
    source 87
    target 84
    cd19dm [
      diagram "WP4846"
      edge_type "INHIBITION"
      source_id "W1_114"
      target_id "W1_126"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 175
    source 84
    target 88
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_126"
      target_id "W1_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 176
    source 85
    target 86
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_139"
      target_id "W1_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
