# generated with VANTED V2.8.2 at Fri Mar 04 10:04:39 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:uniprot:P02679"
      hgnc "NA"
      map_id "W14_57"
      name "FGG_space_(_gamma_)"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "efb81"
      uniprot "UNIPROT:P02679"
    ]
    graphics [
      x 254.84949552288344
      y 1335.591918087834
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_70"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "faf78"
      uniprot "NA"
    ]
    graphics [
      x 140.9352772457596
      y 1288.8055788980655
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:pubchem.compound:462382"
      hgnc "NA"
      map_id "W14_52"
      name "MG132"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "e9c4f"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 1191.1273421389678
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:ncbigene:2266"
      hgnc "NA"
      map_id "W14_18"
      name "FGG"
      node_subtype "GENE"
      node_type "species"
      org_id "b543a"
      uniprot "NA"
    ]
    graphics [
      x 161.65983521858732
      y 1163.0582509234725
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_19"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "b8553"
      uniprot "NA"
    ]
    graphics [
      x 77.2542208732932
      y 1062.2580989654475
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:uniprot:P02679"
      hgnc "NA"
      map_id "W14_69"
      name "FGG_space_(_gamma_)"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f949f"
      uniprot "UNIPROT:P02679"
    ]
    graphics [
      x 106.75995645091837
      y 924.4543202914175
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_15"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "b068e"
      uniprot "NA"
    ]
    graphics [
      x 190.8151044594349
      y 813.5411371353737
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:uniprot:P02679;urn:miriam:uniprot:P02671"
      hgnc "NA"
      map_id "W14_49"
      name "dea90"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "dea90"
      uniprot "UNIPROT:P02679;UNIPROT:P02671"
    ]
    graphics [
      x 264.88439851078135
      y 902.3637690592378
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A31206;urn:miriam:obo.chebi:CHEBI%3A6426;urn:miriam:obo.chebi:CHEBI%3A3638"
      hgnc "NA"
      map_id "W14_30"
      name "c43f2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c43f2"
      uniprot "NA"
    ]
    graphics [
      x 323.6043919406634
      y 812.4283669959219
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_73"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id5e482a09"
      uniprot "NA"
    ]
    graphics [
      x 376.87049307975633
      y 934.4656324983335
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:uniprot:P02671"
      hgnc "NA"
      map_id "W14_1"
      name "FGA_space_(A_alpha_)"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a38e8"
      uniprot "UNIPROT:P02671"
    ]
    graphics [
      x 427.70195575081607
      y 1063.932476442536
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_74"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idc381d3da"
      uniprot "NA"
    ]
    graphics [
      x 466.3840592878206
      y 1194.2047097081554
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:pubchem.compound:462382;urn:miriam:obo.chebi:CHEBI%3A6426"
      hgnc "NA"
      map_id "W14_4"
      name "a4d07"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "a4d07"
      uniprot "NA"
    ]
    graphics [
      x 519.9464307871776
      y 1306.4131749748976
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:ncbigene:2243"
      hgnc "NA"
      map_id "W14_56"
      name "FGA"
      node_subtype "GENE"
      node_type "species"
      org_id "ef261"
      uniprot "NA"
    ]
    graphics [
      x 582.7917175500932
      y 1222.3483216080033
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 15
    source 1
    target 2
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "W14_57"
      target_id "W14_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 16
    source 3
    target 2
    cd19dm [
      diagram "WP4927"
      edge_type "INHIBITION"
      source_id "W14_52"
      target_id "W14_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 17
    source 2
    target 4
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_70"
      target_id "W14_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 18
    source 3
    target 5
    cd19dm [
      diagram "WP4927"
      edge_type "INHIBITION"
      source_id "W14_52"
      target_id "W14_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 19
    source 5
    target 4
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_19"
      target_id "W14_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 20
    source 6
    target 5
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "W14_69"
      target_id "W14_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 21
    source 7
    target 6
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_15"
      target_id "W14_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 22
    source 8
    target 7
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "W14_49"
      target_id "W14_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 23
    source 9
    target 7
    cd19dm [
      diagram "WP4927"
      edge_type "INHIBITION"
      source_id "W14_30"
      target_id "W14_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 24
    source 8
    target 10
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "W14_49"
      target_id "W14_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 25
    source 9
    target 10
    cd19dm [
      diagram "WP4927"
      edge_type "INHIBITION"
      source_id "W14_30"
      target_id "W14_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 26
    source 10
    target 11
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_73"
      target_id "W14_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 27
    source 11
    target 12
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "W14_1"
      target_id "W14_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 28
    source 13
    target 12
    cd19dm [
      diagram "WP4927"
      edge_type "INHIBITION"
      source_id "W14_4"
      target_id "W14_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 29
    source 12
    target 14
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_74"
      target_id "W14_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
