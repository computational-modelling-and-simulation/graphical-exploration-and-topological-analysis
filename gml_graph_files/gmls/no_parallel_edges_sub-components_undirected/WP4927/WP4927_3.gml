# generated with VANTED V2.8.2 at Fri Mar 04 10:04:39 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:uniprot:P02675"
      hgnc "NA"
      map_id "W14_34"
      name "FGB_space_(B_beta_)"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "cab41"
      uniprot "UNIPROT:P02675"
    ]
    graphics [
      x 1039.247051187757
      y 1552.2227414968793
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:11460506"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_51"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "e202f"
      uniprot "NA"
    ]
    graphics [
      x 1001.900403707944
      y 1667.9859906177503
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A52722;urn:miriam:pubchem.compound:462382"
      hgnc "NA"
      map_id "W14_55"
      name "ef073"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ef073"
      uniprot "NA"
    ]
    graphics [
      x 1111.1565555651855
      y 1616.11084796583
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:ncbigene:2244"
      hgnc "NA"
      map_id "W14_72"
      name "FGB"
      node_subtype "GENE"
      node_type "species"
      org_id "fef9d"
      uniprot "NA"
    ]
    graphics [
      x 943.6073657472648
      y 1564.4504869781967
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 5
    source 1
    target 2
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "W14_34"
      target_id "W14_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 6
    source 3
    target 2
    cd19dm [
      diagram "WP4927"
      edge_type "INHIBITION"
      source_id "W14_55"
      target_id "W14_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 7
    source 2
    target 4
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_51"
      target_id "W14_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
