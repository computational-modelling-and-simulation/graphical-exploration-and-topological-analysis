# generated with VANTED V2.8.2 at Fri Mar 04 09:53:02 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:interpro:IPR002552"
      hgnc "NA"
      map_id "M18_188"
      name "S2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2188"
      uniprot "NA"
    ]
    graphics [
      x 687.7208106892465
      y 450.84188800997856
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_188"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:32047258"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_121"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re981"
      uniprot "NA"
    ]
    graphics [
      x 458.8332059297777
      y 748.8264371615426
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:interpro:IPR002552"
      hgnc "NA"
      map_id "M18_164"
      name "S2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2052"
      uniprot "NA"
    ]
    graphics [
      x 302.2633139207459
      y 1071.3497434387996
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_164"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_173"
      name "Orf7a_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2105"
      uniprot "NA"
    ]
    graphics [
      x 1648.4568583513192
      y 1392.2450198911704
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_173"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_44"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1025"
      uniprot "NA"
    ]
    graphics [
      x 1527.1188539082477
      y 1203.2557328890107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0070992"
      hgnc "NA"
      map_id "M18_14"
      name "Host_space_translation_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa428"
      uniprot "NA"
    ]
    graphics [
      x 1514.8394963027954
      y 985.1775331194128
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740573;urn:miriam:uniprot:P0DTC7"
      hgnc "NA"
      map_id "M18_146"
      name "Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1875"
      uniprot "UNIPROT:P0DTC7"
    ]
    graphics [
      x 1349.0144261436112
      y 1261.4221284689074
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_146"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_135"
      name "N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1685"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 836.7482985365097
      y 1819.632560916712
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "PUBMED:28720894"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_114"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re932"
      uniprot "NA"
    ]
    graphics [
      x 1052.657045962059
      y 1724.230104654574
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_151"
      name "N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1887"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 1242.1582020262558
      y 1712.753972015886
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_151"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_174"
      name "Orf7b_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2106"
      uniprot "NA"
    ]
    graphics [
      x 1889.1058058631577
      y 1145.5364385431371
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_174"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_50"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1031"
      uniprot "NA"
    ]
    graphics [
      x 1766.7239503540136
      y 1176.0133061670658
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740574;urn:miriam:uniprot:P0DTD8"
      hgnc "NA"
      map_id "M18_147"
      name "Orf7b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1876"
      uniprot "UNIPROT:P0DTD8"
    ]
    graphics [
      x 1807.556723287961
      y 1409.88663496394
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_147"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_3"
      name "Nucleocapsid"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa365"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 158.85583220354488
      y 1321.1091761242296
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "PUBMED:32047258;PUBMED:32142651"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_120"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re979"
      uniprot "NA"
    ]
    graphics [
      x 207.60067505811048
      y 1437.1652034428112
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570;urn:miriam:hgnc.symbol:E;urn:miriam:uniprot:P0DTC4"
      hgnc "HGNC_SYMBOL:E"
      map_id "M18_167"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2062"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 93.39603089285629
      y 1347.2455771098928
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_167"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740573;urn:miriam:uniprot:P0DTC7"
      hgnc "NA"
      map_id "M18_165"
      name "Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2060"
      uniprot "UNIPROT:P0DTC7"
    ]
    graphics [
      x 88.94172682312148
      y 1523.7747106512231
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_165"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "M18_166"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2061"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 72.31561608016546
      y 1406.8246584688054
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_166"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_5"
      name "Nucleocapsid"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa369"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 591.1750739414997
      y 1683.3658510570613
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740573;urn:miriam:uniprot:P0DTC7"
      hgnc "NA"
      map_id "M18_182"
      name "Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2114"
      uniprot "UNIPROT:P0DTC7"
    ]
    graphics [
      x 150.55959203574946
      y 1552.3662931927402
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_182"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570;urn:miriam:hgnc.symbol:E;urn:miriam:uniprot:P0DTC4"
      hgnc "HGNC_SYMBOL:E"
      map_id "M18_183"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2115"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 62.5
      y 1466.7273316074047
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_183"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "M18_184"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2116"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 391.3272700197257
      y 1448.804098009266
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_184"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725306"
      hgnc "NA"
      map_id "M18_192"
      name "Nsp10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2200"
      uniprot "NA"
    ]
    graphics [
      x 1250.8100793373385
      y 1134.9260819958363
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_192"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "PUBMED:11907209;PUBMED:30632963"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_97"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1112"
      uniprot "NA"
    ]
    graphics [
      x 1043.1755701086022
      y 1166.1974630607742
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725311"
      hgnc "NA"
      map_id "M18_191"
      name "Nsp16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2199"
      uniprot "NA"
    ]
    graphics [
      x 1138.9437198037674
      y 1100.4880660442489
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_191"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725310"
      hgnc "NA"
      map_id "M18_197"
      name "Nsp15"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2205"
      uniprot "NA"
    ]
    graphics [
      x 1207.8630155348362
      y 1034.2205722723256
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_197"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725309"
      hgnc "NA"
      map_id "M18_198"
      name "Nsp14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2206"
      uniprot "NA"
    ]
    graphics [
      x 1192.1868705112543
      y 1119.542057446018
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_198"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725308"
      hgnc "NA"
      map_id "M18_190"
      name "Nsp13"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2198"
      uniprot "NA"
    ]
    graphics [
      x 1135.8071447156742
      y 1286.8278717522612
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_190"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725307"
      hgnc "NA"
      map_id "M18_189"
      name "Nsp12"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2197"
      uniprot "NA"
    ]
    graphics [
      x 1177.9553060625637
      y 1325.684527976293
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_189"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725305"
      hgnc "NA"
      map_id "M18_193"
      name "Nsp9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2201"
      uniprot "NA"
    ]
    graphics [
      x 1204.3469900945508
      y 1181.8329870650218
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_193"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725304"
      hgnc "NA"
      map_id "M18_194"
      name "Nsp8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2202"
      uniprot "NA"
    ]
    graphics [
      x 1153.8235411079577
      y 1211.1984794491916
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_194"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725303"
      hgnc "NA"
      map_id "M18_195"
      name "Nsp7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2203"
      uniprot "NA"
    ]
    graphics [
      x 1191.3283511347952
      y 1258.5222605820863
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_195"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725302"
      hgnc "NA"
      map_id "M18_196"
      name "Nsp6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2204"
      uniprot "NA"
    ]
    graphics [
      x 975.6564450007718
      y 1239.8339104307884
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_196"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725299;urn:miriam:uniprot:Nsp3"
      hgnc "NA"
      map_id "M18_203"
      name "Nsp3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2222"
      uniprot "UNIPROT:Nsp3"
    ]
    graphics [
      x 720.3679722795911
      y 1196.810622950743
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_203"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725300"
      hgnc "NA"
      map_id "M18_205"
      name "Nsp4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2228"
      uniprot "NA"
    ]
    graphics [
      x 783.1286028972847
      y 1202.947976655109
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_205"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725302"
      hgnc "NA"
      map_id "M18_281"
      name "Nsp6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2367"
      uniprot "NA"
    ]
    graphics [
      x 846.2286722917
      y 1352.2155105628647
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_281"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725303"
      hgnc "NA"
      map_id "M18_277"
      name "Nsp7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2363"
      uniprot "NA"
    ]
    graphics [
      x 1046.4223355503482
      y 1394.3487912297683
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_277"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725304"
      hgnc "NA"
      map_id "M18_276"
      name "Nsp8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2362"
      uniprot "NA"
    ]
    graphics [
      x 922.9033630074364
      y 1322.409449072663
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_276"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725305"
      hgnc "NA"
      map_id "M18_275"
      name "Nsp9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2361"
      uniprot "NA"
    ]
    graphics [
      x 1001.0491147230448
      y 1363.178934176397
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_275"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725306"
      hgnc "NA"
      map_id "M18_274"
      name "Nsp10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2360"
      uniprot "NA"
    ]
    graphics [
      x 942.2309914278025
      y 1379.4705886574989
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_274"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725298"
      hgnc "NA"
      map_id "M18_200"
      name "Nsp2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2217"
      uniprot "NA"
    ]
    graphics [
      x 1431.9824474215395
      y 1269.5577070522795
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_200"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725298"
      hgnc "NA"
      map_id "M18_271"
      name "Nsp2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2357"
      uniprot "NA"
    ]
    graphics [
      x 899.9078371540492
      y 916.8230515950243
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_271"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_12"
      name "Replication_space_transcription_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa410"
      uniprot "NA"
    ]
    graphics [
      x 1579.420824718929
      y 873.9757351979426
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_6"
      name "Nucleocapsid"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa374"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 882.0951704639355
      y 2257.2355533129166
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "PUBMED:32047258;PUBMED:32142651;PUBMED:32944968;PUBMED:32094589"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_109"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re855"
      uniprot "NA"
    ]
    graphics [
      x 963.7853431210148
      y 2117.8036604061026
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570;urn:miriam:hgnc.symbol:E;urn:miriam:uniprot:P0DTC4"
      hgnc "HGNC_SYMBOL:E"
      map_id "M18_160"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2023"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 902.1598825045292
      y 2183.084663190917
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_160"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "M18_161"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2024"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 1114.7658830038577
      y 2209.9697420902685
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_161"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740573;urn:miriam:uniprot:P0DTC7"
      hgnc "NA"
      map_id "M18_162"
      name "Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2025"
      uniprot "UNIPROT:P0DTC7"
    ]
    graphics [
      x 1077.0383176895987
      y 1995.8036644984948
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_162"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:interpro:IPR002552"
      hgnc "NA"
      map_id "M18_132"
      name "S2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1601"
      uniprot "NA"
    ]
    graphics [
      x 1153.0222170229504
      y 2108.3159289044615
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16113"
      hgnc "NA"
      map_id "M18_286"
      name "cholesterol"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa2372"
      uniprot "NA"
    ]
    graphics [
      x 970.7457111665489
      y 2297.6149921936303
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_286"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:interpro:IPR002551;urn:miriam:ncbigene:8829;urn:miriam:ncbigene:8829;urn:miriam:hgnc:8004;urn:miriam:uniprot:O14786;urn:miriam:uniprot:O14786;urn:miriam:ensembl:ENSG00000099250;urn:miriam:refseq:NM_001024628;urn:miriam:hgnc.symbol:NRP1;urn:miriam:hgnc.symbol:NRP1"
      hgnc "HGNC_SYMBOL:NRP1"
      map_id "M18_21"
      name "S1:NRP1_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa441"
      uniprot "UNIPROT:O14786"
    ]
    graphics [
      x 938.2932415369105
      y 2250.9223281930613
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570;urn:miriam:hgnc.symbol:E;urn:miriam:uniprot:P0DTC4"
      hgnc "HGNC_SYMBOL:E"
      map_id "M18_169"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2065"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 1040.3057944748375
      y 2067.725500549727
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_169"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "M18_170"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2066"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 1053.689551041355
      y 2265.4247661054255
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_170"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740573;urn:miriam:uniprot:P0DTC7"
      hgnc "NA"
      map_id "M18_171"
      name "Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2067"
      uniprot "UNIPROT:P0DTC7"
    ]
    graphics [
      x 1088.461358762334
      y 2118.0172958251096
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_171"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_263"
      name "Orf8_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2349"
      uniprot "NA"
    ]
    graphics [
      x 2001.3743898255723
      y 690.0205107870293
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_263"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_82"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1095"
      uniprot "NA"
    ]
    graphics [
      x 1941.3250526333309
      y 854.0550353605498
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_229"
      name "Orf8_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2299"
      uniprot "NA"
    ]
    graphics [
      x 1882.4454843447643
      y 1074.28976559176
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_229"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_175"
      name "Orf8_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2107"
      uniprot "NA"
    ]
    graphics [
      x 1845.2404549142598
      y 1022.0917663290771
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_175"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725300;urn:miriam:ncbiprotein:YP_009725299"
      hgnc "NA"
      map_id "M18_213"
      name "Nsp3_minus_4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2243"
      uniprot "NA"
    ]
    graphics [
      x 874.7376299984451
      y 1481.9357518488891
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_213"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_93"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1108"
      uniprot "NA"
    ]
    graphics [
      x 738.1429188595164
      y 1352.5586254519023
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_237"
      name "Orf9b_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2320"
      uniprot "NA"
    ]
    graphics [
      x 1623.2719850516407
      y 666.6640195926876
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_237"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_72"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1084"
      uniprot "NA"
    ]
    graphics [
      x 1504.5454918977075
      y 770.589643491878
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_264"
      name "Orf9b_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2350"
      uniprot "NA"
    ]
    graphics [
      x 1319.6625281857387
      y 911.3055458378635
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_264"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_102"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1120"
      uniprot "NA"
    ]
    graphics [
      x 1602.0126940502068
      y 1143.0419288763837
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_11"
      name "Replication_space_transcription_space_complex:N_space_oligomer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa398"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 1596.6796501647877
      y 1432.2023225839257
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "PUBMED:28720894"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_52"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1061"
      uniprot "NA"
    ]
    graphics [
      x 1432.2414300752694
      y 1603.9972121280505
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:interpro:IPR002551"
      hgnc "NA"
      map_id "M18_129"
      name "S1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1539"
      uniprot "NA"
    ]
    graphics [
      x 770.2869734398877
      y 1958.4223351532
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_129"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      annotation "PUBMED:33082294;PUBMED:33082293"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_104"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1124"
      uniprot "NA"
    ]
    graphics [
      x 800.5363676186241
      y 2201.147944100212
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:8829;urn:miriam:ncbigene:8829;urn:miriam:hgnc:8004;urn:miriam:uniprot:O14786;urn:miriam:uniprot:O14786;urn:miriam:ensembl:ENSG00000099250;urn:miriam:refseq:NM_001024628;urn:miriam:hgnc.symbol:NRP1;urn:miriam:hgnc.symbol:NRP1"
      hgnc "HGNC_SYMBOL:NRP1"
      map_id "M18_285"
      name "NRP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2371"
      uniprot "UNIPROT:O14786"
    ]
    graphics [
      x 879.9182875744151
      y 2073.17760272183
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_285"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570"
      hgnc "NA"
      map_id "M18_243"
      name "E_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2326"
      uniprot "NA"
    ]
    graphics [
      x 1403.1099285207133
      y 830.3506255807758
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_243"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      annotation "PUBMED:11142;PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_66"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1078"
      uniprot "NA"
    ]
    graphics [
      x 1449.5764674013542
      y 1010.6521050184197
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_258"
      name "E_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2344"
      uniprot "NA"
    ]
    graphics [
      x 1433.6115231017698
      y 1218.7456689173853
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_258"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTD2"
      hgnc "NA"
      map_id "M18_149"
      name "Orf9b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1878"
      uniprot "UNIPROT:P0DTD2"
    ]
    graphics [
      x 1131.987402175179
      y 807.5317597823762
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_149"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_37"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1018"
      uniprot "NA"
    ]
    graphics [
      x 1003.1261739109168
      y 826.7852947685537
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTD2"
      hgnc "NA"
      map_id "M18_219"
      name "Orf9b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2249"
      uniprot "UNIPROT:P0DTD2"
    ]
    graphics [
      x 988.3837588983916
      y 938.1788962466717
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_219"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_239"
      name "Orf3a_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2322"
      uniprot "NA"
    ]
    graphics [
      x 1511.0931997225332
      y 497.6992672534923
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_239"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_70"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1082"
      uniprot "NA"
    ]
    graphics [
      x 1585.4505200292003
      y 609.8988301267652
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_262"
      name "Orf3a_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2348"
      uniprot "NA"
    ]
    graphics [
      x 1585.6088696563484
      y 743.3670004287123
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_262"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571"
      hgnc "NA"
      map_id "M18_244"
      name "M_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2327"
      uniprot "NA"
    ]
    graphics [
      x 1392.0640297541531
      y 620.0327001785051
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_244"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_65"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1077"
      uniprot "NA"
    ]
    graphics [
      x 1289.181564534126
      y 731.4871572027973
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_257"
      name "M_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2343"
      uniprot "NA"
    ]
    graphics [
      x 1038.1321502117544
      y 678.4964085580966
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_257"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      annotation "PUBMED:28484023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_96"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re1111"
      uniprot "NA"
    ]
    graphics [
      x 1264.5978693946072
      y 808.8799945170786
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0071360"
      hgnc "NA"
      map_id "M18_222"
      name "cellular_space_response_space_to_space_exogenous_space_dsRNA"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa2291"
      uniprot "NA"
    ]
    graphics [
      x 1294.2214504366036
      y 627.4513500588253
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_222"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M18_214"
      name "pp1ab_space_Nsp3_minus_16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2244"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1761.4741756960693
      y 1721.5927055982688
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_214"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      annotation "PUBMED:21203998;PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_26"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1006"
      uniprot "NA"
    ]
    graphics [
      x 1534.394864095278
      y 1634.9666110874987
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M18_206"
      name "pp1ab_space_nsp6_minus_16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2229"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1426.1547339774477
      y 1449.2432623910079
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_206"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725301"
      hgnc "NA"
      map_id "M18_211"
      name "Nsp5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2241"
      uniprot "NA"
    ]
    graphics [
      x 1480.355565303213
      y 1442.7131720156376
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_211"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725300;urn:miriam:ncbiprotein:YP_009725299"
      hgnc "NA"
      map_id "M18_212"
      name "Nsp3_minus_4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2242"
      uniprot "NA"
    ]
    graphics [
      x 1316.0745583815194
      y 1669.6845328745646
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_212"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_225"
      name "S_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2295"
      uniprot "NA"
    ]
    graphics [
      x 2098.2782591352197
      y 1057.6915683366433
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_225"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_56"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1067"
      uniprot "NA"
    ]
    graphics [
      x 2273.793860913887
      y 1085.5889305818043
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_248"
      name "sa2295_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2332"
      uniprot "NA"
    ]
    graphics [
      x 2339.350887091814
      y 1184.3716258391253
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_248"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740575"
      hgnc "NA"
      map_id "M18_267"
      name "N_space__space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2353"
      uniprot "NA"
    ]
    graphics [
      x 1516.3930946388152
      y 882.6961453063732
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_267"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_86"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1099"
      uniprot "NA"
    ]
    graphics [
      x 1418.6041826790643
      y 1105.7050317719459
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_268"
      name "N_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2354"
      uniprot "NA"
    ]
    graphics [
      x 1196.447497130111
      y 1475.1149672295605
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_268"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "M18_138"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1688"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 2006.60013776483
      y 1446.976087477995
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_138"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_32"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1013"
      uniprot "NA"
    ]
    graphics [
      x 2052.2662836925765
      y 1652.3795688759872
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "M18_154"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1893"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 1949.9010602247195
      y 1816.3166570822664
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_154"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_77"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1090"
      uniprot "NA"
    ]
    graphics [
      x 1417.7029685295097
      y 1509.3685771564276
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570"
      hgnc "NA"
      map_id "M18_224"
      name "E_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2294"
      uniprot "NA"
    ]
    graphics [
      x 1475.8378062918096
      y 1726.713948724552
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_224"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_179"
      name "E_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2111"
      uniprot "NA"
    ]
    graphics [
      x 1327.448587991545
      y 1547.4337561124626
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_179"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_240"
      name "Orf6_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2323"
      uniprot "NA"
    ]
    graphics [
      x 1617.0126608998025
      y 793.0492314729827
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_240"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_69"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1081"
      uniprot "NA"
    ]
    graphics [
      x 1730.0947135630718
      y 835.6571944193664
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_261"
      name "Orf6_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2347"
      uniprot "NA"
    ]
    graphics [
      x 1758.9735181780597
      y 720.9460464374172
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_261"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M18_140"
      name "pp1ab"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1790"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1475.6459734837529
      y 1310.1753972956787
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_140"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      annotation "PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_88"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1102"
      uniprot "NA"
    ]
    graphics [
      x 1794.39280977011
      y 1339.28190768384
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M18_202"
      name "pp1ab_space_Nsp3_minus_16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2221"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1931.9754101909546
      y 1522.4547806895948
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_202"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725297"
      hgnc "NA"
      map_id "M18_199"
      name "Nsp1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2216"
      uniprot "NA"
    ]
    graphics [
      x 2060.2901590038696
      y 1214.624506433135
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_199"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740575"
      hgnc "NA"
      map_id "M18_157"
      name "N_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa1962"
      uniprot "NA"
    ]
    graphics [
      x 820.9303751525988
      y 1898.3069269945659
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_157"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_115"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re940"
      uniprot "NA"
    ]
    graphics [
      x 619.382797673568
      y 1941.6218017166393
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0070992"
      hgnc "NA"
      map_id "M18_15"
      name "Host_space_translation_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa429"
      uniprot "NA"
    ]
    graphics [
      x 524.1618585867227
      y 1985.7884485650293
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0005783"
      hgnc "NA"
      map_id "M18_19"
      name "Endoplasmic_space_reticulum"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa439"
      uniprot "NA"
    ]
    graphics [
      x 568.4721411917567
      y 1228.1180603355365
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      annotation "PUBMED:23943763"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_123"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re999"
      uniprot "NA"
    ]
    graphics [
      x 691.3510220349792
      y 1283.9001192465357
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0039718"
      hgnc "NA"
      map_id "M18_18"
      name "Double_minus_membrane_space_vesicle"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa438"
      uniprot "NA"
    ]
    graphics [
      x 567.4605572479666
      y 1305.6179680413532
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_76"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1089"
      uniprot "NA"
    ]
    graphics [
      x 821.0964772339681
      y 607.6532430189038
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571"
      hgnc "NA"
      map_id "M18_223"
      name "M_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2292"
      uniprot "NA"
    ]
    graphics [
      x 644.2481112258776
      y 510.11556923804346
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_223"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_181"
      name "M_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2113"
      uniprot "NA"
    ]
    graphics [
      x 975.5775636840006
      y 638.0285102225516
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_181"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_81"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1094"
      uniprot "NA"
    ]
    graphics [
      x 1634.3773212297892
      y 913.4531294546146
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_228"
      name "Orf3a_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2298"
      uniprot "NA"
    ]
    graphics [
      x 1827.0505667309499
      y 932.6281239182217
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_228"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_172"
      name "Orf3a_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2104"
      uniprot "NA"
    ]
    graphics [
      x 1455.453474297872
      y 865.4775757889004
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_172"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_20"
      name "Replication_space_transcription_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa440"
      uniprot "NA"
    ]
    graphics [
      x 1732.7485527858366
      y 1384.376585924893
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_101"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1119"
      uniprot "NA"
    ]
    graphics [
      x 1687.7498136868037
      y 1114.8971704154455
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:refseq:NC_045512"
      hgnc "NA"
      map_id "M18_245"
      name "(_minus_)ss_space_gRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2328"
      uniprot "NA"
    ]
    graphics [
      x 1464.2486736245814
      y 924.0954012178443
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_245"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_64"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1076"
      uniprot "NA"
    ]
    graphics [
      x 1569.9128064121237
      y 1060.872329574142
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:refseq:NC_045512"
      hgnc "NA"
      map_id "M18_256"
      name "ds_space_gRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2342"
      uniprot "NA"
    ]
    graphics [
      x 1574.4561786896902
      y 1313.0138785626177
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_256"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_87"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1100"
      uniprot "NA"
    ]
    graphics [
      x 958.9169522287534
      y 1780.1388794224656
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_269"
      name "N_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2355"
      uniprot "NA"
    ]
    graphics [
      x 788.0889333680461
      y 2006.2186263596143
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_269"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_83"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1096"
      uniprot "NA"
    ]
    graphics [
      x 1091.2428116303838
      y 960.7209269902122
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_230"
      name "Orf9b_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2300"
      uniprot "NA"
    ]
    graphics [
      x 881.0438960843692
      y 982.3522792459355
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_230"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_177"
      name "Orf9b_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2109"
      uniprot "NA"
    ]
    graphics [
      x 1183.5592095189975
      y 952.9654892683185
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_177"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC8;urn:miriam:ncbigene:43740577"
      hgnc "NA"
      map_id "M18_150"
      name "Orf8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1879"
      uniprot "UNIPROT:P0DTC8"
    ]
    graphics [
      x 1699.0570217428
      y 1508.3245888805538
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_150"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1017"
      uniprot "NA"
    ]
    graphics [
      x 1678.5564519768618
      y 1737.0191514442631
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC8;urn:miriam:ncbigene:43740577"
      hgnc "NA"
      map_id "M18_218"
      name "Orf8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2248"
      uniprot "UNIPROT:P0DTC8"
    ]
    graphics [
      x 1575.0028146918874
      y 1871.7967902393934
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_218"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_41"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1022"
      uniprot "NA"
    ]
    graphics [
      x 1175.9978096512318
      y 709.5342840200441
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "M18_136"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1686"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 1022.529377942725
      y 489.7657137105557
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_133"
      name "N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1667"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 450.9764410418312
      y 1302.937128012825
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_133"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_113"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re908"
      uniprot "NA"
    ]
    graphics [
      x 307.8148730172246
      y 1209.1194849222925
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_155"
      name "s2919"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa1920"
      uniprot "NA"
    ]
    graphics [
      x 194.97918426390254
      y 1144.014117354564
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_155"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:refseq:NC_045512"
      hgnc "NA"
      map_id "M18_134"
      name "(_plus_)ss_space_gRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa1675"
      uniprot "NA"
    ]
    graphics [
      x 944.8736646900454
      y 1169.5780061283313
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_99"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1115"
      uniprot "NA"
    ]
    graphics [
      x 1092.0577793633722
      y 1247.7812578549317
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0070992"
      hgnc "NA"
      map_id "M18_13"
      name "Host_space_translation_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa427"
      uniprot "NA"
    ]
    graphics [
      x 887.3661729286808
      y 1152.7128558089514
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_238"
      name "Orf8_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2321"
      uniprot "NA"
    ]
    graphics [
      x 1705.9451755080408
      y 569.7000751201291
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_238"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_71"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1083"
      uniprot "NA"
    ]
    graphics [
      x 1841.841568339736
      y 664.1568607806057
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725300;urn:miriam:ncbiprotein:YP_009725299"
      hgnc "NA"
      map_id "M18_273"
      name "Nsp3_minus_4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2359"
      uniprot "NA"
    ]
    graphics [
      x 738.6433903717106
      y 1123.4652702264307
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_273"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      annotation "PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_91"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1106"
      uniprot "NA"
    ]
    graphics [
      x 606.3932134589031
      y 1021.3365546479446
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725300;urn:miriam:ncbiprotein:YP_009725299"
      hgnc "NA"
      map_id "M18_279"
      name "Nsp3_minus_4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2365"
      uniprot "NA"
    ]
    graphics [
      x 487.0424001314059
      y 1014.2334841781644
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_279"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      annotation "PUBMED:21203998"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_25"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1004"
      uniprot "NA"
    ]
    graphics [
      x 1928.045289150001
      y 1707.1017800366662
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_231"
      name "Orf14_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2301"
      uniprot "NA"
    ]
    graphics [
      x 761.5749718077057
      y 489.48673048970215
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_231"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_62"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1074"
      uniprot "NA"
    ]
    graphics [
      x 570.1424278826837
      y 545.5555181925367
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_254"
      name "sa2301_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2339"
      uniprot "NA"
    ]
    graphics [
      x 431.8926214376654
      y 547.561610932067
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_254"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      annotation "PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_24"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1002"
      uniprot "NA"
    ]
    graphics [
      x 1093.3407765112647
      y 1612.7396452814476
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_100"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1118"
      uniprot "NA"
    ]
    graphics [
      x 613.9798273920808
      y 2111.4105423495894
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_282"
      name "sa2355_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2368"
      uniprot "NA"
    ]
    graphics [
      x 483.88170427534214
      y 2078.240582260357
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_282"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M18_210"
      name "pp1a_space_Nsp3_minus_11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2240"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 968.5114833708416
      y 1086.8341111327718
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_210"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      annotation "PUBMED:21203998;PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_122"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re989"
      uniprot "NA"
    ]
    graphics [
      x 840.0999161632473
      y 1276.487068566354
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTC1;urn:miriam:ec-code:3.4.22.-;urn:miriam:ncbigene:43740578"
      hgnc "NA"
      map_id "M18_204"
      name "pp1a_space_Nsp6_minus_11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2224"
      uniprot "UNIPROT:P0DTC1"
    ]
    graphics [
      x 795.1287148205433
      y 1461.8740451608448
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_204"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725301"
      hgnc "NA"
      map_id "M18_280"
      name "Nsp5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2366"
      uniprot "NA"
    ]
    graphics [
      x 975.0034071285016
      y 1458.7252863621256
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_280"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_42"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1023"
      uniprot "NA"
    ]
    graphics [
      x 1324.9031959399115
      y 1343.2769631485655
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570;urn:miriam:hgnc.symbol:E;urn:miriam:uniprot:P0DTC4"
      hgnc "HGNC_SYMBOL:E"
      map_id "M18_137"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1687"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 1101.9106836133872
      y 1498.2436949323549
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_137"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_30"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1011"
      uniprot "NA"
    ]
    graphics [
      x 857.2250286334622
      y 326.3676798284465
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "M18_152"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1891"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 715.3621134466456
      y 223.7728625649779
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_152"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      annotation "PUBMED:11907209"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_94"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1109"
      uniprot "NA"
    ]
    graphics [
      x 937.8944779222106
      y 1523.1504899207962
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725312"
      hgnc "NA"
      map_id "M18_278"
      name "Nsp11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2364"
      uniprot "NA"
    ]
    graphics [
      x 970.3908200942759
      y 1658.9124195468478
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_278"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_227"
      name "Orf6_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2297"
      uniprot "NA"
    ]
    graphics [
      x 1715.2223968577268
      y 686.1700141115659
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_227"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_58"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1069"
      uniprot "NA"
    ]
    graphics [
      x 1869.9460515846565
      y 782.564781512606
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_250"
      name "sa2297_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2334"
      uniprot "NA"
    ]
    graphics [
      x 1949.7137396020448
      y 898.4909556927543
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_250"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570;urn:miriam:hgnc.symbol:E;urn:miriam:uniprot:P0DTC4"
      hgnc "HGNC_SYMBOL:E"
      map_id "M18_153"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1892"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 664.3425379984049
      y 1588.9934034269488
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_153"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 167
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_28"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1009"
      uniprot "NA"
    ]
    graphics [
      x 482.02768740230067
      y 1653.5893545932395
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 168
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570;urn:miriam:hgnc.symbol:E;urn:miriam:uniprot:P0DTC4"
      hgnc "HGNC_SYMBOL:E"
      map_id "M18_142"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1858"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 366.4267633878443
      y 1701.3554521036483
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_142"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 169
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_226"
      name "Orf7a_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2296"
      uniprot "NA"
    ]
    graphics [
      x 1413.6004036285804
      y 1737.27913962279
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_226"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 170
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_57"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1068"
      uniprot "NA"
    ]
    graphics [
      x 1224.829753657864
      y 1838.1331130024737
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 171
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_249"
      name "sa2296_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2333"
      uniprot "NA"
    ]
    graphics [
      x 1097.9597503585348
      y 1892.179826276255
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_249"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 172
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_39"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1020"
      uniprot "NA"
    ]
    graphics [
      x 1861.4958822635285
      y 1549.8113220126163
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 173
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740574;urn:miriam:uniprot:P0DTD8"
      hgnc "NA"
      map_id "M18_221"
      name "Orf7b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2251"
      uniprot "UNIPROT:P0DTD8"
    ]
    graphics [
      x 1914.6702030187078
      y 1647.0193986843708
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_221"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 174
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_178"
      name "Orf14_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2110"
      uniprot "NA"
    ]
    graphics [
      x 1137.7416007099484
      y 522.7050824784595
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_178"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 175
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_49"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1030"
      uniprot "NA"
    ]
    graphics [
      x 1302.5549324391473
      y 671.4154788438336
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 176
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTD3"
      hgnc "NA"
      map_id "M18_148"
      name "Orf14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1877"
      uniprot "UNIPROT:P0DTD3"
    ]
    graphics [
      x 1245.1349193804213
      y 441.2157454897207
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_148"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 177
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_55"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1066"
      uniprot "NA"
    ]
    graphics [
      x 1461.1801688152868
      y 1896.3983858386568
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 178
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_247"
      name "sa2294_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2331"
      uniprot "NA"
    ]
    graphics [
      x 1398.9157532540335
      y 2002.9665725726095
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_247"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 179
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S;urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:S;HGNC_SYMBOL:ACE2"
      map_id "M18_4"
      name "ACE2:SPIKE_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa368"
      uniprot "UNIPROT:P0DTC2;UNIPROT:Q9BYF1"
    ]
    graphics [
      x 531.5138445626253
      y 1594.0442023075384
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 180
    zlevel -1

    cd19dm [
      annotation "PUBMED:32142651;PUBMED:32362314"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_117"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re950"
      uniprot "NA"
    ]
    graphics [
      x 688.5424526263167
      y 1786.425436379519
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 181
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:hgnc:8568;urn:miriam:ensembl:ENSG00000140564;urn:miriam:ec-code:3.4.21.75;urn:miriam:uniprot:P09958;urn:miriam:uniprot:P09958;urn:miriam:ncbigene:5045;urn:miriam:ncbigene:5045;urn:miriam:hgnc.symbol:FURIN;urn:miriam:hgnc.symbol:FURIN;urn:miriam:refseq:NM_002569"
      hgnc "HGNC_SYMBOL:FURIN"
      map_id "M18_156"
      name "FURIN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1921"
      uniprot "UNIPROT:P09958"
    ]
    graphics [
      x 673.1577010957997
      y 1676.7853382678554
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_156"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 182
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:hgnc:11876;urn:miriam:hgnc.symbol:TMPRSS2;urn:miriam:uniprot:O15393;urn:miriam:uniprot:O15393;urn:miriam:hgnc.symbol:TMPRSS2;urn:miriam:ncbigene:7113;urn:miriam:ncbigene:7113;urn:miriam:ec-code:3.4.21.-;urn:miriam:ensembl:ENSG00000184012;urn:miriam:refseq:NM_001135099"
      hgnc "HGNC_SYMBOL:TMPRSS2"
      map_id "M18_128"
      name "TMPRSS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1537"
      uniprot "UNIPROT:O15393"
    ]
    graphics [
      x 749.4525928500796
      y 1857.818335076363
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 183
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:pubchem.compound:2536"
      hgnc "NA"
      map_id "M18_130"
      name "Camostat_space_mesylate"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1544"
      uniprot "NA"
    ]
    graphics [
      x 562.9895134937321
      y 1768.2964751554414
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_130"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 184
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:interpro:IPR002552"
      hgnc "NA"
      map_id "M18_168"
      name "S2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2063"
      uniprot "NA"
    ]
    graphics [
      x 942.7286759720672
      y 1869.4574072067849
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_168"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 185
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:ACE2"
      map_id "M18_209"
      name "ACE2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2239"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 614.7102036063626
      y 1857.3377816140774
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_209"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 186
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:ACE2"
      map_id "M18_208"
      name "ACE2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2238"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 550.4301407033948
      y 1836.6620035722112
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_208"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 187
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_236"
      name "Orf14_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2319"
      uniprot "NA"
    ]
    graphics [
      x 1435.6422530471323
      y 769.5392660592997
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_236"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 188
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_73"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1085"
      uniprot "NA"
    ]
    graphics [
      x 1425.4101933900888
      y 683.9153714524324
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 189
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_265"
      name "Orf14_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2351"
      uniprot "NA"
    ]
    graphics [
      x 1212.9997193823397
      y 539.1790746577899
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_265"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 190
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S;urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:S;HGNC_SYMBOL:ACE2"
      map_id "M18_17"
      name "ACE2:SPIKE_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa431"
      uniprot "UNIPROT:P0DTC2;UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1144.1203762914101
      y 351.59134535252406
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 191
    zlevel -1

    cd19dm [
      annotation "PUBMED:32142651"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_107"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re843"
      uniprot "NA"
    ]
    graphics [
      x 953.0186983657754
      y 241.57968487463472
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 192
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ensembl:ENSG00000164733;urn:miriam:ncbigene:1508;urn:miriam:ncbigene:1508;urn:miriam:refseq:NM_147780;urn:miriam:uniprot:P07858;urn:miriam:uniprot:P07858;urn:miriam:hgnc:2527;urn:miriam:ec-code:3.4.22.1;urn:miriam:hgnc.symbol:CTSB;urn:miriam:hgnc.symbol:CTSB"
      hgnc "HGNC_SYMBOL:CTSB"
      map_id "M18_126"
      name "CTSB"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1524"
      uniprot "UNIPROT:P07858"
    ]
    graphics [
      x 1147.5283234466467
      y 254.77778676824448
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 193
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.22.15;urn:miriam:hgnc.symbol:CTSL;urn:miriam:ncbigene:1514;urn:miriam:hgnc.symbol:CTSL;urn:miriam:ncbigene:1514;urn:miriam:uniprot:P07711;urn:miriam:uniprot:P07711;urn:miriam:ensembl:ENSG00000135047;urn:miriam:refseq:NM_001912;urn:miriam:hgnc:2537"
      hgnc "HGNC_SYMBOL:CTSL"
      map_id "M18_127"
      name "CTSL"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1525"
      uniprot "UNIPROT:P07711"
    ]
    graphics [
      x 997.398002564733
      y 114.95268281840413
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_127"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 194
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:interpro:IPR002551"
      hgnc "NA"
      map_id "M18_125"
      name "S1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1516"
      uniprot "NA"
    ]
    graphics [
      x 1110.654478919
      y 214.42172937876785
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_125"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 195
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_232"
      name "Orf7b_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2302"
      uniprot "NA"
    ]
    graphics [
      x 1613.993113483301
      y 1216.6758947317833
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_232"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 196
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_63"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1075"
      uniprot "NA"
    ]
    graphics [
      x 1446.1557282110932
      y 1372.0789485944292
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 197
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_255"
      name "sa2302_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2340"
      uniprot "NA"
    ]
    graphics [
      x 1286.5813533828552
      y 1499.5645907762832
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_255"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 198
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_33"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1014"
      uniprot "NA"
    ]
    graphics [
      x 1114.8198862608424
      y 1161.345411832874
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 199
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740573;urn:miriam:uniprot:P0DTC7"
      hgnc "NA"
      map_id "M18_215"
      name "Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2245"
      uniprot "UNIPROT:P0DTC7"
    ]
    graphics [
      x 932.7422598029289
      y 1039.4719200318461
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_215"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 200
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_110"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re859"
      uniprot "NA"
    ]
    graphics [
      x 643.5203868570718
      y 1404.5045360711738
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 201
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_75"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1088"
      uniprot "NA"
    ]
    graphics [
      x 1518.8448988570685
      y 1539.4317704008304
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 202
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:refseq:NC_045512"
      hgnc "NA"
      map_id "M18_233"
      name "(_minus_)ss_space_gRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2314"
      uniprot "NA"
    ]
    graphics [
      x 1337.045086700728
      y 1466.623700636529
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_233"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 203
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:refseq:NC_045512"
      hgnc "NA"
      map_id "M18_185"
      name "(_plus_)ss_space_gRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2153"
      uniprot "NA"
    ]
    graphics [
      x 1619.3679920275963
      y 1669.2149302963114
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_185"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 204
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_51"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1057"
      uniprot "NA"
    ]
    graphics [
      x 1144.2997745093394
      y 1407.1214293598896
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 205
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_234"
      name "sa2314_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2315"
      uniprot "NA"
    ]
    graphics [
      x 1003.2888806587591
      y 1306.602447193635
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_234"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 206
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTC1;urn:miriam:ec-code:3.4.22.-;urn:miriam:ncbigene:43740578"
      hgnc "NA"
      map_id "M18_139"
      name "pp1a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1789"
      uniprot "UNIPROT:P0DTC1"
    ]
    graphics [
      x 735.8996606701849
      y 852.7767466588259
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_139"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 207
    zlevel -1

    cd19dm [
      annotation "PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_89"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1104"
      uniprot "NA"
    ]
    graphics [
      x 778.5373451776509
      y 743.462118222194
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 208
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTC1;urn:miriam:ec-code:3.4.22.-;urn:miriam:ncbigene:43740578"
      hgnc "NA"
      map_id "M18_201"
      name "pp1a_space_Nsp3_minus_11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2220"
      uniprot "UNIPROT:P0DTC1"
    ]
    graphics [
      x 945.8954049484812
      y 779.6924012571311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_201"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 209
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725297"
      hgnc "NA"
      map_id "M18_270"
      name "Nsp1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2356"
      uniprot "NA"
    ]
    graphics [
      x 616.0329851742283
      y 635.4417759051901
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_270"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 210
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_27"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1008"
      uniprot "NA"
    ]
    graphics [
      x 595.6276043562622
      y 128.2324160381977
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 211
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "M18_141"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1857"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 669.9324994575456
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_141"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 212
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_266"
      name "Orf7b_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2352"
      uniprot "NA"
    ]
    graphics [
      x 1807.7498366962227
      y 839.567119189884
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_266"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 213
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_85"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1098"
      uniprot "NA"
    ]
    graphics [
      x 1772.1016086458148
      y 1050.3447328811765
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 214
    zlevel -1

    cd19dm [
      annotation "PUBMED:23035226"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_90"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re1105"
      uniprot "NA"
    ]
    graphics [
      x 507.8700035457606
      y 501.1782772509431
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 215
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0006412"
      hgnc "NA"
      map_id "M18_272"
      name "Host_space_translation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa2358"
      uniprot "NA"
    ]
    graphics [
      x 396.17936212669815
      y 429.5782966548503
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_272"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 216
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_80"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1093"
      uniprot "NA"
    ]
    graphics [
      x 1747.2291758676356
      y 520.4964739660853
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 217
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_176"
      name "Orf6_space__space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2108"
      uniprot "NA"
    ]
    graphics [
      x 1628.4929477256076
      y 427.50789715477913
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_176"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 218
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_38"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1019"
      uniprot "NA"
    ]
    graphics [
      x 1109.6110604641503
      y 303.8564790432529
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 219
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTD3"
      hgnc "NA"
      map_id "M18_220"
      name "Orf14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2250"
      uniprot "UNIPROT:P0DTD3"
    ]
    graphics [
      x 1083.113709560297
      y 399.38209430622385
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_220"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 220
    zlevel -1

    cd19dm [
      annotation "PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_92"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1107"
      uniprot "NA"
    ]
    graphics [
      x 592.6266087338431
      y 1124.7052913088291
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 221
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_241"
      name "Orf7a_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2324"
      uniprot "NA"
    ]
    graphics [
      x 1668.954511498298
      y 783.1393482953539
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_241"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 222
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_68"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1080"
      uniprot "NA"
    ]
    graphics [
      x 1686.0268076220848
      y 1000.9908502317686
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 223
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_260"
      name "Orf7a_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2346"
      uniprot "NA"
    ]
    graphics [
      x 1683.1315611856448
      y 1285.3877461802313
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_260"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 224
    zlevel -1

    cd19dm [
      annotation "PUBMED:32047258"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_118"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re952"
      uniprot "NA"
    ]
    graphics [
      x 1127.6923812589919
      y 1972.262122830891
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 225
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_61"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1073"
      uniprot "NA"
    ]
    graphics [
      x 714.1193771428257
      y 1008.4613124856444
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 226
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_253"
      name "sa2300_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2338"
      uniprot "NA"
    ]
    graphics [
      x 712.5473447708918
      y 926.5559016240285
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_253"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 227
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_79"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1092"
      uniprot "NA"
    ]
    graphics [
      x 1609.3060767474217
      y 1536.1291852436011
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 228
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_48"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1029"
      uniprot "NA"
    ]
    graphics [
      x 1271.7544330757555
      y 886.2480494702036
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 229
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1064"
      uniprot "NA"
    ]
    graphics [
      x 516.6228181958695
      y 386.83006689462945
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 230
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_246"
      name "sa2292_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2329"
      uniprot "NA"
    ]
    graphics [
      x 608.4876880556958
      y 356.88894762325197
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_246"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 231
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_1"
      name "Nucleocapsid"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa353"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 650.9335422842037
      y 1934.1588783760321
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 232
    zlevel -1

    cd19dm [
      annotation "PUBMED:32142651;PUBMED:32094589"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_106"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re841"
      uniprot "NA"
    ]
    graphics [
      x 427.5166197506064
      y 1984.7288874079782
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 233
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S;urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:S;HGNC_SYMBOL:ACE2"
      map_id "M18_16"
      name "ACE2:SPIKE_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa430"
      uniprot "UNIPROT:P0DTC2;UNIPROT:Q9BYF1"
    ]
    graphics [
      x 290.0371268405446
      y 1878.9806361970282
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 234
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_2"
      name "Nucleocapsid"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa357"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 421.4277204549576
      y 2118.502259013487
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 235
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC3;urn:miriam:ncbigene:43740569"
      hgnc "NA"
      map_id "M18_144"
      name "Orf3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1873"
      uniprot "UNIPROT:P0DTC3"
    ]
    graphics [
      x 1200.8644776080555
      y 632.028246799919
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_144"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 236
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_35"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1016"
      uniprot "NA"
    ]
    graphics [
      x 1056.0874671030888
      y 567.6499022491224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 237
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC3;urn:miriam:ncbigene:43740569"
      hgnc "NA"
      map_id "M18_217"
      name "Orf3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2247"
      uniprot "UNIPROT:P0DTC3"
    ]
    graphics [
      x 917.5723486895841
      y 572.0189860446096
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_217"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 238
    zlevel -1

    cd19dm [
      annotation "PUBMED:8830530"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_103"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1123"
      uniprot "NA"
    ]
    graphics [
      x 1537.8251676600971
      y 667.3688276761129
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 239
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_235"
      name "Orf7b_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2318"
      uniprot "NA"
    ]
    graphics [
      x 1653.2842677273047
      y 532.9615262438747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_235"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 240
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_242"
      name "S_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2325"
      uniprot "NA"
    ]
    graphics [
      x 1739.520097390143
      y 620.2601947999592
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_242"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 241
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_98"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1113"
      uniprot "NA"
    ]
    graphics [
      x 801.9718650357671
      y 1034.8950160151992
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 242
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_9"
      name "Nucleocapsid"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa391"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 1309.8460805240957
      y 2185.315275320798
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 243
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_112"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re895"
      uniprot "NA"
    ]
    graphics [
      x 1067.414470334893
      y 2198.7172503797283
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 244
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_8"
      name "Nucleocapsid"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa389"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 861.3674344373692
      y 2139.0815579354485
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 245
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:ACE2"
      map_id "M18_124"
      name "ACE2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1462"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 299.77861514140716
      y 1962.1054144305708
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_124"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 246
    zlevel -1

    cd19dm [
      annotation "PUBMED:32970989;PUBMED:32142651;PUBMED:32094589"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_105"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re838"
      uniprot "NA"
    ]
    graphics [
      x 186.2834289300705
      y 2013.0282458823199
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 247
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "M18_163"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2040"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 78.81405535226463
      y 1970.9364741285533
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_163"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 248
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "M18_187"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2178"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 334.10775056355146
      y 1915.843490550466
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_187"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 249
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A28815"
      hgnc "NA"
      map_id "M18_283"
      name "Heparan_space_sulfate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa2369"
      uniprot "NA"
    ]
    graphics [
      x 138.5929766464019
      y 1901.7339029443292
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_283"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 250
    zlevel -1

    cd19dm [
      annotation "PUBMED:11142;PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_67"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1079"
      uniprot "NA"
    ]
    graphics [
      x 1898.3564707788382
      y 736.5581730791473
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 251
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_259"
      name "S_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2345"
      uniprot "NA"
    ]
    graphics [
      x 2111.727520207332
      y 889.0829115565333
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_259"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 252
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_84"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1097"
      uniprot "NA"
    ]
    graphics [
      x 965.1036317460773
      y 466.7939809141965
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 253
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_111"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re894"
      uniprot "NA"
    ]
    graphics [
      x 692.943814147279
      y 2126.2429038559303
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 254
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_7"
      name "Nucleocapsid"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa387"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 759.2900821217677
      y 2158.6013588479514
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 255
    zlevel -1

    cd19dm [
      annotation "PUBMED:23035226"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_22"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re1000"
      uniprot "NA"
    ]
    graphics [
      x 2166.516674837043
      y 1037.479456887118
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 256
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0006412"
      hgnc "NA"
      map_id "M18_207"
      name "Host_space_translation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa2237"
      uniprot "NA"
    ]
    graphics [
      x 2051.950709496715
      y 889.6903783513676
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_207"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 257
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_10"
      name "Nucleocapsid"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa397"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 1636.399244047689
      y 1868.3310381007996
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 258
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_116"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re948"
      uniprot "NA"
    ]
    graphics [
      x 1505.8760029610137
      y 2073.94796646658
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 259
    zlevel -1

    cd19dm [
      annotation "PUBMED:21203998"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_23"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1001"
      uniprot "NA"
    ]
    graphics [
      x 1067.4141819227239
      y 903.8229763238533
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 260
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_53"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1063"
      uniprot "NA"
    ]
    graphics [
      x 1309.2612450381666
      y 1007.3872627224468
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 261
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1026"
      uniprot "NA"
    ]
    graphics [
      x 1497.1021985632183
      y 576.3392700669784
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 262
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740572;urn:miriam:uniprot:P0DTC6"
      hgnc "NA"
      map_id "M18_145"
      name "Orf6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1874"
      uniprot "UNIPROT:P0DTC6"
    ]
    graphics [
      x 1379.038642003641
      y 382.3463403252715
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_145"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 263
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_29"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1010"
      uniprot "NA"
    ]
    graphics [
      x 1950.2777482378651
      y 1965.2454861126396
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 264
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "M18_143"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1859"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 1810.3492383629227
      y 1979.1494681613804
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_143"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 265
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_40"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1021"
      uniprot "NA"
    ]
    graphics [
      x 794.1037103149099
      y 909.3876221575614
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 266
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740573;urn:miriam:uniprot:P0DTC7"
      hgnc "NA"
      map_id "M18_158"
      name "Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1986"
      uniprot "UNIPROT:P0DTC7"
    ]
    graphics [
      x 658.2088832718814
      y 841.7741406510329
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_158"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 267
    zlevel -1

    cd19dm [
      annotation "PUBMED:28720894"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_119"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re972"
      uniprot "NA"
    ]
    graphics [
      x 1715.763801791419
      y 1617.955064362011
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 268
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1012"
      uniprot "NA"
    ]
    graphics [
      x 871.0092611071859
      y 1559.148661661945
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 269
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "M18_159"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2009"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 355.38191506177327
      y 1418.7707632560082
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_159"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 270
    zlevel -1

    cd19dm [
      annotation "PUBMED:32970989;PUBMED:32142651;PUBMED:32155444;PUBMED:32094589"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_108"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re852"
      uniprot "NA"
    ]
    graphics [
      x 334.0174061742616
      y 1535.1295357386018
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 271
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:ACE2"
      map_id "M18_131"
      name "ACE2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1545"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 251.68243920213968
      y 1603.3093817522856
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_131"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 272
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "M18_186"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2173"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 333.1542430325617
      y 1370.0098447174846
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_186"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 273
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A28815"
      hgnc "NA"
      map_id "M18_284"
      name "Heparan_space_sulfate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa2370"
      uniprot "NA"
    ]
    graphics [
      x 275.68677562955713
      y 1407.3677902658724
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_284"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 274
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_78"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1091"
      uniprot "NA"
    ]
    graphics [
      x 2098.1592840424196
      y 1128.1975369193995
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 275
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_180"
      name "S_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2112"
      uniprot "NA"
    ]
    graphics [
      x 2018.235001595251
      y 1258.456214267875
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_180"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 276
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_74"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1086"
      uniprot "NA"
    ]
    graphics [
      x 1774.976498296057
      y 669.1323884687624
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 277
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_46"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1027"
      uniprot "NA"
    ]
    graphics [
      x 1355.7400082137046
      y 780.9864030209436
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 278
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1015"
      uniprot "NA"
    ]
    graphics [
      x 1193.655821420469
      y 314.44803230461844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 279
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740572;urn:miriam:uniprot:P0DTC6"
      hgnc "NA"
      map_id "M18_216"
      name "Orf6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2246"
      uniprot "UNIPROT:P0DTC6"
    ]
    graphics [
      x 1033.3859799225356
      y 329.24077321585264
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_216"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 280
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1028"
      uniprot "NA"
    ]
    graphics [
      x 1706.6923898957639
      y 1201.509944847379
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 281
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_59"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1070"
      uniprot "NA"
    ]
    graphics [
      x 1969.576245229243
      y 993.3379819130091
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 282
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_251"
      name "sa2298_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2335"
      uniprot "NA"
    ]
    graphics [
      x 2046.0757456864271
      y 943.2694085290802
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_251"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 283
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_60"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1072"
      uniprot "NA"
    ]
    graphics [
      x 1963.3282014031183
      y 1220.3603488667864
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 284
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_252"
      name "sa2299_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2337"
      uniprot "NA"
    ]
    graphics [
      x 2071.7288733793853
      y 1320.8171174277165
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_252"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 285
    zlevel -1

    cd19dm [
      annotation "PUBMED:11907209"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_95"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1110"
      uniprot "NA"
    ]
    graphics [
      x 1279.2435611394158
      y 1248.6448363053598
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 286
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_43"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1024"
      uniprot "NA"
    ]
    graphics [
      x 1850.2832089782594
      y 1236.624781875823
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 287
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_188"
      target_id "M18_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 2
    target 3
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_121"
      target_id "M18_164"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 4
    target 5
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_173"
      target_id "M18_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 6
    target 5
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 5
    target 7
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_44"
      target_id "M18_146"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 8
    target 9
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_135"
      target_id "M18_114"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 9
    target 10
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_114"
      target_id "M18_151"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 11
    target 12
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_174"
      target_id "M18_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 6
    target 12
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 12
    target 13
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_50"
      target_id "M18_147"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 14
    target 15
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_3"
      target_id "M18_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 16
    target 15
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_167"
      target_id "M18_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 17
    target 15
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_165"
      target_id "M18_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 18
    target 15
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_166"
      target_id "M18_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 3
    target 15
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M18_164"
      target_id "M18_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 15
    target 19
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_120"
      target_id "M18_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 15
    target 20
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_120"
      target_id "M18_182"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 304
    source 15
    target 21
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_120"
      target_id "M18_183"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 305
    source 15
    target 22
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_120"
      target_id "M18_184"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 306
    source 23
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_192"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 307
    source 25
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_191"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 308
    source 26
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_197"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 309
    source 27
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_198"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 310
    source 28
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_190"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 311
    source 29
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_189"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 312
    source 30
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_193"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 313
    source 31
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_194"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 314
    source 32
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_195"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 315
    source 33
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_196"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 316
    source 34
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_203"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 317
    source 35
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_205"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 318
    source 36
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_281"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 319
    source 37
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_277"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 320
    source 38
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_276"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 321
    source 39
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_275"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 322
    source 40
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_274"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 323
    source 41
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_200"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 324
    source 42
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_271"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 325
    source 24
    target 43
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_97"
      target_id "M18_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 326
    source 44
    target 45
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_6"
      target_id "M18_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 327
    source 46
    target 45
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_160"
      target_id "M18_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 328
    source 47
    target 45
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_161"
      target_id "M18_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 329
    source 48
    target 45
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_162"
      target_id "M18_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 330
    source 49
    target 45
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M18_132"
      target_id "M18_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 331
    source 50
    target 45
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M18_286"
      target_id "M18_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 332
    source 51
    target 45
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M18_21"
      target_id "M18_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 333
    source 45
    target 19
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_109"
      target_id "M18_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 334
    source 45
    target 52
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_109"
      target_id "M18_169"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 335
    source 45
    target 53
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_109"
      target_id "M18_170"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 336
    source 45
    target 54
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_109"
      target_id "M18_171"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 337
    source 55
    target 56
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_263"
      target_id "M18_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 338
    source 56
    target 57
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_82"
      target_id "M18_229"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 339
    source 56
    target 58
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_82"
      target_id "M18_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 340
    source 59
    target 60
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_213"
      target_id "M18_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 341
    source 60
    target 34
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_93"
      target_id "M18_203"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 342
    source 60
    target 35
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_93"
      target_id "M18_205"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 343
    source 61
    target 62
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_237"
      target_id "M18_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 344
    source 43
    target 62
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 345
    source 62
    target 63
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_72"
      target_id "M18_264"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 346
    source 43
    target 64
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_12"
      target_id "M18_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 347
    source 64
    target 65
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_102"
      target_id "M18_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 348
    source 10
    target 66
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_151"
      target_id "M18_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 349
    source 66
    target 65
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_52"
      target_id "M18_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 350
    source 67
    target 68
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_129"
      target_id "M18_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 351
    source 69
    target 68
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_285"
      target_id "M18_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 352
    source 68
    target 51
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_104"
      target_id "M18_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 353
    source 70
    target 71
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_243"
      target_id "M18_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 354
    source 43
    target 71
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 355
    source 71
    target 72
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_66"
      target_id "M18_258"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 356
    source 73
    target 74
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_149"
      target_id "M18_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 357
    source 74
    target 75
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_37"
      target_id "M18_219"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 358
    source 76
    target 77
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_239"
      target_id "M18_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 359
    source 43
    target 77
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 360
    source 77
    target 78
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_70"
      target_id "M18_262"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 361
    source 79
    target 80
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_244"
      target_id "M18_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 362
    source 43
    target 80
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 363
    source 80
    target 81
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_65"
      target_id "M18_257"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 364
    source 26
    target 82
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_197"
      target_id "M18_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 365
    source 82
    target 83
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_96"
      target_id "M18_222"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 366
    source 84
    target 85
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_214"
      target_id "M18_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 367
    source 85
    target 86
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_26"
      target_id "M18_206"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 368
    source 85
    target 87
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_26"
      target_id "M18_211"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 369
    source 85
    target 88
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_26"
      target_id "M18_212"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 370
    source 89
    target 90
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_225"
      target_id "M18_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 371
    source 90
    target 91
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_56"
      target_id "M18_248"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 372
    source 92
    target 93
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_267"
      target_id "M18_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 373
    source 43
    target 93
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 374
    source 93
    target 94
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_86"
      target_id "M18_268"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 375
    source 95
    target 96
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_138"
      target_id "M18_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 376
    source 96
    target 97
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_32"
      target_id "M18_154"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 377
    source 72
    target 98
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_258"
      target_id "M18_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 378
    source 98
    target 99
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_77"
      target_id "M18_224"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 379
    source 98
    target 100
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_77"
      target_id "M18_179"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 380
    source 101
    target 102
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_240"
      target_id "M18_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 381
    source 43
    target 102
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 382
    source 102
    target 103
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_69"
      target_id "M18_261"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 383
    source 104
    target 105
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_140"
      target_id "M18_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 384
    source 105
    target 106
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_88"
      target_id "M18_202"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 385
    source 105
    target 41
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_88"
      target_id "M18_200"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 386
    source 105
    target 107
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_88"
      target_id "M18_199"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 387
    source 108
    target 109
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_157"
      target_id "M18_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 388
    source 110
    target 109
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_15"
      target_id "M18_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 389
    source 109
    target 8
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_115"
      target_id "M18_135"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 390
    source 111
    target 112
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_19"
      target_id "M18_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 391
    source 35
    target 112
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "MODULATION"
      source_id "M18_205"
      target_id "M18_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 392
    source 34
    target 112
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "MODULATION"
      source_id "M18_203"
      target_id "M18_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 393
    source 36
    target 112
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "MODULATION"
      source_id "M18_281"
      target_id "M18_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 394
    source 33
    target 112
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "MODULATION"
      source_id "M18_196"
      target_id "M18_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 395
    source 112
    target 113
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_123"
      target_id "M18_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 396
    source 81
    target 114
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_257"
      target_id "M18_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 397
    source 114
    target 115
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_76"
      target_id "M18_223"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 398
    source 114
    target 116
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_76"
      target_id "M18_181"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 399
    source 78
    target 117
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_262"
      target_id "M18_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 400
    source 117
    target 118
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_81"
      target_id "M18_228"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 401
    source 117
    target 119
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_81"
      target_id "M18_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 402
    source 120
    target 121
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_20"
      target_id "M18_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 403
    source 121
    target 43
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_101"
      target_id "M18_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 404
    source 122
    target 123
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_245"
      target_id "M18_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 405
    source 43
    target 123
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 406
    source 123
    target 124
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_64"
      target_id "M18_256"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 407
    source 94
    target 125
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_268"
      target_id "M18_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 408
    source 125
    target 126
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_87"
      target_id "M18_269"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 409
    source 125
    target 108
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_87"
      target_id "M18_157"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 410
    source 63
    target 127
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_264"
      target_id "M18_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 411
    source 127
    target 128
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_83"
      target_id "M18_230"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 412
    source 127
    target 129
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_83"
      target_id "M18_177"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 413
    source 130
    target 131
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_150"
      target_id "M18_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 414
    source 131
    target 132
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_36"
      target_id "M18_218"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 415
    source 116
    target 133
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_181"
      target_id "M18_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 416
    source 6
    target 133
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 417
    source 133
    target 134
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_41"
      target_id "M18_136"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 418
    source 135
    target 136
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_133"
      target_id "M18_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 419
    source 136
    target 137
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_113"
      target_id "M18_155"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 420
    source 138
    target 139
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_134"
      target_id "M18_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 421
    source 140
    target 139
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_13"
      target_id "M18_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 422
    source 139
    target 104
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_99"
      target_id "M18_140"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 423
    source 141
    target 142
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_238"
      target_id "M18_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 424
    source 43
    target 142
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 425
    source 142
    target 55
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_71"
      target_id "M18_263"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 426
    source 143
    target 144
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_273"
      target_id "M18_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 427
    source 144
    target 145
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_91"
      target_id "M18_279"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 428
    source 106
    target 146
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_202"
      target_id "M18_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 429
    source 146
    target 84
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_25"
      target_id "M18_214"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 430
    source 147
    target 148
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_231"
      target_id "M18_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 431
    source 148
    target 149
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_62"
      target_id "M18_254"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 432
    source 88
    target 150
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_212"
      target_id "M18_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 433
    source 150
    target 59
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_24"
      target_id "M18_213"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 434
    source 126
    target 151
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_269"
      target_id "M18_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 435
    source 151
    target 152
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_100"
      target_id "M18_282"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 436
    source 153
    target 154
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_210"
      target_id "M18_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 437
    source 154
    target 155
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_122"
      target_id "M18_204"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 438
    source 154
    target 143
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_122"
      target_id "M18_273"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 439
    source 154
    target 156
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_122"
      target_id "M18_280"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 440
    source 100
    target 157
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_179"
      target_id "M18_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 441
    source 6
    target 157
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 442
    source 157
    target 158
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_42"
      target_id "M18_137"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 443
    source 134
    target 159
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_136"
      target_id "M18_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 444
    source 159
    target 160
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_30"
      target_id "M18_152"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 445
    source 155
    target 161
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_204"
      target_id "M18_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 446
    source 156
    target 161
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "M18_280"
      target_id "M18_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 447
    source 161
    target 36
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_94"
      target_id "M18_281"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 448
    source 161
    target 37
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_94"
      target_id "M18_277"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 449
    source 161
    target 38
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_94"
      target_id "M18_276"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 450
    source 161
    target 39
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_94"
      target_id "M18_275"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 451
    source 161
    target 40
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_94"
      target_id "M18_274"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 452
    source 161
    target 162
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_94"
      target_id "M18_278"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 453
    source 163
    target 164
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_227"
      target_id "M18_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 454
    source 164
    target 165
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_58"
      target_id "M18_250"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 455
    source 166
    target 167
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_153"
      target_id "M18_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 456
    source 167
    target 168
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_28"
      target_id "M18_142"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 457
    source 169
    target 170
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_226"
      target_id "M18_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 458
    source 170
    target 171
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_57"
      target_id "M18_249"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 459
    source 13
    target 172
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_147"
      target_id "M18_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 460
    source 172
    target 173
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_39"
      target_id "M18_221"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 461
    source 174
    target 175
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_178"
      target_id "M18_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 462
    source 6
    target 175
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 463
    source 175
    target 176
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_49"
      target_id "M18_148"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 464
    source 99
    target 177
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_224"
      target_id "M18_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 465
    source 177
    target 178
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_55"
      target_id "M18_247"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 466
    source 179
    target 180
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_4"
      target_id "M18_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 467
    source 181
    target 180
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "M18_156"
      target_id "M18_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 468
    source 182
    target 180
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "M18_128"
      target_id "M18_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 469
    source 183
    target 180
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "INHIBITION"
      source_id "M18_130"
      target_id "M18_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 470
    source 180
    target 184
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_117"
      target_id "M18_168"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 471
    source 180
    target 185
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_117"
      target_id "M18_209"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 472
    source 180
    target 67
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_117"
      target_id "M18_129"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 473
    source 180
    target 186
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_117"
      target_id "M18_208"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 474
    source 187
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_236"
      target_id "M18_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 475
    source 43
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 476
    source 188
    target 189
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_73"
      target_id "M18_265"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 477
    source 190
    target 191
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_17"
      target_id "M18_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 478
    source 192
    target 191
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "M18_126"
      target_id "M18_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 479
    source 193
    target 191
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "M18_127"
      target_id "M18_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 480
    source 191
    target 1
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_107"
      target_id "M18_188"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 481
    source 191
    target 194
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_107"
      target_id "M18_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 482
    source 195
    target 196
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_232"
      target_id "M18_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 483
    source 196
    target 197
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_63"
      target_id "M18_255"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 484
    source 7
    target 198
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_146"
      target_id "M18_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 485
    source 198
    target 199
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_33"
      target_id "M18_215"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 486
    source 19
    target 200
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_5"
      target_id "M18_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 487
    source 200
    target 138
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_110"
      target_id "M18_134"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 488
    source 200
    target 135
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_110"
      target_id "M18_133"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 489
    source 124
    target 201
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_256"
      target_id "M18_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 490
    source 201
    target 202
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_75"
      target_id "M18_233"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 491
    source 201
    target 203
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_75"
      target_id "M18_185"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 492
    source 202
    target 204
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_233"
      target_id "M18_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 493
    source 204
    target 205
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_51"
      target_id "M18_234"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 494
    source 206
    target 207
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_139"
      target_id "M18_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 495
    source 207
    target 208
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_89"
      target_id "M18_201"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 496
    source 207
    target 42
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_89"
      target_id "M18_271"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 497
    source 207
    target 209
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_89"
      target_id "M18_270"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 498
    source 160
    target 210
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_152"
      target_id "M18_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 499
    source 210
    target 211
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_27"
      target_id "M18_141"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 500
    source 212
    target 213
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_266"
      target_id "M18_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 501
    source 213
    target 195
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_85"
      target_id "M18_232"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 502
    source 213
    target 11
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_85"
      target_id "M18_174"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 503
    source 209
    target 214
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_270"
      target_id "M18_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 504
    source 214
    target 215
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_90"
      target_id "M18_272"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 505
    source 103
    target 216
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_261"
      target_id "M18_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 506
    source 216
    target 163
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_80"
      target_id "M18_227"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 507
    source 216
    target 217
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_80"
      target_id "M18_176"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 508
    source 176
    target 218
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_148"
      target_id "M18_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 509
    source 218
    target 219
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_38"
      target_id "M18_220"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 510
    source 145
    target 220
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_279"
      target_id "M18_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 511
    source 220
    target 35
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_92"
      target_id "M18_205"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 512
    source 220
    target 34
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_92"
      target_id "M18_203"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 513
    source 221
    target 222
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_241"
      target_id "M18_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 514
    source 43
    target 222
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 515
    source 222
    target 223
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_68"
      target_id "M18_260"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 516
    source 184
    target 224
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_168"
      target_id "M18_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 517
    source 224
    target 49
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_118"
      target_id "M18_132"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 518
    source 128
    target 225
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_230"
      target_id "M18_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 519
    source 225
    target 226
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_61"
      target_id "M18_253"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 520
    source 223
    target 227
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_260"
      target_id "M18_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 521
    source 227
    target 169
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_79"
      target_id "M18_226"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 522
    source 227
    target 4
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_79"
      target_id "M18_173"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 523
    source 129
    target 228
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_177"
      target_id "M18_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 524
    source 6
    target 228
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 525
    source 228
    target 73
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_48"
      target_id "M18_149"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 526
    source 115
    target 229
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_223"
      target_id "M18_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 527
    source 229
    target 230
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_54"
      target_id "M18_246"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 528
    source 231
    target 232
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_1"
      target_id "M18_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 529
    source 233
    target 232
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M18_16"
      target_id "M18_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 530
    source 232
    target 234
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_106"
      target_id "M18_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 531
    source 235
    target 236
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_144"
      target_id "M18_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 532
    source 236
    target 237
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_35"
      target_id "M18_217"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 533
    source 122
    target 238
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_245"
      target_id "M18_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 534
    source 43
    target 238
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 535
    source 238
    target 239
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_235"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 536
    source 238
    target 92
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_267"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 537
    source 238
    target 79
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_244"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 538
    source 238
    target 70
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_243"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 539
    source 238
    target 240
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_242"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 540
    source 238
    target 221
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_241"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 541
    source 238
    target 101
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_240"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 542
    source 238
    target 76
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_239"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 543
    source 238
    target 141
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_238"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 544
    source 238
    target 61
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_237"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 545
    source 238
    target 187
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_236"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 546
    source 138
    target 241
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_134"
      target_id "M18_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 547
    source 140
    target 241
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_13"
      target_id "M18_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 548
    source 241
    target 206
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_98"
      target_id "M18_139"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 549
    source 242
    target 243
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_9"
      target_id "M18_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 550
    source 243
    target 244
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_112"
      target_id "M18_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 551
    source 245
    target 246
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_124"
      target_id "M18_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 552
    source 247
    target 246
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_163"
      target_id "M18_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 553
    source 248
    target 246
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_187"
      target_id "M18_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 554
    source 249
    target 246
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M18_283"
      target_id "M18_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 555
    source 246
    target 233
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_105"
      target_id "M18_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 556
    source 240
    target 250
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_242"
      target_id "M18_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 557
    source 43
    target 250
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 558
    source 250
    target 251
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_67"
      target_id "M18_259"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 559
    source 189
    target 252
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_265"
      target_id "M18_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 560
    source 252
    target 147
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_84"
      target_id "M18_231"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 561
    source 252
    target 174
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_84"
      target_id "M18_178"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 562
    source 244
    target 253
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_8"
      target_id "M18_111"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 563
    source 253
    target 254
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_111"
      target_id "M18_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 564
    source 107
    target 255
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_199"
      target_id "M18_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 565
    source 255
    target 256
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_22"
      target_id "M18_207"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 566
    source 257
    target 258
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_10"
      target_id "M18_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 567
    source 258
    target 242
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_116"
      target_id "M18_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 568
    source 208
    target 259
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_201"
      target_id "M18_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 569
    source 259
    target 153
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_23"
      target_id "M18_210"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 570
    source 138
    target 260
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_134"
      target_id "M18_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 571
    source 43
    target 260
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 572
    source 260
    target 122
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_53"
      target_id "M18_245"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 573
    source 217
    target 261
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_176"
      target_id "M18_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 574
    source 6
    target 261
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 575
    source 261
    target 262
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_45"
      target_id "M18_145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 576
    source 97
    target 263
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_154"
      target_id "M18_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 577
    source 263
    target 264
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_29"
      target_id "M18_143"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 578
    source 199
    target 265
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_215"
      target_id "M18_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 579
    source 265
    target 266
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_40"
      target_id "M18_158"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 580
    source 65
    target 267
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_11"
      target_id "M18_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 581
    source 203
    target 267
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_185"
      target_id "M18_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 582
    source 267
    target 257
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_119"
      target_id "M18_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 583
    source 267
    target 120
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_119"
      target_id "M18_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 584
    source 158
    target 268
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_137"
      target_id "M18_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 585
    source 268
    target 166
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_31"
      target_id "M18_153"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 586
    source 269
    target 270
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_159"
      target_id "M18_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 587
    source 271
    target 270
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_131"
      target_id "M18_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 588
    source 272
    target 270
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_186"
      target_id "M18_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 589
    source 273
    target 270
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M18_284"
      target_id "M18_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 590
    source 270
    target 179
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_108"
      target_id "M18_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 591
    source 251
    target 274
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_259"
      target_id "M18_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 592
    source 274
    target 89
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_78"
      target_id "M18_225"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 593
    source 274
    target 275
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_78"
      target_id "M18_180"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 594
    source 239
    target 276
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_235"
      target_id "M18_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 595
    source 43
    target 276
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 596
    source 276
    target 212
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_74"
      target_id "M18_266"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 597
    source 119
    target 277
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_172"
      target_id "M18_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 598
    source 6
    target 277
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 599
    source 277
    target 235
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_46"
      target_id "M18_144"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 600
    source 262
    target 278
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_145"
      target_id "M18_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 601
    source 278
    target 279
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_34"
      target_id "M18_216"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 602
    source 58
    target 280
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_175"
      target_id "M18_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 603
    source 6
    target 280
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 604
    source 280
    target 130
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_47"
      target_id "M18_150"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 605
    source 118
    target 281
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_228"
      target_id "M18_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 606
    source 281
    target 282
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_59"
      target_id "M18_251"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 607
    source 57
    target 283
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_229"
      target_id "M18_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 608
    source 283
    target 284
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_60"
      target_id "M18_252"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 609
    source 86
    target 285
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_206"
      target_id "M18_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 610
    source 87
    target 285
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "M18_211"
      target_id "M18_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 611
    source 285
    target 28
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_190"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 612
    source 285
    target 29
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_189"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 613
    source 285
    target 23
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_192"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 614
    source 285
    target 30
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_193"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 615
    source 285
    target 31
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_194"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 616
    source 285
    target 32
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_195"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 617
    source 285
    target 33
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_196"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 618
    source 285
    target 25
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_191"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 619
    source 285
    target 26
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_197"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 620
    source 285
    target 27
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_198"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 621
    source 275
    target 286
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_180"
      target_id "M18_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 622
    source 6
    target 286
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 623
    source 286
    target 95
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_43"
      target_id "M18_138"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
