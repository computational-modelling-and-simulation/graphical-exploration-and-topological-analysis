# generated with VANTED V2.8.2 at Fri Mar 04 10:04:40 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:hgnc:14495;urn:miriam:refseq:NM_001371401;urn:miriam:ncbigene:51116;urn:miriam:ensembl:ENSG00000122140;urn:miriam:ncbigene:51116;urn:miriam:hgnc.symbol:MRPS2;urn:miriam:hgnc.symbol:MRPS2;urn:miriam:uniprot:Q9Y399;urn:miriam:uniprot:Q9Y399;urn:miriam:uniprot:P82663;urn:miriam:uniprot:P82663;urn:miriam:ensembl:ENSG00000131368;urn:miriam:hgnc.symbol:MRPS25;urn:miriam:ncbigene:64432;urn:miriam:hgnc.symbol:MRPS25;urn:miriam:ncbigene:64432;urn:miriam:hgnc:14511;urn:miriam:refseq:NM_022497;urn:miriam:ensembl:ENSG00000144029;urn:miriam:uniprot:P82675;urn:miriam:uniprot:P82675;urn:miriam:hgnc:14498;urn:miriam:hgnc.symbol:MRPS5;urn:miriam:hgnc.symbol:MRPS5;urn:miriam:refseq:NM_031902;urn:miriam:ncbigene:64969;urn:miriam:ncbigene:64969;urn:miriam:hgnc:14512;urn:miriam:refseq:NM_015084;urn:miriam:hgnc.symbol:MRPS27;urn:miriam:ensembl:ENSG00000113048;urn:miriam:hgnc.symbol:MRPS27;urn:miriam:uniprot:Q92552;urn:miriam:uniprot:Q92552;urn:miriam:ncbigene:23107;urn:miriam:ncbigene:23107"
      hgnc "HGNC_SYMBOL:MRPS2;HGNC_SYMBOL:MRPS25;HGNC_SYMBOL:MRPS5;HGNC_SYMBOL:MRPS27"
      map_id "M13_21"
      name "Nsp8_minus_affected_space_Mt_space_ribosomal_space_proteins"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa91"
      uniprot "UNIPROT:Q9Y399;UNIPROT:P82663;UNIPROT:P82675;UNIPROT:Q92552"
    ]
    graphics [
      x 719.1025928291375
      y 1237.175858352579
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_41"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re147"
      uniprot "NA"
    ]
    graphics [
      x 802.373897444384
      y 1157.9814524079327
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ncbiprotein:YP_009742615"
      hgnc "NA"
      map_id "M13_146"
      name "Nsp8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa494"
      uniprot "NA"
    ]
    graphics [
      x 820.3984743975731
      y 1276.2879453152427
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_146"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:hgnc:14495;urn:miriam:refseq:NM_001371401;urn:miriam:ncbigene:51116;urn:miriam:ensembl:ENSG00000122140;urn:miriam:ncbigene:51116;urn:miriam:hgnc.symbol:MRPS2;urn:miriam:hgnc.symbol:MRPS2;urn:miriam:uniprot:Q9Y399;urn:miriam:uniprot:Q9Y399;urn:miriam:hgnc:14512;urn:miriam:refseq:NM_015084;urn:miriam:hgnc.symbol:MRPS27;urn:miriam:ensembl:ENSG00000113048;urn:miriam:hgnc.symbol:MRPS27;urn:miriam:uniprot:Q92552;urn:miriam:uniprot:Q92552;urn:miriam:ncbigene:23107;urn:miriam:ncbigene:23107;urn:miriam:ensembl:ENSG00000144029;urn:miriam:uniprot:P82675;urn:miriam:uniprot:P82675;urn:miriam:hgnc:14498;urn:miriam:hgnc.symbol:MRPS5;urn:miriam:hgnc.symbol:MRPS5;urn:miriam:refseq:NM_031902;urn:miriam:ncbigene:64969;urn:miriam:ncbigene:64969;urn:miriam:uniprot:P82663;urn:miriam:uniprot:P82663;urn:miriam:ensembl:ENSG00000131368;urn:miriam:hgnc.symbol:MRPS25;urn:miriam:ncbigene:64432;urn:miriam:hgnc.symbol:MRPS25;urn:miriam:ncbigene:64432;urn:miriam:hgnc:14511;urn:miriam:refseq:NM_022497"
      hgnc "HGNC_SYMBOL:MRPS2;HGNC_SYMBOL:MRPS27;HGNC_SYMBOL:MRPS5;HGNC_SYMBOL:MRPS25"
      map_id "M13_20"
      name "Nsp8_minus_affected_space_Mt_space_ribosomal_space_proteins"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa90"
      uniprot "UNIPROT:Q9Y399;UNIPROT:Q92552;UNIPROT:P82675;UNIPROT:P82663"
    ]
    graphics [
      x 857.7158758447282
      y 999.3798441972883
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_86"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re93"
      uniprot "NA"
    ]
    graphics [
      x 998.1606035799239
      y 855.8395484342327
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A33699"
      hgnc "NA"
      map_id "M13_142"
      name "mt_space_mRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa459"
      uniprot "NA"
    ]
    graphics [
      x 1159.5238605286397
      y 871.7318103386895
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_142"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:doi:10.1155/2010/737385;urn:miriam:hgnc.symbol:TWNK;urn:miriam:hgnc.symbol:TWNK;urn:miriam:ncbigene:56652;urn:miriam:ncbigene:56652;urn:miriam:ec-code:3.6.4.12;urn:miriam:refseq:NM_021830;urn:miriam:ensembl:ENSG00000107815;urn:miriam:hgnc:1160;urn:miriam:uniprot:Q96RR1;urn:miriam:uniprot:Q96RR1;urn:miriam:ensembl:ENSG00000123297;urn:miriam:uniprot:P43897;urn:miriam:uniprot:P43897;urn:miriam:hgnc:12367;urn:miriam:ncbigene:10102;urn:miriam:refseq:NM_005726;urn:miriam:ncbigene:10102;urn:miriam:hgnc.symbol:TSFM;urn:miriam:hgnc.symbol:TSFM;urn:miriam:ncbigene:7284;urn:miriam:ncbigene:7284;urn:miriam:ensembl:ENSG00000178952;urn:miriam:refseq:NM_003321;urn:miriam:hgnc:12420;urn:miriam:uniprot:P49411;urn:miriam:uniprot:P49411;urn:miriam:hgnc.symbol:TUFM;urn:miriam:hgnc.symbol:TUFM;urn:miriam:uniprot:Q96RP9;urn:miriam:uniprot:Q96RP9;urn:miriam:ncbigene:85476;urn:miriam:ncbigene:85476;urn:miriam:hgnc.symbol:GFM1;urn:miriam:hgnc.symbol:GFM1;urn:miriam:refseq:NM_024996;urn:miriam:ensembl:ENSG00000168827;urn:miriam:hgnc:13780"
      hgnc "HGNC_SYMBOL:TWNK;HGNC_SYMBOL:TSFM;HGNC_SYMBOL:TUFM;HGNC_SYMBOL:GFM1"
      map_id "M13_17"
      name "Mt_space_translation"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa58"
      uniprot "UNIPROT:Q96RR1;UNIPROT:P43897;UNIPROT:P49411;UNIPROT:Q96RP9"
    ]
    graphics [
      x 1015.273029426536
      y 733.7001201946221
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:pubmed:23149385;urn:miriam:hgnc.symbol:MRPS22;urn:miriam:ensembl:ENSG00000175110;urn:miriam:hgnc.symbol:MRPS22;urn:miriam:ncbigene:56945;urn:miriam:ncbigene:56945;urn:miriam:hgnc:14508;urn:miriam:uniprot:P82650;urn:miriam:uniprot:P82650;urn:miriam:refseq:NM_020191;urn:miriam:refseq:NM_007208;urn:miriam:hgnc.symbol:MRPL3;urn:miriam:ncbigene:11222;urn:miriam:hgnc.symbol:MRPL3;urn:miriam:ncbigene:11222;urn:miriam:hgnc:10379;urn:miriam:ensembl:ENSG00000114686;urn:miriam:uniprot:P09001;urn:miriam:uniprot:P09001;urn:miriam:ensembl:ENSG00000182180;urn:miriam:hgnc:14048;urn:miriam:ncbigene:51021;urn:miriam:ncbigene:51021;urn:miriam:hgnc.symbol:MRPS16;urn:miriam:refseq:NM_016065;urn:miriam:hgnc.symbol:MRPS16;urn:miriam:uniprot:Q9Y3D3;urn:miriam:uniprot:Q9Y3D3"
      hgnc "HGNC_SYMBOL:MRPS22;HGNC_SYMBOL:MRPL3;HGNC_SYMBOL:MRPS16"
      map_id "M13_10"
      name "Mt_space_ribosomal_space_proteins"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa51"
      uniprot "UNIPROT:P82650;UNIPROT:P09001;UNIPROT:Q9Y3D3"
    ]
    graphics [
      x 918.3953887504687
      y 797.0947113472319
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:pubmed:23149385;urn:miriam:pubmed:30030361"
      hgnc "NA"
      map_id "M13_18"
      name "mtDNA_space_encoded_space_OXPHOS_space_units"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa60"
      uniprot "NA"
    ]
    graphics [
      x 1082.8797646095022
      y 782.1938348805027
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_81"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re88"
      uniprot "NA"
    ]
    graphics [
      x 1306.1372698025268
      y 913.5506656127881
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:obo.go:GO%3A0000262"
      hgnc "NA"
      map_id "M13_156"
      name "mt_space_DNA"
      node_subtype "GENE"
      node_type "species"
      org_id "sa652"
      uniprot "NA"
    ]
    graphics [
      x 1439.313208629499
      y 1131.9473571928252
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_156"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:refseq:NM_003201;urn:miriam:hgnc:11741;urn:miriam:hgnc.symbol:TFAM;urn:miriam:hgnc.symbol:TFAM;urn:miriam:uniprot:Q00059;urn:miriam:ensembl:ENSG00000108064;urn:miriam:ncbigene:7019;urn:miriam:ncbigene:7019"
      hgnc "HGNC_SYMBOL:TFAM"
      map_id "M13_143"
      name "TFAM"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa460"
      uniprot "UNIPROT:Q00059"
    ]
    graphics [
      x 1329.6396630054066
      y 646.6498835823254
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_143"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:pubmed:18391175;urn:miriam:hgnc.symbol:TFB1M;urn:miriam:hgnc.symbol:TFB1M;urn:miriam:ncbigene:51106;urn:miriam:ncbigene:51106;urn:miriam:hgnc:17037;urn:miriam:ensembl:ENSG00000029639;urn:miriam:uniprot:Q8WVM0;urn:miriam:uniprot:Q8WVM0;urn:miriam:ec-code:2.1.1.-;urn:miriam:refseq:NM_001350501;urn:miriam:hgnc.symbol:TFB2M;urn:miriam:ncbigene:64216;urn:miriam:hgnc.symbol:TFB2M;urn:miriam:ncbigene:64216;urn:miriam:uniprot:Q9H5Q4;urn:miriam:uniprot:Q9H5Q4;urn:miriam:hgnc:18559;urn:miriam:refseq:NM_022366;urn:miriam:ec-code:2.1.1.-;urn:miriam:ensembl:ENSG00000162851;urn:miriam:hgnc.symbol:POLRMT;urn:miriam:hgnc.symbol:POLRMT;urn:miriam:uniprot:O00411;urn:miriam:uniprot:O00411;urn:miriam:refseq:NM_005035;urn:miriam:ncbigene:5442;urn:miriam:ncbigene:5442;urn:miriam:ec-code:2.7.7.6;urn:miriam:hgnc:9200;urn:miriam:ensembl:ENSG00000099821"
      hgnc "HGNC_SYMBOL:TFB1M;HGNC_SYMBOL:TFB2M;HGNC_SYMBOL:POLRMT"
      map_id "M13_15"
      name "MT_space_transcription"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa56"
      uniprot "UNIPROT:Q8WVM0;UNIPROT:Q9H5Q4;UNIPROT:O00411"
    ]
    graphics [
      x 1275.2432386821292
      y 1019.1458139890367
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_157"
      name "damaged_space_mt_space_DNA"
      node_subtype "GENE"
      node_type "species"
      org_id "sa653"
      uniprot "NA"
    ]
    graphics [
      x 1409.3796179834944
      y 1025.371762561011
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_157"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_39"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re141"
      uniprot "NA"
    ]
    graphics [
      x 1554.1302626023548
      y 1135.533994493909
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_40"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re142"
      uniprot "NA"
    ]
    graphics [
      x 1380.3602842380678
      y 896.4918558513307
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:pubmed:23149385;urn:miriam:hgnc.symbol:ERCC8;urn:miriam:hgnc.symbol:ERCC8;urn:miriam:ncbigene:1161;urn:miriam:ncbigene:1161;urn:miriam:uniprot:Q13216;urn:miriam:uniprot:Q13216;urn:miriam:hgnc:3439;urn:miriam:refseq:NM_000082;urn:miriam:ensembl:ENSG00000049167;urn:miriam:uniprot:P54098;urn:miriam:uniprot:P54098;urn:miriam:ensembl:ENSG00000140521;urn:miriam:ncbigene:5428;urn:miriam:ncbigene:5428;urn:miriam:refseq:NM_002693;urn:miriam:hgnc.symbol:POLG;urn:miriam:hgnc.symbol:POLG;urn:miriam:ec-code:2.7.7.7;urn:miriam:hgnc:9179;urn:miriam:ensembl:ENSG00000225830;urn:miriam:refseq:NM_000124;urn:miriam:ncbigene:2074;urn:miriam:ncbigene:2074;urn:miriam:hgnc:3438;urn:miriam:uniprot:P0DP91;urn:miriam:uniprot:P0DP91;urn:miriam:hgnc.symbol:ERCC6;urn:miriam:uniprot:Q03468;urn:miriam:hgnc.symbol:ERCC6;urn:miriam:uniprot:Q03468;urn:miriam:ec-code:3.6.4.-"
      hgnc "HGNC_SYMBOL:ERCC8;HGNC_SYMBOL:POLG;HGNC_SYMBOL:ERCC6"
      map_id "M13_13"
      name "Mt_minus_DNA_space_repair"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa54"
      uniprot "UNIPROT:Q13216;UNIPROT:P54098;UNIPROT:P0DP91;UNIPROT:Q03468"
    ]
    graphics [
      x 1303.3542508284304
      y 819.9269831791715
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:pubmed:23149385;urn:miriam:ec-code:6.2.1.5;urn:miriam:hgnc:11448;urn:miriam:hgnc.symbol:SUCLA2;urn:miriam:refseq:NM_003850;urn:miriam:hgnc.symbol:SUCLA2;urn:miriam:ensembl:ENSG00000136143;urn:miriam:uniprot:Q9P2R7;urn:miriam:uniprot:Q9P2R7;urn:miriam:ncbigene:8803;urn:miriam:ncbigene:8803;urn:miriam:ncbigene:50484;urn:miriam:ncbigene:50484;urn:miriam:ensembl:ENSG00000048392;urn:miriam:hgnc.symbol:RRM2B;urn:miriam:hgnc.symbol:RRM2B;urn:miriam:uniprot:Q7LG56;urn:miriam:uniprot:Q7LG56;urn:miriam:hgnc:17296;urn:miriam:ec-code:1.17.4.1;urn:miriam:refseq:NM_001172477;urn:miriam:ec-code:6.2.1.5;urn:miriam:ensembl:ENSG00000163541;urn:miriam:hgnc:11449;urn:miriam:ec-code:6.2.1.4;urn:miriam:hgnc.symbol:SUCLG1;urn:miriam:hgnc.symbol:SUCLG1;urn:miriam:refseq:NM_003849;urn:miriam:uniprot:P53597;urn:miriam:uniprot:P53597;urn:miriam:ncbigene:8802;urn:miriam:ncbigene:8802;urn:miriam:hgnc.symbol:DGUOK;urn:miriam:hgnc.symbol:DGUOK;urn:miriam:ncbigene:1716;urn:miriam:ncbigene:1716;urn:miriam:ec-code:2.7.1.76;urn:miriam:ec-code:2.7.1.113;urn:miriam:refseq:NM_001318859;urn:miriam:uniprot:Q16854;urn:miriam:uniprot:Q16854;urn:miriam:hgnc:2858;urn:miriam:ensembl:ENSG00000114956;urn:miriam:hgnc.symbol:TK2;urn:miriam:hgnc.symbol:TK2;urn:miriam:ncbigene:7084;urn:miriam:ncbigene:7084;urn:miriam:hgnc:11831;urn:miriam:ensembl:ENSG00000166548;urn:miriam:ec-code:2.7.1.21;urn:miriam:refseq:NM_001172643;urn:miriam:uniprot:O00142;urn:miriam:uniprot:O00142"
      hgnc "HGNC_SYMBOL:SUCLA2;HGNC_SYMBOL:RRM2B;HGNC_SYMBOL:SUCLG1;HGNC_SYMBOL:DGUOK;HGNC_SYMBOL:TK2"
      map_id "M13_11"
      name "Mt_minus_dNTP_space_pool"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa52"
      uniprot "UNIPROT:Q9P2R7;UNIPROT:Q7LG56;UNIPROT:P53597;UNIPROT:Q16854;UNIPROT:O00142"
    ]
    graphics [
      x 1432.4754008374712
      y 763.6539567463801
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_85"
      name "NA"
      node_subtype "MODULATION"
      node_type "reaction"
      org_id "re92"
      uniprot "NA"
    ]
    graphics [
      x 1496.4798914920216
      y 645.5716208878122
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_145"
      name "mt_space_DNA_space_replication"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa463"
      uniprot "NA"
    ]
    graphics [
      x 1605.907440392114
      y 577.1484116850836
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_145"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_82"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re89"
      uniprot "NA"
    ]
    graphics [
      x 1782.1598462718869
      y 515.8196927770936
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_79"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "re85"
      uniprot "NA"
    ]
    graphics [
      x 1456.561703643662
      y 546.3741199793608
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_80"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re87"
      uniprot "NA"
    ]
    graphics [
      x 1566.0337721792312
      y 827.6587143400961
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_158"
      name "s1119"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa655"
      uniprot "NA"
    ]
    graphics [
      x 1584.6638071437235
      y 708.0808373699508
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_158"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_144"
      name "mt_space_DNA_space_damage"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa461"
      uniprot "NA"
    ]
    graphics [
      x 1604.841891052019
      y 975.5445219210886
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_144"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:doi:10.1155/2010/737385;urn:miriam:doi:10.1042/EBC20170103;urn:miriam:obo.go:GO%3A0006264;urn:miriam:uniprot:P54098;urn:miriam:uniprot:P54098;urn:miriam:ensembl:ENSG00000140521;urn:miriam:ncbigene:5428;urn:miriam:ncbigene:5428;urn:miriam:refseq:NM_002693;urn:miriam:hgnc.symbol:POLG;urn:miriam:hgnc.symbol:POLG;urn:miriam:ec-code:2.7.7.7;urn:miriam:hgnc:9179;urn:miriam:hgnc.symbol:POLG2;urn:miriam:ncbigene:11232;urn:miriam:hgnc.symbol:POLG2;urn:miriam:ncbigene:11232;urn:miriam:refseq:NM_007215;urn:miriam:uniprot:Q9UHN1;urn:miriam:uniprot:Q9UHN1;urn:miriam:hgnc:9180;urn:miriam:ensembl:ENSG00000256525;urn:miriam:refseq:NM_018109;urn:miriam:ensembl:ENSG00000107951;urn:miriam:ncbigene:55149;urn:miriam:uniprot:Q9NVV4;urn:miriam:uniprot:Q9NVV4;urn:miriam:ncbigene:55149;urn:miriam:hgnc.symbol:MTPAP;urn:miriam:hgnc.symbol:MTPAP;urn:miriam:ec-code:2.7.7.19;urn:miriam:hgnc:25532;urn:miriam:hgnc:29666;urn:miriam:ensembl:ENSG00000103707;urn:miriam:ncbigene:123263;urn:miriam:ncbigene:123263;urn:miriam:uniprot:Q96DP5;urn:miriam:uniprot:Q96DP5;urn:miriam:ec-code:2.1.2.9;urn:miriam:refseq:NM_139242;urn:miriam:hgnc.symbol:MTFMT;urn:miriam:hgnc.symbol:MTFMT"
      hgnc "HGNC_SYMBOL:POLG;HGNC_SYMBOL:POLG2;HGNC_SYMBOL:MTPAP;HGNC_SYMBOL:MTFMT"
      map_id "M13_16"
      name "Mt_space_replication"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa57"
      uniprot "UNIPROT:P54098;UNIPROT:Q9UHN1;UNIPROT:Q9NVV4;UNIPROT:Q96DP5"
    ]
    graphics [
      x 1893.9544883936828
      y 621.2663979858635
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "PUBMED:23149385"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_84"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re91"
      uniprot "NA"
    ]
    graphics [
      x 1518.22964124148
      y 1498.7967960857748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:pubmed:23149385;urn:miriam:refseq:NM_002047;urn:miriam:ensembl:ENSG00000106105;urn:miriam:uniprot:P41250;urn:miriam:uniprot:P41250;urn:miriam:hgnc.symbol:GARS1;urn:miriam:hgnc.symbol:GARS1;urn:miriam:ec-code:2.7.7.-;urn:miriam:ncbigene:2617;urn:miriam:ncbigene:2617;urn:miriam:hgnc:4162;urn:miriam:ec-code:6.1.1.14;urn:miriam:hgnc:25538;urn:miriam:hgnc.symbol:DARS2;urn:miriam:hgnc.symbol:DARS2;urn:miriam:ncbigene:55157;urn:miriam:refseq:NM_018122;urn:miriam:ncbigene:55157;urn:miriam:ensembl:ENSG00000117593;urn:miriam:ec-code:6.1.1.12;urn:miriam:uniprot:Q6PI48;urn:miriam:uniprot:Q6PI48;urn:miriam:uniprot:Q5JTZ9;urn:miriam:uniprot:Q5JTZ9;urn:miriam:ensembl:ENSG00000124608;urn:miriam:hgnc.symbol:AARS2;urn:miriam:hgnc.symbol:AARS2;urn:miriam:hgnc:21022;urn:miriam:ncbigene:57505;urn:miriam:ncbigene:57505;urn:miriam:refseq:NM_020745;urn:miriam:ec-code:6.1.1.7;urn:miriam:ncbigene:3735;urn:miriam:ncbigene:3735;urn:miriam:ensembl:ENSG00000065427;urn:miriam:hgnc:6215;urn:miriam:ec-code:2.7.7.-;urn:miriam:uniprot:Q15046;urn:miriam:uniprot:Q15046;urn:miriam:refseq:NM_005548;urn:miriam:ec-code:6.1.1.6;urn:miriam:hgnc.symbol:KARS1;urn:miriam:hgnc.symbol:KARS1;urn:miriam:hgnc:21406;urn:miriam:hgnc.symbol:RARS2;urn:miriam:hgnc.symbol:RARS2;urn:miriam:ncbigene:57038;urn:miriam:refseq:NM_020320;urn:miriam:ncbigene:57038;urn:miriam:ec-code:6.1.1.19;urn:miriam:uniprot:Q5T160;urn:miriam:uniprot:Q5T160;urn:miriam:ensembl:ENSG00000146282"
      hgnc "HGNC_SYMBOL:GARS1;HGNC_SYMBOL:DARS2;HGNC_SYMBOL:AARS2;HGNC_SYMBOL:KARS1;HGNC_SYMBOL:RARS2"
      map_id "M13_12"
      name "Mt_minus_tRNA_space_synthetase"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa53"
      uniprot "UNIPROT:P41250;UNIPROT:Q6PI48;UNIPROT:Q5JTZ9;UNIPROT:Q15046;UNIPROT:Q5T160"
    ]
    graphics [
      x 1537.1340982614038
      y 1627.254775938123
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:pubmed:28752201;urn:miriam:refseq:NM_017722;urn:miriam:hgnc.symbol:TRMT1;urn:miriam:ncbigene:55621;urn:miriam:hgnc.symbol:TRMT1;urn:miriam:ncbigene:55621;urn:miriam:ensembl:ENSG00000104907;urn:miriam:uniprot:Q9NXH9;urn:miriam:uniprot:Q9NXH9;urn:miriam:ec-code:2.1.1.216;urn:miriam:hgnc:25980"
      hgnc "HGNC_SYMBOL:TRMT1"
      map_id "M13_173"
      name "TRMT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa692"
      uniprot "UNIPROT:Q9NXH9"
    ]
    graphics [
      x 1510.3104293603483
      y 1733.4275346200147
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_173"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:pubmed:23149385;urn:miriam:ensembl:ENSG00000210174;urn:miriam:ncbigene:4573;urn:miriam:hgnc:7496;urn:miriam:hgnc.symbol:MT-TR;urn:miriam:hgnc:7489;urn:miriam:hgnc.symbol:MT-TK;urn:miriam:ensembl:ENSG00000210156;urn:miriam:ncbigene:4566;urn:miriam:hgnc:7501;urn:miriam:hgnc.symbol:MT-TW;urn:miriam:ensembl:ENSG00000210117;urn:miriam:ncbigene:4578;urn:miriam:ensembl:ENSG00000210077;urn:miriam:hgnc:7500;urn:miriam:hgnc.symbol:MT-TV;urn:miriam:ncbigene:4577;urn:miriam:ensembl:ENSG00000210100;urn:miriam:hgnc:7488;urn:miriam:hgnc.symbol:MT-TI;urn:miriam:ncbigene:4565;urn:miriam:hgnc.symbol:MT-TP;urn:miriam:ensembl:ENSG00000210196;urn:miriam:ncbigene:4571;urn:miriam:hgnc:7494;urn:miriam:ensembl:ENSG00000210151;urn:miriam:hgnc.symbol:MT-TS1;urn:miriam:ncbigene:4574;urn:miriam:hgnc:7497;urn:miriam:ncbigene:4549;urn:miriam:uniprot:A0A0C5B5G6;urn:miriam:hgnc:7470;urn:miriam:ensembl:ENSG00000211459;urn:miriam:hgnc.symbol:MT-RNR1;urn:miriam:hgnc:7479;urn:miriam:ensembl:ENSG00000210194;urn:miriam:hgnc.symbol:MT-TE;urn:miriam:ncbigene:4556;urn:miriam:hgnc.symbol:MT-TL2;urn:miriam:ensembl:ENSG00000210191;urn:miriam:hgnc:7491;urn:miriam:ncbigene:4568;urn:miriam:hgnc:7499;urn:miriam:ensembl:ENSG00000210195;urn:miriam:hgnc.symbol:MT-TT;urn:miriam:ncbigene:4576;urn:miriam:hgnc:7498;urn:miriam:ensembl:ENSG00000210184;urn:miriam:hgnc.symbol:MT-TS2;urn:miriam:ncbigene:4575;urn:miriam:ncbigene:4558;urn:miriam:hgnc:7481;urn:miriam:hgnc.symbol:MT-TF;urn:miriam:ensembl:ENSG00000210049;urn:miriam:hgnc.symbol:MT-TL1;urn:miriam:hgnc:7490;urn:miriam:ensembl:ENSG00000209082;urn:miriam:ncbigene:4567;urn:miriam:hgnc.symbol:MT-TN;urn:miriam:ncbigene:4570;urn:miriam:hgnc:7493;urn:miriam:ensembl:ENSG00000210135;urn:miriam:ensembl:ENSG00000210176;urn:miriam:hgnc:7487;urn:miriam:ncbigene:4564;urn:miriam:hgnc.symbol:MT-TH;urn:miriam:ensembl:ENSG00000210107;urn:miriam:ncbigene:4572;urn:miriam:hgnc.symbol:MT-TQ;urn:miriam:hgnc:7495;urn:miriam:hgnc:7477;urn:miriam:ensembl:ENSG00000210140;urn:miriam:hgnc.symbol:MT-TC;urn:miriam:ncbigene:4511"
      hgnc "HGNC_SYMBOL:MT-TR;HGNC_SYMBOL:MT-TK;HGNC_SYMBOL:MT-TW;HGNC_SYMBOL:MT-TV;HGNC_SYMBOL:MT-TI;HGNC_SYMBOL:MT-TP;HGNC_SYMBOL:MT-TS1;HGNC_SYMBOL:MT-RNR1;HGNC_SYMBOL:MT-TE;HGNC_SYMBOL:MT-TL2;HGNC_SYMBOL:MT-TT;HGNC_SYMBOL:MT-TS2;HGNC_SYMBOL:MT-TF;HGNC_SYMBOL:MT-TL1;HGNC_SYMBOL:MT-TN;HGNC_SYMBOL:MT-TH;HGNC_SYMBOL:MT-TQ;HGNC_SYMBOL:MT-TC"
      map_id "M13_14"
      name "MT_space_tRNAs"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa55"
      uniprot "UNIPROT:A0A0C5B5G6"
    ]
    graphics [
      x 1616.4638963478083
      y 1588.1949531358932
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_50"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re161"
      uniprot "NA"
    ]
    graphics [
      x 1473.753533461396
      y 1909.2234857457083
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "urn:miriam:ncbiprotein:YP_009742612"
      hgnc "NA"
      map_id "M13_174"
      name "Nsp5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa693"
      uniprot "NA"
    ]
    graphics [
      x 1403.7530267502627
      y 2022.4255350552326
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_174"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Electron Transport Chain disruption"
      full_annotation "NA"
      hgnc "NA"
      map_id "M13_175"
      name "s1167"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa694"
      uniprot "NA"
    ]
    graphics [
      x 1345.8305001219505
      y 1934.870753676581
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M13_175"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 34
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_21"
      target_id "M13_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 35
    source 3
    target 2
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "INHIBITION"
      source_id "M13_146"
      target_id "M13_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 36
    source 2
    target 4
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_41"
      target_id "M13_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 37
    source 4
    target 5
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "TRIGGER"
      source_id "M13_20"
      target_id "M13_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 38
    source 6
    target 5
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_142"
      target_id "M13_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 39
    source 7
    target 5
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "TRIGGER"
      source_id "M13_17"
      target_id "M13_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 40
    source 8
    target 5
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "TRIGGER"
      source_id "M13_10"
      target_id "M13_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 41
    source 5
    target 9
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_86"
      target_id "M13_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 42
    source 10
    target 6
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_81"
      target_id "M13_142"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 43
    source 11
    target 10
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_156"
      target_id "M13_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 44
    source 12
    target 10
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "TRIGGER"
      source_id "M13_143"
      target_id "M13_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 45
    source 13
    target 10
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "TRIGGER"
      source_id "M13_15"
      target_id "M13_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 46
    source 14
    target 10
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "INHIBITION"
      source_id "M13_157"
      target_id "M13_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 47
    source 23
    target 11
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_80"
      target_id "M13_156"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 48
    source 16
    target 11
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_40"
      target_id "M13_156"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 49
    source 11
    target 27
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_156"
      target_id "M13_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 50
    source 11
    target 15
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_156"
      target_id "M13_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 51
    source 12
    target 22
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_143"
      target_id "M13_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 52
    source 12
    target 16
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_143"
      target_id "M13_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 53
    source 15
    target 14
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_39"
      target_id "M13_157"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 54
    source 14
    target 16
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_157"
      target_id "M13_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 55
    source 25
    target 15
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_144"
      target_id "M13_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 56
    source 17
    target 16
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_13"
      target_id "M13_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 57
    source 18
    target 16
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "TRIGGER"
      source_id "M13_11"
      target_id "M13_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 58
    source 18
    target 19
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_11"
      target_id "M13_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 59
    source 19
    target 20
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_85"
      target_id "M13_145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 60
    source 21
    target 20
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_82"
      target_id "M13_145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 61
    source 22
    target 20
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_79"
      target_id "M13_145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 62
    source 20
    target 23
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_145"
      target_id "M13_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 63
    source 26
    target 21
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_16"
      target_id "M13_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 64
    source 24
    target 23
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_158"
      target_id "M13_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 25
    target 23
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "INHIBITION"
      source_id "M13_144"
      target_id "M13_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 28
    target 27
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_12"
      target_id "M13_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 29
    target 27
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_173"
      target_id "M13_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 27
    target 30
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_84"
      target_id "M13_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 69
    source 29
    target 31
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CONSPUMPTION"
      source_id "M13_173"
      target_id "M13_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 70
    source 32
    target 31
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "CATALYSIS"
      source_id "M13_174"
      target_id "M13_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 71
    source 31
    target 33
    cd19dm [
      diagram "C19DMap:Electron Transport Chain disruption"
      edge_type "PRODUCTION"
      source_id "M13_50"
      target_id "M13_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
