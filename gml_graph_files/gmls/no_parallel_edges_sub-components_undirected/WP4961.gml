# generated with VANTED V2.8.2 at Fri Mar 04 09:53:04 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000184584"
      hgnc "NA"
      map_id "W20_11"
      name "STING1"
      node_subtype "GENE"
      node_type "species"
      org_id "c14f2"
      uniprot "NA"
    ]
    graphics [
      x 602.5473999832047
      y 729.0834979846039
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_25"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "e0e13"
      uniprot "NA"
    ]
    graphics [
      x 737.3651418059954
      y 685.8895261127781
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000162711"
      hgnc "NA"
      map_id "W20_36"
      name "NLRP3"
      node_subtype "GENE"
      node_type "species"
      org_id "f72d3"
      uniprot "NA"
    ]
    graphics [
      x 860.0180240773803
      y 645.0751356888395
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_38"
      name "Cytosolic_space_DNA_br_(viral_space_DNA_space_and_space_damaged_space_mtDNA)"
      node_subtype "GENE"
      node_type "species"
      org_id "fa31c"
      uniprot "NA"
    ]
    graphics [
      x 1032.5229659784254
      y 147.92904956182588
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_40"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "fc1c4"
      uniprot "NA"
    ]
    graphics [
      x 925.9988643401003
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000164430"
      hgnc "NA"
      map_id "W20_2"
      name "CGAS"
      node_subtype "GENE"
      node_type "species"
      org_id "a9c5b"
      uniprot "NA"
    ]
    graphics [
      x 775.4261789588944
      y 74.99934824101206
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000150995"
      hgnc "NA"
      map_id "W20_30"
      name "ITPR1"
      node_subtype "GENE"
      node_type "species"
      org_id "ea20e"
      uniprot "NA"
    ]
    graphics [
      x 836.7688211854116
      y 963.7042888272883
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_49"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idab07d001"
      uniprot "NA"
    ]
    graphics [
      x 928.9627383524922
      y 1093.5202412653312
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_22"
      name "Calcium_space_release"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "d8293"
      uniprot "NA"
    ]
    graphics [
      x 970.5059480042452
      y 1247.7495758220577
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "cc8bf"
      uniprot "NA"
    ]
    graphics [
      x 984.8134392955121
      y 1405.7983730200049
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000104518"
      hgnc "NA"
      map_id "W20_24"
      name "GSDMD"
      node_subtype "GENE"
      node_type "species"
      org_id "dcc65"
      uniprot "NA"
    ]
    graphics [
      x 1051.1998094241444
      y 1529.70670100317
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_26"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "e393d"
      uniprot "NA"
    ]
    graphics [
      x 477.51113121831077
      y 875.7965405045578
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000263528;urn:miriam:ensembl:ENSG00000183735"
      hgnc "NA"
      map_id "W20_39"
      name "fb896"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "fb896"
      uniprot "NA"
    ]
    graphics [
      x 381.8567916944352
      y 1025.6796013674298
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000160703"
      hgnc "NA"
      map_id "W20_4"
      name "NLRX1"
      node_subtype "GENE"
      node_type "species"
      org_id "ab61d"
      uniprot "NA"
    ]
    graphics [
      x 602.8885125954521
      y 962.8783613573027
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_16"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "ca979"
      uniprot "NA"
    ]
    graphics [
      x 594.5360050497734
      y 847.5443804729944
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000126456"
      hgnc "NA"
      map_id "W20_32"
      name "IRF3"
      node_subtype "GENE"
      node_type "species"
      org_id "f0259"
      uniprot "NA"
    ]
    graphics [
      x 91.38152240860222
      y 852.2072058431019
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_52"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idf594d3e0"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 694.3539261107323
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000126456"
      hgnc "NA"
      map_id "W20_29"
      name "IRF3"
      node_subtype "GENE"
      node_type "species"
      org_id "e9185"
      uniprot "NA"
    ]
    graphics [
      x 102.54291958356498
      y 546.3429444572334
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000109320;urn:miriam:ensembl:ENSG00000100906;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ensembl:ENSG00000146232;urn:miriam:ensembl:ENSG00000162924"
      hgnc "NA"
      map_id "W20_28"
      name "e7a9d"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e7a9d"
      uniprot "NA"
    ]
    graphics [
      x 1059.9832415123549
      y 1064.3324179893689
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_7"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "b3b9f"
      uniprot "NA"
    ]
    graphics [
      x 1216.2022637633886
      y 1080.0957876533237
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000173039;urn:miriam:ensembl:ENSG00000162924;urn:miriam:ensembl:ENSG00000109320"
      hgnc "NA"
      map_id "W20_12"
      name "c329d"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c329d"
      uniprot "NA"
    ]
    graphics [
      x 1358.2440747052956
      y 1072.498986799326
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000173039;urn:miriam:ensembl:ENSG00000100906;urn:miriam:ensembl:ENSG00000146232;urn:miriam:ensembl:ENSG00000109320;urn:miriam:ensembl:ENSG00000162924"
      hgnc "NA"
      map_id "W20_13"
      name "c72d7"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c72d7"
      uniprot "NA"
    ]
    graphics [
      x 809.1423004958006
      y 1242.9713774006693
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_17"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "cb0f5"
      uniprot "NA"
    ]
    graphics [
      x 855.1679640241531
      y 1135.2508193528324
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000269335;urn:miriam:ensembl:ENSG00000104365;urn:miriam:ensembl:ENSG00000213341"
      hgnc "NA"
      map_id "W20_44"
      name "fef8c"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "fef8c"
      uniprot "NA"
    ]
    graphics [
      x 680.080626707955
      y 1149.272895030183
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A71580"
      hgnc "NA"
      map_id "W20_10"
      name "cGAMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "bd7f8"
      uniprot "NA"
    ]
    graphics [
      x 609.8523771138528
      y 342.87662059301823
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_37"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f8097"
      uniprot "NA"
    ]
    graphics [
      x 599.4362476145869
      y 518.9333269265296
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A2719"
      hgnc "NA"
      map_id "W20_31"
      name "Angiotensin_space_II"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "ecfbd"
      uniprot "NA"
    ]
    graphics [
      x 615.2038574985611
      y 613.4475198418368
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_53"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idf6aa73a2"
      uniprot "NA"
    ]
    graphics [
      x 508.1158948434306
      y 648.0548904402149
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_20"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "d60dd"
      uniprot "NA"
    ]
    graphics [
      x 173.493339140138
      y 423.76302486514226
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000171855"
      hgnc "NA"
      map_id "W20_27"
      name "IFNB1"
      node_subtype "GENE"
      node_type "species"
      org_id "e699d"
      uniprot "NA"
    ]
    graphics [
      x 275.51897340057377
      y 346.79385115512764
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_48"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id9ea5ec2d"
      uniprot "NA"
    ]
    graphics [
      x 200.81216421590625
      y 975.0546150244445
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_41"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "fc9fd"
      uniprot "NA"
    ]
    graphics [
      x 1487.2234952181316
      y 1076.7910939239118
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_8"
      name "Cytokines"
      node_subtype "GENE"
      node_type "species"
      org_id "b9d10"
      uniprot "NA"
    ]
    graphics [
      x 1455.5584863283511
      y 1183.997604607644
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_1"
      name "Pyroptosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "a367e"
      uniprot "NA"
    ]
    graphics [
      x 1251.1249783273101
      y 1473.9194941599121
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f3a2a"
      uniprot "NA"
    ]
    graphics [
      x 1184.196282650325
      y 1378.11801997874
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000117525"
      hgnc "NA"
      map_id "W20_5"
      name "F3"
      node_subtype "GENE"
      node_type "species"
      org_id "ae651"
      uniprot "NA"
    ]
    graphics [
      x 1074.649325476144
      y 1310.0901871412393
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_23"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "d99fa"
      uniprot "NA"
    ]
    graphics [
      x 1163.3773280283071
      y 945.5370685738058
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000100906;urn:miriam:ensembl:ENSG00000146232"
      hgnc "NA"
      map_id "W20_19"
      name "d1991"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d1991"
      uniprot "NA"
    ]
    graphics [
      x 1259.4200733090672
      y 835.2716367930602
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_3"
      name "Intravenous_br_immunoglobulins"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "aa4ea"
      uniprot "NA"
    ]
    graphics [
      x 190.11905313575835
      y 740.7177451544169
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_45"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id231333be"
      uniprot "NA"
    ]
    graphics [
      x 292.98355888047723
      y 730.9900956620947
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000143226"
      hgnc "NA"
      map_id "W20_35"
      name "FCGR2A"
      node_subtype "GENE"
      node_type "species"
      org_id "f7129"
      uniprot "NA"
    ]
    graphics [
      x 279.64970432122374
      y 850.4506985060798
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_14"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "c76d4"
      uniprot "NA"
    ]
    graphics [
      x 977.6352880435679
      y 615.4854504601111
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:ensembl:ENSG00000125538"
      hgnc "NA"
      map_id "W20_15"
      name "IL1B"
      node_subtype "GENE"
      node_type "species"
      org_id "c9a82"
      uniprot "NA"
    ]
    graphics [
      x 1054.8023935341025
      y 698.0452561718896
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id8f3d31d3"
      uniprot "NA"
    ]
    graphics [
      x 653.832196958112
      y 176.85457577516092
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A28940"
      hgnc "NA"
      map_id "W20_43"
      name "Vitamin_space_D3"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "fef60"
      uniprot "NA"
    ]
    graphics [
      x 717.9263308775807
      y 264.51198961136254
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_46"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id394406f9"
      uniprot "NA"
    ]
    graphics [
      x 1172.3647714493572
      y 1574.3476597774602
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_51"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idcfcc5c05"
      uniprot "NA"
    ]
    graphics [
      x 290.01383031316567
      y 971.0391947567198
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15365"
      hgnc "NA"
      map_id "W20_42"
      name "Aspirin"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "feddd"
      uniprot "NA"
    ]
    graphics [
      x 910.0346944216102
      y 253.1905069609462
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_50"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idcde3e513"
      uniprot "NA"
    ]
    graphics [
      x 862.4404038062795
      y 149.657560726116
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_33"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f375a"
      uniprot "NA"
    ]
    graphics [
      x 1314.1755525176445
      y 712.7318181897817
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_6"
      name "NA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "b2dd0"
      uniprot "NA"
    ]
    graphics [
      x 1260.6368582121402
      y 599.9887286069263
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_21"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "d73de"
      uniprot "NA"
    ]
    graphics [
      x 730.7880370348204
      y 844.2272222071456
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4961"
      full_annotation "NA"
      hgnc "NA"
      map_id "W20_9"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "bc321"
      uniprot "NA"
    ]
    graphics [
      x 517.3116837909263
      y 1119.7787525431672
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W20_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 54
    source 1
    target 2
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "W20_11"
      target_id "W20_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 55
    source 2
    target 3
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_25"
      target_id "W20_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 56
    source 4
    target 5
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "W20_38"
      target_id "W20_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 57
    source 5
    target 6
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_40"
      target_id "W20_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 58
    source 7
    target 8
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "W20_30"
      target_id "W20_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 59
    source 8
    target 9
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_49"
      target_id "W20_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 60
    source 9
    target 10
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "W20_22"
      target_id "W20_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 61
    source 10
    target 11
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_18"
      target_id "W20_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 62
    source 1
    target 12
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "W20_11"
      target_id "W20_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 63
    source 12
    target 13
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_26"
      target_id "W20_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 64
    source 14
    target 15
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "W20_4"
      target_id "W20_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 15
    target 1
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_16"
      target_id "W20_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 16
    target 17
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "W20_32"
      target_id "W20_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 17
    target 18
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_52"
      target_id "W20_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 19
    target 20
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "W20_28"
      target_id "W20_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 69
    source 20
    target 21
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_7"
      target_id "W20_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 70
    source 22
    target 23
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "W20_13"
      target_id "W20_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 71
    source 24
    target 23
    cd19dm [
      diagram "WP4961"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W20_44"
      target_id "W20_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 72
    source 23
    target 19
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_17"
      target_id "W20_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 73
    source 25
    target 26
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "W20_10"
      target_id "W20_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 74
    source 26
    target 1
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_37"
      target_id "W20_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 75
    source 27
    target 28
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "W20_31"
      target_id "W20_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 76
    source 28
    target 1
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_53"
      target_id "W20_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 77
    source 18
    target 29
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "W20_29"
      target_id "W20_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 78
    source 29
    target 30
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_20"
      target_id "W20_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 79
    source 13
    target 31
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "W20_39"
      target_id "W20_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 31
    target 16
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_48"
      target_id "W20_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 81
    source 21
    target 32
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "W20_12"
      target_id "W20_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 82
    source 32
    target 33
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_41"
      target_id "W20_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 83
    source 34
    target 35
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "W20_1"
      target_id "W20_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 84
    source 35
    target 36
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_34"
      target_id "W20_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 85
    source 19
    target 37
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "W20_28"
      target_id "W20_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 86
    source 37
    target 38
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_23"
      target_id "W20_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 87
    source 39
    target 40
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "W20_3"
      target_id "W20_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 88
    source 40
    target 41
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_45"
      target_id "W20_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 89
    source 3
    target 42
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "W20_36"
      target_id "W20_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 90
    source 42
    target 43
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_14"
      target_id "W20_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 91
    source 6
    target 44
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "W20_2"
      target_id "W20_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 92
    source 45
    target 44
    cd19dm [
      diagram "WP4961"
      edge_type "INHIBITION"
      source_id "W20_43"
      target_id "W20_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 93
    source 44
    target 25
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_47"
      target_id "W20_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 94
    source 11
    target 46
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "W20_24"
      target_id "W20_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 95
    source 46
    target 34
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_46"
      target_id "W20_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 96
    source 41
    target 47
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "W20_35"
      target_id "W20_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 97
    source 47
    target 13
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_51"
      target_id "W20_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 98
    source 48
    target 49
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "W20_42"
      target_id "W20_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 99
    source 49
    target 6
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_50"
      target_id "W20_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 100
    source 38
    target 50
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "W20_19"
      target_id "W20_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 50
    target 51
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_33"
      target_id "W20_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 1
    target 52
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "W20_11"
      target_id "W20_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 52
    target 7
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_21"
      target_id "W20_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 13
    target 53
    cd19dm [
      diagram "WP4961"
      edge_type "CONSPUMPTION"
      source_id "W20_39"
      target_id "W20_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 53
    target 24
    cd19dm [
      diagram "WP4961"
      edge_type "PRODUCTION"
      source_id "W20_9"
      target_id "W20_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
