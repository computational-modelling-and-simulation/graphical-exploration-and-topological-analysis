# generated with VANTED V2.8.2 at Fri Mar 04 10:04:35 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_13"
      name "Apoptosome"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ca06b"
      uniprot "NA"
    ]
    graphics [
      x 1205.2184881456876
      y 782.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_42"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idb82c8f11"
      uniprot "NA"
    ]
    graphics [
      x 1502.6016867009223
      y 873.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_26"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id23a5b04e"
      uniprot "NA"
    ]
    graphics [
      x 1044.9001423498903
      y 452.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_38"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id9d25fe73"
      uniprot "NA"
    ]
    graphics [
      x 907.9537821199187
      y 1073.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_28"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id332625ef"
      uniprot "NA"
    ]
    graphics [
      x 1510.4536808090763
      y 1170.9044512172738
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "urn:miriam:ensembl:ENSG00000164305;urn:miriam:ensembl:ENSG00000165806"
      hgnc "NA"
      map_id "W8_10"
      name "bbd5b"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "bbd5b"
      uniprot "NA"
    ]
    graphics [
      x 1875.1861638094097
      y 1545.104735476776
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_30"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id42dbcb94"
      uniprot "NA"
    ]
    graphics [
      x 1750.6116425685227
      y 1773.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_35"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id67844ae8"
      uniprot "NA"
    ]
    graphics [
      x 1986.5573446818603
      y 843.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_41"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idb0a41cb7"
      uniprot "NA"
    ]
    graphics [
      x 1581.4000079142156
      y 2133.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_16"
      name "Apoptosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "d1a8d"
      uniprot "NA"
    ]
    graphics [
      x 1150.9288685419035
      y 2432.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_40"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idad374e81"
      uniprot "NA"
    ]
    graphics [
      x 1143.990140247231
      y 2282.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_44"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "ide4442266"
      uniprot "NA"
    ]
    graphics [
      x 1397.2351379193215
      y 2162.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "urn:miriam:ncbigene:207"
      hgnc "NA"
      map_id "W8_11"
      name "AKT1"
      node_subtype "GENE"
      node_type "species"
      org_id "bfe85"
      uniprot "NA"
    ]
    graphics [
      x 1800.3294408623765
      y 1533.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_37"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id812e8e5d"
      uniprot "NA"
    ]
    graphics [
      x 2116.367176027826
      y 1125.104735476776
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "urn:miriam:uniprot:P59596"
      hgnc "NA"
      map_id "W8_6"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b1ff7"
      uniprot "UNIPROT:P59596"
    ]
    graphics [
      x 2104.8340829311323
      y 916.7568868046694
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "urn:miriam:uniprot:P59595;urn:miriam:uniprot:Q7TFA0;urn:miriam:uniprot:P59636;urn:miriam:uniprot:P59634;urn:miriam:uniprot:P59637;urn:miriam:uniprot:P59633;urn:miriam:uniprot:P59594"
      hgnc "NA"
      map_id "W8_24"
      name "f5b62"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f5b62"
      uniprot "UNIPROT:P59595;UNIPROT:Q7TFA0;UNIPROT:P59636;UNIPROT:P59634;UNIPROT:P59637;UNIPROT:P59633;UNIPROT:P59594"
    ]
    graphics [
      x 834.3582268982625
      y 2072.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_2"
      name "OC43_br_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "a5a4a"
      uniprot "NA"
    ]
    graphics [
      x 1846.769568075647
      y 443.5706859687666
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_12"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "c78a8"
      uniprot "NA"
    ]
    graphics [
      x 1700.5843841930148
      y 199.49884152663162
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "urn:miriam:ensembl:ENSG00000132906"
      hgnc "NA"
      map_id "W8_14"
      name "CASP9"
      node_subtype "GENE"
      node_type "species"
      org_id "cacde"
      uniprot "NA"
    ]
    graphics [
      x 1668.0108517848698
      y 422.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "urn:miriam:ensembl:ENSG00000064012"
      hgnc "NA"
      map_id "W8_25"
      name "CASP8"
      node_subtype "GENE"
      node_type "species"
      org_id "f60b1"
      uniprot "NA"
    ]
    graphics [
      x 1412.8793892601886
      y 1622.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_33"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id54c6a1c1"
      uniprot "NA"
    ]
    graphics [
      x 1710.3753793280857
      y 1173.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "PUBMED:9727492"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id5ea6cbdd"
      uniprot "NA"
    ]
    graphics [
      x 713.2753057692557
      y 1673.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "urn:miriam:ensembl:ENSG00000015475"
      hgnc "NA"
      map_id "W8_4"
      name "BID"
      node_subtype "GENE"
      node_type "species"
      org_id "aff48"
      uniprot "NA"
    ]
    graphics [
      x 680.2672566420472
      y 1885.8429747656378
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "urn:miriam:uniprot:PRO_0000223233;urn:miriam:pubmed:9727492"
      hgnc "NA"
      map_id "W8_21"
      name "tBID"
      node_subtype "GENE"
      node_type "species"
      org_id "ef970"
      uniprot "UNIPROT:PRO_0000223233"
    ]
    graphics [
      x 687.0997834123543
      y 1223.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_32"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id4d5fb94c"
      uniprot "NA"
    ]
    graphics [
      x 568.8806415049567
      y 1488.281291735611
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "urn:miriam:ensembl:ENSG00000087088"
      hgnc "NA"
      map_id "W8_18"
      name "BAX"
      node_subtype "GENE"
      node_type "species"
      org_id "dbfc4"
      uniprot "NA"
    ]
    graphics [
      x 1042.92395945198
      y 1686.5222752501516
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_3"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "a8baf"
      uniprot "NA"
    ]
    graphics [
      x 1330.7729750819085
      y 2012.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "urn:miriam:ensembl:ENSG00000171791;urn:miriam:ensembl:ENSG00000143384;urn:miriam:ensembl:ENSG00000171552"
      hgnc "NA"
      map_id "W8_17"
      name "d5ccc"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d5ccc"
      uniprot "NA"
    ]
    graphics [
      x 1450.2172877618877
      y 1802.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_43"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idda923b3f"
      uniprot "NA"
    ]
    graphics [
      x 870.2206558478019
      y 1545.1653488559077
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_36"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id6ca195ae"
      uniprot "NA"
    ]
    graphics [
      x 1937.518266077193
      y 1340.006361208802
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "urn:miriam:ensembl:ENSG00000105327;urn:miriam:ensembl:ENSG00000002330;urn:miriam:ensembl:ENSG00000153094"
      hgnc "NA"
      map_id "W8_8"
      name "b7dd0"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b7dd0"
      uniprot "NA"
    ]
    graphics [
      x 2189.4405633543174
      y 1226.5258104936293
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "urn:miriam:ensembl:ENSG00000087088"
      hgnc "NA"
      map_id "W8_9"
      name "BAX"
      node_subtype "GENE"
      node_type "species"
      org_id "bb36d"
      uniprot "NA"
    ]
    graphics [
      x 1738.8081249068287
      y 1023.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_39"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id9d6e5912"
      uniprot "NA"
    ]
    graphics [
      x 1426.1167461621371
      y 872.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_15"
      name "OC43_space_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "cdc80"
      uniprot "NA"
    ]
    graphics [
      x 1199.985974681931
      y 872.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "urn:miriam:uniprot:P59635;urn:miriam:uniprot:P59637"
      hgnc "NA"
      map_id "W8_19"
      name "dcc55"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "dcc55"
      uniprot "UNIPROT:P59635;UNIPROT:P59637"
    ]
    graphics [
      x 506.1266337102262
      y 1368.281291735611
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "urn:miriam:ensembl:ENSG00000168040"
      hgnc "NA"
      map_id "W8_5"
      name "FADD"
      node_subtype "GENE"
      node_type "species"
      org_id "b16d4"
      uniprot "NA"
    ]
    graphics [
      x 2036.6674409907146
      y 987.9197193482314
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_27"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id26c91cfd"
      uniprot "NA"
    ]
    graphics [
      x 2041.9488622672434
      y 729.505194455975
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "urn:miriam:ensembl:ENSG00000232810;urn:miriam:ensembl:ENSG00000117560"
      hgnc "NA"
      map_id "W8_1"
      name "a042f"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "a042f"
      uniprot "NA"
    ]
    graphics [
      x 2084.5443167420835
      y 514.6137470729877
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18070"
      hgnc "NA"
      map_id "W8_23"
      name "Cytochrome_space_C"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "f4f84"
      uniprot "NA"
    ]
    graphics [
      x 799.6135915227404
      y 1140.9085006241935
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_31"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id44e385eb"
      uniprot "NA"
    ]
    graphics [
      x 667.5602412829403
      y 1103.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "urn:miriam:ensembl:ENSG00000188130;urn:miriam:ensembl:ENSG00000156711;urn:miriam:ensembl:ENSG00000185386;urn:miriam:ensembl:ENSG00000112062"
      hgnc "NA"
      map_id "W8_20"
      name "e188e"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e188e"
      uniprot "NA"
    ]
    graphics [
      x 739.1401812965644
      y 1373.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4864"
      full_annotation "NA"
      hgnc "NA"
      map_id "W8_29"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id3bbd5b9"
      uniprot "NA"
    ]
    graphics [
      x 811.4953591968886
      y 1772.024986525728
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "urn:miriam:uniprot:P59632"
      hgnc "NA"
      map_id "W8_7"
      name "3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b5cfb"
      uniprot "UNIPROT:P59632"
    ]
    graphics [
      x 1020.6819430412633
      y 1922.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4864"
      full_annotation "urn:miriam:ensembl:ENSG00000120868"
      hgnc "NA"
      map_id "W8_22"
      name "APAF1"
      node_subtype "GENE"
      node_type "species"
      org_id "f2fff"
      uniprot "NA"
    ]
    graphics [
      x 1194.3027999992698
      y 542.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W8_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 45
    source 2
    target 1
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_42"
      target_id "W8_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 46
    source 3
    target 1
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_26"
      target_id "W8_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 47
    source 4
    target 1
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_38"
      target_id "W8_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 48
    source 1
    target 5
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "W8_13"
      target_id "W8_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 49
    source 19
    target 2
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "W8_14"
      target_id "W8_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 50
    source 44
    target 3
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "W8_22"
      target_id "W8_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 51
    source 39
    target 4
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "W8_23"
      target_id "W8_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 52
    source 5
    target 6
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_28"
      target_id "W8_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 53
    source 7
    target 6
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_30"
      target_id "W8_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 54
    source 8
    target 6
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_35"
      target_id "W8_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 55
    source 6
    target 9
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "W8_10"
      target_id "W8_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 56
    source 20
    target 7
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "W8_25"
      target_id "W8_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 57
    source 17
    target 8
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "W8_2"
      target_id "W8_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 58
    source 9
    target 10
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_41"
      target_id "W8_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 59
    source 11
    target 10
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_40"
      target_id "W8_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 60
    source 12
    target 10
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_44"
      target_id "W8_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 61
    source 16
    target 11
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "W8_24"
      target_id "W8_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 62
    source 13
    target 12
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "W8_11"
      target_id "W8_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 63
    source 14
    target 13
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_37"
      target_id "W8_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 64
    source 15
    target 14
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "W8_6"
      target_id "W8_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 17
    target 18
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "W8_2"
      target_id "W8_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 18
    target 19
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_12"
      target_id "W8_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 21
    target 20
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_33"
      target_id "W8_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 20
    target 22
    cd19dm [
      diagram "WP4864"
      edge_type "CATALYSIS"
      source_id "W8_25"
      target_id "W8_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 69
    source 36
    target 21
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "W8_5"
      target_id "W8_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 70
    source 23
    target 22
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "W8_4"
      target_id "W8_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 71
    source 22
    target 24
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_34"
      target_id "W8_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 72
    source 24
    target 25
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "W8_21"
      target_id "W8_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 73
    source 25
    target 26
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_32"
      target_id "W8_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 74
    source 27
    target 26
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_3"
      target_id "W8_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 75
    source 28
    target 27
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "W8_17"
      target_id "W8_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 76
    source 29
    target 28
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_43"
      target_id "W8_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 77
    source 28
    target 30
    cd19dm [
      diagram "WP4864"
      edge_type "INHIBITION"
      source_id "W8_17"
      target_id "W8_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 78
    source 35
    target 29
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "W8_19"
      target_id "W8_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 79
    source 31
    target 30
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "W8_8"
      target_id "W8_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 30
    target 32
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_36"
      target_id "W8_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 81
    source 33
    target 32
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_39"
      target_id "W8_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 82
    source 34
    target 33
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "W8_15"
      target_id "W8_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 83
    source 37
    target 36
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_27"
      target_id "W8_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 84
    source 38
    target 37
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "W8_1"
      target_id "W8_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 85
    source 40
    target 39
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_31"
      target_id "W8_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 86
    source 41
    target 40
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "W8_20"
      target_id "W8_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 87
    source 42
    target 41
    cd19dm [
      diagram "WP4864"
      edge_type "PRODUCTION"
      source_id "W8_29"
      target_id "W8_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 88
    source 43
    target 42
    cd19dm [
      diagram "WP4864"
      edge_type "CONSPUMPTION"
      source_id "W8_7"
      target_id "W8_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
