# generated with VANTED V2.8.2 at Fri Mar 04 10:04:36 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:ensembl:ENSG00000126456"
      hgnc "NA"
      map_id "W15_15"
      name "ea539"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ea539"
      uniprot "NA"
    ]
    graphics [
      x 1467.2809781724031
      y 2432.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idbcd773be"
      uniprot "NA"
    ]
    graphics [
      x 1273.5867388467234
      y 2552.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "PUBMED:15681410"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_36"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "ided8e62a3"
      uniprot "NA"
    ]
    graphics [
      x 1610.2239984829444
      y 2103.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_7"
      name "IFN_minus_beta_br_Response_space_element"
      node_subtype "GENE"
      node_type "species"
      org_id "c7d27"
      uniprot "NA"
    ]
    graphics [
      x 1577.926320086893
      y 1533.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:ensembl:ENSG00000126456"
      hgnc "NA"
      map_id "W15_18"
      name "IRF3"
      node_subtype "GENE"
      node_type "species"
      org_id "f0053"
      uniprot "NA"
    ]
    graphics [
      x 1012.2601649203594
      y 2402.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:ensembl:ENSG00000131323;urn:miriam:ensembl:ENSG00000183735;urn:miriam:ensembl:ENSG00000263528"
      hgnc "NA"
      map_id "W15_12"
      name "d1b60"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d1b60"
      uniprot "NA"
    ]
    graphics [
      x 1299.8781762224862
      y 2102.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_38"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idf94c3c70"
      uniprot "NA"
    ]
    graphics [
      x 1254.7650783306426
      y 1532.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:ensembl:ENSG00000088888"
      hgnc "NA"
      map_id "W15_19"
      name "MAVS"
      node_subtype "GENE"
      node_type "species"
      org_id "f0e60"
      uniprot "NA"
    ]
    graphics [
      x 1307.5121412826727
      y 1892.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_27"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id3573c07"
      uniprot "NA"
    ]
    graphics [
      x 1419.154715910524
      y 2402.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:ensembl:ENSG00000115267;urn:miriam:ensembl:ENSG00000107201"
      hgnc "NA"
      map_id "W15_4"
      name "b9f09"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b9f09"
      uniprot "NA"
    ]
    graphics [
      x 1612.4810021007875
      y 2482.028353752775
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_16"
      name "NA"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "ecbcf"
      uniprot "NA"
    ]
    graphics [
      x 1267.174354690558
      y 2342.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_31"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id95166c5e"
      uniprot "NA"
    ]
    graphics [
      x 1094.9660346788132
      y 2612.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:ncbigene:148022"
      hgnc "NA"
      map_id "W15_23"
      name "TICAM1"
      node_subtype "GENE"
      node_type "species"
      org_id "f9104"
      uniprot "NA"
    ]
    graphics [
      x 1272.2133712160248
      y 2492.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 14
    source 2
    target 1
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_34"
      target_id "W15_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 15
    source 1
    target 3
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "W15_15"
      target_id "W15_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 16
    source 5
    target 2
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "W15_18"
      target_id "W15_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 17
    source 6
    target 2
    cd19dm [
      diagram "WP4912"
      edge_type "CATALYSIS"
      source_id "W15_12"
      target_id "W15_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 18
    source 3
    target 4
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_36"
      target_id "W15_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 19
    source 12
    target 5
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_31"
      target_id "W15_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 20
    source 7
    target 6
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_38"
      target_id "W15_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 21
    source 8
    target 7
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "W15_19"
      target_id "W15_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 22
    source 9
    target 8
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_27"
      target_id "W15_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 23
    source 10
    target 9
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "W15_4"
      target_id "W15_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 24
    source 11
    target 9
    cd19dm [
      diagram "WP4912"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W15_16"
      target_id "W15_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 25
    source 13
    target 12
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "W15_23"
      target_id "W15_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
