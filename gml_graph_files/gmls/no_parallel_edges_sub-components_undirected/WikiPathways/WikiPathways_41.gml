# generated with VANTED V2.8.2 at Fri Mar 04 10:04:36 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:uniprot:P59633;urn:miriam:wikidata:Q89458416"
      hgnc "NA"
      map_id "W9_32"
      name "ee4e9"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ee4e9"
      uniprot "UNIPROT:P59633"
    ]
    graphics [
      x 1114.5682560119185
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_62"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id81131143"
      uniprot "NA"
    ]
    graphics [
      x 1416.8894320897243
      y 122.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ncbigene:5595;urn:miriam:ncbigene:5594"
      hgnc "NA"
      map_id "W9_38"
      name "f9084"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f9084"
      uniprot "NA"
    ]
    graphics [
      x 1831.4757726430807
      y 496.4202426509187
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_59"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id7c30bf1f"
      uniprot "NA"
    ]
    graphics [
      x 2145.8765680596584
      y 855.1047354767761
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_67"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idc07c34c7"
      uniprot "NA"
    ]
    graphics [
      x 1675.131338473034
      y 753.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_64"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "ida2a9cdb0"
      uniprot "NA"
    ]
    graphics [
      x 1706.998968586627
      y 933.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_77"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "ideee7f970"
      uniprot "NA"
    ]
    graphics [
      x 1466.8529870749578
      y 752.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_40"
      name "SARS_br_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "f9996"
      uniprot "NA"
    ]
    graphics [
      x 1501.0104048966764
      y 1008.592740722627
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000117676;urn:miriam:ensembl:ENSG00000177189;urn:miriam:ensembl:ENSG00000071242"
      hgnc "NA"
      map_id "W9_24"
      name "dd506"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "dd506"
      uniprot "NA"
    ]
    graphics [
      x 1071.2401662335915
      y 1112.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_53"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id589b86fe"
      uniprot "NA"
    ]
    graphics [
      x 1455.6270924163025
      y 1170.9044512172738
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_68"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idc1c0f088"
      uniprot "NA"
    ]
    graphics [
      x 1203.4607920861758
      y 1322.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_56"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id63f652b7"
      uniprot "NA"
    ]
    graphics [
      x 643.3445805458232
      y 1163.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_3"
      name "Protein_br_synthesis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "a5ffe"
      uniprot "NA"
    ]
    graphics [
      x 418.29729455242716
      y 886.2360652828256
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_47"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id392d909b"
      uniprot "NA"
    ]
    graphics [
      x 612.5721346727931
      y 726.9065679735925
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000151247"
      hgnc "NA"
      map_id "W9_20"
      name "EIF4E"
      node_subtype "GENE"
      node_type "species"
      org_id "ce80a"
      uniprot "NA"
    ]
    graphics [
      x 827.7599449613554
      y 833.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_49"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id44f6de9d"
      uniprot "NA"
    ]
    graphics [
      x 1221.077357567352
      y 1105.8881790957905
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ncbigene:5600;urn:miriam:ncbigene:6300;urn:miriam:ensembl:ENSG00000112062;urn:miriam:ncbigene:5603"
      hgnc "NA"
      map_id "W9_37"
      name "f53ff"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f53ff"
      uniprot "NA"
    ]
    graphics [
      x 1794.6784317264103
      y 1312.5654308722508
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_58"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id79ad2aef"
      uniprot "NA"
    ]
    graphics [
      x 2132.3382014291938
      y 1643.5989309397503
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_70"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idcac13c17"
      uniprot "NA"
    ]
    graphics [
      x 2123.9647178655046
      y 1490.7213110276348
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_42"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id198c85f"
      uniprot "NA"
    ]
    graphics [
      x 2024.9829442643331
      y 1335.104735476776
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_57"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id6fd5bed2"
      uniprot "NA"
    ]
    graphics [
      x 1599.9741112088116
      y 1653.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_54"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id60fca41b"
      uniprot "NA"
    ]
    graphics [
      x 1537.248343032482
      y 602.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000175197"
      hgnc "NA"
      map_id "W9_1"
      name "DDIT3"
      node_subtype "GENE"
      node_type "species"
      org_id "a158c"
      uniprot "NA"
    ]
    graphics [
      x 1256.3269098369526
      y 182.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_73"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "ide06bd151"
      uniprot "NA"
    ]
    graphics [
      x 1114.1766398524946
      y 212.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_12"
      name "Apoptosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "be42e"
      uniprot "NA"
    ]
    graphics [
      x 1718.511643393
      y 291.00748584443136
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_61"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id81035407"
      uniprot "NA"
    ]
    graphics [
      x 2177.3372609037406
      y 615.1047354767761
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000171791"
      hgnc "NA"
      map_id "W9_27"
      name "BCL2"
      node_subtype "GENE"
      node_type "species"
      org_id "e21a3"
      uniprot "NA"
    ]
    graphics [
      x 2285.319522630737
      y 1300.14237199455
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_74"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "ide37bb3b6"
      uniprot "NA"
    ]
    graphics [
      x 1762.1092599425178
      y 1743.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_50"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id4b1d8b89"
      uniprot "NA"
    ]
    graphics [
      x 2174.5357757209485
      y 1005.1047354767761
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_19"
      name "Autophagy"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "cc8d3"
      uniprot "NA"
    ]
    graphics [
      x 1919.2065647505228
      y 566.2299452773914
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ncbigene:5599;urn:miriam:ncbigene:5601;urn:miriam:ncbigene:5602"
      hgnc "NA"
      map_id "W9_17"
      name "ca580"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ca580"
      uniprot "NA"
    ]
    graphics [
      x 1269.5001563914614
      y 1652.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_46"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id352f2389"
      uniprot "NA"
    ]
    graphics [
      x 1433.4797627137523
      y 1472.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_65"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idac31cf2f"
      uniprot "NA"
    ]
    graphics [
      x 794.1654813493047
      y 970.3774709048998
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_72"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "ide012a8a5"
      uniprot "NA"
    ]
    graphics [
      x 1515.5720831273009
      y 1380.9044512172738
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_51"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id54d4a9a9"
      uniprot "NA"
    ]
    graphics [
      x 1724.9295568595255
      y 1293.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_44"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id2520366d"
      uniprot "NA"
    ]
    graphics [
      x 824.8039788041117
      y 1742.024986525728
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000170345;urn:miriam:ensembl:ENSG00000177606;urn:miriam:ncbigene:3726"
      hgnc "NA"
      map_id "W9_22"
      name "d382f"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d382f"
      uniprot "NA"
    ]
    graphics [
      x 1152.8640565427565
      y 1622.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_75"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ide8866e40"
      uniprot "NA"
    ]
    graphics [
      x 1275.5017463301187
      y 1082.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_6"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ad2dd"
      uniprot "NA"
    ]
    graphics [
      x 1303.4246845769433
      y 1442.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ea117"
      uniprot "NA"
    ]
    graphics [
      x 912.8074735295725
      y 1712.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_52"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id559f5826"
      uniprot "NA"
    ]
    graphics [
      x 1267.1772587968117
      y 2132.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f1e34"
      uniprot "NA"
    ]
    graphics [
      x 566.9058142713636
      y 1428.281291735611
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_69"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idc72f872e"
      uniprot "NA"
    ]
    graphics [
      x 1467.2663291139895
      y 1587.1414895553223
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ncbigene:3429"
      hgnc "NA"
      map_id "W9_39"
      name "IFI27"
      node_subtype "GENE"
      node_type "species"
      org_id "f93d3"
      uniprot "NA"
    ]
    graphics [
      x 1682.1103196869137
      y 1541.7774858034927
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ncbigene:3429"
      hgnc "NA"
      map_id "W9_15"
      name "IFI27"
      node_subtype "GENE"
      node_type "species"
      org_id "c5ac8"
      uniprot "NA"
    ]
    graphics [
      x 1501.1967085577114
      y 1470.9044512172738
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ncbigene:684"
      hgnc "NA"
      map_id "W9_2"
      name "BST2"
      node_subtype "GENE"
      node_type "species"
      org_id "a24ed"
      uniprot "NA"
    ]
    graphics [
      x 790.3061023506914
      y 1283.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ncbigene:684"
      hgnc "NA"
      map_id "W9_8"
      name "BST2"
      node_subtype "GENE"
      node_type "species"
      org_id "b17a4"
      uniprot "NA"
    ]
    graphics [
      x 484.5059844625314
      y 1018.8798096866146
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_18"
      name "Innate_br_immunity"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "cc6a0"
      uniprot "NA"
    ]
    graphics [
      x 1089.5053550186562
      y 2042.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_48"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id3d6c0762"
      uniprot "NA"
    ]
    graphics [
      x 1109.1901375911616
      y 1682.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000115966;urn:miriam:ensembl:ENSG00000170345"
      hgnc "NA"
      map_id "W9_10"
      name "b90b1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b90b1"
      uniprot "NA"
    ]
    graphics [
      x 1521.2220595158788
      y 1440.9044512172738
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_60"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id80dca1f4"
      uniprot "NA"
    ]
    graphics [
      x 1581.3026333075832
      y 1623.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_23"
      name "Cell_space_survival_space__br_and_space_proliferation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "d8558"
      uniprot "NA"
    ]
    graphics [
      x 1376.3442854128962
      y 1592.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000185201"
      hgnc "NA"
      map_id "W9_5"
      name "IFITM2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "acf10"
      uniprot "NA"
    ]
    graphics [
      x 674.8812187847778
      y 1583.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000185201"
      hgnc "NA"
      map_id "W9_35"
      name "IFITM2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f247c"
      uniprot "NA"
    ]
    graphics [
      x 1025.5161288445702
      y 1753.641163269892
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000142089"
      hgnc "NA"
      map_id "W9_36"
      name "IFITM3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f50e2"
      uniprot "NA"
    ]
    graphics [
      x 1409.3045245570668
      y 1322.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000142089"
      hgnc "NA"
      map_id "W9_33"
      name "IFITM3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f1794"
      uniprot "NA"
    ]
    graphics [
      x 1375.882425257161
      y 1262.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000185885"
      hgnc "NA"
      map_id "W9_21"
      name "IFITM1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "cf41c"
      uniprot "NA"
    ]
    graphics [
      x 1315.7866507832437
      y 782.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000185885"
      hgnc "NA"
      map_id "W9_9"
      name "IFITM1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b8cbc"
      uniprot "NA"
    ]
    graphics [
      x 1361.0846432938297
      y 872.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_25"
      name "SARS,_space_229E_br_IBV_space_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "e046c"
      uniprot "NA"
    ]
    graphics [
      x 1795.142480800865
      y 873.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_76"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "ided0c8de7"
      uniprot "NA"
    ]
    graphics [
      x 1548.5668229506798
      y 692.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000076984"
      hgnc "NA"
      map_id "W9_4"
      name "MAP2K7"
      node_subtype "GENE"
      node_type "species"
      org_id "ac39a"
      uniprot "NA"
    ]
    graphics [
      x 1367.6650610944037
      y 842.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_45"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id264ad72f"
      uniprot "NA"
    ]
    graphics [
      x 1103.6423008492088
      y 512.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000173327;urn:miriam:ensembl:ENSG00000130758;urn:miriam:ensembl:ENSG00000006432"
      hgnc "NA"
      map_id "W9_11"
      name "b9bb1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b9bb1"
      uniprot "NA"
    ]
    graphics [
      x 894.409523072085
      y 272.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000065559"
      hgnc "NA"
      map_id "W9_13"
      name "MAP2K4"
      node_subtype "GENE"
      node_type "species"
      org_id "bed55"
      uniprot "NA"
    ]
    graphics [
      x 2215.9504536751447
      y 1554.0247407639072
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_63"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id9808cc66"
      uniprot "NA"
    ]
    graphics [
      x 2399.345491773142
      y 1587.571587666379
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000085511;urn:miriam:ensembl:ENSG00000095015"
      hgnc "NA"
      map_id "W9_16"
      name "c8921"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c8921"
      uniprot "NA"
    ]
    graphics [
      x 2014.7372705806795
      y 1605.104735476776
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_71"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idda26811d"
      uniprot "NA"
    ]
    graphics [
      x 1601.104879661717
      y 1443.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000034152;urn:miriam:ensembl:ENSG00000108984"
      hgnc "NA"
      map_id "W9_30"
      name "e6b7b"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e6b7b"
      uniprot "NA"
    ]
    graphics [
      x 1835.0687320071388
      y 1282.5654308722508
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_43"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id1b1215f4"
      uniprot "NA"
    ]
    graphics [
      x 2058.9987252433793
      y 1439.5343132758844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_28"
      name "SARS,_space_MERS,_space__br_229E_space_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "e5c80"
      uniprot "NA"
    ]
    graphics [
      x 2185.8674309016865
      y 1425.104735476776
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_78"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idfb2829ab"
      uniprot "NA"
    ]
    graphics [
      x 2430.7810381198087
      y 1669.9919928881518
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:uniprot:P59635;urn:miriam:uniprot:P59632"
      hgnc "NA"
      map_id "W9_26"
      name "e11b1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e11b1"
      uniprot "UNIPROT:P59635;UNIPROT:P59632"
    ]
    graphics [
      x 2265.684171847502
      y 1736.2316154258053
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:wikidata:Q89458416;urn:miriam:uniprot:P59632;urn:miriam:uniprot:P59635;urn:miriam:uniprot:P59634;urn:miriam:uniprot:P59633;urn:miriam:wikidata:Q89457519"
      hgnc "NA"
      map_id "W9_7"
      name "afd54"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "afd54"
      uniprot "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59634;UNIPROT:P59633"
    ]
    graphics [
      x 623.907732865594
      y 666.9065679735925
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000169032;urn:miriam:ensembl:ENSG00000126934"
      hgnc "NA"
      map_id "W9_29"
      name "e5ca8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e5ca8"
      uniprot "NA"
    ]
    graphics [
      x 1583.087233091047
      y 662.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_55"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id62eba7d3"
      uniprot "NA"
    ]
    graphics [
      x 1809.12383825383
      y 284.2068053307805
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_66"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idbeaaca30"
      uniprot "NA"
    ]
    graphics [
      x 1547.7523955131608
      y 1290.9044512172738
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000132155"
      hgnc "NA"
      map_id "W9_41"
      name "RAF1"
      node_subtype "GENE"
      node_type "species"
      org_id "fd989"
      uniprot "NA"
    ]
    graphics [
      x 1757.3962050959822
      y 1593.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_14"
      name "SARS,_space_MERS,_space_229E_br_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "bf897"
      uniprot "NA"
    ]
    graphics [
      x 2150.2823930819295
      y 442.3733096122189
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 79
    source 1
    target 2
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_32"
      target_id "W9_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 2
    target 3
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_62"
      target_id "W9_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 81
    source 4
    target 3
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_59"
      target_id "W9_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 82
    source 5
    target 3
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_67"
      target_id "W9_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 83
    source 3
    target 6
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_38"
      target_id "W9_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 84
    source 3
    target 7
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_38"
      target_id "W9_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 85
    source 78
    target 4
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_14"
      target_id "W9_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 86
    source 74
    target 5
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_29"
      target_id "W9_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 87
    source 6
    target 50
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_64"
      target_id "W9_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 88
    source 8
    target 7
    cd19dm [
      diagram "WP4877"
      edge_type "INHIBITION"
      source_id "W9_40"
      target_id "W9_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 89
    source 7
    target 9
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_77"
      target_id "W9_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 90
    source 8
    target 10
    cd19dm [
      diagram "WP4877"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W9_40"
      target_id "W9_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 91
    source 10
    target 9
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_53"
      target_id "W9_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 92
    source 9
    target 11
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_24"
      target_id "W9_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 93
    source 9
    target 12
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_24"
      target_id "W9_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 94
    source 17
    target 10
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_37"
      target_id "W9_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 95
    source 11
    target 52
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_68"
      target_id "W9_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 96
    source 12
    target 13
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_56"
      target_id "W9_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 97
    source 14
    target 13
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_47"
      target_id "W9_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 98
    source 15
    target 14
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_20"
      target_id "W9_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 99
    source 16
    target 15
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_49"
      target_id "W9_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 100
    source 17
    target 16
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_37"
      target_id "W9_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 18
    target 17
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_58"
      target_id "W9_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 19
    target 17
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_70"
      target_id "W9_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 20
    target 17
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_42"
      target_id "W9_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 17
    target 21
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_37"
      target_id "W9_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 17
    target 22
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_37"
      target_id "W9_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 106
    source 64
    target 18
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_13"
      target_id "W9_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 107
    source 72
    target 19
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_26"
      target_id "W9_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 108
    source 68
    target 20
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_30"
      target_id "W9_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 109
    source 21
    target 37
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_57"
      target_id "W9_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 110
    source 22
    target 23
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_54"
      target_id "W9_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 111
    source 23
    target 24
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_1"
      target_id "W9_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 112
    source 24
    target 25
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_73"
      target_id "W9_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 113
    source 26
    target 25
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_61"
      target_id "W9_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 114
    source 27
    target 26
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_27"
      target_id "W9_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 28
    target 27
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_74"
      target_id "W9_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 27
    target 29
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_27"
      target_id "W9_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 31
    target 28
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_17"
      target_id "W9_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 29
    target 30
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_50"
      target_id "W9_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 32
    target 31
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_46"
      target_id "W9_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 33
    target 31
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_65"
      target_id "W9_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 34
    target 31
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_72"
      target_id "W9_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 35
    target 31
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_51"
      target_id "W9_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 31
    target 36
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_17"
      target_id "W9_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 61
    target 32
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_4"
      target_id "W9_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 73
    target 33
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_7"
      target_id "W9_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 126
    source 64
    target 34
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_13"
      target_id "W9_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 127
    source 59
    target 35
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_25"
      target_id "W9_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 128
    source 36
    target 37
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_44"
      target_id "W9_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 37
    target 38
    cd19dm [
      diagram "WP4877"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W9_22"
      target_id "W9_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 130
    source 37
    target 39
    cd19dm [
      diagram "WP4877"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W9_22"
      target_id "W9_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 131
    source 37
    target 40
    cd19dm [
      diagram "WP4877"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W9_22"
      target_id "W9_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 132
    source 37
    target 41
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_22"
      target_id "W9_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 133
    source 37
    target 42
    cd19dm [
      diagram "WP4877"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W9_22"
      target_id "W9_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 134
    source 37
    target 43
    cd19dm [
      diagram "WP4877"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W9_22"
      target_id "W9_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 135
    source 57
    target 38
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_21"
      target_id "W9_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 136
    source 38
    target 58
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_75"
      target_id "W9_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 137
    source 55
    target 39
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_36"
      target_id "W9_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 138
    source 39
    target 56
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_6"
      target_id "W9_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 139
    source 53
    target 40
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_5"
      target_id "W9_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 40
    target 54
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_31"
      target_id "W9_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 41
    target 48
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_52"
      target_id "W9_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 46
    target 42
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_2"
      target_id "W9_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 42
    target 47
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_34"
      target_id "W9_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 144
    source 44
    target 43
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_39"
      target_id "W9_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 145
    source 43
    target 45
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_69"
      target_id "W9_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 146
    source 49
    target 48
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_48"
      target_id "W9_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 147
    source 50
    target 49
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_10"
      target_id "W9_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 148
    source 50
    target 51
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_10"
      target_id "W9_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 149
    source 51
    target 52
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_60"
      target_id "W9_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 150
    source 59
    target 60
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_25"
      target_id "W9_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 151
    source 60
    target 61
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_76"
      target_id "W9_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 152
    source 62
    target 61
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_45"
      target_id "W9_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 153
    source 63
    target 62
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_11"
      target_id "W9_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 154
    source 65
    target 64
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_63"
      target_id "W9_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 155
    source 66
    target 65
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_16"
      target_id "W9_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 156
    source 66
    target 67
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_16"
      target_id "W9_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 157
    source 67
    target 68
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_71"
      target_id "W9_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 158
    source 69
    target 68
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_43"
      target_id "W9_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 159
    source 70
    target 69
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_28"
      target_id "W9_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 160
    source 70
    target 71
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_28"
      target_id "W9_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 161
    source 71
    target 72
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_78"
      target_id "W9_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 162
    source 75
    target 74
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_55"
      target_id "W9_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 163
    source 76
    target 74
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_66"
      target_id "W9_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 164
    source 78
    target 75
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_14"
      target_id "W9_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 165
    source 77
    target 76
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_41"
      target_id "W9_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
