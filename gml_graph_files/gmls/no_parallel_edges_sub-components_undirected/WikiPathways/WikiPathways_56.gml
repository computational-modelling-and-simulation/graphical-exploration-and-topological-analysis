# generated with VANTED V2.8.2 at Fri Mar 04 10:04:36 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A133068"
      hgnc "NA"
      map_id "W13_11"
      name "bradykinin,_space_des_minus_arg(9)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "aea90"
      uniprot "NA"
    ]
    graphics [
      x 1655.6721914294972
      y 1893.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_86"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id63c55d3"
      uniprot "NA"
    ]
    graphics [
      x 2245.539845997858
      y 1921.275226933289
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_17"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "b1f87"
      uniprot "NA"
    ]
    graphics [
      x 1390.6404947713584
      y 1802.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_90"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id73c52fb1"
      uniprot "NA"
    ]
    graphics [
      x 1401.5654374837277
      y 1953.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_21"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "b63af"
      uniprot "NA"
    ]
    graphics [
      x 1841.7500514224312
      y 1685.18067980018
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_83"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id501f9be8"
      uniprot "NA"
    ]
    graphics [
      x 1359.520266381813
      y 1472.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16467"
      hgnc "NA"
      map_id "W13_46"
      name "L_minus_arginine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "d996c"
      uniprot "NA"
    ]
    graphics [
      x 1636.786292841501
      y 1173.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_89"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id6c85e882"
      uniprot "NA"
    ]
    graphics [
      x 1940.0161629260556
      y 873.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:hmdb:HMDB0000464"
      hgnc "NA"
      map_id "W13_34"
      name "Ca_plus__plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "cc565"
      uniprot "NA"
    ]
    graphics [
      x 2133.3814443112497
      y 675.1047354767761
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ncbigene:623"
      hgnc "NA"
      map_id "W13_12"
      name "BDKRB1"
      node_subtype "GENE"
      node_type "species"
      org_id "b029e"
      uniprot "NA"
    ]
    graphics [
      x 1691.5316656793975
      y 1263.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_1"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "a2920"
      uniprot "NA"
    ]
    graphics [
      x 1830.0568352091213
      y 723.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_14"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "b1b8e"
      uniprot "NA"
    ]
    graphics [
      x 1563.7019229736586
      y 1503.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:cas:35121-78-9;urn:miriam:cas:363-24-6;urn:miriam:hgnc.symbol:IL1B;urn:miriam:hgnc.symbol:IL1A"
      hgnc "HGNC_SYMBOL:IL1B;HGNC_SYMBOL:IL1A"
      map_id "W13_78"
      name "Inflammatory_space_mediators"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ff4e0"
      uniprot "NA"
    ]
    graphics [
      x 2034.431898878041
      y 1545.9032955921962
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_61"
      name "NA"
      node_subtype "UNKNOWN_POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "ef0da"
      uniprot "NA"
    ]
    graphics [
      x 2459.949833073444
      y 1552.6396061234
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:hgnc.symbol:IL1A;urn:miriam:cas:363-24-6;urn:miriam:cas:35121-78-9;urn:miriam:hgnc.symbol:IL1B"
      hgnc "HGNC_SYMBOL:IL1A;HGNC_SYMBOL:IL1B"
      map_id "W13_69"
      name "f66e8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f66e8"
      uniprot "NA"
    ]
    graphics [
      x 2369.9688065948876
      y 1617.571587666379
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ensembl:ENSG00000232810;urn:miriam:hgnc.symbol:IL1B;urn:miriam:hgnc.symbol:IL1A;urn:miriam:hgnc.symbol:NFKB1"
      hgnc "HGNC_SYMBOL:IL1B;HGNC_SYMBOL:IL1A;HGNC_SYMBOL:NFKB1"
      map_id "W13_70"
      name "f6746"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f6746"
      uniprot "NA"
    ]
    graphics [
      x 1821.2646743408366
      y 413.5706859687666
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ncbigene:1636;urn:miriam:pubmed:15174896"
      hgnc "NA"
      map_id "W13_60"
      name "ACE"
      node_subtype "GENE"
      node_type "species"
      org_id "ed520"
      uniprot "NA"
    ]
    graphics [
      x 1815.6664804632187
      y 1563.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_99"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idc8e789dd"
      uniprot "NA"
    ]
    graphics [
      x 1977.9691468780572
      y 1023.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_53"
      name "Degradation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "e5292"
      uniprot "NA"
    ]
    graphics [
      x 1823.0705789160597
      y 633.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_82"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id4ceb9356"
      uniprot "NA"
    ]
    graphics [
      x 1558.6415632555434
      y 903.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ncbigene:1636;urn:miriam:pubmed:15174896"
      hgnc "NA"
      map_id "W13_50"
      name "ACE"
      node_subtype "GENE"
      node_type "species"
      org_id "e130d"
      uniprot "NA"
    ]
    graphics [
      x 1680.6353836927358
      y 1504.0648872957925
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_85"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id54c92813"
      uniprot "NA"
    ]
    graphics [
      x 1921.7721620260415
      y 1887.1821497933659
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ensembl:ENSG00000109320"
      hgnc "NA"
      map_id "W13_10"
      name "NFKB1"
      node_subtype "GENE"
      node_type "species"
      org_id "ae038"
      uniprot "NA"
    ]
    graphics [
      x 2025.3186168442508
      y 2080.1934030728726
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_96"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idb55155be"
      uniprot "NA"
    ]
    graphics [
      x 1731.5178440457205
      y 2193.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ensembl:ENSG00000100030"
      hgnc "NA"
      map_id "W13_26"
      name "MAPK1"
      node_subtype "GENE"
      node_type "species"
      org_id "c0de8"
      uniprot "NA"
    ]
    graphics [
      x 1395.931891581175
      y 2102.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:pubmed:18449520;urn:miriam:ncbigene:59272"
      hgnc "NA"
      map_id "W13_47"
      name "ACE2"
      node_subtype "GENE"
      node_type "species"
      org_id "dc981"
      uniprot "NA"
    ]
    graphics [
      x 1236.6907605991378
      y 1442.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:hmdb:HMDB0004246"
      hgnc "NA"
      map_id "W13_24"
      name "Bradykinin"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "bc2f2"
      uniprot "NA"
    ]
    graphics [
      x 2134.6956478641787
      y 1729.5827836404899
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ensembl:ENSG00000120054"
      hgnc "NA"
      map_id "W13_75"
      name "CPN1"
      node_subtype "GENE"
      node_type "species"
      org_id "fe018"
      uniprot "NA"
    ]
    graphics [
      x 2438.2291309248944
      y 1773.9056223277285
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_84"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id54a8211b"
      uniprot "NA"
    ]
    graphics [
      x 1901.273178852507
      y 1156.8870861110843
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "PUBMED:33375371"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_103"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idfbc3672"
      uniprot "NA"
    ]
    graphics [
      x 2371.4870098797155
      y 1807.7996912903727
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_22"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "b7518"
      uniprot "NA"
    ]
    graphics [
      x 1483.073120167682
      y 2013.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ncbigene:624"
      hgnc "NA"
      map_id "W13_68"
      name "BDKRB2"
      node_subtype "GENE"
      node_type "species"
      org_id "f5820"
      uniprot "NA"
    ]
    graphics [
      x 1031.495467200908
      y 2312.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_93"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ida315d709"
      uniprot "NA"
    ]
    graphics [
      x 1181.7777242267362
      y 2132.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_40"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "d28a2"
      uniprot "NA"
    ]
    graphics [
      x 1209.7105404722247
      y 2042.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_66"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f390a"
      uniprot "NA"
    ]
    graphics [
      x 662.2176753510984
      y 2063.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ensembl:ENSG00000089250"
      hgnc "NA"
      map_id "W13_37"
      name "NOS1"
      node_subtype "GENE"
      node_type "species"
      org_id "cf49a"
      uniprot "NA"
    ]
    graphics [
      x 334.3577764115256
      y 1795.370067306213
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_3"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "a7811"
      uniprot "NA"
    ]
    graphics [
      x 502.32527990874553
      y 1935.9103278678272
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16467"
      hgnc "NA"
      map_id "W13_42"
      name "L_minus_arginine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "d555e"
      uniprot "NA"
    ]
    graphics [
      x 526.213386229805
      y 2034.5896272200578
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ensembl:ENSG00000164867;urn:miriam:pubmed:18040024"
      hgnc "NA"
      map_id "W13_74"
      name "NOS3"
      node_subtype "GENE"
      node_type "species"
      org_id "fc11d"
      uniprot "NA"
    ]
    graphics [
      x 932.85995778909
      y 1742.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16480"
      hgnc "NA"
      map_id "W13_33"
      name "nitric_space_oxide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "c57f1"
      uniprot "NA"
    ]
    graphics [
      x 882.5439433898819
      y 1802.024986525728
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_27"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "c1e80"
      uniprot "NA"
    ]
    graphics [
      x 1327.4767714453606
      y 1742.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16480"
      hgnc "NA"
      map_id "W13_57"
      name "nitric_space_oxide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "e5fdd"
      uniprot "NA"
    ]
    graphics [
      x 1555.0272761166239
      y 1410.9044512172738
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_25"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "bd07a"
      uniprot "NA"
    ]
    graphics [
      x 1623.2499283451007
      y 1113.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:uniprot:A0A140VJE6"
      hgnc "NA"
      map_id "W13_56"
      name "Guanylate_space_cyclase"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e5e16"
      uniprot "UNIPROT:A0A140VJE6"
    ]
    graphics [
      x 1397.0836485673105
      y 1140.9044512172738
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "PUBMED:10320667"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_43"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "d69af"
      uniprot "NA"
    ]
    graphics [
      x 1246.9838658449507
      y 1292.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15996"
      hgnc "NA"
      map_id "W13_52"
      name "Guanosine_space_triphosphate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "e3d3f"
      uniprot "NA"
    ]
    graphics [
      x 1153.1007155017514
      y 1426.977845591481
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16356"
      hgnc "NA"
      map_id "W13_59"
      name "Cyclic_space_guanosine_space_monophosphate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "ec563"
      uniprot "NA"
    ]
    graphics [
      x 1196.349227965187
      y 962.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_19"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "b3d21"
      uniprot "NA"
    ]
    graphics [
      x 1237.8052917352989
      y 692.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ensembl:ENSG00000185532"
      hgnc "NA"
      map_id "W13_35"
      name "PRKG1"
      node_subtype "GENE"
      node_type "species"
      org_id "cdaee"
      uniprot "NA"
    ]
    graphics [
      x 1436.5597928232637
      y 782.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_32"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "c4399"
      uniprot "NA"
    ]
    graphics [
      x 1911.330823835899
      y 903.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_44"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "d96ba"
      uniprot "NA"
    ]
    graphics [
      x 1290.5003659654747
      y 1112.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ensembl:ENSG00000067560"
      hgnc "NA"
      map_id "W13_49"
      name "RHOA"
      node_subtype "GENE"
      node_type "species"
      org_id "e0505"
      uniprot "NA"
    ]
    graphics [
      x 1445.8634292260788
      y 1502.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_55"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "e5c48"
      uniprot "NA"
    ]
    graphics [
      x 1724.0263110864385
      y 1623.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ensembl:ENSG00000067900"
      hgnc "NA"
      map_id "W13_4"
      name "ROCK1"
      node_subtype "GENE"
      node_type "species"
      org_id "a8b46"
      uniprot "NA"
    ]
    graphics [
      x 1836.5920125798805
      y 1402.5654308722508
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_13"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "b06b4"
      uniprot "NA"
    ]
    graphics [
      x 2117.461882457925
      y 1520.7213110276348
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:brenda:3.1.3.53"
      hgnc "NA"
      map_id "W13_76"
      name "MLCP"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "feb86"
      uniprot "NA"
    ]
    graphics [
      x 2224.3838068935006
      y 1195.520352918807
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      annotation "PUBMED:1336455"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_45"
      name "NA"
      node_subtype "UNKNOWN_POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "d9883"
      uniprot "NA"
    ]
    graphics [
      x 1970.2570793815562
      y 1545.104735476776
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_30"
      name "Relaxation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "c3988"
      uniprot "NA"
    ]
    graphics [
      x 2060.006988988437
      y 1703.5989309397503
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:pubmed:18035185;urn:miriam:ncbigene:186"
      hgnc "NA"
      map_id "W13_65"
      name "AGTR2"
      node_subtype "GENE"
      node_type "species"
      org_id "f2fd8"
      uniprot "NA"
    ]
    graphics [
      x 1260.289476515454
      y 1772.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_80"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id11cf8705"
      uniprot "NA"
    ]
    graphics [
      x 1272.7224543088562
      y 1502.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:kegg.compound:C15851"
      hgnc "NA"
      map_id "W13_15"
      name "Ang_space_1_minus_9"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "b1bb7"
      uniprot "NA"
    ]
    graphics [
      x 1344.6201066756053
      y 1712.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "b31de"
      uniprot "NA"
    ]
    graphics [
      x 981.498948047583
      y 2372.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_16"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "b1ef8"
      uniprot "NA"
    ]
    graphics [
      x 1947.9146055653673
      y 2072.8858530100542
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ncbigene:1636;urn:miriam:pubmed:15174896"
      hgnc "NA"
      map_id "W13_8"
      name "ACE"
      node_subtype "GENE"
      node_type "species"
      org_id "ab666"
      uniprot "NA"
    ]
    graphics [
      x 2183.3594690838463
      y 1841.9543923012677
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:chemspider:110354"
      hgnc "NA"
      map_id "W13_41"
      name "Ang_space_1_minus_7"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "d4174"
      uniprot "NA"
    ]
    graphics [
      x 2117.496080622746
      y 2237.5492089999966
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "PUBMED:33375371"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_88"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id672631f5"
      uniprot "NA"
    ]
    graphics [
      x 1915.8573164570398
      y 1672.0899601841807
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      annotation "PUBMED:33375371"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_73"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "fae04"
      uniprot "NA"
    ]
    graphics [
      x 1858.5848577410059
      y 2014.244079670056
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:pubchem.compound:3081372"
      hgnc "NA"
      map_id "W13_31"
      name "Angiotensin_space_I"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "c3eaf"
      uniprot "NA"
    ]
    graphics [
      x 1334.2980275828486
      y 2462.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:hmdb:HMDB0001035"
      hgnc "NA"
      map_id "W13_62"
      name "Angiotensin_space_II"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "f080e"
      uniprot "NA"
    ]
    graphics [
      x 1812.666873452446
      y 1833.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_94"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ida74e8c"
      uniprot "NA"
    ]
    graphics [
      x 1215.6092850786197
      y 1472.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_87"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id65de959d"
      uniprot "NA"
    ]
    graphics [
      x 2170.281900943668
      y 1961.9543923012677
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:pubmed:33375371;urn:miriam:ncbigene:185"
      hgnc "NA"
      map_id "W13_72"
      name "AGTR1"
      node_subtype "GENE"
      node_type "species"
      org_id "f99b1"
      uniprot "NA"
    ]
    graphics [
      x 2090.7377677873756
      y 1759.5827836404899
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_92"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id99222b0a"
      uniprot "NA"
    ]
    graphics [
      x 1771.1962188805855
      y 1443.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:cas:52-39-1"
      hgnc "NA"
      map_id "W13_51"
      name "Aldosterone"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "e3c20"
      uniprot "NA"
    ]
    graphics [
      x 1741.981726065707
      y 1533.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ncbigene:5054;urn:miriam:pubmed:33375371"
      hgnc "NA"
      map_id "W13_7"
      name "SERPINE1"
      node_subtype "GENE"
      node_type "species"
      org_id "aac02"
      uniprot "NA"
    ]
    graphics [
      x 830.4741625606305
      y 893.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      annotation "PUBMED:33375371"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_101"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ide64f6ad9"
      uniprot "NA"
    ]
    graphics [
      x 570.2820176708295
      y 590.1864283594114
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:wikipathways:WP558"
      hgnc "NA"
      map_id "W13_39"
      name "Complement_space_and_br_Coagulation_space_Cascades"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "d20d8"
      uniprot "NA"
    ]
    graphics [
      x 659.5131756300493
      y 387.29457952301743
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      annotation "PUBMED:32562843;PUBMED:33065209"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_95"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ida91dd59d"
      uniprot "NA"
    ]
    graphics [
      x 998.130141842754
      y 212.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ensembl:ENSG00000131187"
      hgnc "NA"
      map_id "W13_63"
      name "F12"
      node_subtype "GENE"
      node_type "species"
      org_id "f0b03"
      uniprot "NA"
    ]
    graphics [
      x 1413.5294799029098
      y 392.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_104"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idfe038846"
      uniprot "NA"
    ]
    graphics [
      x 1702.9631351970463
      y 1233.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_91"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id7951d7ac"
      uniprot "NA"
    ]
    graphics [
      x 1311.871456022805
      y 152.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ensembl:ENSG00000167748"
      hgnc "NA"
      map_id "W13_48"
      name "KLK1"
      node_subtype "GENE"
      node_type "species"
      org_id "ddce0"
      uniprot "NA"
    ]
    graphics [
      x 1335.6428127883223
      y 362.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_79"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id11c88b0d"
      uniprot "NA"
    ]
    graphics [
      x 1696.3151525135197
      y 783.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_20"
      name "Kallikrein_minus_Kinin_br_System"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "b4cad"
      uniprot "NA"
    ]
    graphics [
      x 1755.2061020659933
      y 1113.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_102"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idfbb4ef92"
      uniprot "NA"
    ]
    graphics [
      x 1624.623699415591
      y 1576.8718244337863
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ncbigene:3827"
      hgnc "NA"
      map_id "W13_23"
      name "KNG1"
      node_subtype "GENE"
      node_type "species"
      org_id "b76f8"
      uniprot "NA"
    ]
    graphics [
      x 1607.977709735904
      y 1953.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_58"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ec428"
      uniprot "NA"
    ]
    graphics [
      x 1716.9449194823683
      y 1863.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ensembl:ENSG00000164344"
      hgnc "NA"
      map_id "W13_71"
      name "KLKB1"
      node_subtype "GENE"
      node_type "species"
      org_id "f7722"
      uniprot "NA"
    ]
    graphics [
      x 1865.1508993652988
      y 1515.104735476776
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ensembl:ENSG00000149131"
      hgnc "NA"
      map_id "W13_29"
      name "SERPING1"
      node_subtype "GENE"
      node_type "species"
      org_id "c32b7"
      uniprot "NA"
    ]
    graphics [
      x 1694.3290926465202
      y 1653.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "e5729"
      uniprot "NA"
    ]
    graphics [
      x 913.6418059517782
      y 2402.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ncbigene:183"
      hgnc "NA"
      map_id "W13_38"
      name "AGT"
      node_subtype "GENE"
      node_type "species"
      org_id "cfd56"
      uniprot "NA"
    ]
    graphics [
      x 1042.0046594067912
      y 2432.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:ncbigene:5972"
      hgnc "NA"
      map_id "W13_64"
      name "REN"
      node_subtype "GENE"
      node_type "species"
      org_id "f2946"
      uniprot "NA"
    ]
    graphics [
      x 748.3096718867021
      y 1853.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4969"
      full_annotation "NA"
      hgnc "NA"
      map_id "W13_81"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id1454daff"
      uniprot "NA"
    ]
    graphics [
      x 641.9808598827173
      y 1133.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A28940;urn:miriam:ensembl:ENSG00000111424"
      hgnc "NA"
      map_id "W13_28"
      name "c2c3d"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c2c3d"
      uniprot "NA"
    ]
    graphics [
      x 728.9082824655427
      y 705.8032865800787
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4969"
      full_annotation "urn:miriam:pubmed:18449520;urn:miriam:ncbigene:59272"
      hgnc "NA"
      map_id "W13_6"
      name "ACE2"
      node_subtype "GENE"
      node_type "species"
      org_id "aa820"
      uniprot "NA"
    ]
    graphics [
      x 627.025678356564
      y 2228.197215732725
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W13_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 96
    source 2
    target 1
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_86"
      target_id "W13_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 97
    source 3
    target 1
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_17"
      target_id "W13_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 98
    source 1
    target 4
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_11"
      target_id "W13_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 99
    source 1
    target 5
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_11"
      target_id "W13_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 100
    source 1
    target 6
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_11"
      target_id "W13_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 27
    target 2
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_24"
      target_id "W13_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 28
    target 2
    cd19dm [
      diagram "WP4969"
      edge_type "CATALYSIS"
      source_id "W13_75"
      target_id "W13_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 26
    target 3
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_47"
      target_id "W13_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 4
    target 17
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_90"
      target_id "W13_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 5
    target 10
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_21"
      target_id "W13_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 106
    source 6
    target 7
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_83"
      target_id "W13_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 107
    source 7
    target 8
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_46"
      target_id "W13_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 108
    source 8
    target 9
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_89"
      target_id "W13_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 109
    source 11
    target 10
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_1"
      target_id "W13_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 110
    source 10
    target 12
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_12"
      target_id "W13_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 111
    source 16
    target 11
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_70"
      target_id "W13_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 112
    source 12
    target 13
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_14"
      target_id "W13_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 113
    source 13
    target 14
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_78"
      target_id "W13_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 114
    source 14
    target 15
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_61"
      target_id "W13_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 17
    target 18
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_60"
      target_id "W13_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 18
    target 19
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_99"
      target_id "W13_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 20
    target 19
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_82"
      target_id "W13_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 21
    target 20
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_50"
      target_id "W13_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 22
    target 21
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_85"
      target_id "W13_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 23
    target 22
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_10"
      target_id "W13_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 24
    target 23
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_96"
      target_id "W13_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 25
    target 24
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_26"
      target_id "W13_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 29
    target 27
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_84"
      target_id "W13_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 30
    target 27
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_103"
      target_id "W13_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 27
    target 31
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_24"
      target_id "W13_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 126
    source 88
    target 29
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_71"
      target_id "W13_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 127
    source 64
    target 30
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_8"
      target_id "W13_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 128
    source 31
    target 32
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_22"
      target_id "W13_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 33
    target 32
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_93"
      target_id "W13_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 130
    source 32
    target 34
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_68"
      target_id "W13_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 131
    source 32
    target 35
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_68"
      target_id "W13_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 132
    source 59
    target 33
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_65"
      target_id "W13_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 133
    source 34
    target 39
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_40"
      target_id "W13_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 134
    source 35
    target 36
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_66"
      target_id "W13_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 135
    source 36
    target 37
    cd19dm [
      diagram "WP4969"
      edge_type "CATALYSIS"
      source_id "W13_37"
      target_id "W13_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 136
    source 38
    target 37
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_42"
      target_id "W13_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 137
    source 39
    target 37
    cd19dm [
      diagram "WP4969"
      edge_type "CATALYSIS"
      source_id "W13_74"
      target_id "W13_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 138
    source 37
    target 40
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_3"
      target_id "W13_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 139
    source 40
    target 41
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_33"
      target_id "W13_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 41
    target 42
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_27"
      target_id "W13_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 42
    target 43
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_57"
      target_id "W13_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 43
    target 44
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_25"
      target_id "W13_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 44
    target 45
    cd19dm [
      diagram "WP4969"
      edge_type "CATALYSIS"
      source_id "W13_56"
      target_id "W13_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 144
    source 46
    target 45
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_52"
      target_id "W13_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 145
    source 45
    target 47
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_43"
      target_id "W13_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 146
    source 47
    target 48
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_59"
      target_id "W13_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 147
    source 48
    target 49
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_19"
      target_id "W13_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 148
    source 49
    target 50
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_35"
      target_id "W13_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 149
    source 49
    target 51
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_35"
      target_id "W13_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 150
    source 50
    target 56
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_32"
      target_id "W13_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 151
    source 51
    target 52
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_44"
      target_id "W13_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 152
    source 52
    target 53
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_49"
      target_id "W13_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 153
    source 53
    target 54
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_55"
      target_id "W13_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 154
    source 54
    target 55
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_4"
      target_id "W13_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 155
    source 55
    target 56
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_13"
      target_id "W13_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 156
    source 56
    target 57
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_76"
      target_id "W13_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 157
    source 57
    target 58
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_45"
      target_id "W13_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 158
    source 60
    target 59
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_80"
      target_id "W13_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 159
    source 61
    target 60
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_15"
      target_id "W13_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 160
    source 62
    target 61
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_18"
      target_id "W13_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 161
    source 61
    target 63
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_15"
      target_id "W13_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 162
    source 68
    target 62
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_31"
      target_id "W13_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 163
    source 95
    target 62
    cd19dm [
      diagram "WP4969"
      edge_type "CATALYSIS"
      source_id "W13_6"
      target_id "W13_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 164
    source 64
    target 63
    cd19dm [
      diagram "WP4969"
      edge_type "CATALYSIS"
      source_id "W13_8"
      target_id "W13_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 165
    source 63
    target 65
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_16"
      target_id "W13_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 166
    source 66
    target 64
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_88"
      target_id "W13_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 167
    source 64
    target 67
    cd19dm [
      diagram "WP4969"
      edge_type "CATALYSIS"
      source_id "W13_8"
      target_id "W13_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 168
    source 74
    target 66
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_51"
      target_id "W13_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 169
    source 68
    target 67
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_31"
      target_id "W13_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 170
    source 67
    target 69
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_73"
      target_id "W13_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 171
    source 90
    target 68
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_54"
      target_id "W13_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 172
    source 69
    target 70
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_62"
      target_id "W13_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 173
    source 69
    target 71
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_62"
      target_id "W13_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 174
    source 70
    target 75
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_94"
      target_id "W13_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 175
    source 71
    target 72
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_87"
      target_id "W13_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 176
    source 72
    target 73
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_72"
      target_id "W13_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 177
    source 73
    target 74
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_92"
      target_id "W13_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 178
    source 75
    target 76
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_7"
      target_id "W13_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 179
    source 76
    target 77
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_101"
      target_id "W13_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 180
    source 78
    target 77
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_95"
      target_id "W13_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 181
    source 79
    target 78
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_63"
      target_id "W13_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 182
    source 80
    target 79
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_104"
      target_id "W13_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 79
    target 81
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_63"
      target_id "W13_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 89
    target 80
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_29"
      target_id "W13_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 81
    target 82
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_91"
      target_id "W13_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 82
    target 83
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_48"
      target_id "W13_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 83
    target 84
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_79"
      target_id "W13_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 84
    target 85
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_20"
      target_id "W13_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 85
    target 86
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_102"
      target_id "W13_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 86
    target 87
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_23"
      target_id "W13_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 87
    target 88
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_58"
      target_id "W13_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 91
    target 90
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_38"
      target_id "W13_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 92
    target 90
    cd19dm [
      diagram "WP4969"
      edge_type "CATALYSIS"
      source_id "W13_64"
      target_id "W13_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 93
    target 92
    cd19dm [
      diagram "WP4969"
      edge_type "PRODUCTION"
      source_id "W13_81"
      target_id "W13_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 94
    target 93
    cd19dm [
      diagram "WP4969"
      edge_type "CONSPUMPTION"
      source_id "W13_28"
      target_id "W13_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
