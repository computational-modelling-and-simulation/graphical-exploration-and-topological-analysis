# generated with VANTED V2.8.2 at Fri Mar 04 10:04:36 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:wikipathways:WP4723"
      hgnc "NA"
      map_id "W22_19"
      name "Omega_minus_3_slash_Omega_minus_6_space__br_FA_space_synthesis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "ccb5e"
      uniprot "NA"
    ]
    graphics [
      x 520.1156801841389
      y 649.4234012663749
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:20923771"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_12"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "b7a91"
      uniprot "NA"
    ]
    graphics [
      x 755.6301602439111
      y 602.8443946190057
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "PUBMED:33571544"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_46"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id66f48e91"
      uniprot "NA"
    ]
    graphics [
      x 741.8066218110805
      y 795.8032865800787
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "PUBMED:33571544"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_42"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id4f2a84fe"
      uniprot "NA"
    ]
    graphics [
      x 675.7533035160466
      y 1002.4074840370697
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:lipidmaps:LMPG01050137"
      hgnc "NA"
      map_id "W22_7"
      name "Omega_minus_6"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "b0824"
      uniprot "NA"
    ]
    graphics [
      x 1099.165941677137
      y 632.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "PUBMED:33377319;PUBMED:33571544"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_50"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id93f06a92"
      uniprot "NA"
    ]
    graphics [
      x 1088.2208718025033
      y 302.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:ensembl:ENSG00000130234"
      hgnc "NA"
      map_id "W22_31"
      name "ACE2"
      node_subtype "GENE"
      node_type "species"
      org_id "f3245"
      uniprot "NA"
    ]
    graphics [
      x 739.7007049970426
      y 517.2165927516163
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "PUBMED:33170317"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_44"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id5b9fb57"
      uniprot "NA"
    ]
    graphics [
      x 633.7490415184703
      y 1403.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "PUBMED:33664446"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_39"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id3263c402"
      uniprot "NA"
    ]
    graphics [
      x 867.8281394971877
      y 793.3200649828557
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "PUBMED:33505321;PUBMED:33571544"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_38"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id2cffd52"
      uniprot "NA"
    ]
    graphics [
      x 594.0344694654915
      y 548.3041807688387
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id5cd8db28"
      uniprot "NA"
    ]
    graphics [
      x 1002.6473084354864
      y 152.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_10"
      name "Virus_space_in_space_host_space_cell_br_"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "b668e"
      uniprot "NA"
    ]
    graphics [
      x 1211.8425726574674
      y 272.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "PUBMED:32422320"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_51"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ida0676778"
      uniprot "NA"
    ]
    graphics [
      x 1074.361338497843
      y 542.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_6"
      name "mitogen_minus_activated_space_protein_space_kinase"
      node_subtype "GENE"
      node_type "species"
      org_id "b0174"
      uniprot "NA"
    ]
    graphics [
      x 987.8025392892075
      y 812.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "PUBMED:29167338"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_41"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id488708d6"
      uniprot "NA"
    ]
    graphics [
      x 1092.887444785047
      y 1232.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:brenda:3.1.1.4"
      hgnc "NA"
      map_id "W22_13"
      name "Cytosolic_space_Phospholipase_space_A2_br_(cPLA2)"
      node_subtype "GENE"
      node_type "species"
      org_id "b7b4c"
      uniprot "NA"
    ]
    graphics [
      x 1241.6319085919965
      y 1352.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "PUBMED:33571544"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_43"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id4fda8300"
      uniprot "NA"
    ]
    graphics [
      x 1109.0557713165065
      y 1202.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A37739"
      hgnc "NA"
      map_id "W22_32"
      name "Glycerophospholipids"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "f34d0"
      uniprot "NA"
    ]
    graphics [
      x 1337.7020710920995
      y 1052.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:lipidmaps:LMFA01030120"
      hgnc "NA"
      map_id "W22_14"
      name "linoleic_space_acid"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "b9202"
      uniprot "NA"
    ]
    graphics [
      x 1262.3775622971232
      y 842.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:lipidmaps:LMFA01010001"
      hgnc "NA"
      map_id "W22_8"
      name "palmitic_space_acid"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "b2e17"
      uniprot "NA"
    ]
    graphics [
      x 1428.0031080221984
      y 1352.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_17"
      name "stearic_space_acid"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "c3274"
      uniprot "NA"
    ]
    graphics [
      x 876.9163935103513
      y 1501.7850268315137
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:lipidmaps:LMFA01030002"
      hgnc "NA"
      map_id "W22_27"
      name "oleic_space_acid_br_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "ecc95"
      uniprot "NA"
    ]
    graphics [
      x 864.1606082918638
      y 923.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "PUBMED:34281182"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_40"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id35e835c1"
      uniprot "NA"
    ]
    graphics [
      x 656.1005338623258
      y 1064.3207889184534
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_18"
      name "Cytokine_space_Storm"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "c4a45"
      uniprot "NA"
    ]
    graphics [
      x 761.0708757919408
      y 1223.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "PUBMED:33664446"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_54"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "idbc38d6ef"
      uniprot "NA"
    ]
    graphics [
      x 1169.7216924822264
      y 1502.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "PUBMED:26271607"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_52"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idb6a5e755"
      uniprot "NA"
    ]
    graphics [
      x 606.0745590413144
      y 1585.2306846104175
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "PUBMED:32469225"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_53"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idbb7f8442"
      uniprot "NA"
    ]
    graphics [
      x 1112.7427407056798
      y 1393.4786341831896
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      annotation "PUBMED:33571544"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_55"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idebf627ef"
      uniprot "NA"
    ]
    graphics [
      x 1027.3493948975986
      y 752.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "PUBMED:1608291"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id18e899db"
      uniprot "NA"
    ]
    graphics [
      x 526.5517312646763
      y 816.9065679735925
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:lipidmaps:LMFA07050288"
      hgnc "NA"
      map_id "W22_3"
      name "Arachidonoyl_minus_CoA"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "a74ef"
      uniprot "NA"
    ]
    graphics [
      x 788.2028352789883
      y 688.4335268073403
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "PUBMED:20923771"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_1"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "a616d"
      uniprot "NA"
    ]
    graphics [
      x 1014.1245811186562
      y 692.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "PUBMED:20923771"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_26"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ec306"
      uniprot "NA"
    ]
    graphics [
      x 722.555228957088
      y 426.5457423328745
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:lipidmaps:LMFA01030001"
      hgnc "NA"
      map_id "W22_9"
      name "Arachidonic_space_acid"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "b4efb"
      uniprot "NA"
    ]
    graphics [
      x 831.4895096281249
      y 484.95458103435317
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:lipidmaps:LMFA07050278"
      hgnc "NA"
      map_id "W22_30"
      name "CoA(20:3(8Z,11Z,14Z))"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "f1c02"
      uniprot "NA"
    ]
    graphics [
      x 969.4894216610971
      y 362.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:ensembl:ENSG00000149485"
      hgnc "NA"
      map_id "W22_16"
      name "FADS1"
      node_subtype "GENE"
      node_type "species"
      org_id "bd87a"
      uniprot "NA"
    ]
    graphics [
      x 966.046698936646
      y 1073.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "PUBMED:20923771"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_23"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "d3b92"
      uniprot "NA"
    ]
    graphics [
      x 833.3711949072009
      y 763.3200649828557
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "PUBMED:20923771"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_5"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "aa396"
      uniprot "NA"
    ]
    graphics [
      x 1064.1510867075049
      y 272.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:lipidmaps:LMFA01030158"
      hgnc "NA"
      map_id "W22_25"
      name "bishomo_minus_gamma_minus_linolenic_space_acid"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "e6b51"
      uniprot "NA"
    ]
    graphics [
      x 939.3985487005183
      y 182.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:lipidmaps:LMFA07050322"
      hgnc "NA"
      map_id "W22_20"
      name "CoA(18:3(6Z,9Z,12Z))"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "cee96"
      uniprot "NA"
    ]
    graphics [
      x 925.5720450067486
      y 512.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:ensembl:ENSG00000012660;urn:miriam:ensembl:ENSG00000197977"
      hgnc "NA"
      map_id "W22_29"
      name "f1b8f"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f1b8f"
      uniprot "NA"
    ]
    graphics [
      x 1101.3225124022956
      y 782.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "PUBMED:20923771"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id7c94a43"
      uniprot "NA"
    ]
    graphics [
      x 1304.6853806574893
      y 872.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "PUBMED:20923771"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_24"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "dd433"
      uniprot "NA"
    ]
    graphics [
      x 834.0116459346768
      y 197.19986096760022
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:ensembl:ENSG00000119673"
      hgnc "NA"
      map_id "W22_15"
      name "ACOT2"
      node_subtype "GENE"
      node_type "species"
      org_id "ba85d"
      uniprot "NA"
    ]
    graphics [
      x 668.9939094049366
      y 215.92384483580486
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:lipidmaps:LMFA01030141"
      hgnc "NA"
      map_id "W22_21"
      name "gamma_minus_linolenic_space_acid"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "cf458"
      uniprot "NA"
    ]
    graphics [
      x 1091.9328647510702
      y 242.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:lipidmaps:LMFA07050343"
      hgnc "NA"
      map_id "W22_11"
      name "CoA(18:2(9Z,12Z))"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "b79f3"
      uniprot "NA"
    ]
    graphics [
      x 1598.906297872645
      y 843.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:uniprot:O95864"
      hgnc "NA"
      map_id "W22_35"
      name "FADS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "fa2ba"
      uniprot "UNIPROT:O95864"
    ]
    graphics [
      x 1246.7025161974607
      y 1172.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_48"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id8ca14613"
      uniprot "NA"
    ]
    graphics [
      x 1837.681713674569
      y 963.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:brenda:1.14.19.3"
      hgnc "NA"
      map_id "W22_22"
      name "Linoleoyl_minus_CoA_br_desaturase_br_"
      node_subtype "GENE"
      node_type "species"
      org_id "d2e0b"
      uniprot "NA"
    ]
    graphics [
      x 2180.1408451232946
      y 1126.8450802508999
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:wikipathways:WP5039"
      hgnc "NA"
      map_id "W22_2"
      name "Immune_space_reponse_space_to_space_SARS_minus_COV_minus_2"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "a661b"
      uniprot "NA"
    ]
    graphics [
      x 1591.9726032537233
      y 1350.9044512172738
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      annotation "PUBMED:32469225"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_49"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id8d80a48f"
      uniprot "NA"
    ]
    graphics [
      x 1877.9429548083438
      y 1342.5654308722508
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:pubmed:32130973"
      hgnc "NA"
      map_id "W22_33"
      name "NA"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "f71d4"
      uniprot "NA"
    ]
    graphics [
      x 1897.1618825460264
      y 1575.104735476776
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4853"
      full_annotation "NA"
      hgnc "NA"
      map_id "W22_37"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id1bbbeedc"
      uniprot "NA"
    ]
    graphics [
      x 1960.0511331540486
      y 1960.6334849759444
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:wikipathways:WP4846"
      hgnc "NA"
      map_id "W22_4"
      name "SARS_minus_CoV_minus_2_space_and_space__br_COVID_minus_19_space_Pathway_br_Molecular_space_mechanism"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "a93c5"
      uniprot "NA"
    ]
    graphics [
      x 1929.8208820698126
      y 2216.0626827930764
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:lipidmaps:LMFA01030818"
      hgnc "NA"
      map_id "W22_34"
      name "Omega_minus_3"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "f8b43"
      uniprot "NA"
    ]
    graphics [
      x 558.7555894277389
      y 786.9065679735925
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4853"
      full_annotation "urn:miriam:uniprot:P0DTC2"
      hgnc "NA"
      map_id "W22_28"
      name "trimer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "eef69"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 456.94950366829516
      y 1722.127527840072
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W22_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 56
    source 2
    target 1
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_12"
      target_id "W22_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 57
    source 1
    target 3
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "W22_19"
      target_id "W22_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 58
    source 1
    target 4
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "W22_19"
      target_id "W22_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 59
    source 30
    target 2
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "W22_3"
      target_id "W22_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 60
    source 3
    target 54
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_46"
      target_id "W22_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 61
    source 4
    target 5
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_42"
      target_id "W22_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 62
    source 5
    target 6
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "W22_7"
      target_id "W22_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 63
    source 6
    target 7
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_50"
      target_id "W22_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 64
    source 8
    target 7
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_44"
      target_id "W22_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 9
    target 7
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_39"
      target_id "W22_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 10
    target 7
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_38"
      target_id "W22_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 7
    target 11
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "W22_31"
      target_id "W22_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 55
    target 8
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "W22_28"
      target_id "W22_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 69
    source 19
    target 9
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "W22_14"
      target_id "W22_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 70
    source 54
    target 10
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "W22_34"
      target_id "W22_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 71
    source 11
    target 12
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_45"
      target_id "W22_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 72
    source 12
    target 13
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "W22_10"
      target_id "W22_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 73
    source 13
    target 14
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_51"
      target_id "W22_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 74
    source 14
    target 15
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "W22_6"
      target_id "W22_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 75
    source 15
    target 16
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_41"
      target_id "W22_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 76
    source 16
    target 17
    cd19dm [
      diagram "WP4853"
      edge_type "CATALYSIS"
      source_id "W22_13"
      target_id "W22_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 77
    source 18
    target 17
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "W22_32"
      target_id "W22_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 78
    source 17
    target 19
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_43"
      target_id "W22_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 79
    source 17
    target 20
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_43"
      target_id "W22_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 17
    target 21
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_43"
      target_id "W22_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 81
    source 17
    target 22
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_43"
      target_id "W22_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 82
    source 19
    target 47
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "W22_14"
      target_id "W22_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 83
    source 20
    target 25
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "W22_8"
      target_id "W22_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 84
    source 21
    target 26
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "W22_17"
      target_id "W22_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 85
    source 22
    target 23
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "W22_27"
      target_id "W22_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 86
    source 23
    target 24
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_40"
      target_id "W22_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 87
    source 25
    target 24
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_54"
      target_id "W22_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 88
    source 26
    target 24
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_52"
      target_id "W22_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 89
    source 27
    target 24
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_53"
      target_id "W22_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 90
    source 28
    target 24
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_55"
      target_id "W22_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 91
    source 24
    target 29
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "W22_18"
      target_id "W22_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 92
    source 49
    target 27
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "W22_2"
      target_id "W22_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 93
    source 33
    target 28
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "W22_9"
      target_id "W22_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 94
    source 29
    target 30
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_36"
      target_id "W22_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 95
    source 31
    target 30
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_1"
      target_id "W22_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 96
    source 30
    target 32
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "W22_3"
      target_id "W22_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 97
    source 34
    target 31
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "W22_30"
      target_id "W22_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 98
    source 35
    target 31
    cd19dm [
      diagram "WP4853"
      edge_type "CATALYSIS"
      source_id "W22_16"
      target_id "W22_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 99
    source 32
    target 33
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_26"
      target_id "W22_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 100
    source 36
    target 34
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_23"
      target_id "W22_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 34
    target 37
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "W22_30"
      target_id "W22_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 39
    target 36
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "W22_20"
      target_id "W22_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 40
    target 36
    cd19dm [
      diagram "WP4853"
      edge_type "CATALYSIS"
      source_id "W22_29"
      target_id "W22_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 37
    target 38
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_5"
      target_id "W22_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 41
    target 39
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_47"
      target_id "W22_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 106
    source 39
    target 42
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "W22_20"
      target_id "W22_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 107
    source 45
    target 41
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "W22_11"
      target_id "W22_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 108
    source 46
    target 41
    cd19dm [
      diagram "WP4853"
      edge_type "CATALYSIS"
      source_id "W22_35"
      target_id "W22_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 109
    source 43
    target 42
    cd19dm [
      diagram "WP4853"
      edge_type "CATALYSIS"
      source_id "W22_15"
      target_id "W22_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 110
    source 42
    target 44
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_24"
      target_id "W22_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 111
    source 47
    target 45
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_48"
      target_id "W22_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 112
    source 48
    target 47
    cd19dm [
      diagram "WP4853"
      edge_type "CATALYSIS"
      source_id "W22_22"
      target_id "W22_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 113
    source 50
    target 49
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_49"
      target_id "W22_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 114
    source 51
    target 50
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "W22_33"
      target_id "W22_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 52
    target 51
    cd19dm [
      diagram "WP4853"
      edge_type "PRODUCTION"
      source_id "W22_37"
      target_id "W22_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 53
    target 52
    cd19dm [
      diagram "WP4853"
      edge_type "CONSPUMPTION"
      source_id "W22_4"
      target_id "W22_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
