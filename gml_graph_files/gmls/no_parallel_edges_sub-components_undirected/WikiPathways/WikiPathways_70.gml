# generated with VANTED V2.8.2 at Fri Mar 04 10:04:36 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "urn:miriam:uniprot:P59634"
      hgnc "NA"
      map_id "W17_12"
      name "6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ac5ae"
      uniprot "UNIPROT:P59634"
    ]
    graphics [
      x 1570.7207341496728
      y 1803.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_106"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idfc1498e4"
      uniprot "NA"
    ]
    graphics [
      x 1848.6712030285157
      y 1893.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "urn:miriam:ensembl:ENSG00000115415"
      hgnc "NA"
      map_id "W17_73"
      name "STAT1"
      node_subtype "GENE"
      node_type "species"
      org_id "f2e43"
      uniprot "NA"
    ]
    graphics [
      x 1982.228276972759
      y 2034.649759167914
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 4
    source 1
    target 2
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_12"
      target_id "W17_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 5
    source 2
    target 3
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_106"
      target_id "W17_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
