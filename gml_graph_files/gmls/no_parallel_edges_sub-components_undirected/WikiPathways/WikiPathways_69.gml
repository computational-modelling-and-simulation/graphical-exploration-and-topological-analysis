# generated with VANTED V2.8.2 at Fri Mar 04 10:04:36 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "urn:miriam:ensembl:ENSG00000107643;urn:miriam:ensembl:ENSG00000112062"
      hgnc "NA"
      map_id "W17_52"
      name "e2203"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e2203"
      uniprot "NA"
    ]
    graphics [
      x 1068.659555540362
      y 1507.0742286417976
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_6"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "a744d"
      uniprot "NA"
    ]
    graphics [
      x 1158.1975458326199
      y 1592.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_22"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "b9b9b"
      uniprot "NA"
    ]
    graphics [
      x 1155.5137673376978
      y 1232.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_32"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "c3ed8"
      uniprot "NA"
    ]
    graphics [
      x 1133.8201302913076
      y 1652.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "urn:miriam:ensembl:ENSG00000177606;urn:miriam:ensembl:ENSG00000170345"
      hgnc "NA"
      map_id "W17_64"
      name "ebc96"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ebc96"
      uniprot "NA"
    ]
    graphics [
      x 1340.4610818085819
      y 1952.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "dabfe"
      uniprot "NA"
    ]
    graphics [
      x 1552.4599745737332
      y 2073.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_70"
      name "INF_minus_I"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f1657"
      uniprot "NA"
    ]
    graphics [
      x 1502.2909936343935
      y 1863.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_87"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id35fa82f"
      uniprot "NA"
    ]
    graphics [
      x 1469.1478240056376
      y 1230.9044512172738
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_11"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "aa450"
      uniprot "NA"
    ]
    graphics [
      x 1502.1902698757954
      y 2252.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_93"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id619b1996"
      uniprot "NA"
    ]
    graphics [
      x 1303.6755292978166
      y 1322.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_9"
      name "INF_minus_I"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a9f04"
      uniprot "NA"
    ]
    graphics [
      x 1145.225265123029
      y 872.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_97"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id782ae218"
      uniprot "NA"
    ]
    graphics [
      x 1220.7689979715663
      y 632.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_37"
      name "INF_minus_I"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "cae33"
      uniprot "NA"
    ]
    graphics [
      x 1148.0517455674646
      y 362.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_89"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id3cf0d202"
      uniprot "NA"
    ]
    graphics [
      x 693.8759447313712
      y 634.0952905246459
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "urn:miriam:ensembl:ENSG00000159110;urn:miriam:ensembl:ENSG00000142166"
      hgnc "NA"
      map_id "W17_20"
      name "b8407"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b8407"
      uniprot "NA"
    ]
    graphics [
      x 522.0440512840661
      y 1108.8798096866146
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_66"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "ed722"
      uniprot "NA"
    ]
    graphics [
      x 551.3518989593179
      y 1188.281291735611
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "urn:miriam:ensembl:ENSG00000105397;urn:miriam:ensembl:ENSG00000162434"
      hgnc "NA"
      map_id "W17_48"
      name "dd322"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "dd322"
      uniprot "NA"
    ]
    graphics [
      x 704.6801105293027
      y 1163.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_88"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id38b9357c"
      uniprot "NA"
    ]
    graphics [
      x 659.6333069928562
      y 1463.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "urn:miriam:ensembl:ENSG00000170581;urn:miriam:ensembl:ENSG00000115415;urn:miriam:ensembl:ENSG00000213928"
      hgnc "NA"
      map_id "W17_42"
      name "d46a8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d46a8"
      uniprot "NA"
    ]
    graphics [
      x 1088.0184619123506
      y 1423.4786341831896
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_25"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "bc446"
      uniprot "NA"
    ]
    graphics [
      x 1603.8246866875802
      y 1143.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_21"
      name "ISRE"
      node_subtype "GENE"
      node_type "species"
      org_id "b94d2"
      uniprot "NA"
    ]
    graphics [
      x 1871.2827618740855
      y 933.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_71"
      name "ISGs"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f1bfb"
      uniprot "NA"
    ]
    graphics [
      x 1739.5284945288658
      y 1563.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_29"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "c04cc"
      uniprot "NA"
    ]
    graphics [
      x 1564.154059168823
      y 1833.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "urn:miriam:ensembl:ENSG00000055332;urn:miriam:ensembl:ENSG00000089127;urn:miriam:ensembl:ENSG00000111335;urn:miriam:ensembl:ENSG00000111331"
      hgnc "NA"
      map_id "W17_35"
      name "c82e3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c82e3"
      uniprot "NA"
    ]
    graphics [
      x 1214.980950683065
      y 1802.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_94"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id6248a0cf"
      uniprot "NA"
    ]
    graphics [
      x 613.3660590258356
      y 1343.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_14"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "af7a1"
      uniprot "NA"
    ]
    graphics [
      x 1469.7333970297339
      y 1953.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "urn:miriam:wikipathways:WP4912"
      hgnc "NA"
      map_id "W17_30"
      name "Innate_space_Immunity"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "c1ec8"
      uniprot "NA"
    ]
    graphics [
      x 1320.6947406985357
      y 1982.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_92"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id563444ae"
      uniprot "NA"
    ]
    graphics [
      x 1480.561553231424
      y 2073.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_65"
      name "nsp3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ed5ac"
      uniprot "NA"
    ]
    graphics [
      x 1362.237504458096
      y 2072.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_33"
      name "nsp15"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c625e"
      uniprot "NA"
    ]
    graphics [
      x 367.518628499776
      y 1102.0525171559132
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "urn:miriam:ensembl:ENSG00000126456"
      hgnc "NA"
      map_id "W17_57"
      name "IRF3"
      node_subtype "GENE"
      node_type "species"
      org_id "e7683"
      uniprot "NA"
    ]
    graphics [
      x 1314.551261306529
      y 2252.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_55"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "e5c9e"
      uniprot "NA"
    ]
    graphics [
      x 1686.4485395449935
      y 2419.4163610143014
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_104"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idb5791c5d"
      uniprot "NA"
    ]
    graphics [
      x 1303.0424182128804
      y 2042.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_15"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "afaf3"
      uniprot "NA"
    ]
    graphics [
      x 1619.8773352446021
      y 2043.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_51"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "dff12"
      uniprot "NA"
    ]
    graphics [
      x 1434.757166011326
      y 1540.135870656576
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_67"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "edd33"
      uniprot "NA"
    ]
    graphics [
      x 926.3277123078024
      y 2252.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_5"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "a5527"
      uniprot "NA"
    ]
    graphics [
      x 1196.6371780228567
      y 1772.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_96"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id764393e3"
      uniprot "NA"
    ]
    graphics [
      x 1116.5852344497966
      y 1862.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_95"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id72e167d2"
      uniprot "NA"
    ]
    graphics [
      x 1734.0058899661549
      y 1833.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_80"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "fa67c"
      uniprot "NA"
    ]
    graphics [
      x 905.8558420029555
      y 2042.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "urn:miriam:ensembl:ENSG00000183735;urn:miriam:ensembl:ENSG00000263528"
      hgnc "NA"
      map_id "W17_24"
      name "bc0e3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "bc0e3"
      uniprot "NA"
    ]
    graphics [
      x 913.9175960547395
      y 2102.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "urn:miriam:refseq:YP_009725299.1"
      hgnc "NA"
      map_id "W17_62"
      name "PLpro_space_(nsp3)"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e9fd0"
      uniprot "NA"
    ]
    graphics [
      x 1968.2011075605778
      y 1642.0899601841807
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_46"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "dca81"
      uniprot "NA"
    ]
    graphics [
      x 2018.973117328783
      y 1395.104735476776
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_43"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "d52dc"
      uniprot "NA"
    ]
    graphics [
      x 1692.1851034405051
      y 1413.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_23"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "ba761"
      uniprot "NA"
    ]
    graphics [
      x 1991.9557822829347
      y 1792.0899601841807
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "urn:miriam:ensembl:ENSG00000175104"
      hgnc "NA"
      map_id "W17_84"
      name "TRAF6"
      node_subtype "GENE"
      node_type "species"
      org_id "fe40f"
      uniprot "NA"
    ]
    graphics [
      x 2352.298634120917
      y 1880.5232412744913
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_34"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "c6c90"
      uniprot "NA"
    ]
    graphics [
      x 2287.31821199761
      y 1699.8492002083665
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_85"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "ff46e"
      uniprot "NA"
    ]
    graphics [
      x 2162.450737400339
      y 1901.9543923012677
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_28"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "c039d"
      uniprot "NA"
    ]
    graphics [
      x 2240.11503074978
      y 1585.1487458172744
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "urn:miriam:ensembl:ENSG00000213341;urn:miriam:ensembl:ENSG00000104365;urn:miriam:ensembl:ENSG00000269335"
      hgnc "NA"
      map_id "W17_83"
      name "fc0ce"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "fc0ce"
      uniprot "NA"
    ]
    graphics [
      x 1854.103152830984
      y 1312.5654308722508
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_101"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id8f5bbbe4"
      uniprot "NA"
    ]
    graphics [
      x 1367.866491765557
      y 1202.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_49"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "de0a5"
      uniprot "NA"
    ]
    graphics [
      x 2116.9425450071003
      y 1436.9055911032879
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "urn:miriam:ensembl:ENSG00000100906"
      hgnc "NA"
      map_id "W17_17"
      name "NFKBIA"
      node_subtype "GENE"
      node_type "species"
      org_id "b6958"
      uniprot "NA"
    ]
    graphics [
      x 1927.8192178321985
      y 1381.2336068741074
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_50"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "de9af"
      uniprot "NA"
    ]
    graphics [
      x 1692.755065274518
      y 1143.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "urn:miriam:ensembl:ENSG00000109320"
      hgnc "NA"
      map_id "W17_41"
      name "NFKB1"
      node_subtype "GENE"
      node_type "species"
      org_id "d3bcb"
      uniprot "NA"
    ]
    graphics [
      x 1401.0453533161215
      y 1064.4172900869066
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_4"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "a4da4"
      uniprot "NA"
    ]
    graphics [
      x 1565.7369675626915
      y 1113.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_10"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "a9f45"
      uniprot "NA"
    ]
    graphics [
      x 1630.2396283899784
      y 1317.544093254072
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_3"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "a4cdc"
      uniprot "NA"
    ]
    graphics [
      x 1003.562066322653
      y 1413.281195954836
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_44"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "da889"
      uniprot "NA"
    ]
    graphics [
      x 945.3565854862687
      y 975.8856856625509
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_13"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "ad7f1"
      uniprot "NA"
    ]
    graphics [
      x 857.9154761674342
      y 983.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_2"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "a258c"
      uniprot "NA"
    ]
    graphics [
      x 1717.1043025030647
      y 1083.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_86"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "ff758"
      uniprot "NA"
    ]
    graphics [
      x 1461.0999400599544
      y 632.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_81"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "fa9c7"
      uniprot "NA"
    ]
    graphics [
      x 1579.443043894656
      y 963.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "urn:miriam:uniprot:P59635"
      hgnc "NA"
      map_id "W17_27"
      name "7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c0237"
      uniprot "UNIPROT:P59635"
    ]
    graphics [
      x 1517.385403025268
      y 1053.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "urn:miriam:uniprot:P59637"
      hgnc "NA"
      map_id "W17_54"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e5c2e"
      uniprot "UNIPROT:P59637"
    ]
    graphics [
      x 1451.6217087339503
      y 842.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_60"
      name "PLPro"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e9521"
      uniprot "NA"
    ]
    graphics [
      x 1879.829151370354
      y 1222.5654308722508
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "urn:miriam:uniprot:P59632"
      hgnc "NA"
      map_id "W17_18"
      name "3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b7423"
      uniprot "UNIPROT:P59632"
    ]
    graphics [
      x 589.2727855587666
      y 953.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_36"
      name "4a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c9f11"
      uniprot "NA"
    ]
    graphics [
      x 651.0279996761517
      y 1032.4074840370697
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "urn:miriam:wikidata:Q89457519"
      hgnc "NA"
      map_id "W17_59"
      name "N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e932d"
      uniprot "NA"
    ]
    graphics [
      x 966.5245200389812
      y 1772.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_105"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "idd054dc88"
      uniprot "NA"
    ]
    graphics [
      x 1118.248341606728
      y 1952.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_58"
      name "viral_br_RNA"
      node_subtype "RNA"
      node_type "species"
      org_id "e7c25"
      uniprot "NA"
    ]
    graphics [
      x 580.7452550059292
      y 1518.281291735611
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_40"
      name "PLPro"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "cec53"
      uniprot "NA"
    ]
    graphics [
      x 1355.4006383934486
      y 1682.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "urn:miriam:uniprot:P59596"
      hgnc "NA"
      map_id "W17_63"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ea9e0"
      uniprot "UNIPROT:P59596"
    ]
    graphics [
      x 917.4810950094992
      y 2282.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "urn:miriam:ensembl:ENSG00000115267;urn:miriam:ensembl:ENSG00000107201"
      hgnc "NA"
      map_id "W17_77"
      name "f7910"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f7910"
      uniprot "NA"
    ]
    graphics [
      x 1350.3465256299332
      y 2282.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_103"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "ida2f990b7"
      uniprot "NA"
    ]
    graphics [
      x 1261.9972875252429
      y 2282.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "urn:miriam:ensembl:ENSG00000088888"
      hgnc "NA"
      map_id "W17_8"
      name "MAVS"
      node_subtype "GENE"
      node_type "species"
      org_id "a978e"
      uniprot "NA"
    ]
    graphics [
      x 1187.1178593570378
      y 2492.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_102"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id981e6cb4"
      uniprot "NA"
    ]
    graphics [
      x 1099.3290171978288
      y 2012.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_100"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id82ecd04c"
      uniprot "NA"
    ]
    graphics [
      x 1378.4842596752767
      y 2582.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "urn:miriam:ensembl:ENSG00000183735"
      hgnc "NA"
      map_id "W17_82"
      name "TBK1"
      node_subtype "GENE"
      node_type "species"
      org_id "fb718"
      uniprot "NA"
    ]
    graphics [
      x 1635.1949536649931
      y 2609.5027463910837
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_68"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "ef27c"
      uniprot "NA"
    ]
    graphics [
      x 1919.008058240403
      y 2427.599724399407
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "urn:miriam:ensembl:ENSG00000131323"
      hgnc "NA"
      map_id "W17_76"
      name "TRAF3"
      node_subtype "GENE"
      node_type "species"
      org_id "f673f"
      uniprot "NA"
    ]
    graphics [
      x 2192.6067474143842
      y 2052.294565491233
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_98"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id7c297d34"
      uniprot "NA"
    ]
    graphics [
      x 2166.11721262792
      y 2114.0130003571476
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "urn:miriam:ensembl:ENSG00000172936"
      hgnc "NA"
      map_id "W17_79"
      name "MYD88"
      node_subtype "GENE"
      node_type "species"
      org_id "f8b63"
      uniprot "NA"
    ]
    graphics [
      x 2115.1631695639253
      y 1931.9543923012677
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_16"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "b1a40"
      uniprot "NA"
    ]
    graphics [
      x 1920.0258476110716
      y 2026.7590708305167
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "urn:miriam:ensembl:ENSG00000196664"
      hgnc "NA"
      map_id "W17_31"
      name "TLR7"
      node_subtype "GENE"
      node_type "species"
      org_id "c3d61"
      uniprot "NA"
    ]
    graphics [
      x 1983.7212878625655
      y 1909.5734581461315
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_90"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id3f333171"
      uniprot "NA"
    ]
    graphics [
      x 1766.0231641382122
      y 1713.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_69"
      name "PAMP"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "efdda"
      uniprot "NA"
    ]
    graphics [
      x 1420.8411154200303
      y 1682.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "urn:miriam:uniprot:P59636"
      hgnc "NA"
      map_id "W17_61"
      name "9b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e9876"
      uniprot "UNIPROT:P59636"
    ]
    graphics [
      x 1027.6402543822858
      y 1592.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_53"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "e3799"
      uniprot "NA"
    ]
    graphics [
      x 800.4519785801691
      y 2432.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "urn:miriam:ensembl:ENSG00000213341"
      hgnc "NA"
      map_id "W17_72"
      name "CHUK"
      node_subtype "GENE"
      node_type "species"
      org_id "f1ceb"
      uniprot "NA"
    ]
    graphics [
      x 936.0192068438546
      y 2312.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_99"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id7fbf0e5c"
      uniprot "NA"
    ]
    graphics [
      x 407.1738914627466
      y 1222.6625385285988
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_39"
      name "4a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "cbc6c"
      uniprot "NA"
    ]
    graphics [
      x 716.2135069209908
      y 1343.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_75"
      name "S_space_"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f54c4"
      uniprot "NA"
    ]
    graphics [
      x 1838.0784299071552
      y 1443.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_26"
      name "4b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "be379"
      uniprot "NA"
    ]
    graphics [
      x 1665.0955701345665
      y 1203.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "urn:miriam:ensembl:ENSG00000137275"
      hgnc "NA"
      map_id "W17_74"
      name "RIPK1"
      node_subtype "GENE"
      node_type "species"
      org_id "f3ad2"
      uniprot "NA"
    ]
    graphics [
      x 1015.5381363205058
      y 1263.281195954836
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_91"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id4db933d9"
      uniprot "NA"
    ]
    graphics [
      x 580.0821583089429
      y 1548.281291735611
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "urn:miriam:ncbigene:148022"
      hgnc "NA"
      map_id "W17_19"
      name "TICAM1"
      node_subtype "GENE"
      node_type "species"
      org_id "b7b77"
      uniprot "NA"
    ]
    graphics [
      x 370.4228314269867
      y 1691.2405444890771
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "urn:miriam:ensembl:ENSG00000135341"
      hgnc "NA"
      map_id "W17_7"
      name "MAP3K7"
      node_subtype "GENE"
      node_type "species"
      org_id "a85dc"
      uniprot "NA"
    ]
    graphics [
      x 1644.8401446783103
      y 1833.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "urn:miriam:ensembl:ENSG00000263528"
      hgnc "NA"
      map_id "W17_38"
      name "IKBKE"
      node_subtype "GENE"
      node_type "species"
      org_id "cb0df"
      uniprot "NA"
    ]
    graphics [
      x 1443.8108626852293
      y 1262.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "urn:miriam:uniprot:P59633"
      hgnc "NA"
      map_id "W17_56"
      name "3b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e6060"
      uniprot "UNIPROT:P59633"
    ]
    graphics [
      x 691.4553804986506
      y 2183.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_1"
      name "8ab"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a0f5b"
      uniprot "NA"
    ]
    graphics [
      x 1800.6661311506061
      y 2017.7756707200435
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_47"
      name "S_space_"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "dcf15"
      uniprot "NA"
    ]
    graphics [
      x 1316.139424224201
      y 1592.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4880"
      full_annotation "NA"
      hgnc "NA"
      map_id "W17_78"
      name "8b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f8423"
      uniprot "NA"
    ]
    graphics [
      x 1928.5277204099275
      y 2291.6245006614954
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W17_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 104
    source 2
    target 1
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_6"
      target_id "W17_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 3
    target 1
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_22"
      target_id "W17_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 106
    source 1
    target 4
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_52"
      target_id "W17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 107
    source 98
    target 2
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_7"
      target_id "W17_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 108
    source 65
    target 3
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_54"
      target_id "W17_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 109
    source 4
    target 5
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_32"
      target_id "W17_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 110
    source 5
    target 6
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_64"
      target_id "W17_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 111
    source 6
    target 7
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_45"
      target_id "W17_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 112
    source 8
    target 7
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_87"
      target_id "W17_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 113
    source 9
    target 7
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_11"
      target_id "W17_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 114
    source 7
    target 10
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_70"
      target_id "W17_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 55
    target 8
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_41"
      target_id "W17_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 31
    target 9
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_57"
      target_id "W17_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 10
    target 11
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_93"
      target_id "W17_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 11
    target 12
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_9"
      target_id "W17_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 12
    target 13
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_97"
      target_id "W17_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 13
    target 14
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_37"
      target_id "W17_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 14
    target 15
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_89"
      target_id "W17_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 15
    target 16
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_20"
      target_id "W17_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 16
    target 17
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_66"
      target_id "W17_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 17
    target 18
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_48"
      target_id "W17_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 18
    target 19
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_88"
      target_id "W17_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 126
    source 19
    target 20
    cd19dm [
      diagram "WP4880"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W17_42"
      target_id "W17_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 127
    source 21
    target 20
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_21"
      target_id "W17_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 128
    source 20
    target 22
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_25"
      target_id "W17_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 22
    target 23
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_71"
      target_id "W17_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 130
    source 23
    target 24
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_29"
      target_id "W17_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 131
    source 25
    target 24
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_94"
      target_id "W17_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 132
    source 24
    target 26
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_35"
      target_id "W17_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 133
    source 30
    target 25
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_33"
      target_id "W17_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 134
    source 26
    target 27
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_14"
      target_id "W17_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 135
    source 28
    target 27
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_92"
      target_id "W17_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 136
    source 29
    target 28
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_65"
      target_id "W17_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 137
    source 32
    target 31
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_55"
      target_id "W17_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 138
    source 33
    target 31
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_104"
      target_id "W17_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 139
    source 34
    target 31
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_15"
      target_id "W17_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 35
    target 31
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_51"
      target_id "W17_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 36
    target 31
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_67"
      target_id "W17_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 37
    target 31
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_5"
      target_id "W17_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 38
    target 31
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_96"
      target_id "W17_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 144
    source 39
    target 31
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_95"
      target_id "W17_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 145
    source 40
    target 31
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_80"
      target_id "W17_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 146
    source 103
    target 32
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_78"
      target_id "W17_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 147
    source 102
    target 33
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_47"
      target_id "W17_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 148
    source 101
    target 34
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_1"
      target_id "W17_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 149
    source 94
    target 35
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_26"
      target_id "W17_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 150
    source 100
    target 36
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_56"
      target_id "W17_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 151
    source 92
    target 37
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_39"
      target_id "W17_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 152
    source 69
    target 38
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_59"
      target_id "W17_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 153
    source 42
    target 39
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_62"
      target_id "W17_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 154
    source 41
    target 40
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_24"
      target_id "W17_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 155
    source 42
    target 43
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_62"
      target_id "W17_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 156
    source 42
    target 44
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_62"
      target_id "W17_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 157
    source 42
    target 45
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_62"
      target_id "W17_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 158
    source 43
    target 81
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_46"
      target_id "W17_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 159
    source 44
    target 99
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_43"
      target_id "W17_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 160
    source 45
    target 46
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_23"
      target_id "W17_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 161
    source 47
    target 46
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_34"
      target_id "W17_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 162
    source 46
    target 48
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_84"
      target_id "W17_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 163
    source 46
    target 49
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_84"
      target_id "W17_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 164
    source 83
    target 47
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_79"
      target_id "W17_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 165
    source 48
    target 98
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_85"
      target_id "W17_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 166
    source 49
    target 50
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_28"
      target_id "W17_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 167
    source 51
    target 50
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_101"
      target_id "W17_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 168
    source 50
    target 52
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_83"
      target_id "W17_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 169
    source 95
    target 51
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_74"
      target_id "W17_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 170
    source 52
    target 53
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_49"
      target_id "W17_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 171
    source 53
    target 54
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_17"
      target_id "W17_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 172
    source 54
    target 55
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_50"
      target_id "W17_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 173
    source 56
    target 55
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_4"
      target_id "W17_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 174
    source 57
    target 55
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_10"
      target_id "W17_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 175
    source 58
    target 55
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_3"
      target_id "W17_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 176
    source 59
    target 55
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_44"
      target_id "W17_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 177
    source 60
    target 55
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_13"
      target_id "W17_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 178
    source 61
    target 55
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_2"
      target_id "W17_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 179
    source 62
    target 55
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_86"
      target_id "W17_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 180
    source 63
    target 55
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_81"
      target_id "W17_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 181
    source 94
    target 56
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_26"
      target_id "W17_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 182
    source 93
    target 57
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_75"
      target_id "W17_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 69
    target 58
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_59"
      target_id "W17_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 68
    target 59
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_36"
      target_id "W17_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 67
    target 60
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_18"
      target_id "W17_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 66
    target 61
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_60"
      target_id "W17_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 65
    target 62
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_54"
      target_id "W17_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 64
    target 63
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_27"
      target_id "W17_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 69
    target 70
    cd19dm [
      diagram "WP4880"
      edge_type "INHIBITION"
      source_id "W17_59"
      target_id "W17_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 71
    target 70
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_58"
      target_id "W17_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 72
    target 70
    cd19dm [
      diagram "WP4880"
      edge_type "INHIBITION"
      source_id "W17_40"
      target_id "W17_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 73
    target 70
    cd19dm [
      diagram "WP4880"
      edge_type "INHIBITION"
      source_id "W17_63"
      target_id "W17_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 70
    target 74
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_105"
      target_id "W17_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 91
    target 71
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_99"
      target_id "W17_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 73
    target 89
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_63"
      target_id "W17_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 74
    target 75
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_77"
      target_id "W17_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 75
    target 76
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_103"
      target_id "W17_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 77
    target 76
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_102"
      target_id "W17_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 76
    target 78
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_8"
      target_id "W17_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 88
    target 77
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_61"
      target_id "W17_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 78
    target 79
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_100"
      target_id "W17_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 80
    target 79
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_68"
      target_id "W17_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 81
    target 80
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_76"
      target_id "W17_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 82
    target 81
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_98"
      target_id "W17_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 83
    target 82
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_79"
      target_id "W17_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 84
    target 83
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_16"
      target_id "W17_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 85
    target 84
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_31"
      target_id "W17_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 86
    target 85
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_90"
      target_id "W17_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 87
    target 86
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_69"
      target_id "W17_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 89
    target 90
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_53"
      target_id "W17_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 92
    target 91
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_39"
      target_id "W17_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 96
    target 95
    cd19dm [
      diagram "WP4880"
      edge_type "PRODUCTION"
      source_id "W17_91"
      target_id "W17_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 97
    target 96
    cd19dm [
      diagram "WP4880"
      edge_type "CONSPUMPTION"
      source_id "W17_19"
      target_id "W17_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
