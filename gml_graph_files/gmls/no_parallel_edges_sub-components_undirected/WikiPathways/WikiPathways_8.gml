# generated with VANTED V2.8.2 at Fri Mar 04 10:04:35 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16113"
      hgnc "NA"
      map_id "W1_1"
      name "cholesterol"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "a1c2c"
      uniprot "NA"
    ]
    graphics [
      x 750.4909896410807
      y 1313.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:32944968"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_166"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "idff4da966"
      uniprot "NA"
    ]
    graphics [
      x 476.15926870734165
      y 1248.281291735611
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_166"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:ensembl:ENSG00000075239"
      hgnc "NA"
      map_id "W1_95"
      name "ACAT"
      node_subtype "GENE"
      node_type "species"
      org_id "f0ad5"
      uniprot "NA"
    ]
    graphics [
      x 792.5011231543508
      y 1449.4005607102026
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16113"
      hgnc "NA"
      map_id "W1_23"
      name "cholesterol"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "b41ce"
      uniprot "NA"
    ]
    graphics [
      x 487.92832592310447
      y 1078.8798096866146
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "PUBMED:32944968"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_140"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id912daad6"
      uniprot "NA"
    ]
    graphics [
      x 727.0109323656088
      y 1283.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_140"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A42977"
      hgnc "NA"
      map_id "W1_46"
      name "25HC"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "c9b05"
      uniprot "NA"
    ]
    graphics [
      x 569.6810275581655
      y 1223.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 7
    source 1
    target 2
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_1"
      target_id "W1_166"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 8
    source 3
    target 2
    cd19dm [
      diagram "WP4846"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W1_95"
      target_id "W1_166"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 9
    source 2
    target 4
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_166"
      target_id "W1_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 10
    source 5
    target 3
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_140"
      target_id "W1_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 11
    source 6
    target 5
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_46"
      target_id "W1_140"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
