# generated with VANTED V2.8.2 at Fri Mar 04 09:53:02 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:BCL2;urn:miriam:refseq:NM_000657;urn:miriam:ncbigene:596;urn:miriam:uniprot:P10415;urn:miriam:ensembl:ENSG00000171791;urn:miriam:refseq:NM_000633;urn:miriam:hgnc:990"
      hgnc "HGNC_SYMBOL:BCL2"
      map_id "M118_188"
      name "BCL2"
      node_subtype "GENE"
      node_type "species"
      org_id "path_0_sa55"
      uniprot "UNIPROT:P10415"
    ]
    graphics [
      x 2153.5250417147004
      y 1806.5598647397292
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_188"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:30662442;PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_85"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "path_0_re38"
      uniprot "NA"
    ]
    graphics [
      x 2022.3909942649634
      y 1784.041767806913
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:DDIT3;urn:miriam:ncbigene:1649;urn:miriam:ncbigene:1649;urn:miriam:uniprot:P35638;urn:miriam:uniprot:P35638;urn:miriam:hgnc:2726;urn:miriam:refseq:NM_004083;urn:miriam:ensembl:ENSG00000175197;urn:miriam:uniprot:P0DPQ6;urn:miriam:uniprot:P0DPQ6"
      hgnc "HGNC_SYMBOL:DDIT3"
      map_id "M118_175"
      name "DDIT3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa46"
      uniprot "UNIPROT:P35638;UNIPROT:P0DPQ6"
    ]
    graphics [
      x 1734.8816522018392
      y 1568.6936571266106
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_175"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:BCL2;urn:miriam:refseq:NM_000657;urn:miriam:ncbigene:596;urn:miriam:uniprot:P10415;urn:miriam:ensembl:ENSG00000171791;urn:miriam:refseq:NM_000633;urn:miriam:hgnc:990"
      hgnc "HGNC_SYMBOL:BCL2"
      map_id "M118_187"
      name "BCL2"
      node_subtype "RNA"
      node_type "species"
      org_id "path_0_sa54"
      uniprot "UNIPROT:P10415"
    ]
    graphics [
      x 2103.400916341939
      y 1898.1709519866276
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_187"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000102580;urn:miriam:hgnc.symbol:DNAJC3;urn:miriam:hgnc:9439;urn:miriam:refseq:NM_006260;urn:miriam:uniprot:Q13217;urn:miriam:ncbigene:5611"
      hgnc "HGNC_SYMBOL:DNAJC3"
      map_id "M118_186"
      name "DNAJC3"
      node_subtype "RNA"
      node_type "species"
      org_id "path_0_sa517"
      uniprot "UNIPROT:Q13217"
    ]
    graphics [
      x 1164.7264905079517
      y 1098.732306040193
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_186"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "PUBMED:12601012;PUBMED:18360008"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_44"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "path_0_re274"
      uniprot "NA"
    ]
    graphics [
      x 1018.0204657210552
      y 1091.704086054179
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000102580;urn:miriam:hgnc.symbol:DNAJC3;urn:miriam:hgnc:9439;urn:miriam:refseq:NM_006260;urn:miriam:uniprot:Q13217;urn:miriam:uniprot:Q13217;urn:miriam:ncbigene:5611;urn:miriam:ncbigene:5611"
      hgnc "HGNC_SYMBOL:DNAJC3"
      map_id "M118_184"
      name "DNAJC3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa515"
      uniprot "UNIPROT:Q13217"
    ]
    graphics [
      x 855.5954301029781
      y 988.5739858784781
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_184"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_005080;urn:miriam:hgnc:12801;urn:miriam:ensembl:ENSG00000100219;urn:miriam:hgnc.symbol:XBP1;urn:miriam:ncbigene:7494;urn:miriam:uniprot:P17861"
      hgnc "HGNC_SYMBOL:XBP1"
      map_id "M118_139"
      name "XBP1"
      node_subtype "RNA"
      node_type "species"
      org_id "path_0_sa206"
      uniprot "UNIPROT:P17861"
    ]
    graphics [
      x 1755.0932081828264
      y 778.9992460275296
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_139"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:26587781;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_34"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "path_0_re19"
      uniprot "NA"
    ]
    graphics [
      x 1830.3554836582275
      y 647.0673028719859
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_005080;urn:miriam:hgnc:12801;urn:miriam:ensembl:ENSG00000100219;urn:miriam:hgnc.symbol:XBP1;urn:miriam:ncbigene:7494;urn:miriam:ncbigene:7494;urn:miriam:uniprot:P17861;urn:miriam:uniprot:P17861"
      hgnc "HGNC_SYMBOL:XBP1"
      map_id "M118_149"
      name "XBP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa26"
      uniprot "UNIPROT:P17861"
    ]
    graphics [
      x 1667.4836225198344
      y 556.6413319073232
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_149"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000130234;urn:miriam:ncbigene:59272;urn:miriam:ncbigene:59272;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:15692567;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:refseq:NM_001371415;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:ACE2"
      map_id "M118_291"
      name "ACE2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa145"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1256.9021180389539
      y 981.3862315180826
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_291"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "PUBMED:32075877;PUBMED:32225175"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_330"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re134"
      uniprot "NA"
    ]
    graphics [
      x 1626.9661744422847
      y 920.3648729777085
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_330"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:uniprot:W6A028;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "M118_343"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa258"
      uniprot "UNIPROT:P0DTC2;UNIPROT:W6A028;UNIPROT:P59594"
    ]
    graphics [
      x 1885.380281694708
      y 866.8259147216326
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_343"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:uniprot:W6A028;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S;urn:miriam:ensembl:ENSG00000130234;urn:miriam:ncbigene:59272;urn:miriam:ncbigene:59272;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:15692567;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:refseq:NM_001371415;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:S;HGNC_SYMBOL:ACE2"
      map_id "M118_1"
      name "ACE2:Spike"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa29"
      uniprot "UNIPROT:P0DTC2;UNIPROT:W6A028;UNIPROT:P59594;UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1795.425740851049
      y 890.7056513305546
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:22926;urn:miriam:ensembl:ENSG00000118217;urn:miriam:ncbigene:22926;urn:miriam:refseq:NM_007348;urn:miriam:uniprot:P18850;urn:miriam:uniprot:P18850;urn:miriam:hgnc.symbol:ATF6;urn:miriam:hgnc:791;urn:miriam:hgnc.symbol:HSPA5;urn:miriam:ensembl:ENSG00000044574;urn:miriam:hgnc.symbol:HSPA5;urn:miriam:refseq:NM_005347;urn:miriam:hgnc:5238;urn:miriam:ncbigene:3309;urn:miriam:ncbigene:3309;urn:miriam:uniprot:P11021;urn:miriam:uniprot:P11021;urn:miriam:ec-code:3.6.4.10"
      hgnc "HGNC_SYMBOL:ATF6;HGNC_SYMBOL:HSPA5"
      map_id "M118_4"
      name "ATF6:HSPA5"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa1"
      uniprot "UNIPROT:P18850;UNIPROT:P11021"
    ]
    graphics [
      x 2030.3847665882931
      y 1079.9959237399894
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:17090218"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_22"
      name "NA"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "path_0_re1"
      uniprot "NA"
    ]
    graphics [
      x 1938.448225100391
      y 1297.4679479890729
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_207"
      name "Unfolded_space_protein"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa618"
      uniprot "NA"
    ]
    graphics [
      x 2081.8902834282876
      y 1197.674941776561
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_207"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:22926;urn:miriam:ensembl:ENSG00000118217;urn:miriam:ncbigene:22926;urn:miriam:refseq:NM_007348;urn:miriam:uniprot:P18850;urn:miriam:uniprot:P18850;urn:miriam:hgnc.symbol:ATF6;urn:miriam:hgnc:791"
      hgnc "HGNC_SYMBOL:ATF6"
      map_id "M118_157"
      name "ATF6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa3"
      uniprot "UNIPROT:P18850"
    ]
    graphics [
      x 1674.6538101845395
      y 1714.678999654425
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_157"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:HSPA5;urn:miriam:ensembl:ENSG00000044574;urn:miriam:hgnc.symbol:HSPA5;urn:miriam:refseq:NM_005347;urn:miriam:hgnc:5238;urn:miriam:ncbigene:3309;urn:miriam:ncbigene:3309;urn:miriam:uniprot:P11021;urn:miriam:uniprot:P11021;urn:miriam:ec-code:3.6.4.10"
      hgnc "HGNC_SYMBOL:HSPA5"
      map_id "M118_163"
      name "HSPA5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa4"
      uniprot "UNIPROT:P11021"
    ]
    graphics [
      x 1947.640930763936
      y 1091.2818169426077
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_163"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:12649;urn:miriam:ensembl:ENSG00000124164;urn:miriam:hgnc.symbol:VAPB;urn:miriam:hgnc.symbol:VAPB;urn:miriam:uniprot:O95292;urn:miriam:uniprot:O95292;urn:miriam:ncbigene:9217;urn:miriam:refseq:NM_001195677;urn:miriam:ncbigene:9217"
      hgnc "HGNC_SYMBOL:VAPB"
      map_id "M118_293"
      name "VAPB"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa147"
      uniprot "UNIPROT:O95292"
    ]
    graphics [
      x 256.15442403601196
      y 974.3066361699043
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_293"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "PUBMED:28132811"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_255"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re129"
      uniprot "NA"
    ]
    graphics [
      x 164.545794785196
      y 1169.4938395586876
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_255"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:obo.go:GO%3A0051561;urn:miriam:pubmed:28132811"
      hgnc "NA"
      map_id "M118_296"
      name "Ca2_plus__space_mitochondrial_space_concentration"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa150"
      uniprot "NA"
    ]
    graphics [
      x 220.30790989826914
      y 1348.9688920947883
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_296"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:hgnc.symbol:TRAF2;urn:miriam:ncbigene:7186;urn:miriam:ncbigene:7186;urn:miriam:ensembl:ENSG00000127191;urn:miriam:refseq:NM_021138;urn:miriam:uniprot:Q12933;urn:miriam:uniprot:Q12933;urn:miriam:hgnc:12032"
      hgnc "HGNC_SYMBOL:TRAF2"
      map_id "M118_118"
      name "TRAF2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa119"
      uniprot "UNIPROT:Q12933"
    ]
    graphics [
      x 1664.3272373204045
      y 1078.5667819894677
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re28"
      uniprot "NA"
    ]
    graphics [
      x 1679.7628881399305
      y 956.4110113583338
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:2081;urn:miriam:refseq:NM_001433;urn:miriam:ncbigene:2081;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:3.1.26.-;urn:miriam:uniprot:O75460;urn:miriam:uniprot:O75460;urn:miriam:hgnc:3449;urn:miriam:ensembl:ENSG00000178607;urn:miriam:hgnc.symbol:ERN1;urn:miriam:ec-code:2.3.2.27;urn:miriam:hgnc.symbol:TRAF2;urn:miriam:ncbigene:7186;urn:miriam:ncbigene:7186;urn:miriam:ensembl:ENSG00000127191;urn:miriam:refseq:NM_021138;urn:miriam:uniprot:Q12933;urn:miriam:uniprot:Q12933;urn:miriam:hgnc:12032"
      hgnc "HGNC_SYMBOL:ERN1;HGNC_SYMBOL:TRAF2"
      map_id "M118_19"
      name "TRAF2:ERN1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa6"
      uniprot "UNIPROT:O75460;UNIPROT:Q12933"
    ]
    graphics [
      x 1470.1278399608975
      y 831.955479961666
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:hgnc.symbol:TRAF2;urn:miriam:ncbigene:7186;urn:miriam:ncbigene:7186;urn:miriam:ensembl:ENSG00000127191;urn:miriam:refseq:NM_021138;urn:miriam:uniprot:Q12933;urn:miriam:uniprot:Q12933;urn:miriam:hgnc:12032"
      hgnc "HGNC_SYMBOL:TRAF2"
      map_id "M118_117"
      name "TRAF2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa118"
      uniprot "UNIPROT:Q12933"
    ]
    graphics [
      x 1744.9907576614632
      y 870.5833378005043
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_005080;urn:miriam:hgnc:12801;urn:miriam:ensembl:ENSG00000100219;urn:miriam:hgnc.symbol:XBP1;urn:miriam:ncbigene:7494;urn:miriam:uniprot:P17861"
      hgnc "HGNC_SYMBOL:XBP1"
      map_id "M118_152"
      name "XBP1"
      node_subtype "GENE"
      node_type "species"
      org_id "path_0_sa28"
      uniprot "UNIPROT:P17861"
    ]
    graphics [
      x 1614.449558035871
      y 1003.3922497667266
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_152"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      annotation "PUBMED:26587781;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_33"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "path_0_re18"
      uniprot "NA"
    ]
    graphics [
      x 1583.4091683082925
      y 849.0896130949327
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:22926;urn:miriam:ensembl:ENSG00000118217;urn:miriam:ncbigene:22926;urn:miriam:refseq:NM_007348;urn:miriam:uniprot:P18850;urn:miriam:uniprot:P18850;urn:miriam:hgnc.symbol:ATF6;urn:miriam:hgnc:791"
      hgnc "HGNC_SYMBOL:ATF6"
      map_id "M118_218"
      name "ATF6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa65"
      uniprot "UNIPROT:P18850"
    ]
    graphics [
      x 1292.629401147935
      y 863.7806367915912
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_218"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:2081;urn:miriam:refseq:NM_001433;urn:miriam:ncbigene:2081;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:3.1.26.-;urn:miriam:uniprot:O75460;urn:miriam:uniprot:O75460;urn:miriam:hgnc:3449;urn:miriam:ensembl:ENSG00000178607;urn:miriam:hgnc.symbol:ERN1;urn:miriam:uniprot:Q9BXH1;urn:miriam:uniprot:Q9BXH1;urn:miriam:ncbigene:27113;urn:miriam:ncbigene:27113;urn:miriam:hgnc:17868;urn:miriam:ensembl:ENSG00000105327;urn:miriam:hgnc.symbol:BBC3;urn:miriam:refseq:NM_014417;urn:miriam:uniprot:Q96PG8;urn:miriam:uniprot:Q96PG8"
      hgnc "HGNC_SYMBOL:ERN1;HGNC_SYMBOL:BBC3"
      map_id "M118_17"
      name "ERN1:BBC3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa51"
      uniprot "UNIPROT:O75460;UNIPROT:Q9BXH1;UNIPROT:Q96PG8"
    ]
    graphics [
      x 1602.4045833933608
      y 793.4228922720076
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_001204106;urn:miriam:hgnc:994;urn:miriam:hgnc.symbol:BCL2L11;urn:miriam:ncbigene:10018;urn:miriam:ensembl:ENSG00000153094;urn:miriam:ncbigene:10018;urn:miriam:uniprot:O43521;urn:miriam:uniprot:O43521;urn:miriam:ncbigene:2081;urn:miriam:refseq:NM_001433;urn:miriam:ncbigene:2081;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:3.1.26.-;urn:miriam:uniprot:O75460;urn:miriam:uniprot:O75460;urn:miriam:hgnc:3449;urn:miriam:ensembl:ENSG00000178607;urn:miriam:hgnc.symbol:ERN1"
      hgnc "HGNC_SYMBOL:BCL2L11;HGNC_SYMBOL:ERN1"
      map_id "M118_16"
      name "ERN1:BCL2L11"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa50"
      uniprot "UNIPROT:O43521;UNIPROT:O75460"
    ]
    graphics [
      x 1773.8314192150583
      y 586.0037526493206
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:16695;urn:miriam:refseq:NM_005745;urn:miriam:ensembl:ENSG00000185825;urn:miriam:ncbigene:10134;urn:miriam:ncbigene:10134;urn:miriam:hgnc.symbol:BCAP31;urn:miriam:hgnc.symbol:BCAP31;urn:miriam:uniprot:P51572;urn:miriam:uniprot:P51572"
      hgnc "HGNC_SYMBOL:BCAP31"
      map_id "M118_301"
      name "BCAP31"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa156"
      uniprot "UNIPROT:P51572"
    ]
    graphics [
      x 687.2050001498186
      y 800.5407331581894
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_301"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "PUBMED:15692567"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_256"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_1_re133"
      uniprot "NA"
    ]
    graphics [
      x 501.18115303601735
      y 777.1416278967079
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_256"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:PACS2;urn:miriam:hgnc.symbol:PACS2;urn:miriam:ncbigene:23241;urn:miriam:uniprot:Q86VP3;urn:miriam:uniprot:Q86VP3;urn:miriam:ncbigene:23241;urn:miriam:pubmed:15692567;urn:miriam:hgnc:23794;urn:miriam:refseq:NM_001100913;urn:miriam:ensembl:ENSG00000179364"
      hgnc "HGNC_SYMBOL:PACS2"
      map_id "M118_311"
      name "PACS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa3"
      uniprot "UNIPROT:Q86VP3"
    ]
    graphics [
      x 476.4083911222357
      y 580.2030265576602
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_311"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:22511781;urn:miriam:obo.go:GO%3A0006915;urn:miriam:pubmed:19052620;urn:miriam:pubmed:15692567"
      hgnc "NA"
      map_id "M118_286"
      name "Apoptosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa110"
      uniprot "NA"
    ]
    graphics [
      x 735.4607595466796
      y 970.8504448777713
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_286"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:16695;urn:miriam:refseq:NM_005745;urn:miriam:ensembl:ENSG00000185825;urn:miriam:ncbigene:10134;urn:miriam:ncbigene:10134;urn:miriam:hgnc.symbol:BCAP31;urn:miriam:uniprot:P51572;urn:miriam:uniprot:P51572"
      hgnc "HGNC_SYMBOL:BCAP31"
      map_id "M118_304"
      name "p20"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa166"
      uniprot "UNIPROT:P51572"
    ]
    graphics [
      x 350.1956607805207
      y 805.2976834461139
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_304"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:836;urn:miriam:refseq:NM_004346;urn:miriam:ncbigene:836;urn:miriam:ec-code:3.4.22.56;urn:miriam:ensembl:ENSG00000164305;urn:miriam:hgnc:1504;urn:miriam:uniprot:P42574;urn:miriam:uniprot:P42574;urn:miriam:hgnc.symbol:CASP3"
      hgnc "HGNC_SYMBOL:CASP3"
      map_id "M118_196"
      name "cleaved~CASP3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa597"
      uniprot "UNIPROT:P42574"
    ]
    graphics [
      x 1208.4282223378964
      y 1365.0812168118584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_196"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_77"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re363"
      uniprot "NA"
    ]
    graphics [
      x 1381.7242273840016
      y 1202.2151263544622
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0006921"
      hgnc "NA"
      map_id "M118_172"
      name "Apoptosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa44"
      uniprot "NA"
    ]
    graphics [
      x 1561.972765618472
      y 1030.742417050272
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_172"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:32340551;urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:uniprot:W6A028;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S;urn:miriam:hgnc.symbol:HSPA5;urn:miriam:ensembl:ENSG00000044574;urn:miriam:hgnc.symbol:HSPA5;urn:miriam:refseq:NM_005347;urn:miriam:hgnc:5238;urn:miriam:ncbigene:3309;urn:miriam:ncbigene:3309;urn:miriam:uniprot:P11021;urn:miriam:uniprot:P11021;urn:miriam:ec-code:3.6.4.10"
      hgnc "HGNC_SYMBOL:S;HGNC_SYMBOL:HSPA5"
      map_id "M118_2"
      name "HSPA5_minus_Spike_space_interaction"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa30"
      uniprot "UNIPROT:P0DTC2;UNIPROT:W6A028;UNIPROT:P59594;UNIPROT:P11021"
    ]
    graphics [
      x 2062.220136792913
      y 1120.3810574579813
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "PUBMED:29887526"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_339"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re143"
      uniprot "NA"
    ]
    graphics [
      x 2051.301949887516
      y 1374.075321662124
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_339"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0046718;urn:miriam:pubmed:19411314"
      hgnc "NA"
      map_id "M118_346"
      name "viral_space_entry_space_into_space_host_space_cell"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa271"
      uniprot "NA"
    ]
    graphics [
      x 1961.286079249226
      y 1640.654603194822
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_346"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_014874;urn:miriam:pubmed:22511781;urn:miriam:hgnc:16877;urn:miriam:ec-code:3.6.5.-;urn:miriam:pubmed:29491369;urn:miriam:hgnc.symbol:MFN2;urn:miriam:ensembl:ENSG00000116688;urn:miriam:uniprot:O95140;urn:miriam:uniprot:O95140;urn:miriam:hgnc.symbol:MFN2;urn:miriam:ncbigene:9927;urn:miriam:ncbigene:9927;urn:miriam:pubmed:19052620"
      hgnc "HGNC_SYMBOL:MFN2"
      map_id "M118_328"
      name "MFN2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa98"
      uniprot "UNIPROT:O95140"
    ]
    graphics [
      x 585.5498169491003
      y 461.81296362147384
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_328"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "PUBMED:30590907;PUBMED:29491369;PUBMED:19052620"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_276"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_1_re53"
      uniprot "NA"
    ]
    graphics [
      x 342.0735584743775
      y 367.2138166923378
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_276"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:1093;urn:miriam:ensembl:ENSG00000230681;urn:miriam:refseq:NG_001099;urn:miriam:hgnc:1826;urn:miriam:hgnc.symbol:CEACAMP4"
      hgnc "HGNC_SYMBOL:CEACAMP4"
      map_id "M118_329"
      name "MFN1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa99"
      uniprot "NA"
    ]
    graphics [
      x 348.9594483224405
      y 547.194967658884
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_329"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:29491369;urn:miriam:pubmed:19052620;urn:miriam:refseq:NM_014874;urn:miriam:hgnc.symbol:MFN2;urn:miriam:ensembl:ENSG00000116688;urn:miriam:uniprot:O95140;urn:miriam:uniprot:O95140;urn:miriam:hgnc.symbol:MFN2;urn:miriam:ncbigene:9927;urn:miriam:ncbigene:9927;urn:miriam:hgnc:16877;urn:miriam:ec-code:3.6.5.-;urn:miriam:ncbigene:1093;urn:miriam:ensembl:ENSG00000230681;urn:miriam:refseq:NG_001099;urn:miriam:hgnc:1826;urn:miriam:hgnc.symbol:CEACAMP4"
      hgnc "HGNC_SYMBOL:MFN2;HGNC_SYMBOL:CEACAMP4"
      map_id "M118_234"
      name "MFN1:MFN2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_1_csa4"
      uniprot "UNIPROT:O95140"
    ]
    graphics [
      x 325.85442047966717
      y 212.70703491411348
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_234"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:13280;urn:miriam:hgnc.symbol:ERO1A;urn:miriam:refseq:NM_014584;urn:miriam:uniprot:Q96HE7;urn:miriam:ncbigene:30001;urn:miriam:ensembl:ENSG00000197930"
      hgnc "HGNC_SYMBOL:ERO1A"
      map_id "M118_120"
      name "ERO1A"
      node_subtype "GENE"
      node_type "species"
      org_id "path_0_sa122"
      uniprot "UNIPROT:Q96HE7"
    ]
    graphics [
      x 883.0573781297853
      y 782.715468622418
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:25387528"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_101"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "path_0_re76"
      uniprot "NA"
    ]
    graphics [
      x 1009.8698090617881
      y 832.6213442144244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:DDIT3;urn:miriam:ncbigene:1649;urn:miriam:ncbigene:1649;urn:miriam:uniprot:P35638;urn:miriam:uniprot:P35638;urn:miriam:hgnc:2726;urn:miriam:refseq:NM_004083;urn:miriam:ensembl:ENSG00000175197;urn:miriam:uniprot:P0DPQ6;urn:miriam:uniprot:P0DPQ6"
      hgnc "HGNC_SYMBOL:DDIT3"
      map_id "M118_219"
      name "DDIT3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa69"
      uniprot "UNIPROT:P35638;UNIPROT:P0DPQ6"
    ]
    graphics [
      x 1087.1369187413968
      y 1041.816637962666
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_219"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:13280;urn:miriam:hgnc.symbol:ERO1A;urn:miriam:refseq:NM_014584;urn:miriam:uniprot:Q96HE7;urn:miriam:ncbigene:30001;urn:miriam:ensembl:ENSG00000197930"
      hgnc "HGNC_SYMBOL:ERO1A"
      map_id "M118_121"
      name "ERO1A"
      node_subtype "RNA"
      node_type "species"
      org_id "path_0_sa123"
      uniprot "UNIPROT:Q96HE7"
    ]
    graphics [
      x 784.2677809831032
      y 743.0349253791435
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0030970"
      hgnc "NA"
      map_id "M118_130"
      name "retrograde_space_transport_space_from_space_ER_space_to_space_cytosol"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa15"
      uniprot "NA"
    ]
    graphics [
      x 1199.212734394406
      y 1901.2537547152594
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_130"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_26"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re13"
      uniprot "NA"
    ]
    graphics [
      x 966.9949330338154
      y 1818.9183822458776
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0006511"
      hgnc "NA"
      map_id "M118_133"
      name "protein_space_ubiquitination_space_and_space_destruction"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa16"
      uniprot "NA"
    ]
    graphics [
      x 806.7346241811367
      y 1678.580257511416
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_133"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ec-code:2.7.11.24;urn:miriam:ensembl:ENSG00000112062;urn:miriam:hgnc:6876;urn:miriam:uniprot:Q16539;urn:miriam:uniprot:Q16539;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:ncbigene:1432;urn:miriam:refseq:NM_001315"
      hgnc "HGNC_SYMBOL:MAPK14"
      map_id "M118_166"
      name "MAPK14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa42"
      uniprot "UNIPROT:Q16539"
    ]
    graphics [
      x 1075.8108496039629
      y 456.81589368711275
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_166"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:12215209"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re30"
      uniprot "NA"
    ]
    graphics [
      x 1226.9405735567761
      y 538.020243844785
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:Q99683;urn:miriam:uniprot:Q99683;urn:miriam:ncbigene:4217;urn:miriam:ncbigene:4217;urn:miriam:hgnc:6857;urn:miriam:refseq:NM_005923;urn:miriam:hgnc.symbol:MAP3K5;urn:miriam:ensembl:ENSG00000197442;urn:miriam:ec-code:2.7.11.25"
      hgnc "HGNC_SYMBOL:MAP3K5"
      map_id "M118_162"
      name "MAP3K5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa39"
      uniprot "UNIPROT:Q99683"
    ]
    graphics [
      x 1413.6499225019454
      y 682.5589561946297
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_162"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ec-code:2.7.11.24;urn:miriam:ensembl:ENSG00000112062;urn:miriam:hgnc:6876;urn:miriam:uniprot:Q16539;urn:miriam:uniprot:Q16539;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:ncbigene:1432;urn:miriam:refseq:NM_001315"
      hgnc "HGNC_SYMBOL:MAPK14"
      map_id "M118_167"
      name "MAPK14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa43"
      uniprot "UNIPROT:Q16539"
    ]
    graphics [
      x 1115.969935931432
      y 672.5216284007149
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_167"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:2081;urn:miriam:refseq:NM_001433;urn:miriam:ncbigene:2081;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:3.1.26.-;urn:miriam:uniprot:O75460;urn:miriam:uniprot:O75460;urn:miriam:hgnc:3449;urn:miriam:ensembl:ENSG00000178607;urn:miriam:hgnc.symbol:ERN1"
      hgnc "HGNC_SYMBOL:ERN1"
      map_id "M118_107"
      name "ERN1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa1"
      uniprot "UNIPROT:O75460"
    ]
    graphics [
      x 1117.483397546714
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      annotation "PUBMED:30773986;PUBMED:23850759;PUBMED:26587781;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_35"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_0_re2"
      uniprot "NA"
    ]
    graphics [
      x 1261.5704519704714
      y 104.5693712084335
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_137"
      name "Unfolded_space_protein"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa203"
      uniprot "NA"
    ]
    graphics [
      x 1158.9704685121535
      y 238.5672118947291
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_137"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:2081;urn:miriam:refseq:NM_001433;urn:miriam:ncbigene:2081;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:3.1.26.-;urn:miriam:uniprot:O75460;urn:miriam:uniprot:O75460;urn:miriam:hgnc:3449;urn:miriam:ensembl:ENSG00000178607;urn:miriam:hgnc.symbol:ERN1"
      hgnc "HGNC_SYMBOL:ERN1"
      map_id "M118_6"
      name "ERN1:Unfolded_space_protein"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa2"
      uniprot "UNIPROT:O75460"
    ]
    graphics [
      x 1384.578193309343
      y 236.3609670115494
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:13280;urn:miriam:hgnc.symbol:ERO1A;urn:miriam:refseq:NM_014584;urn:miriam:uniprot:Q96HE7;urn:miriam:uniprot:Q96HE7;urn:miriam:ncbigene:30001;urn:miriam:ncbigene:30001;urn:miriam:ec-code:1.8.4.-;urn:miriam:ensembl:ENSG00000197930"
      hgnc "HGNC_SYMBOL:ERO1A"
      map_id "M118_119"
      name "ERO1A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa121"
      uniprot "UNIPROT:Q96HE7"
    ]
    graphics [
      x 489.70230581697774
      y 982.7300501446398
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_27"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "path_0_re133"
      uniprot "NA"
    ]
    graphics [
      x 421.6936981003121
      y 1216.116853385626
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:13280;urn:miriam:hgnc.symbol:ERO1A;urn:miriam:refseq:NM_014584;urn:miriam:uniprot:Q96HE7;urn:miriam:uniprot:Q96HE7;urn:miriam:ncbigene:30001;urn:miriam:ncbigene:30001;urn:miriam:ec-code:1.8.4.-;urn:miriam:ensembl:ENSG00000197930"
      hgnc "HGNC_SYMBOL:ERO1A"
      map_id "M118_142"
      name "ERO1A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa212"
      uniprot "UNIPROT:Q96HE7"
    ]
    graphics [
      x 395.26564232822113
      y 1411.7481964269703
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_142"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:3255;urn:miriam:ensembl:ENSG00000172071;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:EIF2AK3;urn:miriam:uniprot:Q9NZJ5;urn:miriam:uniprot:Q9NZJ5;urn:miriam:ncbigene:9451;urn:miriam:ncbigene:9451;urn:miriam:refseq:NM_004836"
      hgnc "HGNC_SYMBOL:EIF2AK3"
      map_id "M118_8"
      name "EIF2AK3:EIF2AK3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa21"
      uniprot "UNIPROT:Q9NZJ5"
    ]
    graphics [
      x 774.6363830001421
      y 1390.8103462816025
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_71"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re357"
      uniprot "NA"
    ]
    graphics [
      x 689.1762957534975
      y 1509.2450385618513
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0006986"
      hgnc "NA"
      map_id "M118_159"
      name "UPR"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa36"
      uniprot "NA"
    ]
    graphics [
      x 820.3245881723627
      y 1479.4648649237297
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_159"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:PPP1R15A;urn:miriam:hgnc:14375;urn:miriam:refseq:NM_014330;urn:miriam:ensembl:ENSG00000087074;urn:miriam:uniprot:O75807;urn:miriam:ncbigene:23645"
      hgnc "HGNC_SYMBOL:PPP1R15A"
      map_id "M118_115"
      name "PPP1R15A"
      node_subtype "RNA"
      node_type "species"
      org_id "path_0_sa108"
      uniprot "UNIPROT:O75807"
    ]
    graphics [
      x 751.7732189166708
      y 1226.9545128514364
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_99"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "path_0_re68"
      uniprot "NA"
    ]
    graphics [
      x 488.9328834064189
      y 1151.753439174925
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:PPP1R15A;urn:miriam:hgnc:14375;urn:miriam:refseq:NM_014330;urn:miriam:ensembl:ENSG00000087074;urn:miriam:uniprot:O75807;urn:miriam:uniprot:O75807;urn:miriam:ncbigene:23645;urn:miriam:ncbigene:23645"
      hgnc "HGNC_SYMBOL:PPP1R15A"
      map_id "M118_114"
      name "PPP1R15A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa107"
      uniprot "UNIPROT:O75807"
    ]
    graphics [
      x 355.7622207851532
      y 1006.4584975966458
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:22926;urn:miriam:ensembl:ENSG00000118217;urn:miriam:ncbigene:22926;urn:miriam:refseq:NM_007348;urn:miriam:uniprot:P18850;urn:miriam:uniprot:P18850;urn:miriam:hgnc.symbol:ATF6;urn:miriam:hgnc:791"
      hgnc "HGNC_SYMBOL:ATF6"
      map_id "M118_208"
      name "ATF6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa62"
      uniprot "UNIPROT:P18850"
    ]
    graphics [
      x 611.7831970814902
      y 1653.257221184755
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_208"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:17991856;PUBMED:26587781;PUBMED:18360008"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_88"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re45"
      uniprot "NA"
    ]
    graphics [
      x 507.70942372559693
      y 1343.2436120176249
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_015884;urn:miriam:hgnc:15455;urn:miriam:ec-code:3.4.24.85;urn:miriam:hgnc.symbol:MBTPS2;urn:miriam:ncbigene:51360;urn:miriam:ncbigene:51360;urn:miriam:ensembl:ENSG00000012174;urn:miriam:uniprot:O43462;urn:miriam:uniprot:O43462"
      hgnc "HGNC_SYMBOL:MBTPS2"
      map_id "M118_205"
      name "MBTPS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa61"
      uniprot "UNIPROT:O43462"
    ]
    graphics [
      x 361.3899538782812
      y 1268.4184924906842
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_205"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:22926;urn:miriam:ensembl:ENSG00000118217;urn:miriam:ncbigene:22926;urn:miriam:refseq:NM_007348;urn:miriam:uniprot:P18850;urn:miriam:uniprot:P18850;urn:miriam:hgnc.symbol:ATF6;urn:miriam:hgnc:791"
      hgnc "HGNC_SYMBOL:ATF6"
      map_id "M118_217"
      name "ATF6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa64"
      uniprot "UNIPROT:P18850"
    ]
    graphics [
      x 651.9674981459576
      y 1121.7258566714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_217"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:pubmed:17981125;urn:miriam:taxonomy:10029;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:pubmed:25704011;urn:miriam:uniprot:Q99720;urn:miriam:uniprot:Q99720"
      hgnc "HGNC_SYMBOL:SIGMAR1"
      map_id "M118_324"
      name "SIGMAR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa86"
      uniprot "UNIPROT:Q99720"
    ]
    graphics [
      x 798.7058959817516
      y 856.6942978977116
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_324"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      annotation "PUBMED:17981125"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_333"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re137"
      uniprot "NA"
    ]
    graphics [
      x 884.9234812161793
      y 1097.3986282740318
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_333"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:17991856;PUBMED:26587781;PUBMED:18360008"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_73"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re359"
      uniprot "NA"
    ]
    graphics [
      x 679.7592782414979
      y 1250.9051175471338
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:823;urn:miriam:ncbigene:823;urn:miriam:ensembl:ENSG00000014216;urn:miriam:hgnc:1476;urn:miriam:ec-code:3.4.22.52;urn:miriam:hgnc.symbol:CAPN1;urn:miriam:refseq:NM_001198868;urn:miriam:uniprot:P07384;urn:miriam:uniprot:P07384"
      hgnc "HGNC_SYMBOL:CAPN1"
      map_id "M118_200"
      name "CAPN1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa602"
      uniprot "UNIPROT:P07384"
    ]
    graphics [
      x 1835.6817731922536
      y 1626.9791342705134
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_200"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      annotation "PUBMED:19931333"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_53"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_0_re324"
      uniprot "NA"
    ]
    graphics [
      x 1802.4052991036006
      y 1742.992475606615
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29108"
      hgnc "NA"
      map_id "M118_135"
      name "Ca2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "path_0_sa164"
      uniprot "NA"
    ]
    graphics [
      x 1663.0754152479772
      y 1895.0729249296305
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A1902656"
      hgnc "NA"
      map_id "M118_148"
      name "high_space_Ca2_plus__space_cytosolic_space_concentration"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa259"
      uniprot "NA"
    ]
    graphics [
      x 1941.6585395108782
      y 1510.435165065872
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_148"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29108;urn:miriam:ncbigene:823;urn:miriam:ncbigene:823;urn:miriam:ensembl:ENSG00000014216;urn:miriam:hgnc:1476;urn:miriam:ec-code:3.4.22.52;urn:miriam:hgnc.symbol:CAPN1;urn:miriam:refseq:NM_001198868;urn:miriam:uniprot:P07384;urn:miriam:uniprot:P07384"
      hgnc "HGNC_SYMBOL:CAPN1"
      map_id "M118_18"
      name "CAPN1:Ca2_plus_"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa56"
      uniprot "UNIPROT:P07384"
    ]
    graphics [
      x 1608.1750893542635
      y 1652.4670441323535
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      annotation "PUBMED:19052620"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_279"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re59"
      uniprot "NA"
    ]
    graphics [
      x 651.2365527717769
      y 692.1039343405645
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_279"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0034976"
      hgnc "NA"
      map_id "M118_190"
      name "ER_space_Stress"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa58"
      uniprot "NA"
    ]
    graphics [
      x 1457.2827380214082
      y 1631.2418400328265
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_190"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      annotation "PUBMED:17991856"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_65"
      name "NA"
      node_subtype "MODULATION"
      node_type "reaction"
      org_id "path_0_re349"
      uniprot "NA"
    ]
    graphics [
      x 1362.0449044333636
      y 1286.8427020088914
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0034976"
      hgnc "NA"
      map_id "M118_143"
      name "Persistant_space_ER_space_Stress"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa231"
      uniprot "NA"
    ]
    graphics [
      x 1316.4562873698742
      y 945.4896695176692
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_143"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:HSPA5;urn:miriam:ensembl:ENSG00000044574;urn:miriam:hgnc.symbol:HSPA5;urn:miriam:refseq:NM_005347;urn:miriam:hgnc:5238;urn:miriam:ncbigene:3309;urn:miriam:ncbigene:3309;urn:miriam:uniprot:P11021;urn:miriam:uniprot:P11021;urn:miriam:ec-code:3.6.4.10"
      hgnc "HGNC_SYMBOL:HSPA5"
      map_id "M118_325"
      name "HSPA5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa87"
      uniprot "UNIPROT:P11021"
    ]
    graphics [
      x 982.50540481563
      y 509.58654969818576
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_325"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "PUBMED:9388233"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_261"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re146"
      uniprot "NA"
    ]
    graphics [
      x 1081.5522376156719
      y 363.9225771203504
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_261"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:22511781;urn:miriam:taxonomy:10090;urn:miriam:obo.go:GO%3A0032469"
      hgnc "NA"
      map_id "M118_290"
      name "endoplasmic_space_reticulum_space_calcium_space_ion_space_homeostasis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa143"
      uniprot "NA"
    ]
    graphics [
      x 1180.1546708154854
      y 278.9249304574573
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_290"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:15692567;urn:miriam:obo.go:GO%3A0000266"
      hgnc "NA"
      map_id "M118_305"
      name "mitochondrial_space_fission"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa169"
      uniprot "NA"
    ]
    graphics [
      x 547.0423406840162
      y 1066.9324989598472
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_305"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      annotation "PUBMED:15692567"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_259"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re142"
      uniprot "NA"
    ]
    graphics [
      x 700.1055754230844
      y 1295.2791265775486
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_259"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:15692567;urn:miriam:obo.go:GO%3A0043653"
      hgnc "NA"
      map_id "M118_300"
      name "mitochondria_space_fragmentation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa154"
      uniprot "NA"
    ]
    graphics [
      x 827.7126010375882
      y 1401.279600769308
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_300"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      annotation "PUBMED:21183955"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_252"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_1_re126"
      uniprot "NA"
    ]
    graphics [
      x 614.0088066929114
      y 884.5615489907992
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_252"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:21689;urn:miriam:hgnc.symbol:FIS1;urn:miriam:refseq:NM_016068;urn:miriam:hgnc.symbol:FIS1;urn:miriam:ncbigene:51024;urn:miriam:ncbigene:51024;urn:miriam:ensembl:ENSG00000214253;urn:miriam:uniprot:Q9Y3D6;urn:miriam:uniprot:Q9Y3D6"
      hgnc "HGNC_SYMBOL:FIS1"
      map_id "M118_302"
      name "FIS1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa158"
      uniprot "UNIPROT:Q9Y3D6"
    ]
    graphics [
      x 287.2343155496243
      y 1047.0488588054718
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_302"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:15692567;urn:miriam:pubmed:21183955;urn:miriam:hgnc:16695;urn:miriam:refseq:NM_005745;urn:miriam:ensembl:ENSG00000185825;urn:miriam:ncbigene:10134;urn:miriam:ncbigene:10134;urn:miriam:hgnc.symbol:BCAP31;urn:miriam:hgnc.symbol:BCAP31;urn:miriam:uniprot:P51572;urn:miriam:uniprot:P51572;urn:miriam:hgnc:21689;urn:miriam:hgnc.symbol:FIS1;urn:miriam:refseq:NM_016068;urn:miriam:hgnc.symbol:FIS1;urn:miriam:ncbigene:51024;urn:miriam:ncbigene:51024;urn:miriam:ensembl:ENSG00000214253;urn:miriam:uniprot:Q9Y3D6;urn:miriam:uniprot:Q9Y3D6"
      hgnc "HGNC_SYMBOL:BCAP31;HGNC_SYMBOL:FIS1"
      map_id "M118_227"
      name "FIS1:BCAP31"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_1_csa1"
      uniprot "UNIPROT:P51572;UNIPROT:Q9Y3D6"
    ]
    graphics [
      x 870.9901004475182
      y 654.5547205124075
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_227"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      annotation "PUBMED:30773986;PUBMED:23850759;PUBMED:26587781;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_32"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re17"
      uniprot "NA"
    ]
    graphics [
      x 1436.8758349098696
      y 488.8598879175744
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:hgnc.symbol:TRAF2;urn:miriam:ncbigene:7186;urn:miriam:ncbigene:7186;urn:miriam:ensembl:ENSG00000127191;urn:miriam:refseq:NM_021138;urn:miriam:uniprot:Q12933;urn:miriam:uniprot:Q12933;urn:miriam:hgnc:12032"
      hgnc "HGNC_SYMBOL:TRAF2"
      map_id "M118_138"
      name "TRAF2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa204"
      uniprot "UNIPROT:Q12933"
    ]
    graphics [
      x 1580.9020444764906
      y 535.777947365639
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_138"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_001188;urn:miriam:ensembl:ENSG00000030110;urn:miriam:uniprot:Q16611;urn:miriam:uniprot:Q16611;urn:miriam:hgnc.symbol:BAK1;urn:miriam:ncbigene:578;urn:miriam:ncbigene:578;urn:miriam:hgnc:949;urn:miriam:ncbigene:2081;urn:miriam:refseq:NM_001433;urn:miriam:ncbigene:2081;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:3.1.26.-;urn:miriam:uniprot:O75460;urn:miriam:uniprot:O75460;urn:miriam:hgnc:3449;urn:miriam:ensembl:ENSG00000178607;urn:miriam:hgnc.symbol:ERN1"
      hgnc "HGNC_SYMBOL:BAK1;HGNC_SYMBOL:ERN1"
      map_id "M118_5"
      name "BAK1:ERN1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa19"
      uniprot "UNIPROT:Q16611;UNIPROT:O75460"
    ]
    graphics [
      x 1482.978260493689
      y 598.0645092571651
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:2081;urn:miriam:refseq:NM_001433;urn:miriam:ncbigene:2081;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:3.1.26.-;urn:miriam:uniprot:O75460;urn:miriam:uniprot:O75460;urn:miriam:hgnc:3449;urn:miriam:ensembl:ENSG00000178607;urn:miriam:hgnc.symbol:ERN1;urn:miriam:refseq:NM_138763;urn:miriam:hgnc:959;urn:miriam:ensembl:ENSG00000087088;urn:miriam:hgnc.symbol:BAX;urn:miriam:ncbigene:581;urn:miriam:uniprot:Q07812;urn:miriam:uniprot:Q07812;urn:miriam:ncbigene:581"
      hgnc "HGNC_SYMBOL:ERN1;HGNC_SYMBOL:BAX"
      map_id "M118_7"
      name "BAX:ERN1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa20"
      uniprot "UNIPROT:O75460;UNIPROT:Q07812"
    ]
    graphics [
      x 1580.040652116249
      y 431.881833279916
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:BCL2;urn:miriam:refseq:NM_000657;urn:miriam:ncbigene:596;urn:miriam:ncbigene:596;urn:miriam:uniprot:P10415;urn:miriam:uniprot:P10415;urn:miriam:ensembl:ENSG00000171791;urn:miriam:refseq:NM_000633;urn:miriam:hgnc:990"
      hgnc "HGNC_SYMBOL:BCL2"
      map_id "M118_131"
      name "BCL2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa152"
      uniprot "UNIPROT:P10415"
    ]
    graphics [
      x 1478.6734355053948
      y 332.13308629294204
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_131"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_138578;urn:miriam:ncbigene:598;urn:miriam:ncbigene:598;urn:miriam:ensembl:ENSG00000171552;urn:miriam:uniprot:Q07817;urn:miriam:uniprot:Q07817;urn:miriam:hgnc:992;urn:miriam:hgnc.symbol:BCL2L1"
      hgnc "HGNC_SYMBOL:BCL2L1"
      map_id "M118_132"
      name "BCL2L1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa153"
      uniprot "UNIPROT:Q07817"
    ]
    graphics [
      x 1534.0906689744502
      y 362.94092669738507
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_138763;urn:miriam:hgnc:959;urn:miriam:ensembl:ENSG00000087088;urn:miriam:hgnc.symbol:BAX;urn:miriam:ncbigene:581;urn:miriam:uniprot:Q07812;urn:miriam:uniprot:Q07812;urn:miriam:ncbigene:581"
      hgnc "HGNC_SYMBOL:BAX"
      map_id "M118_173"
      name "BAX"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa458"
      uniprot "UNIPROT:Q07812"
    ]
    graphics [
      x 1553.5196030688091
      y 491.8406746050058
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_173"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_001188;urn:miriam:ensembl:ENSG00000030110;urn:miriam:uniprot:Q16611;urn:miriam:uniprot:Q16611;urn:miriam:hgnc.symbol:BAK1;urn:miriam:ncbigene:578;urn:miriam:ncbigene:578;urn:miriam:hgnc:949"
      hgnc "HGNC_SYMBOL:BAK1"
      map_id "M118_174"
      name "BAK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa459"
      uniprot "UNIPROT:Q16611"
    ]
    graphics [
      x 1412.8527031430501
      y 351.3686141778368
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_174"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      annotation "PUBMED:21183955"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_253"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re127"
      uniprot "NA"
    ]
    graphics [
      x 97.14776855366745
      y 1068.8300107546258
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_253"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:obo.go:GO%3A0051882;urn:miriam:pubmed:21183955"
      hgnc "NA"
      map_id "M118_303"
      name "mitochondrial_space_outer_space_membrane_space_depolarization"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa159"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 1189.5275162306964
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_303"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:786;urn:miriam:refseq:NM_001675;urn:miriam:ncbigene:468;urn:miriam:hgnc.symbol:ATF4;urn:miriam:uniprot:P18848;urn:miriam:ensembl:ENSG00000128272"
      hgnc "HGNC_SYMBOL:ATF4"
      map_id "M118_112"
      name "ATF4"
      node_subtype "GENE"
      node_type "species"
      org_id "path_0_sa105"
      uniprot "UNIPROT:P18848"
    ]
    graphics [
      x 1577.2599471878418
      y 1457.2037833993825
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:15277680"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_96"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re65"
      uniprot "NA"
    ]
    graphics [
      x 1437.692006015899
      y 1373.7915900788735
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000130741;urn:miriam:hgnc.symbol:EIF2S3;urn:miriam:hgnc.symbol:EIF2S3;urn:miriam:hgnc:3267;urn:miriam:uniprot:P41091;urn:miriam:uniprot:P41091;urn:miriam:ec-code:3.6.5.3;urn:miriam:ncbigene:1968;urn:miriam:ncbigene:1968;urn:miriam:refseq:NM_001415;urn:miriam:obo.chebi:CHEBI%3A65180;urn:miriam:hgnc:3266;urn:miriam:hgnc.symbol:EIF2S2;urn:miriam:ncbigene:8894;urn:miriam:refseq:NM_003908;urn:miriam:ncbigene:8894;urn:miriam:uniprot:P20042;urn:miriam:uniprot:P20042;urn:miriam:ensembl:ENSG00000125977;urn:miriam:hgnc.symbol:EIF2S1;urn:miriam:uniprot:P05198;urn:miriam:uniprot:P05198;urn:miriam:hgnc:3265;urn:miriam:ncbigene:1965;urn:miriam:ncbigene:1965;urn:miriam:ensembl:ENSG00000134001;urn:miriam:refseq:NM_004094"
      hgnc "HGNC_SYMBOL:EIF2S3;HGNC_SYMBOL:EIF2S2;HGNC_SYMBOL:EIF2S1"
      map_id "M118_13"
      name "EIF2_minus_P:GDP"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa33"
      uniprot "UNIPROT:P41091;UNIPROT:P20042;UNIPROT:P05198"
    ]
    graphics [
      x 1179.1882561761456
      y 1288.8211981188024
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:786;urn:miriam:refseq:NM_001675;urn:miriam:ncbigene:468;urn:miriam:hgnc.symbol:ATF4;urn:miriam:uniprot:P18848;urn:miriam:ensembl:ENSG00000128272"
      hgnc "HGNC_SYMBOL:ATF4"
      map_id "M118_113"
      name "ATF4"
      node_subtype "RNA"
      node_type "species"
      org_id "path_0_sa106"
      uniprot "UNIPROT:P18848"
    ]
    graphics [
      x 1532.1535445478546
      y 1420.871915897851
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:3266;urn:miriam:hgnc.symbol:EIF2S2;urn:miriam:ncbigene:8894;urn:miriam:refseq:NM_003908;urn:miriam:ncbigene:8894;urn:miriam:uniprot:P20042;urn:miriam:uniprot:P20042;urn:miriam:ensembl:ENSG00000125977"
      hgnc "HGNC_SYMBOL:EIF2S2"
      map_id "M118_223"
      name "EIF2S2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa88"
      uniprot "UNIPROT:P20042"
    ]
    graphics [
      x 948.5342937944567
      y 1093.6739535316165
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_223"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:12667446;PUBMED:12601012"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_93"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_0_re62"
      uniprot "NA"
    ]
    graphics [
      x 872.3525888606202
      y 1048.5194966954573
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A65180;urn:miriam:ensembl:ENSG00000130741;urn:miriam:hgnc.symbol:EIF2S3;urn:miriam:hgnc.symbol:EIF2S3;urn:miriam:hgnc:3267;urn:miriam:uniprot:P41091;urn:miriam:uniprot:P41091;urn:miriam:ec-code:3.6.5.3;urn:miriam:ncbigene:1968;urn:miriam:ncbigene:1968;urn:miriam:refseq:NM_001415"
      hgnc "HGNC_SYMBOL:EIF2S3"
      map_id "M118_12"
      name "EIF2S3:GDP"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa31"
      uniprot "UNIPROT:P41091"
    ]
    graphics [
      x 978.7210738394485
      y 1039.0071655947972
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:EIF2S1;urn:miriam:uniprot:P05198;urn:miriam:uniprot:P05198;urn:miriam:hgnc:3265;urn:miriam:ncbigene:1965;urn:miriam:ncbigene:1965;urn:miriam:ensembl:ENSG00000134001;urn:miriam:refseq:NM_004094"
      hgnc "HGNC_SYMBOL:EIF2S1"
      map_id "M118_224"
      name "EIF2S1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa90"
      uniprot "UNIPROT:P05198"
    ]
    graphics [
      x 619.6937470594766
      y 1039.7164278193918
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_224"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:EIF2S1;urn:miriam:uniprot:P05198;urn:miriam:uniprot:P05198;urn:miriam:hgnc:3265;urn:miriam:ncbigene:1965;urn:miriam:ncbigene:1965;urn:miriam:ensembl:ENSG00000134001;urn:miriam:refseq:NM_004094;urn:miriam:hgnc:3266;urn:miriam:hgnc.symbol:EIF2S2;urn:miriam:ncbigene:8894;urn:miriam:refseq:NM_003908;urn:miriam:ncbigene:8894;urn:miriam:uniprot:P20042;urn:miriam:uniprot:P20042;urn:miriam:ensembl:ENSG00000125977;urn:miriam:ensembl:ENSG00000130741;urn:miriam:hgnc.symbol:EIF2S3;urn:miriam:hgnc.symbol:EIF2S3;urn:miriam:hgnc:3267;urn:miriam:uniprot:P41091;urn:miriam:uniprot:P41091;urn:miriam:ec-code:3.6.5.3;urn:miriam:ncbigene:1968;urn:miriam:ncbigene:1968;urn:miriam:refseq:NM_001415;urn:miriam:obo.chebi:CHEBI%3A65180"
      hgnc "HGNC_SYMBOL:EIF2S1;HGNC_SYMBOL:EIF2S2;HGNC_SYMBOL:EIF2S3"
      map_id "M118_11"
      name "EIF2:GDP"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa30"
      uniprot "UNIPROT:P05198;UNIPROT:P20042;UNIPROT:P41091"
    ]
    graphics [
      x 1063.3667249584023
      y 1247.058740413705
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:BID;urn:miriam:ncbigene:637;urn:miriam:ncbigene:637;urn:miriam:refseq:NM_197966;urn:miriam:uniprot:P55957;urn:miriam:uniprot:P55957;urn:miriam:ensembl:ENSG00000015475;urn:miriam:hgnc:1050"
      hgnc "HGNC_SYMBOL:BID"
      map_id "M118_171"
      name "BID"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa436"
      uniprot "UNIPROT:P55957"
    ]
    graphics [
      x 1003.1073181820814
      y 1208.0078551993665
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_171"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      annotation "PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_75"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re361"
      uniprot "NA"
    ]
    graphics [
      x 1293.5832715400118
      y 1156.8218290930345
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      annotation "PUBMED:29491369;PUBMED:19052620"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_249"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_1_re120"
      uniprot "NA"
    ]
    graphics [
      x 425.72861687744194
      y 359.41532668499633
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_249"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_014874;urn:miriam:hgnc.symbol:MFN2;urn:miriam:ensembl:ENSG00000116688;urn:miriam:uniprot:O95140;urn:miriam:uniprot:O95140;urn:miriam:hgnc.symbol:MFN2;urn:miriam:ncbigene:9927;urn:miriam:ncbigene:9927;urn:miriam:hgnc:16877;urn:miriam:ec-code:3.6.5.-"
      hgnc "HGNC_SYMBOL:MFN2"
      map_id "M118_299"
      name "MFN2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa153"
      uniprot "UNIPROT:O95140"
    ]
    graphics [
      x 433.617722603434
      y 518.2355397581107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_299"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:29491369;urn:miriam:pubmed:19052620;urn:miriam:refseq:NM_014874;urn:miriam:hgnc.symbol:MFN2;urn:miriam:ensembl:ENSG00000116688;urn:miriam:uniprot:O95140;urn:miriam:uniprot:O95140;urn:miriam:hgnc.symbol:MFN2;urn:miriam:ncbigene:9927;urn:miriam:ncbigene:9927;urn:miriam:hgnc:16877;urn:miriam:ec-code:3.6.5.-"
      hgnc "HGNC_SYMBOL:MFN2"
      map_id "M118_231"
      name "MFN2:MFN2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_1_csa17"
      uniprot "UNIPROT:O95140"
    ]
    graphics [
      x 427.58946493369615
      y 233.52042981482384
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_231"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:CDK5;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc:1774;urn:miriam:ensembl:ENSG00000164885;urn:miriam:ncbigene:1020;urn:miriam:ncbigene:1020;urn:miriam:uniprot:Q00535;urn:miriam:uniprot:Q00535;urn:miriam:refseq:NM_001164410"
      hgnc "HGNC_SYMBOL:CDK5"
      map_id "M118_129"
      name "CDK5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa147"
      uniprot "UNIPROT:Q00535"
    ]
    graphics [
      x 1804.6921707744384
      y 1566.5607350782693
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_129"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_106"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "path_0_re94"
      uniprot "NA"
    ]
    graphics [
      x 1670.3500937048593
      y 1581.3875159033082
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:CDK5;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc:1774;urn:miriam:ensembl:ENSG00000164885;urn:miriam:ncbigene:1020;urn:miriam:ncbigene:1020;urn:miriam:uniprot:Q00535;urn:miriam:uniprot:Q00535;urn:miriam:refseq:NM_001164410"
      hgnc "HGNC_SYMBOL:CDK5"
      map_id "M118_128"
      name "CDK5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa146"
      uniprot "UNIPROT:Q00535"
    ]
    graphics [
      x 1768.8048397116004
      y 1476.7160075498348
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:3266;urn:miriam:hgnc.symbol:EIF2S2;urn:miriam:ncbigene:8894;urn:miriam:refseq:NM_003908;urn:miriam:ncbigene:8894;urn:miriam:uniprot:P20042;urn:miriam:uniprot:P20042;urn:miriam:ensembl:ENSG00000125977;urn:miriam:ensembl:ENSG00000130741;urn:miriam:hgnc.symbol:EIF2S3;urn:miriam:hgnc.symbol:EIF2S3;urn:miriam:hgnc:3267;urn:miriam:uniprot:P41091;urn:miriam:uniprot:P41091;urn:miriam:ec-code:3.6.5.3;urn:miriam:ncbigene:1968;urn:miriam:ncbigene:1968;urn:miriam:refseq:NM_001415;urn:miriam:hgnc.symbol:EIF2S1;urn:miriam:uniprot:P05198;urn:miriam:uniprot:P05198;urn:miriam:hgnc:3265;urn:miriam:ncbigene:1965;urn:miriam:ncbigene:1965;urn:miriam:ensembl:ENSG00000134001;urn:miriam:refseq:NM_004094;urn:miriam:obo.chebi:CHEBI%3A65180;urn:miriam:hgnc:34779;urn:miriam:hgnc.symbol:TRM-CAT3-1;urn:miriam:ncbigene:100189216"
      hgnc "HGNC_SYMBOL:EIF2S2;HGNC_SYMBOL:EIF2S3;HGNC_SYMBOL:EIF2S1;HGNC_SYMBOL:TRM-CAT3-1"
      map_id "M118_20"
      name "EIF2:GDP:Met_minus_tRNA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa7"
      uniprot "UNIPROT:P20042;UNIPROT:P41091;UNIPROT:P05198"
    ]
    graphics [
      x 1169.1949433195143
      y 1829.064296562428
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_94"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re63"
      uniprot "NA"
    ]
    graphics [
      x 1077.2492504629604
      y 1741.957616616633
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57600"
      hgnc "NA"
      map_id "M118_109"
      name "GTP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "path_0_sa102"
      uniprot "NA"
    ]
    graphics [
      x 1085.6417333043812
      y 1608.047307552495
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000145191;urn:miriam:ncbigene:8893;urn:miriam:ncbigene:8893;urn:miriam:hgnc.symbol:EIF2B5;urn:miriam:refseq:NM_003907;urn:miriam:uniprot:Q13144;urn:miriam:uniprot:Q13144;urn:miriam:hgnc.symbol:EIF2B5;urn:miriam:hgnc:3261;urn:miriam:hgnc.symbol:EIF2B1;urn:miriam:hgnc.symbol:EIF2B1;urn:miriam:hgnc:3257;urn:miriam:ncbigene:1967;urn:miriam:ncbigene:1967;urn:miriam:ensembl:ENSG00000111361;urn:miriam:uniprot:Q14232;urn:miriam:uniprot:Q14232;urn:miriam:refseq:NM_001414;urn:miriam:ensembl:ENSG00000070785;urn:miriam:hgnc.symbol:EIF2B3;urn:miriam:hgnc.symbol:EIF2B3;urn:miriam:hgnc:3259;urn:miriam:ncbigene:8891;urn:miriam:ncbigene:8891;urn:miriam:refseq:NM_020365;urn:miriam:uniprot:Q9NR50;urn:miriam:uniprot:Q9NR50;urn:miriam:ncbigene:8890;urn:miriam:ncbigene:8890;urn:miriam:uniprot:Q9UI10;urn:miriam:uniprot:Q9UI10;urn:miriam:hgnc.symbol:EIF2B4;urn:miriam:hgnc.symbol:EIF2B4;urn:miriam:ensembl:ENSG00000115211;urn:miriam:hgnc:3260;urn:miriam:refseq:NM_001034116;urn:miriam:hgnc.symbol:EIF2B2;urn:miriam:hgnc.symbol:EIF2B2;urn:miriam:ncbigene:8892;urn:miriam:ncbigene:8892;urn:miriam:hgnc:3258;urn:miriam:uniprot:P49770;urn:miriam:uniprot:P49770;urn:miriam:ensembl:ENSG00000119718;urn:miriam:refseq:NM_014239"
      hgnc "HGNC_SYMBOL:EIF2B5;HGNC_SYMBOL:EIF2B1;HGNC_SYMBOL:EIF2B3;HGNC_SYMBOL:EIF2B4;HGNC_SYMBOL:EIF2B2"
      map_id "M118_14"
      name "EIF2B"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa34"
      uniprot "UNIPROT:Q13144;UNIPROT:Q14232;UNIPROT:Q9NR50;UNIPROT:Q9UI10;UNIPROT:P49770"
    ]
    graphics [
      x 999.2247258622621
      y 1544.162634249813
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:EIF2S1;urn:miriam:uniprot:P05198;urn:miriam:uniprot:P05198;urn:miriam:hgnc:3265;urn:miriam:ncbigene:1965;urn:miriam:ncbigene:1965;urn:miriam:ensembl:ENSG00000134001;urn:miriam:refseq:NM_004094;urn:miriam:hgnc:3266;urn:miriam:hgnc.symbol:EIF2S2;urn:miriam:ncbigene:8894;urn:miriam:refseq:NM_003908;urn:miriam:ncbigene:8894;urn:miriam:uniprot:P20042;urn:miriam:uniprot:P20042;urn:miriam:ensembl:ENSG00000125977;urn:miriam:obo.chebi:CHEBI%3A57600;urn:miriam:ensembl:ENSG00000130741;urn:miriam:hgnc.symbol:EIF2S3;urn:miriam:hgnc.symbol:EIF2S3;urn:miriam:hgnc:3267;urn:miriam:uniprot:P41091;urn:miriam:uniprot:P41091;urn:miriam:ec-code:3.6.5.3;urn:miriam:ncbigene:1968;urn:miriam:ncbigene:1968;urn:miriam:refseq:NM_001415;urn:miriam:hgnc:34779;urn:miriam:hgnc.symbol:TRM-CAT3-1;urn:miriam:ncbigene:100189216"
      hgnc "HGNC_SYMBOL:EIF2S1;HGNC_SYMBOL:EIF2S2;HGNC_SYMBOL:EIF2S3;HGNC_SYMBOL:TRM-CAT3-1"
      map_id "M118_21"
      name "Ternary_space_Complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa8"
      uniprot "UNIPROT:P05198;UNIPROT:P20042;UNIPROT:P41091"
    ]
    graphics [
      x 1017.3712601158924
      y 1966.4447893375063
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A65180"
      hgnc "NA"
      map_id "M118_108"
      name "GDP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "path_0_sa100"
      uniprot "NA"
    ]
    graphics [
      x 1014.5674709684888
      y 1804.9422068554672
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      annotation "PUBMED:30590907;PUBMED:17981125"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_268"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_1_re42"
      uniprot "NA"
    ]
    graphics [
      x 742.6553162947894
      y 603.9920283745055
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_268"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_002224;urn:miriam:hgnc.symbol:ITPR3;urn:miriam:hgnc.symbol:ITPR3;urn:miriam:pubmed:17981125;urn:miriam:uniprot:Q14573;urn:miriam:uniprot:Q14573;urn:miriam:hgnc:6182;urn:miriam:ncbigene:3710;urn:miriam:ensembl:ENSG00000096433;urn:miriam:ncbigene:3710"
      hgnc "HGNC_SYMBOL:ITPR3"
      map_id "M118_326"
      name "ITPR3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa88"
      uniprot "UNIPROT:Q14573"
    ]
    graphics [
      x 799.9074213675638
      y 504.4037610296807
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_326"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:22511781;urn:miriam:taxonomy:10090;urn:miriam:obo.go:GO%3A0034976"
      hgnc "NA"
      map_id "M118_322"
      name "ER_space_stress"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa69"
      uniprot "NA"
    ]
    graphics [
      x 766.2419135790983
      y 796.5733870584756
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_322"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:30590907;urn:miriam:obo.go:GO%3A0032471;urn:miriam:pubmed:17981125;urn:miriam:taxonomy:10029"
      hgnc "NA"
      map_id "M118_319"
      name "Ca2_plus__space_ER_space_depletion"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa60"
      uniprot "NA"
    ]
    graphics [
      x 712.7174073842018
      y 495.3058428842593
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_319"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:30590907;urn:miriam:pubmed:17981125;urn:miriam:taxonomy:10029;urn:miriam:refseq:NM_002224;urn:miriam:hgnc.symbol:ITPR3;urn:miriam:hgnc.symbol:ITPR3;urn:miriam:pubmed:17981125;urn:miriam:uniprot:Q14573;urn:miriam:uniprot:Q14573;urn:miriam:hgnc:6182;urn:miriam:ncbigene:3710;urn:miriam:ensembl:ENSG00000096433;urn:miriam:ncbigene:3710;urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:uniprot:Q99720;urn:miriam:uniprot:Q99720"
      hgnc "HGNC_SYMBOL:ITPR3;HGNC_SYMBOL:SIGMAR1"
      map_id "M118_229"
      name "SIGMAR1:ITPR3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_1_csa14"
      uniprot "UNIPROT:Q14573;UNIPROT:Q99720"
    ]
    graphics [
      x 785.3049186317621
      y 324.88282829222055
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_229"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:2081;urn:miriam:refseq:NM_001433;urn:miriam:ncbigene:2081;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:3.1.26.-;urn:miriam:uniprot:O75460;urn:miriam:uniprot:O75460;urn:miriam:hgnc:3449;urn:miriam:ensembl:ENSG00000178607;urn:miriam:hgnc.symbol:ERN1"
      hgnc "HGNC_SYMBOL:ERN1"
      map_id "M118_206"
      name "ERN1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa617"
      uniprot "UNIPROT:O75460"
    ]
    graphics [
      x 2006.386277255508
      y 439.5036880988031
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_206"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      annotation "PUBMED:17991856;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_39"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_0_re245"
      uniprot "NA"
    ]
    graphics [
      x 1865.7558443904236
      y 561.0838986765593
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:Q9BXH1;urn:miriam:uniprot:Q9BXH1;urn:miriam:ncbigene:27113;urn:miriam:ncbigene:27113;urn:miriam:hgnc:17868;urn:miriam:ensembl:ENSG00000105327;urn:miriam:hgnc.symbol:BBC3;urn:miriam:refseq:NM_014417;urn:miriam:uniprot:Q96PG8;urn:miriam:uniprot:Q96PG8"
      hgnc "HGNC_SYMBOL:BBC3"
      map_id "M118_177"
      name "BBC3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa476"
      uniprot "UNIPROT:Q9BXH1;UNIPROT:Q96PG8"
    ]
    graphics [
      x 1858.5220035942584
      y 428.265506636656
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_177"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0034976"
      hgnc "NA"
      map_id "M118_216"
      name "Persistant_space_ER_space_Stress"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa633"
      uniprot "NA"
    ]
    graphics [
      x 1696.8371454164703
      y 455.3740808191935
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_216"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      annotation "PUBMED:22511781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_282"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re71"
      uniprot "NA"
    ]
    graphics [
      x 358.9370414878696
      y 463.998684773926
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_282"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0030968;urn:miriam:pubmed:22511781;urn:miriam:taxonomy:10090"
      hgnc "NA"
      map_id "M118_287"
      name "unfolded_space_protein_space_response_space_(UPR)"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa120"
      uniprot "NA"
    ]
    graphics [
      x 267.43080861242606
      y 601.2819602647382
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_287"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:19411314;urn:miriam:uniprot:P59594;urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:uniprot:W6A028;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S;urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:S;HGNC_SYMBOL:ACE2"
      map_id "M118_3"
      name "ACE2_minus_SARS_minus_CoV_space_interaction"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa31"
      uniprot "UNIPROT:P59594;UNIPROT:P0DTC2;UNIPROT:W6A028;UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1785.587685953583
      y 1664.0850408667093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      annotation "PUBMED:19411314"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_337"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re141"
      uniprot "NA"
    ]
    graphics [
      x 2012.9475670627362
      y 1578.0143073081904
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_337"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_350"
      name "SARS_minus_CoV_space_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa279"
      uniprot "NA"
    ]
    graphics [
      x 2160.036833097254
      y 1542.3578900549512
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_350"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_80"
      name "NA"
      node_subtype "MODULATION"
      node_type "reaction"
      org_id "path_0_re367"
      uniprot "NA"
    ]
    graphics [
      x 1766.7954450266411
      y 1059.5993056481802
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0043066"
      hgnc "NA"
      map_id "M118_125"
      name "Cell_space_survival"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa135"
      uniprot "NA"
    ]
    graphics [
      x 1887.630938526403
      y 1107.8594034856808
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_125"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      annotation "PUBMED:22511781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_280"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re67"
      uniprot "NA"
    ]
    graphics [
      x 807.2880354805981
      y 1040.666052563809
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_280"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ec-code:2.7.11.24;urn:miriam:refseq:NM_001278547;urn:miriam:ensembl:ENSG00000107643;urn:miriam:hgnc.symbol:MAPK8;urn:miriam:uniprot:P45983;urn:miriam:uniprot:P45983;urn:miriam:hgnc:6881;urn:miriam:ncbigene:5599;urn:miriam:ncbigene:5599"
      hgnc "HGNC_SYMBOL:MAPK8"
      map_id "M118_164"
      name "MAPK8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa40"
      uniprot "UNIPROT:P45983"
    ]
    graphics [
      x 1384.464473198414
      y 1079.1118349176058
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_164"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:26587781;PUBMED:18191217"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_46"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re29"
      uniprot "NA"
    ]
    graphics [
      x 1399.6013189304285
      y 958.6798600352639
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_001291958;urn:miriam:hgnc:6856;urn:miriam:hgnc.symbol:MAP3K4;urn:miriam:uniprot:Q9Y6R4;urn:miriam:uniprot:Q9Y6R4;urn:miriam:ncbigene:4216;urn:miriam:ncbigene:4216;urn:miriam:ensembl:ENSG00000085511;urn:miriam:ec-code:2.7.11.25"
      hgnc "HGNC_SYMBOL:MAP3K4"
      map_id "M118_127"
      name "MAP3K4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa145"
      uniprot "UNIPROT:Q9Y6R4"
    ]
    graphics [
      x 1508.3346261311806
      y 1193.352433016298
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_127"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ec-code:2.7.11.24;urn:miriam:refseq:NM_001278547;urn:miriam:ensembl:ENSG00000107643;urn:miriam:hgnc.symbol:MAPK8;urn:miriam:uniprot:P45983;urn:miriam:uniprot:P45983;urn:miriam:hgnc:6881;urn:miriam:ncbigene:5599;urn:miriam:ncbigene:5599"
      hgnc "HGNC_SYMBOL:MAPK8"
      map_id "M118_165"
      name "MAPK8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa41"
      uniprot "UNIPROT:P45983"
    ]
    graphics [
      x 1154.1281376282031
      y 990.8866979301513
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_165"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      annotation "PUBMED:17981125"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_332"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re136"
      uniprot "NA"
    ]
    graphics [
      x 725.5856290051113
      y 930.5690368388205
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_332"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      annotation "PUBMED:30590907;PUBMED:29491369;PUBMED:19052620"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_273"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re49"
      uniprot "NA"
    ]
    graphics [
      x 547.8084893057375
      y 338.68153555864205
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_273"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:27457486;urn:miriam:pubmed:30590907;urn:miriam:pubmed:29491369;urn:miriam:obo.go:GO%3A1990456;urn:miriam:pubmed:28132811;urn:miriam:pubmed:19052620"
      hgnc "NA"
      map_id "M118_327"
      name "mitochondrion_minus_endoplasmic_space_reticulum_space_membrane_space_tethering_space_"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa97"
      uniprot "NA"
    ]
    graphics [
      x 559.4373130110703
      y 195.77312665323814
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_327"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_001204106;urn:miriam:hgnc:994;urn:miriam:hgnc.symbol:BCL2L11;urn:miriam:ncbigene:10018;urn:miriam:ensembl:ENSG00000153094;urn:miriam:ncbigene:10018;urn:miriam:uniprot:O43521;urn:miriam:uniprot:O43521"
      hgnc "HGNC_SYMBOL:BCL2L11"
      map_id "M118_169"
      name "BCL2L11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa434"
      uniprot "UNIPROT:O43521"
    ]
    graphics [
      x 1450.5711327143447
      y 1512.8035229834736
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_169"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_40"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re249"
      uniprot "NA"
    ]
    graphics [
      x 1383.0697674768076
      y 1739.4443823445397
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:5515;urn:miriam:ncbigene:5515;urn:miriam:ensembl:ENSG00000113575;urn:miriam:refseq:NM_002715;urn:miriam:ec-code:3.1.3.16;urn:miriam:uniprot:P67775;urn:miriam:uniprot:P67775;urn:miriam:hgnc.symbol:PPP2CA;urn:miriam:hgnc:9299"
      hgnc "HGNC_SYMBOL:PPP2CA"
      map_id "M118_201"
      name "PPP2CA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa603"
      uniprot "UNIPROT:P67775"
    ]
    graphics [
      x 1441.1810077130601
      y 1683.7568164440604
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_201"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_001204106;urn:miriam:hgnc:994;urn:miriam:hgnc.symbol:BCL2L11;urn:miriam:ncbigene:10018;urn:miriam:ensembl:ENSG00000153094;urn:miriam:ncbigene:10018;urn:miriam:uniprot:O43521;urn:miriam:uniprot:O43521"
      hgnc "HGNC_SYMBOL:BCL2L11"
      map_id "M118_168"
      name "BCL2L11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa433"
      uniprot "UNIPROT:O43521"
    ]
    graphics [
      x 1305.2852214205247
      y 1580.051189416995
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_168"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0032471"
      hgnc "NA"
      map_id "M118_124"
      name "release_space_of_space_ER_space_Ca2_plus_"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa132"
      uniprot "NA"
    ]
    graphics [
      x 1900.4304829095204
      y 700.0376815308081
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_124"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_56"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re329"
      uniprot "NA"
    ]
    graphics [
      x 1982.7757428033633
      y 1118.1438807968657
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re221"
      uniprot "NA"
    ]
    graphics [
      x 1285.4947125231722
      y 1336.0302602501536
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:34779;urn:miriam:hgnc.symbol:TRM-CAT3-1;urn:miriam:ncbigene:100189216"
      hgnc "HGNC_SYMBOL:TRM-CAT3-1"
      map_id "M118_156"
      name "Met_minus_tRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "path_0_sa293"
      uniprot "NA"
    ]
    graphics [
      x 1251.5618955124037
      y 1432.2994030261184
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_156"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:12667446"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_30"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_0_re165"
      uniprot "NA"
    ]
    graphics [
      x 1140.062342235079
      y 1526.3837017592857
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      annotation "PUBMED:17991856;PUBMED:26587781;PUBMED:18360008"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_86"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "path_0_re42"
      uniprot "NA"
    ]
    graphics [
      x 1394.3772755247455
      y 1858.6838303730553
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:22926;urn:miriam:ensembl:ENSG00000118217;urn:miriam:ncbigene:22926;urn:miriam:refseq:NM_007348;urn:miriam:uniprot:P18850;urn:miriam:uniprot:P18850;urn:miriam:hgnc.symbol:ATF6;urn:miriam:hgnc:791"
      hgnc "HGNC_SYMBOL:ATF6"
      map_id "M118_189"
      name "ATF6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa57"
      uniprot "UNIPROT:P18850"
    ]
    graphics [
      x 1116.0704492278428
      y 1958.9737304514595
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_189"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:PACS2;urn:miriam:hgnc.symbol:PACS2;urn:miriam:ncbigene:23241;urn:miriam:uniprot:Q86VP3;urn:miriam:uniprot:Q86VP3;urn:miriam:ncbigene:23241;urn:miriam:pubmed:15692567;urn:miriam:hgnc:23794;urn:miriam:pubmed:26108729;urn:miriam:refseq:NM_001100913;urn:miriam:ensembl:ENSG00000179364"
      hgnc "HGNC_SYMBOL:PACS2"
      map_id "M118_308"
      name "PACS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa175"
      uniprot "UNIPROT:Q86VP3"
    ]
    graphics [
      x 523.749117443537
      y 1852.0231021882125
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_308"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      annotation "PUBMED:26108729"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_263"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_1_re152"
      uniprot "NA"
    ]
    graphics [
      x 619.5549872279496
      y 1920.9082842700009
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_263"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000151694;urn:miriam:ncbigene:6868;urn:miriam:ncbigene:6868;urn:miriam:refseq:NM_001382777;urn:miriam:ec-code:3.4.24.86;urn:miriam:hgnc:195;urn:miriam:uniprot:P78536;urn:miriam:uniprot:P78536;urn:miriam:pubmed:32264791;urn:miriam:hgnc.symbol:ADAM17;urn:miriam:hgnc.symbol:ADAM17;urn:miriam:pubmed:26108729"
      hgnc "HGNC_SYMBOL:ADAM17"
      map_id "M118_307"
      name "ADAM17"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa174"
      uniprot "UNIPROT:P78536"
    ]
    graphics [
      x 640.8633873602655
      y 2033.9000562251933
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_307"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 167
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:32264791;urn:miriam:pubmed:26108729;urn:miriam:ensembl:ENSG00000151694;urn:miriam:ncbigene:6868;urn:miriam:ncbigene:6868;urn:miriam:refseq:NM_001382777;urn:miriam:ec-code:3.4.24.86;urn:miriam:hgnc:195;urn:miriam:uniprot:P78536;urn:miriam:uniprot:P78536;urn:miriam:pubmed:32264791;urn:miriam:hgnc.symbol:ADAM17;urn:miriam:hgnc.symbol:ADAM17;urn:miriam:pubmed:26108729;urn:miriam:hgnc.symbol:PACS2;urn:miriam:hgnc.symbol:PACS2;urn:miriam:ncbigene:23241;urn:miriam:uniprot:Q86VP3;urn:miriam:uniprot:Q86VP3;urn:miriam:ncbigene:23241;urn:miriam:pubmed:15692567;urn:miriam:hgnc:23794;urn:miriam:refseq:NM_001100913;urn:miriam:ensembl:ENSG00000179364"
      hgnc "HGNC_SYMBOL:ADAM17;HGNC_SYMBOL:PACS2"
      map_id "M118_232"
      name "PACS2_minus_ADAM17_space_interaction"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_1_csa18"
      uniprot "UNIPROT:P78536;UNIPROT:Q86VP3"
    ]
    graphics [
      x 785.2504177556277
      y 1820.598446873799
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_232"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 168
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_006389;urn:miriam:hgnc:16931;urn:miriam:ncbigene:10525;urn:miriam:ensembl:ENSG00000149428;urn:miriam:uniprot:Q9Y4L1;urn:miriam:hgnc.symbol:HYOU1"
      hgnc "HGNC_SYMBOL:HYOU1"
      map_id "M118_202"
      name "HYOU1"
      node_subtype "RNA"
      node_type "species"
      org_id "path_0_sa604"
      uniprot "UNIPROT:Q9Y4L1"
    ]
    graphics [
      x 1641.7429794934499
      y 1243.1824444927938
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_202"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 169
    zlevel -1

    cd19dm [
      annotation "PUBMED:18360008"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_55"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "path_0_re328"
      uniprot "NA"
    ]
    graphics [
      x 1613.3107685223463
      y 1385.5485745890726
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 170
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_006389;urn:miriam:hgnc:16931;urn:miriam:ncbigene:10525;urn:miriam:ncbigene:10525;urn:miriam:ensembl:ENSG00000149428;urn:miriam:uniprot:Q9Y4L1;urn:miriam:uniprot:Q9Y4L1;urn:miriam:hgnc.symbol:HYOU1"
      hgnc "HGNC_SYMBOL:HYOU1"
      map_id "M118_204"
      name "HYOU1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa606"
      uniprot "UNIPROT:Q9Y4L1"
    ]
    graphics [
      x 1480.9835848333773
      y 1433.9850935068955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_204"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 171
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:CASP9;urn:miriam:hgnc.symbol:CASP9;urn:miriam:refseq:NM_032996;urn:miriam:ncbigene:842;urn:miriam:ncbigene:842;urn:miriam:hgnc.symbol:CSAP9;urn:miriam:hgnc:1511;urn:miriam:ensembl:ENSG00000132906;urn:miriam:ec-code:3.4.22.62;urn:miriam:uniprot:P55211;urn:miriam:uniprot:P55211"
      hgnc "HGNC_SYMBOL:CASP9;HGNC_SYMBOL:CSAP9"
      map_id "M118_197"
      name "CASP9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa598"
      uniprot "UNIPROT:P55211"
    ]
    graphics [
      x 1610.8602400280727
      y 1719.6568722984007
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_197"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 172
    zlevel -1

    cd19dm [
      annotation "PUBMED:18955970;PUBMED:19931333;PUBMED:24373849"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_52"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re323"
      uniprot "NA"
    ]
    graphics [
      x 1603.2033295585836
      y 1559.919165886493
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 173
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ec-code:3.4.22.57;urn:miriam:ensembl:ENSG00000196954;urn:miriam:ncbigene:837;urn:miriam:ncbigene:837;urn:miriam:hgnc:1505;urn:miriam:refseq:NM_001225;urn:miriam:hgnc.symbol:CASP4;urn:miriam:uniprot:P49662;urn:miriam:uniprot:P49662"
      hgnc "HGNC_SYMBOL:CASP4"
      map_id "M118_194"
      name "cleaved~CASP4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa595"
      uniprot "UNIPROT:P49662"
    ]
    graphics [
      x 1394.4396428240466
      y 1617.86762405011
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_194"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 174
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:CASP9;urn:miriam:refseq:NM_032996;urn:miriam:ncbigene:842;urn:miriam:ncbigene:842;urn:miriam:hgnc:1511;urn:miriam:ensembl:ENSG00000132906;urn:miriam:ec-code:3.4.22.62;urn:miriam:uniprot:P55211;urn:miriam:uniprot:P55211"
      hgnc "HGNC_SYMBOL:CASP9"
      map_id "M118_198"
      name "cleaved~CASP9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa599"
      uniprot "UNIPROT:P55211"
    ]
    graphics [
      x 1742.1841043690185
      y 1363.6720701821607
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_198"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 175
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29108"
      hgnc "NA"
      map_id "M118_317"
      name "Ca2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "path_1_sa54"
      uniprot "NA"
    ]
    graphics [
      x 943.3791022427754
      y 297.84969898456893
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_317"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 176
    zlevel -1

    cd19dm [
      annotation "PUBMED:30590907;PUBMED:30590033"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_265"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "path_1_re19"
      uniprot "NA"
    ]
    graphics [
      x 887.3429037757129
      y 136.29191638710563
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_265"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 177
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:30590033;urn:miriam:taxonomy:9606;urn:miriam:pubmed:29491369;urn:miriam:refseq:NM_002224;urn:miriam:hgnc.symbol:ITPR3;urn:miriam:hgnc.symbol:ITPR3;urn:miriam:pubmed:17981125;urn:miriam:uniprot:Q14573;urn:miriam:uniprot:Q14573;urn:miriam:hgnc:6182;urn:miriam:ncbigene:3710;urn:miriam:ensembl:ENSG00000096433;urn:miriam:ncbigene:3710;urn:miriam:ensembl:ENSG00000213585;urn:miriam:hgnc:12669;urn:miriam:refseq:NM_003374;urn:miriam:uniprot:P21796;urn:miriam:uniprot:P21796;urn:miriam:hgnc.symbol:VDAC1;urn:miriam:hgnc.symbol:VDAC1;urn:miriam:ncbigene:7416;urn:miriam:ncbigene:7416;urn:miriam:hgnc:5244;urn:miriam:refseq:NM_004134;urn:miriam:ensembl:ENSG00000113013;urn:miriam:uniprot:P38646;urn:miriam:uniprot:P38646;urn:miriam:hgnc.symbol:HSPA9;urn:miriam:hgnc.symbol:HSPA9;urn:miriam:ncbigene:3313;urn:miriam:ncbigene:3313"
      hgnc "HGNC_SYMBOL:ITPR3;HGNC_SYMBOL:VDAC1;HGNC_SYMBOL:HSPA9"
      map_id "M118_233"
      name "ITPR3:HSPA9:VDAC1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_1_csa2"
      uniprot "UNIPROT:Q14573;UNIPROT:P21796;UNIPROT:P38646"
    ]
    graphics [
      x 1011.4904913213607
      y 303.27001466174283
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_233"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 178
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_002224;urn:miriam:hgnc.symbol:ITPR3;urn:miriam:hgnc.symbol:ITPR3;urn:miriam:pubmed:17981125;urn:miriam:uniprot:Q14573;urn:miriam:uniprot:Q14573;urn:miriam:hgnc:6182;urn:miriam:ncbigene:3710;urn:miriam:ensembl:ENSG00000096433;urn:miriam:ncbigene:3710"
      hgnc "HGNC_SYMBOL:ITPR3"
      map_id "M118_285"
      name "ITPR3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa11"
      uniprot "UNIPROT:Q14573"
    ]
    graphics [
      x 793.4202707315616
      y 169.67817322736346
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_285"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 179
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29108"
      hgnc "NA"
      map_id "M118_318"
      name "Ca2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "path_1_sa55"
      uniprot "NA"
    ]
    graphics [
      x 996.3486137608176
      y 141.84890431202507
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_318"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 180
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_82"
      name "NA"
      node_subtype "MODULATION"
      node_type "reaction"
      org_id "path_0_re372"
      uniprot "NA"
    ]
    graphics [
      x 1778.650036997944
      y 1004.3626079993511
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 181
    zlevel -1

    cd19dm [
      annotation "PUBMED:18955970"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_67"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re352"
      uniprot "NA"
    ]
    graphics [
      x 1866.4010042826053
      y 1769.5126816765996
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 182
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:29491369;urn:miriam:pubmed:28132811;urn:miriam:hgnc:12649;urn:miriam:ensembl:ENSG00000124164;urn:miriam:hgnc.symbol:VAPB;urn:miriam:hgnc.symbol:VAPB;urn:miriam:uniprot:O95292;urn:miriam:uniprot:O95292;urn:miriam:ncbigene:9217;urn:miriam:refseq:NM_001195677;urn:miriam:ncbigene:9217;urn:miriam:uniprot:Q96TC7;urn:miriam:uniprot:Q96TC7;urn:miriam:refseq:NM_018145;urn:miriam:ncbigene:55177;urn:miriam:ncbigene:55177;urn:miriam:hgnc.symbol:RMDN3;urn:miriam:hgnc.symbol:RMDN3;urn:miriam:ensembl:ENSG00000137824;urn:miriam:hgnc:25550"
      hgnc "HGNC_SYMBOL:VAPB;HGNC_SYMBOL:RMDN3"
      map_id "M118_235"
      name "VAPB:RMDN3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_1_csa5"
      uniprot "UNIPROT:O95292;UNIPROT:Q96TC7"
    ]
    graphics [
      x 431.3105833002327
      y 644.8524080889466
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_235"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 183
    zlevel -1

    cd19dm [
      annotation "PUBMED:30590907;PUBMED:28132811"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_274"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re50"
      uniprot "NA"
    ]
    graphics [
      x 538.9463074105155
      y 416.7825050548561
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_274"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 184
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:DDIT3;urn:miriam:ncbigene:1649;urn:miriam:uniprot:P35638;urn:miriam:hgnc:2726;urn:miriam:refseq:NM_004083;urn:miriam:ensembl:ENSG00000175197;urn:miriam:uniprot:P0DPQ6"
      hgnc "HGNC_SYMBOL:DDIT3"
      map_id "M118_221"
      name "DDIT3"
      node_subtype "RNA"
      node_type "species"
      org_id "path_0_sa71"
      uniprot "UNIPROT:P35638;UNIPROT:P0DPQ6"
    ]
    graphics [
      x 1297.2327115714284
      y 610.804595654427
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_221"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 185
    zlevel -1

    cd19dm [
      annotation "PUBMED:26137585;PUBMED:23850759;PUBMED:17991856"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_90"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "path_0_re49"
      uniprot "NA"
    ]
    graphics [
      x 1173.1591408059473
      y 786.4050535897911
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 186
    zlevel -1

    cd19dm [
      annotation "PUBMED:28132811"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_247"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "path_1_re118"
      uniprot "NA"
    ]
    graphics [
      x 646.846649363828
      y 305.8290189476413
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_247"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 187
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:obo.go:GO%3A0006914;urn:miriam:pubmed:28132811"
      hgnc "NA"
      map_id "M118_298"
      name "autophagy"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa152"
      uniprot "NA"
    ]
    graphics [
      x 693.2952551645681
      y 565.5670808546231
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_298"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 188
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:Q96TC7;urn:miriam:uniprot:Q96TC7;urn:miriam:refseq:NM_018145;urn:miriam:ncbigene:55177;urn:miriam:ncbigene:55177;urn:miriam:hgnc.symbol:RMDN3;urn:miriam:hgnc.symbol:RMDN3;urn:miriam:ensembl:ENSG00000137824;urn:miriam:hgnc:25550"
      hgnc "HGNC_SYMBOL:RMDN3"
      map_id "M118_292"
      name "RMDN3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa146"
      uniprot "UNIPROT:Q96TC7"
    ]
    graphics [
      x 485.87591475886825
      y 1087.5449318341455
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_292"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 189
    zlevel -1

    cd19dm [
      annotation "PUBMED:28132811"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_242"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "path_1_re111"
      uniprot "NA"
    ]
    graphics [
      x 420.7285441065188
      y 1325.8893951131695
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_242"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 190
    zlevel -1

    cd19dm [
      annotation "PUBMED:26108729"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_264"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "path_1_re153"
      uniprot "NA"
    ]
    graphics [
      x 929.7211300769304
      y 1858.0188118192482
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_264"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 191
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000151694;urn:miriam:ncbigene:6868;urn:miriam:ncbigene:6868;urn:miriam:refseq:NM_001382777;urn:miriam:ec-code:3.4.24.86;urn:miriam:hgnc:195;urn:miriam:uniprot:P78536;urn:miriam:uniprot:P78536;urn:miriam:pubmed:32264791;urn:miriam:hgnc.symbol:ADAM17;urn:miriam:hgnc.symbol:ADAM17"
      hgnc "HGNC_SYMBOL:ADAM17"
      map_id "M118_306"
      name "ADAM17"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa171"
      uniprot "UNIPROT:P78536"
    ]
    graphics [
      x 1079.9050509524302
      y 1680.8602356827569
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_306"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 192
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:9437;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc.symbol:EIF2AK2;urn:miriam:uniprot:P19525;urn:miriam:uniprot:P19525;urn:miriam:refseq:NM_002759;urn:miriam:ncbigene:5610;urn:miriam:ensembl:ENSG00000055332;urn:miriam:ncbigene:5610"
      hgnc "HGNC_SYMBOL:EIF2AK2"
      map_id "M118_153"
      name "EIF2AK2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa287"
      uniprot "UNIPROT:P19525"
    ]
    graphics [
      x 1188.9606948733478
      y 177.06658069304694
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_153"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 193
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:26587781;PUBMED:12601012"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_29"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re162"
      uniprot "NA"
    ]
    graphics [
      x 1047.3335883879772
      y 249.63464756921633
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 194
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0071359"
      hgnc "NA"
      map_id "M118_154"
      name "presence_space_of_space_dsRNA"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa288"
      uniprot "NA"
    ]
    graphics [
      x 1113.152372639735
      y 129.17001623508463
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_154"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 195
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000102580;urn:miriam:hgnc.symbol:DNAJC3;urn:miriam:hgnc:9439;urn:miriam:refseq:NM_006260;urn:miriam:uniprot:Q13217;urn:miriam:uniprot:Q13217;urn:miriam:ncbigene:5611;urn:miriam:ncbigene:5611"
      hgnc "HGNC_SYMBOL:DNAJC3"
      map_id "M118_210"
      name "DNAJC3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa624"
      uniprot "UNIPROT:Q13217"
    ]
    graphics [
      x 1155.6168734056298
      y 316.4553266628419
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_210"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 196
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:9437;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc.symbol:EIF2AK2;urn:miriam:uniprot:P19525;urn:miriam:uniprot:P19525;urn:miriam:refseq:NM_002759;urn:miriam:ncbigene:5610;urn:miriam:ensembl:ENSG00000055332;urn:miriam:ncbigene:5610"
      hgnc "HGNC_SYMBOL:EIF2AK2"
      map_id "M118_140"
      name "EIF2AK2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa210"
      uniprot "UNIPROT:P19525"
    ]
    graphics [
      x 914.0379265633067
      y 609.1288195305004
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_140"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 197
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:EIF2S1;urn:miriam:uniprot:P05198;urn:miriam:uniprot:P05198;urn:miriam:hgnc:3265;urn:miriam:ncbigene:1965;urn:miriam:ncbigene:1965;urn:miriam:ensembl:ENSG00000134001;urn:miriam:refseq:NM_004094"
      hgnc "HGNC_SYMBOL:EIF2S1"
      map_id "M118_222"
      name "EIF2S1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa87"
      uniprot "UNIPROT:P05198"
    ]
    graphics [
      x 705.1141183556945
      y 1140.792095865307
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_222"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 198
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:12667446;PUBMED:12601012"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_31"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_0_re166"
      uniprot "NA"
    ]
    graphics [
      x 937.1237948175362
      y 1186.2677587138144
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 199
    zlevel -1

    cd19dm [
      annotation "PUBMED:22511781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_250"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re122"
      uniprot "NA"
    ]
    graphics [
      x 875.8440582597243
      y 340.8951160261555
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_250"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 200
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:836;urn:miriam:refseq:NM_004346;urn:miriam:ncbigene:836;urn:miriam:ec-code:3.4.22.56;urn:miriam:ensembl:ENSG00000164305;urn:miriam:hgnc:1504;urn:miriam:uniprot:P42574;urn:miriam:uniprot:P42574;urn:miriam:hgnc.symbol:CASP3"
      hgnc "HGNC_SYMBOL:CASP3"
      map_id "M118_195"
      name "CASP3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa596"
      uniprot "UNIPROT:P42574"
    ]
    graphics [
      x 1039.4435220579762
      y 1584.8968179390408
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_195"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 201
    zlevel -1

    cd19dm [
      annotation "PUBMED:18955970;PUBMED:19931333;PUBMED:24373849"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_51"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re322"
      uniprot "NA"
    ]
    graphics [
      x 1211.0410453873726
      y 1576.4405655033943
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 202
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_59"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re333"
      uniprot "NA"
    ]
    graphics [
      x 1353.3712289978128
      y 819.88890198806
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 203
    zlevel -1

    cd19dm [
      annotation "PUBMED:15692567"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_258"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "path_1_re140"
      uniprot "NA"
    ]
    graphics [
      x 440.79952166432304
      y 829.5397012717322
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_258"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 204
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:17991856;PUBMED:26587781;PUBMED:18360008"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_87"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re44"
      uniprot "NA"
    ]
    graphics [
      x 831.9472517331637
      y 1904.0303097368626
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 205
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:15456;urn:miriam:refseq:NM_003791;urn:miriam:ec-code:3.4.21.112;urn:miriam:hgnc.symbol:MBTPS1;urn:miriam:ncbigene:8720;urn:miriam:ncbigene:8720;urn:miriam:ensembl:ENSG00000140943;urn:miriam:uniprot:Q14703;urn:miriam:uniprot:Q14703"
      hgnc "HGNC_SYMBOL:MBTPS1"
      map_id "M118_199"
      name "MBTPS1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa60"
      uniprot "UNIPROT:Q14703"
    ]
    graphics [
      x 842.1862223414913
      y 2059.3921537729384
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_199"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 206
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_97"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "path_0_re66"
      uniprot "NA"
    ]
    graphics [
      x 1440.8373736271963
      y 1246.9485532262452
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 207
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:786;urn:miriam:refseq:NM_001675;urn:miriam:ncbigene:468;urn:miriam:ncbigene:468;urn:miriam:hgnc.symbol:ATF4;urn:miriam:uniprot:P18848;urn:miriam:uniprot:P18848;urn:miriam:ensembl:ENSG00000128272"
      hgnc "HGNC_SYMBOL:ATF4"
      map_id "M118_111"
      name "ATF4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa104"
      uniprot "UNIPROT:P18848"
    ]
    graphics [
      x 1289.5428990669325
      y 1065.6211178102262
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 208
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000101255;urn:miriam:ncbigene:57761;urn:miriam:hgnc.symbol:TRIB3;urn:miriam:hgnc:16228;urn:miriam:refseq:NM_021158;urn:miriam:uniprot:Q96RU7"
      hgnc "HGNC_SYMBOL:TRIB3"
      map_id "M118_179"
      name "TRIB3"
      node_subtype "RNA"
      node_type "species"
      org_id "path_0_sa485"
      uniprot "UNIPROT:Q96RU7"
    ]
    graphics [
      x 588.1722653343435
      y 728.5798755832882
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_179"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 209
    zlevel -1

    cd19dm [
      annotation "PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_42"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "path_0_re253"
      uniprot "NA"
    ]
    graphics [
      x 359.93336108588085
      y 715.2211377803561
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 210
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000101255;urn:miriam:ncbigene:57761;urn:miriam:ncbigene:57761;urn:miriam:hgnc.symbol:TRIB3;urn:miriam:hgnc:16228;urn:miriam:refseq:NM_021158;urn:miriam:uniprot:Q96RU7;urn:miriam:uniprot:Q96RU7"
      hgnc "HGNC_SYMBOL:TRIB3"
      map_id "M118_180"
      name "TRIB3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa486"
      uniprot "UNIPROT:Q96RU7"
    ]
    graphics [
      x 237.97265021956207
      y 805.306509326781
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_180"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 211
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ec-code:3.4.22.57;urn:miriam:ensembl:ENSG00000196954;urn:miriam:ncbigene:837;urn:miriam:ncbigene:837;urn:miriam:hgnc:1505;urn:miriam:refseq:NM_001225;urn:miriam:hgnc.symbol:CASP4;urn:miriam:uniprot:P49662;urn:miriam:uniprot:P49662"
      hgnc "HGNC_SYMBOL:CASP4"
      map_id "M118_193"
      name "CASP4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa594"
      uniprot "UNIPROT:P49662"
    ]
    graphics [
      x 1211.9416168118166
      y 1439.7131748468344
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_193"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 212
    zlevel -1

    cd19dm [
      annotation "PUBMED:18955970;PUBMED:19931333;PUBMED:24373849"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_50"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re321"
      uniprot "NA"
    ]
    graphics [
      x 1377.6583149443238
      y 1538.499953188404
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 213
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:9474;urn:miriam:ensembl:ENSG00000057663;urn:miriam:ncbigene:9474;urn:miriam:hgnc:589;urn:miriam:uniprot:Q9H1Y0;urn:miriam:uniprot:Q9H1Y0;urn:miriam:hgnc.symbol:ATG5;urn:miriam:refseq:NM_004849;urn:miriam:hgnc:3573;urn:miriam:ncbigene:8772;urn:miriam:ncbigene:8772;urn:miriam:uniprot:Q13158;urn:miriam:uniprot:Q13158;urn:miriam:ensembl:ENSG00000168040;urn:miriam:refseq:NM_003824;urn:miriam:hgnc.symbol:FADD;urn:miriam:hgnc.symbol:FADD;urn:miriam:refseq:NM_181509;urn:miriam:ncbigene:84557;urn:miriam:ncbigene:84557;urn:miriam:hgnc.symbol:MAP1LC3A;urn:miriam:ensembl:ENSG00000101460;urn:miriam:hgnc:6838;urn:miriam:uniprot:Q9H492;urn:miriam:uniprot:Q9H492;urn:miriam:hgnc:1509;urn:miriam:hgnc.symbol:CASP8;urn:miriam:uniprot:Q14790;urn:miriam:uniprot:Q14790;urn:miriam:hgnc.symbol:CASP8;urn:miriam:ncbigene:841;urn:miriam:ncbigene:841;urn:miriam:ec-code:3.4.22.61;urn:miriam:refseq:NM_001228;urn:miriam:ensembl:ENSG00000064012"
      hgnc "HGNC_SYMBOL:ATG5;HGNC_SYMBOL:FADD;HGNC_SYMBOL:MAP1LC3A;HGNC_SYMBOL:CASP8"
      map_id "M118_10"
      name "CASP8:FADD:MAP1LC3A:SQSTM1:ATG5"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa26"
      uniprot "UNIPROT:Q9H1Y0;UNIPROT:Q13158;UNIPROT:Q9H492;UNIPROT:Q14790"
    ]
    graphics [
      x 1657.0995100639993
      y 644.0705801459644
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 214
    zlevel -1

    cd19dm [
      annotation "PUBMED:26587781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_66"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re350"
      uniprot "NA"
    ]
    graphics [
      x 1677.5176840701217
      y 800.0172464953196
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 215
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_014874;urn:miriam:hgnc.symbol:MFN2;urn:miriam:ensembl:ENSG00000116688;urn:miriam:uniprot:O95140;urn:miriam:ncbigene:9927;urn:miriam:hgnc:16877"
      hgnc "HGNC_SYMBOL:MFN2"
      map_id "M118_294"
      name "MFN2"
      node_subtype "RNA"
      node_type "species"
      org_id "path_1_sa148"
      uniprot "UNIPROT:O95140"
    ]
    graphics [
      x 1155.2001484872026
      y 650.0482797609384
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_294"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 216
    zlevel -1

    cd19dm [
      annotation "PUBMED:22511781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_239"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "path_1_re108"
      uniprot "NA"
    ]
    graphics [
      x 831.4088712492485
      y 579.8468136191321
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_239"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 217
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:26587781;PUBMED:12667446;PUBMED:12601012;PUBMED:18360008"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_92"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re61"
      uniprot "NA"
    ]
    graphics [
      x 734.2620412319455
      y 1041.138635862444
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 218
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15422;urn:miriam:hgnc:19687;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:EIF2AK4;urn:miriam:refseq:NM_001013703;urn:miriam:hgnc.symbol:EIF2AK4;urn:miriam:ncbigene:440275;urn:miriam:ncbigene:440275;urn:miriam:ensembl:ENSG00000128829;urn:miriam:uniprot:Q9P2K8;urn:miriam:uniprot:Q9P2K8"
      hgnc "HGNC_SYMBOL:EIF2AK4"
      map_id "M118_15"
      name "GCN2:ATP"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa35"
      uniprot "UNIPROT:Q9P2K8"
    ]
    graphics [
      x 651.3044484383074
      y 957.48961977348
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 219
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:17991856;PUBMED:26587781;PUBMED:18360008"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_89"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "path_0_re46"
      uniprot "NA"
    ]
    graphics [
      x 955.363812323774
      y 967.5036774725929
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 220
    zlevel -1

    cd19dm [
      annotation "PUBMED:30590907;PUBMED:29491369;PUBMED:21183955"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_272"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re48"
      uniprot "NA"
    ]
    graphics [
      x 759.1120612857952
      y 401.10908097269635
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_272"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 221
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_102"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "path_0_re77"
      uniprot "NA"
    ]
    graphics [
      x 579.249446686635
      y 790.7891613079246
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 222
    zlevel -1

    cd19dm [
      annotation "PUBMED:19052620"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_277"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re57"
      uniprot "NA"
    ]
    graphics [
      x 698.1284177984742
      y 325.8450829497202
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_277"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 223
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:19052620;urn:miriam:obo.go:GO%3A0007029"
      hgnc "NA"
      map_id "M118_284"
      name "endoplasmic_space_reticulum_space_organization"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa109"
      uniprot "NA"
    ]
    graphics [
      x 839.4354956632083
      y 261.7316593088816
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_284"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 224
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:DDIT3;urn:miriam:ncbigene:1649;urn:miriam:ncbigene:1649;urn:miriam:uniprot:P35638;urn:miriam:uniprot:P35638;urn:miriam:hgnc:2726;urn:miriam:refseq:NM_004083;urn:miriam:ensembl:ENSG00000175197;urn:miriam:uniprot:P0DPQ6;urn:miriam:uniprot:P0DPQ6"
      hgnc "HGNC_SYMBOL:DDIT3"
      map_id "M118_215"
      name "DDIT3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa630"
      uniprot "UNIPROT:P35638;UNIPROT:P0DPQ6"
    ]
    graphics [
      x 1231.612698781191
      y 1036.846302568198
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_215"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 225
    zlevel -1

    cd19dm [
      annotation "PUBMED:30662442;PUBMED:23850759;PUBMED:26587781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_60"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re334"
      uniprot "NA"
    ]
    graphics [
      x 1427.4381297300592
      y 1031.9122961220494
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 226
    zlevel -1

    cd19dm [
      annotation "PUBMED:9606;PUBMED:21183955"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_254"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re128"
      uniprot "NA"
    ]
    graphics [
      x 279.0436393346338
      y 1257.2078761638686
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_254"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 227
    zlevel -1

    cd19dm [
      annotation "PUBMED:22511781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_281"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re70"
      uniprot "NA"
    ]
    graphics [
      x 620.3360583808038
      y 610.5875258105899
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_281"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 228
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0006986"
      hgnc "NA"
      map_id "M118_158"
      name "accumulation_space_of_space_misfolded_space_protein_space_in_space_ER"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa336"
      uniprot "NA"
    ]
    graphics [
      x 934.2159468631869
      y 1948.4920968889746
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_158"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 229
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_70"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re356"
      uniprot "NA"
    ]
    graphics [
      x 878.2109197651508
      y 1731.3110898569553
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 230
    zlevel -1

    cd19dm [
      annotation "PUBMED:15692567"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_260"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re144"
      uniprot "NA"
    ]
    graphics [
      x 259.84608934944606
      y 745.8892897832211
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_260"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 231
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0036503"
      hgnc "NA"
      map_id "M118_160"
      name "ERAD"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa37"
      uniprot "NA"
    ]
    graphics [
      x 514.2151623929016
      y 1949.8505907334986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_160"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 232
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_81"
      name "NA"
      node_subtype "MODULATION"
      node_type "reaction"
      org_id "path_0_re371"
      uniprot "NA"
    ]
    graphics [
      x 725.2506246572302
      y 2045.9972810716445
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 233
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_014874;urn:miriam:hgnc.symbol:MFN2;urn:miriam:ensembl:ENSG00000116688;urn:miriam:uniprot:O95140;urn:miriam:ncbigene:9927;urn:miriam:hgnc:16877"
      hgnc "HGNC_SYMBOL:MFN2"
      map_id "M118_288"
      name "MFN2"
      node_subtype "GENE"
      node_type "species"
      org_id "path_1_sa122"
      uniprot "UNIPROT:O95140"
    ]
    graphics [
      x 1653.623509997361
      y 844.2389740696278
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_288"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 234
    zlevel -1

    cd19dm [
      annotation "PUBMED:22511781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_240"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "path_1_re109"
      uniprot "NA"
    ]
    graphics [
      x 1434.1198555790952
      y 765.9704011926281
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_240"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 235
    zlevel -1

    cd19dm [
      annotation "PUBMED:31775868;PUBMED:30604460"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_237"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "path_1_re100"
      uniprot "NA"
    ]
    graphics [
      x 999.5344489007748
      y 896.3144955340773
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_237"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 236
    zlevel -1

    cd19dm [
      annotation "PUBMED:14647384"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_341"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re145"
      uniprot "NA"
    ]
    graphics [
      x 1167.556304991859
      y 858.7943747277376
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_341"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 237
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:14647384;urn:miriam:obo.go:GO%3A0039694"
      hgnc "NA"
      map_id "M118_351"
      name "viral_space_RNA_space_genome_space_replication"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa280"
      uniprot "NA"
    ]
    graphics [
      x 1097.185044912314
      y 752.1622829561309
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_351"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 238
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_74"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re360"
      uniprot "NA"
    ]
    graphics [
      x 1536.7079656526375
      y 1285.640929267017
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 239
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:23526;urn:miriam:taxonomy:9606;urn:miriam:uniprot:Q8NE86;urn:miriam:uniprot:Q8NE86;urn:miriam:hgnc.symbol:MCU;urn:miriam:refseq:NM_138357;urn:miriam:hgnc.symbol:MCU;urn:miriam:ensembl:ENSG00000156026;urn:miriam:pubmed:24231807;urn:miriam:ncbigene:90550;urn:miriam:ncbigene:90550"
      hgnc "HGNC_SYMBOL:MCU"
      map_id "M118_315"
      name "MCU"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa34"
      uniprot "UNIPROT:Q8NE86"
    ]
    graphics [
      x 1860.360644582627
      y 951.5017972330977
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_315"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 240
    zlevel -1

    cd19dm [
      annotation "PUBMED:24231807"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_270"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_1_re46"
      uniprot "NA"
    ]
    graphics [
      x 1947.2047081877067
      y 991.6032647873369
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_270"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 241
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000183172;urn:miriam:pubmed:24231807;urn:miriam:ncbigene:91689;urn:miriam:uniprot:Q9H4I9;urn:miriam:uniprot:Q9H4I9;urn:miriam:ncbigene:91689;urn:miriam:hgnc.symbol:SMDT1;urn:miriam:hgnc.symbol:SMDT1;urn:miriam:refseq:NM_033318;urn:miriam:hgnc:25055"
      hgnc "HGNC_SYMBOL:SMDT1"
      map_id "M118_316"
      name "SMDT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa35"
      uniprot "UNIPROT:Q9H4I9"
    ]
    graphics [
      x 1814.6258432957575
      y 1113.893852587726
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_316"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 242
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:24231807;urn:miriam:ensembl:ENSG00000107745;urn:miriam:uniprot:Q9BPX6;urn:miriam:uniprot:Q9BPX6;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1530;urn:miriam:pubmed:24231807;urn:miriam:hgnc.symbol:MICU1;urn:miriam:hgnc.symbol:MICU1;urn:miriam:refseq:NM_006077;urn:miriam:ncbigene:10367;urn:miriam:ncbigene:10367;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000183172;urn:miriam:pubmed:24231807;urn:miriam:ncbigene:91689;urn:miriam:uniprot:Q9H4I9;urn:miriam:uniprot:Q9H4I9;urn:miriam:ncbigene:91689;urn:miriam:hgnc.symbol:SMDT1;urn:miriam:hgnc.symbol:SMDT1;urn:miriam:refseq:NM_033318;urn:miriam:hgnc:25055;urn:miriam:hgnc:23526;urn:miriam:taxonomy:9606;urn:miriam:uniprot:Q8NE86;urn:miriam:uniprot:Q8NE86;urn:miriam:hgnc.symbol:MCU;urn:miriam:refseq:NM_138357;urn:miriam:hgnc.symbol:MCU;urn:miriam:ensembl:ENSG00000156026;urn:miriam:pubmed:24231807;urn:miriam:ncbigene:90550;urn:miriam:ncbigene:90550;urn:miriam:hgnc:31830;urn:miriam:ensembl:ENSG00000165487;urn:miriam:refseq:NM_152726;urn:miriam:taxonomy:9606;urn:miriam:pubmed:24231807;urn:miriam:hgnc.symbol:MICU2;urn:miriam:hgnc.symbol:MICU2;urn:miriam:ncbigene:221154;urn:miriam:ncbigene:221154;urn:miriam:uniprot:Q8IYU8;urn:miriam:uniprot:Q8IYU8"
      hgnc "HGNC_SYMBOL:MICU1;HGNC_SYMBOL:SMDT1;HGNC_SYMBOL:MCU;HGNC_SYMBOL:MICU2"
      map_id "M118_230"
      name "MCU:MICU1:MICU2:SMDT1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_1_csa16"
      uniprot "UNIPROT:Q9BPX6;UNIPROT:Q9H4I9;UNIPROT:Q8NE86;UNIPROT:Q8IYU8"
    ]
    graphics [
      x 2146.612232096726
      y 927.7567643121126
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_230"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 243
    zlevel -1

    cd19dm [
      annotation "PUBMED:30590907;PUBMED:29491369;PUBMED:19052620"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_271"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re47"
      uniprot "NA"
    ]
    graphics [
      x 414.32182898688643
      y 101.04626834793248
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_271"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 244
    zlevel -1

    cd19dm [
      annotation "PUBMED:15692567"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_251"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re123"
      uniprot "NA"
    ]
    graphics [
      x 550.7430190891995
      y 302.4611976427842
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_251"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 245
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29108"
      hgnc "NA"
      map_id "M118_320"
      name "Ca2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "path_1_sa63"
      uniprot "NA"
    ]
    graphics [
      x 2220.6255454474294
      y 1385.0266207520606
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_320"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 246
    zlevel -1

    cd19dm [
      annotation "PUBMED:24231807"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_266"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "path_1_re25"
      uniprot "NA"
    ]
    graphics [
      x 2100.844992710934
      y 1410.6792886589778
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_266"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 247
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:26968367;urn:miriam:ensembl:ENSG00000107745;urn:miriam:uniprot:Q9BPX6;urn:miriam:uniprot:Q9BPX6;urn:miriam:hgnc:1530;urn:miriam:hgnc.symbol:MICU1;urn:miriam:hgnc.symbol:MICU1;urn:miriam:refseq:NM_006077;urn:miriam:ncbigene:10367;urn:miriam:ncbigene:10367;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000183172;urn:miriam:pubmed:24231807;urn:miriam:ncbigene:91689;urn:miriam:uniprot:Q9H4I9;urn:miriam:uniprot:Q9H4I9;urn:miriam:ncbigene:91689;urn:miriam:hgnc.symbol:SMDT1;urn:miriam:hgnc.symbol:SMDT1;urn:miriam:refseq:NM_033318;urn:miriam:hgnc:25055;urn:miriam:hgnc:26076;urn:miriam:hgnc.symbol:MCUB;urn:miriam:hgnc.symbol:MCUB;urn:miriam:ensembl:ENSG00000005059;urn:miriam:refseq:NM_017918;urn:miriam:ncbigene:55013;urn:miriam:ncbigene:55013;urn:miriam:uniprot:Q9NWR8;urn:miriam:uniprot:Q9NWR8;urn:miriam:hgnc:31830;urn:miriam:ensembl:ENSG00000165487;urn:miriam:refseq:NM_152726;urn:miriam:hgnc.symbol:MICU2;urn:miriam:hgnc.symbol:MICU2;urn:miriam:ncbigene:221154;urn:miriam:ncbigene:221154;urn:miriam:uniprot:Q8IYU8;urn:miriam:uniprot:Q8IYU8;urn:miriam:hgnc:23526;urn:miriam:taxonomy:9606;urn:miriam:uniprot:Q8NE86;urn:miriam:uniprot:Q8NE86;urn:miriam:hgnc.symbol:MCU;urn:miriam:refseq:NM_138357;urn:miriam:hgnc.symbol:MCU;urn:miriam:ensembl:ENSG00000156026;urn:miriam:pubmed:24231807;urn:miriam:ncbigene:90550;urn:miriam:ncbigene:90550"
      hgnc "HGNC_SYMBOL:MICU1;HGNC_SYMBOL:SMDT1;HGNC_SYMBOL:MCUB;HGNC_SYMBOL:MICU2;HGNC_SYMBOL:MCU"
      map_id "M118_236"
      name "Mitochondrial_space_calcium_space_uniporter_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_1_csa6"
      uniprot "UNIPROT:Q9BPX6;UNIPROT:Q9H4I9;UNIPROT:Q9NWR8;UNIPROT:Q8IYU8;UNIPROT:Q8NE86"
    ]
    graphics [
      x 2103.3845151593528
      y 1503.1478098500615
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_236"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 248
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:24231807;urn:miriam:obo.chebi:CHEBI%3A29108"
      hgnc "NA"
      map_id "M118_321"
      name "Ca2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "path_1_sa64"
      uniprot "NA"
    ]
    graphics [
      x 1960.6920663641122
      y 1354.4934615837496
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_321"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 249
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000130234;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:ACE2"
      map_id "M118_348"
      name "ACE2"
      node_subtype "GENE"
      node_type "species"
      org_id "sa277"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 2027.2892877442973
      y 1685.4288704705596
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_348"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 250
    zlevel -1

    cd19dm [
      annotation "PUBMED:14647384;PUBMED:19411314"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_340"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re144"
      uniprot "NA"
    ]
    graphics [
      x 2089.7684357375165
      y 1610.990159332645
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_340"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 251
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000130234;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:ACE2"
      map_id "M118_349"
      name "ACE2"
      node_subtype "RNA"
      node_type "species"
      org_id "sa278"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1881.8481775676498
      y 1483.7663709249225
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_349"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 252
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:12667446;PUBMED:12601012"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_103"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re80"
      uniprot "NA"
    ]
    graphics [
      x 431.1604848433807
      y 1073.187934276114
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 253
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:Q9BQI3;urn:miriam:uniprot:Q9BQI3;urn:miriam:refseq:NM_014413;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:EIF2AK1;urn:miriam:hgnc:24921;urn:miriam:ncbigene:27102;urn:miriam:ncbigene:27102;urn:miriam:ensembl:ENSG00000086232"
      hgnc "HGNC_SYMBOL:EIF2AK1"
      map_id "M118_155"
      name "EIF2AK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa290"
      uniprot "UNIPROT:Q9BQI3"
    ]
    graphics [
      x 295.2569712701086
      y 1116.6230878381853
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_155"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 254
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:Q99683;urn:miriam:uniprot:Q99683;urn:miriam:ncbigene:4217;urn:miriam:ncbigene:4217;urn:miriam:hgnc:6857;urn:miriam:refseq:NM_005923;urn:miriam:hgnc.symbol:MAP3K5;urn:miriam:ensembl:ENSG00000197442;urn:miriam:ec-code:2.7.11.25"
      hgnc "HGNC_SYMBOL:MAP3K5"
      map_id "M118_161"
      name "MAP3K5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa38"
      uniprot "UNIPROT:Q99683"
    ]
    graphics [
      x 1766.4447103966777
      y 663.1724322705298
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_161"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 255
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_100"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re75"
      uniprot "NA"
    ]
    graphics [
      x 1655.9924760897095
      y 720.3390556364286
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 256
    zlevel -1

    cd19dm [
      annotation "PUBMED:28132811"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_246"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "path_1_re117"
      uniprot "NA"
    ]
    graphics [
      x 591.8817507441589
      y 1294.8286305009417
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_246"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 257
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:28132811;urn:miriam:obo.go:GO%3A0007204"
      hgnc "NA"
      map_id "M118_297"
      name "Ca2_plus__space_cytosolic_space_concentration"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa151"
      uniprot "NA"
    ]
    graphics [
      x 546.7778755626483
      y 1238.093829255412
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_297"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 258
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_001291958;urn:miriam:hgnc:6856;urn:miriam:hgnc.symbol:MAP3K4;urn:miriam:uniprot:Q9Y6R4;urn:miriam:uniprot:Q9Y6R4;urn:miriam:ncbigene:4216;urn:miriam:ncbigene:4216;urn:miriam:ensembl:ENSG00000085511;urn:miriam:ec-code:2.7.11.25"
      hgnc "HGNC_SYMBOL:MAP3K4"
      map_id "M118_126"
      name "MAP3K4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa144"
      uniprot "UNIPROT:Q9Y6R4"
    ]
    graphics [
      x 1631.672881510859
      y 1310.1821890978947
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 259
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_105"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re93"
      uniprot "NA"
    ]
    graphics [
      x 1687.780534575208
      y 1365.678538244976
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 260
    zlevel -1

    cd19dm [
      annotation "PUBMED:25387528;PUBMED:23027870"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_79"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re366"
      uniprot "NA"
    ]
    graphics [
      x 519.7596097955808
      y 1473.0043427063276
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 261
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0018158"
      hgnc "NA"
      map_id "M118_141"
      name "hyperoxidation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa211"
      uniprot "NA"
    ]
    graphics [
      x 656.8675386027903
      y 1397.548857961358
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_141"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 262
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:DDIT3;urn:miriam:ncbigene:1649;urn:miriam:uniprot:P35638;urn:miriam:hgnc:2726;urn:miriam:refseq:NM_004083;urn:miriam:ensembl:ENSG00000175197;urn:miriam:uniprot:P0DPQ6"
      hgnc "HGNC_SYMBOL:DDIT3"
      map_id "M118_220"
      name "DDIT3"
      node_subtype "GENE"
      node_type "species"
      org_id "path_0_sa70"
      uniprot "UNIPROT:P35638;UNIPROT:P0DPQ6"
    ]
    graphics [
      x 1376.9930636687977
      y 468.62520992498924
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_220"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 263
    zlevel -1

    cd19dm [
      annotation "PUBMED:22802018;PUBMED:17991856;PUBMED:26587781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_91"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "path_0_re50"
      uniprot "NA"
    ]
    graphics [
      x 1422.0512382810389
      y 614.2154567493626
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 264
    zlevel -1

    cd19dm [
      annotation "PUBMED:17991856;PUBMED:26587781;PUBMED:18360008"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_64"
      name "NA"
      node_subtype "MODULATION"
      node_type "reaction"
      org_id "path_0_re344"
      uniprot "NA"
    ]
    graphics [
      x 1475.7361614231845
      y 1885.9333461790563
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 265
    zlevel -1

    cd19dm [
      annotation "PUBMED:32169481;PUBMED:32340551"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_331"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re135"
      uniprot "NA"
    ]
    graphics [
      x 2024.365961596765
      y 895.6125546537016
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_331"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 266
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:HSPA5;urn:miriam:ensembl:ENSG00000044574;urn:miriam:hgnc.symbol:HSPA5;urn:miriam:pubmed:32169481;urn:miriam:hgnc:5238;urn:miriam:pubmed:30978349;urn:miriam:refseq:NM_005347;urn:miriam:ncbigene:3309;urn:miriam:ncbigene:3309;urn:miriam:uniprot:P11021;urn:miriam:uniprot:P11021;urn:miriam:pubmed:32340551;urn:miriam:ec-code:3.6.4.10"
      hgnc "HGNC_SYMBOL:HSPA5"
      map_id "M118_344"
      name "HSPA5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa262"
      uniprot "UNIPROT:P11021"
    ]
    graphics [
      x 1815.2616294656636
      y 777.7055123352907
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_344"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 267
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:HSPA5;urn:miriam:ensembl:ENSG00000044574;urn:miriam:refseq:NM_005347;urn:miriam:hgnc:5238;urn:miriam:ncbigene:3309;urn:miriam:uniprot:P11021"
      hgnc "HGNC_SYMBOL:HSPA5"
      map_id "M118_295"
      name "HSPA5"
      node_subtype "RNA"
      node_type "species"
      org_id "path_1_sa149"
      uniprot "UNIPROT:P11021"
    ]
    graphics [
      x 1085.9098273074383
      y 954.6468482083329
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_295"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 268
    zlevel -1

    cd19dm [
      annotation "PUBMED:31775868;PUBMED:22511781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_257"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_1_re137"
      uniprot "NA"
    ]
    graphics [
      x 1010.8645418394035
      y 758.6476713515519
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_257"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 269
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_006389;urn:miriam:hgnc:16931;urn:miriam:ncbigene:10525;urn:miriam:ensembl:ENSG00000149428;urn:miriam:uniprot:Q9Y4L1;urn:miriam:hgnc.symbol:HYOU1"
      hgnc "HGNC_SYMBOL:HYOU1"
      map_id "M118_203"
      name "HYOU1"
      node_subtype "GENE"
      node_type "species"
      org_id "path_0_sa605"
      uniprot "UNIPROT:Q9Y4L1"
    ]
    graphics [
      x 1605.8198030282276
      y 1159.8885162896838
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_203"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 270
    zlevel -1

    cd19dm [
      annotation "PUBMED:18360008"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_54"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "path_0_re327"
      uniprot "NA"
    ]
    graphics [
      x 1510.7853105209574
      y 1080.8587069558264
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 271
    zlevel -1

    cd19dm [
      annotation "PUBMED:28132811"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_245"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "path_1_re116"
      uniprot "NA"
    ]
    graphics [
      x 357.4810821932123
      y 1146.256455583123
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_245"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 272
    zlevel -1

    cd19dm [
      annotation "PUBMED:28132811"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_244"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "path_1_re114"
      uniprot "NA"
    ]
    graphics [
      x 538.7290539249843
      y 831.4030452322215
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_244"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 273
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:DDIT3;urn:miriam:ncbigene:1649;urn:miriam:ncbigene:1649;urn:miriam:uniprot:P35638;urn:miriam:uniprot:P35638;urn:miriam:hgnc:2726;urn:miriam:refseq:NM_004083;urn:miriam:ensembl:ENSG00000175197;urn:miriam:uniprot:P0DPQ6;urn:miriam:uniprot:P0DPQ6"
      hgnc "HGNC_SYMBOL:DDIT3"
      map_id "M118_214"
      name "DDIT3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa629"
      uniprot "UNIPROT:P35638;UNIPROT:P0DPQ6"
    ]
    graphics [
      x 1222.1599321942776
      y 892.0727298486347
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_214"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 274
    zlevel -1

    cd19dm [
      annotation "PUBMED:26137585;PUBMED:23850759;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_84"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re374"
      uniprot "NA"
    ]
    graphics [
      x 1107.3882376119518
      y 851.365818081019
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 275
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:1509;urn:miriam:hgnc.symbol:CASP8;urn:miriam:uniprot:Q14790;urn:miriam:uniprot:Q14790;urn:miriam:hgnc.symbol:CASP8;urn:miriam:ncbigene:841;urn:miriam:ncbigene:841;urn:miriam:ec-code:3.4.22.61;urn:miriam:refseq:NM_001228;urn:miriam:ensembl:ENSG00000064012;urn:miriam:hgnc:3573;urn:miriam:ncbigene:8772;urn:miriam:ncbigene:8772;urn:miriam:uniprot:Q13158;urn:miriam:uniprot:Q13158;urn:miriam:ensembl:ENSG00000168040;urn:miriam:refseq:NM_003824;urn:miriam:hgnc.symbol:FADD;urn:miriam:hgnc.symbol:FADD"
      hgnc "HGNC_SYMBOL:CASP8;HGNC_SYMBOL:FADD"
      map_id "M118_9"
      name "CASP8:CASP8_minus_ubq:FADD"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa25"
      uniprot "UNIPROT:Q14790;UNIPROT:Q13158"
    ]
    graphics [
      x 1590.0886803536691
      y 689.8329973371074
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 276
    zlevel -1

    cd19dm [
      annotation "PUBMED:17991856"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_83"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re373"
      uniprot "NA"
    ]
    graphics [
      x 1475.941041203293
      y 678.5690187270104
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 277
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:11280;urn:miriam:ensembl:ENSG00000161011;urn:miriam:uniprot:Q13501;urn:miriam:uniprot:Q13501;urn:miriam:hgnc.symbol:SQSTM1;urn:miriam:ncbigene:8878;urn:miriam:refseq:NM_001142298;urn:miriam:ncbigene:8878"
      hgnc "HGNC_SYMBOL:SQSTM1"
      map_id "M118_144"
      name "SQSTM1_space_"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa238"
      uniprot "UNIPROT:Q13501"
    ]
    graphics [
      x 1566.3049559447386
      y 595.9029345355166
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_144"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 278
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_181509;urn:miriam:ncbigene:84557;urn:miriam:ncbigene:84557;urn:miriam:hgnc.symbol:MAP1LC3A;urn:miriam:ensembl:ENSG00000101460;urn:miriam:hgnc:6838;urn:miriam:uniprot:Q9H492;urn:miriam:uniprot:Q9H492"
      hgnc "HGNC_SYMBOL:MAP1LC3A"
      map_id "M118_145"
      name "MAP1LC3A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa239"
      uniprot "UNIPROT:Q9H492"
    ]
    graphics [
      x 1384.5733519669368
      y 739.2754638445226
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_145"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 279
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:9474;urn:miriam:ensembl:ENSG00000057663;urn:miriam:ncbigene:9474;urn:miriam:hgnc:589;urn:miriam:uniprot:Q9H1Y0;urn:miriam:uniprot:Q9H1Y0;urn:miriam:hgnc.symbol:ATG5;urn:miriam:refseq:NM_004849"
      hgnc "HGNC_SYMBOL:ATG5"
      map_id "M118_146"
      name "ATG5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa241"
      uniprot "UNIPROT:Q9H1Y0"
    ]
    graphics [
      x 1342.285017456204
      y 699.5097981630292
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_146"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 280
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:9976;urn:miriam:ensembl:ENSG00000204977;urn:miriam:ec-code:2.3.2.27;urn:miriam:uniprot:O60858;urn:miriam:uniprot:O60858;urn:miriam:ncbigene:10206;urn:miriam:ncbigene:10206;urn:miriam:hgnc.symbol:TRIM13;urn:miriam:refseq:NM_001007278"
      hgnc "HGNC_SYMBOL:TRIM13"
      map_id "M118_147"
      name "TRIM13"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa254"
      uniprot "UNIPROT:O60858"
    ]
    graphics [
      x 1522.0612144847687
      y 768.380053880739
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_147"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 281
    zlevel -1

    cd19dm [
      annotation "PUBMED:28132811"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_243"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "path_1_re113"
      uniprot "NA"
    ]
    graphics [
      x 609.253128125633
      y 828.445133116396
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_243"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 282
    zlevel -1

    cd19dm [
      annotation "PUBMED:28132811"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_238"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_1_re103"
      uniprot "NA"
    ]
    graphics [
      x 377.1318613554813
      y 899.7968584001154
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_238"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 283
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:26587781;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_38"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_0_re244"
      uniprot "NA"
    ]
    graphics [
      x 1901.6711219557574
      y 376.6926870652404
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 284
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_001204106;urn:miriam:hgnc:994;urn:miriam:hgnc.symbol:BCL2L11;urn:miriam:ncbigene:10018;urn:miriam:ensembl:ENSG00000153094;urn:miriam:ncbigene:10018;urn:miriam:uniprot:O43521;urn:miriam:uniprot:O43521"
      hgnc "HGNC_SYMBOL:BCL2L11"
      map_id "M118_176"
      name "BCL2L11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa475"
      uniprot "UNIPROT:O43521"
    ]
    graphics [
      x 1864.826076669076
      y 276.7683804944762
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_176"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 285
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_95"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re64"
      uniprot "NA"
    ]
    graphics [
      x 1130.35274867965
      y 2078.4362719993082
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 286
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A65180"
      hgnc "NA"
      map_id "M118_110"
      name "GDP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "path_0_sa103"
      uniprot "NA"
    ]
    graphics [
      x 1164.1647633617147
      y 2197.0025602987407
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 287
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57600"
      hgnc "NA"
      map_id "M118_225"
      name "GTP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "path_0_sa95"
      uniprot "NA"
    ]
    graphics [
      x 1248.563920905096
      y 2120.005197098316
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_225"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 288
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:ACE2"
      map_id "M118_309"
      name "ACE2,_space_membrane_minus_bound"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa178"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1360.7503381664492
      y 1669.554145897916
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_309"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 289
    zlevel -1

    cd19dm [
      annotation "PUBMED:19411314;PUBMED:15983030"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_336"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re140"
      uniprot "NA"
    ]
    graphics [
      x 1537.3820388045783
      y 1631.2965543978237
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_336"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 290
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:uniprot:W6A028;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "M118_347"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa275"
      uniprot "UNIPROT:P0DTC2;UNIPROT:W6A028;UNIPROT:P59594"
    ]
    graphics [
      x 1355.3834682822674
      y 1413.248353316184
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_347"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 291
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:ACE2"
      map_id "M118_310"
      name "ACE2,_space_soluble"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa180"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1517.9557568487041
      y 1742.570229993472
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_310"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 292
    zlevel -1

    cd19dm [
      annotation "PUBMED:26137585;PUBMED:23850759;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_49"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re32"
      uniprot "NA"
    ]
    graphics [
      x 1405.7695116158484
      y 1329.8610013371697
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 293
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ec-code:2.7.11.24;urn:miriam:ensembl:ENSG00000112062;urn:miriam:hgnc:6876;urn:miriam:uniprot:Q16539;urn:miriam:uniprot:Q16539;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:ncbigene:1432;urn:miriam:refseq:NM_001315"
      hgnc "HGNC_SYMBOL:MAPK14"
      map_id "M118_209"
      name "MAPK14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa620"
      uniprot "UNIPROT:Q16539"
    ]
    graphics [
      x 1408.6562721738626
      y 1453.3219028916217
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_209"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 294
    zlevel -1

    cd19dm [
      annotation "PUBMED:24231807"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_275"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re51"
      uniprot "NA"
    ]
    graphics [
      x 1985.8784240746045
      y 1743.466659238545
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_275"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 295
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:24231807;urn:miriam:obo.go:GO%3A0006851"
      hgnc "NA"
      map_id "M118_312"
      name "mitochondrial_space_calcium_space_ion_space_transmembrane_space_transport"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa31"
      uniprot "NA"
    ]
    graphics [
      x 1943.2064405046567
      y 1895.1441750118659
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_312"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 296
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:PPP1R15A;urn:miriam:hgnc:14375;urn:miriam:refseq:NM_014330;urn:miriam:ensembl:ENSG00000087074;urn:miriam:uniprot:O75807;urn:miriam:ncbigene:23645"
      hgnc "HGNC_SYMBOL:PPP1R15A"
      map_id "M118_116"
      name "PPP1R15A"
      node_subtype "GENE"
      node_type "species"
      org_id "path_0_sa109"
      uniprot "UNIPROT:O75807"
    ]
    graphics [
      x 930.0190789194467
      y 1361.0240670129751
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 297
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:26584763"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_98"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "path_0_re67"
      uniprot "NA"
    ]
    graphics [
      x 1005.3735927955819
      y 1253.5709311373355
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 298
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_183"
      name "Unfolded_space_protein"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa5"
      uniprot "NA"
    ]
    graphics [
      x 858.2622603289221
      y 1577.8711312238063
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_183"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 299
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_68"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re353"
      uniprot "NA"
    ]
    graphics [
      x 854.1412977855814
      y 1807.5294181486647
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 300
    zlevel -1

    cd19dm [
      annotation "PUBMED:19411314;PUBMED:15983030;PUBMED:32264791"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_334"
      name "NA"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "re138"
      uniprot "NA"
    ]
    graphics [
      x 1291.708416329042
      y 1504.9146906885335
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_334"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 301
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:31830;urn:miriam:ensembl:ENSG00000165487;urn:miriam:refseq:NM_152726;urn:miriam:taxonomy:9606;urn:miriam:pubmed:24231807;urn:miriam:hgnc.symbol:MICU2;urn:miriam:hgnc.symbol:MICU2;urn:miriam:ncbigene:221154;urn:miriam:ncbigene:221154;urn:miriam:uniprot:Q8IYU8;urn:miriam:uniprot:Q8IYU8"
      hgnc "HGNC_SYMBOL:MICU2"
      map_id "M118_314"
      name "MCU2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa33"
      uniprot "UNIPROT:Q8IYU8"
    ]
    graphics [
      x 2036.684195301009
      y 719.1795566821875
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_314"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 302
    zlevel -1

    cd19dm [
      annotation "PUBMED:24231807"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_269"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_1_re44"
      uniprot "NA"
    ]
    graphics [
      x 2044.3161797014677
      y 638.1586002444724
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_269"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 303
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000107745;urn:miriam:uniprot:Q9BPX6;urn:miriam:uniprot:Q9BPX6;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1530;urn:miriam:pubmed:24231807;urn:miriam:hgnc.symbol:MICU1;urn:miriam:hgnc.symbol:MICU1;urn:miriam:refseq:NM_006077;urn:miriam:ncbigene:10367;urn:miriam:ncbigene:10367"
      hgnc "HGNC_SYMBOL:MICU1"
      map_id "M118_313"
      name "MCU1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa32"
      uniprot "UNIPROT:Q9BPX6"
    ]
    graphics [
      x 1833.283734656247
      y 521.9071942380042
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_313"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 304
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_138763;urn:miriam:hgnc:959;urn:miriam:ensembl:ENSG00000087088;urn:miriam:hgnc.symbol:BAX;urn:miriam:ncbigene:581;urn:miriam:uniprot:Q07812;urn:miriam:uniprot:Q07812;urn:miriam:ncbigene:581"
      hgnc "HGNC_SYMBOL:BAX"
      map_id "M118_122"
      name "BAX"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa126"
      uniprot "UNIPROT:Q07812"
    ]
    graphics [
      x 1153.9849013526484
      y 532.0503967110174
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 305
    zlevel -1

    cd19dm [
      annotation "PUBMED:18191217;PUBMED:23430059;PUBMED:29450140"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_104"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re81"
      uniprot "NA"
    ]
    graphics [
      x 1242.976414910532
      y 637.1655426142847
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 306
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_138763;urn:miriam:hgnc:959;urn:miriam:ensembl:ENSG00000087088;urn:miriam:hgnc.symbol:BAX;urn:miriam:ncbigene:581;urn:miriam:uniprot:Q07812;urn:miriam:uniprot:Q07812;urn:miriam:ncbigene:581"
      hgnc "HGNC_SYMBOL:BAX"
      map_id "M118_123"
      name "BAX"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa127"
      uniprot "UNIPROT:Q07812"
    ]
    graphics [
      x 1497.059962504539
      y 502.80801868882577
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 307
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_005080;urn:miriam:hgnc:12801;urn:miriam:ensembl:ENSG00000100219;urn:miriam:hgnc.symbol:XBP1;urn:miriam:ncbigene:7494;urn:miriam:ncbigene:7494;urn:miriam:uniprot:P17861;urn:miriam:uniprot:P17861"
      hgnc "HGNC_SYMBOL:XBP1"
      map_id "M118_213"
      name "XBP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa627"
      uniprot "UNIPROT:P17861"
    ]
    graphics [
      x 355.9773649737431
      y 1617.2131366100734
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_213"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 308
    zlevel -1

    cd19dm [
      annotation "PUBMED:26137585;PUBMED:23850759;PUBMED:26587781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_58"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re332"
      uniprot "NA"
    ]
    graphics [
      x 425.67982232403585
      y 1741.7232891972894
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 309
    zlevel -1

    cd19dm [
      annotation "PUBMED:24231807"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_278"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_1_re58"
      uniprot "NA"
    ]
    graphics [
      x 2196.3698650374017
      y 1216.9669351352743
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_278"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 310
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_63"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re342"
      uniprot "NA"
    ]
    graphics [
      x 1749.745601090348
      y 460.4751957188721
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 311
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:3255;urn:miriam:ensembl:ENSG00000172071;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:EIF2AK3;urn:miriam:uniprot:Q9NZJ5;urn:miriam:uniprot:Q9NZJ5;urn:miriam:ncbigene:9451;urn:miriam:ncbigene:9451;urn:miriam:refseq:NM_004836"
      hgnc "HGNC_SYMBOL:EIF2AK3"
      map_id "M118_136"
      name "EIF2AK3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa2"
      uniprot "UNIPROT:Q9NZJ5"
    ]
    graphics [
      x 881.3927686798206
      y 1279.583870457624
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 312
    zlevel -1

    cd19dm [
      annotation "PUBMED:17991856"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_25"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re129"
      uniprot "NA"
    ]
    graphics [
      x 1003.9628258442197
      y 1336.9317989452022
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 313
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000125740;urn:miriam:hgnc.symbol:JUN;urn:miriam:refseq:NM_005252;urn:miriam:refseq:NM_005253;urn:miriam:uniprot:P01100;urn:miriam:uniprot:P01100;urn:miriam:ncbigene:3725;urn:miriam:ncbigene:3725;urn:miriam:hgnc.symbol:FOSL1;urn:miriam:ncbigene:3727;urn:miriam:ncbigene:3727;urn:miriam:hgnc.symbol:FOSL2;urn:miriam:ncbigene:3726;urn:miriam:ncbigene:3726;urn:miriam:uniprot:P17535;urn:miriam:ensembl:ENSG00000170345;urn:miriam:uniprot:P17535;urn:miriam:ncbigene:8061;urn:miriam:ncbigene:8061;urn:miriam:refseq:NM_002229;urn:miriam:refseq:NM_002228;urn:miriam:ensembl:ENSG00000075426;urn:miriam:hgnc:6206;urn:miriam:hgnc:6204;urn:miriam:hgnc:6205;urn:miriam:uniprot:P15407;urn:miriam:uniprot:P15407;urn:miriam:uniprot:P15408;urn:miriam:uniprot:P15408;urn:miriam:hgnc:13718;urn:miriam:ensembl:ENSG00000171223;urn:miriam:hgnc.symbol:FOSB;urn:miriam:hgnc.symbol:FOS;urn:miriam:hgnc.symbol:JUNB;urn:miriam:hgnc.symbol:JUND;urn:miriam:refseq:NM_005354;urn:miriam:uniprot:P05412;urn:miriam:uniprot:P05412;urn:miriam:uniprot:P17275;urn:miriam:refseq:NM_005438;urn:miriam:uniprot:P17275;urn:miriam:uniprot:P53539;urn:miriam:uniprot:P53539;urn:miriam:ensembl:ENSG00000130522;urn:miriam:hgnc:3796;urn:miriam:hgnc:3798;urn:miriam:hgnc:3797;urn:miriam:ncbigene:2353;urn:miriam:ncbigene:2353;urn:miriam:ncbigene:2355;urn:miriam:ncbigene:2355;urn:miriam:ncbigene:2354;urn:miriam:ncbigene:2354;urn:miriam:ensembl:ENSG00000175592;urn:miriam:refseq:NM_006732;urn:miriam:ensembl:ENSG00000177606"
      hgnc "HGNC_SYMBOL:JUN;HGNC_SYMBOL:FOSL1;HGNC_SYMBOL:FOSL2;HGNC_SYMBOL:FOSB;HGNC_SYMBOL:FOS;HGNC_SYMBOL:JUNB;HGNC_SYMBOL:JUND"
      map_id "M118_150"
      name "AP_minus_1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa260"
      uniprot "UNIPROT:P01100;UNIPROT:P17535;UNIPROT:P15407;UNIPROT:P15408;UNIPROT:P05412;UNIPROT:P17275;UNIPROT:P53539"
    ]
    graphics [
      x 1480.6805504796744
      y 891.6602028433239
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_150"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 314
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:26587781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_28"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re154"
      uniprot "NA"
    ]
    graphics [
      x 1357.9970670489397
      y 895.8786473625031
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 315
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000125740;urn:miriam:hgnc.symbol:JUN;urn:miriam:refseq:NM_005252;urn:miriam:refseq:NM_005253;urn:miriam:uniprot:P01100;urn:miriam:uniprot:P01100;urn:miriam:ncbigene:3725;urn:miriam:ncbigene:3725;urn:miriam:hgnc.symbol:FOSL1;urn:miriam:ncbigene:3727;urn:miriam:ncbigene:3727;urn:miriam:hgnc.symbol:FOSL2;urn:miriam:ncbigene:3726;urn:miriam:ncbigene:3726;urn:miriam:uniprot:P17535;urn:miriam:ensembl:ENSG00000170345;urn:miriam:uniprot:P17535;urn:miriam:ncbigene:8061;urn:miriam:ncbigene:8061;urn:miriam:refseq:NM_002229;urn:miriam:refseq:NM_002228;urn:miriam:ensembl:ENSG00000075426;urn:miriam:hgnc:6206;urn:miriam:hgnc:6204;urn:miriam:hgnc:6205;urn:miriam:uniprot:P15407;urn:miriam:uniprot:P15407;urn:miriam:uniprot:P15408;urn:miriam:uniprot:P15408;urn:miriam:hgnc:13718;urn:miriam:ensembl:ENSG00000171223;urn:miriam:hgnc.symbol:FOSB;urn:miriam:hgnc.symbol:FOS;urn:miriam:hgnc.symbol:JUNB;urn:miriam:hgnc.symbol:JUND;urn:miriam:refseq:NM_005354;urn:miriam:uniprot:P05412;urn:miriam:uniprot:P05412;urn:miriam:uniprot:P17275;urn:miriam:refseq:NM_005438;urn:miriam:uniprot:P17275;urn:miriam:uniprot:P53539;urn:miriam:uniprot:P53539;urn:miriam:ensembl:ENSG00000130522;urn:miriam:hgnc:3796;urn:miriam:hgnc:3798;urn:miriam:hgnc:3797;urn:miriam:ncbigene:2353;urn:miriam:ncbigene:2353;urn:miriam:ncbigene:2355;urn:miriam:ncbigene:2355;urn:miriam:ncbigene:2354;urn:miriam:ncbigene:2354;urn:miriam:ensembl:ENSG00000175592;urn:miriam:refseq:NM_006732;urn:miriam:ensembl:ENSG00000177606"
      hgnc "HGNC_SYMBOL:JUN;HGNC_SYMBOL:FOSL1;HGNC_SYMBOL:FOSL2;HGNC_SYMBOL:FOSB;HGNC_SYMBOL:FOS;HGNC_SYMBOL:JUNB;HGNC_SYMBOL:JUND"
      map_id "M118_151"
      name "AP_minus_1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa261"
      uniprot "UNIPROT:P01100;UNIPROT:P17535;UNIPROT:P15407;UNIPROT:P15408;UNIPROT:P05412;UNIPROT:P17275;UNIPROT:P53539"
    ]
    graphics [
      x 1477.8352079738206
      y 953.0929678452071
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_151"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 316
    zlevel -1

    cd19dm [
      annotation "PUBMED:30590907;PUBMED:17981125;PUBMED:25704011"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_262"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_1_re147"
      uniprot "NA"
    ]
    graphics [
      x 1144.012270872331
      y 442.73966100638336
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_262"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 317
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:uniprot:Q99720;urn:miriam:uniprot:Q99720"
      hgnc "HGNC_SYMBOL:SIGMAR1"
      map_id "M118_323"
      name "SIGMAR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa82"
      uniprot "UNIPROT:Q99720"
    ]
    graphics [
      x 1276.384362164844
      y 483.9796731242071
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_323"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 318
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:30590907;urn:miriam:pubmed:17981125;urn:miriam:taxonomy:10029;urn:miriam:pubmed:25704011;urn:miriam:hgnc.symbol:HSPA5;urn:miriam:ensembl:ENSG00000044574;urn:miriam:hgnc.symbol:HSPA5;urn:miriam:refseq:NM_005347;urn:miriam:hgnc:5238;urn:miriam:ncbigene:3309;urn:miriam:ncbigene:3309;urn:miriam:uniprot:P11021;urn:miriam:uniprot:P11021;urn:miriam:ec-code:3.6.4.10;urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:uniprot:Q99720;urn:miriam:uniprot:Q99720"
      hgnc "HGNC_SYMBOL:HSPA5;HGNC_SYMBOL:SIGMAR1"
      map_id "M118_228"
      name "SIGMAR1:HSPA5"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_1_csa13"
      uniprot "UNIPROT:P11021;UNIPROT:Q99720"
    ]
    graphics [
      x 1039.0229352677316
      y 579.9147975160963
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_228"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 319
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29108"
      hgnc "NA"
      map_id "M118_134"
      name "Ca2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "path_0_sa163"
      uniprot "NA"
    ]
    graphics [
      x 1231.9433514652565
      y 1702.4480185397817
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 320
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_23"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "path_0_re107"
      uniprot "NA"
    ]
    graphics [
      x 1333.9710116469923
      y 1913.5142442054116
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 321
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:11723;urn:miriam:ensembl:ENSG00000139644;urn:miriam:uniprot:P55061;urn:miriam:uniprot:P55061;urn:miriam:hgnc.symbol:TMBIM6;urn:miriam:ncbigene:7009;urn:miriam:ncbigene:7009;urn:miriam:refseq:NM_003217"
      hgnc "HGNC_SYMBOL:TMBIM6"
      map_id "M118_181"
      name "TMBIM6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa487"
      uniprot "UNIPROT:P55061"
    ]
    graphics [
      x 1117.4204466747753
      y 1848.1135248989258
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_181"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 322
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:6261;urn:miriam:refseq:NM_000540;urn:miriam:ncbigene:6261;urn:miriam:ensembl:ENSG00000196218;urn:miriam:hgnc:10483;urn:miriam:hgnc.symbol:RYR1;urn:miriam:uniprot:P21817;urn:miriam:uniprot:P21817"
      hgnc "HGNC_SYMBOL:RYR1"
      map_id "M118_182"
      name "RYR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa494"
      uniprot "UNIPROT:P21817"
    ]
    graphics [
      x 1352.8235583766916
      y 2016.5667922759578
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_182"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 323
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:ITPR1;urn:miriam:hgnc.symbol:ITPR3;urn:miriam:hgnc.symbol:ITPR2;urn:miriam:ensembl:ENSG00000123104;urn:miriam:ncbigene:3710;urn:miriam:ncbigene:3710;urn:miriam:refseq:NM_002223;urn:miriam:refseq:NM_002222;urn:miriam:uniprot:Q14571;urn:miriam:uniprot:Q14571;urn:miriam:refseq:NM_002224;urn:miriam:hgnc:6180;urn:miriam:uniprot:Q14573;urn:miriam:uniprot:Q14573;urn:miriam:ncbigene:3709;urn:miriam:ncbigene:3709;urn:miriam:ncbigene:3708;urn:miriam:ncbigene:3708;urn:miriam:ensembl:ENSG00000150995;urn:miriam:hgnc:6181;urn:miriam:uniprot:Q14643;urn:miriam:hgnc:6182;urn:miriam:uniprot:Q14643;urn:miriam:ensembl:ENSG00000096433"
      hgnc "HGNC_SYMBOL:ITPR1;HGNC_SYMBOL:ITPR3;HGNC_SYMBOL:ITPR2"
      map_id "M118_192"
      name "ITPR"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa583"
      uniprot "UNIPROT:Q14571;UNIPROT:Q14573;UNIPROT:Q14643"
    ]
    graphics [
      x 1180.8160833202646
      y 1776.6160839788643
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_192"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 324
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_61"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re337"
      uniprot "NA"
    ]
    graphics [
      x 822.9598261023243
      y 1973.4596526044884
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 325
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0006412"
      hgnc "NA"
      map_id "M118_226"
      name "Translation_space_initiation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa96"
      uniprot "NA"
    ]
    graphics [
      x 702.5766887694763
      y 1991.4933291626955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_226"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 326
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000101255;urn:miriam:ncbigene:57761;urn:miriam:hgnc.symbol:TRIB3;urn:miriam:hgnc:16228;urn:miriam:refseq:NM_021158;urn:miriam:uniprot:Q96RU7"
      hgnc "HGNC_SYMBOL:TRIB3"
      map_id "M118_178"
      name "TRIB3"
      node_subtype "GENE"
      node_type "species"
      org_id "path_0_sa484"
      uniprot "UNIPROT:Q96RU7"
    ]
    graphics [
      x 918.6531552871066
      y 740.9152942130736
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_178"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 327
    zlevel -1

    cd19dm [
      annotation "PUBMED:23430059;PUBMED:18940792"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_41"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "path_0_re252"
      uniprot "NA"
    ]
    graphics [
      x 861.811145165992
      y 863.872677116035
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 328
    zlevel -1

    cd19dm [
      annotation "PUBMED:28132811"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_248"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "path_1_re119"
      uniprot "NA"
    ]
    graphics [
      x 864.783072805006
      y 460.0617324496019
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_248"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 329
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000102580;urn:miriam:hgnc.symbol:DNAJC3;urn:miriam:hgnc:9439;urn:miriam:refseq:NM_006260;urn:miriam:uniprot:Q13217;urn:miriam:ncbigene:5611"
      hgnc "HGNC_SYMBOL:DNAJC3"
      map_id "M118_185"
      name "DNAJC3"
      node_subtype "GENE"
      node_type "species"
      org_id "path_0_sa516"
      uniprot "UNIPROT:Q13217"
    ]
    graphics [
      x 1430.5649564955002
      y 1119.7847087903297
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_185"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 330
    zlevel -1

    cd19dm [
      annotation "PUBMED:12601012;PUBMED:18360008"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_43"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "path_0_re273"
      uniprot "NA"
    ]
    graphics [
      x 1330.8636923156957
      y 1023.6281917619995
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 331
    zlevel -1

    cd19dm [
      annotation "PUBMED:18441099"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_335"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re139"
      uniprot "NA"
    ]
    graphics [
      x 1251.9466166808809
      y 805.9240676818129
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_335"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 332
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:18441099;urn:miriam:taxonomy:9606;urn:miriam:mesh:D011658"
      hgnc "NA"
      map_id "M118_345"
      name "pulmonary_space_fibrosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa266"
      uniprot "NA"
    ]
    graphics [
      x 1236.8929277842408
      y 700.1771490148757
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_345"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 333
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_005080;urn:miriam:hgnc:12801;urn:miriam:ensembl:ENSG00000100219;urn:miriam:hgnc.symbol:XBP1;urn:miriam:ncbigene:7494;urn:miriam:ncbigene:7494;urn:miriam:uniprot:P17861;urn:miriam:uniprot:P17861"
      hgnc "HGNC_SYMBOL:XBP1"
      map_id "M118_212"
      name "XBP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa626"
      uniprot "UNIPROT:P17861"
    ]
    graphics [
      x 677.3510706258577
      y 1750.1609010888633
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_212"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 334
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:26587781;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_57"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re331"
      uniprot "NA"
    ]
    graphics [
      x 699.1167304882615
      y 1621.0242016172651
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 335
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:HSPA5;urn:miriam:ensembl:ENSG00000044574;urn:miriam:refseq:NM_005347;urn:miriam:hgnc:5238;urn:miriam:ncbigene:3309;urn:miriam:uniprot:P11021"
      hgnc "HGNC_SYMBOL:HSPA5"
      map_id "M118_289"
      name "HSPA5"
      node_subtype "GENE"
      node_type "species"
      org_id "path_1_sa125"
      uniprot "UNIPROT:P11021"
    ]
    graphics [
      x 1201.5141809611875
      y 1003.7570365135514
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_289"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 336
    zlevel -1

    cd19dm [
      annotation "PUBMED:31775868;PUBMED:22511781;PUBMED:16940539"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_241"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "path_1_re110"
      uniprot "NA"
    ]
    graphics [
      x 1220.5114845805563
      y 1164.6574884732356
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_241"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 337
    zlevel -1

    cd19dm [
      annotation "PUBMED:22511781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_283"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re73"
      uniprot "NA"
    ]
    graphics [
      x 484.62928597405096
      y 694.0827326356161
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_283"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 338
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_78"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re364"
      uniprot "NA"
    ]
    graphics [
      x 1705.816010743245
      y 1191.3695272357174
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 339
    zlevel -1

    cd19dm [
      annotation "PUBMED:30590907;PUBMED:17981125;PUBMED:25704011"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_267"
      name "NA"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "path_1_re40"
      uniprot "NA"
    ]
    graphics [
      x 818.3850865010872
      y 647.678471663189
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_267"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 340
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_72"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re358"
      uniprot "NA"
    ]
    graphics [
      x 1136.2277572319888
      y 1179.2179196764123
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 341
    zlevel -1

    cd19dm [
      annotation "PUBMED:30773986;PUBMED:23850759;PUBMED:12847084"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_24"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re11"
      uniprot "NA"
    ]
    graphics [
      x 928.8259315594366
      y 1668.7970075710332
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 342
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_62"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re338"
      uniprot "NA"
    ]
    graphics [
      x 1543.5354124455425
      y 721.6449368766487
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 343
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_69"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re355"
      uniprot "NA"
    ]
    graphics [
      x 1242.5534216055132
      y 1820.80181375134
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 344
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:BID;urn:miriam:ncbigene:637;urn:miriam:ncbigene:637;urn:miriam:refseq:NM_197966;urn:miriam:uniprot:P55957;urn:miriam:uniprot:P55957;urn:miriam:ensembl:ENSG00000015475;urn:miriam:hgnc:1050"
      hgnc "HGNC_SYMBOL:BID"
      map_id "M118_170"
      name "BID"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa435"
      uniprot "UNIPROT:P55957"
    ]
    graphics [
      x 654.5424533902574
      y 1183.8002189088722
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_170"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 345
    zlevel -1

    cd19dm [
      annotation "PUBMED:23430059;PUBMED:11583631"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_37"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re222"
      uniprot "NA"
    ]
    graphics [
      x 855.1452668548591
      y 1157.267125850026
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 346
    zlevel -1

    cd19dm [
      annotation "PUBMED:19411314"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_338"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re142"
      uniprot "NA"
    ]
    graphics [
      x 1753.0715767404372
      y 1790.3282209066188
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_338"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 347
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000101255;urn:miriam:ncbigene:57761;urn:miriam:ncbigene:57761;urn:miriam:hgnc.symbol:TRIB3;urn:miriam:hgnc:16228;urn:miriam:refseq:NM_021158;urn:miriam:uniprot:Q96RU7;urn:miriam:uniprot:Q96RU7"
      hgnc "HGNC_SYMBOL:TRIB3"
      map_id "M118_342"
      name "TRIB3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa257"
      uniprot "UNIPROT:Q96RU7"
    ]
    graphics [
      x 1835.114679972426
      y 1189.8679030739306
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_342"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 348
    zlevel -1

    cd19dm [
      annotation "PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_76"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re362"
      uniprot "NA"
    ]
    graphics [
      x 1723.2863757582077
      y 1098.5228658869655
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 349
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:ITPR1;urn:miriam:hgnc.symbol:ITPR3;urn:miriam:hgnc.symbol:ITPR2;urn:miriam:ensembl:ENSG00000123104;urn:miriam:ncbigene:3710;urn:miriam:ncbigene:3710;urn:miriam:refseq:NM_002223;urn:miriam:refseq:NM_002222;urn:miriam:uniprot:Q14571;urn:miriam:uniprot:Q14571;urn:miriam:refseq:NM_002224;urn:miriam:hgnc:6180;urn:miriam:uniprot:Q14573;urn:miriam:uniprot:Q14573;urn:miriam:ncbigene:3709;urn:miriam:ncbigene:3709;urn:miriam:ncbigene:3708;urn:miriam:ncbigene:3708;urn:miriam:ensembl:ENSG00000150995;urn:miriam:hgnc:6181;urn:miriam:uniprot:Q14643;urn:miriam:hgnc:6182;urn:miriam:uniprot:Q14643;urn:miriam:ensembl:ENSG00000096433"
      hgnc "HGNC_SYMBOL:ITPR1;HGNC_SYMBOL:ITPR3;HGNC_SYMBOL:ITPR2"
      map_id "M118_191"
      name "ITPR"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa582"
      uniprot "UNIPROT:Q14571;UNIPROT:Q14573;UNIPROT:Q14643"
    ]
    graphics [
      x 1054.6106631594394
      y 1533.756655004454
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_191"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 350
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_48"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re317"
      uniprot "NA"
    ]
    graphics [
      x 1010.4681920179252
      y 1666.0188148563157
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 351
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:13280;urn:miriam:hgnc.symbol:ERO1A;urn:miriam:refseq:NM_014584;urn:miriam:uniprot:Q96HE7;urn:miriam:uniprot:Q96HE7;urn:miriam:ncbigene:30001;urn:miriam:ncbigene:30001;urn:miriam:ec-code:1.8.4.-;urn:miriam:ensembl:ENSG00000197930"
      hgnc "HGNC_SYMBOL:ERO1A"
      map_id "M118_211"
      name "ERO1A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa625"
      uniprot "UNIPROT:Q96HE7"
    ]
    graphics [
      x 919.1007852688031
      y 1579.1936166371786
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_211"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 352
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_188"
      target_id "M118_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 353
    source 3
    target 2
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "M118_175"
      target_id "M118_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 354
    source 2
    target 4
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_85"
      target_id "M118_187"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 355
    source 5
    target 6
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_186"
      target_id "M118_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 356
    source 6
    target 7
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_44"
      target_id "M118_184"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 357
    source 8
    target 9
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_139"
      target_id "M118_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 358
    source 9
    target 10
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_34"
      target_id "M118_149"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 359
    source 11
    target 12
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_291"
      target_id "M118_330"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 360
    source 13
    target 12
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_343"
      target_id "M118_330"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 361
    source 12
    target 14
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_330"
      target_id "M118_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 362
    source 15
    target 16
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_4"
      target_id "M118_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 363
    source 17
    target 16
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_207"
      target_id "M118_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 364
    source 16
    target 18
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_22"
      target_id "M118_157"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 365
    source 16
    target 19
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_22"
      target_id "M118_163"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 366
    source 20
    target 21
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_293"
      target_id "M118_255"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 367
    source 21
    target 22
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_255"
      target_id "M118_296"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 368
    source 23
    target 24
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_118"
      target_id "M118_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 369
    source 25
    target 24
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_19"
      target_id "M118_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 370
    source 24
    target 26
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_45"
      target_id "M118_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 371
    source 27
    target 28
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_152"
      target_id "M118_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 372
    source 29
    target 28
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_218"
      target_id "M118_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 373
    source 25
    target 28
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_19"
      target_id "M118_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 374
    source 30
    target 28
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_17"
      target_id "M118_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 375
    source 31
    target 28
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_16"
      target_id "M118_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 376
    source 28
    target 8
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_33"
      target_id "M118_139"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 377
    source 32
    target 33
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_301"
      target_id "M118_256"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 378
    source 34
    target 33
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "M118_311"
      target_id "M118_256"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 379
    source 35
    target 33
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M118_286"
      target_id "M118_256"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 380
    source 33
    target 36
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_256"
      target_id "M118_304"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 381
    source 37
    target 38
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_196"
      target_id "M118_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 382
    source 38
    target 39
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_77"
      target_id "M118_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 383
    source 40
    target 41
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_2"
      target_id "M118_339"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 384
    source 41
    target 42
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_339"
      target_id "M118_346"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 385
    source 43
    target 44
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_328"
      target_id "M118_276"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 386
    source 45
    target 44
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_329"
      target_id "M118_276"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 387
    source 44
    target 46
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_276"
      target_id "M118_234"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 388
    source 47
    target 48
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_120"
      target_id "M118_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 389
    source 49
    target 48
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_219"
      target_id "M118_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 390
    source 29
    target 48
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_218"
      target_id "M118_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 391
    source 48
    target 50
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_101"
      target_id "M118_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 392
    source 51
    target 52
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_130"
      target_id "M118_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 393
    source 52
    target 53
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_26"
      target_id "M118_133"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 394
    source 54
    target 55
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_166"
      target_id "M118_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 395
    source 56
    target 55
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_162"
      target_id "M118_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 396
    source 55
    target 57
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_47"
      target_id "M118_167"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 397
    source 58
    target 59
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_107"
      target_id "M118_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 398
    source 60
    target 59
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_137"
      target_id "M118_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 399
    source 59
    target 61
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_35"
      target_id "M118_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 400
    source 62
    target 63
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_119"
      target_id "M118_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 401
    source 63
    target 64
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_27"
      target_id "M118_142"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 402
    source 65
    target 66
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_8"
      target_id "M118_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 403
    source 66
    target 67
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_71"
      target_id "M118_159"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 404
    source 68
    target 69
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_115"
      target_id "M118_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 405
    source 69
    target 70
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_99"
      target_id "M118_114"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 406
    source 71
    target 72
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_208"
      target_id "M118_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 407
    source 73
    target 72
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_205"
      target_id "M118_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 408
    source 72
    target 74
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_88"
      target_id "M118_217"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 409
    source 75
    target 76
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_324"
      target_id "M118_333"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 410
    source 76
    target 35
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_333"
      target_id "M118_286"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 411
    source 74
    target 77
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_217"
      target_id "M118_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 412
    source 77
    target 67
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_73"
      target_id "M118_159"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 413
    source 78
    target 79
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_200"
      target_id "M118_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 414
    source 80
    target 79
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_135"
      target_id "M118_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 415
    source 81
    target 79
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_148"
      target_id "M118_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 416
    source 79
    target 82
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_53"
      target_id "M118_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 417
    source 43
    target 83
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_328"
      target_id "M118_279"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 418
    source 83
    target 35
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_279"
      target_id "M118_286"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 419
    source 84
    target 85
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_190"
      target_id "M118_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 420
    source 85
    target 86
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_65"
      target_id "M118_143"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 421
    source 87
    target 88
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_325"
      target_id "M118_261"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 422
    source 88
    target 89
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_261"
      target_id "M118_290"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 423
    source 90
    target 91
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_305"
      target_id "M118_259"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 424
    source 91
    target 92
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_259"
      target_id "M118_300"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 425
    source 32
    target 93
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_301"
      target_id "M118_252"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 426
    source 94
    target 93
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_302"
      target_id "M118_252"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 427
    source 93
    target 95
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_252"
      target_id "M118_227"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 428
    source 61
    target 96
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_6"
      target_id "M118_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 429
    source 97
    target 96
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_138"
      target_id "M118_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 430
    source 98
    target 96
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_5"
      target_id "M118_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 431
    source 99
    target 96
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_7"
      target_id "M118_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 432
    source 100
    target 96
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "M118_131"
      target_id "M118_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 433
    source 101
    target 96
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "M118_132"
      target_id "M118_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 434
    source 86
    target 96
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "M118_143"
      target_id "M118_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 435
    source 102
    target 96
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_173"
      target_id "M118_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 436
    source 103
    target 96
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_174"
      target_id "M118_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 437
    source 96
    target 25
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_32"
      target_id "M118_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 438
    source 94
    target 104
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_302"
      target_id "M118_253"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 439
    source 104
    target 105
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_253"
      target_id "M118_303"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 440
    source 106
    target 107
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_112"
      target_id "M118_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 441
    source 108
    target 107
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_13"
      target_id "M118_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 442
    source 107
    target 109
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_96"
      target_id "M118_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 443
    source 110
    target 111
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_223"
      target_id "M118_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 444
    source 112
    target 111
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_12"
      target_id "M118_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 445
    source 113
    target 111
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_224"
      target_id "M118_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 446
    source 111
    target 114
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_93"
      target_id "M118_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 447
    source 115
    target 116
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_171"
      target_id "M118_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 448
    source 116
    target 39
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_75"
      target_id "M118_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 449
    source 43
    target 117
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_328"
      target_id "M118_249"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 450
    source 118
    target 117
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_299"
      target_id "M118_249"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 451
    source 117
    target 119
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_249"
      target_id "M118_231"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 452
    source 120
    target 121
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_129"
      target_id "M118_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 453
    source 84
    target 121
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_190"
      target_id "M118_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 454
    source 121
    target 122
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_106"
      target_id "M118_128"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 455
    source 123
    target 124
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_20"
      target_id "M118_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 456
    source 125
    target 124
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_109"
      target_id "M118_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 457
    source 126
    target 124
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_14"
      target_id "M118_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 458
    source 124
    target 127
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_94"
      target_id "M118_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 459
    source 124
    target 128
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_94"
      target_id "M118_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 460
    source 75
    target 129
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_324"
      target_id "M118_268"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 461
    source 130
    target 129
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_326"
      target_id "M118_268"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 462
    source 131
    target 129
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "TRIGGER"
      source_id "M118_322"
      target_id "M118_268"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 463
    source 132
    target 129
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "TRIGGER"
      source_id "M118_319"
      target_id "M118_268"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 464
    source 129
    target 133
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_268"
      target_id "M118_229"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 465
    source 134
    target 135
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_206"
      target_id "M118_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 466
    source 136
    target 135
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_177"
      target_id "M118_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 467
    source 137
    target 135
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "M118_216"
      target_id "M118_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 468
    source 135
    target 30
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_39"
      target_id "M118_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 469
    source 43
    target 138
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_328"
      target_id "M118_282"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 470
    source 138
    target 139
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_282"
      target_id "M118_287"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 471
    source 140
    target 141
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_3"
      target_id "M118_337"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 472
    source 142
    target 141
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_350"
      target_id "M118_337"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 473
    source 141
    target 42
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_337"
      target_id "M118_346"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 474
    source 39
    target 143
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_172"
      target_id "M118_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 475
    source 143
    target 144
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_80"
      target_id "M118_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 476
    source 131
    target 145
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_322"
      target_id "M118_280"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 477
    source 145
    target 35
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_280"
      target_id "M118_286"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 478
    source 146
    target 147
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_164"
      target_id "M118_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 479
    source 148
    target 147
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_127"
      target_id "M118_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 480
    source 56
    target 147
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_162"
      target_id "M118_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 481
    source 147
    target 149
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_46"
      target_id "M118_165"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 482
    source 75
    target 150
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_324"
      target_id "M118_332"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 483
    source 150
    target 131
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_332"
      target_id "M118_322"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 484
    source 119
    target 151
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_231"
      target_id "M118_273"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 485
    source 151
    target 152
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_273"
      target_id "M118_327"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 486
    source 153
    target 154
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_169"
      target_id "M118_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 487
    source 155
    target 154
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_201"
      target_id "M118_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 488
    source 154
    target 156
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_40"
      target_id "M118_168"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 489
    source 157
    target 158
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_124"
      target_id "M118_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 490
    source 158
    target 81
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_56"
      target_id "M118_148"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 491
    source 156
    target 159
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_168"
      target_id "M118_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 492
    source 149
    target 159
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_165"
      target_id "M118_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 493
    source 159
    target 153
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_36"
      target_id "M118_169"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 494
    source 160
    target 161
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_156"
      target_id "M118_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 495
    source 114
    target 161
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_11"
      target_id "M118_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 496
    source 161
    target 123
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_30"
      target_id "M118_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 497
    source 18
    target 162
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_157"
      target_id "M118_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 498
    source 84
    target 162
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_190"
      target_id "M118_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 499
    source 51
    target 162
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "M118_130"
      target_id "M118_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 500
    source 162
    target 163
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_86"
      target_id "M118_189"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 501
    source 164
    target 165
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_308"
      target_id "M118_263"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 502
    source 166
    target 165
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_307"
      target_id "M118_263"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 503
    source 165
    target 167
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_263"
      target_id "M118_232"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 504
    source 168
    target 169
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_202"
      target_id "M118_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 505
    source 169
    target 170
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_55"
      target_id "M118_204"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 506
    source 171
    target 172
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_197"
      target_id "M118_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 507
    source 173
    target 172
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_194"
      target_id "M118_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 508
    source 172
    target 174
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_52"
      target_id "M118_198"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 509
    source 175
    target 176
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_317"
      target_id "M118_265"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 510
    source 177
    target 176
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "TRIGGER"
      source_id "M118_233"
      target_id "M118_265"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 511
    source 178
    target 176
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "TRIGGER"
      source_id "M118_285"
      target_id "M118_265"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 512
    source 133
    target 176
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "TRIGGER"
      source_id "M118_229"
      target_id "M118_265"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 513
    source 176
    target 179
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_265"
      target_id "M118_318"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 514
    source 144
    target 180
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_125"
      target_id "M118_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 515
    source 180
    target 39
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_82"
      target_id "M118_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 516
    source 80
    target 181
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_135"
      target_id "M118_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 517
    source 181
    target 81
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_67"
      target_id "M118_148"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 518
    source 182
    target 183
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_235"
      target_id "M118_274"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 519
    source 183
    target 152
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_274"
      target_id "M118_327"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 520
    source 184
    target 185
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_221"
      target_id "M118_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 521
    source 185
    target 49
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_90"
      target_id "M118_219"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 522
    source 152
    target 186
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_327"
      target_id "M118_247"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 523
    source 186
    target 187
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_247"
      target_id "M118_298"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 524
    source 188
    target 189
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_292"
      target_id "M118_242"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 525
    source 189
    target 22
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_242"
      target_id "M118_296"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 526
    source 167
    target 190
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_232"
      target_id "M118_264"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 527
    source 190
    target 191
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_264"
      target_id "M118_306"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 528
    source 192
    target 193
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_153"
      target_id "M118_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 529
    source 194
    target 193
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_154"
      target_id "M118_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 530
    source 195
    target 193
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "M118_210"
      target_id "M118_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 531
    source 193
    target 196
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_29"
      target_id "M118_140"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 532
    source 197
    target 198
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_222"
      target_id "M118_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 533
    source 110
    target 198
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_223"
      target_id "M118_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 534
    source 112
    target 198
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_12"
      target_id "M118_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 535
    source 198
    target 108
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_31"
      target_id "M118_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 536
    source 43
    target 199
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_328"
      target_id "M118_250"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 537
    source 199
    target 89
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_250"
      target_id "M118_290"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 538
    source 200
    target 201
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_195"
      target_id "M118_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 539
    source 173
    target 201
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_194"
      target_id "M118_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 540
    source 201
    target 37
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_51"
      target_id "M118_196"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 541
    source 57
    target 202
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_167"
      target_id "M118_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 542
    source 202
    target 39
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_59"
      target_id "M118_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 543
    source 34
    target 203
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_311"
      target_id "M118_258"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 544
    source 203
    target 90
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_258"
      target_id "M118_305"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 545
    source 163
    target 204
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_189"
      target_id "M118_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 546
    source 205
    target 204
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_199"
      target_id "M118_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 547
    source 204
    target 71
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_87"
      target_id "M118_208"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 548
    source 109
    target 206
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_113"
      target_id "M118_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 549
    source 206
    target 207
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_97"
      target_id "M118_111"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 550
    source 208
    target 209
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_179"
      target_id "M118_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 551
    source 209
    target 210
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_42"
      target_id "M118_180"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 552
    source 211
    target 212
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_193"
      target_id "M118_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 553
    source 82
    target 212
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_18"
      target_id "M118_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 554
    source 212
    target 173
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_50"
      target_id "M118_194"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 555
    source 213
    target 214
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_10"
      target_id "M118_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 556
    source 214
    target 39
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_66"
      target_id "M118_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 557
    source 215
    target 216
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_294"
      target_id "M118_239"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 558
    source 131
    target 216
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "TRIGGER"
      source_id "M118_322"
      target_id "M118_239"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 559
    source 216
    target 43
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_239"
      target_id "M118_328"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 560
    source 113
    target 217
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_224"
      target_id "M118_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 561
    source 65
    target 217
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_8"
      target_id "M118_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 562
    source 218
    target 217
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_15"
      target_id "M118_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 563
    source 196
    target 217
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_140"
      target_id "M118_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 564
    source 7
    target 217
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "M118_184"
      target_id "M118_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 565
    source 217
    target 197
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_92"
      target_id "M118_222"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 566
    source 74
    target 219
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_217"
      target_id "M118_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 567
    source 219
    target 29
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_89"
      target_id "M118_218"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 568
    source 95
    target 220
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_227"
      target_id "M118_272"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 569
    source 220
    target 152
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_272"
      target_id "M118_327"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 570
    source 50
    target 221
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_121"
      target_id "M118_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 571
    source 221
    target 62
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_102"
      target_id "M118_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 572
    source 43
    target 222
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_328"
      target_id "M118_277"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 573
    source 222
    target 223
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_277"
      target_id "M118_284"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 574
    source 224
    target 225
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_215"
      target_id "M118_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 575
    source 225
    target 39
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_60"
      target_id "M118_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 576
    source 94
    target 226
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_302"
      target_id "M118_254"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 577
    source 226
    target 22
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_254"
      target_id "M118_296"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 578
    source 43
    target 227
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_328"
      target_id "M118_281"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 579
    source 227
    target 131
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_281"
      target_id "M118_322"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 580
    source 228
    target 229
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_158"
      target_id "M118_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 581
    source 229
    target 67
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_70"
      target_id "M118_159"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 582
    source 36
    target 230
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_304"
      target_id "M118_260"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 583
    source 230
    target 139
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_260"
      target_id "M118_287"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 584
    source 231
    target 232
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_160"
      target_id "M118_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 585
    source 232
    target 228
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_81"
      target_id "M118_158"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 586
    source 233
    target 234
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_288"
      target_id "M118_240"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 587
    source 234
    target 215
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_240"
      target_id "M118_294"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 588
    source 11
    target 235
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_291"
      target_id "M118_237"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 589
    source 235
    target 131
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_237"
      target_id "M118_322"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 590
    source 11
    target 236
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_291"
      target_id "M118_341"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 591
    source 236
    target 237
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_341"
      target_id "M118_351"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 592
    source 153
    target 238
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_169"
      target_id "M118_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 593
    source 238
    target 39
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_74"
      target_id "M118_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 594
    source 239
    target 240
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_315"
      target_id "M118_270"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 595
    source 241
    target 240
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_316"
      target_id "M118_270"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 596
    source 240
    target 242
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_270"
      target_id "M118_230"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 597
    source 46
    target 243
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_234"
      target_id "M118_271"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 598
    source 243
    target 152
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_271"
      target_id "M118_327"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 599
    source 34
    target 244
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_311"
      target_id "M118_251"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 600
    source 244
    target 152
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_251"
      target_id "M118_327"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 601
    source 245
    target 246
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_320"
      target_id "M118_266"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 602
    source 247
    target 246
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "TRIGGER"
      source_id "M118_236"
      target_id "M118_266"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 603
    source 246
    target 248
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_266"
      target_id "M118_321"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 604
    source 249
    target 250
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_348"
      target_id "M118_340"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 605
    source 42
    target 250
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "M118_346"
      target_id "M118_340"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 606
    source 250
    target 251
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_340"
      target_id "M118_349"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 607
    source 197
    target 252
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_222"
      target_id "M118_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 608
    source 70
    target 252
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_114"
      target_id "M118_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 609
    source 253
    target 252
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_155"
      target_id "M118_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 610
    source 252
    target 113
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_103"
      target_id "M118_224"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 611
    source 254
    target 255
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_161"
      target_id "M118_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 612
    source 26
    target 255
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_117"
      target_id "M118_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 613
    source 25
    target 255
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_19"
      target_id "M118_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 614
    source 255
    target 56
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_100"
      target_id "M118_162"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 615
    source 188
    target 256
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_292"
      target_id "M118_246"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 616
    source 256
    target 257
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_246"
      target_id "M118_297"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 617
    source 258
    target 259
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_126"
      target_id "M118_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 618
    source 122
    target 259
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_128"
      target_id "M118_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 619
    source 259
    target 148
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_105"
      target_id "M118_127"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 620
    source 64
    target 260
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_142"
      target_id "M118_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 621
    source 260
    target 261
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_79"
      target_id "M118_141"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 622
    source 262
    target 263
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_220"
      target_id "M118_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 623
    source 29
    target 263
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_218"
      target_id "M118_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 624
    source 207
    target 263
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "M118_111"
      target_id "M118_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 625
    source 137
    target 263
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "M118_216"
      target_id "M118_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 626
    source 10
    target 263
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_149"
      target_id "M118_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 627
    source 263
    target 184
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_91"
      target_id "M118_221"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 628
    source 18
    target 264
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_157"
      target_id "M118_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 629
    source 264
    target 51
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_64"
      target_id "M118_130"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 630
    source 13
    target 265
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_343"
      target_id "M118_331"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 631
    source 266
    target 265
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_344"
      target_id "M118_331"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 632
    source 265
    target 40
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_331"
      target_id "M118_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 633
    source 267
    target 268
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_295"
      target_id "M118_257"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 634
    source 131
    target 268
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "TRIGGER"
      source_id "M118_322"
      target_id "M118_257"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 635
    source 11
    target 268
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "M118_291"
      target_id "M118_257"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 636
    source 268
    target 87
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_257"
      target_id "M118_325"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 637
    source 269
    target 270
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_203"
      target_id "M118_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 638
    source 29
    target 270
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_218"
      target_id "M118_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 639
    source 270
    target 168
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_54"
      target_id "M118_202"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 640
    source 20
    target 271
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_293"
      target_id "M118_245"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 641
    source 271
    target 257
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_245"
      target_id "M118_297"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 642
    source 20
    target 272
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_293"
      target_id "M118_244"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 643
    source 272
    target 187
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_244"
      target_id "M118_298"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 644
    source 273
    target 274
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_214"
      target_id "M118_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 645
    source 57
    target 274
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_167"
      target_id "M118_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 646
    source 274
    target 224
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_84"
      target_id "M118_215"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 647
    source 275
    target 276
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_9"
      target_id "M118_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 648
    source 277
    target 276
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_144"
      target_id "M118_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 649
    source 278
    target 276
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_145"
      target_id "M118_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 650
    source 279
    target 276
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_146"
      target_id "M118_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 651
    source 280
    target 276
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_147"
      target_id "M118_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 652
    source 86
    target 276
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_143"
      target_id "M118_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 653
    source 276
    target 213
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_83"
      target_id "M118_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 654
    source 188
    target 281
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_292"
      target_id "M118_243"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 655
    source 281
    target 187
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_243"
      target_id "M118_298"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 656
    source 20
    target 282
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_293"
      target_id "M118_238"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 657
    source 188
    target 282
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_292"
      target_id "M118_238"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 658
    source 282
    target 182
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_238"
      target_id "M118_235"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 659
    source 134
    target 283
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_206"
      target_id "M118_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 660
    source 284
    target 283
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_176"
      target_id "M118_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 661
    source 137
    target 283
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "M118_216"
      target_id "M118_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 662
    source 283
    target 31
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_38"
      target_id "M118_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 663
    source 127
    target 285
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_21"
      target_id "M118_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 664
    source 286
    target 285
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_110"
      target_id "M118_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 665
    source 285
    target 123
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_95"
      target_id "M118_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 666
    source 285
    target 287
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_95"
      target_id "M118_225"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 667
    source 288
    target 289
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_309"
      target_id "M118_336"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 668
    source 290
    target 289
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_347"
      target_id "M118_336"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 669
    source 291
    target 289
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "M118_310"
      target_id "M118_336"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 670
    source 289
    target 140
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_336"
      target_id "M118_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 671
    source 49
    target 292
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_219"
      target_id "M118_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 672
    source 293
    target 292
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_209"
      target_id "M118_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 673
    source 292
    target 3
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_49"
      target_id "M118_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 674
    source 247
    target 294
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_236"
      target_id "M118_275"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 675
    source 294
    target 295
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_275"
      target_id "M118_312"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 676
    source 296
    target 297
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_116"
      target_id "M118_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 677
    source 207
    target 297
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_111"
      target_id "M118_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 678
    source 49
    target 297
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_219"
      target_id "M118_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 679
    source 297
    target 68
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_98"
      target_id "M118_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 680
    source 298
    target 299
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_183"
      target_id "M118_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 681
    source 299
    target 228
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_68"
      target_id "M118_158"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 682
    source 11
    target 300
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_291"
      target_id "M118_334"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 683
    source 191
    target 300
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_306"
      target_id "M118_334"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 684
    source 300
    target 291
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_334"
      target_id "M118_310"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 685
    source 300
    target 288
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_334"
      target_id "M118_309"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 686
    source 301
    target 302
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_314"
      target_id "M118_269"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 687
    source 303
    target 302
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_313"
      target_id "M118_269"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 688
    source 302
    target 242
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_269"
      target_id "M118_230"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 689
    source 304
    target 305
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_122"
      target_id "M118_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 690
    source 149
    target 305
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_165"
      target_id "M118_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 691
    source 57
    target 305
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_167"
      target_id "M118_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 692
    source 305
    target 306
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_104"
      target_id "M118_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 693
    source 307
    target 308
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_213"
      target_id "M118_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 694
    source 308
    target 231
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_58"
      target_id "M118_160"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 695
    source 242
    target 309
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_230"
      target_id "M118_278"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 696
    source 309
    target 247
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_278"
      target_id "M118_236"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 697
    source 306
    target 310
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_123"
      target_id "M118_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 698
    source 310
    target 157
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_63"
      target_id "M118_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 699
    source 311
    target 312
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_136"
      target_id "M118_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 700
    source 298
    target 312
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_183"
      target_id "M118_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 701
    source 86
    target 312
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_143"
      target_id "M118_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 702
    source 312
    target 65
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_25"
      target_id "M118_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 703
    source 313
    target 314
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_150"
      target_id "M118_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 704
    source 149
    target 314
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_165"
      target_id "M118_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 705
    source 273
    target 314
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_214"
      target_id "M118_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 706
    source 314
    target 315
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_28"
      target_id "M118_151"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 707
    source 87
    target 316
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_325"
      target_id "M118_262"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 708
    source 317
    target 316
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_323"
      target_id "M118_262"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 709
    source 316
    target 318
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_262"
      target_id "M118_228"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 710
    source 319
    target 320
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_134"
      target_id "M118_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 711
    source 321
    target 320
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_181"
      target_id "M118_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 712
    source 322
    target 320
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_182"
      target_id "M118_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 713
    source 323
    target 320
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_192"
      target_id "M118_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 714
    source 320
    target 80
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_23"
      target_id "M118_135"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 715
    source 127
    target 324
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_21"
      target_id "M118_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 716
    source 324
    target 325
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_61"
      target_id "M118_226"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 717
    source 326
    target 327
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_178"
      target_id "M118_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 718
    source 49
    target 327
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_219"
      target_id "M118_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 719
    source 327
    target 208
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_41"
      target_id "M118_179"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 720
    source 130
    target 328
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_326"
      target_id "M118_248"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 721
    source 328
    target 187
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_248"
      target_id "M118_298"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 722
    source 329
    target 330
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_185"
      target_id "M118_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 723
    source 29
    target 330
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_218"
      target_id "M118_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 724
    source 330
    target 5
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_43"
      target_id "M118_186"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 725
    source 11
    target 331
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_291"
      target_id "M118_335"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 726
    source 331
    target 332
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_335"
      target_id "M118_345"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 727
    source 333
    target 334
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_212"
      target_id "M118_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 728
    source 334
    target 67
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_57"
      target_id "M118_159"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 729
    source 335
    target 336
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_289"
      target_id "M118_241"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 730
    source 11
    target 336
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "M118_291"
      target_id "M118_241"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 731
    source 290
    target 336
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "TRIGGER"
      source_id "M118_347"
      target_id "M118_241"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 732
    source 336
    target 267
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_241"
      target_id "M118_295"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 733
    source 131
    target 337
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_322"
      target_id "M118_283"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 734
    source 337
    target 139
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_283"
      target_id "M118_287"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 735
    source 174
    target 338
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_198"
      target_id "M118_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 736
    source 338
    target 39
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_78"
      target_id "M118_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 737
    source 318
    target 339
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_228"
      target_id "M118_267"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 738
    source 131
    target 339
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "TRIGGER"
      source_id "M118_322"
      target_id "M118_267"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 739
    source 132
    target 339
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "TRIGGER"
      source_id "M118_319"
      target_id "M118_267"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 740
    source 339
    target 87
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_267"
      target_id "M118_325"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 741
    source 339
    target 75
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_267"
      target_id "M118_324"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 742
    source 25
    target 340
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_19"
      target_id "M118_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 743
    source 340
    target 67
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_72"
      target_id "M118_159"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 744
    source 65
    target 341
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_8"
      target_id "M118_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 745
    source 341
    target 51
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_24"
      target_id "M118_130"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 746
    source 306
    target 342
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_123"
      target_id "M118_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 747
    source 342
    target 39
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_62"
      target_id "M118_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 748
    source 228
    target 343
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_158"
      target_id "M118_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 749
    source 343
    target 84
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_69"
      target_id "M118_190"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 750
    source 344
    target 345
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_170"
      target_id "M118_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 751
    source 149
    target 345
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_165"
      target_id "M118_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 752
    source 345
    target 115
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_37"
      target_id "M118_171"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 753
    source 291
    target 346
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_310"
      target_id "M118_338"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 754
    source 346
    target 42
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_338"
      target_id "M118_346"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 755
    source 347
    target 348
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_342"
      target_id "M118_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 756
    source 348
    target 39
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_76"
      target_id "M118_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 757
    source 349
    target 350
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_191"
      target_id "M118_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 758
    source 321
    target 350
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_181"
      target_id "M118_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 759
    source 351
    target 350
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_211"
      target_id "M118_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 760
    source 350
    target 323
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_48"
      target_id "M118_192"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
