# generated with VANTED V2.8.2 at Fri Mar 04 10:04:40 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694339;urn:miriam:refseq:NC_004718.3;urn:miriam:reactome:R-COV-9681658"
      hgnc "NA"
      map_id "R1_77"
      name "SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand)"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_1932"
      uniprot "NA"
    ]
    graphics [
      x 620.4271966075642
      y 1483.6926949606964
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9682009;PUBMED:22362731"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_270"
      name "ZCRB1 binds 5'UTR of SARS-CoV-1 genomic RNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9682009__layout_200"
      uniprot "NA"
    ]
    graphics [
      x 504.432093319924
      y 1614.2668043836857
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_270"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-HSA-9682010;urn:miriam:uniprot:Q8TBF4"
      hgnc "NA"
      map_id "R1_87"
      name "ZCRB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_201"
      uniprot "UNIPROT:Q8TBF4"
    ]
    graphics [
      x 555.5276855144889
      y 1735.8291356110117
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-HSA-9682008;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:Q8TBF4"
      hgnc "NA"
      map_id "R1_88"
      name "ZCRB1:SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand)"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_202"
      uniprot "UNIPROT:Q8TBF4"
    ]
    graphics [
      x 548.7619120332942
      y 1469.3500989669742
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 5
    source 1
    target 2
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_77"
      target_id "R1_270"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 6
    source 3
    target 2
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_87"
      target_id "R1_270"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 7
    source 2
    target 4
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_270"
      target_id "R1_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
