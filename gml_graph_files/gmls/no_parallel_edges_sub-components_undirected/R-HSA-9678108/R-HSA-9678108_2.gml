# generated with VANTED V2.8.2 at Fri Mar 04 10:04:40 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:pubmed:15094372;urn:miriam:reactome:R-COV-9683649;urn:miriam:uniprot:P59595"
      hgnc "NA"
      map_id "R1_245"
      name "SUMO1_minus_K62_minus_p_minus_S177_minus_N_space_dimer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_739"
      uniprot "UNIPROT:P59595"
    ]
    graphics [
      x 473.85117303259403
      y 292.72474140416296
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_245"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683606;PUBMED:15848177"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_286"
      name "Nucleoprotein translocates to the nucleolus"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683606__layout_262"
      uniprot "NA"
    ]
    graphics [
      x 355.97797093296094
      y 257.1323430074042
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_286"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9683761;urn:miriam:pubmed:15094372;urn:miriam:uniprot:P59595;urn:miriam:reactome:R-COV-9694659"
      hgnc "NA"
      map_id "R1_132"
      name "SUMO_minus_p_minus_N_space_dimer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_263"
      uniprot "UNIPROT:P59595"
    ]
    graphics [
      x 407.4838298344665
      y 381.2175493597015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 4
    source 1
    target 2
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_245"
      target_id "R1_286"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 5
    source 2
    target 3
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_286"
      target_id "R1_132"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
