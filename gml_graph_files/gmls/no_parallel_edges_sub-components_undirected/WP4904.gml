# generated with VANTED V2.8.2 at Fri Mar 04 09:53:04 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:ensembl:ENSG00000267690"
      hgnc "NA"
      map_id "W23_7"
      name "LDLRAD4_minus_AS1"
      node_subtype "RNA"
      node_type "species"
      org_id "c9042"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 306.77436987055984
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:32111819"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_27"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idfb4f20fd"
      uniprot "NA"
    ]
    graphics [
      x 167.0824192178938
      y 219.95912604722963
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:ensembl:ENSG00000168675"
      hgnc "NA"
      map_id "W23_14"
      name "LDLRAD4"
      node_subtype "GENE"
      node_type "species"
      org_id "e9bfd"
      uniprot "NA"
    ]
    graphics [
      x 321.1049226176225
      y 244.67528124730808
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "PUBMED:31819986"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_28"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "idfdf4dec2"
      uniprot "NA"
    ]
    graphics [
      x 523.1363413305193
      y 218.58312826877972
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:mirbase.mature:MIMAT0000075"
      hgnc "NA"
      map_id "W23_15"
      name "hsa_minus_miR_minus_20a_minus_5p"
      node_subtype "RNA"
      node_type "species"
      org_id "f25b2"
      uniprot "NA"
    ]
    graphics [
      x 687.6065280967886
      y 237.96971629412997
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:ensembl:ENSG00000069869"
      hgnc "NA"
      map_id "W23_5"
      name "NEDD4"
      node_subtype "GENE"
      node_type "species"
      org_id "c276a"
      uniprot "NA"
    ]
    graphics [
      x 555.2938985494109
      y 434.6365087786535
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_22"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id71ecd0f3"
      uniprot "NA"
    ]
    graphics [
      x 676.6470639415227
      y 453.86281930073983
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:wikipathways:WP4217"
      hgnc "NA"
      map_id "W23_3"
      name "Ebola_space_virus_br_pathway_space_in_space_host"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "b8756"
      uniprot "NA"
    ]
    graphics [
      x 763.9133314131284
      y 524.6548881238912
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:ensembl:ENSG00000163513;urn:miriam:ensembl:ENSG00000106799"
      hgnc "NA"
      map_id "W23_12"
      name "e3254"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e3254"
      uniprot "NA"
    ]
    graphics [
      x 604.0133666052797
      y 1188.3146135803008
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "PUBMED:29945215;PUBMED:24627487;PUBMED:24438557"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_24"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ide33a2255"
      uniprot "NA"
    ]
    graphics [
      x 509.86941312182614
      y 1258.5402346580308
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:ensembl:ENSG00000124225;urn:miriam:ensembl:ENSG00000168675"
      hgnc "NA"
      map_id "W23_2"
      name "b0004"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b0004"
      uniprot "NA"
    ]
    graphics [
      x 504.19140916795
      y 1138.4300153379618
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:ensembl:ENSG00000175387"
      hgnc "NA"
      map_id "W23_10"
      name "df78d"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "df78d"
      uniprot "NA"
    ]
    graphics [
      x 409.56631396567593
      y 1198.2678619629642
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_1"
      name "Autophagosome_br_formation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "a5486"
      uniprot "NA"
    ]
    graphics [
      x 963.420474570245
      y 680.9703922122699
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "PUBMED:26165754"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_19"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id694a53fa"
      uniprot "NA"
    ]
    graphics [
      x 978.1181151978786
      y 553.1422085926055
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:ensembl:ENSG00000085978"
      hgnc "NA"
      map_id "W23_16"
      name "ATG16L1"
      node_subtype "GENE"
      node_type "species"
      org_id "f314c"
      uniprot "NA"
    ]
    graphics [
      x 927.8791415904136
      y 420.4155231240437
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_11"
      name "Neurodevelopmental_br_disorders"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "e1533"
      uniprot "NA"
    ]
    graphics [
      x 242.4275895960878
      y 507.5143117845021
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "PUBMED:31602316"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_21"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id6fa109c9"
      uniprot "NA"
    ]
    graphics [
      x 264.16120783299635
      y 381.1065639288722
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_26"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idf54d419"
      uniprot "NA"
    ]
    graphics [
      x 278.59546335417235
      y 115.04235490512173
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:ensembl:ENSG00000168675"
      hgnc "NA"
      map_id "W23_8"
      name "LDLRAD4"
      node_subtype "GENE"
      node_type "species"
      org_id "ca96b"
      uniprot "NA"
    ]
    graphics [
      x 375.67189940690014
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_23"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id7947a72f"
      uniprot "NA"
    ]
    graphics [
      x 190.84661870707293
      y 321.84783758877876
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:ensembl:ENSG00000168675"
      hgnc "NA"
      map_id "W23_9"
      name "LDLRAD4"
      node_subtype "GENE"
      node_type "species"
      org_id "d98b8"
      uniprot "NA"
    ]
    graphics [
      x 123.606117394217
      y 437.3823272008588
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "PUBMED:28888937"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_20"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id6cc9fb0d"
      uniprot "NA"
    ]
    graphics [
      x 461.4127683458709
      y 321.48696264763765
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:wikipathways:WP3874"
      hgnc "NA"
      map_id "W23_4"
      name "TGF_minus_B_space_signaling_br_pathway"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "bab41"
      uniprot "NA"
    ]
    graphics [
      x 439.5724584675105
      y 207.53168975736054
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_13"
      name "Blood_br_pressure"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "e3ffd"
      uniprot "NA"
    ]
    graphics [
      x 376.16614611608173
      y 479.16795609797794
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "PUBMED:19430479"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_25"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "idef60a322"
      uniprot "NA"
    ]
    graphics [
      x 359.97936301583746
      y 363.32405496631736
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "PUBMED:26165754"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_17"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id3b8d16d5"
      uniprot "NA"
    ]
    graphics [
      x 827.7003391286462
      y 307.6755895356128
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id4fe61d16"
      uniprot "NA"
    ]
    graphics [
      x 561.063209984525
      y 565.0679341677861
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:ensembl:ENSG00000069869"
      hgnc "NA"
      map_id "W23_6"
      name "NEDD4"
      node_subtype "GENE"
      node_type "species"
      org_id "c628c"
      uniprot "NA"
    ]
    graphics [
      x 580.2030714766886
      y 678.1736300424824
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 29
    source 1
    target 2
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "W23_7"
      target_id "W23_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 30
    source 2
    target 3
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_27"
      target_id "W23_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 31
    source 3
    target 4
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "W23_14"
      target_id "W23_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 32
    source 4
    target 5
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_28"
      target_id "W23_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 33
    source 6
    target 7
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "W23_5"
      target_id "W23_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 34
    source 7
    target 8
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_22"
      target_id "W23_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 35
    source 9
    target 10
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "W23_12"
      target_id "W23_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 36
    source 11
    target 10
    cd19dm [
      diagram "WP4904"
      edge_type "INHIBITION"
      source_id "W23_2"
      target_id "W23_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 37
    source 10
    target 12
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_24"
      target_id "W23_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 38
    source 13
    target 14
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "W23_1"
      target_id "W23_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 39
    source 14
    target 15
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_19"
      target_id "W23_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 40
    source 16
    target 17
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "W23_11"
      target_id "W23_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 41
    source 17
    target 3
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_21"
      target_id "W23_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 42
    source 3
    target 18
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "W23_14"
      target_id "W23_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 43
    source 18
    target 19
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_26"
      target_id "W23_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 44
    source 3
    target 20
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "W23_14"
      target_id "W23_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 45
    source 20
    target 21
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_23"
      target_id "W23_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 46
    source 3
    target 22
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "W23_14"
      target_id "W23_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 47
    source 22
    target 6
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_20"
      target_id "W23_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 48
    source 22
    target 23
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_20"
      target_id "W23_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 49
    source 24
    target 25
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "W23_13"
      target_id "W23_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 50
    source 25
    target 3
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_25"
      target_id "W23_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 51
    source 5
    target 26
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "W23_15"
      target_id "W23_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 52
    source 26
    target 15
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_17"
      target_id "W23_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 53
    source 6
    target 27
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "W23_5"
      target_id "W23_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 54
    source 27
    target 28
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_18"
      target_id "W23_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
