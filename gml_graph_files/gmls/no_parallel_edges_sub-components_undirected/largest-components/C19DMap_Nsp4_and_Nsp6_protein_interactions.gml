# generated with VANTED V2.8.2 at Fri Mar 04 10:04:39 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000133657;urn:miriam:uniprot:Q9H7F0;urn:miriam:hgnc.symbol:ATP13A3;urn:miriam:hgnc.symbol:ATP13A3;urn:miriam:hgnc:24113;urn:miriam:refseq:NM_024524;urn:miriam:ec-code:7.2.2.-;urn:miriam:ncbigene:79572;urn:miriam:ncbigene:79572"
      hgnc "HGNC_SYMBOL:ATP13A3"
      map_id "M14_116"
      name "ATP13A3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa222"
      uniprot "UNIPROT:Q9H7F0"
    ]
    graphics [
      x 447.79936492807735
      y 942.331528895045
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_78"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re66"
      uniprot "NA"
    ]
    graphics [
      x 447.7345176583218
      y 1047.4778577848733
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_31"
      name "P_minus_ATPase"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa80"
      uniprot "NA"
    ]
    graphics [
      x 261.2226527459833
      y 1033.5819239472614
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000133657;urn:miriam:uniprot:Q9H7F0;urn:miriam:hgnc.symbol:ATP13A3;urn:miriam:hgnc.symbol:ATP13A3;urn:miriam:ec-code:7.6.2.16;urn:miriam:hgnc:24113;urn:miriam:refseq:NM_024524;urn:miriam:ncbigene:79572;urn:miriam:ncbigene:79572"
      hgnc "HGNC_SYMBOL:ATP13A3"
      map_id "M14_20"
      name "P_minus_ATPase"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa41"
      uniprot "UNIPROT:Q9H7F0"
    ]
    graphics [
      x 687.4986961873792
      y 1056.562012104504
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_51"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re26"
      uniprot "NA"
    ]
    graphics [
      x 952.6456315573923
      y 1077.432554635307
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049"
      hgnc "NA"
      map_id "M14_129"
      name "Nsp6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa48"
      uniprot "NA"
    ]
    graphics [
      x 1215.1019136203558
      y 1084.2971367289651
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_129"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000133657;urn:miriam:uniprot:Q9H7F0;urn:miriam:hgnc.symbol:ATP13A3;urn:miriam:hgnc.symbol:ATP13A3;urn:miriam:ec-code:7.6.2.16;urn:miriam:hgnc:24113;urn:miriam:refseq:NM_024524;urn:miriam:ncbigene:79572;urn:miriam:ncbigene:79572;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049"
      hgnc "HGNC_SYMBOL:ATP13A3"
      map_id "M14_19"
      name "P_minus_ATPase:Nsp6"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa40"
      uniprot "UNIPROT:Q9H7F0"
    ]
    graphics [
      x 936.5953100605735
      y 1183.2254895850874
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re22"
      uniprot "NA"
    ]
    graphics [
      x 1378.5602806174818
      y 1306.3433933387328
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_48"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re23"
      uniprot "NA"
    ]
    graphics [
      x 1154.4399745826622
      y 1002.3987578739229
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_50"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re25"
      uniprot "NA"
    ]
    graphics [
      x 1402.3993039254754
      y 886.0834418923057
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_69"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re54"
      uniprot "NA"
    ]
    graphics [
      x 1050.4029543204404
      y 1314.6959471904486
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_55"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re31"
      uniprot "NA"
    ]
    graphics [
      x 1278.027986562674
      y 908.4864487661332
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_52"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re27"
      uniprot "NA"
    ]
    graphics [
      x 1472.0451434216134
      y 949.1288413292208
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859;PUBMED:29128390"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_67"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re51"
      uniprot "NA"
    ]
    graphics [
      x 880.477893612421
      y 981.557651529389
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049;urn:miriam:uniprot:Nsp3"
      hgnc "NA"
      map_id "M14_102"
      name "Nsp3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa162"
      uniprot "UNIPROT:Nsp3"
    ]
    graphics [
      x 647.4317559380704
      y 1168.6417358902734
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761"
      hgnc "NA"
      map_id "M14_127"
      name "Nsp4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa4"
      uniprot "NA"
    ]
    graphics [
      x 833.3825370089753
      y 629.0565797005324
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_127"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:doi:10.1016/j.virol.2017.07.019;urn:miriam:taxonomy:694009;urn:miriam:pubmed:29128390;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049;urn:miriam:uniprot:Nsp3;urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761"
      hgnc "NA"
      map_id "M14_17"
      name "Nsp3:Nsp4:Nsp6"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa39"
      uniprot "UNIPROT:Nsp3"
    ]
    graphics [
      x 804.4559176939194
      y 1096.614005166114
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_77"
      name "NA"
      node_subtype "POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "re64"
      uniprot "NA"
    ]
    graphics [
      x 755.6873829278765
      y 1215.0442760048081
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:obo.go:GO%3A0007009;urn:miriam:taxonomy:694009;urn:miriam:pubmed:23943763"
      hgnc "NA"
      map_id "M14_104"
      name "Plasma_space_membrane_space_organization"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa164"
      uniprot "NA"
    ]
    graphics [
      x 680.9425175645688
      y 1301.3095969289773
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_44"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re2"
      uniprot "NA"
    ]
    graphics [
      x 1145.748650145477
      y 517.7561741570609
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_94"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re9"
      uniprot "NA"
    ]
    graphics [
      x 921.013066109902
      y 891.4759828004474
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_73"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re6"
      uniprot "NA"
    ]
    graphics [
      x 752.2677928928601
      y 421.9848077667925
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_76"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re63"
      uniprot "NA"
    ]
    graphics [
      x 665.1023671287132
      y 425.91502217697763
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_40"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re16"
      uniprot "NA"
    ]
    graphics [
      x 925.8860682650178
      y 547.0968860448374
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re3"
      uniprot "NA"
    ]
    graphics [
      x 832.4559144351331
      y 491.30760080540944
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_42"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re18"
      uniprot "NA"
    ]
    graphics [
      x 657.0646263005025
      y 642.2807637186354
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:hgnc:32456;urn:miriam:ec-code:2.4.1.131;urn:miriam:ncbigene:440138;urn:miriam:ncbigene:440138;urn:miriam:uniprot:Q2TAA5;urn:miriam:hgnc.symbol:ALG11;urn:miriam:hgnc.symbol:ALG11;urn:miriam:pubmed:20080937;urn:miriam:refseq:NM_001004127;urn:miriam:ensembl:ENSG00000253710"
      hgnc "HGNC_SYMBOL:ALG11"
      map_id "M14_96"
      name "ALG11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa14"
      uniprot "UNIPROT:Q2TAA5"
    ]
    graphics [
      x 459.4500618205226
      y 636.5747178931018
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:20080937;urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761;urn:miriam:hgnc:32456;urn:miriam:ec-code:2.4.1.131;urn:miriam:ncbigene:440138;urn:miriam:ncbigene:440138;urn:miriam:uniprot:Q2TAA5;urn:miriam:hgnc.symbol:ALG11;urn:miriam:hgnc.symbol:ALG11;urn:miriam:pubmed:20080937;urn:miriam:refseq:NM_001004127;urn:miriam:ensembl:ENSG00000253710"
      hgnc "HGNC_SYMBOL:ALG11"
      map_id "M14_6"
      name "Nsp4_underscore_ALG11"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa15"
      uniprot "UNIPROT:Q2TAA5"
    ]
    graphics [
      x 770.1227919236542
      y 728.4912401519289
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_86"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re76"
      uniprot "NA"
    ]
    graphics [
      x 906.5299875422832
      y 803.3616545583361
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:20080937;urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761;urn:miriam:hgnc:32456;urn:miriam:ec-code:2.4.1.131;urn:miriam:ncbigene:440138;urn:miriam:ncbigene:440138;urn:miriam:uniprot:Q2TAA5;urn:miriam:hgnc.symbol:ALG11;urn:miriam:hgnc.symbol:ALG11;urn:miriam:pubmed:20080937;urn:miriam:refseq:NM_001004127;urn:miriam:ensembl:ENSG00000253710"
      hgnc "HGNC_SYMBOL:ALG11"
      map_id "M14_35"
      name "Nsp4_underscore_ALG11"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa93"
      uniprot "UNIPROT:Q2TAA5"
    ]
    graphics [
      x 1016.8761034196165
      y 907.7902386534167
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_87"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re77"
      uniprot "NA"
    ]
    graphics [
      x 315.05091710040415
      y 637.1750677538126
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:hgnc:32456;urn:miriam:ec-code:2.4.1.131;urn:miriam:ncbigene:440138;urn:miriam:ncbigene:440138;urn:miriam:uniprot:Q2TAA5;urn:miriam:hgnc.symbol:ALG11;urn:miriam:hgnc.symbol:ALG11;urn:miriam:pubmed:20080937;urn:miriam:refseq:NM_001004127;urn:miriam:ensembl:ENSG00000253710"
      hgnc "HGNC_SYMBOL:ALG11"
      map_id "M14_122"
      name "ALG11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa230"
      uniprot "UNIPROT:Q2TAA5"
    ]
    graphics [
      x 275.5914450407238
      y 745.8969562456039
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_131"
      name "sa5_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa5"
      uniprot "NA"
    ]
    graphics [
      x 868.2891992435466
      y 376.74440413952766
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_131"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:uniprot:Q9NVH1;urn:miriam:hgnc:25570;urn:miriam:ncbigene:55735;urn:miriam:ncbigene:55735;urn:miriam:pubmed:32353859;urn:miriam:pubmed:25997101;urn:miriam:ensembl:ENSG00000007923;urn:miriam:hgnc.symbol:DNAJC11;urn:miriam:refseq:NM_018198;urn:miriam:hgnc.symbol:DNAJC11"
      hgnc "HGNC_SYMBOL:DNAJC11"
      map_id "M14_95"
      name "DNAJC11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa13"
      uniprot "UNIPROT:Q9NVH1"
    ]
    graphics [
      x 1011.260863685835
      y 618.3351225889282
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:pubmed:25997101;urn:miriam:uniprot:Q9NVH1;urn:miriam:hgnc:25570;urn:miriam:ncbigene:55735;urn:miriam:ncbigene:55735;urn:miriam:pubmed:32353859;urn:miriam:pubmed:25997101;urn:miriam:ensembl:ENSG00000007923;urn:miriam:hgnc.symbol:DNAJC11;urn:miriam:refseq:NM_018198;urn:miriam:hgnc.symbol:DNAJC11;urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761"
      hgnc "HGNC_SYMBOL:DNAJC11"
      map_id "M14_3"
      name "Nsp4:DNAJC11"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa12"
      uniprot "UNIPROT:Q9NVH1"
    ]
    graphics [
      x 926.7245156615022
      y 658.3886304389683
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:uniprot:Q8N4H5;urn:miriam:uniprot:Q8N4H5;urn:miriam:hgnc:31369;urn:miriam:refseq:NM_001001790;urn:miriam:ensembl:ENSG00000175768;urn:miriam:hgnc.symbol:TOMM5;urn:miriam:hgnc.symbol:TOMM5;urn:miriam:ncbigene:401505;urn:miriam:ncbigene:401505;urn:miriam:ncbigene:10452;urn:miriam:ensembl:ENSG00000130204;urn:miriam:ncbigene:10452;urn:miriam:refseq:NM_001128916;urn:miriam:uniprot:O96008;urn:miriam:uniprot:O96008;urn:miriam:hgnc.symbol:TOMM40;urn:miriam:hgnc.symbol:TOMM40;urn:miriam:hgnc:18001;urn:miriam:ensembl:ENSG00000173726;urn:miriam:refseq:NM_014765;urn:miriam:uniprot:Q15388;urn:miriam:uniprot:Q15388;urn:miriam:ncbigene:9804;urn:miriam:ncbigene:9804;urn:miriam:hgnc.symbol:TOMM20;urn:miriam:hgnc.symbol:TOMM20;urn:miriam:hgnc:20947;urn:miriam:refseq:NM_020243;urn:miriam:uniprot:Q9NS69;urn:miriam:uniprot:Q9NS69;urn:miriam:ncbigene:56993;urn:miriam:ncbigene:56993;urn:miriam:hgnc:18002;urn:miriam:ensembl:ENSG00000100216;urn:miriam:hgnc.symbol:TOMM22;urn:miriam:hgnc.symbol:TOMM22;urn:miriam:refseq:NM_014820;urn:miriam:uniprot:O94826;urn:miriam:uniprot:O94826;urn:miriam:hgnc:11985;urn:miriam:hgnc.symbol:TOMM70;urn:miriam:hgnc.symbol:TOMM70;urn:miriam:ncbigene:9868;urn:miriam:ncbigene:9868;urn:miriam:ensembl:ENSG00000154174;urn:miriam:hgnc:21648;urn:miriam:hgnc.symbol:TOMM7;urn:miriam:hgnc.symbol:TOMM7;urn:miriam:refseq:NM_019059;urn:miriam:ncbigene:54543;urn:miriam:ncbigene:54543;urn:miriam:uniprot:Q9P0U1;urn:miriam:uniprot:Q9P0U1;urn:miriam:ensembl:ENSG00000196683"
      hgnc "HGNC_SYMBOL:TOMM5;HGNC_SYMBOL:TOMM40;HGNC_SYMBOL:TOMM20;HGNC_SYMBOL:TOMM22;HGNC_SYMBOL:TOMM70;HGNC_SYMBOL:TOMM7"
      map_id "M14_10"
      name "TOM_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa24"
      uniprot "UNIPROT:Q8N4H5;UNIPROT:O96008;UNIPROT:Q15388;UNIPROT:Q9NS69;UNIPROT:O94826;UNIPROT:Q9P0U1"
    ]
    graphics [
      x 644.2673986191426
      y 302.2558443982218
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761"
      hgnc "NA"
      map_id "M14_112"
      name "Nsp4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa217"
      uniprot "NA"
    ]
    graphics [
      x 506.639444353149
      y 297.66144696285005
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_58"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re37"
      uniprot "NA"
    ]
    graphics [
      x 352.2672561544723
      y 246.1841899480063
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000142444;urn:miriam:hgnc:25152;urn:miriam:hgnc.symbol:TIMM29;urn:miriam:hgnc.symbol:TIMM29;urn:miriam:refseq:NM_138358;urn:miriam:uniprot:Q9BSF4;urn:miriam:uniprot:Q9BSF4;urn:miriam:ncbigene:90580;urn:miriam:ncbigene:90580;urn:miriam:hgnc:4022;urn:miriam:uniprot:Q9Y5J6;urn:miriam:uniprot:Q9Y5J6;urn:miriam:hgnc.symbol:TIMM10B;urn:miriam:hgnc.symbol:TIMM10B;urn:miriam:ensembl:ENSG00000132286;urn:miriam:ncbigene:26515;urn:miriam:ncbigene:26515;urn:miriam:refseq:NM_012192;urn:miriam:uniprot:Q9Y584;urn:miriam:uniprot:Q9Y584;urn:miriam:hgnc.symbol:TIMM22;urn:miriam:hgnc.symbol:TIMM22;urn:miriam:hgnc:17317;urn:miriam:ncbigene:29928;urn:miriam:ncbigene:29928;urn:miriam:ensembl:ENSG00000177370;urn:miriam:refseq:NM_013337;urn:miriam:hgnc:11814;urn:miriam:uniprot:P62072;urn:miriam:uniprot:P62072;urn:miriam:ncbigene:26519;urn:miriam:ncbigene:26519;urn:miriam:ensembl:ENSG00000134809;urn:miriam:hgnc.symbol:TIMM10;urn:miriam:hgnc.symbol:TIMM10;urn:miriam:refseq:NM_012456;urn:miriam:refseq:NM_001304485;urn:miriam:ncbigene:26520;urn:miriam:ncbigene:26520;urn:miriam:hgnc.symbol:TIMM9;urn:miriam:ensembl:ENSG00000100575;urn:miriam:hgnc.symbol:TIMM9;urn:miriam:hgnc:11819;urn:miriam:uniprot:Q9Y5J7;urn:miriam:uniprot:Q9Y5J7"
      hgnc "HGNC_SYMBOL:TIMM29;HGNC_SYMBOL:TIMM10B;HGNC_SYMBOL:TIMM22;HGNC_SYMBOL:TIMM10;HGNC_SYMBOL:TIMM9"
      map_id "M14_12"
      name "TIM_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa29"
      uniprot "UNIPROT:Q9BSF4;UNIPROT:Q9Y5J6;UNIPROT:Q9Y584;UNIPROT:P62072;UNIPROT:Q9Y5J7"
    ]
    graphics [
      x 280.8729404334931
      y 369.14861739588224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:uniprot:Q9Y584;urn:miriam:uniprot:Q9Y584;urn:miriam:hgnc.symbol:TIMM22;urn:miriam:hgnc.symbol:TIMM22;urn:miriam:hgnc:17317;urn:miriam:ncbigene:29928;urn:miriam:ncbigene:29928;urn:miriam:ensembl:ENSG00000177370;urn:miriam:refseq:NM_013337;urn:miriam:hgnc:4022;urn:miriam:uniprot:Q9Y5J6;urn:miriam:uniprot:Q9Y5J6;urn:miriam:hgnc.symbol:TIMM10B;urn:miriam:hgnc.symbol:TIMM10B;urn:miriam:ensembl:ENSG00000132286;urn:miriam:ncbigene:26515;urn:miriam:ncbigene:26515;urn:miriam:refseq:NM_012192;urn:miriam:ensembl:ENSG00000142444;urn:miriam:hgnc:25152;urn:miriam:hgnc.symbol:TIMM29;urn:miriam:hgnc.symbol:TIMM29;urn:miriam:refseq:NM_138358;urn:miriam:uniprot:Q9BSF4;urn:miriam:uniprot:Q9BSF4;urn:miriam:ncbigene:90580;urn:miriam:ncbigene:90580;urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761;urn:miriam:hgnc:11814;urn:miriam:uniprot:P62072;urn:miriam:uniprot:P62072;urn:miriam:ncbigene:26519;urn:miriam:ncbigene:26519;urn:miriam:ensembl:ENSG00000134809;urn:miriam:hgnc.symbol:TIMM10;urn:miriam:hgnc.symbol:TIMM10;urn:miriam:refseq:NM_012456;urn:miriam:refseq:NM_001304485;urn:miriam:ncbigene:26520;urn:miriam:ncbigene:26520;urn:miriam:hgnc.symbol:TIMM9;urn:miriam:ensembl:ENSG00000100575;urn:miriam:hgnc.symbol:TIMM9;urn:miriam:hgnc:11819;urn:miriam:uniprot:Q9Y5J7;urn:miriam:uniprot:Q9Y5J7"
      hgnc "HGNC_SYMBOL:TIMM22;HGNC_SYMBOL:TIMM10B;HGNC_SYMBOL:TIMM29;HGNC_SYMBOL:TIMM10;HGNC_SYMBOL:TIMM9"
      map_id "M14_11"
      name "TIM_space_complex:Nsp4"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa27"
      uniprot "UNIPROT:Q9Y584;UNIPROT:Q9Y5J6;UNIPROT:Q9BSF4;UNIPROT:P62072;UNIPROT:Q9Y5J7"
    ]
    graphics [
      x 359.07159232555955
      y 359.28038292266143
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:uniprot:P14735;urn:miriam:ncbigene:3416;urn:miriam:ncbigene:3416;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1210/mend-4-8-1125;urn:miriam:hgnc:5381;urn:miriam:refseq:NM_004969;urn:miriam:ec-code:3.4.24.56;urn:miriam:ensembl:ENSG00000119912;urn:miriam:hgnc.symbol:IDE;urn:miriam:hgnc.symbol:IDE"
      hgnc "HGNC_SYMBOL:IDE"
      map_id "M14_136"
      name "IDE"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa8"
      uniprot "UNIPROT:P14735"
    ]
    graphics [
      x 588.5101037414901
      y 500.15099556350515
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:taxonomy:10116;urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761;urn:miriam:uniprot:P14735;urn:miriam:ncbigene:3416;urn:miriam:ncbigene:3416;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1210/mend-4-8-1125;urn:miriam:taxonomy:10116;urn:miriam:hgnc:5381;urn:miriam:refseq:NM_004969;urn:miriam:ec-code:3.4.24.56;urn:miriam:ensembl:ENSG00000119912;urn:miriam:hgnc.symbol:IDE;urn:miriam:hgnc.symbol:IDE"
      hgnc "HGNC_SYMBOL:IDE"
      map_id "M14_9"
      name "Nsp4:IDE"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa2"
      uniprot "UNIPROT:P14735"
    ]
    graphics [
      x 804.7817416200957
      y 221.94368068909102
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_39"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10"
      uniprot "NA"
    ]
    graphics [
      x 854.5909557595429
      y 81.9135836935792
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_80"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re7"
      uniprot "NA"
    ]
    graphics [
      x 935.4632864699319
      y 192.5626657954174
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:taxonomy:10116;urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761;urn:miriam:uniprot:P14735;urn:miriam:ncbigene:3416;urn:miriam:ncbigene:3416;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1210/mend-4-8-1125;urn:miriam:taxonomy:10116;urn:miriam:hgnc:5381;urn:miriam:refseq:NM_004969;urn:miriam:ec-code:3.4.24.56;urn:miriam:ensembl:ENSG00000119912;urn:miriam:hgnc.symbol:IDE;urn:miriam:hgnc.symbol:IDE"
      hgnc "HGNC_SYMBOL:IDE"
      map_id "M14_18"
      name "Nsp4:IDE"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa4"
      uniprot "UNIPROT:P14735"
    ]
    graphics [
      x 1041.6783752006236
      y 272.87258918819145
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761;urn:miriam:uniprot:P14735;urn:miriam:ncbigene:3416;urn:miriam:ncbigene:3416;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1210/mend-4-8-1125;urn:miriam:taxonomy:10116;urn:miriam:hgnc:5381;urn:miriam:refseq:NM_004969;urn:miriam:ec-code:3.4.24.56;urn:miriam:ensembl:ENSG00000119912;urn:miriam:hgnc.symbol:IDE;urn:miriam:hgnc.symbol:IDE"
      hgnc "HGNC_SYMBOL:IDE"
      map_id "M14_28"
      name "Nsp4_underscore_IDE"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa5"
      uniprot "UNIPROT:P14735"
    ]
    graphics [
      x 982.0123021310793
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "PUBMED:9830016"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_60"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re40"
      uniprot "NA"
    ]
    graphics [
      x 455.34806563733036
      y 462.5240326255555
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_82"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re72"
      uniprot "NA"
    ]
    graphics [
      x 542.9221560683134
      y 632.5818457805735
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:uniprot:P14735;urn:miriam:ncbigene:3416;urn:miriam:ncbigene:3416;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1210/mend-4-8-1125;urn:miriam:hgnc:5381;urn:miriam:refseq:NM_004969;urn:miriam:ec-code:3.4.24.56;urn:miriam:ensembl:ENSG00000119912;urn:miriam:hgnc.symbol:IDE;urn:miriam:hgnc.symbol:IDE"
      hgnc "HGNC_SYMBOL:IDE"
      map_id "M14_119"
      name "IDE"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa225"
      uniprot "UNIPROT:P14735"
    ]
    graphics [
      x 547.2918667564428
      y 776.2843938816017
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:uniprot:P14735;urn:miriam:ncbigene:3416;urn:miriam:ncbigene:3416;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1210/mend-4-8-1125;urn:miriam:taxonomy:10116;urn:miriam:hgnc:5381;urn:miriam:refseq:NM_004969;urn:miriam:ec-code:3.4.24.56;urn:miriam:ensembl:ENSG00000119912;urn:miriam:hgnc.symbol:IDE;urn:miriam:hgnc.symbol:IDE"
      hgnc "HGNC_SYMBOL:IDE"
      map_id "M14_97"
      name "IDE"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa154"
      uniprot "UNIPROT:P14735"
    ]
    graphics [
      x 551.7612807890962
      y 415.9993278574651
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:NUP210;urn:miriam:hgnc.symbol:NUP210;urn:miriam:pubmed:14517331;urn:miriam:uniprot:Q8TEM1;urn:miriam:ncbigene:23225;urn:miriam:ensembl:ENSG00000132182;urn:miriam:ncbigene:23225;urn:miriam:hgnc:30052;urn:miriam:refseq:NM_024923"
      hgnc "HGNC_SYMBOL:NUP210"
      map_id "M14_137"
      name "NUP210"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa9"
      uniprot "UNIPROT:Q8TEM1"
    ]
    graphics [
      x 1073.24316342677
      y 1144.647842675064
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_137"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:14517331;urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761;urn:miriam:hgnc.symbol:NUP210;urn:miriam:hgnc.symbol:NUP210;urn:miriam:pubmed:14517331;urn:miriam:uniprot:Q8TEM1;urn:miriam:ncbigene:23225;urn:miriam:ensembl:ENSG00000132182;urn:miriam:ncbigene:23225;urn:miriam:hgnc:30052;urn:miriam:refseq:NM_024923"
      hgnc "HGNC_SYMBOL:NUP210"
      map_id "M14_5"
      name "Nsp4:NUP210"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa14"
      uniprot "UNIPROT:Q8TEM1"
    ]
    graphics [
      x 783.8035778011174
      y 891.3180115511688
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "PUBMED:14517331"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_41"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re17"
      uniprot "NA"
    ]
    graphics [
      x 664.2507575898519
      y 892.3872124507922
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:14517331;urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:BCD58761;urn:miriam:hgnc.symbol:NUP210;urn:miriam:hgnc.symbol:NUP210;urn:miriam:pubmed:14517331;urn:miriam:uniprot:Q8TEM1;urn:miriam:ncbigene:23225;urn:miriam:ensembl:ENSG00000132182;urn:miriam:ncbigene:23225;urn:miriam:hgnc:30052;urn:miriam:refseq:NM_024923"
      hgnc "HGNC_SYMBOL:NUP210"
      map_id "M14_4"
      name "Nsp4:NUP210"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa13"
      uniprot "UNIPROT:Q8TEM1"
    ]
    graphics [
      x 628.8996805105506
      y 793.7536823129705
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_68"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re53"
      uniprot "NA"
    ]
    graphics [
      x 1164.886956297715
      y 1352.2616327818298
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "PUBMED:14517331"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_81"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re71"
      uniprot "NA"
    ]
    graphics [
      x 1177.767439829056
      y 1259.5333391554102
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:NUP210;urn:miriam:hgnc.symbol:NUP210;urn:miriam:pubmed:14517331;urn:miriam:uniprot:Q8TEM1;urn:miriam:ncbigene:23225;urn:miriam:ensembl:ENSG00000132182;urn:miriam:ncbigene:23225;urn:miriam:hgnc:30052;urn:miriam:refseq:NM_024923"
      hgnc "HGNC_SYMBOL:NUP210"
      map_id "M14_118"
      name "NUP210"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa224"
      uniprot "UNIPROT:Q8TEM1"
    ]
    graphics [
      x 1276.2712425171949
      y 1323.6980841766851
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859"
      hgnc "NA"
      map_id "M14_105"
      name "Selinexor"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa187"
      uniprot "NA"
    ]
    graphics [
      x 1202.2086252934412
      y 1493.044176719062
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:hgnc.symbol:NUP210;urn:miriam:hgnc.symbol:NUP210;urn:miriam:pubmed:14517331;urn:miriam:uniprot:Q8TEM1;urn:miriam:ncbigene:23225;urn:miriam:ensembl:ENSG00000132182;urn:miriam:ncbigene:23225;urn:miriam:hgnc:30052;urn:miriam:refseq:NM_024923;urn:miriam:pubmed:32353859"
      hgnc "HGNC_SYMBOL:NUP210"
      map_id "M14_23"
      name "NUP210:Selinexor"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa44"
      uniprot "UNIPROT:Q8TEM1"
    ]
    graphics [
      x 1263.4430644609122
      y 1436.5853562320265
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859"
      hgnc "NA"
      map_id "M14_126"
      name "Nsp4_space_(_plus_)"
      node_subtype "RNA"
      node_type "species"
      org_id "sa3"
      uniprot "NA"
    ]
    graphics [
      x 1433.7797124387648
      y 464.1010083755559
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_38"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1"
      uniprot "NA"
    ]
    graphics [
      x 1635.3222299958265
      y 489.4348490440336
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_43"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re19"
      uniprot "NA"
    ]
    graphics [
      x 1627.7160215974463
      y 412.78584228823365
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_59"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re4"
      uniprot "NA"
    ]
    graphics [
      x 1474.1254307174884
      y 590.9307969281881
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_135"
      name "sa2_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa6"
      uniprot "NA"
    ]
    graphics [
      x 1371.7595898111379
      y 672.6456336335821
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859"
      hgnc "NA"
      map_id "M14_115"
      name "Nsp4_space_(_minus_)"
      node_subtype "RNA"
      node_type "species"
      org_id "sa220"
      uniprot "NA"
    ]
    graphics [
      x 1793.2716583537772
      y 481.1787630279626
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re20"
      uniprot "NA"
    ]
    graphics [
      x 1939.2704034356507
      y 492.98071279165396
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_99"
      name "sa3_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa16"
      uniprot "NA"
    ]
    graphics [
      x 1970.0251851541677
      y 609.7094209301423
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_63"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re47"
      uniprot "NA"
    ]
    graphics [
      x 557.6392900557727
      y 1347.2133279292611
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_66"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re50"
      uniprot "NA"
    ]
    graphics [
      x 482.3626029425078
      y 1234.9921273988382
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_103"
      name "sa171_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa163"
      uniprot "NA"
    ]
    graphics [
      x 368.3041476467515
      y 1310.6841885539554
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859"
      hgnc "NA"
      map_id "M14_100"
      name "Nsp3_space_(_plus_)"
      node_subtype "RNA"
      node_type "species"
      org_id "sa160"
      uniprot "NA"
    ]
    graphics [
      x 470.91413615944725
      y 1495.2621978622624
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_61"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re45"
      uniprot "NA"
    ]
    graphics [
      x 565.3922261863128
      y 1588.835410084444
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_65"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re49"
      uniprot "NA"
    ]
    graphics [
      x 462.9557979407174
      y 1331.0440563507464
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_62"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re46"
      uniprot "NA"
    ]
    graphics [
      x 522.4100470869157
      y 1649.3146286921888
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859"
      hgnc "NA"
      map_id "M14_114"
      name "Nsp3_space_(_minus_)"
      node_subtype "RNA"
      node_type "species"
      org_id "sa219"
      uniprot "NA"
    ]
    graphics [
      x 688.6486269346574
      y 1652.0270609350541
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_64"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re48"
      uniprot "NA"
    ]
    graphics [
      x 858.1932854765118
      y 1651.747215804482
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_98"
      name "sa169_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa159"
      uniprot "NA"
    ]
    graphics [
      x 1010.5055268476501
      y 1588.8512972607798
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_101"
      name "sa170_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa161"
      uniprot "NA"
    ]
    graphics [
      x 507.411615768301
      y 1161.1687907846
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:10406945;urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1074/jbc.272.43.27107;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:uniprot:Q99720"
      hgnc "HGNC_SYMBOL:SIGMAR1"
      map_id "M14_133"
      name "SIGMAR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa53"
      uniprot "UNIPROT:Q99720"
    ]
    graphics [
      x 1539.4741099305202
      y 721.9453655805434
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_133"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:pubmed:10406945;urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1074/jbc.272.43.27107;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:uniprot:Q99720;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049"
      hgnc "HGNC_SYMBOL:SIGMAR1"
      map_id "M14_7"
      name "Nsp6:SIGMAR1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa18"
      uniprot "UNIPROT:Q99720"
    ]
    graphics [
      x 1673.095764190402
      y 938.9998895380701
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      annotation "PUBMED:10406945"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_53"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re28"
      uniprot "NA"
    ]
    graphics [
      x 1671.2767777291401
      y 799.8283045443691
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      annotation "PUBMED:10406945"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_85"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re75"
      uniprot "NA"
    ]
    graphics [
      x 1838.227119700005
      y 890.3910111685498
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:10406945;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049;urn:miriam:pubmed:10406945;urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1074/jbc.272.43.27107;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:uniprot:Q99720"
      hgnc "HGNC_SYMBOL:SIGMAR1"
      map_id "M14_34"
      name "Nsp6:SIGMAR1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa92"
      uniprot "UNIPROT:Q99720"
    ]
    graphics [
      x 1961.01449603836
      y 862.4029225551567
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:10406945;urn:miriam:pubmed:10406945;urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1074/jbc.272.43.27107;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:uniprot:Q99720;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049"
      hgnc "HGNC_SYMBOL:SIGMAR1"
      map_id "M14_8"
      name "Nsp6:SIGMAR1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa19"
      uniprot "UNIPROT:Q99720"
    ]
    graphics [
      x 1553.3916396505829
      y 821.3082650851107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      annotation "PUBMED:10406945"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_83"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re73"
      uniprot "NA"
    ]
    graphics [
      x 1691.9208630673297
      y 719.5253058380378
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      annotation "PUBMED:10406945"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_84"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re74"
      uniprot "NA"
    ]
    graphics [
      x 1424.87702386204
      y 633.0576648669946
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_75"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re62"
      uniprot "NA"
    ]
    graphics [
      x 1553.5344133858432
      y 523.4996535569397
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_111"
      name "Several_space_drugs"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa212"
      uniprot "NA"
    ]
    graphics [
      x 1534.660645462732
      y 364.7867796027403
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:pubmed:10406945;urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1074/jbc.272.43.27107;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:uniprot:Q99720"
      hgnc "HGNC_SYMBOL:SIGMAR1"
      map_id "M14_30"
      name "SIGMAR1:Drugs"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa53"
      uniprot "UNIPROT:Q99720"
    ]
    graphics [
      x 1498.3553060252887
      y 415.04947080226816
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:10406945;urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1074/jbc.272.43.27107;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:uniprot:Q99720"
      hgnc "HGNC_SYMBOL:SIGMAR1"
      map_id "M14_121"
      name "SIGMAR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa227"
      uniprot "UNIPROT:Q99720"
    ]
    graphics [
      x 1330.1347423836535
      y 532.6701256871947
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:10406945;urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:pubmed:32353859;urn:miriam:doi:10.1074/jbc.272.43.27107;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:uniprot:Q99720"
      hgnc "HGNC_SYMBOL:SIGMAR1"
      map_id "M14_120"
      name "SIGMAR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa226"
      uniprot "UNIPROT:Q99720"
    ]
    graphics [
      x 1680.0033928642151
      y 853.1116487648209
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:uniprot:Q15904;urn:miriam:ncbigene:537;urn:miriam:ensembl:ENSG00000071553;urn:miriam:ncbigene:537;urn:miriam:hgnc:868;urn:miriam:refseq:NM_001183;urn:miriam:uniprot:Q15904;urn:miriam:pubmed:27231034;urn:miriam:hgnc.symbol:ATP6AP1;urn:miriam:hgnc.symbol:ATP6AP1"
      hgnc "HGNC_SYMBOL:ATP6AP1"
      map_id "M14_13"
      name "V_minus_ATPase"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa30"
      uniprot "UNIPROT:Q15904"
    ]
    graphics [
      x 1408.2497818975592
      y 794.2783721302916
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:uniprot:Q15904;urn:miriam:ncbigene:537;urn:miriam:ensembl:ENSG00000071553;urn:miriam:ncbigene:537;urn:miriam:hgnc:868;urn:miriam:refseq:NM_001183;urn:miriam:uniprot:Q15904;urn:miriam:pubmed:27231034;urn:miriam:hgnc.symbol:ATP6AP1;urn:miriam:hgnc.symbol:ATP6AP1;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049"
      hgnc "HGNC_SYMBOL:ATP6AP1"
      map_id "M14_14"
      name "V_minus_ATPase:Nsp6"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa32"
      uniprot "UNIPROT:Q15904"
    ]
    graphics [
      x 1211.5826818323596
      y 775.9834509694696
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_91"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re81"
      uniprot "NA"
    ]
    graphics [
      x 1147.887350052261
      y 650.2198808190456
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:uniprot:Q15904;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049;urn:miriam:ncbigene:537;urn:miriam:ensembl:ENSG00000071553;urn:miriam:ncbigene:537;urn:miriam:hgnc:868;urn:miriam:refseq:NM_001183;urn:miriam:uniprot:Q15904;urn:miriam:pubmed:27231034;urn:miriam:hgnc.symbol:ATP6AP1;urn:miriam:hgnc.symbol:ATP6AP1"
      hgnc "HGNC_SYMBOL:ATP6AP1"
      map_id "M14_16"
      name "V_minus_ATPase:Nsp6"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa34"
      uniprot "UNIPROT:Q15904"
    ]
    graphics [
      x 1073.940222376074
      y 535.0039280631884
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_89"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re79"
      uniprot "NA"
    ]
    graphics [
      x 1577.5430223273286
      y 929.0601647654984
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_90"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re80"
      uniprot "NA"
    ]
    graphics [
      x 1356.3469938798446
      y 591.8425220983308
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:uniprot:Q15904;urn:miriam:ncbigene:537;urn:miriam:ensembl:ENSG00000071553;urn:miriam:ncbigene:537;urn:miriam:hgnc:868;urn:miriam:refseq:NM_001183;urn:miriam:uniprot:Q15904;urn:miriam:pubmed:27231034;urn:miriam:hgnc.symbol:ATP6AP1;urn:miriam:hgnc.symbol:ATP6AP1"
      hgnc "HGNC_SYMBOL:ATP6AP1"
      map_id "M14_15"
      name "V_minus_ATPase"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa33"
      uniprot "UNIPROT:Q15904"
    ]
    graphics [
      x 1331.3341966816506
      y 442.4843684345683
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:obo.go:GO%3A0046611"
      hgnc "NA"
      map_id "M14_36"
      name "V_minus_type_space_proton_space_ATPase"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa94"
      uniprot "NA"
    ]
    graphics [
      x 1657.2747742336587
      y 1084.8167022105204
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:ncbigene:537;urn:miriam:ensembl:ENSG00000071553;urn:miriam:ncbigene:537;urn:miriam:hgnc:868;urn:miriam:refseq:NM_001183;urn:miriam:uniprot:Q15904;urn:miriam:pubmed:27231034;urn:miriam:hgnc.symbol:ATP6AP1;urn:miriam:hgnc.symbol:ATP6AP1"
      hgnc "HGNC_SYMBOL:ATP6AP1"
      map_id "M14_117"
      name "ATP6AP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa223"
      uniprot "UNIPROT:Q15904"
    ]
    graphics [
      x 1765.94654684316
      y 965.0408819121074
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_74"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re60"
      uniprot "NA"
    ]
    graphics [
      x 1932.9804604486562
      y 1002.6803004841902
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_106"
      name "Bafilomycin_space_A1"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa188"
      uniprot "NA"
    ]
    graphics [
      x 1847.2786248765665
      y 1074.560856331
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:ncbigene:537;urn:miriam:ensembl:ENSG00000071553;urn:miriam:ncbigene:537;urn:miriam:hgnc:868;urn:miriam:refseq:NM_001183;urn:miriam:uniprot:Q15904;urn:miriam:pubmed:27231034;urn:miriam:hgnc.symbol:ATP6AP1;urn:miriam:hgnc.symbol:ATP6AP1"
      hgnc "HGNC_SYMBOL:ATP6AP1"
      map_id "M14_29"
      name "ATP6AP1:Bafilomycin_space_A1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa50"
      uniprot "UNIPROT:Q15904"
    ]
    graphics [
      x 1969.0660510629145
      y 1108.9896205929094
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      annotation "PUBMED:22335796"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_88"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re78"
      uniprot "NA"
    ]
    graphics [
      x 1745.35330060862
      y 1204.6090184764216
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "M14_124"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa234"
      uniprot "NA"
    ]
    graphics [
      x 1775.4532646435023
      y 1317.039894204275
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_124"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "M14_123"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa233"
      uniprot "NA"
    ]
    graphics [
      x 1857.1281109804381
      y 1252.006683528033
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:uniprot:Q8IY34;urn:miriam:pubmed:32353859;urn:miriam:ncbigene:55117;urn:miriam:ncbigene:55117;urn:miriam:hgnc.symbol:SLC15A3;urn:miriam:hgnc:13621;urn:miriam:refseq:NM_182767;urn:miriam:uniprot:Q9H2J7;urn:miriam:ncbigene:51296;urn:miriam:uniprot:Q9H2J7;urn:miriam:hgnc.symbol:SLC6A15;urn:miriam:hgnc.symbol:SLC6A15;urn:miriam:refseq:NM_018057;urn:miriam:ensembl:ENSG00000072041"
      hgnc "HGNC_SYMBOL:SLC15A3;HGNC_SYMBOL:SLC6A15"
      map_id "M14_107"
      name "SLC6A15"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa191"
      uniprot "UNIPROT:Q8IY34;UNIPROT:Q9H2J7"
    ]
    graphics [
      x 901.7864383634895
      y 1548.9181532706607
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:uniprot:Q8IY34;urn:miriam:pubmed:32353859;urn:miriam:ncbigene:55117;urn:miriam:ncbigene:55117;urn:miriam:hgnc.symbol:SLC15A3;urn:miriam:hgnc:13621;urn:miriam:refseq:NM_182767;urn:miriam:uniprot:Q9H2J7;urn:miriam:ncbigene:51296;urn:miriam:uniprot:Q9H2J7;urn:miriam:hgnc.symbol:SLC6A15;urn:miriam:hgnc.symbol:SLC6A15;urn:miriam:refseq:NM_018057;urn:miriam:ensembl:ENSG00000072041;urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049"
      hgnc "HGNC_SYMBOL:SLC15A3;HGNC_SYMBOL:SLC6A15"
      map_id "M14_24"
      name "SLC6A15:Nsp6"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa45"
      uniprot "UNIPROT:Q8IY34;UNIPROT:Q9H2J7"
    ]
    graphics [
      x 934.6759286867153
      y 1306.7345198595058
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_71"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re56"
      uniprot "NA"
    ]
    graphics [
      x 841.0608309302872
      y 1739.4132427065329
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_70"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re55"
      uniprot "NA"
    ]
    graphics [
      x 998.0138442145843
      y 1519.6895836765657
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_72"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re57"
      uniprot "NA"
    ]
    graphics [
      x 755.7194653100387
      y 1568.593557371687
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:123134323"
      hgnc "NA"
      map_id "M14_110"
      name "Loratadine"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa200"
      uniprot "NA"
    ]
    graphics [
      x 810.5617747040196
      y 1486.4988144201623
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:123134323;urn:miriam:uniprot:Q8IY34;urn:miriam:pubmed:32353859;urn:miriam:ncbigene:55117;urn:miriam:ncbigene:55117;urn:miriam:hgnc.symbol:SLC15A3;urn:miriam:hgnc:13621;urn:miriam:refseq:NM_182767;urn:miriam:uniprot:Q9H2J7;urn:miriam:ncbigene:51296;urn:miriam:uniprot:Q9H2J7;urn:miriam:hgnc.symbol:SLC6A15;urn:miriam:hgnc.symbol:SLC6A15;urn:miriam:refseq:NM_018057;urn:miriam:ensembl:ENSG00000072041"
      hgnc "HGNC_SYMBOL:SLC15A3;HGNC_SYMBOL:SLC6A15"
      map_id "M14_27"
      name "SLC6A15:Loratadine"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa49"
      uniprot "UNIPROT:Q8IY34;UNIPROT:Q9H2J7"
    ]
    graphics [
      x 725.5656354799536
      y 1443.1684433648782
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:taxonomy:2697049"
      hgnc "NA"
      map_id "M14_108"
      name "Orf9c"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa192"
      uniprot "NA"
    ]
    graphics [
      x 1092.2192227708763
      y 1481.7917471444334
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049;urn:miriam:uniprot:Q8IY34;urn:miriam:pubmed:32353859;urn:miriam:ncbigene:55117;urn:miriam:ncbigene:55117;urn:miriam:hgnc.symbol:SLC15A3;urn:miriam:hgnc:13621;urn:miriam:refseq:NM_182767;urn:miriam:uniprot:Q9H2J7;urn:miriam:ncbigene:51296;urn:miriam:uniprot:Q9H2J7;urn:miriam:hgnc.symbol:SLC6A15;urn:miriam:hgnc.symbol:SLC6A15;urn:miriam:refseq:NM_018057;urn:miriam:ensembl:ENSG00000072041"
      hgnc "HGNC_SYMBOL:SLC15A3;HGNC_SYMBOL:SLC6A15"
      map_id "M14_25"
      name "SLC6A15:Orf9c"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa47"
      uniprot "UNIPROT:Q8IY34;UNIPROT:Q9H2J7"
    ]
    graphics [
      x 1002.7078252679917
      y 1409.14357389437
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:taxonomy:2697049;urn:miriam:uniprot:M"
      hgnc "NA"
      map_id "M14_109"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa193"
      uniprot "UNIPROT:M"
    ]
    graphics [
      x 802.0451429768952
      y 1859.1920902738875
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:uniprot:Q8IY34;urn:miriam:pubmed:32353859;urn:miriam:ncbigene:55117;urn:miriam:ncbigene:55117;urn:miriam:hgnc.symbol:SLC15A3;urn:miriam:hgnc:13621;urn:miriam:refseq:NM_182767;urn:miriam:uniprot:Q9H2J7;urn:miriam:ncbigene:51296;urn:miriam:uniprot:Q9H2J7;urn:miriam:hgnc.symbol:SLC6A15;urn:miriam:hgnc.symbol:SLC6A15;urn:miriam:refseq:NM_018057;urn:miriam:ensembl:ENSG00000072041;urn:miriam:taxonomy:2697049;urn:miriam:uniprot:M"
      hgnc "HGNC_SYMBOL:SLC15A3;HGNC_SYMBOL:SLC6A15"
      map_id "M14_26"
      name "SLC6A15:M"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa48"
      uniprot "UNIPROT:Q8IY34;UNIPROT:Q9H2J7;UNIPROT:M"
    ]
    graphics [
      x 930.1404008172201
      y 1795.418239679248
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:uniprot:O75964;urn:miriam:hgnc:14247;urn:miriam:refseq:NM_006476;urn:miriam:hgnc.symbol:ATP5MG;urn:miriam:hgnc.symbol:ATP5MG;urn:miriam:ensembl:ENSG00000167283;urn:miriam:ncbigene:10632;urn:miriam:ncbigene:10632;urn:miriam:uniprot:O75964"
      hgnc "HGNC_SYMBOL:ATP5MG"
      map_id "M14_21"
      name "F_minus_ATPase"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa42"
      uniprot "UNIPROT:O75964"
    ]
    graphics [
      x 1597.1444363320027
      y 649.3504027351314
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:taxonomy:2697049;urn:miriam:hgnc:14247;urn:miriam:refseq:NM_006476;urn:miriam:hgnc.symbol:ATP5MG;urn:miriam:hgnc.symbol:ATP5MG;urn:miriam:ensembl:ENSG00000167283;urn:miriam:ncbigene:10632;urn:miriam:ncbigene:10632;urn:miriam:uniprot:O75964"
      hgnc "HGNC_SYMBOL:ATP5MG"
      map_id "M14_22"
      name "F_minus_ATPase:Nsp6"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa43"
      uniprot "UNIPROT:O75964"
    ]
    graphics [
      x 1389.7941381705405
      y 1005.4512844600587
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_92"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re82"
      uniprot "NA"
    ]
    graphics [
      x 1744.390234833595
      y 428.835832277503
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:hgnc:14247;urn:miriam:refseq:NM_006476;urn:miriam:hgnc.symbol:ATP5MG;urn:miriam:hgnc.symbol:ATP5MG;urn:miriam:ensembl:ENSG00000167283;urn:miriam:ncbigene:10632;urn:miriam:ncbigene:10632;urn:miriam:uniprot:O75964"
      hgnc "HGNC_SYMBOL:ATP5MG"
      map_id "M14_125"
      name "ATP5MG"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa255"
      uniprot "UNIPROT:O75964"
    ]
    graphics [
      x 1759.4110425267522
      y 314.67490441748475
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_125"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:uniprot:O75964"
      hgnc "NA"
      map_id "M14_37"
      name "F_minus_ATPase"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa97"
      uniprot "UNIPROT:O75964"
    ]
    graphics [
      x 1867.5404141439913
      y 282.07976158502663
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_93"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re83"
      uniprot "NA"
    ]
    graphics [
      x 1910.6388071260876
      y 129.6919165053191
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_2"
      name "F1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa101"
      uniprot "NA"
    ]
    graphics [
      x 1818.251679475272
      y 166.06061443460612
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_1"
      name "F0"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa100"
      uniprot "NA"
    ]
    graphics [
      x 1760.9993041256664
      y 118.46529114425903
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_130"
      name "sa5_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa49"
      uniprot "NA"
    ]
    graphics [
      x 1123.6489416385957
      y 897.4375390822031
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_130"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859"
      hgnc "NA"
      map_id "M14_128"
      name "Nsp6_space_(_plus_)"
      node_subtype "RNA"
      node_type "species"
      org_id "sa47"
      uniprot "NA"
    ]
    graphics [
      x 1490.6467824104927
      y 1529.9412863435246
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_46"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re21"
      uniprot "NA"
    ]
    graphics [
      x 1618.4979444873318
      y 1582.4271365761674
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_56"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re32"
      uniprot "NA"
    ]
    graphics [
      x 1596.0674729899265
      y 1659.1624031982935
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_49"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re24"
      uniprot "NA"
    ]
    graphics [
      x 1462.7432901124507
      y 1672.695379915619
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_132"
      name "sa2_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa50"
      uniprot "NA"
    ]
    graphics [
      x 1413.2319550782202
      y 1780.6978566209941
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859"
      hgnc "NA"
      map_id "M14_113"
      name "Nsp6_space_(_minus_)"
      node_subtype "RNA"
      node_type "species"
      org_id "sa218"
      uniprot "NA"
    ]
    graphics [
      x 1726.7936847138953
      y 1634.2057561844117
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_57"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re33"
      uniprot "NA"
    ]
    graphics [
      x 1683.4189644063954
      y 1492.42097661682
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_134"
      name "sa3_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa58"
      uniprot "NA"
    ]
    graphics [
      x 1605.3523979746712
      y 1398.3953198258773
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_79"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re67"
      uniprot "NA"
    ]
    graphics [
      x 99.14394558837648
      y 1023.3487825664076
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_33"
      name "P1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa84"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 1145.6723786758766
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M14_32"
      name "P0"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa83"
      uniprot "NA"
    ]
    graphics [
      x 132.3324805504642
      y 1123.5202317352023
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M14_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 138
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_116"
      target_id "M14_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 139
    source 3
    target 2
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_31"
      target_id "M14_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 2
    target 4
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_78"
      target_id "M14_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 135
    target 3
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_79"
      target_id "M14_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 4
    target 5
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_20"
      target_id "M14_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 6
    target 5
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_129"
      target_id "M14_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 144
    source 5
    target 7
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_51"
      target_id "M14_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 145
    source 8
    target 6
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_47"
      target_id "M14_129"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 146
    source 6
    target 9
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_129"
      target_id "M14_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 147
    source 6
    target 10
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_129"
      target_id "M14_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 148
    source 6
    target 11
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_129"
      target_id "M14_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 149
    source 6
    target 12
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_129"
      target_id "M14_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 150
    source 6
    target 13
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_129"
      target_id "M14_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 151
    source 6
    target 14
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_129"
      target_id "M14_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 152
    source 127
    target 8
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_128"
      target_id "M14_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 153
    source 9
    target 126
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_48"
      target_id "M14_130"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 154
    source 118
    target 10
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_21"
      target_id "M14_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 155
    source 10
    target 119
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_50"
      target_id "M14_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 156
    source 107
    target 11
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_107"
      target_id "M14_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 157
    source 11
    target 108
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_69"
      target_id "M14_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 158
    source 92
    target 12
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_13"
      target_id "M14_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 159
    source 12
    target 93
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_55"
      target_id "M14_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 160
    source 79
    target 13
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_133"
      target_id "M14_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 161
    source 13
    target 80
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_52"
      target_id "M14_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 162
    source 15
    target 14
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_102"
      target_id "M14_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 163
    source 16
    target 14
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_127"
      target_id "M14_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 164
    source 14
    target 17
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_67"
      target_id "M14_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 165
    source 68
    target 15
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_63"
      target_id "M14_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 166
    source 15
    target 69
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_102"
      target_id "M14_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 167
    source 20
    target 16
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_44"
      target_id "M14_127"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 168
    source 16
    target 21
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_127"
      target_id "M14_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 169
    source 16
    target 22
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_127"
      target_id "M14_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 170
    source 16
    target 23
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_127"
      target_id "M14_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 171
    source 16
    target 24
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_127"
      target_id "M14_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 172
    source 16
    target 25
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_127"
      target_id "M14_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 173
    source 16
    target 26
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_127"
      target_id "M14_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 174
    source 17
    target 18
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_17"
      target_id "M14_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 175
    source 18
    target 19
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_77"
      target_id "M14_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 176
    source 60
    target 20
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_126"
      target_id "M14_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 177
    source 51
    target 21
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_137"
      target_id "M14_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 178
    source 21
    target 52
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_94"
      target_id "M14_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 179
    source 41
    target 22
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_136"
      target_id "M14_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 180
    source 22
    target 42
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_73"
      target_id "M14_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 181
    source 36
    target 23
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M14_10"
      target_id "M14_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 182
    source 23
    target 37
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_76"
      target_id "M14_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 34
    target 24
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_95"
      target_id "M14_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 24
    target 35
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_40"
      target_id "M14_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 25
    target 33
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_54"
      target_id "M14_131"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 27
    target 26
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_96"
      target_id "M14_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 26
    target 28
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_42"
      target_id "M14_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 27
    target 31
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_96"
      target_id "M14_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 28
    target 29
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_6"
      target_id "M14_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 29
    target 30
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_86"
      target_id "M14_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 31
    target 32
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_87"
      target_id "M14_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 37
    target 38
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_112"
      target_id "M14_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 39
    target 38
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_12"
      target_id "M14_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 38
    target 40
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_58"
      target_id "M14_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 41
    target 47
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_136"
      target_id "M14_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 41
    target 48
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_136"
      target_id "M14_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 42
    target 43
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_9"
      target_id "M14_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 42
    target 44
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_9"
      target_id "M14_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 43
    target 46
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_39"
      target_id "M14_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 44
    target 45
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_80"
      target_id "M14_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 47
    target 50
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_60"
      target_id "M14_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 48
    target 49
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_82"
      target_id "M14_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 51
    target 55
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_137"
      target_id "M14_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 51
    target 56
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_137"
      target_id "M14_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 52
    target 53
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_5"
      target_id "M14_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 53
    target 54
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_41"
      target_id "M14_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 58
    target 55
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_105"
      target_id "M14_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 55
    target 59
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_68"
      target_id "M14_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 56
    target 57
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_81"
      target_id "M14_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 61
    target 60
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_38"
      target_id "M14_126"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 60
    target 62
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_126"
      target_id "M14_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 60
    target 63
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_126"
      target_id "M14_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 65
    target 61
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_115"
      target_id "M14_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 62
    target 65
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_43"
      target_id "M14_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 63
    target 64
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_59"
      target_id "M14_135"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 65
    target 66
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_115"
      target_id "M14_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 66
    target 67
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_45"
      target_id "M14_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 71
    target 68
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_100"
      target_id "M14_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 69
    target 70
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_66"
      target_id "M14_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 72
    target 71
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_61"
      target_id "M14_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 71
    target 73
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_100"
      target_id "M14_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 71
    target 74
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_100"
      target_id "M14_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 75
    target 72
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_114"
      target_id "M14_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 73
    target 78
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_65"
      target_id "M14_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 74
    target 75
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_62"
      target_id "M14_114"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 75
    target 76
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_114"
      target_id "M14_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 76
    target 77
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_64"
      target_id "M14_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 79
    target 85
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_133"
      target_id "M14_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 79
    target 86
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_133"
      target_id "M14_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 79
    target 87
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_133"
      target_id "M14_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 80
    target 81
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_7"
      target_id "M14_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 80
    target 82
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_7"
      target_id "M14_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 81
    target 84
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_53"
      target_id "M14_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 82
    target 83
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_85"
      target_id "M14_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 85
    target 91
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_83"
      target_id "M14_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 86
    target 90
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_84"
      target_id "M14_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 88
    target 87
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_111"
      target_id "M14_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 87
    target 89
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_75"
      target_id "M14_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 239
    source 96
    target 92
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_89"
      target_id "M14_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 240
    source 92
    target 97
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_13"
      target_id "M14_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 241
    source 93
    target 94
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_14"
      target_id "M14_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 242
    source 94
    target 95
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_91"
      target_id "M14_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 243
    source 99
    target 96
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_36"
      target_id "M14_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 244
    source 100
    target 96
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_117"
      target_id "M14_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 245
    source 97
    target 98
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_90"
      target_id "M14_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 246
    source 99
    target 104
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CATALYSIS"
      source_id "M14_36"
      target_id "M14_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 247
    source 100
    target 101
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_117"
      target_id "M14_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 248
    source 102
    target 101
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_106"
      target_id "M14_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 249
    source 101
    target 103
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_74"
      target_id "M14_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 250
    source 105
    target 104
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_124"
      target_id "M14_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 251
    source 104
    target 106
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_88"
      target_id "M14_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 252
    source 107
    target 109
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_107"
      target_id "M14_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 253
    source 107
    target 110
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_107"
      target_id "M14_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 254
    source 107
    target 111
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_107"
      target_id "M14_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 255
    source 116
    target 109
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_109"
      target_id "M14_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 256
    source 109
    target 117
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_71"
      target_id "M14_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 257
    source 114
    target 110
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_108"
      target_id "M14_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 258
    source 110
    target 115
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_70"
      target_id "M14_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 259
    source 112
    target 111
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_110"
      target_id "M14_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 260
    source 111
    target 113
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_72"
      target_id "M14_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 261
    source 120
    target 118
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_92"
      target_id "M14_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 262
    source 121
    target 120
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_125"
      target_id "M14_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 263
    source 122
    target 120
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_37"
      target_id "M14_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 264
    source 123
    target 122
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_93"
      target_id "M14_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 265
    source 124
    target 123
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_2"
      target_id "M14_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 266
    source 125
    target 123
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_1"
      target_id "M14_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 267
    source 128
    target 127
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_46"
      target_id "M14_128"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 268
    source 127
    target 129
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_128"
      target_id "M14_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 269
    source 127
    target 130
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_128"
      target_id "M14_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 270
    source 132
    target 128
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_113"
      target_id "M14_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 271
    source 129
    target 132
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_56"
      target_id "M14_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 272
    source 130
    target 131
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_49"
      target_id "M14_132"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 273
    source 132
    target 133
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_113"
      target_id "M14_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 274
    source 133
    target 134
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "PRODUCTION"
      source_id "M14_57"
      target_id "M14_134"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 275
    source 136
    target 135
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_33"
      target_id "M14_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 276
    source 137
    target 135
    cd19dm [
      diagram "C19DMap:Nsp4 and Nsp6 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M14_32"
      target_id "M14_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
