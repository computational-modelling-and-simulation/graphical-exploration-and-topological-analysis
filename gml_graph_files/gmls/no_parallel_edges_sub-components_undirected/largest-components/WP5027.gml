# generated with VANTED V2.8.2 at Fri Mar 04 10:04:39 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5027"
      full_annotation "urn:miriam:ncbigene:1983;urn:miriam:ncbigene:3646;urn:miriam:obo.chebi:CHEBI%3A13145;urn:miriam:ncbigene:1964;urn:miriam:ncbigene:8668;urn:miriam:ncbigene:8669;urn:miriam:ncbigene:10209;urn:miriam:ncbigene:8667;urn:miriam:ncbigene:8666;urn:miriam:ncbigene:8663;urn:miriam:ncbigene:8664;urn:miriam:ncbigene:8662;urn:miriam:ncbigene:8894;urn:miriam:ncbigene:1965;urn:miriam:ncbigene:8661;urn:miriam:ncbigene:1968;https://identifiers.org/complexportal:CPX-6036;urn:miriam:ncbigene:8665"
      hgnc "NA"
      map_id "W4_1"
      name "40S_space_cytosolic_space_small_space__br_ribosomal_space_subunit"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b327e"
      uniprot "NA"
    ]
    graphics [
      x 786.9189159303726
      y 676.0565461284727
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_20"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idb76f4fdf"
      uniprot "NA"
    ]
    graphics [
      x 685.0098000370967
      y 766.1670406566495
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_12"
      name "NA"
      node_subtype "UNKNOWN_POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "id19d08a66"
      uniprot "NA"
    ]
    graphics [
      x 877.1436430017686
      y 587.7436720552219
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_7"
      name "Load_space_mRNA_space_and_space_start_space_translation"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "dfc42"
      uniprot "NA"
    ]
    graphics [
      x 916.1413827086863
      y 476.79227356490264
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5027"
      full_annotation "urn:miriam:ncbigene:8665;urn:miriam:ncbigene:10209;urn:miriam:ncbigene:3646;urn:miriam:ncbigene:1983;urn:miriam:ncbigene:8669;urn:miriam:ncbigene:8666;https://identifiers.org/complexportal:CPX-6036;urn:miriam:ncbigene:8667;urn:miriam:ncbigene:8661;urn:miriam:ncbigene:8668;urn:miriam:ncbigene:1964;urn:miriam:ncbigene:8664;urn:miriam:ncbigene:8663;urn:miriam:ncbigene:8662"
      hgnc "NA"
      map_id "W4_9"
      name "40S_space_cytosolic_space_small_space__br_ribosomal_space_subunit"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ed8b6"
      uniprot "NA"
    ]
    graphics [
      x 583.0462708299515
      y 872.630757295842
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_16"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id78120f40"
      uniprot "NA"
    ]
    graphics [
      x 435.3874751368231
      y 913.2337621361173
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_13"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id2d00e6ed"
      uniprot "NA"
    ]
    graphics [
      x 663.557491284736
      y 969.8914860081966
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5027"
      full_annotation "urn:miriam:ncbigene:1965;urn:miriam:ncbigene:1968;urn:miriam:obo.chebi:CHEBI%3A13145;urn:miriam:ncbigene:8894"
      hgnc "NA"
      map_id "W4_4"
      name "Ternary_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d6fe1"
      uniprot "NA"
    ]
    graphics [
      x 759.279966373982
      y 906.4973831212465
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5027"
      full_annotation "https://identifiers.org/complexportal:CPX-5223"
      hgnc "NA"
      map_id "W4_6"
      name "40S_space_cytosolic_space_small_space__br_ribosomal_space_subunit"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "dc35b"
      uniprot "NA"
    ]
    graphics [
      x 279.0739945388643
      y 882.1570060181178
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_14"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id4b83f12a"
      uniprot "NA"
    ]
    graphics [
      x 292.37815987683064
      y 743.3546082126983
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_18"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id8da43876"
      uniprot "NA"
    ]
    graphics [
      x 190.19713828481628
      y 768.1626851559797
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_15"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id573935d6"
      uniprot "NA"
    ]
    graphics [
      x 143.89294161079783
      y 888.908924292338
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_17"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id8b1ce7b7"
      uniprot "NA"
    ]
    graphics [
      x 380.96200682293045
      y 813.9284564510037
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5027"
      full_annotation "urn:miriam:ncbigene:1983"
      hgnc "NA"
      map_id "W4_5"
      name "EIF5"
      node_subtype "GENE"
      node_type "species"
      org_id "db339"
      uniprot "NA"
    ]
    graphics [
      x 448.78820784866934
      y 718.7862173748613
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5027"
      full_annotation "urn:miriam:ncbigene:10209"
      hgnc "NA"
      map_id "W4_11"
      name "EIF1"
      node_subtype "GENE"
      node_type "species"
      org_id "fea09"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 794.375999466176
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5027"
      full_annotation "urn:miriam:ncbigene:1964"
      hgnc "NA"
      map_id "W4_3"
      name "EIF1A"
      node_subtype "GENE"
      node_type "species"
      org_id "ca957"
      uniprot "NA"
    ]
    graphics [
      x 156.54098384352082
      y 643.8342216473917
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5027"
      full_annotation "urn:miriam:ncbigene:8668;urn:miriam:ncbigene:8669;urn:miriam:ncbigene:8664;urn:miriam:ncbigene:8662;urn:miriam:ncbigene:3646;urn:miriam:ncbigene:8661;urn:miriam:ncbigene:8663;urn:miriam:ncbigene:8665;urn:miriam:ncbigene:8667;urn:miriam:ncbigene:8666"
      hgnc "NA"
      map_id "W4_2"
      name "Eukaryotic_space_translation_space_initiation_br_factor_space_3_space_complex_br_"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b97c5"
      uniprot "NA"
    ]
    graphics [
      x 310.73117315955574
      y 620.9900127247814
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 18
    source 2
    target 1
    cd19dm [
      diagram "WP5027"
      edge_type "PRODUCTION"
      source_id "W4_20"
      target_id "W4_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 19
    source 1
    target 3
    cd19dm [
      diagram "WP5027"
      edge_type "CONSPUMPTION"
      source_id "W4_1"
      target_id "W4_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 20
    source 5
    target 2
    cd19dm [
      diagram "WP5027"
      edge_type "CONSPUMPTION"
      source_id "W4_9"
      target_id "W4_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 21
    source 3
    target 4
    cd19dm [
      diagram "WP5027"
      edge_type "PRODUCTION"
      source_id "W4_12"
      target_id "W4_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 22
    source 6
    target 5
    cd19dm [
      diagram "WP5027"
      edge_type "PRODUCTION"
      source_id "W4_16"
      target_id "W4_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 23
    source 7
    target 5
    cd19dm [
      diagram "WP5027"
      edge_type "PRODUCTION"
      source_id "W4_13"
      target_id "W4_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 24
    source 9
    target 6
    cd19dm [
      diagram "WP5027"
      edge_type "CONSPUMPTION"
      source_id "W4_6"
      target_id "W4_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 25
    source 8
    target 7
    cd19dm [
      diagram "WP5027"
      edge_type "CONSPUMPTION"
      source_id "W4_4"
      target_id "W4_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 26
    source 10
    target 9
    cd19dm [
      diagram "WP5027"
      edge_type "PRODUCTION"
      source_id "W4_14"
      target_id "W4_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 27
    source 11
    target 9
    cd19dm [
      diagram "WP5027"
      edge_type "PRODUCTION"
      source_id "W4_18"
      target_id "W4_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 28
    source 12
    target 9
    cd19dm [
      diagram "WP5027"
      edge_type "PRODUCTION"
      source_id "W4_15"
      target_id "W4_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 29
    source 13
    target 9
    cd19dm [
      diagram "WP5027"
      edge_type "PRODUCTION"
      source_id "W4_17"
      target_id "W4_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 30
    source 17
    target 10
    cd19dm [
      diagram "WP5027"
      edge_type "CONSPUMPTION"
      source_id "W4_2"
      target_id "W4_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 31
    source 16
    target 11
    cd19dm [
      diagram "WP5027"
      edge_type "CONSPUMPTION"
      source_id "W4_3"
      target_id "W4_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 32
    source 15
    target 12
    cd19dm [
      diagram "WP5027"
      edge_type "CONSPUMPTION"
      source_id "W4_11"
      target_id "W4_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 33
    source 14
    target 13
    cd19dm [
      diagram "WP5027"
      edge_type "CONSPUMPTION"
      source_id "W4_5"
      target_id "W4_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
