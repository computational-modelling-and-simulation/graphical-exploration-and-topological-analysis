# generated with VANTED V2.8.2 at Fri Mar 04 10:04:32 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:Q9H492"
      hgnc "NA"
      map_id "W7_10"
      name "LC3"
      node_subtype "GENE"
      node_type "species"
      org_id "b3be1"
      uniprot "UNIPROT:Q9H492"
    ]
    graphics [
      x 911.6592840696293
      y 770.5084285738467
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id7b795d10"
      uniprot "NA"
    ]
    graphics [
      x 1011.5200873780129
      y 806.9039291293316
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:obo.chebi:16038"
      hgnc "NA"
      map_id "W7_27"
      name "PE"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "fa8aa"
      uniprot "NA"
    ]
    graphics [
      x 967.2279052647236
      y 698.6419042545192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:Q8WYN0;urn:miriam:uniprot:O95352;urn:miriam:uniprot:Q9NT62"
      hgnc "NA"
      map_id "W7_9"
      name "b392b"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b392b"
      uniprot "UNIPROT:Q8WYN0;UNIPROT:O95352;UNIPROT:Q9NT62"
    ]
    graphics [
      x 860.2333178735115
      y 842.7900946045509
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:obo.chebi:16038;urn:miriam:uniprot:Q9H492"
      hgnc "NA"
      map_id "W7_8"
      name "b35a3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b35a3"
      uniprot "UNIPROT:Q9H492"
    ]
    graphics [
      x 1038.1762426173098
      y 666.6450337594304
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_25"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f1056"
      uniprot "NA"
    ]
    graphics [
      x 734.3509690734367
      y 774.5017631474651
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:O94817;urn:miriam:uniprot:Q676U5;urn:miriam:uniprot:Q9H1Y0;urn:miriam:uniprot:Q8NAA4"
      hgnc "NA"
      map_id "W7_22"
      name "e7545"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e7545"
      uniprot "UNIPROT:O94817;UNIPROT:Q676U5;UNIPROT:Q9H1Y0;UNIPROT:Q8NAA4"
    ]
    graphics [
      x 635.2923535510333
      y 672.9101648580437
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_24"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f0c56"
      uniprot "NA"
    ]
    graphics [
      x 528.3703293630431
      y 568.9659701709435
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:Q9H1Y0;urn:miriam:uniprot:O94817"
      hgnc "NA"
      map_id "W7_1"
      name "a2002"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "a2002"
      uniprot "UNIPROT:Q9H1Y0;UNIPROT:O94817"
    ]
    graphics [
      x 409.91204896595224
      y 461.45084365134653
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:Q676U5;urn:miriam:uniprot:Q8NAA4"
      hgnc "NA"
      map_id "W7_6"
      name "ac403"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ac403"
      uniprot "UNIPROT:Q676U5;UNIPROT:Q8NAA4"
    ]
    graphics [
      x 489.4004911827885
      y 675.7469362978949
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_2"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "a613c"
      uniprot "NA"
    ]
    graphics [
      x 301.51854694037945
      y 334.43643646231504
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:O94817"
      hgnc "NA"
      map_id "W7_18"
      name "ATG12"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d2b7f"
      uniprot "UNIPROT:O94817"
    ]
    graphics [
      x 192.42392397713985
      y 285.40155823797613
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:Q9H1Y0"
      hgnc "NA"
      map_id "W7_14"
      name "ATG5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c82d3"
      uniprot "UNIPROT:Q9H1Y0"
    ]
    graphics [
      x 382.24569482507917
      y 253.31956577331255
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:Q9H0Y0;urn:miriam:uniprot:O95352"
      hgnc "NA"
      map_id "W7_20"
      name "e453d"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e453d"
      uniprot "UNIPROT:Q9H0Y0;UNIPROT:O95352"
    ]
    graphics [
      x 312.0355545260314
      y 200.5215672048646
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 15
    source 1
    target 2
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "W7_10"
      target_id "W7_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 16
    source 3
    target 2
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "W7_27"
      target_id "W7_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 17
    source 4
    target 2
    cd19dm [
      diagram "WP4863"
      edge_type "CATALYSIS"
      source_id "W7_9"
      target_id "W7_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 18
    source 2
    target 5
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_34"
      target_id "W7_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 19
    source 6
    target 4
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_25"
      target_id "W7_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 20
    source 7
    target 6
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "W7_22"
      target_id "W7_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 21
    source 8
    target 7
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_24"
      target_id "W7_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 22
    source 9
    target 8
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "W7_1"
      target_id "W7_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 23
    source 10
    target 8
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "W7_6"
      target_id "W7_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 24
    source 11
    target 9
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_2"
      target_id "W7_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 25
    source 12
    target 11
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "W7_18"
      target_id "W7_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 26
    source 13
    target 11
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "W7_14"
      target_id "W7_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 27
    source 14
    target 11
    cd19dm [
      diagram "WP4863"
      edge_type "CATALYSIS"
      source_id "W7_20"
      target_id "W7_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
