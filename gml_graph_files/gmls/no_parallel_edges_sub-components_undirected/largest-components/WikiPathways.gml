# generated with VANTED V2.8.2 at Fri Mar 04 10:04:36 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ncbiprotein:YP_009725306"
      hgnc "NA"
      map_id "W16_13"
      name "nsp10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b19e5"
      uniprot "NA"
    ]
    graphics [
      x 1332.9283481989892
      y 2402.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_46"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "d1353"
      uniprot "NA"
    ]
    graphics [
      x 1245.6599154620967
      y 1892.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_73"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "e7327"
      uniprot "NA"
    ]
    graphics [
      x 1087.3320631112463
      y 2522.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000107201"
      hgnc "NA"
      map_id "W16_52"
      name "RIG_minus_I_space_(DDX58)"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d88ba"
      uniprot "NA"
    ]
    graphics [
      x 915.6470507164138
      y 2162.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_10"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "ae85d"
      uniprot "NA"
    ]
    graphics [
      x 465.4053337660264
      y 1897.4935923850153
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_44"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "d00bf"
      uniprot "NA"
    ]
    graphics [
      x 785.4896151858787
      y 1583.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355;PUBMED:32695122"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_106"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id5e8cde6a"
      uniprot "NA"
    ]
    graphics [
      x 1510.7581195838427
      y 2043.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355;PUBMED:32695122"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_80"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "ed2f7"
      uniprot "NA"
    ]
    graphics [
      x 1197.9600276537958
      y 2192.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000088888"
      hgnc "NA"
      map_id "W16_94"
      name "MAVS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "fa763"
      uniprot "NA"
    ]
    graphics [
      x 1286.2756356127306
      y 1802.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355;PUBMED:32695122"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_29"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "bd252"
      uniprot "NA"
    ]
    graphics [
      x 1136.4332102551268
      y 1353.7015574910176
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "PUBMED:32695122"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_103"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id51069b65"
      uniprot "NA"
    ]
    graphics [
      x 1395.7657483074386
      y 2312.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000131323"
      hgnc "NA"
      map_id "W16_88"
      name "TRAF3"
      node_subtype "GENE"
      node_type "species"
      org_id "f673f"
      uniprot "NA"
    ]
    graphics [
      x 1405.5967995366213
      y 2462.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_2"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "a7711"
      uniprot "NA"
    ]
    graphics [
      x 1526.7036187337037
      y 2282.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_57"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "dca81"
      uniprot "NA"
    ]
    graphics [
      x 1519.5416347003493
      y 2547.247230831348
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_110"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id7c297d34"
      uniprot "NA"
    ]
    graphics [
      x 969.4241324922731
      y 2537.445782186531
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "PUBMED:32695122"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_82"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ef27c"
      uniprot "NA"
    ]
    graphics [
      x 1459.6425470107536
      y 1712.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000263528;urn:miriam:ensembl:ENSG00000183735"
      hgnc "NA"
      map_id "W16_27"
      name "bc0e3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "bc0e3"
      uniprot "NA"
    ]
    graphics [
      x 1610.5162471610465
      y 1473.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "PUBMED:32695122"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_93"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "fa67c"
      uniprot "NA"
    ]
    graphics [
      x 1397.7323935312033
      y 1442.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000126456"
      hgnc "NA"
      map_id "W16_74"
      name "IRF3"
      node_subtype "GENE"
      node_type "species"
      org_id "e7683"
      uniprot "NA"
    ]
    graphics [
      x 1192.6372882787018
      y 1532.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_108"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id72e167d2"
      uniprot "NA"
    ]
    graphics [
      x 1535.409596572843
      y 1953.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "PUBMED:32695122;PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_43"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "cfeb0"
      uniprot "NA"
    ]
    graphics [
      x 938.8321683218796
      y 1503.281195954836
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "PUBMED:32695122;PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_58"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "dd2da"
      uniprot "NA"
    ]
    graphics [
      x 1012.6884726834846
      y 1293.281195954836
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000126456"
      hgnc "NA"
      map_id "W16_21"
      name "b8d9b"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b8d9b"
      uniprot "NA"
    ]
    graphics [
      x 1011.6246317438643
      y 1477.0742286417976
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_112"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idbbf051e0"
      uniprot "NA"
    ]
    graphics [
      x 1088.7458427626807
      y 1832.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:wikidata:Q6046488"
      hgnc "NA"
      map_id "W16_83"
      name "INF_minus_I_space_alpha_slash__space_beta"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f1657"
      uniprot "NA"
    ]
    graphics [
      x 989.2688295778074
      y 2222.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_97"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "fc80b"
      uniprot "NA"
    ]
    graphics [
      x 1330.3044489083334
      y 2222.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_107"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id619b1996"
      uniprot "NA"
    ]
    graphics [
      x 572.1075271213809
      y 2153.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:wikidata:Q6046488"
      hgnc "NA"
      map_id "W16_72"
      name "INF_minus_I_space_alpha_slash__space_beta"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e6fbe"
      uniprot "NA"
    ]
    graphics [
      x 369.0691415219462
      y 1825.370067306213
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_105"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id5db145b0"
      uniprot "NA"
    ]
    graphics [
      x 331.80470247074607
      y 1721.2405444890771
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_109"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id782ae218"
      uniprot "NA"
    ]
    graphics [
      x 503.5500240548174
      y 2134.918994262337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:wikidata:Q6046488"
      hgnc "NA"
      map_id "W16_30"
      name "INF_minus_I_space_alpha_slash__space_beta"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bd4e9"
      uniprot "NA"
    ]
    graphics [
      x 472.04450539611105
      y 2249.4233902663073
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_102"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id3cf0d202"
      uniprot "NA"
    ]
    graphics [
      x 659.7359432421295
      y 2153.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000159110;urn:miriam:ensembl:ENSG00000142166"
      hgnc "NA"
      map_id "W16_19"
      name "b8407"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b8407"
      uniprot "NA"
    ]
    graphics [
      x 1061.9526343946222
      y 2282.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_81"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "ed722"
      uniprot "NA"
    ]
    graphics [
      x 975.7729572568924
      y 2072.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000162434;urn:miriam:ensembl:ENSG00000105397"
      hgnc "NA"
      map_id "W16_59"
      name "dd322"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "dd322"
      uniprot "NA"
    ]
    graphics [
      x 718.9602408363262
      y 1703.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_101"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id38b9357c"
      uniprot "NA"
    ]
    graphics [
      x 597.3829001373978
      y 2063.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000115415"
      hgnc "NA"
      map_id "W16_92"
      name "STAT1"
      node_subtype "GENE"
      node_type "species"
      org_id "f9489"
      uniprot "NA"
    ]
    graphics [
      x 834.5009018037051
      y 2282.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_86"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "f2bcd"
      uniprot "NA"
    ]
    graphics [
      x 802.4265248067512
      y 2252.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "PUBMED:25848864"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_100"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id2aa49a5d"
      uniprot "NA"
    ]
    graphics [
      x 670.4898416356363
      y 2342.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:uniprot:Q6UXN2"
      hgnc "NA"
      map_id "W16_50"
      name "TREML4_space_"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d4fdc"
      uniprot "UNIPROT:Q6UXN2"
    ]
    graphics [
      x 669.7556285978562
      y 1855.8429747656378
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "PUBMED:25848864"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_116"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "idf6e0bc7f"
      uniprot "NA"
    ]
    graphics [
      x 747.4086458922708
      y 1493.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000196664"
      hgnc "NA"
      map_id "W16_71"
      name "TLR7"
      node_subtype "GENE"
      node_type "species"
      org_id "e4953"
      uniprot "NA"
    ]
    graphics [
      x 895.6662240930993
      y 1922.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000172936"
      hgnc "NA"
      map_id "W16_91"
      name "MYD88"
      node_subtype "GENE"
      node_type "species"
      org_id "f8b63"
      uniprot "NA"
    ]
    graphics [
      x 293.4891474429494
      y 1588.317339033685
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_14"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "b1a40"
      uniprot "NA"
    ]
    graphics [
      x 310.3702986563751
      y 1206.3974803179037
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_89"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f88d9"
      uniprot "NA"
    ]
    graphics [
      x 457.0325601555784
      y 1994.8727351074174
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000198001"
      hgnc "NA"
      map_id "W16_70"
      name "IRAK4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e33f8"
      uniprot "NA"
    ]
    graphics [
      x 628.892089422148
      y 2380.270044684019
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_35"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "c6c90"
      uniprot "NA"
    ]
    graphics [
      x 763.1042711810762
      y 2282.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000175104"
      hgnc "NA"
      map_id "W16_98"
      name "TRAF6"
      node_subtype "GENE"
      node_type "species"
      org_id "fe40f"
      uniprot "NA"
    ]
    graphics [
      x 735.5831927940815
      y 1883.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "d0ce3"
      uniprot "NA"
    ]
    graphics [
      x 740.4340720726326
      y 2252.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_24"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "ba761"
      uniprot "NA"
    ]
    graphics [
      x 1011.4044713213777
      y 2162.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_11"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "aef8e"
      uniprot "NA"
    ]
    graphics [
      x 1266.5111183070046
      y 1622.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_75"
      name "NFkB"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "e788c"
      uniprot "NA"
    ]
    graphics [
      x 1370.2005356414263
      y 1922.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_96"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "fbdcb"
      uniprot "NA"
    ]
    graphics [
      x 1119.5905306541406
      y 2102.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_53"
      name "Pro_minus_inflammatory_space__br_cytokines_space_production"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "d99d5"
      uniprot "NA"
    ]
    graphics [
      x 717.3415299978993
      y 1999.7525696696582
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "PUBMED:19893639;PUBMED:17667842"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_15"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "b22d1"
      uniprot "NA"
    ]
    graphics [
      x 546.120539280623
      y 1793.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:wikidata:Q165399;urn:miriam:pubmed:32205204"
      hgnc "NA"
      map_id "W16_8"
      name "azithromycin"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "ac609"
      uniprot "NA"
    ]
    graphics [
      x 626.2993013733546
      y 1973.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      annotation "PUBMED:30406125"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_67"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "e1bdb"
      uniprot "NA"
    ]
    graphics [
      x 528.6929998095166
      y 2183.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000213928;urn:miriam:ensembl:ENSG00000115415;urn:miriam:ensembl:ENSG00000170581"
      hgnc "NA"
      map_id "W16_49"
      name "d46a8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d46a8"
      uniprot "NA"
    ]
    graphics [
      x 827.2810377354231
      y 2192.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_65"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "e0118"
      uniprot "NA"
    ]
    graphics [
      x 1573.4016156373343
      y 2223.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_28"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "bc446"
      uniprot "NA"
    ]
    graphics [
      x 581.2725520837952
      y 1703.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_85"
      name "ISGs"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f1bfb"
      uniprot "NA"
    ]
    graphics [
      x 560.322593601258
      y 1973.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_33"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "c04cc"
      uniprot "NA"
    ]
    graphics [
      x 944.9380181094341
      y 1802.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000089127;urn:miriam:ensembl:ENSG00000055332;urn:miriam:ensembl:ENSG00000111331;urn:miriam:ensembl:ENSG00000111335"
      hgnc "NA"
      map_id "W16_37"
      name "c82e3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c82e3"
      uniprot "NA"
    ]
    graphics [
      x 664.3030235793696
      y 1253.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_12"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "af7a1"
      uniprot "NA"
    ]
    graphics [
      x 501.763710659008
      y 749.9793961096518
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_34"
      name "Innate_space_Immunity"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "c1ec8"
      uniprot "NA"
    ]
    graphics [
      x 351.5601070880442
      y 603.8923085142253
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000115415;urn:miriam:ensembl:ENSG00000170581"
      hgnc "NA"
      map_id "W16_64"
      name "dffb5"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "dffb5"
      uniprot "NA"
    ]
    graphics [
      x 1794.4386286075915
      y 2323.579430662189
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:refseq:YP_009725299.1"
      hgnc "NA"
      map_id "W16_77"
      name "PLpro_space_(nsp3)"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e9fd0"
      uniprot "NA"
    ]
    graphics [
      x 1392.0327689467445
      y 2342.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      annotation "PUBMED:22088887;PUBMED:18852458"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_68"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "e29be"
      uniprot "NA"
    ]
    graphics [
      x 1254.7115604896337
      y 1832.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_51"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "d52dc"
      uniprot "NA"
    ]
    graphics [
      x 861.619301284785
      y 2372.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000263528"
      hgnc "NA"
      map_id "W16_42"
      name "IKBKE"
      node_subtype "GENE"
      node_type "species"
      org_id "cb0df"
      uniprot "NA"
    ]
    graphics [
      x 498.0693524793795
      y 2104.918994262337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:wikidata:Q27097846"
      hgnc "NA"
      map_id "W16_79"
      name "GRL0617"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "ebd2b"
      uniprot "NA"
    ]
    graphics [
      x 1050.6216587054676
      y 1652.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000172936"
      hgnc "NA"
      map_id "W16_63"
      name "MYD88"
      node_subtype "GENE"
      node_type "species"
      org_id "dfbb7"
      uniprot "NA"
    ]
    graphics [
      x 1034.557107971707
      y 2252.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "b5fd0"
      uniprot "NA"
    ]
    graphics [
      x 1461.0098671293076
      y 2282.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_90"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f8a9c"
      uniprot "NA"
    ]
    graphics [
      x 896.9078118116975
      y 2132.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_111"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id97a8368b"
      uniprot "NA"
    ]
    graphics [
      x 1201.954561414926
      y 1952.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:uniprot:O60603"
      hgnc "NA"
      map_id "W16_69"
      name "TLR2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e33e6"
      uniprot "UNIPROT:O60603"
    ]
    graphics [
      x 1239.4128717154058
      y 1565.5216341918122
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_23"
      name "NA"
      node_subtype "POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "b9579"
      uniprot "NA"
    ]
    graphics [
      x 1558.0641909883318
      y 1563.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_38"
      name "SARS_minus_CoV_space_2"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "c8487"
      uniprot "NA"
    ]
    graphics [
      x 1810.8386226002058
      y 1642.105742949566
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_78"
      name "NA"
      node_subtype "POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "eaa62"
      uniprot "NA"
    ]
    graphics [
      x 2290.2258145471346
      y 1615.1487458172744
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_4"
      name "NA"
      node_subtype "POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "aabad"
      uniprot "NA"
    ]
    graphics [
      x 1378.008560319156
      y 1502.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_60"
      name "NA"
      node_subtype "POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "de4c5"
      uniprot "NA"
    ]
    graphics [
      x 1944.775396638673
      y 1702.0899601841807
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:uniprot:Q9Y2C9"
      hgnc "NA"
      map_id "W16_16"
      name "TLR6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b5481"
      uniprot "UNIPROT:Q9Y2C9"
    ]
    graphics [
      x 1762.8594244634205
      y 1953.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:uniprot:O00206"
      hgnc "NA"
      map_id "W16_36"
      name "TLR4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c7d93"
      uniprot "UNIPROT:O00206"
    ]
    graphics [
      x 1123.684077694403
      y 1802.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_32"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "bebd6"
      uniprot "NA"
    ]
    graphics [
      x 1271.7650657286654
      y 2072.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_3"
      name "TRIF_br_"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a8003"
      uniprot "NA"
    ]
    graphics [
      x 1469.1851332210817
      y 2162.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "da0cb"
      uniprot "NA"
    ]
    graphics [
      x 1003.5923592643956
      y 2462.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:uniprot:O15455"
      hgnc "NA"
      map_id "W16_7"
      name "TLR3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ab922"
      uniprot "UNIPROT:O15455"
    ]
    graphics [
      x 705.3398441235702
      y 2282.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_114"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idd0587e82"
      uniprot "NA"
    ]
    graphics [
      x 910.2787714730216
      y 2072.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:wikidata:Q82069695"
      hgnc "NA"
      map_id "W16_26"
      name "SARS_minus_CoV_space_2_space_RNA"
      node_subtype "RNA"
      node_type "species"
      org_id "bbbdd"
      uniprot "NA"
    ]
    graphics [
      x 1094.2985380397586
      y 1592.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_40"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "c8d59"
      uniprot "NA"
    ]
    graphics [
      x 859.1842835510627
      y 1892.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_17"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "b5e59"
      uniprot "NA"
    ]
    graphics [
      x 1133.4803236385483
      y 932.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:uniprot:Q9NR96"
      hgnc "NA"
      map_id "W16_9"
      name "TLR9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "adc15"
      uniprot "UNIPROT:Q9NR96"
    ]
    graphics [
      x 729.7032704498328
      y 909.5201944147736
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:uniprot:Q9BYF1"
      hgnc "NA"
      map_id "W16_76"
      name "ACE2"
      node_subtype "GENE"
      node_type "species"
      org_id "e92a9"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 2466.274628254666
      y 1478.6352404461693
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      annotation "PUBMED:33596266"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_115"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idd827ab35"
      uniprot "NA"
    ]
    graphics [
      x 2368.0287793316547
      y 1666.2014119363448
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_20"
      name "Endosome"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "b8c2d"
      uniprot "NA"
    ]
    graphics [
      x 2496.20216649388
      y 1820.2096388311056
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ncbiprotein:YP_009725297"
      hgnc "NA"
      map_id "W16_47"
      name "nsp1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d2ec8"
      uniprot "NA"
    ]
    graphics [
      x 716.4537279564568
      y 2390.630592014414
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ncbiprotein:YP_009725297"
      hgnc "NA"
      map_id "W16_55"
      name "nsp1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "da173"
      uniprot "NA"
    ]
    graphics [
      x 792.2434732998663
      y 1712.024986525728
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000185507;urn:miriam:ensembl:ENSG00000126456"
      hgnc "NA"
      map_id "W16_56"
      name "db1fb"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "db1fb"
      uniprot "NA"
    ]
    graphics [
      x 1153.6236780466165
      y 1832.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000115267"
      hgnc "NA"
      map_id "W16_22"
      name "MDA5_space_"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b93b0"
      uniprot "NA"
    ]
    graphics [
      x 848.5201796574518
      y 1261.3064338229367
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_66"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "e1751"
      uniprot "NA"
    ]
    graphics [
      x 646.7442189117456
      y 801.0063320843233
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_41"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "ca320"
      uniprot "NA"
    ]
    graphics [
      x 520.2736378784284
      y 953.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_48"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "d3137"
      uniprot "NA"
    ]
    graphics [
      x 423.23162846817615
      y 1397.3908346082928
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355;PUBMED:32695122"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_87"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "f3314"
      uniprot "NA"
    ]
    graphics [
      x 1558.0563149065108
      y 1593.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:wikidata:Q82069695"
      hgnc "NA"
      map_id "W16_99"
      name "SARS_minus_CoV_space_2_space_RNA"
      node_subtype "RNA"
      node_type "species"
      org_id "ff396"
      uniprot "NA"
    ]
    graphics [
      x 2018.6989597680872
      y 1822.0899601841807
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      annotation "PUBMED:32695122"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_104"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id5bdc9aa8"
      uniprot "NA"
    ]
    graphics [
      x 1909.4448213092383
      y 2120.107135739424
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_1"
      name "SARS_minus_CoV_space_2"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "a63e3"
      uniprot "NA"
    ]
    graphics [
      x 1581.2107547012383
      y 2283.8366773977227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_61"
      name "NA"
      node_subtype "POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "de4da"
      uniprot "NA"
    ]
    graphics [
      x 965.2754838342184
      y 1982.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_25"
      name "NA"
      node_subtype "POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "bb605"
      uniprot "NA"
    ]
    graphics [
      x 1981.5404776701803
      y 2156.0626827930764
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:uniprot:O15393"
      hgnc "NA"
      map_id "W16_62"
      name "TMPRSS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "df9cf"
      uniprot "UNIPROT:O15393"
    ]
    graphics [
      x 2018.1703161309745
      y 2004.649759167914
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:uniprot:Q9BYF1"
      hgnc "NA"
      map_id "W16_6"
      name "ACE2"
      node_subtype "GENE"
      node_type "species"
      org_id "aaf33"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 714.6420640205466
      y 1823.314128735235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ncbiprotein:YP_009725311"
      hgnc "NA"
      map_id "W16_39"
      name "nsp16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c8bd9"
      uniprot "NA"
    ]
    graphics [
      x 350.22209882783716
      y 1541.2405444890771
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ncbiprotein:YP_009725309"
      hgnc "NA"
      map_id "W16_5"
      name "nsp14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "aaed9"
      uniprot "NA"
    ]
    graphics [
      x 493.3462969482009
      y 1158.281291735611
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ncbiprotein:YP_009725310"
      hgnc "NA"
      map_id "W16_31"
      name "nsp15"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "beaa5"
      uniprot "NA"
    ]
    graphics [
      x 677.5805811236671
      y 527.1043855117134
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 114
    source 1
    target 2
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_13"
      target_id "W16_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 1
    target 3
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_13"
      target_id "W16_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 2
    target 99
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_46"
      target_id "W16_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 3
    target 4
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_73"
      target_id "W16_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 5
    target 4
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_10"
      target_id "W16_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 6
    target 4
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_44"
      target_id "W16_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 7
    target 4
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_106"
      target_id "W16_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 4
    target 8
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_52"
      target_id "W16_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 111
    target 5
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_39"
      target_id "W16_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 112
    target 6
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_5"
      target_id "W16_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 104
    target 7
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_99"
      target_id "W16_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 8
    target 9
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_80"
      target_id "W16_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 126
    source 10
    target 9
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_29"
      target_id "W16_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 127
    source 9
    target 11
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_94"
      target_id "W16_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 128
    source 99
    target 10
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_22"
      target_id "W16_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 11
    target 12
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_103"
      target_id "W16_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 130
    source 13
    target 12
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_2"
      target_id "W16_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 131
    source 14
    target 12
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_57"
      target_id "W16_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 132
    source 15
    target 12
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_110"
      target_id "W16_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 133
    source 12
    target 16
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_88"
      target_id "W16_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 134
    source 85
    target 13
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_3"
      target_id "W16_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 135
    source 67
    target 14
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_77"
      target_id "W16_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 136
    source 46
    target 15
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_70"
      target_id "W16_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 137
    source 16
    target 17
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_82"
      target_id "W16_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 138
    source 17
    target 18
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_27"
      target_id "W16_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 139
    source 18
    target 19
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_93"
      target_id "W16_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 20
    target 19
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_108"
      target_id "W16_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 19
    target 21
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_74"
      target_id "W16_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 19
    target 22
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_74"
      target_id "W16_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 67
    target 20
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_77"
      target_id "W16_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 144
    source 21
    target 98
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_43"
      target_id "W16_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 145
    source 22
    target 23
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_58"
      target_id "W16_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 146
    source 23
    target 24
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_21"
      target_id "W16_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 147
    source 24
    target 25
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_112"
      target_id "W16_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 148
    source 26
    target 25
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_97"
      target_id "W16_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 149
    source 25
    target 27
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_83"
      target_id "W16_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 150
    source 98
    target 26
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_56"
      target_id "W16_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 151
    source 27
    target 28
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_107"
      target_id "W16_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 152
    source 29
    target 28
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_105"
      target_id "W16_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 153
    source 28
    target 30
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_72"
      target_id "W16_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 154
    source 97
    target 29
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_55"
      target_id "W16_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 155
    source 30
    target 31
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_109"
      target_id "W16_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 156
    source 31
    target 32
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_30"
      target_id "W16_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 157
    source 32
    target 33
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_102"
      target_id "W16_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 158
    source 33
    target 34
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_19"
      target_id "W16_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 159
    source 34
    target 35
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_81"
      target_id "W16_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 160
    source 35
    target 36
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_59"
      target_id "W16_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 161
    source 36
    target 37
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_101"
      target_id "W16_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 162
    source 38
    target 37
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_86"
      target_id "W16_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 163
    source 39
    target 37
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_100"
      target_id "W16_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 164
    source 96
    target 38
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_47"
      target_id "W16_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 165
    source 40
    target 39
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_50"
      target_id "W16_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 166
    source 40
    target 41
    cd19dm [
      diagram "WP4868"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W16_50"
      target_id "W16_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 167
    source 42
    target 41
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_71"
      target_id "W16_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 168
    source 41
    target 43
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_116"
      target_id "W16_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 169
    source 90
    target 42
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_40"
      target_id "W16_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 170
    source 44
    target 43
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_14"
      target_id "W16_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 171
    source 43
    target 45
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_91"
      target_id "W16_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 172
    source 92
    target 44
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_9"
      target_id "W16_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 173
    source 45
    target 46
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_89"
      target_id "W16_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 174
    source 46
    target 47
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_70"
      target_id "W16_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 175
    source 47
    target 48
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_35"
      target_id "W16_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 176
    source 49
    target 48
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_45"
      target_id "W16_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 177
    source 50
    target 48
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_24"
      target_id "W16_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 178
    source 48
    target 51
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_98"
      target_id "W16_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 179
    source 72
    target 49
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_63"
      target_id "W16_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 180
    source 67
    target 50
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_77"
      target_id "W16_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 181
    source 51
    target 52
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_11"
      target_id "W16_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 182
    source 52
    target 53
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_75"
      target_id "W16_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 53
    target 54
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_96"
      target_id "W16_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 55
    target 54
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_15"
      target_id "W16_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 56
    target 55
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_8"
      target_id "W16_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 56
    target 57
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_8"
      target_id "W16_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 57
    target 58
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_67"
      target_id "W16_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 59
    target 58
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_65"
      target_id "W16_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 58
    target 60
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_49"
      target_id "W16_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 66
    target 59
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_64"
      target_id "W16_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 60
    target 61
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_28"
      target_id "W16_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 61
    target 62
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_85"
      target_id "W16_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 62
    target 63
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_33"
      target_id "W16_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 63
    target 64
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_37"
      target_id "W16_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 64
    target 65
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_12"
      target_id "W16_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 68
    target 67
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_68"
      target_id "W16_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 67
    target 69
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_77"
      target_id "W16_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 71
    target 68
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_79"
      target_id "W16_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 69
    target 70
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_51"
      target_id "W16_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 73
    target 72
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_18"
      target_id "W16_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 74
    target 72
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_90"
      target_id "W16_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 75
    target 72
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_111"
      target_id "W16_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 82
    target 73
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_16"
      target_id "W16_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 83
    target 74
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_36"
      target_id "W16_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 76
    target 75
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_69"
      target_id "W16_111"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 77
    target 76
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_23"
      target_id "W16_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 78
    target 77
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_38"
      target_id "W16_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 78
    target 79
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_38"
      target_id "W16_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 78
    target 80
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_38"
      target_id "W16_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 78
    target 81
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_38"
      target_id "W16_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 79
    target 93
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_78"
      target_id "W16_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 80
    target 83
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_4"
      target_id "W16_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 81
    target 82
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_60"
      target_id "W16_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 83
    target 84
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_36"
      target_id "W16_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 84
    target 85
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_32"
      target_id "W16_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 86
    target 85
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_54"
      target_id "W16_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 87
    target 86
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_7"
      target_id "W16_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 88
    target 87
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_114"
      target_id "W16_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 89
    target 88
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_26"
      target_id "W16_114"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 89
    target 90
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_26"
      target_id "W16_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 89
    target 91
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_26"
      target_id "W16_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 91
    target 92
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_17"
      target_id "W16_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 93
    target 94
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_76"
      target_id "W16_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 94
    target 95
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_115"
      target_id "W16_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 100
    target 99
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_66"
      target_id "W16_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 101
    target 99
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_41"
      target_id "W16_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 102
    target 99
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_48"
      target_id "W16_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 103
    target 99
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_87"
      target_id "W16_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 113
    target 100
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_31"
      target_id "W16_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 112
    target 101
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_5"
      target_id "W16_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 111
    target 102
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_39"
      target_id "W16_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 104
    target 103
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_99"
      target_id "W16_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 105
    target 104
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_104"
      target_id "W16_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 106
    target 105
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_1"
      target_id "W16_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 106
    target 107
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_1"
      target_id "W16_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 106
    target 108
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_1"
      target_id "W16_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 107
    target 110
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_61"
      target_id "W16_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 108
    target 109
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_25"
      target_id "W16_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
