# generated with VANTED V2.8.2 at Fri Mar 04 10:04:32 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:uniprot:P59633;urn:miriam:wikidata:Q89458416"
      hgnc "NA"
      map_id "W9_32"
      name "ee4e9"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ee4e9"
      uniprot "UNIPROT:P59633"
    ]
    graphics [
      x 1529.5671840038679
      y 1201.08656388902
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_62"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id81131143"
      uniprot "NA"
    ]
    graphics [
      x 1491.1129930304814
      y 1357.3154500253509
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ncbigene:5595;urn:miriam:ncbigene:5594"
      hgnc "NA"
      map_id "W9_38"
      name "f9084"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f9084"
      uniprot "NA"
    ]
    graphics [
      x 1327.4146215627281
      y 1509.3658276612489
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_59"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id7c30bf1f"
      uniprot "NA"
    ]
    graphics [
      x 1248.0153578836346
      y 1398.098995267997
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_67"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idc07c34c7"
      uniprot "NA"
    ]
    graphics [
      x 1181.800142792315
      y 1533.408023473711
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_64"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "ida2a9cdb0"
      uniprot "NA"
    ]
    graphics [
      x 1454.2857460517089
      y 1492.2653983716054
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_77"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "ideee7f970"
      uniprot "NA"
    ]
    graphics [
      x 1121.020000262823
      y 1541.0721347146673
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_40"
      name "SARS_br_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "f9996"
      uniprot "NA"
    ]
    graphics [
      x 998.0902357193435
      y 1371.2092919183365
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000117676;urn:miriam:ensembl:ENSG00000177189;urn:miriam:ensembl:ENSG00000071242"
      hgnc "NA"
      map_id "W9_24"
      name "dd506"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "dd506"
      uniprot "NA"
    ]
    graphics [
      x 1025.625182596143
      y 1445.9050199862904
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_53"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id589b86fe"
      uniprot "NA"
    ]
    graphics [
      x 879.459039756883
      y 1249.9141787045867
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_68"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idc1c0f088"
      uniprot "NA"
    ]
    graphics [
      x 1198.862120786438
      y 1450.8670513241823
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_56"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id63f652b7"
      uniprot "NA"
    ]
    graphics [
      x 852.1973227396556
      y 1490.8494455898795
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_3"
      name "Protein_br_synthesis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "a5ffe"
      uniprot "NA"
    ]
    graphics [
      x 704.4761213801361
      y 1500.2221518932943
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_47"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id392d909b"
      uniprot "NA"
    ]
    graphics [
      x 592.2267149738266
      y 1425.507611347437
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000151247"
      hgnc "NA"
      map_id "W9_20"
      name "EIF4E"
      node_subtype "GENE"
      node_type "species"
      org_id "ce80a"
      uniprot "NA"
    ]
    graphics [
      x 578.9179382762055
      y 1288.0114564160383
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_49"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id44f6de9d"
      uniprot "NA"
    ]
    graphics [
      x 632.2076716882965
      y 1145.1670765697666
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ncbigene:5600;urn:miriam:ncbigene:6300;urn:miriam:ensembl:ENSG00000112062;urn:miriam:ncbigene:5603"
      hgnc "NA"
      map_id "W9_37"
      name "f53ff"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f53ff"
      uniprot "NA"
    ]
    graphics [
      x 711.2950574545428
      y 991.4430137123738
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_58"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id79ad2aef"
      uniprot "NA"
    ]
    graphics [
      x 785.015018262148
      y 837.2020391037323
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_70"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idcac13c17"
      uniprot "NA"
    ]
    graphics [
      x 619.0286281861603
      y 863.0266735253523
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_42"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id198c85f"
      uniprot "NA"
    ]
    graphics [
      x 705.6444905861586
      y 771.0133325056914
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_57"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id6fd5bed2"
      uniprot "NA"
    ]
    graphics [
      x 881.2364724647522
      y 913.9716325755235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_54"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id60fca41b"
      uniprot "NA"
    ]
    graphics [
      x 495.6916236099462
      y 890.5031301257604
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000175197"
      hgnc "NA"
      map_id "W9_1"
      name "DDIT3"
      node_subtype "GENE"
      node_type "species"
      org_id "a158c"
      uniprot "NA"
    ]
    graphics [
      x 336.71644697155676
      y 768.4302929754563
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_73"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "ide06bd151"
      uniprot "NA"
    ]
    graphics [
      x 217.006228743561
      y 657.9603976441282
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_12"
      name "Apoptosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "be42e"
      uniprot "NA"
    ]
    graphics [
      x 94.47196059318014
      y 600.1538883790403
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_61"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id81035407"
      uniprot "NA"
    ]
    graphics [
      x 97.20203683979764
      y 715.5174803516886
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000171791"
      hgnc "NA"
      map_id "W9_27"
      name "BCL2"
      node_subtype "GENE"
      node_type "species"
      org_id "e21a3"
      uniprot "NA"
    ]
    graphics [
      x 221.60131876005994
      y 784.9973209825134
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_74"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "ide37bb3b6"
      uniprot "NA"
    ]
    graphics [
      x 406.9173734731425
      y 724.169916186396
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_50"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id4b1d8b89"
      uniprot "NA"
    ]
    graphics [
      x 143.0064656633516
      y 896.5837014679497
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_19"
      name "Autophagy"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "cc8d3"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 827.7743711527121
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ncbigene:5599;urn:miriam:ncbigene:5601;urn:miriam:ncbigene:5602"
      hgnc "NA"
      map_id "W9_17"
      name "ca580"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ca580"
      uniprot "NA"
    ]
    graphics [
      x 597.5687223131923
      y 653.4614609533239
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_46"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id352f2389"
      uniprot "NA"
    ]
    graphics [
      x 550.1054769090701
      y 470.86095840445773
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_65"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idac31cf2f"
      uniprot "NA"
    ]
    graphics [
      x 547.2902808879438
      y 824.7831939269469
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_72"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "ide012a8a5"
      uniprot "NA"
    ]
    graphics [
      x 721.718434875487
      y 648.3067865116705
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_51"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id54d4a9a9"
      uniprot "NA"
    ]
    graphics [
      x 639.6133734372314
      y 509.59767416451626
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_44"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id2520366d"
      uniprot "NA"
    ]
    graphics [
      x 833.4686121887473
      y 742.746101894681
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000170345;urn:miriam:ensembl:ENSG00000177606;urn:miriam:ncbigene:3726"
      hgnc "NA"
      map_id "W9_22"
      name "d382f"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d382f"
      uniprot "NA"
    ]
    graphics [
      x 1051.860214495654
      y 824.4001770891366
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_75"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ide8866e40"
      uniprot "NA"
    ]
    graphics [
      x 1177.173252345211
      y 897.0676188054332
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_6"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ad2dd"
      uniprot "NA"
    ]
    graphics [
      x 1194.5919019768803
      y 695.8809637623734
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ea117"
      uniprot "NA"
    ]
    graphics [
      x 1062.2614911926214
      y 679.3365667053575
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_52"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id559f5826"
      uniprot "NA"
    ]
    graphics [
      x 1147.831577776283
      y 986.4269922467552
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f1e34"
      uniprot "NA"
    ]
    graphics [
      x 1015.1493245127538
      y 957.4017470674069
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_69"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idc72f872e"
      uniprot "NA"
    ]
    graphics [
      x 1258.7011601818053
      y 810.870631348349
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ncbigene:3429"
      hgnc "NA"
      map_id "W9_39"
      name "IFI27"
      node_subtype "GENE"
      node_type "species"
      org_id "f93d3"
      uniprot "NA"
    ]
    graphics [
      x 1378.0654957239465
      y 765.1210083437661
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ncbigene:3429"
      hgnc "NA"
      map_id "W9_15"
      name "IFI27"
      node_subtype "GENE"
      node_type "species"
      org_id "c5ac8"
      uniprot "NA"
    ]
    graphics [
      x 1376.9666850584836
      y 857.0551435806327
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ncbigene:684"
      hgnc "NA"
      map_id "W9_2"
      name "BST2"
      node_subtype "GENE"
      node_type "species"
      org_id "a24ed"
      uniprot "NA"
    ]
    graphics [
      x 1005.8142047918167
      y 1079.38105709249
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ncbigene:684"
      hgnc "NA"
      map_id "W9_8"
      name "BST2"
      node_subtype "GENE"
      node_type "species"
      org_id "b17a4"
      uniprot "NA"
    ]
    graphics [
      x 923.4585077572083
      y 1031.60252894327
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_18"
      name "Innate_br_immunity"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "cc6a0"
      uniprot "NA"
    ]
    graphics [
      x 1252.6223959227896
      y 1107.1340368180026
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_48"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id3d6c0762"
      uniprot "NA"
    ]
    graphics [
      x 1348.9007846697164
      y 1216.0126126960295
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000115966;urn:miriam:ensembl:ENSG00000170345"
      hgnc "NA"
      map_id "W9_10"
      name "b90b1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b90b1"
      uniprot "NA"
    ]
    graphics [
      x 1410.8387982664472
      y 1353.53298731953
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_60"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id80dca1f4"
      uniprot "NA"
    ]
    graphics [
      x 1462.0619861761215
      y 1283.823302991581
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_23"
      name "Cell_space_survival_space__br_and_space_proliferation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "d8558"
      uniprot "NA"
    ]
    graphics [
      x 1351.3921581177292
      y 1400.1626614480406
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000185201"
      hgnc "NA"
      map_id "W9_5"
      name "IFITM2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "acf10"
      uniprot "NA"
    ]
    graphics [
      x 1140.3151228714944
      y 585.9098092917575
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000185201"
      hgnc "NA"
      map_id "W9_35"
      name "IFITM2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f247c"
      uniprot "NA"
    ]
    graphics [
      x 1054.7312978187776
      y 561.5294678190162
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000142089"
      hgnc "NA"
      map_id "W9_36"
      name "IFITM3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f50e2"
      uniprot "NA"
    ]
    graphics [
      x 1261.4433871930564
      y 586.3827531379662
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000142089"
      hgnc "NA"
      map_id "W9_33"
      name "IFITM3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f1794"
      uniprot "NA"
    ]
    graphics [
      x 1315.5951225436488
      y 658.2512570419242
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000185885"
      hgnc "NA"
      map_id "W9_21"
      name "IFITM1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "cf41c"
      uniprot "NA"
    ]
    graphics [
      x 1162.3652898466544
      y 797.7984824389788
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000185885"
      hgnc "NA"
      map_id "W9_9"
      name "IFITM1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b8cbc"
      uniprot "NA"
    ]
    graphics [
      x 1283.2514391883315
      y 956.4742016786887
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_25"
      name "SARS,_space_229E_br_IBV_space_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "e046c"
      uniprot "NA"
    ]
    graphics [
      x 698.8828962798334
      y 375.17791018041504
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_76"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "ided0c8de7"
      uniprot "NA"
    ]
    graphics [
      x 733.6121933750616
      y 263.64048009401245
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000076984"
      hgnc "NA"
      map_id "W9_4"
      name "MAP2K7"
      node_subtype "GENE"
      node_type "species"
      org_id "ac39a"
      uniprot "NA"
    ]
    graphics [
      x 601.5614007762871
      y 286.56284802146655
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_45"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id264ad72f"
      uniprot "NA"
    ]
    graphics [
      x 671.0769644308766
      y 168.00570281578177
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000173327;urn:miriam:ensembl:ENSG00000130758;urn:miriam:ensembl:ENSG00000006432"
      hgnc "NA"
      map_id "W9_11"
      name "b9bb1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b9bb1"
      uniprot "NA"
    ]
    graphics [
      x 640.6641490861434
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000065559"
      hgnc "NA"
      map_id "W9_13"
      name "MAP2K4"
      node_subtype "GENE"
      node_type "species"
      org_id "bed55"
      uniprot "NA"
    ]
    graphics [
      x 846.5522286711429
      y 655.7767119290131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_63"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id9808cc66"
      uniprot "NA"
    ]
    graphics [
      x 914.0038400304284
      y 518.798344262875
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000085511;urn:miriam:ensembl:ENSG00000095015"
      hgnc "NA"
      map_id "W9_16"
      name "c8921"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c8921"
      uniprot "NA"
    ]
    graphics [
      x 941.4463773181419
      y 390.27950549505016
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_71"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idda26811d"
      uniprot "NA"
    ]
    graphics [
      x 823.0538406055852
      y 434.67883812471285
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000034152;urn:miriam:ensembl:ENSG00000108984"
      hgnc "NA"
      map_id "W9_30"
      name "e6b7b"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e6b7b"
      uniprot "NA"
    ]
    graphics [
      x 721.9644956469301
      y 548.9678968862634
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_43"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id1b1215f4"
      uniprot "NA"
    ]
    graphics [
      x 599.6909429384326
      y 423.7751481408625
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_28"
      name "SARS,_space_MERS,_space__br_229E_space_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "e5c80"
      uniprot "NA"
    ]
    graphics [
      x 450.54170153990907
      y 431.9235282205158
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_78"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idfb2829ab"
      uniprot "NA"
    ]
    graphics [
      x 434.87083719095267
      y 570.4660213731366
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:uniprot:P59635;urn:miriam:uniprot:P59632"
      hgnc "NA"
      map_id "W9_26"
      name "e11b1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e11b1"
      uniprot "UNIPROT:P59635;UNIPROT:P59632"
    ]
    graphics [
      x 511.67570114452667
      y 713.8279661335467
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:wikidata:Q89458416;urn:miriam:uniprot:P59632;urn:miriam:uniprot:P59635;urn:miriam:uniprot:P59634;urn:miriam:uniprot:P59633;urn:miriam:wikidata:Q89457519"
      hgnc "NA"
      map_id "W9_7"
      name "afd54"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "afd54"
      uniprot "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59634;UNIPROT:P59633"
    ]
    graphics [
      x 499.6071519809012
      y 955.1194128747402
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000169032;urn:miriam:ensembl:ENSG00000126934"
      hgnc "NA"
      map_id "W9_29"
      name "e5ca8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e5ca8"
      uniprot "NA"
    ]
    graphics [
      x 1015.7187094629091
      y 1586.4284945914253
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_55"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id62eba7d3"
      uniprot "NA"
    ]
    graphics [
      x 965.7318034861933
      y 1457.264996075468
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_66"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idbeaaca30"
      uniprot "NA"
    ]
    graphics [
      x 891.2664575830502
      y 1642.832733250029
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000132155"
      hgnc "NA"
      map_id "W9_41"
      name "RAF1"
      node_subtype "GENE"
      node_type "species"
      org_id "fd989"
      uniprot "NA"
    ]
    graphics [
      x 842.3203862128247
      y 1751.007085648619
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_14"
      name "SARS,_space_MERS,_space_229E_br_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "bf897"
      uniprot "NA"
    ]
    graphics [
      x 1107.5854728012098
      y 1388.9026008381034
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 79
    source 1
    target 2
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_32"
      target_id "W9_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 2
    target 3
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_62"
      target_id "W9_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 81
    source 4
    target 3
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_59"
      target_id "W9_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 82
    source 5
    target 3
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_67"
      target_id "W9_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 83
    source 3
    target 6
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_38"
      target_id "W9_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 84
    source 3
    target 7
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_38"
      target_id "W9_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 85
    source 78
    target 4
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_14"
      target_id "W9_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 86
    source 74
    target 5
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_29"
      target_id "W9_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 87
    source 6
    target 50
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_64"
      target_id "W9_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 88
    source 8
    target 7
    cd19dm [
      diagram "WP4877"
      edge_type "INHIBITION"
      source_id "W9_40"
      target_id "W9_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 89
    source 7
    target 9
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_77"
      target_id "W9_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 90
    source 8
    target 10
    cd19dm [
      diagram "WP4877"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W9_40"
      target_id "W9_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 91
    source 10
    target 9
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_53"
      target_id "W9_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 92
    source 9
    target 11
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_24"
      target_id "W9_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 93
    source 9
    target 12
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_24"
      target_id "W9_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 94
    source 17
    target 10
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_37"
      target_id "W9_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 95
    source 11
    target 52
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_68"
      target_id "W9_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 96
    source 12
    target 13
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_56"
      target_id "W9_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 97
    source 14
    target 13
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_47"
      target_id "W9_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 98
    source 15
    target 14
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_20"
      target_id "W9_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 99
    source 16
    target 15
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_49"
      target_id "W9_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 100
    source 17
    target 16
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_37"
      target_id "W9_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 18
    target 17
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_58"
      target_id "W9_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 19
    target 17
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_70"
      target_id "W9_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 20
    target 17
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_42"
      target_id "W9_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 17
    target 21
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_37"
      target_id "W9_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 17
    target 22
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_37"
      target_id "W9_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 106
    source 64
    target 18
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_13"
      target_id "W9_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 107
    source 72
    target 19
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_26"
      target_id "W9_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 108
    source 68
    target 20
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_30"
      target_id "W9_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 109
    source 21
    target 37
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_57"
      target_id "W9_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 110
    source 22
    target 23
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_54"
      target_id "W9_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 111
    source 23
    target 24
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_1"
      target_id "W9_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 112
    source 24
    target 25
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_73"
      target_id "W9_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 113
    source 26
    target 25
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_61"
      target_id "W9_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 114
    source 27
    target 26
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_27"
      target_id "W9_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 28
    target 27
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_74"
      target_id "W9_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 27
    target 29
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_27"
      target_id "W9_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 31
    target 28
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_17"
      target_id "W9_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 29
    target 30
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_50"
      target_id "W9_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 32
    target 31
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_46"
      target_id "W9_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 33
    target 31
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_65"
      target_id "W9_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 34
    target 31
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_72"
      target_id "W9_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 35
    target 31
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_51"
      target_id "W9_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 31
    target 36
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_17"
      target_id "W9_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 61
    target 32
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_4"
      target_id "W9_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 73
    target 33
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_7"
      target_id "W9_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 126
    source 64
    target 34
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_13"
      target_id "W9_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 127
    source 59
    target 35
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_25"
      target_id "W9_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 128
    source 36
    target 37
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_44"
      target_id "W9_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 37
    target 38
    cd19dm [
      diagram "WP4877"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W9_22"
      target_id "W9_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 130
    source 37
    target 39
    cd19dm [
      diagram "WP4877"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W9_22"
      target_id "W9_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 131
    source 37
    target 40
    cd19dm [
      diagram "WP4877"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W9_22"
      target_id "W9_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 132
    source 37
    target 41
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_22"
      target_id "W9_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 133
    source 37
    target 42
    cd19dm [
      diagram "WP4877"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W9_22"
      target_id "W9_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 134
    source 37
    target 43
    cd19dm [
      diagram "WP4877"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W9_22"
      target_id "W9_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 135
    source 57
    target 38
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_21"
      target_id "W9_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 136
    source 38
    target 58
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_75"
      target_id "W9_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 137
    source 55
    target 39
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_36"
      target_id "W9_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 138
    source 39
    target 56
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_6"
      target_id "W9_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 139
    source 53
    target 40
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_5"
      target_id "W9_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 40
    target 54
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_31"
      target_id "W9_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 41
    target 48
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_52"
      target_id "W9_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 46
    target 42
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_2"
      target_id "W9_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 42
    target 47
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_34"
      target_id "W9_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 144
    source 44
    target 43
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_39"
      target_id "W9_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 145
    source 43
    target 45
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_69"
      target_id "W9_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 146
    source 49
    target 48
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_48"
      target_id "W9_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 147
    source 50
    target 49
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_10"
      target_id "W9_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 148
    source 50
    target 51
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_10"
      target_id "W9_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 149
    source 51
    target 52
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_60"
      target_id "W9_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 150
    source 59
    target 60
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_25"
      target_id "W9_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 151
    source 60
    target 61
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_76"
      target_id "W9_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 152
    source 62
    target 61
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_45"
      target_id "W9_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 153
    source 63
    target 62
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_11"
      target_id "W9_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 154
    source 65
    target 64
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_63"
      target_id "W9_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 155
    source 66
    target 65
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_16"
      target_id "W9_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 156
    source 66
    target 67
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_16"
      target_id "W9_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 157
    source 67
    target 68
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_71"
      target_id "W9_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 158
    source 69
    target 68
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_43"
      target_id "W9_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 159
    source 70
    target 69
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_28"
      target_id "W9_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 160
    source 70
    target 71
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_28"
      target_id "W9_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 161
    source 71
    target 72
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_78"
      target_id "W9_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 162
    source 75
    target 74
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_55"
      target_id "W9_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 163
    source 76
    target 74
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_66"
      target_id "W9_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 164
    source 78
    target 75
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_14"
      target_id "W9_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 165
    source 77
    target 76
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_41"
      target_id "W9_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
