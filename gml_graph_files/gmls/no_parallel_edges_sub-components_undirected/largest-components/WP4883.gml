# generated with VANTED V2.8.2 at Fri Mar 04 10:04:40 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_6"
      name "Pro_minus_atrophy_br_Pro_minus_Inflammation_br_Vasoconstriction_br_Pro_minus_oxidant_br_Pro_minus_fibrosis"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b0920"
      uniprot "NA"
    ]
    graphics [
      x 562.3872598627831
      y 304.6453405130137
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:32125455"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_35"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id93eee6dc"
      uniprot "NA"
    ]
    graphics [
      x 523.0608929884374
      y 181.1521013963578
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "PUBMED:32125455"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_37"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idb0d71735"
      uniprot "NA"
    ]
    graphics [
      x 652.4132441239377
      y 400.4101452713585
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_12"
      name "Tissue_space_injury"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "cfb7f"
      uniprot "NA"
    ]
    graphics [
      x 765.4587160877484
      y 472.2780098295663
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "urn:miriam:ncbigene:185"
      hgnc "NA"
      map_id "W11_24"
      name "AT1R"
      node_subtype "GENE"
      node_type "species"
      org_id "f6bb2"
      uniprot "NA"
    ]
    graphics [
      x 565.9678324468829
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "PUBMED:32125455"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_32"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id7114ee4c"
      uniprot "NA"
    ]
    graphics [
      x 688.8327713442632
      y 65.50511780447721
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "urn:miriam:obo.chebi:2719"
      hgnc "NA"
      map_id "W11_14"
      name "Angiotensin_space_2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "d23b7"
      uniprot "NA"
    ]
    graphics [
      x 744.3634261283241
      y 181.84071550871835
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "PUBMED:32125455"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_29"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id45cf6f5e"
      uniprot "NA"
    ]
    graphics [
      x 748.3731120589741
      y 323.8584878984914
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "urn:miriam:obo.chebi:2718"
      hgnc "NA"
      map_id "W11_16"
      name "Angiotensin_space_1"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "d391a"
      uniprot "NA"
    ]
    graphics [
      x 686.8702449257316
      y 480.5418354992815
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "urn:miriam:ncbigene:1636"
      hgnc "NA"
      map_id "W11_22"
      name "ACE"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e5d6d"
      uniprot "NA"
    ]
    graphics [
      x 860.6696098913504
      y 294.1726764286133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "PUBMED:32125455"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id695320d0"
      uniprot "NA"
    ]
    graphics [
      x 663.7734938080769
      y 618.2880470623884
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "urn:miriam:ncbigene:183"
      hgnc "NA"
      map_id "W11_23"
      name "AGT"
      node_subtype "GENE"
      node_type "species"
      org_id "f53bd"
      uniprot "NA"
    ]
    graphics [
      x 719.5578973881209
      y 726.3038205594208
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "urn:miriam:ncbigene:5972"
      hgnc "NA"
      map_id "W11_9"
      name "REN"
      node_subtype "GENE"
      node_type "species"
      org_id "c91b1"
      uniprot "NA"
    ]
    graphics [
      x 609.1876997098668
      y 724.6554421909007
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 14
    source 2
    target 1
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_35"
      target_id "W11_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 15
    source 1
    target 3
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "W11_6"
      target_id "W11_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 16
    source 5
    target 2
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "W11_24"
      target_id "W11_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 17
    source 3
    target 4
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_37"
      target_id "W11_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 18
    source 6
    target 5
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_32"
      target_id "W11_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 19
    source 7
    target 6
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "W11_14"
      target_id "W11_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 20
    source 8
    target 7
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_29"
      target_id "W11_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 21
    source 9
    target 8
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "W11_16"
      target_id "W11_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 22
    source 10
    target 8
    cd19dm [
      diagram "WP4883"
      edge_type "UNKNOWN_CATALYSIS"
      source_id "W11_22"
      target_id "W11_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 23
    source 11
    target 9
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_31"
      target_id "W11_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 24
    source 12
    target 11
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "W11_23"
      target_id "W11_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 25
    source 13
    target 11
    cd19dm [
      diagram "WP4883"
      edge_type "UNKNOWN_CATALYSIS"
      source_id "W11_9"
      target_id "W11_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
