# generated with VANTED V2.8.2 at Fri Mar 04 10:04:40 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ncbigene:23476;urn:miriam:ncbigene:23476;urn:miriam:hgnc:13575;urn:miriam:refseq:NM_058243;urn:miriam:hgnc.symbol:BRD4;urn:miriam:uniprot:O60885;urn:miriam:uniprot:O60885;urn:miriam:hgnc.symbol:BRD4;urn:miriam:ensembl:ENSG00000141867"
      hgnc "HGNC_SYMBOL:BRD4"
      map_id "M17_38"
      name "BRD4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa12"
      uniprot "UNIPROT:O60885"
    ]
    graphics [
      x 1232.2441532563103
      y 604.434823859935
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re4"
      uniprot "NA"
    ]
    graphics [
      x 1285.7402322192133
      y 706.0123701110336
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ncbiprotein:BCD58755;urn:miriam:uniprot:E"
      hgnc "NA"
      map_id "M17_39"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa13"
      uniprot "UNIPROT:E"
    ]
    graphics [
      x 1354.0433225427741
      y 615.3909767818326
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ncbigene:23476;urn:miriam:ncbigene:23476;urn:miriam:hgnc:13575;urn:miriam:refseq:NM_058243;urn:miriam:hgnc.symbol:BRD4;urn:miriam:uniprot:O60885;urn:miriam:uniprot:O60885;urn:miriam:hgnc.symbol:BRD4;urn:miriam:ensembl:ENSG00000141867"
      hgnc "HGNC_SYMBOL:BRD4"
      map_id "M17_42"
      name "BRD4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa16"
      uniprot "UNIPROT:O60885"
    ]
    graphics [
      x 1305.131948078403
      y 826.2863521317382
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_9"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1"
      uniprot "NA"
    ]
    graphics [
      x 1223.899547368039
      y 983.473082149278
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_19"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re2"
      uniprot "NA"
    ]
    graphics [
      x 1160.5211895838145
      y 897.8488314989754
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_23"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re3"
      uniprot "NA"
    ]
    graphics [
      x 1451.2328742287386
      y 687.7316106814021
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ncbigene:8350;urn:miriam:uniprot:P68431;urn:miriam:uniprot:P68431;urn:miriam:ncbigene:8350;urn:miriam:hgnc:4766;urn:miriam:refseq:NM_003529;urn:miriam:hgnc.symbol:H3C1;urn:miriam:ensembl:ENSG00000275714;urn:miriam:hgnc.symbol:H3C1;urn:miriam:ensembl:ENSG00000278637;urn:miriam:hgnc:4781;urn:miriam:ncbigene:8359;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:refseq:NM_003538;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:hgnc:4794;urn:miriam:ncbigene:8370;urn:miriam:ensembl:ENSG00000270882;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:refseq:NM_003548;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:hgnc.symbol:H4C14;urn:miriam:interpro:IPR002119;urn:miriam:hgnc.symbol:H3C15;urn:miriam:hgnc.symbol:H3C15;urn:miriam:ncbigene:333932;urn:miriam:ncbigene:126961;urn:miriam:hgnc:20505;urn:miriam:uniprot:Q71DI3;urn:miriam:uniprot:Q71DI3;urn:miriam:ensembl:ENSG00000203852;urn:miriam:refseq:NM_001005464;urn:miriam:hgnc:4760;urn:miriam:hgnc.symbol:H2BC21;urn:miriam:hgnc.symbol:H2BC21;urn:miriam:ensembl:ENSG00000184678;urn:miriam:refseq:NM_003528;urn:miriam:uniprot:Q16778;urn:miriam:uniprot:Q16778;urn:miriam:ncbigene:8349;urn:miriam:ncbigene:8349;urn:miriam:hgnc:4793;urn:miriam:ncbigene:8294;urn:miriam:ensembl:ENSG00000276180;urn:miriam:hgnc.symbol:H4C9;urn:miriam:refseq:NM_003495;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:ensembl:ENSG00000197837;urn:miriam:hgnc:20510;urn:miriam:refseq:NM_175054;urn:miriam:hgnc.symbol:H4-16;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:ncbigene:121504"
      hgnc "HGNC_SYMBOL:H3C1;HGNC_SYMBOL:H4C1;HGNC_SYMBOL:H4C14;HGNC_SYMBOL:H3C15;HGNC_SYMBOL:H2BC21;HGNC_SYMBOL:H4C9;HGNC_SYMBOL:H4-16"
      map_id "M17_3"
      name "histone"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa2"
      uniprot "UNIPROT:P68431;UNIPROT:P62805;UNIPROT:Q71DI3;UNIPROT:Q16778"
    ]
    graphics [
      x 1375.2311978661144
      y 434.34783412189137
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:refseq:NM_001113182;urn:miriam:hgnc:1103;urn:miriam:uniprot:P25440;urn:miriam:uniprot:P25440;urn:miriam:ncbigene:6046;urn:miriam:ncbigene:6046;urn:miriam:ensembl:ENSG00000204256;urn:miriam:hgnc.symbol:BRD2;urn:miriam:hgnc.symbol:BRD2"
      hgnc "HGNC_SYMBOL:BRD2"
      map_id "M17_48"
      name "BRD2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa21"
      uniprot "UNIPROT:P25440"
    ]
    graphics [
      x 1522.1873651584874
      y 879.9478970862299
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc:4760;urn:miriam:hgnc.symbol:H2BC21;urn:miriam:hgnc.symbol:H2BC21;urn:miriam:ensembl:ENSG00000184678;urn:miriam:refseq:NM_003528;urn:miriam:uniprot:Q16778;urn:miriam:uniprot:Q16778;urn:miriam:ncbigene:8349;urn:miriam:ncbigene:8349;urn:miriam:ncbigene:8350;urn:miriam:uniprot:P68431;urn:miriam:uniprot:P68431;urn:miriam:ncbigene:8350;urn:miriam:hgnc:4766;urn:miriam:refseq:NM_003529;urn:miriam:hgnc.symbol:H3C1;urn:miriam:ensembl:ENSG00000275714;urn:miriam:hgnc.symbol:H3C1;urn:miriam:hgnc:4793;urn:miriam:ncbigene:8294;urn:miriam:ensembl:ENSG00000276180;urn:miriam:hgnc.symbol:H4C9;urn:miriam:refseq:NM_003495;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:ensembl:ENSG00000278637;urn:miriam:hgnc:4781;urn:miriam:ncbigene:8359;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:refseq:NM_003538;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:ensembl:ENSG00000197837;urn:miriam:hgnc:20510;urn:miriam:refseq:NM_175054;urn:miriam:hgnc.symbol:H4-16;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:ncbigene:121504;urn:miriam:hgnc.symbol:H3C15;urn:miriam:hgnc.symbol:H3C15;urn:miriam:ncbigene:333932;urn:miriam:ncbigene:126961;urn:miriam:hgnc:20505;urn:miriam:uniprot:Q71DI3;urn:miriam:uniprot:Q71DI3;urn:miriam:ensembl:ENSG00000203852;urn:miriam:refseq:NM_001005464;urn:miriam:hgnc:4794;urn:miriam:ncbigene:8370;urn:miriam:ensembl:ENSG00000270882;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:refseq:NM_003548;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:hgnc.symbol:H4C14;urn:miriam:interpro:IPR002119"
      hgnc "HGNC_SYMBOL:H2BC21;HGNC_SYMBOL:H3C1;HGNC_SYMBOL:H4C9;HGNC_SYMBOL:H4C1;HGNC_SYMBOL:H4-16;HGNC_SYMBOL:H3C15;HGNC_SYMBOL:H4C14"
      map_id "M17_1"
      name "histone"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa1"
      uniprot "UNIPROT:Q16778;UNIPROT:P68431;UNIPROT:P62805;UNIPROT:Q71DI3"
    ]
    graphics [
      x 1593.199435423655
      y 667.9116113173742
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "PUBMED:18406326"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_24"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re30"
      uniprot "NA"
    ]
    graphics [
      x 1721.3820656612752
      y 645.4876502367766
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_73"
      name "Chromatin_space_organization"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa78"
      uniprot "NA"
    ]
    graphics [
      x 1667.929814784327
      y 548.5327786505337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_33"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re6"
      uniprot "NA"
    ]
    graphics [
      x 1623.398957455446
      y 1011.0703710207175
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re7"
      uniprot "NA"
    ]
    graphics [
      x 1463.2280389047523
      y 1028.462676808664
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000112592;urn:miriam:ncbigene:6908;urn:miriam:ncbigene:6908;urn:miriam:uniprot:P20226;urn:miriam:uniprot:P20226;urn:miriam:hgnc:11588;urn:miriam:hgnc.symbol:TBP;urn:miriam:hgnc.symbol:TBP;urn:miriam:refseq:NM_003194"
      hgnc "HGNC_SYMBOL:TBP"
      map_id "M17_44"
      name "TBP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa18"
      uniprot "UNIPROT:P20226"
    ]
    graphics [
      x 1336.6807162416724
      y 1076.5770013952258
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000112592;urn:miriam:ncbigene:6908;urn:miriam:ncbigene:6908;urn:miriam:uniprot:P20226;urn:miriam:uniprot:P20226;urn:miriam:hgnc:11588;urn:miriam:hgnc.symbol:TBP;urn:miriam:hgnc.symbol:TBP;urn:miriam:refseq:NM_003194"
      hgnc "HGNC_SYMBOL:TBP"
      map_id "M17_43"
      name "TBP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa17"
      uniprot "UNIPROT:P20226"
    ]
    graphics [
      x 1398.6736131453997
      y 1143.9427676248827
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:refseq:NM_001113182;urn:miriam:hgnc:1103;urn:miriam:uniprot:P25440;urn:miriam:uniprot:P25440;urn:miriam:ncbigene:6046;urn:miriam:ncbigene:6046;urn:miriam:ensembl:ENSG00000204256;urn:miriam:hgnc.symbol:BRD2;urn:miriam:hgnc.symbol:BRD2"
      hgnc "HGNC_SYMBOL:BRD2"
      map_id "M17_47"
      name "BRD2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa20"
      uniprot "UNIPROT:P25440"
    ]
    graphics [
      x 1597.2971932628102
      y 1130.121648623668
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ncbiprotein:BCD58755;urn:miriam:uniprot:E"
      hgnc "NA"
      map_id "M17_45"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa19"
      uniprot "UNIPROT:E"
    ]
    graphics [
      x 1731.6450144843875
      y 1064.0850095231758
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:pubchem.compound:46907787"
      hgnc "NA"
      map_id "M17_74"
      name "JQ_minus_1"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa79"
      uniprot "NA"
    ]
    graphics [
      x 1663.5840365268814
      y 916.0627733792948
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_32"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re5"
      uniprot "NA"
    ]
    graphics [
      x 1309.8211191671555
      y 175.98989560289988
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc:4794;urn:miriam:ncbigene:8370;urn:miriam:ensembl:ENSG00000270882;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:refseq:NM_003548;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:hgnc.symbol:H4C14"
      hgnc "HGNC_SYMBOL:H4C1;HGNC_SYMBOL:H4C14"
      map_id "M17_76"
      name "H4C14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa9"
      uniprot "UNIPROT:P62805"
    ]
    graphics [
      x 1224.2465951451907
      y 297.20848280266875
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000197837;urn:miriam:hgnc:20510;urn:miriam:refseq:NM_175054;urn:miriam:hgnc.symbol:H4-16;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:ncbigene:121504"
      hgnc "HGNC_SYMBOL:H4-16;HGNC_SYMBOL:H4C1"
      map_id "M17_75"
      name "H4_minus_16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa8"
      uniprot "UNIPROT:P62805"
    ]
    graphics [
      x 1170.3624785847232
      y 235.1192379492424
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000278637;urn:miriam:hgnc:4781;urn:miriam:ncbigene:8359;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:refseq:NM_003538;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504"
      hgnc "HGNC_SYMBOL:H4C1"
      map_id "M17_46"
      name "H4C1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2"
      uniprot "UNIPROT:P62805"
    ]
    graphics [
      x 1256.5544664169106
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:H3C15;urn:miriam:hgnc.symbol:H3C15;urn:miriam:ncbigene:333932;urn:miriam:ncbigene:126961;urn:miriam:hgnc:20505;urn:miriam:uniprot:Q71DI3;urn:miriam:uniprot:Q71DI3;urn:miriam:ensembl:ENSG00000203852;urn:miriam:refseq:NM_001005464"
      hgnc "HGNC_SYMBOL:H3C15"
      map_id "M17_56"
      name "H3C15"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa3"
      uniprot "UNIPROT:Q71DI3"
    ]
    graphics [
      x 1401.891053186562
      y 215.39917715383706
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ncbigene:8350;urn:miriam:uniprot:P68431;urn:miriam:uniprot:P68431;urn:miriam:ncbigene:8350;urn:miriam:hgnc:4766;urn:miriam:refseq:NM_003529;urn:miriam:hgnc.symbol:H3C1;urn:miriam:ensembl:ENSG00000275714;urn:miriam:hgnc.symbol:H3C1"
      hgnc "HGNC_SYMBOL:H3C1"
      map_id "M17_35"
      name "H3C1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1"
      uniprot "UNIPROT:P68431"
    ]
    graphics [
      x 1335.311953023268
      y 285.4064594573686
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc:4793;urn:miriam:ncbigene:8294;urn:miriam:ensembl:ENSG00000276180;urn:miriam:hgnc.symbol:H4C9;urn:miriam:refseq:NM_003495;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504"
      hgnc "HGNC_SYMBOL:H4C9;HGNC_SYMBOL:H4C1"
      map_id "M17_63"
      name "H4C9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa5"
      uniprot "UNIPROT:P62805"
    ]
    graphics [
      x 1142.919656022292
      y 172.5556720805556
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc:4760;urn:miriam:hgnc.symbol:H2BC21;urn:miriam:hgnc.symbol:H2BC21;urn:miriam:ensembl:ENSG00000184678;urn:miriam:refseq:NM_003528;urn:miriam:uniprot:Q16778;urn:miriam:uniprot:Q16778;urn:miriam:ncbigene:8349;urn:miriam:ncbigene:8349"
      hgnc "HGNC_SYMBOL:H2BC21"
      map_id "M17_69"
      name "H2BC21"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa7"
      uniprot "UNIPROT:Q16778"
    ]
    graphics [
      x 1227.5817830440576
      y 218.40944520794415
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:interpro:IPR002119"
      hgnc "NA"
      map_id "M17_64"
      name "H2A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa6"
      uniprot "NA"
    ]
    graphics [
      x 1177.599784416136
      y 112.17304814086515
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:CCNT1;urn:miriam:hgnc.symbol:CCNT1;urn:miriam:refseq:NM_001240;urn:miriam:ensembl:ENSG00000129315;urn:miriam:hgnc:1599;urn:miriam:ncbigene:904;urn:miriam:ncbigene:904;urn:miriam:uniprot:O60563;urn:miriam:uniprot:O60563"
      hgnc "HGNC_SYMBOL:CCNT1"
      map_id "M17_37"
      name "CCNT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa11"
      uniprot "UNIPROT:O60563"
    ]
    graphics [
      x 1053.5083745061042
      y 856.9700841412013
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:CCNT1;urn:miriam:hgnc.symbol:CCNT1;urn:miriam:refseq:NM_001240;urn:miriam:ensembl:ENSG00000129315;urn:miriam:hgnc:1599;urn:miriam:ncbigene:904;urn:miriam:ncbigene:904;urn:miriam:uniprot:O60563;urn:miriam:uniprot:O60563"
      hgnc "HGNC_SYMBOL:CCNT1"
      map_id "M17_40"
      name "CCNT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa14"
      uniprot "UNIPROT:O60563"
    ]
    graphics [
      x 1057.885885485619
      y 1022.2074098138587
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_21"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re28"
      uniprot "NA"
    ]
    graphics [
      x 987.7713026880319
      y 1158.1938487870723
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ec-code:2.7.11.23;urn:miriam:hgnc:1780;urn:miriam:ensembl:ENSG00000136807;urn:miriam:ec-code:2.7.11.22;urn:miriam:refseq:NM_001261;urn:miriam:uniprot:P50750;urn:miriam:uniprot:P50750;urn:miriam:hgnc.symbol:CDK9;urn:miriam:hgnc.symbol:CDK9;urn:miriam:ncbigene:1025;urn:miriam:ncbigene:1025"
      hgnc "HGNC_SYMBOL:CDK9"
      map_id "M17_41"
      name "CDK9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa15"
      uniprot "UNIPROT:P50750"
    ]
    graphics [
      x 1125.8413749624508
      y 1107.4581053764618
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ec-code:2.7.11.23;urn:miriam:hgnc:1780;urn:miriam:ensembl:ENSG00000136807;urn:miriam:ec-code:2.7.11.22;urn:miriam:refseq:NM_001261;urn:miriam:uniprot:P50750;urn:miriam:uniprot:P50750;urn:miriam:hgnc.symbol:CDK9;urn:miriam:hgnc.symbol:CDK9;urn:miriam:ncbigene:1025;urn:miriam:ncbigene:1025;urn:miriam:hgnc.symbol:CCNT1;urn:miriam:hgnc.symbol:CCNT1;urn:miriam:refseq:NM_001240;urn:miriam:ensembl:ENSG00000129315;urn:miriam:hgnc:1599;urn:miriam:ncbigene:904;urn:miriam:ncbigene:904;urn:miriam:uniprot:O60563;urn:miriam:uniprot:O60563"
      hgnc "HGNC_SYMBOL:CDK9;HGNC_SYMBOL:CCNT1"
      map_id "M17_8"
      name "P_minus_TEFb"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa8"
      uniprot "UNIPROT:P50750;UNIPROT:O60563"
    ]
    graphics [
      x 835.450711097013
      y 1265.6533872493123
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_22"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re29"
      uniprot "NA"
    ]
    graphics [
      x 750.5290751005242
      y 1369.3938693670266
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_72"
      name "RNA_space_Polymerase_space_II_minus_dependent_space_Transcription_space_"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa77"
      uniprot "NA"
    ]
    graphics [
      x 725.47503602017
      y 1482.7421640366465
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ec-code:2.7.11.23;urn:miriam:hgnc:1780;urn:miriam:ensembl:ENSG00000136807;urn:miriam:ec-code:2.7.11.22;urn:miriam:refseq:NM_001261;urn:miriam:uniprot:P50750;urn:miriam:uniprot:P50750;urn:miriam:hgnc.symbol:CDK9;urn:miriam:hgnc.symbol:CDK9;urn:miriam:ncbigene:1025;urn:miriam:ncbigene:1025"
      hgnc "HGNC_SYMBOL:CDK9"
      map_id "M17_36"
      name "CDK9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa10"
      uniprot "UNIPROT:P50750"
    ]
    graphics [
      x 1329.5458078561742
      y 962.4961962581499
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 37
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_38"
      target_id "M17_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 38
    source 3
    target 2
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "INHIBITION"
      source_id "M17_39"
      target_id "M17_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 39
    source 2
    target 4
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_31"
      target_id "M17_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 40
    source 4
    target 5
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CATALYSIS"
      source_id "M17_42"
      target_id "M17_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 41
    source 4
    target 6
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CATALYSIS"
      source_id "M17_42"
      target_id "M17_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 42
    source 4
    target 7
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CATALYSIS"
      source_id "M17_42"
      target_id "M17_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 43
    source 36
    target 5
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_36"
      target_id "M17_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 44
    source 5
    target 32
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_9"
      target_id "M17_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 45
    source 29
    target 6
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_37"
      target_id "M17_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 46
    source 6
    target 30
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_19"
      target_id "M17_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 47
    source 8
    target 7
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_3"
      target_id "M17_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 48
    source 9
    target 7
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CATALYSIS"
      source_id "M17_48"
      target_id "M17_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 49
    source 7
    target 10
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_23"
      target_id "M17_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 50
    source 20
    target 8
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_32"
      target_id "M17_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 51
    source 13
    target 9
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_33"
      target_id "M17_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 52
    source 9
    target 14
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CATALYSIS"
      source_id "M17_48"
      target_id "M17_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 53
    source 10
    target 11
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_1"
      target_id "M17_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 54
    source 11
    target 12
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_24"
      target_id "M17_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 55
    source 17
    target 13
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_47"
      target_id "M17_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 56
    source 18
    target 13
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "INHIBITION"
      source_id "M17_45"
      target_id "M17_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 57
    source 19
    target 13
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "INHIBITION"
      source_id "M17_74"
      target_id "M17_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 58
    source 15
    target 14
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_44"
      target_id "M17_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 59
    source 14
    target 16
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_34"
      target_id "M17_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 60
    source 21
    target 20
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_76"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 61
    source 22
    target 20
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_75"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 62
    source 23
    target 20
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_46"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 63
    source 24
    target 20
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_56"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 64
    source 25
    target 20
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_35"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 26
    target 20
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_63"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 27
    target 20
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_69"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 28
    target 20
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_64"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 30
    target 31
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_40"
      target_id "M17_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 69
    source 32
    target 31
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_41"
      target_id "M17_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 70
    source 31
    target 33
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_21"
      target_id "M17_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 71
    source 33
    target 34
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_8"
      target_id "M17_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 72
    source 34
    target 35
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_22"
      target_id "M17_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
