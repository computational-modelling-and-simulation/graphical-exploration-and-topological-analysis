# generated with VANTED V2.8.2 at Fri Mar 04 10:04:32 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_24"
      name "SARS,_space_OC43,_br_MHV,_space_IBV_space_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "cd9a0"
      uniprot "NA"
    ]
    graphics [
      x 444.43151320617136
      y 495.2866463002274
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_88"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idf3808898"
      uniprot "NA"
    ]
    graphics [
      x 459.25107357088535
      y 610.3970690636042
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "urn:miriam:uniprot:Q53XC0"
      hgnc "NA"
      map_id "W3_11"
      name "EIF2A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b6968"
      uniprot "UNIPROT:Q53XC0"
    ]
    graphics [
      x 562.5613690696878
      y 692.9250746666262
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_75"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idb174dd6a"
      uniprot "NA"
    ]
    graphics [
      x 541.6336329779039
      y 451.85845180889703
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_87"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idead04e33"
      uniprot "NA"
    ]
    graphics [
      x 759.3117845511158
      y 808.9178688359592
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_70"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id915467c9"
      uniprot "NA"
    ]
    graphics [
      x 433.53364109272223
      y 738.7204285618187
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_72"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ida575a860"
      uniprot "NA"
    ]
    graphics [
      x 600.9543991372575
      y 882.175531013619
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "urn:miriam:uniprot:P18848"
      hgnc "NA"
      map_id "W3_38"
      name "ATF4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e01f1"
      uniprot "UNIPROT:P18848"
    ]
    graphics [
      x 682.1797274637619
      y 1046.8075231913733
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_80"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idc828ca15"
      uniprot "NA"
    ]
    graphics [
      x 839.55456127616
      y 1144.7883839077963
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_56"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id1f373365"
      uniprot "NA"
    ]
    graphics [
      x 568.3709153106734
      y 1125.0599418356958
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_26"
      name "CRE_br_cAMP_space_response_space_element"
      node_subtype "GENE"
      node_type "species"
      org_id "d0da6"
      uniprot "NA"
    ]
    graphics [
      x 471.7351691360337
      y 1210.5712092162064
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_61"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id5031f464"
      uniprot "NA"
    ]
    graphics [
      x 451.18240298966816
      y 1330.771557228975
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_21"
      name "Amino_space_acid_space_synthesis_br_Antioxidant_space_response_br_Apoptosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "c90ec"
      uniprot "NA"
    ]
    graphics [
      x 532.0402311756011
      y 1417.1235544100764
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "urn:miriam:uniprot:P35638"
      hgnc "NA"
      map_id "W3_28"
      name "CHOP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d2d29"
      uniprot "UNIPROT:P35638"
    ]
    graphics [
      x 1010.1104317930112
      y 1217.3764669391226
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_55"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id17eaab75"
      uniprot "NA"
    ]
    graphics [
      x 964.1882642496121
      y 1069.3873301075903
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_62"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id5555a7cf"
      uniprot "NA"
    ]
    graphics [
      x 1060.3814453792709
      y 1418.299354693846
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_86"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idde832765"
      uniprot "NA"
    ]
    graphics [
      x 1152.6970113545085
      y 1114.2002650735292
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_39"
      name "C_slash_EBP_br_CCAAT_space_enhancer_space_binding_space_protein"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e0713"
      uniprot "NA"
    ]
    graphics [
      x 1289.2704593027995
      y 1021.079143872726
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_57"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id315b7e46"
      uniprot "NA"
    ]
    graphics [
      x 1396.6699385435927
      y 909.4280377081008
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "urn:miriam:ensembl:ENSG00000171791"
      hgnc "NA"
      map_id "W3_44"
      name "BCL2"
      node_subtype "GENE"
      node_type "species"
      org_id "eef8e"
      uniprot "NA"
    ]
    graphics [
      x 1338.4396390737986
      y 816.0292258359565
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "urn:miriam:uniprot:P10415"
      hgnc "NA"
      map_id "W3_19"
      name "BCL2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c7da2"
      uniprot "UNIPROT:P10415"
    ]
    graphics [
      x 1416.2625203853177
      y 765.7247636516985
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "urn:miriam:wikipathways:WP354"
      hgnc "NA"
      map_id "W3_5"
      name "Apoptosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "aaed2"
      uniprot "NA"
    ]
    graphics [
      x 1076.8794693696289
      y 1626.1306218635923
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_52"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "fc5c1"
      uniprot "NA"
    ]
    graphics [
      x 1035.612926565847
      y 1757.9501961901883
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_73"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ida913653e"
      uniprot "NA"
    ]
    graphics [
      x 1173.9937822263162
      y 1632.5194183679391
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_46"
      name "f61a6"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f61a6"
      uniprot "NA"
    ]
    graphics [
      x 1034.3021741435668
      y 1574.406227408347
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id11f5ed11"
      uniprot "NA"
    ]
    graphics [
      x 870.9204188574238
      y 1588.3156084015932
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_45"
      name "f5b2d"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f5b2d"
      uniprot "NA"
    ]
    graphics [
      x 960.2349603346445
      y 1506.6939843414823
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "urn:miriam:uniprot:O75460"
      hgnc "NA"
      map_id "W3_48"
      name "f8553"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f8553"
      uniprot "UNIPROT:O75460"
    ]
    graphics [
      x 692.6749354396395
      y 1610.1574729277381
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_78"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idc7e2ae4"
      uniprot "NA"
    ]
    graphics [
      x 720.2739907690055
      y 1490.7666454847695
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_63"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id74bb08d8"
      uniprot "NA"
    ]
    graphics [
      x 495.56359626356164
      y 1680.850917362373
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_77"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idc4a4b8a1"
      uniprot "NA"
    ]
    graphics [
      x 798.6703346594458
      y 1679.0135177820152
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "urn:miriam:uniprot:P45983"
      hgnc "NA"
      map_id "W3_7"
      name "MAPK8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b098e"
      uniprot "UNIPROT:P45983"
    ]
    graphics [
      x 907.9746573878022
      y 1747.3483413936042
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "urn:miriam:uniprot:P17861"
      hgnc "NA"
      map_id "W3_30"
      name "XBP1u"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d3eb7"
      uniprot "UNIPROT:P17861"
    ]
    graphics [
      x 363.7847790586428
      y 1680.3297645816183
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "urn:miriam:uniprot:P0DTC4"
      hgnc "NA"
      map_id "W3_53"
      name "SARS_space_E"
      node_subtype "GENE"
      node_type "species"
      org_id "fce54"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 572.1782320512373
      y 1762.2426656308708
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "urn:miriam:uniprot:P17861"
      hgnc "NA"
      map_id "W3_13"
      name "XBP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bbd1f"
      uniprot "UNIPROT:P17861"
    ]
    graphics [
      x 321.2614357112606
      y 1733.2680376570825
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_64"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id7f19c7ea"
      uniprot "NA"
    ]
    graphics [
      x 382.7400518334422
      y 1814.002656615241
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_69"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id906309ce"
      uniprot "NA"
    ]
    graphics [
      x 236.00133245173686
      y 1611.7142864808286
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_27"
      name "UPRE_br_unfolded_space_protein_br_response_space_element_br_ERSE_br_ER_space_stress_space__br_response_space_element"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d1b2b"
      uniprot "NA"
    ]
    graphics [
      x 111.82936203084512
      y 1577.7116235091005
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_68"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id858a8309"
      uniprot "NA"
    ]
    graphics [
      x 203.35482301203263
      y 1683.6803946221532
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_74"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idad83fc4d"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 1448.6353505199309
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_17"
      name "ER_space_protein_space_chaperones_br_Lipid_space_biosynthesis_br_ER_minus_associated_space_degradation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "c085b"
      uniprot "NA"
    ]
    graphics [
      x 70.20928050900079
      y 1325.4822722787253
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_6"
      name "Unfolded_br_protein"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "b00dc"
      uniprot "NA"
    ]
    graphics [
      x 733.4740385356502
      y 1379.3138919491155
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "urn:miriam:uniprot:O75807;urn:miriam:wikidata:Q7251492"
      hgnc "NA"
      map_id "W3_37"
      name "df8e0"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "df8e0"
      uniprot "UNIPROT:O75807"
    ]
    graphics [
      x 902.001395136685
      y 919.3572295443943
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_42"
      name "Global_space_protein_br_translation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "ed36d"
      uniprot "NA"
    ]
    graphics [
      x 316.91578860608456
      y 736.8488016493158
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "urn:miriam:uniprot:Q53XC0"
      hgnc "NA"
      map_id "W3_33"
      name "EIF2A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d4ef9"
      uniprot "UNIPROT:Q53XC0"
    ]
    graphics [
      x 637.6098154799665
      y 476.66740824949727
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "urn:miriam:uniprot:P19525;urn:miriam:wikidata:Q2819370"
      hgnc "NA"
      map_id "W3_32"
      name "d477c"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d477c"
      uniprot "UNIPROT:P19525"
    ]
    graphics [
      x 418.81001036320606
      y 315.21305324666525
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "urn:miriam:uniprot:Q9H492;urn:miriam:uniprot:Q9BQI3"
      hgnc "NA"
      map_id "W3_41"
      name "e7f9b"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e7f9b"
      uniprot "UNIPROT:Q9H492;UNIPROT:Q9BQI3"
    ]
    graphics [
      x 602.0075304802455
      y 339.73368383750983
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "urn:miriam:uniprot:Q9NZJ5"
      hgnc "NA"
      map_id "W3_51"
      name "fbd45"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "fbd45"
      uniprot "UNIPROT:Q9NZJ5"
    ]
    graphics [
      x 719.1044714029256
      y 416.67591677527554
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_83"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idcf12fa8a"
      uniprot "NA"
    ]
    graphics [
      x 895.3363526519865
      y 450.4990796088216
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "urn:miriam:uniprot:Q9NZJ5"
      hgnc "NA"
      map_id "W3_3"
      name "PERK"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a8ce8"
      uniprot "UNIPROT:Q9NZJ5"
    ]
    graphics [
      x 1046.1024711888372
      y 480.9555007906796
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_35"
      name "SARS,_space_IBV_space_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "dc80b"
      uniprot "NA"
    ]
    graphics [
      x 844.7683257884244
      y 567.2787311918931
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_79"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idc824ad90"
      uniprot "NA"
    ]
    graphics [
      x 762.265864319282
      y 649.2791949162263
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "urn:miriam:uniprot:P11021;urn:miriam:uniprot:Q9NZJ5"
      hgnc "NA"
      map_id "W3_1"
      name "a0c95"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "a0c95"
      uniprot "UNIPROT:P11021;UNIPROT:Q9NZJ5"
    ]
    graphics [
      x 710.409110816139
      y 754.7919466301996
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "urn:miriam:uniprot:P11021"
      hgnc "NA"
      map_id "W3_34"
      name "dadd6"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "dadd6"
      uniprot "UNIPROT:P11021"
    ]
    graphics [
      x 657.2106310784176
      y 696.7165265923435
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_82"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idcdf3e573"
      uniprot "NA"
    ]
    graphics [
      x 295.47512588615405
      y 202.81410186054484
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "urn:miriam:uniprot:P19525"
      hgnc "NA"
      map_id "W3_18"
      name "PKR"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c0e50"
      uniprot "UNIPROT:P19525"
    ]
    graphics [
      x 312.4682940180874
      y 71.32710126702659
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "urn:miriam:wikidata:Q2819370"
      hgnc "NA"
      map_id "W3_4"
      name "dsRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "a90b0"
      uniprot "NA"
    ]
    graphics [
      x 177.8788258201996
      y 275.3992183806887
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_71"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "ida0444a3e"
      uniprot "NA"
    ]
    graphics [
      x 200.94631447275992
      y 408.4239711441758
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_31"
      name "MERS_space_4a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d43bc"
      uniprot "NA"
    ]
    graphics [
      x 274.5045611301128
      y 338.93493861868626
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4861"
      full_annotation "NA"
      hgnc "NA"
      map_id "W3_58"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id334c961f"
      uniprot "NA"
    ]
    graphics [
      x 437.47707045210603
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4861"
      full_annotation "urn:miriam:wikidata:Q87917579"
      hgnc "NA"
      map_id "W3_50"
      name "nsp15"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "fa46a"
      uniprot "NA"
    ]
    graphics [
      x 546.3093647054197
      y 107.6329527652091
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W3_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 62
    source 1
    target 2
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "W3_24"
      target_id "W3_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 63
    source 2
    target 3
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_88"
      target_id "W3_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 64
    source 4
    target 3
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_75"
      target_id "W3_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 5
    target 3
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_87"
      target_id "W3_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 3
    target 6
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "W3_11"
      target_id "W3_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 3
    target 7
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "W3_11"
      target_id "W3_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 45
    target 4
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "W3_33"
      target_id "W3_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 69
    source 46
    target 4
    cd19dm [
      diagram "WP4861"
      edge_type "CATALYSIS"
      source_id "W3_32"
      target_id "W3_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 70
    source 47
    target 4
    cd19dm [
      diagram "WP4861"
      edge_type "CATALYSIS"
      source_id "W3_41"
      target_id "W3_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 71
    source 48
    target 4
    cd19dm [
      diagram "WP4861"
      edge_type "CATALYSIS"
      source_id "W3_51"
      target_id "W3_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 72
    source 43
    target 5
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "W3_37"
      target_id "W3_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 73
    source 6
    target 44
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_70"
      target_id "W3_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 74
    source 7
    target 8
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_72"
      target_id "W3_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 75
    source 8
    target 9
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "W3_38"
      target_id "W3_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 76
    source 8
    target 10
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "W3_38"
      target_id "W3_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 77
    source 9
    target 14
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_80"
      target_id "W3_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 78
    source 10
    target 11
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_56"
      target_id "W3_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 79
    source 11
    target 12
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "W3_26"
      target_id "W3_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 12
    target 13
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_61"
      target_id "W3_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 81
    source 14
    target 15
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "W3_28"
      target_id "W3_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 82
    source 14
    target 16
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "W3_28"
      target_id "W3_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 83
    source 14
    target 17
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "W3_28"
      target_id "W3_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 84
    source 15
    target 43
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_55"
      target_id "W3_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 85
    source 16
    target 22
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_62"
      target_id "W3_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 86
    source 17
    target 18
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_86"
      target_id "W3_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 87
    source 18
    target 19
    cd19dm [
      diagram "WP4861"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W3_39"
      target_id "W3_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 88
    source 20
    target 19
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "W3_44"
      target_id "W3_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 89
    source 19
    target 21
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_57"
      target_id "W3_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 90
    source 23
    target 22
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_52"
      target_id "W3_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 91
    source 24
    target 22
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_73"
      target_id "W3_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 92
    source 32
    target 23
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "W3_7"
      target_id "W3_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 93
    source 25
    target 24
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "W3_46"
      target_id "W3_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 94
    source 26
    target 25
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_54"
      target_id "W3_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 95
    source 27
    target 26
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "W3_45"
      target_id "W3_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 96
    source 28
    target 26
    cd19dm [
      diagram "WP4861"
      edge_type "CATALYSIS"
      source_id "W3_48"
      target_id "W3_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 97
    source 29
    target 28
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_78"
      target_id "W3_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 98
    source 28
    target 30
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "W3_48"
      target_id "W3_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 99
    source 28
    target 31
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "W3_48"
      target_id "W3_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 100
    source 42
    target 29
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "W3_6"
      target_id "W3_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 33
    target 30
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "W3_30"
      target_id "W3_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 34
    target 30
    cd19dm [
      diagram "WP4861"
      edge_type "INHIBITION"
      source_id "W3_53"
      target_id "W3_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 30
    target 35
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_63"
      target_id "W3_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 31
    target 32
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_77"
      target_id "W3_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 33
    target 39
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "W3_30"
      target_id "W3_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 106
    source 33
    target 36
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "W3_30"
      target_id "W3_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 107
    source 36
    target 35
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_64"
      target_id "W3_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 108
    source 35
    target 37
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "W3_13"
      target_id "W3_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 109
    source 37
    target 38
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_69"
      target_id "W3_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 110
    source 39
    target 38
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_68"
      target_id "W3_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 111
    source 38
    target 40
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "W3_27"
      target_id "W3_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 112
    source 40
    target 41
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_74"
      target_id "W3_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 113
    source 55
    target 46
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_82"
      target_id "W3_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 114
    source 49
    target 48
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_83"
      target_id "W3_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 50
    target 49
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "W3_3"
      target_id "W3_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 51
    target 49
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "W3_35"
      target_id "W3_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 51
    target 52
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "W3_35"
      target_id "W3_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 53
    target 52
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "W3_1"
      target_id "W3_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 52
    target 54
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_79"
      target_id "W3_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 56
    target 55
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "W3_18"
      target_id "W3_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 57
    target 55
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "W3_4"
      target_id "W3_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 60
    target 56
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_58"
      target_id "W3_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 58
    target 57
    cd19dm [
      diagram "WP4861"
      edge_type "PRODUCTION"
      source_id "W3_71"
      target_id "W3_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 59
    target 58
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "W3_31"
      target_id "W3_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 61
    target 60
    cd19dm [
      diagram "WP4861"
      edge_type "CONSPUMPTION"
      source_id "W3_50"
      target_id "W3_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
