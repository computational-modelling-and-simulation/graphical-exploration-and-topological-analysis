# generated with VANTED V2.8.2 at Fri Mar 04 09:53:02 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ncbiprotein:YP_009725306"
      hgnc "NA"
      map_id "W16_13"
      name "nsp10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b19e5"
      uniprot "NA"
    ]
    graphics [
      x 1482.3848162714742
      y 726.804556565537
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_46"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "d1353"
      uniprot "NA"
    ]
    graphics [
      x 1546.7627996342617
      y 617.3934666086975
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000115267"
      hgnc "NA"
      map_id "W16_22"
      name "MDA5_space_"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b93b0"
      uniprot "NA"
    ]
    graphics [
      x 1685.4175797121932
      y 558.6823111352737
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_73"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "e7327"
      uniprot "NA"
    ]
    graphics [
      x 1588.2244165640811
      y 817.3775090894461
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000107201"
      hgnc "NA"
      map_id "W16_52"
      name "RIG_minus_I_space_(DDX58)"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d88ba"
      uniprot "NA"
    ]
    graphics [
      x 1750.2824108394868
      y 786.2259212984633
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000162434;urn:miriam:ensembl:ENSG00000105397"
      hgnc "NA"
      map_id "W16_59"
      name "dd322"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "dd322"
      uniprot "NA"
    ]
    graphics [
      x 793.3082552610936
      y 1096.8787797629454
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_101"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id38b9357c"
      uniprot "NA"
    ]
    graphics [
      x 920.3151817845803
      y 1294.6637484078765
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000115415"
      hgnc "NA"
      map_id "W16_92"
      name "STAT1"
      node_subtype "GENE"
      node_type "species"
      org_id "f9489"
      uniprot "NA"
    ]
    graphics [
      x 1066.0128915820737
      y 1473.1442789665598
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_75"
      name "NFkB"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "e788c"
      uniprot "NA"
    ]
    graphics [
      x 314.0278830813893
      y 1110.6076266717414
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_96"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "fbdcb"
      uniprot "NA"
    ]
    graphics [
      x 301.9924709682225
      y 974.9632587867882
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_53"
      name "Pro_minus_inflammatory_space__br_cytokines_space_production"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "d99d5"
      uniprot "NA"
    ]
    graphics [
      x 345.65781847514904
      y 829.3686208523237
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ncbiprotein:YP_009725311"
      hgnc "NA"
      map_id "W16_39"
      name "nsp16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c8bd9"
      uniprot "NA"
    ]
    graphics [
      x 1898.3467780914339
      y 784.6899081996422
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_10"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "ae85d"
      uniprot "NA"
    ]
    graphics [
      x 1834.5829805247636
      y 885.9635447853254
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_85"
      name "ISGs"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f1bfb"
      uniprot "NA"
    ]
    graphics [
      x 298.29322720796927
      y 315.3698142311753
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_33"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "c04cc"
      uniprot "NA"
    ]
    graphics [
      x 256.8469401203048
      y 440.2441260672772
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000089127;urn:miriam:ensembl:ENSG00000055332;urn:miriam:ensembl:ENSG00000111331;urn:miriam:ensembl:ENSG00000111335"
      hgnc "NA"
      map_id "W16_37"
      name "c82e3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c82e3"
      uniprot "NA"
    ]
    graphics [
      x 126.84506000743238
      y 474.3166552117147
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000131323"
      hgnc "NA"
      map_id "W16_88"
      name "TRAF3"
      node_subtype "GENE"
      node_type "species"
      org_id "f673f"
      uniprot "NA"
    ]
    graphics [
      x 1239.982999274197
      y 1190.918376012966
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "PUBMED:32695122"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_82"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ef27c"
      uniprot "NA"
    ]
    graphics [
      x 1132.442486643879
      y 1181.594299804415
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000263528;urn:miriam:ensembl:ENSG00000183735"
      hgnc "NA"
      map_id "W16_27"
      name "bc0e3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "bc0e3"
      uniprot "NA"
    ]
    graphics [
      x 1022.7002994368469
      y 1147.5782655102958
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_3"
      name "TRIF_br_"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a8003"
      uniprot "NA"
    ]
    graphics [
      x 1316.0571947958933
      y 826.9897878045203
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_2"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "a7711"
      uniprot "NA"
    ]
    graphics [
      x 1286.799699068649
      y 1010.4127892775202
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000198001"
      hgnc "NA"
      map_id "W16_70"
      name "IRAK4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e33f8"
      uniprot "NA"
    ]
    graphics [
      x 927.996202824487
      y 1510.85770510931
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_35"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "c6c90"
      uniprot "NA"
    ]
    graphics [
      x 738.8236670298122
      y 1356.9796897413662
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000175104"
      hgnc "NA"
      map_id "W16_98"
      name "TRAF6"
      node_subtype "GENE"
      node_type "species"
      org_id "fe40f"
      uniprot "NA"
    ]
    graphics [
      x 617.2924152690041
      y 1128.0807773588467
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ncbiprotein:YP_009725310"
      hgnc "NA"
      map_id "W16_31"
      name "nsp15"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "beaa5"
      uniprot "NA"
    ]
    graphics [
      x 1760.8966377743589
      y 593.9304136102219
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_66"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "e1751"
      uniprot "NA"
    ]
    graphics [
      x 1767.8898798067426
      y 484.0391716814958
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:uniprot:Q9Y2C9"
      hgnc "NA"
      map_id "W16_16"
      name "TLR6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b5481"
      uniprot "UNIPROT:Q9Y2C9"
    ]
    graphics [
      x 1141.3142604986615
      y 186.91797851598676
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "b5fd0"
      uniprot "NA"
    ]
    graphics [
      x 996.5575364512163
      y 298.28378402900745
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000172936"
      hgnc "NA"
      map_id "W16_63"
      name "MYD88"
      node_subtype "GENE"
      node_type "species"
      org_id "dfbb7"
      uniprot "NA"
    ]
    graphics [
      x 905.9840856429539
      y 484.28340290293806
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ncbiprotein:YP_009725309"
      hgnc "NA"
      map_id "W16_5"
      name "nsp14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "aaed9"
      uniprot "NA"
    ]
    graphics [
      x 1599.103889987079
      y 551.7579699769175
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_41"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "ca320"
      uniprot "NA"
    ]
    graphics [
      x 1610.799833093984
      y 444.65314028837645
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_12"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "af7a1"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 581.4856739713455
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_34"
      name "Innate_space_Immunity"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "c1ec8"
      uniprot "NA"
    ]
    graphics [
      x 139.5815850272777
      y 676.7450825476137
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000126456"
      hgnc "NA"
      map_id "W16_21"
      name "b8d9b"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b8d9b"
      uniprot "NA"
    ]
    graphics [
      x 807.5056016621515
      y 666.4742738939085
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_112"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idbbf051e0"
      uniprot "NA"
    ]
    graphics [
      x 903.2821515662309
      y 582.9234399731381
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:wikidata:Q6046488"
      hgnc "NA"
      map_id "W16_83"
      name "INF_minus_I_space_alpha_slash__space_beta"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f1657"
      uniprot "NA"
    ]
    graphics [
      x 996.4852998363447
      y 634.1762786236503
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:refseq:QII57165.1"
      hgnc "NA"
      map_id "W16_84"
      name "nsp13"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f1b6a"
      uniprot "NA"
    ]
    graphics [
      x 2112.6551753976664
      y 1313.3730382568083
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "PUBMED:32979938;PUBMED:33337934;PUBMED:32733001"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_113"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idcb3aff58"
      uniprot "NA"
    ]
    graphics [
      x 2137.6043383524966
      y 1425.6530107186752
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000183735"
      hgnc "NA"
      map_id "W16_95"
      name "TBK1"
      node_subtype "GENE"
      node_type "species"
      org_id "fb718"
      uniprot "NA"
    ]
    graphics [
      x 2022.092521256167
      y 1405.9467182209441
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000126456"
      hgnc "NA"
      map_id "W16_74"
      name "IRF3"
      node_subtype "GENE"
      node_type "species"
      org_id "e7683"
      uniprot "NA"
    ]
    graphics [
      x 873.7868590713666
      y 972.8842689815307
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "PUBMED:32695122;PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_43"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "cfeb0"
      uniprot "NA"
    ]
    graphics [
      x 990.4792668560397
      y 862.9503652959067
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000185507;urn:miriam:ensembl:ENSG00000126456"
      hgnc "NA"
      map_id "W16_56"
      name "db1fb"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "db1fb"
      uniprot "NA"
    ]
    graphics [
      x 1087.0696041889676
      y 771.5650407866211
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000159110;urn:miriam:ensembl:ENSG00000142166"
      hgnc "NA"
      map_id "W16_19"
      name "b8407"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b8407"
      uniprot "NA"
    ]
    graphics [
      x 531.9373711471375
      y 888.984058424436
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_81"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "ed722"
      uniprot "NA"
    ]
    graphics [
      x 657.7112427154818
      y 968.1259708009418
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:wikidata:Q6046488"
      hgnc "NA"
      map_id "W16_72"
      name "INF_minus_I_space_alpha_slash__space_beta"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e6fbe"
      uniprot "NA"
    ]
    graphics [
      x 910.4891465146875
      y 898.0096910793814
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_109"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id782ae218"
      uniprot "NA"
    ]
    graphics [
      x 748.7357823007236
      y 944.3141504438921
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:wikidata:Q6046488"
      hgnc "NA"
      map_id "W16_30"
      name "INF_minus_I_space_alpha_slash__space_beta"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bd4e9"
      uniprot "NA"
    ]
    graphics [
      x 576.3784537681975
      y 971.9363179503648
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_38"
      name "SARS_minus_CoV_space_2"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "c8487"
      uniprot "NA"
    ]
    graphics [
      x 1480.1794796215936
      y 238.20613853434315
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_78"
      name "NA"
      node_subtype "POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "eaa62"
      uniprot "NA"
    ]
    graphics [
      x 1682.723069496209
      y 364.5924450321797
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:uniprot:Q9BYF1"
      hgnc "NA"
      map_id "W16_76"
      name "ACE2"
      node_subtype "GENE"
      node_type "species"
      org_id "e92a9"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1878.6761726609516
      y 521.2394279717936
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:uniprot:O15455"
      hgnc "NA"
      map_id "W16_7"
      name "TLR3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ab922"
      uniprot "UNIPROT:O15455"
    ]
    graphics [
      x 1533.4665695867416
      y 1122.219357269671
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "da0cb"
      uniprot "NA"
    ]
    graphics [
      x 1432.6471164838117
      y 928.7726682145425
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_97"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "fc80b"
      uniprot "NA"
    ]
    graphics [
      x 1109.4576596135382
      y 653.7237171540385
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_4"
      name "NA"
      node_subtype "POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "aabad"
      uniprot "NA"
    ]
    graphics [
      x 1315.7424159833477
      y 295.37538637170934
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:uniprot:O00206"
      hgnc "NA"
      map_id "W16_36"
      name "TLR4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c7d93"
      uniprot "UNIPROT:O00206"
    ]
    graphics [
      x 1195.2271380531615
      y 427.8718869988772
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:uniprot:Q9NR96"
      hgnc "NA"
      map_id "W16_9"
      name "TLR9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "adc15"
      uniprot "UNIPROT:Q9NR96"
    ]
    graphics [
      x 1380.9227986944766
      y 1618.6846820813262
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_14"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "b1a40"
      uniprot "NA"
    ]
    graphics [
      x 1256.8525590298582
      y 1733.890567388461
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000172936"
      hgnc "NA"
      map_id "W16_91"
      name "MYD88"
      node_subtype "GENE"
      node_type "species"
      org_id "f8b63"
      uniprot "NA"
    ]
    graphics [
      x 1162.7757857085646
      y 1810.263345442545
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      annotation "PUBMED:32695122"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_93"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "fa67c"
      uniprot "NA"
    ]
    graphics [
      x 954.607293208559
      y 1059.0780476675882
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_1"
      name "SARS_minus_CoV_space_2"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "a63e3"
      uniprot "NA"
    ]
    graphics [
      x 2052.087362431769
      y 119.73251547165569
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      annotation "PUBMED:32695122"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_104"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id5bdc9aa8"
      uniprot "NA"
    ]
    graphics [
      x 1996.9202371570254
      y 256.7276167672163
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:wikidata:Q82069695"
      hgnc "NA"
      map_id "W16_99"
      name "SARS_minus_CoV_space_2_space_RNA"
      node_subtype "RNA"
      node_type "species"
      org_id "ff396"
      uniprot "NA"
    ]
    graphics [
      x 1930.672090020737
      y 406.9004090520194
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_107"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id619b1996"
      uniprot "NA"
    ]
    graphics [
      x 938.7495570918882
      y 751.9474243522368
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ncbiprotein:YP_009725297"
      hgnc "NA"
      map_id "W16_47"
      name "nsp1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d2ec8"
      uniprot "NA"
    ]
    graphics [
      x 1198.119244615656
      y 1271.7064008557413
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_86"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "f2bcd"
      uniprot "NA"
    ]
    graphics [
      x 1189.5490228460915
      y 1396.6275375368016
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:wikidata:Q82069695"
      hgnc "NA"
      map_id "W16_26"
      name "SARS_minus_CoV_space_2_space_RNA"
      node_subtype "RNA"
      node_type "species"
      org_id "bbbdd"
      uniprot "NA"
    ]
    graphics [
      x 1615.623337156002
      y 1484.6419403138211
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_114"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idd0587e82"
      uniprot "NA"
    ]
    graphics [
      x 1595.149569057127
      y 1303.3715592882031
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "d0ce3"
      uniprot "NA"
    ]
    graphics [
      x 731.4670077687117
      y 789.7880367959536
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_102"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id3cf0d202"
      uniprot "NA"
    ]
    graphics [
      x 447.5840271555659
      y 965.8073933225205
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000213928;urn:miriam:ensembl:ENSG00000115415;urn:miriam:ensembl:ENSG00000170581"
      hgnc "NA"
      map_id "W16_49"
      name "d46a8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d46a8"
      uniprot "NA"
    ]
    graphics [
      x 508.93666072719236
      y 261.00795472928735
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_28"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "bc446"
      uniprot "NA"
    ]
    graphics [
      x 386.30821193061536
      y 223.28931284731823
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:wikidata:Q27097846"
      hgnc "NA"
      map_id "W16_79"
      name "GRL0617"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "ebd2b"
      uniprot "NA"
    ]
    graphics [
      x 691.5067522762006
      y 1047.8864266508745
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      annotation "PUBMED:22088887;PUBMED:18852458"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_68"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "e29be"
      uniprot "NA"
    ]
    graphics [
      x 714.0910777221557
      y 1188.0220237785159
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:refseq:YP_009725299.1"
      hgnc "NA"
      map_id "W16_77"
      name "PLpro_space_(nsp3)"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e9fd0"
      uniprot "NA"
    ]
    graphics [
      x 816.5387032009962
      y 1276.6907649322122
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "PUBMED:32695122;PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_58"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "dd2da"
      uniprot "NA"
    ]
    graphics [
      x 825.3440121766309
      y 812.8803227599346
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000115415;urn:miriam:ensembl:ENSG00000170581"
      hgnc "NA"
      map_id "W16_64"
      name "dffb5"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "dffb5"
      uniprot "NA"
    ]
    graphics [
      x 750.2235967445956
      y 401.88379025948944
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_65"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "e0118"
      uniprot "NA"
    ]
    graphics [
      x 643.0734110641797
      y 297.8325722438921
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355;PUBMED:32695122"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_80"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "ed2f7"
      uniprot "NA"
    ]
    graphics [
      x 1670.0680946874877
      y 914.4529551341341
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000088888"
      hgnc "NA"
      map_id "W16_94"
      name "MAVS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "fa763"
      uniprot "NA"
    ]
    graphics [
      x 1522.2977003385624
      y 940.893281860288
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_90"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f8a9c"
      uniprot "NA"
    ]
    graphics [
      x 1021.3115359280754
      y 411.9851510135051
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_44"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "d00bf"
      uniprot "NA"
    ]
    graphics [
      x 1676.2613461074639
      y 673.464568002712
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_40"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "c8d59"
      uniprot "NA"
    ]
    graphics [
      x 1597.980881153198
      y 1643.7127136829326
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000196664"
      hgnc "NA"
      map_id "W16_71"
      name "TLR7"
      node_subtype "GENE"
      node_type "species"
      org_id "e4953"
      uniprot "NA"
    ]
    graphics [
      x 1494.8304625919204
      y 1769.55936963419
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_32"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "bebd6"
      uniprot "NA"
    ]
    graphics [
      x 1265.4331991269198
      y 620.3156646801687
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355;PUBMED:32695122"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_106"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id5e8cde6a"
      uniprot "NA"
    ]
    graphics [
      x 1831.8848235344392
      y 585.2759589859577
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_17"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "b5e59"
      uniprot "NA"
    ]
    graphics [
      x 1504.2212233733028
      y 1551.4592487616935
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_24"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "ba761"
      uniprot "NA"
    ]
    graphics [
      x 655.4532865442407
      y 1268.694669059365
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ncbiprotein:YP_009725297"
      hgnc "NA"
      map_id "W16_55"
      name "nsp1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "da173"
      uniprot "NA"
    ]
    graphics [
      x 1141.1631006417008
      y 1008.1233481414806
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_105"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id5db145b0"
      uniprot "NA"
    ]
    graphics [
      x 1040.0783094716166
      y 949.1723278410593
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      annotation "PUBMED:32695122"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_103"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id51069b65"
      uniprot "NA"
    ]
    graphics [
      x 1397.5734960290088
      y 1086.268894036662
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_57"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "dca81"
      uniprot "NA"
    ]
    graphics [
      x 1031.6570549136147
      y 1250.930500617219
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      annotation "PUBMED:33596266"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_115"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idd827ab35"
      uniprot "NA"
    ]
    graphics [
      x 1971.3157498120686
      y 696.5298894664043
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_20"
      name "Endosome"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "b8c2d"
      uniprot "NA"
    ]
    graphics [
      x 1966.8803802323778
      y 855.4821913166154
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:wikidata:Q165399;urn:miriam:pubmed:32205204"
      hgnc "NA"
      map_id "W16_8"
      name "azithromycin"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "ac609"
      uniprot "NA"
    ]
    graphics [
      x 441.0941306197865
      y 538.6725707927881
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      annotation "PUBMED:19893639;PUBMED:17667842"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_15"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "b22d1"
      uniprot "NA"
    ]
    graphics [
      x 384.20368234636396
      y 679.9514208570197
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      annotation "PUBMED:30406125"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_67"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "e1bdb"
      uniprot "NA"
    ]
    graphics [
      x 497.9112823319974
      y 401.73520266801535
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_23"
      name "NA"
      node_subtype "POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "b9579"
      uniprot "NA"
    ]
    graphics [
      x 1385.6683497817983
      y 306.2873286318416
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:uniprot:O60603"
      hgnc "NA"
      map_id "W16_69"
      name "TLR2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e33e6"
      uniprot "UNIPROT:O60603"
    ]
    graphics [
      x 1240.2685031299545
      y 354.55518211512566
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:uniprot:Q6UXN2"
      hgnc "NA"
      map_id "W16_50"
      name "TREML4_space_"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d4fdc"
      uniprot "UNIPROT:Q6UXN2"
    ]
    graphics [
      x 1301.4929286467368
      y 1692.1378108006202
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      annotation "PUBMED:25848864"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_100"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id2aa49a5d"
      uniprot "NA"
    ]
    graphics [
      x 1175.5441507671721
      y 1599.9263992010228
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_51"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "d52dc"
      uniprot "NA"
    ]
    graphics [
      x 863.3196102752729
      y 1402.301607192136
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:ensembl:ENSG00000263528"
      hgnc "NA"
      map_id "W16_42"
      name "IKBKE"
      node_subtype "GENE"
      node_type "species"
      org_id "cb0df"
      uniprot "NA"
    ]
    graphics [
      x 987.0862132394396
      y 1374.541921647322
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_111"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id97a8368b"
      uniprot "NA"
    ]
    graphics [
      x 1086.4724494422235
      y 415.7942564700499
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      annotation "PUBMED:25848864"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_116"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "idf6e0bc7f"
      uniprot "NA"
    ]
    graphics [
      x 1336.9941283289406
      y 1827.8209687849285
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_61"
      name "NA"
      node_subtype "POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "de4da"
      uniprot "NA"
    ]
    graphics [
      x 1933.13598576898
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:uniprot:Q9BYF1"
      hgnc "NA"
      map_id "W16_6"
      name "ACE2"
      node_subtype "GENE"
      node_type "species"
      org_id "aaf33"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1814.3521331839129
      y 129.02681327631842
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_89"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f88d9"
      uniprot "NA"
    ]
    graphics [
      x 1022.4442986516345
      y 1688.2587299784136
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_48"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "d3137"
      uniprot "NA"
    ]
    graphics [
      x 1816.3243096474844
      y 679.0061931593569
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_11"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "aef8e"
      uniprot "NA"
    ]
    graphics [
      x 448.8108219928772
      y 1158.1651015449163
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355;PUBMED:32695122"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_87"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "f3314"
      uniprot "NA"
    ]
    graphics [
      x 1801.2283396788534
      y 416.294281926497
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_25"
      name "NA"
      node_subtype "POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "bb605"
      uniprot "NA"
    ]
    graphics [
      x 2150.4657718572344
      y 194.74099174686978
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4868"
      full_annotation "urn:miriam:uniprot:O15393"
      hgnc "NA"
      map_id "W16_62"
      name "TMPRSS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "df9cf"
      uniprot "UNIPROT:O15393"
    ]
    graphics [
      x 2111.6074897462104
      y 313.0601133426633
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      annotation "PUBMED:32726355;PUBMED:32695122"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_29"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "bd252"
      uniprot "NA"
    ]
    graphics [
      x 1605.911956905703
      y 746.04558864058
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_60"
      name "NA"
      node_subtype "POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "de4c5"
      uniprot "NA"
    ]
    graphics [
      x 1315.4526710475773
      y 159.90241758172306
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      annotation "PUBMED:33506952"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_110"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id7c297d34"
      uniprot "NA"
    ]
    graphics [
      x 1101.6071399163418
      y 1377.1232498267177
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "WP4868"
      full_annotation "NA"
      hgnc "NA"
      map_id "W16_108"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id72e167d2"
      uniprot "NA"
    ]
    graphics [
      x 860.5974282611797
      y 1139.6755598907985
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W16_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 117
    source 1
    target 2
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_13"
      target_id "W16_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 2
    target 3
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_46"
      target_id "W16_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 1
    target 4
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_13"
      target_id "W16_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 4
    target 5
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_73"
      target_id "W16_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 6
    target 7
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_59"
      target_id "W16_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 7
    target 8
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_101"
      target_id "W16_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 9
    target 10
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_75"
      target_id "W16_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 10
    target 11
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_96"
      target_id "W16_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 12
    target 13
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_39"
      target_id "W16_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 126
    source 13
    target 5
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_10"
      target_id "W16_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 127
    source 14
    target 15
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_85"
      target_id "W16_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 128
    source 15
    target 16
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_33"
      target_id "W16_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 17
    target 18
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_88"
      target_id "W16_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 130
    source 18
    target 19
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_82"
      target_id "W16_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 131
    source 20
    target 21
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_3"
      target_id "W16_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 132
    source 21
    target 17
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_2"
      target_id "W16_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 133
    source 22
    target 23
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_70"
      target_id "W16_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 134
    source 23
    target 24
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_35"
      target_id "W16_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 135
    source 25
    target 26
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_31"
      target_id "W16_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 136
    source 26
    target 3
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_66"
      target_id "W16_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 137
    source 27
    target 28
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_16"
      target_id "W16_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 138
    source 28
    target 29
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_18"
      target_id "W16_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 139
    source 30
    target 31
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_5"
      target_id "W16_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 31
    target 3
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_41"
      target_id "W16_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 16
    target 32
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_37"
      target_id "W16_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 32
    target 33
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_12"
      target_id "W16_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 34
    target 35
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_21"
      target_id "W16_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 144
    source 35
    target 36
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_112"
      target_id "W16_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 145
    source 37
    target 38
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_84"
      target_id "W16_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 146
    source 38
    target 39
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_113"
      target_id "W16_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 147
    source 40
    target 41
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_74"
      target_id "W16_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 148
    source 41
    target 42
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_43"
      target_id "W16_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 149
    source 43
    target 44
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_19"
      target_id "W16_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 150
    source 44
    target 6
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_81"
      target_id "W16_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 151
    source 45
    target 46
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_72"
      target_id "W16_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 152
    source 46
    target 47
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_109"
      target_id "W16_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 153
    source 48
    target 49
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_38"
      target_id "W16_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 154
    source 49
    target 50
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_78"
      target_id "W16_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 155
    source 51
    target 52
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_7"
      target_id "W16_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 156
    source 52
    target 20
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_54"
      target_id "W16_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 157
    source 42
    target 53
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_56"
      target_id "W16_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 158
    source 53
    target 36
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_97"
      target_id "W16_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 159
    source 48
    target 54
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_38"
      target_id "W16_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 160
    source 54
    target 55
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_4"
      target_id "W16_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 161
    source 56
    target 57
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_9"
      target_id "W16_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 162
    source 57
    target 58
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_14"
      target_id "W16_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 163
    source 19
    target 59
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_27"
      target_id "W16_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 164
    source 59
    target 40
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_93"
      target_id "W16_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 165
    source 60
    target 61
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_1"
      target_id "W16_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 166
    source 61
    target 62
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_104"
      target_id "W16_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 167
    source 36
    target 63
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_83"
      target_id "W16_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 168
    source 63
    target 45
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_107"
      target_id "W16_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 169
    source 64
    target 65
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_47"
      target_id "W16_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 170
    source 65
    target 8
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_86"
      target_id "W16_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 171
    source 66
    target 67
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_26"
      target_id "W16_114"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 172
    source 67
    target 51
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_114"
      target_id "W16_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 173
    source 29
    target 68
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_63"
      target_id "W16_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 174
    source 68
    target 24
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_45"
      target_id "W16_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 175
    source 47
    target 69
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_30"
      target_id "W16_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 176
    source 69
    target 43
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_102"
      target_id "W16_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 177
    source 70
    target 71
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_49"
      target_id "W16_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 178
    source 71
    target 14
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_28"
      target_id "W16_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 179
    source 72
    target 73
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_79"
      target_id "W16_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 180
    source 73
    target 74
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_68"
      target_id "W16_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 181
    source 40
    target 75
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_74"
      target_id "W16_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 182
    source 75
    target 34
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_58"
      target_id "W16_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 76
    target 77
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_64"
      target_id "W16_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 77
    target 70
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_65"
      target_id "W16_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 5
    target 78
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_52"
      target_id "W16_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 78
    target 79
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_80"
      target_id "W16_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 55
    target 80
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_36"
      target_id "W16_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 80
    target 29
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_90"
      target_id "W16_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 30
    target 81
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_5"
      target_id "W16_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 81
    target 5
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_44"
      target_id "W16_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 66
    target 82
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_26"
      target_id "W16_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 82
    target 83
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_40"
      target_id "W16_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 55
    target 84
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_36"
      target_id "W16_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 84
    target 20
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_32"
      target_id "W16_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 62
    target 85
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_99"
      target_id "W16_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 85
    target 5
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_106"
      target_id "W16_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 66
    target 86
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_26"
      target_id "W16_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 86
    target 56
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_17"
      target_id "W16_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 74
    target 87
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_77"
      target_id "W16_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 87
    target 24
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_24"
      target_id "W16_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 88
    target 89
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_55"
      target_id "W16_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 89
    target 45
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_105"
      target_id "W16_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 79
    target 90
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_94"
      target_id "W16_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 90
    target 17
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_103"
      target_id "W16_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 74
    target 91
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_77"
      target_id "W16_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 91
    target 17
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_57"
      target_id "W16_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 50
    target 92
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_76"
      target_id "W16_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 92
    target 93
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_115"
      target_id "W16_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 94
    target 95
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_8"
      target_id "W16_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 95
    target 11
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_15"
      target_id "W16_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 94
    target 96
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_8"
      target_id "W16_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 96
    target 70
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_67"
      target_id "W16_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 48
    target 97
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_38"
      target_id "W16_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 97
    target 98
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_23"
      target_id "W16_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 99
    target 100
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_50"
      target_id "W16_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 100
    target 8
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_100"
      target_id "W16_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 74
    target 101
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_77"
      target_id "W16_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 101
    target 102
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_51"
      target_id "W16_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 98
    target 103
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_69"
      target_id "W16_111"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 103
    target 29
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_111"
      target_id "W16_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 83
    target 104
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_71"
      target_id "W16_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 99
    target 104
    cd19dm [
      diagram "WP4868"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W16_50"
      target_id "W16_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 104
    target 58
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_116"
      target_id "W16_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 60
    target 105
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_1"
      target_id "W16_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 105
    target 106
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_61"
      target_id "W16_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 58
    target 107
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_91"
      target_id "W16_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 107
    target 22
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_89"
      target_id "W16_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 12
    target 108
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_39"
      target_id "W16_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 108
    target 3
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_48"
      target_id "W16_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 24
    target 109
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_98"
      target_id "W16_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 109
    target 9
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_11"
      target_id "W16_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 62
    target 110
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_99"
      target_id "W16_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 110
    target 3
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_87"
      target_id "W16_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 60
    target 111
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_1"
      target_id "W16_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 111
    target 112
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_25"
      target_id "W16_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 3
    target 113
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_22"
      target_id "W16_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 113
    target 79
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_29"
      target_id "W16_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 48
    target 114
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_38"
      target_id "W16_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 239
    source 114
    target 27
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_60"
      target_id "W16_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 240
    source 22
    target 115
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_70"
      target_id "W16_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 241
    source 115
    target 17
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_110"
      target_id "W16_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 242
    source 74
    target 116
    cd19dm [
      diagram "WP4868"
      edge_type "CONSPUMPTION"
      source_id "W16_77"
      target_id "W16_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 243
    source 116
    target 40
    cd19dm [
      diagram "WP4868"
      edge_type "PRODUCTION"
      source_id "W16_108"
      target_id "W16_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
