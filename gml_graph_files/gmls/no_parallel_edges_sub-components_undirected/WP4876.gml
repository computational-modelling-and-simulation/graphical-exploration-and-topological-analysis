# generated with VANTED V2.8.2 at Fri Mar 04 09:53:02 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4876"
      full_annotation "urn:miriam:ensembl:ENSG00000109320"
      hgnc "NA"
      map_id "W18_3"
      name "NFKB1_space_p105"
      node_subtype "GENE"
      node_type "species"
      org_id "ae8f7"
      uniprot "NA"
    ]
    graphics [
      x 166.19707517034414
      y 403.5811183820389
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W18_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4876"
      full_annotation "NA"
      hgnc "NA"
      map_id "W18_12"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id56c2671f"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 460.4659404930549
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W18_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4876"
      full_annotation "urn:miriam:refseq:YP_009724391;urn:miriam:ensembl:ENSG00000131323"
      hgnc "NA"
      map_id "W18_5"
      name "b6589"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b6589"
      uniprot "NA"
    ]
    graphics [
      x 143.61629099183975
      y 563.9029954005732
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W18_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4876"
      full_annotation "urn:miriam:ensembl:ENSG00000109320"
      hgnc "NA"
      map_id "W18_10"
      name "NFKB1"
      node_subtype "GENE"
      node_type "species"
      org_id "dc151"
      uniprot "NA"
    ]
    graphics [
      x 80.095659784477
      y 339.1880633451974
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W18_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4876"
      full_annotation "urn:miriam:ensembl:ENSG00000125538"
      hgnc "NA"
      map_id "W18_7"
      name "pro_minus_IL1B"
      node_subtype "RNA"
      node_type "species"
      org_id "d3559"
      uniprot "NA"
    ]
    graphics [
      x 716.6909220238352
      y 231.90156850818042
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W18_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4876"
      full_annotation "NA"
      hgnc "NA"
      map_id "W18_14"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idaa5a11ed"
      uniprot "NA"
    ]
    graphics [
      x 705.118442393917
      y 370.12984767429214
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W18_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4876"
      full_annotation "urn:miriam:ensembl:ENSG00000125538"
      hgnc "NA"
      map_id "W18_1"
      name "pro_minus_IL1B"
      node_subtype "GENE"
      node_type "species"
      org_id "a1e57"
      uniprot "NA"
    ]
    graphics [
      x 632.7819760088122
      y 486.8182092433764
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W18_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4876"
      full_annotation "urn:miriam:ensembl:ENSG00000137752"
      hgnc "NA"
      map_id "W18_2"
      name "pro_minus_CASP1"
      node_subtype "GENE"
      node_type "species"
      org_id "a2318"
      uniprot "NA"
    ]
    graphics [
      x 170.4401993412666
      y 710.8876645644623
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W18_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4876"
      full_annotation "NA"
      hgnc "NA"
      map_id "W18_13"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id7ef1c6cf"
      uniprot "NA"
    ]
    graphics [
      x 259.5453503797037
      y 634.6920353428366
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W18_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4876"
      full_annotation "urn:miriam:ensembl:ENSG00000137752"
      hgnc "NA"
      map_id "W18_4"
      name "CASP1"
      node_subtype "GENE"
      node_type "species"
      org_id "b43c2"
      uniprot "NA"
    ]
    graphics [
      x 395.6869171725822
      y 625.1626467696977
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W18_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4876"
      full_annotation "urn:miriam:ensembl:ENSG00000173039;urn:miriam:ensembl:ENSG00000109320"
      hgnc "NA"
      map_id "W18_8"
      name "d72a0"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d72a0"
      uniprot "NA"
    ]
    graphics [
      x 496.45424347466064
      y 292.2913567473198
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W18_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4876"
      full_annotation "NA"
      hgnc "NA"
      map_id "W18_16"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ide9837dad"
      uniprot "NA"
    ]
    graphics [
      x 599.0197570323377
      y 242.193767270179
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W18_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4876"
      full_annotation "NA"
      hgnc "NA"
      map_id "W18_11"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ed3f9"
      uniprot "NA"
    ]
    graphics [
      x 525.9377733942894
      y 576.4798888311934
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W18_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4876"
      full_annotation "urn:miriam:ensembl:ENSG00000125538"
      hgnc "NA"
      map_id "W18_6"
      name "IL1B"
      node_subtype "GENE"
      node_type "species"
      org_id "b860a"
      uniprot "NA"
    ]
    graphics [
      x 582.9523443329147
      y 675.9193294452139
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W18_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4876"
      full_annotation "urn:miriam:wikidata:Q422438"
      hgnc "NA"
      map_id "W18_9"
      name "Chloroquine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "d8fdf"
      uniprot "NA"
    ]
    graphics [
      x 538.8381947036239
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W18_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "PUBMED:16418198"
      count 1
      diagram "WP4876"
      full_annotation "NA"
      hgnc "NA"
      map_id "W18_15"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idb1ca554"
      uniprot "NA"
    ]
    graphics [
      x 650.7033981992264
      y 113.78595356752311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W18_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 17
    source 1
    target 2
    cd19dm [
      diagram "WP4876"
      edge_type "CONSPUMPTION"
      source_id "W18_3"
      target_id "W18_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 18
    source 3
    target 2
    cd19dm [
      diagram "WP4876"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W18_5"
      target_id "W18_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 19
    source 2
    target 4
    cd19dm [
      diagram "WP4876"
      edge_type "PRODUCTION"
      source_id "W18_12"
      target_id "W18_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 20
    source 5
    target 6
    cd19dm [
      diagram "WP4876"
      edge_type "CONSPUMPTION"
      source_id "W18_7"
      target_id "W18_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 21
    source 6
    target 7
    cd19dm [
      diagram "WP4876"
      edge_type "PRODUCTION"
      source_id "W18_14"
      target_id "W18_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 22
    source 8
    target 9
    cd19dm [
      diagram "WP4876"
      edge_type "CONSPUMPTION"
      source_id "W18_2"
      target_id "W18_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 23
    source 3
    target 9
    cd19dm [
      diagram "WP4876"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W18_5"
      target_id "W18_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 24
    source 9
    target 10
    cd19dm [
      diagram "WP4876"
      edge_type "PRODUCTION"
      source_id "W18_13"
      target_id "W18_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 25
    source 11
    target 12
    cd19dm [
      diagram "WP4876"
      edge_type "CONSPUMPTION"
      source_id "W18_8"
      target_id "W18_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 26
    source 12
    target 5
    cd19dm [
      diagram "WP4876"
      edge_type "PRODUCTION"
      source_id "W18_16"
      target_id "W18_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 27
    source 7
    target 13
    cd19dm [
      diagram "WP4876"
      edge_type "CONSPUMPTION"
      source_id "W18_1"
      target_id "W18_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 28
    source 10
    target 13
    cd19dm [
      diagram "WP4876"
      edge_type "CATALYSIS"
      source_id "W18_4"
      target_id "W18_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 29
    source 13
    target 14
    cd19dm [
      diagram "WP4876"
      edge_type "PRODUCTION"
      source_id "W18_11"
      target_id "W18_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 30
    source 15
    target 16
    cd19dm [
      diagram "WP4876"
      edge_type "CONSPUMPTION"
      source_id "W18_9"
      target_id "W18_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 31
    source 16
    target 5
    cd19dm [
      diagram "WP4876"
      edge_type "PRODUCTION"
      source_id "W18_15"
      target_id "W18_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
