# generated with VANTED V2.8.2 at Fri Mar 04 09:53:04 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:pubmed:25847972;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q9ULZ3;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033;urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746;urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3"
      hgnc "HGNC_SYMBOL:TRAF3;HGNC_SYMBOL:MAVS;HGNC_SYMBOL:PYCARD"
      map_id "M117_8"
      name "ASC:MAVS:TRAF3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa27"
      uniprot "UNIPROT:Q13114;UNIPROT:Q9ULZ3;UNIPROT:Q7Z434"
    ]
    graphics [
      x 1493.200117088822
      y 534.0288092014114
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:25847972"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_40"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re51"
      uniprot "NA"
    ]
    graphics [
      x 1458.4698442479228
      y 682.2120649918417
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3"
      hgnc "HGNC_SYMBOL:PYCARD"
      map_id "M117_58"
      name "ASC"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa103"
      uniprot "UNIPROT:Q9ULZ3"
    ]
    graphics [
      x 1196.6358969975993
      y 715.6726861726163
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033"
      hgnc "HGNC_SYMBOL:TRAF3"
      map_id "M117_113"
      name "TRAF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa99"
      uniprot "UNIPROT:Q13114"
    ]
    graphics [
      x 1640.8644129039296
      y 699.6205834713437
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746"
      hgnc "HGNC_SYMBOL:MAVS"
      map_id "M117_57"
      name "MAVS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa102"
      uniprot "UNIPROT:Q7Z434"
    ]
    graphics [
      x 1645.926917495064
      y 789.1077777079331
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:3606"
      hgnc "NA"
      map_id "M117_109"
      name "proIL_minus_18"
      node_subtype "RNA"
      node_type "species"
      org_id "sa93"
      uniprot "NA"
    ]
    graphics [
      x 175.85447905851288
      y 1108.6758197710546
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_44"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re56"
      uniprot "NA"
    ]
    graphics [
      x 125.69361480781527
      y 953.5838034702897
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:3606;urn:miriam:uniprot:Q14116;urn:miriam:hgnc.symbol:IL18"
      hgnc "HGNC_SYMBOL:IL18"
      map_id "M117_62"
      name "proIL_minus_18"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa14"
      uniprot "UNIPROT:Q14116"
    ]
    graphics [
      x 175.44752474265658
      y 801.8299193486918
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:NLRP3"
      map_id "M117_108"
      name "NLRP3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa92"
      uniprot "UNIPROT:Q96P20"
    ]
    graphics [
      x 1230.9187021021817
      y 594.0981603527941
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_46"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re6"
      uniprot "NA"
    ]
    graphics [
      x 1261.7196127643824
      y 772.2156343139264
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:hgnc:16952;urn:miriam:ncbigene:10628;urn:miriam:ensembl:ENSG00000265972;urn:miriam:ncbigene:10628;urn:miriam:uniprot:Q9H3M7;urn:miriam:refseq:NM_006472;urn:miriam:hgnc.symbol:TXNIP;urn:miriam:hgnc.symbol:TXNIP"
      hgnc "HGNC_SYMBOL:TXNIP"
      map_id "M117_101"
      name "TXNIP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa8"
      uniprot "UNIPROT:Q9H3M7"
    ]
    graphics [
      x 1395.2533032544336
      y 1015.2839767297095
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:Q9H3M7;urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548;urn:miriam:hgnc:16952;urn:miriam:ncbigene:10628;urn:miriam:ensembl:ENSG00000265972;urn:miriam:ncbigene:10628;urn:miriam:uniprot:Q9H3M7;urn:miriam:refseq:NM_006472;urn:miriam:hgnc.symbol:TXNIP;urn:miriam:hgnc.symbol:TXNIP"
      hgnc "HGNC_SYMBOL:NLRP3;HGNC_SYMBOL:TXNIP"
      map_id "M117_11"
      name "TXNIP:NLRP3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa3"
      uniprot "UNIPROT:Q96P20;UNIPROT:Q9H3M7"
    ]
    graphics [
      x 1099.469515283075
      y 591.730522350653
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:Q9ULZ3;urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746;urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3"
      hgnc "HGNC_SYMBOL:MAVS;HGNC_SYMBOL:PYCARD"
      map_id "M117_9"
      name "ASC:MAVS"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa28"
      uniprot "UNIPROT:Q9ULZ3;UNIPROT:Q7Z434"
    ]
    graphics [
      x 1880.7517511470028
      y 657.0871066905063
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "PUBMED:25847972"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_39"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re50"
      uniprot "NA"
    ]
    graphics [
      x 1763.1371323574544
      y 610.2203301396901
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:pubmed:25847972;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q9ULZ3;urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033;urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746"
      hgnc "HGNC_SYMBOL:PYCARD;HGNC_SYMBOL:TRAF3;HGNC_SYMBOL:MAVS"
      map_id "M117_10"
      name "ASC:MAVS:TRAF3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa29"
      uniprot "UNIPROT:Q13114;UNIPROT:Q9ULZ3;UNIPROT:Q7Z434"
    ]
    graphics [
      x 1699.6825498284832
      y 504.11315615491463
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_53"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re67"
      uniprot "NA"
    ]
    graphics [
      x 1671.128688076099
      y 875.0027033123631
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:1489679;urn:miriam:uniprot:P59636;urn:miriam:taxonomy:694009"
      hgnc "NA"
      map_id "M117_84"
      name "Orf9b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa165"
      uniprot "UNIPROT:P59636"
    ]
    graphics [
      x 1576.3069471874771
      y 983.8105993855152
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_83"
      name "s878"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa164"
      uniprot "NA"
    ]
    graphics [
      x 1771.0912285897944
      y 980.9811238280457
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:ncbigene:1489669;urn:miriam:taxonomy:694009"
      hgnc "NA"
      map_id "M117_68"
      name "Orf3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa147"
      uniprot "UNIPROT:P59632"
    ]
    graphics [
      x 62.5
      y 1292.649329575514
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_48"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re61"
      uniprot "NA"
    ]
    graphics [
      x 69.48927781419309
      y 1411.1922311826816
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033"
      hgnc "HGNC_SYMBOL:TRAF3"
      map_id "M117_69"
      name "TRAF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa148"
      uniprot "UNIPROT:Q13114"
    ]
    graphics [
      x 174.06788394207376
      y 1332.6992825579302
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:uniprot:Q13114;urn:miriam:taxonomy:694009;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033;urn:miriam:uniprot:P59632;urn:miriam:ncbigene:1489669;urn:miriam:taxonomy:694009"
      hgnc "HGNC_SYMBOL:TRAF3"
      map_id "M117_12"
      name "TRAF3:SARS_space_Orf3a"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa30"
      uniprot "UNIPROT:P59632;UNIPROT:Q13114"
    ]
    graphics [
      x 78.40757641660366
      y 1542.722796389352
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P10599;urn:miriam:uniprot:Q9H3M7;urn:miriam:ncbigene:7295;urn:miriam:ncbigene:7295;urn:miriam:uniprot:P10599;urn:miriam:hgnc:12435;urn:miriam:ensembl:ENSG00000136810;urn:miriam:refseq:NM_001244938;urn:miriam:hgnc.symbol:TXN;urn:miriam:hgnc.symbol:TXN;urn:miriam:hgnc:16952;urn:miriam:ncbigene:10628;urn:miriam:ensembl:ENSG00000265972;urn:miriam:ncbigene:10628;urn:miriam:uniprot:Q9H3M7;urn:miriam:refseq:NM_006472;urn:miriam:hgnc.symbol:TXNIP;urn:miriam:hgnc.symbol:TXNIP"
      hgnc "HGNC_SYMBOL:TXN;HGNC_SYMBOL:TXNIP"
      map_id "M117_1"
      name "Thioredoxin:TXNIP"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa1"
      uniprot "UNIPROT:P10599;UNIPROT:Q9H3M7"
    ]
    graphics [
      x 1608.9340460181434
      y 1196.5238728643253
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_17"
      name "NA"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "re1"
      uniprot "NA"
    ]
    graphics [
      x 1502.0844250908872
      y 1137.9234875630298
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:7295;urn:miriam:ncbigene:7295;urn:miriam:uniprot:P10599;urn:miriam:hgnc:12435;urn:miriam:ensembl:ENSG00000136810;urn:miriam:refseq:NM_001244938;urn:miriam:hgnc.symbol:TXN;urn:miriam:hgnc.symbol:TXN"
      hgnc "HGNC_SYMBOL:TXN"
      map_id "M117_91"
      name "TXN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa175"
      uniprot "UNIPROT:P10599"
    ]
    graphics [
      x 1519.5768056071893
      y 1313.1511895072995
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_24"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re25"
      uniprot "NA"
    ]
    graphics [
      x 311.20506373123146
      y 691.423508432907
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P29466;urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292;urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M117_14"
      name "Caspase_minus_1_space_Tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa5"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 520.2227172472897
      y 801.7818309928284
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:3606;urn:miriam:uniprot:Q14116;urn:miriam:hgnc.symbol:IL18"
      hgnc "HGNC_SYMBOL:IL18"
      map_id "M117_79"
      name "IL_minus_18"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa16"
      uniprot "UNIPROT:Q14116"
    ]
    graphics [
      x 359.0389692563485
      y 530.1271689779607
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:3606;urn:miriam:uniprot:Q14116;urn:miriam:hgnc.symbol:IL18"
      hgnc "HGNC_SYMBOL:IL18"
      map_id "M117_92"
      name "proIL_minus_18"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa18"
      uniprot "UNIPROT:Q14116"
    ]
    graphics [
      x 213.1033231460642
      y 619.5257300608234
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_81"
      name "s876"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa162"
      uniprot "NA"
    ]
    graphics [
      x 706.7248574715684
      y 827.2773710875157
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "PUBMED:25770182;PUBMED:28356568"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_51"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re65"
      uniprot "NA"
    ]
    graphics [
      x 860.7990545189548
      y 857.1698719513921
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17245"
      hgnc "NA"
      map_id "M117_78"
      name "CO"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa158"
      uniprot "NA"
    ]
    graphics [
      x 983.2121467132818
      y 1071.5871697921853
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A26523"
      hgnc "NA"
      map_id "M117_54"
      name "Reactive_space_Oxygen_space_Species"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa1"
      uniprot "NA"
    ]
    graphics [
      x 878.8240362776038
      y 636.3970622129939
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_27"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re3"
      uniprot "NA"
    ]
    graphics [
      x 1486.4854162357692
      y 1206.0347070395749
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:7295;urn:miriam:ncbigene:7295;urn:miriam:uniprot:P10599;urn:miriam:hgnc:12435;urn:miriam:ensembl:ENSG00000136810;urn:miriam:refseq:NM_001244938;urn:miriam:hgnc.symbol:TXN;urn:miriam:hgnc.symbol:TXN"
      hgnc "HGNC_SYMBOL:TXN"
      map_id "M117_100"
      name "TXN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa7"
      uniprot "UNIPROT:P10599"
    ]
    graphics [
      x 1599.124152243671
      y 1328.2927239970654
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:4790;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:uniprot:P19838"
      hgnc "HGNC_SYMBOL:NFKB1"
      map_id "M117_71"
      name "p105"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa151"
      uniprot "UNIPROT:P19838"
    ]
    graphics [
      x 174.65774389205512
      y 1639.4377096524904
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_49"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re62"
      uniprot "NA"
    ]
    graphics [
      x 70.350266962403
      y 1689.004308378219
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:4790;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:uniprot:P19838"
      hgnc "HGNC_SYMBOL:NFKB1"
      map_id "M117_104"
      name "p105"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa84"
      uniprot "UNIPROT:P19838"
    ]
    graphics [
      x 151.45649325669774
      y 1805.1365749104784
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:ncbigene:3553"
      hgnc "HGNC_SYMBOL:IL1B"
      map_id "M117_70"
      name "proIL_minus_1B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa15"
      uniprot "UNIPROT:P01584"
    ]
    graphics [
      x 758.0917385035127
      y 977.5507951056151
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_22"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re22"
      uniprot "NA"
    ]
    graphics [
      x 633.7321099553606
      y 1022.9633106468889
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:ncbigene:3553"
      hgnc "HGNC_SYMBOL:IL1B"
      map_id "M117_87"
      name "IL_minus_1B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa17"
      uniprot "UNIPROT:P01584"
    ]
    graphics [
      x 668.0520991631886
      y 1221.7780177822412
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:ncbigene:3553"
      hgnc "HGNC_SYMBOL:IL1B"
      map_id "M117_90"
      name "proIL_minus_1B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa172"
      uniprot "UNIPROT:P01584"
    ]
    graphics [
      x 758.8289854090663
      y 1053.104339874572
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29108"
      hgnc "NA"
      map_id "M117_64"
      name "Ca2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa141"
      uniprot "NA"
    ]
    graphics [
      x 576.7438161612354
      y 74.51402490282715
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "PUBMED:26331680"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re59"
      uniprot "NA"
    ]
    graphics [
      x 724.3626090851534
      y 65.759878485988
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P59637;urn:miriam:ncbigene:1489671;urn:miriam:hgnc.symbol:E"
      hgnc "HGNC_SYMBOL:E"
      map_id "M117_63"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa140"
      uniprot "UNIPROT:P59637"
    ]
    graphics [
      x 629.207358447604
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29108"
      hgnc "NA"
      map_id "M117_65"
      name "Ca2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa142"
      uniprot "NA"
    ]
    graphics [
      x 793.2130922285482
      y 213.72908715090182
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_33"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re44"
      uniprot "NA"
    ]
    graphics [
      x 265.51305105745814
      y 1880.2150712474481
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:4790;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:uniprot:P19838"
      hgnc "HGNC_SYMBOL:NFKB1"
      map_id "M117_103"
      name "p50"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa83"
      uniprot "UNIPROT:P19838"
    ]
    graphics [
      x 401.75806882553223
      y 1858.6127111567055
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30413"
      hgnc "NA"
      map_id "M117_72"
      name "Heme"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa152"
      uniprot "NA"
    ]
    graphics [
      x 1075.760933730675
      y 1431.3633921557605
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_50"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re63"
      uniprot "NA"
    ]
    graphics [
      x 1071.6731590936324
      y 1296.449098205725
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15379"
      hgnc "NA"
      map_id "M117_74"
      name "O2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa154"
      uniprot "NA"
    ]
    graphics [
      x 1186.9493778540996
      y 1269.2822391352345
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16474"
      hgnc "NA"
      map_id "M117_73"
      name "NADPH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa153"
      uniprot "NA"
    ]
    graphics [
      x 961.2734311997262
      y 1334.2184999379426
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ensembl:ENSG00000100292;urn:miriam:hgnc:5013;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:refseq:NM_002133;urn:miriam:uniprot:P09601;urn:miriam:ncbigene:3162;urn:miriam:ncbigene:3162;urn:miriam:ec-code:1.14.14.18"
      hgnc "HGNC_SYMBOL:HMOX1"
      map_id "M117_80"
      name "HMOX1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa161"
      uniprot "UNIPROT:P09601"
    ]
    graphics [
      x 1002.0090362177931
      y 1216.5354494183332
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17033"
      hgnc "NA"
      map_id "M117_76"
      name "Biliverdin"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa156"
      uniprot "NA"
    ]
    graphics [
      x 1144.2698544564303
      y 1409.3062828802745
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "M117_77"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa157"
      uniprot "NA"
    ]
    graphics [
      x 1005.6951982890073
      y 1407.066248812218
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18009"
      hgnc "NA"
      map_id "M117_75"
      name "NADP_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa155"
      uniprot "NA"
    ]
    graphics [
      x 1129.1746368668967
      y 1195.7796257665918
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29033"
      hgnc "NA"
      map_id "M117_82"
      name "Fe2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa163"
      uniprot "NA"
    ]
    graphics [
      x 1189.340133364093
      y 1350.6538642339906
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M117_55"
      name "CASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa100"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 486.07511138982886
      y 1103.5988523157125
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      annotation "PUBMED:25847972"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_26"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re27"
      uniprot "NA"
    ]
    graphics [
      x 534.1383815914653
      y 992.8445768412338
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:Q9ULZ3;urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3"
      hgnc "HGNC_SYMBOL:NLRP3;HGNC_SYMBOL:PYCARD"
      map_id "M117_3"
      name "NLRP3_space_oligomer:ASC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa13"
      uniprot "UNIPROT:Q96P20;UNIPROT:Q9ULZ3"
    ]
    graphics [
      x 651.7167504399001
      y 865.8466362494235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:P29466;urn:miriam:uniprot:Q9ULZ3;urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292;urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3;urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:CASP1;HGNC_SYMBOL:PYCARD;HGNC_SYMBOL:NLRP3"
      map_id "M117_2"
      name "NLRP3_space_oligomer:ASC:proCaspase1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa10"
      uniprot "UNIPROT:Q96P20;UNIPROT:P29466;UNIPROT:Q9ULZ3"
    ]
    graphics [
      x 405.1614460657305
      y 948.4908222522566
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3"
      hgnc "HGNC_SYMBOL:PYCARD"
      map_id "M117_59"
      name "ASC"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa104"
      uniprot "UNIPROT:Q9ULZ3"
    ]
    graphics [
      x 1843.0028549468614
      y 872.6097510192327
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "PUBMED:25847972"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_38"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re49"
      uniprot "NA"
    ]
    graphics [
      x 1811.1088601696242
      y 761.3145104636445
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re19"
      uniprot "NA"
    ]
    graphics [
      x 424.6495955250979
      y 784.527025127643
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P08311;urn:miriam:ncbigene:1511;urn:miriam:ncbigene:1511;urn:miriam:ec-code:3.4.21.20;urn:miriam:hgnc:2532;urn:miriam:hgnc.symbol:CTSG;urn:miriam:hgnc.symbol:CTSG;urn:miriam:refseq:NM_001911;urn:miriam:ensembl:ENSG00000100448"
      hgnc "HGNC_SYMBOL:CTSG"
      map_id "M117_96"
      name "CTSG"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa26"
      uniprot "UNIPROT:P08311"
    ]
    graphics [
      x 354.68965547625453
      y 883.2740308148315
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M117_85"
      name "CASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa168"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 545.7068435262354
      y 713.2779597709309
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M117_88"
      name "CASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa170"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 493.7531443473399
      y 862.0319957604837
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M117_89"
      name "CASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa171"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 477.96756069066475
      y 921.104495225799
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M117_86"
      name "CASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa169"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 491.63033179242467
      y 649.5859456488001
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:ncbigene:1489669;urn:miriam:taxonomy:694009"
      hgnc "NA"
      map_id "M117_97"
      name "Orf3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa3"
      uniprot "UNIPROT:P59632"
    ]
    graphics [
      x 1396.308479902367
      y 824.2989237278523
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_32"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re43"
      uniprot "NA"
    ]
    graphics [
      x 1522.6752997665599
      y 741.315299939927
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:uniprot:Q13114;urn:miriam:taxonomy:694009;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033;urn:miriam:uniprot:P59632;urn:miriam:ncbigene:1489669;urn:miriam:taxonomy:694009"
      hgnc "HGNC_SYMBOL:TRAF3"
      map_id "M117_7"
      name "TRAF3:SARS_space_Orf3a"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa24"
      uniprot "UNIPROT:P59632;UNIPROT:Q13114"
    ]
    graphics [
      x 1561.6707508625916
      y 598.4608092617295
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_25"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re26"
      uniprot "NA"
    ]
    graphics [
      x 479.2677319905204
      y 430.2605399490416
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:3606;urn:miriam:uniprot:Q14116;urn:miriam:hgnc.symbol:IL18"
      hgnc "HGNC_SYMBOL:IL18"
      map_id "M117_94"
      name "IL_minus_18"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa20"
      uniprot "UNIPROT:Q14116"
    ]
    graphics [
      x 601.458915591676
      y 371.03756522614685
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re42"
      uniprot "NA"
    ]
    graphics [
      x 414.48513849015006
      y 1752.391676064185
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:hgnc.symbol:NFKB2;urn:miriam:ncbigene:4791;urn:miriam:uniprot:Q00653"
      hgnc "HGNC_SYMBOL:NFKB2"
      map_id "M117_105"
      name "p65"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa86"
      uniprot "UNIPROT:Q00653"
    ]
    graphics [
      x 442.5611687628533
      y 1629.3260698527777
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:Q00653;urn:miriam:uniprot:P19838;urn:miriam:hgnc.symbol:NFKB2;urn:miriam:ncbigene:4791;urn:miriam:uniprot:Q00653;urn:miriam:ncbigene:4790;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:uniprot:P19838"
      hgnc "HGNC_SYMBOL:NFKB2;HGNC_SYMBOL:NFKB1"
      map_id "M117_5"
      name "Nf_minus_KB_space_Complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa22"
      uniprot "UNIPROT:Q00653;UNIPROT:P19838"
    ]
    graphics [
      x 529.6144709277947
      y 1713.1714457834541
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_42"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re54"
      uniprot "NA"
    ]
    graphics [
      x 1148.5707379431456
      y 439.34632700203053
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P08238;urn:miriam:uniprot:Q9Y2Z0;urn:miriam:ncbigene:3326;urn:miriam:ncbigene:3326;urn:miriam:uniprot:P08238;urn:miriam:hgnc:5258;urn:miriam:refseq:NM_007355;urn:miriam:ensembl:ENSG00000096384;urn:miriam:hgnc.symbol:HSP90AB1;urn:miriam:hgnc.symbol:HSP90AB1;urn:miriam:hgnc:16987;urn:miriam:ncbigene:10910;urn:miriam:ncbigene:10910;urn:miriam:uniprot:Q9Y2Z0;urn:miriam:ensembl:ENSG00000165416;urn:miriam:refseq:NM_001130912;urn:miriam:hgnc.symbol:SUGT1;urn:miriam:hgnc.symbol:SUGT1"
      hgnc "HGNC_SYMBOL:HSP90AB1;HGNC_SYMBOL:SUGT1"
      map_id "M117_13"
      name "SUGT1:HSP90AB1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa4"
      uniprot "UNIPROT:P08238;UNIPROT:Q9Y2Z0"
    ]
    graphics [
      x 1107.0614000670598
      y 315.9473650884945
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:P08238;urn:miriam:uniprot:Q9Y2Z0;urn:miriam:hgnc:16987;urn:miriam:ncbigene:10910;urn:miriam:ncbigene:10910;urn:miriam:uniprot:Q9Y2Z0;urn:miriam:ensembl:ENSG00000165416;urn:miriam:refseq:NM_001130912;urn:miriam:hgnc.symbol:SUGT1;urn:miriam:hgnc.symbol:SUGT1;urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:SUGT1;HGNC_SYMBOL:NLRP3"
      map_id "M117_15"
      name "NLRP3:SUGT1:HSP90"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa6"
      uniprot "UNIPROT:Q96P20;UNIPROT:P08238;UNIPROT:Q9Y2Z0"
    ]
    graphics [
      x 1028.9393964360256
      y 341.37539631365235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P29466;urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292;urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M117_16"
      name "CASP1(120_minus_197):CASP1(317_minus_404)"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa7"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 773.4507252564983
      y 717.8603192595483
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_21"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re21"
      uniprot "NA"
    ]
    graphics [
      x 657.4896398002379
      y 744.5155120128877
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_19"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re2"
      uniprot "NA"
    ]
    graphics [
      x 1585.6948069799532
      y 1450.0359980184844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A26523"
      hgnc "NA"
      map_id "M117_93"
      name "Reactive_space_Oxygen_space_Species"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa2"
      uniprot "NA"
    ]
    graphics [
      x 1534.6262083455204
      y 1565.9719954737716
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re47"
      uniprot "NA"
    ]
    graphics [
      x 526.3288186768657
      y 1545.434216397021
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P19838;urn:miriam:hgnc.symbol:NFKB2;urn:miriam:ncbigene:4791;urn:miriam:uniprot:Q00653;urn:miriam:ncbigene:4790;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:uniprot:P19838"
      hgnc "HGNC_SYMBOL:NFKB2;HGNC_SYMBOL:NFKB1"
      map_id "M117_4"
      name "NF_minus_KB_space_COMPLEX:IKBA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa21"
      uniprot "UNIPROT:P19838;UNIPROT:Q00653"
    ]
    graphics [
      x 618.4967925605667
      y 1662.3217064818787
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:Q00653;urn:miriam:uniprot:P19838;urn:miriam:hgnc.symbol:NFKB2;urn:miriam:ncbigene:4791;urn:miriam:uniprot:Q00653;urn:miriam:ncbigene:4790;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:uniprot:P19838"
      hgnc "HGNC_SYMBOL:NFKB2;HGNC_SYMBOL:NFKB1"
      map_id "M117_6"
      name "Nf_minus_KB_space_Complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa23"
      uniprot "UNIPROT:Q00653;UNIPROT:P19838"
    ]
    graphics [
      x 470.2285367552709
      y 1345.9645841967435
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:3606"
      hgnc "NA"
      map_id "M117_111"
      name "proIL_minus_18"
      node_subtype "GENE"
      node_type "species"
      org_id "sa95"
      uniprot "NA"
    ]
    graphics [
      x 299.53745883986244
      y 1112.9035907061868
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_29"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re38"
      uniprot "NA"
    ]
    graphics [
      x 304.8982482241405
      y 1237.374283154141
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      annotation "PUBMED:25847972;PUBMED:31034780"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_41"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re52"
      uniprot "NA"
    ]
    graphics [
      x 1595.5995161967198
      y 458.27713349053954
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_20"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re20"
      uniprot "NA"
    ]
    graphics [
      x 641.0357817558731
      y 637.2016414968632
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_52"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re66"
      uniprot "NA"
    ]
    graphics [
      x 1669.1112721350878
      y 955.510690215081
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:3553"
      hgnc "NA"
      map_id "M117_110"
      name "proIL_minus_1B"
      node_subtype "RNA"
      node_type "species"
      org_id "sa94"
      uniprot "NA"
    ]
    graphics [
      x 750.8794344503556
      y 1158.8494917228832
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_43"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re55"
      uniprot "NA"
    ]
    graphics [
      x 862.5139225132197
      y 1044.3479496725215
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29108"
      hgnc "NA"
      map_id "M117_66"
      name "Ca2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa144"
      uniprot "NA"
    ]
    graphics [
      x 961.2140599994231
      y 244.57554876861263
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      annotation "PUBMED:26331680"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re60"
      uniprot "NA"
    ]
    graphics [
      x 883.6717389637837
      y 173.76770173105376
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P59637;urn:miriam:taxonomy:694009;urn:miriam:ncbigene:1489671;urn:miriam:hgnc.symbol:E"
      hgnc "HGNC_SYMBOL:E"
      map_id "M117_67"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa146"
      uniprot "UNIPROT:P59637"
    ]
    graphics [
      x 894.608234271916
      y 294.7359500156115
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_102"
      name "IKBA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa82"
      uniprot "NA"
    ]
    graphics [
      x 743.7758946635712
      y 1836.0885494198806
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_35"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re46"
      uniprot "NA"
    ]
    graphics [
      x 631.8871842565939
      y 1797.0424222067647
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_23"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re23"
      uniprot "NA"
    ]
    graphics [
      x 704.4955355311193
      y 1376.9253040980952
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:ncbigene:3553"
      hgnc "HGNC_SYMBOL:IL1B"
      map_id "M117_95"
      name "IL_minus_1B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa21"
      uniprot "UNIPROT:P01584"
    ]
    graphics [
      x 775.2106741275675
      y 1483.247743634771
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:3326;urn:miriam:ncbigene:3326;urn:miriam:uniprot:P08238;urn:miriam:hgnc:5258;urn:miriam:refseq:NM_007355;urn:miriam:ensembl:ENSG00000096384;urn:miriam:hgnc.symbol:HSP90AB1;urn:miriam:hgnc.symbol:HSP90AB1"
      hgnc "HGNC_SYMBOL:HSP90AB1"
      map_id "M117_98"
      name "HSP90AB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa4"
      uniprot "UNIPROT:P08238"
    ]
    graphics [
      x 1243.4024372208594
      y 383.1234942209139
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      annotation "PUBMED:29712950;PUBMED:17435760"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_30"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re4"
      uniprot "NA"
    ]
    graphics [
      x 1233.7953645913228
      y 248.93295044405738
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:hgnc:16987;urn:miriam:ncbigene:10910;urn:miriam:ncbigene:10910;urn:miriam:uniprot:Q9Y2Z0;urn:miriam:ensembl:ENSG00000165416;urn:miriam:refseq:NM_001130912;urn:miriam:hgnc.symbol:SUGT1;urn:miriam:hgnc.symbol:SUGT1"
      hgnc "HGNC_SYMBOL:SUGT1"
      map_id "M117_99"
      name "SUGT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa5"
      uniprot "UNIPROT:Q9Y2Z0"
    ]
    graphics [
      x 1324.1484929616622
      y 324.5514960015058
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      annotation "PUBMED:25847972"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_34"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re45"
      uniprot "NA"
    ]
    graphics [
      x 943.3085800884811
      y 750.356417146509
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:NLRP3"
      map_id "M117_56"
      name "NLRP3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa101"
      uniprot "UNIPROT:Q96P20"
    ]
    graphics [
      x 964.3991085996628
      y 581.2581338798307
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      annotation "PUBMED:25847972;PUBMED:25770182;PUBMED:26331680;PUBMED:29789363;PUBMED:28356568;PUBMED:28741645"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_37"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re48"
      uniprot "NA"
    ]
    graphics [
      x 924.4814032447657
      y 416.4430672903478
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.go:GO%3A0002221"
      hgnc "NA"
      map_id "M117_107"
      name "DAMPs"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa91"
      uniprot "NA"
    ]
    graphics [
      x 771.6133373490774
      y 395.5687130761977
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.go:GO%3A0002221"
      hgnc "NA"
      map_id "M117_106"
      name "PAMPs"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa90"
      uniprot "NA"
    ]
    graphics [
      x 812.3456805733266
      y 327.59089777046484
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A46661;urn:miriam:obo.chebi:CHEBI%3A30563;urn:miriam:obo.chebi:CHEBI%3A16336"
      hgnc "NA"
      map_id "M117_60"
      name "NLRP3_space_Elicitor_space_Small_space_Molecules"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa135"
      uniprot "NA"
    ]
    graphics [
      x 879.4924196418432
      y 510.76635744330855
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:hgnc:16400;urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:Q96P20;urn:miriam:ensembl:ENSG00000162711;urn:miriam:ncbigene:351;urn:miriam:refseq:NM_004895;urn:miriam:uniprot:P05067;urn:miriam:hgnc.symbol:APP;urn:miriam:uniprot:P09616;urn:miriam:hgnc.symbol:hly;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:APP;HGNC_SYMBOL:hly;HGNC_SYMBOL:NLRP3"
      map_id "M117_61"
      name "NLRP3_space_Elicitor_space_Proteins"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa136"
      uniprot "UNIPROT:Q96P20;UNIPROT:P05067;UNIPROT:P09616"
    ]
    graphics [
      x 818.1192033184116
      y 452.41000160194244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:3553"
      hgnc "NA"
      map_id "M117_112"
      name "proIL_minus_1B"
      node_subtype "GENE"
      node_type "species"
      org_id "sa96"
      uniprot "NA"
    ]
    graphics [
      x 730.2286418323065
      y 1257.0346058239115
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_28"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re37"
      uniprot "NA"
    ]
    graphics [
      x 599.3620937601477
      y 1262.954918910071
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 114
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_8"
      target_id "M117_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 2
    target 3
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_40"
      target_id "M117_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 2
    target 4
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_40"
      target_id "M117_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 2
    target 5
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_40"
      target_id "M117_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 6
    target 7
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_109"
      target_id "M117_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 7
    target 8
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_44"
      target_id "M117_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 9
    target 10
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_108"
      target_id "M117_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 11
    target 10
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_101"
      target_id "M117_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 10
    target 12
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_46"
      target_id "M117_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 13
    target 14
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_9"
      target_id "M117_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 4
    target 14
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_113"
      target_id "M117_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 14
    target 15
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_39"
      target_id "M117_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 126
    source 4
    target 16
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_113"
      target_id "M117_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 127
    source 17
    target 16
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "M117_84"
      target_id "M117_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 128
    source 16
    target 18
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_53"
      target_id "M117_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 19
    target 20
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_68"
      target_id "M117_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 130
    source 21
    target 20
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_69"
      target_id "M117_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 131
    source 20
    target 22
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_48"
      target_id "M117_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 132
    source 23
    target 24
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_1"
      target_id "M117_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 133
    source 24
    target 11
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_17"
      target_id "M117_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 134
    source 24
    target 25
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_17"
      target_id "M117_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 135
    source 8
    target 26
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_62"
      target_id "M117_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 136
    source 27
    target 26
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "M117_14"
      target_id "M117_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 137
    source 26
    target 28
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_24"
      target_id "M117_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 138
    source 26
    target 29
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_24"
      target_id "M117_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 139
    source 30
    target 31
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_81"
      target_id "M117_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 32
    target 31
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "INHIBITION"
      source_id "M117_78"
      target_id "M117_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 31
    target 33
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_51"
      target_id "M117_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 11
    target 34
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_101"
      target_id "M117_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 35
    target 34
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_100"
      target_id "M117_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 144
    source 34
    target 23
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_27"
      target_id "M117_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 145
    source 36
    target 37
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_71"
      target_id "M117_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 146
    source 22
    target 37
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "UNKNOWN_CATALYSIS"
      source_id "M117_12"
      target_id "M117_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 147
    source 37
    target 38
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_49"
      target_id "M117_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 148
    source 39
    target 40
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_70"
      target_id "M117_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 149
    source 27
    target 40
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "M117_14"
      target_id "M117_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 150
    source 40
    target 41
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_22"
      target_id "M117_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 151
    source 40
    target 42
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_22"
      target_id "M117_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 152
    source 43
    target 44
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_64"
      target_id "M117_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 153
    source 45
    target 44
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "M117_63"
      target_id "M117_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 154
    source 44
    target 46
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_45"
      target_id "M117_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 155
    source 38
    target 47
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_104"
      target_id "M117_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 156
    source 47
    target 48
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_33"
      target_id "M117_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 157
    source 49
    target 50
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_72"
      target_id "M117_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 158
    source 51
    target 50
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_74"
      target_id "M117_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 159
    source 52
    target 50
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_73"
      target_id "M117_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 160
    source 53
    target 50
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "M117_80"
      target_id "M117_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 161
    source 50
    target 54
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_50"
      target_id "M117_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 162
    source 50
    target 55
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_50"
      target_id "M117_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 163
    source 50
    target 56
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_50"
      target_id "M117_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 164
    source 50
    target 32
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_50"
      target_id "M117_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 165
    source 50
    target 57
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_50"
      target_id "M117_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 166
    source 58
    target 59
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_55"
      target_id "M117_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 167
    source 60
    target 59
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_3"
      target_id "M117_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 168
    source 59
    target 61
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_26"
      target_id "M117_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 169
    source 62
    target 63
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_59"
      target_id "M117_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 170
    source 5
    target 63
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_57"
      target_id "M117_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 171
    source 63
    target 13
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_38"
      target_id "M117_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 172
    source 61
    target 64
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_2"
      target_id "M117_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 173
    source 65
    target 64
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "M117_96"
      target_id "M117_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 174
    source 64
    target 66
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_18"
      target_id "M117_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 175
    source 64
    target 67
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_18"
      target_id "M117_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 176
    source 64
    target 68
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_18"
      target_id "M117_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 177
    source 64
    target 69
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_18"
      target_id "M117_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 178
    source 64
    target 60
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_18"
      target_id "M117_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 179
    source 70
    target 71
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_97"
      target_id "M117_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 180
    source 4
    target 71
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_113"
      target_id "M117_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 181
    source 71
    target 72
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_32"
      target_id "M117_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 182
    source 28
    target 73
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_79"
      target_id "M117_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 73
    target 74
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_25"
      target_id "M117_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 48
    target 75
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_103"
      target_id "M117_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 76
    target 75
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_105"
      target_id "M117_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 75
    target 77
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_31"
      target_id "M117_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 9
    target 78
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_108"
      target_id "M117_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 79
    target 78
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_13"
      target_id "M117_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 78
    target 80
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_42"
      target_id "M117_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 81
    target 82
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_16"
      target_id "M117_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 82
    target 27
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_21"
      target_id "M117_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 35
    target 83
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_100"
      target_id "M117_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 84
    target 83
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "TRIGGER"
      source_id "M117_93"
      target_id "M117_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 83
    target 25
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_19"
      target_id "M117_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 77
    target 85
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_5"
      target_id "M117_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 86
    target 85
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "INHIBITION"
      source_id "M117_4"
      target_id "M117_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 85
    target 87
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_36"
      target_id "M117_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 88
    target 89
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_111"
      target_id "M117_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 87
    target 89
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "M117_6"
      target_id "M117_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 89
    target 6
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_29"
      target_id "M117_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 15
    target 90
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_10"
      target_id "M117_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 72
    target 90
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "UNKNOWN_CATALYSIS"
      source_id "M117_7"
      target_id "M117_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 90
    target 1
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_41"
      target_id "M117_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 66
    target 91
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_85"
      target_id "M117_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 69
    target 91
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_86"
      target_id "M117_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 91
    target 81
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_20"
      target_id "M117_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 5
    target 92
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_57"
      target_id "M117_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 17
    target 92
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "M117_84"
      target_id "M117_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 92
    target 18
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_52"
      target_id "M117_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 93
    target 94
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_110"
      target_id "M117_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 94
    target 39
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_43"
      target_id "M117_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 95
    target 96
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_66"
      target_id "M117_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 97
    target 96
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "M117_67"
      target_id "M117_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 96
    target 46
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_47"
      target_id "M117_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 98
    target 99
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_102"
      target_id "M117_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 77
    target 99
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_5"
      target_id "M117_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 99
    target 86
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_35"
      target_id "M117_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 41
    target 100
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_87"
      target_id "M117_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 100
    target 101
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_23"
      target_id "M117_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 102
    target 103
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_98"
      target_id "M117_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 104
    target 103
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_99"
      target_id "M117_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 103
    target 79
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_30"
      target_id "M117_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 3
    target 105
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_58"
      target_id "M117_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 106
    target 105
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_56"
      target_id "M117_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 105
    target 60
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_34"
      target_id "M117_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 80
    target 107
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_15"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 108
    target 107
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M117_107"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 109
    target 107
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M117_106"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 110
    target 107
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M117_60"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 111
    target 107
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M117_61"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 33
    target 107
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M117_54"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 12
    target 107
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M117_11"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 46
    target 107
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M117_65"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 107
    target 106
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_37"
      target_id "M117_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 107
    target 79
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_37"
      target_id "M117_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 112
    target 113
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_112"
      target_id "M117_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 87
    target 113
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "M117_6"
      target_id "M117_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 113
    target 93
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_28"
      target_id "M117_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
