# generated with VANTED V2.8.2 at Fri Mar 04 10:04:32 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4965"
      full_annotation "urn:miriam:ensembl:ENSMUSG00000049115"
      hgnc "NA"
      map_id "W12_9"
      name "Agtr1a"
      node_subtype "GENE"
      node_type "species"
      org_id "e2b6c"
      uniprot "NA"
    ]
    graphics [
      x 473.91981013898226
      y 655.2974576526333
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W12_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:16007097"
      count 1
      diagram "WP4965"
      full_annotation "NA"
      hgnc "NA"
      map_id "W12_18"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idcc7fc0b6"
      uniprot "NA"
    ]
    graphics [
      x 370.1882576590109
      y 564.3301844497845
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W12_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "PUBMED:16007097"
      count 1
      diagram "WP4965"
      full_annotation "NA"
      hgnc "NA"
      map_id "W12_15"
      name "NA"
      node_subtype "UNKNOWN_POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "idaa16182f"
      uniprot "NA"
    ]
    graphics [
      x 603.9685882179429
      y 662.0521483786215
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W12_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4965"
      full_annotation "urn:miriam:wikipathways:WP5035"
      hgnc "NA"
      map_id "W12_2"
      name "Lung_space_injury"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "a6535"
      uniprot "NA"
    ]
    graphics [
      x 686.6451157746824
      y 564.6043755312195
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W12_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4965"
      full_annotation "NA"
      hgnc "NA"
      map_id "W12_19"
      name "NA"
      node_subtype "UNKNOWN_NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idf8eda287"
      uniprot "NA"
    ]
    graphics [
      x 664.74559296756
      y 436.94337115058937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W12_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4965"
      full_annotation "urn:miriam:ensembl:ENSMUSG00000068122"
      hgnc "NA"
      map_id "W12_6"
      name "Agtr2"
      node_subtype "GENE"
      node_type "species"
      org_id "c8f71"
      uniprot "NA"
    ]
    graphics [
      x 559.2919275166652
      y 360.329617322956
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W12_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4965"
      full_annotation "NA"
      hgnc "NA"
      map_id "W12_20"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idfa0e6009"
      uniprot "NA"
    ]
    graphics [
      x 419.50503005756525
      y 405.4175259011716
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W12_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4965"
      full_annotation "urn:miriam:hmdb:HMDB0001035"
      hgnc "NA"
      map_id "W12_5"
      name "Angiotensin_space_II"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "bab13"
      uniprot "NA"
    ]
    graphics [
      x 305.3740202007576
      y 426.65688502090535
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W12_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4965"
      full_annotation "NA"
      hgnc "NA"
      map_id "W12_16"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idc0a82d8"
      uniprot "NA"
    ]
    graphics [
      x 184.26113919920238
      y 359.1587903212681
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W12_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "PUBMED:16007097"
      count 1
      diagram "WP4965"
      full_annotation "NA"
      hgnc "NA"
      map_id "W12_14"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id3c85f717"
      uniprot "NA"
    ]
    graphics [
      x 354.8812711266829
      y 273.54033492304086
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W12_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4965"
      full_annotation "urn:miriam:ensembl:ENSMUSG00000015405"
      hgnc "NA"
      map_id "W12_12"
      name "Ace2"
      node_subtype "GENE"
      node_type "species"
      org_id "f86f8"
      uniprot "NA"
    ]
    graphics [
      x 328.34991777273524
      y 116.89009400573411
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W12_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4965"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A55438"
      hgnc "NA"
      map_id "W12_11"
      name "Angiotensin_minus_(1_minus_7)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "f34cf"
      uniprot "NA"
    ]
    graphics [
      x 460.6305433479838
      y 350.2417074990999
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W12_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "PUBMED:16007097"
      count 1
      diagram "WP4965"
      full_annotation "NA"
      hgnc "NA"
      map_id "W12_13"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id1fae067f"
      uniprot "NA"
    ]
    graphics [
      x 455.8930656116526
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W12_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4965"
      full_annotation "NA"
      hgnc "NA"
      map_id "W12_1"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "a2a1e"
      uniprot "NA"
    ]
    graphics [
      x 166.86755039430767
      y 120.42495380220299
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W12_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4965"
      full_annotation "urn:miriam:hmdb:HMDB0061196"
      hgnc "NA"
      map_id "W12_3"
      name "Angiotensin_space_I"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "ad44a"
      uniprot "NA"
    ]
    graphics [
      x 103.59121668408432
      y 259.6878050919518
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W12_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4965"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A80128"
      hgnc "NA"
      map_id "W12_8"
      name "Angiotensin_minus_(1_minus_9)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "e166c"
      uniprot "NA"
    ]
    graphics [
      x 249.14298777732228
      y 176.22511915406812
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W12_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4965"
      full_annotation "NA"
      hgnc "NA"
      map_id "W12_17"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idc4c8fdab"
      uniprot "NA"
    ]
    graphics [
      x 85.72153580676712
      y 424.5234060815489
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W12_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4965"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A2720"
      hgnc "NA"
      map_id "W12_4"
      name "Angiotensinogen"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "b3515"
      uniprot "NA"
    ]
    graphics [
      x 113.84797909108568
      y 544.6264263036619
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W12_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4965"
      full_annotation "NA"
      hgnc "NA"
      map_id "W12_7"
      name "NA"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "c9c43"
      uniprot "NA"
    ]
    graphics [
      x 579.8859669577043
      y 79.75745393710503
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W12_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4965"
      full_annotation "urn:miriam:ensembl:ENSMUSG00000020681"
      hgnc "NA"
      map_id "W12_10"
      name "Ace"
      node_subtype "GENE"
      node_type "species"
      org_id "ebb43"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 350.50421278313985
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W12_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 21
    source 2
    target 1
    cd19dm [
      diagram "WP4965"
      edge_type "PRODUCTION"
      source_id "W12_18"
      target_id "W12_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 22
    source 1
    target 3
    cd19dm [
      diagram "WP4965"
      edge_type "CONSPUMPTION"
      source_id "W12_9"
      target_id "W12_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 23
    source 8
    target 2
    cd19dm [
      diagram "WP4965"
      edge_type "CONSPUMPTION"
      source_id "W12_5"
      target_id "W12_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 24
    source 3
    target 4
    cd19dm [
      diagram "WP4965"
      edge_type "PRODUCTION"
      source_id "W12_15"
      target_id "W12_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 25
    source 5
    target 4
    cd19dm [
      diagram "WP4965"
      edge_type "PRODUCTION"
      source_id "W12_19"
      target_id "W12_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 26
    source 6
    target 5
    cd19dm [
      diagram "WP4965"
      edge_type "CONSPUMPTION"
      source_id "W12_6"
      target_id "W12_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 27
    source 7
    target 6
    cd19dm [
      diagram "WP4965"
      edge_type "PRODUCTION"
      source_id "W12_20"
      target_id "W12_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 28
    source 8
    target 7
    cd19dm [
      diagram "WP4965"
      edge_type "CONSPUMPTION"
      source_id "W12_5"
      target_id "W12_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 29
    source 9
    target 8
    cd19dm [
      diagram "WP4965"
      edge_type "PRODUCTION"
      source_id "W12_16"
      target_id "W12_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 30
    source 8
    target 10
    cd19dm [
      diagram "WP4965"
      edge_type "CONSPUMPTION"
      source_id "W12_5"
      target_id "W12_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 31
    source 15
    target 9
    cd19dm [
      diagram "WP4965"
      edge_type "CONSPUMPTION"
      source_id "W12_3"
      target_id "W12_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 32
    source 20
    target 9
    cd19dm [
      diagram "WP4965"
      edge_type "CATALYSIS"
      source_id "W12_10"
      target_id "W12_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 33
    source 11
    target 10
    cd19dm [
      diagram "WP4965"
      edge_type "CATALYSIS"
      source_id "W12_12"
      target_id "W12_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 34
    source 10
    target 12
    cd19dm [
      diagram "WP4965"
      edge_type "PRODUCTION"
      source_id "W12_14"
      target_id "W12_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 35
    source 13
    target 11
    cd19dm [
      diagram "WP4965"
      edge_type "PRODUCTION"
      source_id "W12_13"
      target_id "W12_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 36
    source 11
    target 14
    cd19dm [
      diagram "WP4965"
      edge_type "CATALYSIS"
      source_id "W12_12"
      target_id "W12_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 37
    source 19
    target 13
    cd19dm [
      diagram "WP4965"
      edge_type "CONSPUMPTION"
      source_id "W12_7"
      target_id "W12_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 38
    source 15
    target 14
    cd19dm [
      diagram "WP4965"
      edge_type "CONSPUMPTION"
      source_id "W12_3"
      target_id "W12_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 39
    source 14
    target 16
    cd19dm [
      diagram "WP4965"
      edge_type "PRODUCTION"
      source_id "W12_1"
      target_id "W12_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 40
    source 17
    target 15
    cd19dm [
      diagram "WP4965"
      edge_type "PRODUCTION"
      source_id "W12_17"
      target_id "W12_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 41
    source 18
    target 17
    cd19dm [
      diagram "WP4965"
      edge_type "CONSPUMPTION"
      source_id "W12_4"
      target_id "W12_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
