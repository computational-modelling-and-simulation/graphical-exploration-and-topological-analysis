# generated with VANTED V2.8.2 at Fri Mar 04 09:53:02 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:Q9H492"
      hgnc "NA"
      map_id "W7_10"
      name "LC3"
      node_subtype "GENE"
      node_type "species"
      org_id "b3be1"
      uniprot "UNIPROT:Q9H492"
    ]
    graphics [
      x 911.6592840696293
      y 770.5084285738467
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id7b795d10"
      uniprot "NA"
    ]
    graphics [
      x 1011.5200873780129
      y 806.9039291293316
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:obo.chebi:16038"
      hgnc "NA"
      map_id "W7_27"
      name "PE"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "fa8aa"
      uniprot "NA"
    ]
    graphics [
      x 967.2279052647236
      y 698.6419042545192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:Q8WYN0;urn:miriam:uniprot:O95352;urn:miriam:uniprot:Q9NT62"
      hgnc "NA"
      map_id "W7_9"
      name "b392b"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b392b"
      uniprot "UNIPROT:Q8WYN0;UNIPROT:O95352;UNIPROT:Q9NT62"
    ]
    graphics [
      x 860.2333178735115
      y 842.7900946045509
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:obo.chebi:16038;urn:miriam:uniprot:Q9H492"
      hgnc "NA"
      map_id "W7_8"
      name "b35a3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b35a3"
      uniprot "UNIPROT:Q9H492"
    ]
    graphics [
      x 1038.1762426173098
      y 666.6450337594304
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:O94817;urn:miriam:uniprot:Q676U5;urn:miriam:uniprot:Q9H1Y0;urn:miriam:uniprot:Q8NAA4"
      hgnc "NA"
      map_id "W7_22"
      name "e7545"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e7545"
      uniprot "UNIPROT:O94817;UNIPROT:Q676U5;UNIPROT:Q9H1Y0;UNIPROT:Q8NAA4"
    ]
    graphics [
      x 635.2923535510333
      y 672.9101648580437
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_25"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f1056"
      uniprot "NA"
    ]
    graphics [
      x 734.3509690734367
      y 774.5017631474651
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:O94817"
      hgnc "NA"
      map_id "W7_18"
      name "ATG12"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d2b7f"
      uniprot "UNIPROT:O94817"
    ]
    graphics [
      x 192.42392397713985
      y 285.40155823797613
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_2"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "a613c"
      uniprot "NA"
    ]
    graphics [
      x 301.51854694037945
      y 334.43643646231504
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:Q9H1Y0"
      hgnc "NA"
      map_id "W7_14"
      name "ATG5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c82d3"
      uniprot "UNIPROT:Q9H1Y0"
    ]
    graphics [
      x 382.24569482507917
      y 253.31956577331255
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:Q9H0Y0;urn:miriam:uniprot:O95352"
      hgnc "NA"
      map_id "W7_20"
      name "e453d"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e453d"
      uniprot "UNIPROT:Q9H0Y0;UNIPROT:O95352"
    ]
    graphics [
      x 312.0355545260314
      y 200.5215672048646
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:Q9H1Y0;urn:miriam:uniprot:O94817"
      hgnc "NA"
      map_id "W7_1"
      name "a2002"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "a2002"
      uniprot "UNIPROT:Q9H1Y0;UNIPROT:O94817"
    ]
    graphics [
      x 409.91204896595224
      y 461.45084365134653
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_24"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f0c56"
      uniprot "NA"
    ]
    graphics [
      x 528.3703293630431
      y 568.9659701709435
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:Q676U5;urn:miriam:uniprot:Q8NAA4"
      hgnc "NA"
      map_id "W7_6"
      name "ac403"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ac403"
      uniprot "UNIPROT:Q676U5;UNIPROT:Q8NAA4"
    ]
    graphics [
      x 489.4004911827885
      y 675.7469362978949
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:Q5MNZ9;urn:miriam:uniprot:Q9HBF4"
      hgnc "NA"
      map_id "W7_3"
      name "a61b2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "a61b2"
      uniprot "UNIPROT:Q5MNZ9;UNIPROT:Q9HBF4"
    ]
    graphics [
      x 1099.3425133696496
      y 1049.2469259812397
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_35"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id8d83e948"
      uniprot "NA"
    ]
    graphics [
      x 1189.3595456529117
      y 1127.5778629921138
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_23"
      name "Autophagosome"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "e8965"
      uniprot "NA"
    ]
    graphics [
      x 1223.7235554795552
      y 1255.2187842121698
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:Q14457;urn:miriam:uniprot:Q8NEB9;urn:miriam:uniprot:Q99570"
      hgnc "NA"
      map_id "W7_5"
      name "ac27b"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ac27b"
      uniprot "UNIPROT:Q14457;UNIPROT:Q8NEB9;UNIPROT:Q99570"
    ]
    graphics [
      x 187.7646599414848
      y 573.0146744960379
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id3266321"
      uniprot "NA"
    ]
    graphics [
      x 123.44391287056555
      y 684.4947289109473
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:Q9HBF4"
      hgnc "NA"
      map_id "W7_26"
      name "ZFYVE1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f5ed9"
      uniprot "UNIPROT:Q9HBF4"
    ]
    graphics [
      x 62.5
      y 780.9674705284356
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:Q8TDY2;urn:miriam:uniprot:O75385;urn:miriam:uniprot:O75143;urn:miriam:uniprot:Q8IYT8"
      hgnc "NA"
      map_id "W7_15"
      name "autophagy_br_initiation_br_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ce113"
      uniprot "UNIPROT:Q8TDY2;UNIPROT:O75385;UNIPROT:O75143;UNIPROT:Q8IYT8"
    ]
    graphics [
      x 455.2629708076862
      y 385.0968987512799
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_32"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id4f4ebe11"
      uniprot "NA"
    ]
    graphics [
      x 306.90670165201846
      y 489.03966012474825
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_17"
      name "d2646"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d2646"
      uniprot "NA"
    ]
    graphics [
      x 313.1241541063081
      y 604.9003393787898
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_13"
      name "EDEMosome"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "c4176"
      uniprot "NA"
    ]
    graphics [
      x 1393.3822990362519
      y 343.80217567577046
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_30"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id26b440cd"
      uniprot "NA"
    ]
    graphics [
      x 1330.3992310011427
      y 242.5172043706031
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_21"
      name "DMV_space__br_double_space_membrane_space__br_vesicle"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "e5087"
      uniprot "NA"
    ]
    graphics [
      x 1214.526290821328
      y 248.35912614448165
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idbfaf590f"
      uniprot "NA"
    ]
    graphics [
      x 561.7302147408211
      y 271.9048968311011
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:P42345"
      hgnc "NA"
      map_id "W7_19"
      name "MTOR"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "dc0f7"
      uniprot "UNIPROT:P42345"
    ]
    graphics [
      x 638.2983201453458
      y 155.55337944703405
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:ensembl:ENSG00000175224;urn:miriam:uniprot:O75385;urn:miriam:uniprot:Q8IYT8"
      hgnc "NA"
      map_id "W7_28"
      name "fccb6"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "fccb6"
      uniprot "UNIPROT:O75385;UNIPROT:Q8IYT8"
    ]
    graphics [
      x 669.7062260799613
      y 313.36142277894686
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_33"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id5a4fd155"
      uniprot "NA"
    ]
    graphics [
      x 724.5271250316076
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "urn:miriam:uniprot:P42345"
      hgnc "NA"
      map_id "W7_4"
      name "MTOR"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a9b05"
      uniprot "UNIPROT:P42345"
    ]
    graphics [
      x 840.324674706936
      y 80.93865963759481
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_16"
      name "Endoplasmic_space_Reticulum"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "ceb35"
      uniprot "NA"
    ]
    graphics [
      x 1330.2612636129338
      y 551.3231006101919
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_29"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id1d777c00"
      uniprot "NA"
    ]
    graphics [
      x 1403.3917395294038
      y 462.61168560846613
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "PUBMED:24991833"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_37"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idceed974e"
      uniprot "NA"
    ]
    graphics [
      x 1157.817107685706
      y 1375.7976716560008
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_11"
      name "Lysozome"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "b5087"
      uniprot "NA"
    ]
    graphics [
      x 1111.0497072290018
      y 1268.30799680186
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_7"
      name "b2eb5"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b2eb5"
      uniprot "NA"
    ]
    graphics [
      x 1040.5242203826301
      y 1320.1946330629319
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_12"
      name "Autolysosome"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "c2baf"
      uniprot "NA"
    ]
    graphics [
      x 1036.7979893695167
      y 1405.8327064434093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 38
    source 1
    target 2
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "W7_10"
      target_id "W7_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 39
    source 3
    target 2
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "W7_27"
      target_id "W7_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 40
    source 4
    target 2
    cd19dm [
      diagram "WP4863"
      edge_type "CATALYSIS"
      source_id "W7_9"
      target_id "W7_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 41
    source 2
    target 5
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_34"
      target_id "W7_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 42
    source 6
    target 7
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "W7_22"
      target_id "W7_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 43
    source 7
    target 4
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_25"
      target_id "W7_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 44
    source 8
    target 9
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "W7_18"
      target_id "W7_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 45
    source 10
    target 9
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "W7_14"
      target_id "W7_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 46
    source 11
    target 9
    cd19dm [
      diagram "WP4863"
      edge_type "CATALYSIS"
      source_id "W7_20"
      target_id "W7_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 47
    source 9
    target 12
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_2"
      target_id "W7_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 48
    source 12
    target 13
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "W7_1"
      target_id "W7_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 49
    source 14
    target 13
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "W7_6"
      target_id "W7_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 50
    source 13
    target 6
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_24"
      target_id "W7_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 51
    source 15
    target 16
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "W7_3"
      target_id "W7_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 52
    source 16
    target 17
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_35"
      target_id "W7_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 53
    source 18
    target 19
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "W7_5"
      target_id "W7_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 54
    source 19
    target 20
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_31"
      target_id "W7_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 55
    source 21
    target 22
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "W7_15"
      target_id "W7_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 56
    source 23
    target 22
    cd19dm [
      diagram "WP4863"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W7_17"
      target_id "W7_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 57
    source 22
    target 18
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_32"
      target_id "W7_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 58
    source 24
    target 25
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "W7_13"
      target_id "W7_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 59
    source 25
    target 26
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_30"
      target_id "W7_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 60
    source 21
    target 27
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "W7_15"
      target_id "W7_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 61
    source 28
    target 27
    cd19dm [
      diagram "WP4863"
      edge_type "CATALYSIS"
      source_id "W7_19"
      target_id "W7_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 62
    source 27
    target 29
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_36"
      target_id "W7_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 63
    source 28
    target 30
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "W7_19"
      target_id "W7_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 64
    source 30
    target 31
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_33"
      target_id "W7_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 32
    target 33
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "W7_16"
      target_id "W7_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 33
    target 24
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_29"
      target_id "W7_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 17
    target 34
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "W7_23"
      target_id "W7_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 35
    target 34
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "W7_11"
      target_id "W7_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 69
    source 36
    target 34
    cd19dm [
      diagram "WP4863"
      edge_type "INHIBITION"
      source_id "W7_7"
      target_id "W7_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 70
    source 34
    target 37
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_37"
      target_id "W7_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
