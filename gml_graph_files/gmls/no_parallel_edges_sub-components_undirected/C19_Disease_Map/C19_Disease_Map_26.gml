# generated with VANTED V2.8.2 at Fri Mar 04 10:04:38 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:3615;urn:miriam:ncbigene:3615;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:ec-code:1.1.1.205;urn:miriam:uniprot:P12268;urn:miriam:refseq:NM_000884;urn:miriam:hgnc:6053;urn:miriam:ensembl:ENSG00000178035"
      hgnc "HGNC_SYMBOL:IMPDH2"
      map_id "M115_353"
      name "IMPDH2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1395"
      uniprot "UNIPROT:P12268"
    ]
    graphics [
      x 441.61045924940686
      y 1982.5572932182372
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_353"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:18506437;PUBMED:11752352"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_229"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10208"
      uniprot "NA"
    ]
    graphics [
      x 907.9673850663706
      y 1718.944364236203
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_229"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_226"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10205"
      uniprot "NA"
    ]
    graphics [
      x 714.9101628240435
      y 2288.684085468347
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_226"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "PUBMED:17016423;PUBMED:17139284"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_230"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10209"
      uniprot "NA"
    ]
    graphics [
      x 591.6104592494069
      y 1825.3513309131831
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_230"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "PUBMED:17496727"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_227"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10206"
      uniprot "NA"
    ]
    graphics [
      x 1058.8536650413898
      y 919.5029478087847
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_227"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:446541;urn:miriam:pubmed:17496727"
      hgnc "NA"
      map_id "M115_354"
      name "Mycophenolic_space_acid"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1398"
      uniprot "NA"
    ]
    graphics [
      x 668.8536650413897
      y 718.6540118375857
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_354"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:17496727;urn:miriam:ncbigene:3615;urn:miriam:ncbigene:3615;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:ec-code:1.1.1.205;urn:miriam:uniprot:P12268;urn:miriam:refseq:NM_000884;urn:miriam:hgnc:6053;urn:miriam:ensembl:ENSG00000178035;urn:miriam:pubchem.compound:446541;urn:miriam:pubmed:17496727"
      hgnc "HGNC_SYMBOL:IMPDH2"
      map_id "M115_28"
      name "IMcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa124"
      uniprot "UNIPROT:P12268"
    ]
    graphics [
      x 578.8536650413897
      y 876.68758464586
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_235"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re10214"
      uniprot "NA"
    ]
    graphics [
      x 851.4760702265747
      y 1388.3712469335485
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_235"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:446541;urn:miriam:pubmed:17496727"
      hgnc "NA"
      map_id "M115_367"
      name "Mycophenolic_space_acid"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1427"
      uniprot "NA"
    ]
    graphics [
      x 2051.476070226575
      y 1107.676504848212
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_367"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "PUBMED:15570183"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_228"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10207"
      uniprot "NA"
    ]
    graphics [
      x 2102.844259617327
      y 480.8994042744407
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_228"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:5281078"
      hgnc "NA"
      map_id "M115_355"
      name "Mycophenolate_space_mofetil"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1401"
      uniprot "NA"
    ]
    graphics [
      x 1568.0943449360534
      y 222.06502830998443
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_355"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:37542;urn:miriam:doi:10.1016/S0140-6736(20)31042-4"
      hgnc "NA"
      map_id "M115_357"
      name "Ribavirin"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1405"
      uniprot "NA"
    ]
    graphics [
      x 521.4760702265747
      y 1387.4642399717432
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_357"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:17139284;urn:miriam:ncbigene:3615;urn:miriam:ncbigene:3615;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:ec-code:1.1.1.205;urn:miriam:uniprot:P12268;urn:miriam:refseq:NM_000884;urn:miriam:hgnc:6053;urn:miriam:ensembl:ENSG00000178035;urn:miriam:pubchem.compound:37542;urn:miriam:doi:10.1016/S0140-6736(20)31042-4"
      hgnc "HGNC_SYMBOL:IMPDH2"
      map_id "M115_30"
      name "IRcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa126"
      uniprot "UNIPROT:P12268"
    ]
    graphics [
      x 322.5144998161113
      y 2051.176839935622
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:P0C6X7"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_366"
      name "Nsp14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1426"
      uniprot "UNIPROT:P0C6X7"
    ]
    graphics [
      x 577.9673850663706
      y 1579.7908629539697
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_366"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:P0C6X7;urn:miriam:ncbigene:3615;urn:miriam:ncbigene:3615;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:ec-code:1.1.1.205;urn:miriam:uniprot:P12268;urn:miriam:refseq:NM_000884;urn:miriam:hgnc:6053;urn:miriam:ensembl:ENSG00000178035"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:IMPDH2"
      map_id "M115_27"
      name "INPDH2comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa123"
      uniprot "UNIPROT:P0C6X7;UNIPROT:P12268"
    ]
    graphics [
      x 1448.0046788345837
      y 2742.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_233"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10212"
      uniprot "NA"
    ]
    graphics [
      x 2261.476070226575
      y 1433.3419146221868
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_233"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000196150;urn:miriam:refseq:NM_021061;urn:miriam:hgnc.symbol:ZNF250;urn:miriam:hgnc.symbol:ZNF250;urn:miriam:ncbigene:58500;urn:miriam:ncbigene:58500;urn:miriam:uniprot:P15622;urn:miriam:hgnc:13044"
      hgnc "HGNC_SYMBOL:ZNF250"
      map_id "M115_360"
      name "ZNF250"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1415"
      uniprot "UNIPROT:P15622"
    ]
    graphics [
      x 2677.9673850663708
      y 1583.0810686173336
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_360"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:doi:10.1101/2020.06.17.156455;urn:miriam:ensembl:ENSG00000196150;urn:miriam:refseq:NM_021061;urn:miriam:hgnc.symbol:ZNF250;urn:miriam:hgnc.symbol:ZNF250;urn:miriam:ncbigene:58500;urn:miriam:ncbigene:58500;urn:miriam:uniprot:P15622;urn:miriam:hgnc:13044;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:P0C6X7"
      hgnc "HGNC_SYMBOL:ZNF250;HGNC_SYMBOL:rep"
      map_id "M115_33"
      name "ZNF250comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa129"
      uniprot "UNIPROT:P15622;UNIPROT:P0C6X7"
    ]
    graphics [
      x 2108.85366504139
      y 893.5130198755393
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:667490"
      hgnc "NA"
      map_id "M115_356"
      name "Mercaptopurine"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1402"
      uniprot "NA"
    ]
    graphics [
      x 682.5144998161113
      y 2089.918911278871
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_356"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:18506437;urn:miriam:pubchem.compound:667490;urn:miriam:ncbigene:3615;urn:miriam:ncbigene:3615;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:ec-code:1.1.1.205;urn:miriam:uniprot:P12268;urn:miriam:refseq:NM_000884;urn:miriam:hgnc:6053;urn:miriam:ensembl:ENSG00000178035"
      hgnc "HGNC_SYMBOL:IMPDH2"
      map_id "M115_29"
      name "IMercomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa125"
      uniprot "UNIPROT:P12268"
    ]
    graphics [
      x 1436.685891421194
      y 2202.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 21
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_353"
      target_id "M115_229"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 22
    source 1
    target 3
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_353"
      target_id "M115_226"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 23
    source 1
    target 4
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_353"
      target_id "M115_230"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 24
    source 1
    target 5
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_353"
      target_id "M115_227"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 25
    source 19
    target 2
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_356"
      target_id "M115_229"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 26
    source 2
    target 20
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_229"
      target_id "M115_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 27
    source 14
    target 3
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_366"
      target_id "M115_226"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 28
    source 3
    target 15
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_226"
      target_id "M115_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 29
    source 12
    target 4
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_357"
      target_id "M115_230"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 30
    source 4
    target 13
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_230"
      target_id "M115_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 31
    source 6
    target 5
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_354"
      target_id "M115_227"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 32
    source 5
    target 7
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_227"
      target_id "M115_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 33
    source 8
    target 6
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_235"
      target_id "M115_354"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 34
    source 9
    target 8
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_367"
      target_id "M115_235"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 35
    source 10
    target 9
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_228"
      target_id "M115_367"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 36
    source 11
    target 10
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_355"
      target_id "M115_228"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 37
    source 14
    target 16
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_366"
      target_id "M115_233"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 38
    source 17
    target 16
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_360"
      target_id "M115_233"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 39
    source 16
    target 18
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_233"
      target_id "M115_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
