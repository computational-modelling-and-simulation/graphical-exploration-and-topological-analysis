# generated with VANTED V2.8.2 at Fri Mar 04 10:04:38 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15927"
      hgnc "NA"
      map_id "M123_145"
      name "NR"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa238"
      uniprot "NA"
    ]
    graphics [
      x 1148.8536650413898
      y 803.0689340107122
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_145"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_57"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re70"
      uniprot "NA"
    ]
    graphics [
      x 892.5144998161113
      y 1861.4477491325204
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_55"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re69"
      uniprot "NA"
    ]
    graphics [
      x 1628.8536650413898
      y 852.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30616"
      hgnc "NA"
      map_id "M123_146"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa239"
      uniprot "NA"
    ]
    graphics [
      x 1061.4760702265748
      y 1284.417231836038
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_146"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000106733;urn:miriam:ec-code:2.7.1.173;urn:miriam:uniprot:Q9NWW6;urn:miriam:ncbigene:54981;urn:miriam:refseq:NM_017881;urn:miriam:ncbigene:54981;urn:miriam:ec-code:2.7.1.22;urn:miriam:hgnc:26057;urn:miriam:hgnc.symbol:NMRK1;urn:miriam:hgnc.symbol:NMRK1"
      hgnc "HGNC_SYMBOL:NMRK1"
      map_id "M123_143"
      name "NMRK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa236"
      uniprot "UNIPROT:Q9NWW6"
    ]
    graphics [
      x 1271.4760702265748
      y 1272.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_143"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A14649"
      hgnc "NA"
      map_id "M123_131"
      name "NMN"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa223"
      uniprot "NA"
    ]
    graphics [
      x 712.5144998161113
      y 2075.6209345235757
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_131"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A456216"
      hgnc "NA"
      map_id "M123_124"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa217"
      uniprot "NA"
    ]
    graphics [
      x 2167.9673850663708
      y 1554.2161322491042
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_124"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "M123_142"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa235"
      uniprot "NA"
    ]
    graphics [
      x 1151.4760702265748
      y 1264.672885997767
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_142"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re68"
      uniprot "NA"
    ]
    graphics [
      x 791.3573409585846
      y 2264.8398419206333
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30616"
      hgnc "NA"
      map_id "M123_136"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa229"
      uniprot "NA"
    ]
    graphics [
      x 592.5144998161113
      y 2035.682533944394
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:uniprot:Q9BZQ4;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:obo.chebi:CHEBI%3A18420;urn:miriam:uniprot:Q9BZQ4;urn:miriam:uniprot:Q9BZQ4;urn:miriam:ncbigene:23057;urn:miriam:ncbigene:23057;urn:miriam:refseq:NM_015039;urn:miriam:ec-code:2.7.7.18;urn:miriam:hgnc:16789;urn:miriam:ec-code:2.7.7.1;urn:miriam:hgnc.symbol:NMNAT2;urn:miriam:hgnc.symbol:NMNAT2;urn:miriam:ensembl:ENSG00000157064"
      hgnc "HGNC_SYMBOL:NMNAT2"
      map_id "M123_2"
      name "NMNAT2:Mg2_plus_"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa16"
      uniprot "UNIPROT:Q9BZQ4"
    ]
    graphics [
      x 1360.4075582174767
      y 2502.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57540"
      hgnc "NA"
      map_id "M123_130"
      name "NAD_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa222"
      uniprot "NA"
    ]
    graphics [
      x 1031.4760702265748
      y 1442.3785906698167
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_130"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A33019"
      hgnc "NA"
      map_id "M123_135"
      name "PPi(3_minus_)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa228"
      uniprot "NA"
    ]
    graphics [
      x 498.0386231355054
      y 2385.022051984028
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_53"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re67"
      uniprot "NA"
    ]
    graphics [
      x 537.1571438322437
      y 2429.813283467075
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_63"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re79"
      uniprot "NA"
    ]
    graphics [
      x 1084.973418093434
      y 2547.198533124115
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_59"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re73"
      uniprot "NA"
    ]
    graphics [
      x 922.5144998161113
      y 1876.6019746441584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A133202"
      hgnc "NA"
      map_id "M123_140"
      name "(ADP_minus_D_minus_ribosyl)(n)_minus_acceptor"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa233"
      uniprot "NA"
    ]
    graphics [
      x 1300.4146427238684
      y 2262.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_140"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:uniprot:Q53GL7;urn:miriam:ncbigene:54956;urn:miriam:ec-code:2.4.2.-;urn:miriam:uniprot:Q8N5Y8;urn:miriam:ncbigene:84875;urn:miriam:ncbigene:83666;urn:miriam:hgnc.symbol:PARP9;urn:miriam:hgnc.symbol:PARP10;urn:miriam:hgnc.symbol:PARP16;urn:miriam:uniprot:Q8IXQ6"
      hgnc "HGNC_SYMBOL:PARP9;HGNC_SYMBOL:PARP10;HGNC_SYMBOL:PARP16"
      map_id "M123_116"
      name "PARPs"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa209"
      uniprot "UNIPROT:Q53GL7;UNIPROT:Q8N5Y8;UNIPROT:Q8IXQ6"
    ]
    graphics [
      x 1327.9673850663708
      y 1602.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17154"
      hgnc "NA"
      map_id "M123_129"
      name "NAM"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa221"
      uniprot "NA"
    ]
    graphics [
      x 1450.4146427238684
      y 2232.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_129"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A133203"
      hgnc "NA"
      map_id "M123_141"
      name "(ADP_minus_D_minus_ribosyl)(n_plus_1)_minus_acceptor"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa234"
      uniprot "NA"
    ]
    graphics [
      x 577.6609120566156
      y 2140.9677343238864
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_141"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_61"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re75"
      uniprot "NA"
    ]
    graphics [
      x 2260.4146427238684
      y 2219.2593146530544
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_60"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re74"
      uniprot "NA"
    ]
    graphics [
      x 1495.2508552528402
      y 2562.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17111"
      hgnc "NA"
      map_id "M123_118"
      name "PRPP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa210"
      uniprot "NA"
    ]
    graphics [
      x 1139.1188547295465
      y 2331.4133073492326
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:hgnc.symbol:NAMPT;urn:miriam:hgnc.symbol:NAMPT;urn:miriam:ec-code:2.4.2.12;urn:miriam:hgnc:30092;urn:miriam:uniprot:P43490;urn:miriam:ncbigene:10135;urn:miriam:refseq:NM_182790;urn:miriam:ncbigene:10135;urn:miriam:ensembl:ENSG00000105835"
      hgnc "HGNC_SYMBOL:NAMPT"
      map_id "M123_113"
      name "NAMPT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa206"
      uniprot "UNIPROT:P43490"
    ]
    graphics [
      x 1370.5824814748505
      y 2742.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29888"
      hgnc "NA"
      map_id "M123_100"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa193"
      uniprot "NA"
    ]
    graphics [
      x 1411.6305234852287
      y 2682.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15763"
      hgnc "NA"
      map_id "M123_104"
      name "NAMN"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa199"
      uniprot "NA"
    ]
    graphics [
      x 1900.4146427238684
      y 2140.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_51"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re65"
      uniprot "NA"
    ]
    graphics [
      x 607.9673850663706
      y 1742.6183265894208
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_52"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re66"
      uniprot "NA"
    ]
    graphics [
      x 610.4146427238683
      y 2097.6366193546137
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_58"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re71"
      uniprot "NA"
    ]
    graphics [
      x 1814.8815294551364
      y 2638.141279820647
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_62"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re78"
      uniprot "NA"
    ]
    graphics [
      x 1604.6089340096498
      y 2548.581187646395
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_50"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re64"
      uniprot "NA"
    ]
    graphics [
      x 1056.7483388853393
      y 2456.1759253180135
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18304"
      hgnc "NA"
      map_id "M123_107"
      name "NAAD"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa200"
      uniprot "NA"
    ]
    graphics [
      x 833.0972691745656
      y 2263.853912881765
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A33019"
      hgnc "NA"
      map_id "M123_110"
      name "PPi(3_minus_)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa203"
      uniprot "NA"
    ]
    graphics [
      x 1612.5144998161113
      y 2052.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16675"
      hgnc "NA"
      map_id "M123_237"
      name "QUIN"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa41"
      uniprot "NA"
    ]
    graphics [
      x 1972.5144998161113
      y 2027.606843746084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_237"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17111"
      hgnc "NA"
      map_id "M123_109"
      name "PRPP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa202"
      uniprot "NA"
    ]
    graphics [
      x 1103.4244474734132
      y 2466.795186124574
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "M123_102"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa195"
      uniprot "NA"
    ]
    graphics [
      x 2000.0730886186202
      y 2520.237122509435
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:hgnc:9755;urn:miriam:hgnc.symbol:QPRT;urn:miriam:ncbigene:23475;urn:miriam:ensembl:ENSG00000103485;urn:miriam:hgnc.symbol:QPRT;urn:miriam:ncbigene:23475;urn:miriam:ec-code:2.4.2.19;urn:miriam:uniprot:Q15274;urn:miriam:refseq:NM_014298"
      hgnc "HGNC_SYMBOL:QPRT"
      map_id "M123_114"
      name "QPRT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa207"
      uniprot "UNIPROT:Q15274"
    ]
    graphics [
      x 790.4146427238683
      y 2112.3375438517005
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16526"
      hgnc "NA"
      map_id "M123_108"
      name "CO2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa201"
      uniprot "NA"
    ]
    graphics [
      x 1822.5935627164836
      y 2244.6594484469133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "M123_101"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa194"
      uniprot "NA"
    ]
    graphics [
      x 1810.4146427238684
      y 2119.6311396414317
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_32"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re16"
      uniprot "NA"
    ]
    graphics [
      x 1567.5411537873542
      y 1422.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29044"
      hgnc "NA"
      map_id "M123_229"
      name "ACS"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa38"
      uniprot "NA"
    ]
    graphics [
      x 1177.9673850663708
      y 1663.0804809737745
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_229"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "M123_217"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa36"
      uniprot "NA"
    ]
    graphics [
      x 1457.2023363703734
      y 942.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_217"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_14"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10"
      uniprot "NA"
    ]
    graphics [
      x 1027.9673850663708
      y 1712.3785906698167
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_23"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re11"
      uniprot "NA"
    ]
    graphics [
      x 701.4760702265747
      y 1487.5174839457195
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:refseq:NM_001307983;urn:miriam:hgnc.symbol:ACMSD;urn:miriam:hgnc.symbol:ACMSD;urn:miriam:ncbigene:130013;urn:miriam:ncbigene:130013;urn:miriam:hgnc:19288;urn:miriam:ensembl:ENSG00000153086;urn:miriam:uniprot:Q8TDX5;urn:miriam:ec-code:4.1.1.45"
      hgnc "HGNC_SYMBOL:ACMSD"
      map_id "M123_220"
      name "ACMSD"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa37"
      uniprot "UNIPROT:Q8TDX5"
    ]
    graphics [
      x 997.9673850663706
      y 1720.1472520551508
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_220"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15745"
      hgnc "NA"
      map_id "M123_242"
      name "2AMA"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa47"
      uniprot "NA"
    ]
    graphics [
      x 581.4760702265747
      y 1467.3630500931401
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_242"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16526"
      hgnc "NA"
      map_id "M123_236"
      name "CO2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa40"
      uniprot "NA"
    ]
    graphics [
      x 940.4146427238683
      y 2086.6019746441584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_236"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re12"
      uniprot "NA"
    ]
    graphics [
      x 1251.6104592494069
      y 1782.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "M123_241"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa46"
      uniprot "NA"
    ]
    graphics [
      x 1091.4760702265748
      y 1187.5375698961307
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_241"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57540"
      hgnc "NA"
      map_id "M123_240"
      name "NAD_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa45"
      uniprot "NA"
    ]
    graphics [
      x 1672.5144998161113
      y 2076.690484564035
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_240"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16886"
      hgnc "NA"
      map_id "M123_243"
      name "2AM"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa49"
      uniprot "NA"
    ]
    graphics [
      x 1672.5144998161113
      y 1976.3553958210216
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_243"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57945"
      hgnc "NA"
      map_id "M123_239"
      name "NADH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa44"
      uniprot "NA"
    ]
    graphics [
      x 1851.6104592494069
      y 1776.0412632645964
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_239"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "M123_238"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa43"
      uniprot "NA"
    ]
    graphics [
      x 1462.5144998161113
      y 1902.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_238"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15793"
      hgnc "NA"
      map_id "M123_187"
      name "3HAA"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa31"
      uniprot "NA"
    ]
    graphics [
      x 2050.4146427238684
      y 2161.8305683994477
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_187"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15379"
      hgnc "NA"
      map_id "M123_234"
      name "O2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa39"
      uniprot "NA"
    ]
    graphics [
      x 1021.2861750633202
      y 2271.9577197196427
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_234"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:uniprot:P46952;urn:miriam:ncbigene:23498;urn:miriam:ncbigene:23498;urn:miriam:hgnc:4796;urn:miriam:ensembl:ENSG00000162882;urn:miriam:refseq:NM_012205;urn:miriam:hgnc.symbol:HAAO;urn:miriam:hgnc.symbol:HAAO;urn:miriam:ec-code:1.13.11.6"
      hgnc "HGNC_SYMBOL:HAAO"
      map_id "M123_186"
      name "HAAO"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa30"
      uniprot "UNIPROT:P46952"
    ]
    graphics [
      x 652.5144998161113
      y 1962.1662202339423
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_186"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_71"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re9"
      uniprot "NA"
    ]
    graphics [
      x 1251.6509138727415
      y 2656.0964960198417
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      annotation "PUBMED:12186837"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_28"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re116"
      uniprot "NA"
    ]
    graphics [
      x 1571.4760702265748
      y 1482.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      annotation "PUBMED:17476692"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_26"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re112"
      uniprot "NA"
    ]
    graphics [
      x 1592.8857170263236
      y 2502.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "PUBMED:23123095"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_35"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re20"
      uniprot "NA"
    ]
    graphics [
      x 1612.5144998161113
      y 2022.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      annotation "PUBMED:23123095"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_27"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re115"
      uniprot "NA"
    ]
    graphics [
      x 1270.4146427238684
      y 2202.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "PUBMED:15249210;PUBMED:12706494;PUBMED:11287117"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re103"
      uniprot "NA"
    ]
    graphics [
      x 1185.7419667792801
      y 2412.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "PUBMED:17948274"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_30"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re119"
      uniprot "NA"
    ]
    graphics [
      x 1580.0074341427699
      y 2412.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17380"
      hgnc "NA"
      map_id "M123_193"
      name "3HKYN"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa32"
      uniprot "NA"
    ]
    graphics [
      x 1642.5144998161113
      y 1932.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_193"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0097029"
      hgnc "NA"
      map_id "M123_225"
      name "Dendritic_space_cell_space_maturation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa376"
      uniprot "NA"
    ]
    graphics [
      x 1072.5144998161113
      y 2017.7492719293396
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_225"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_64"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re8"
      uniprot "NA"
    ]
    graphics [
      x 2291.476070226575
      y 1170.5576780288498
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16946"
      hgnc "NA"
      map_id "M123_91"
      name "L_minus_Kynurenine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa17"
      uniprot "NA"
    ]
    graphics [
      x 1177.9673850663708
      y 1603.0804809737745
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15379"
      hgnc "NA"
      map_id "M123_127"
      name "O2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa22"
      uniprot "NA"
    ]
    graphics [
      x 1731.6104592494069
      y 1685.53458931237
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_127"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16474"
      hgnc "NA"
      map_id "M123_147"
      name "NADPH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa24"
      uniprot "NA"
    ]
    graphics [
      x 1747.9673850663708
      y 1585.9277406352314
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_147"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "M123_151"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa25"
      uniprot "NA"
    ]
    graphics [
      x 2081.476070226575
      y 1123.7082153288716
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_151"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:hgnc.symbol:KMO;urn:miriam:hgnc.symbol:KMO;urn:miriam:uniprot:O15229;urn:miriam:ncbigene:8564;urn:miriam:ncbigene:8564;urn:miriam:ec-code:1.14.13.9;urn:miriam:hgnc:6381;urn:miriam:ensembl:ENSG00000117009;urn:miriam:refseq:NM_003679"
      hgnc "HGNC_SYMBOL:KMO"
      map_id "M123_226"
      name "KMO"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa377"
      uniprot "UNIPROT:O15229"
    ]
    graphics [
      x 2111.476070226575
      y 1037.7871053251297
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_226"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "M123_117"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa21"
      uniprot "NA"
    ]
    graphics [
      x 2711.476070226575
      y 1166.0871619512736
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18009"
      hgnc "NA"
      map_id "M123_137"
      name "NADP_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa23"
      uniprot "NA"
    ]
    graphics [
      x 2081.476070226575
      y 1033.1270120257964
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_137"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      annotation "PUBMED:29531094"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_44"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re4"
      uniprot "NA"
    ]
    graphics [
      x 1612.5144998161113
      y 1962.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_48"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re5"
      uniprot "NA"
    ]
    graphics [
      x 697.9673850663706
      y 1541.099716096972
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_49"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re6"
      uniprot "NA"
    ]
    graphics [
      x 1282.5144998161113
      y 1962.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      annotation "PUBMED:28673995;PUBMED:23123095;PUBMED:29531094;PUBMED:21041655"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_33"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re19"
      uniprot "NA"
    ]
    graphics [
      x 817.9673850663706
      y 1536.3585216247106
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:hgnc:348;urn:miriam:hgnc.symbol:AHR;urn:miriam:hgnc.symbol:AHR;urn:miriam:ensembl:ENSG00000106546;urn:miriam:uniprot:P35869;urn:miriam:refseq:NM_001621;urn:miriam:ncbigene:196;urn:miriam:ncbigene:196"
      hgnc "HGNC_SYMBOL:AHR"
      map_id "M123_162"
      name "AHR"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa26"
      uniprot "UNIPROT:P35869"
    ]
    graphics [
      x 487.96738506637064
      y 1688.8328798263947
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_162"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:hgnc:348;urn:miriam:hgnc.symbol:AHR;urn:miriam:hgnc.symbol:AHR;urn:miriam:ensembl:ENSG00000106546;urn:miriam:uniprot:P35869;urn:miriam:refseq:NM_001621;urn:miriam:ncbigene:196;urn:miriam:ncbigene:196;urn:miriam:obo.chebi:CHEBI%3A16946"
      hgnc "HGNC_SYMBOL:AHR"
      map_id "M123_4"
      name "AHR_slash_L_minus_KYN"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa2"
      uniprot "UNIPROT:P35869"
    ]
    graphics [
      x 1610.0074341427699
      y 2352.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      annotation "PUBMED:28673995;PUBMED:23123095;PUBMED:21041655"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_36"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re21"
      uniprot "NA"
    ]
    graphics [
      x 1991.2332957398262
      y 2490.237122509435
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0045066"
      hgnc "NA"
      map_id "M123_207"
      name "Regulatory_space_T_minus_cell_space_generation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa35"
      uniprot "NA"
    ]
    graphics [
      x 1900.4146427238684
      y 2260.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_207"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15361"
      hgnc "NA"
      map_id "M123_106"
      name "PYR"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa20"
      uniprot "NA"
    ]
    graphics [
      x 502.5144998161113
      y 2007.8868492643273
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:uniprot:Q6YP21;urn:miriam:ncbigene:56267;urn:miriam:ec-code:4.4.1.13;urn:miriam:hgnc.symbol:KYAT3;urn:miriam:ec-code:2.6.1.7;urn:miriam:ec-code:2.6.1.63"
      hgnc "HGNC_SYMBOL:KYAT3"
      map_id "M123_96"
      name "CCBL2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa19"
      uniprot "UNIPROT:Q6YP21"
    ]
    graphics [
      x 1312.5144998161113
      y 1812.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57972"
      hgnc "NA"
      map_id "M123_180"
      name "L_minus_Ala"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa29"
      uniprot "NA"
    ]
    graphics [
      x 562.5144998161113
      y 2054.9230009260687
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_180"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:uniprot:Q16773;urn:miriam:obo.chebi:CHEBI%3A18405;urn:miriam:uniprot:Q16773;urn:miriam:uniprot:Q16773;urn:miriam:hgnc:1564;urn:miriam:refseq:NM_001122671;urn:miriam:ec-code:4.4.1.13;urn:miriam:ncbigene:883;urn:miriam:ncbigene:883;urn:miriam:ensembl:ENSG00000171097;urn:miriam:ec-code:2.6.1.7;urn:miriam:hgnc.symbol:KYAT1;urn:miriam:hgnc.symbol:KYAT1;urn:miriam:ec-code:2.6.1.64"
      hgnc "HGNC_SYMBOL:KYAT1"
      map_id "M123_1"
      name "PXLP_minus_KYAT1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa1"
      uniprot "UNIPROT:Q16773"
    ]
    graphics [
      x 937.9673850663706
      y 1726.6019746441584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A43580"
      hgnc "NA"
      map_id "M123_87"
      name "I3PROPA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa13"
      uniprot "NA"
    ]
    graphics [
      x 551.4760702265747
      y 1395.5551199408903
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A58095"
      hgnc "NA"
      map_id "M123_90"
      name "L_minus_Phe"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa16"
      uniprot "NA"
    ]
    graphics [
      x 1434.3653418164047
      y 1512.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A55528"
      hgnc "NA"
      map_id "M123_88"
      name "I3LACT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa14"
      uniprot "NA"
    ]
    graphics [
      x 1305.5674546932569
      y 2022.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17442"
      hgnc "NA"
      map_id "M123_86"
      name "AP_minus_DOBu"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa12"
      uniprot "NA"
    ]
    graphics [
      x 371.47607022657485
      y 1341.0248575265991
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_56"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re7"
      uniprot "NA"
    ]
    graphics [
      x 937.9673850663706
      y 1696.6019746441584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18344"
      hgnc "NA"
      map_id "M123_173"
      name "KYNA"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa28"
      uniprot "NA"
    ]
    graphics [
      x 757.9673850663706
      y 1772.1429123880987
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_173"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "M123_171"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa27"
      uniprot "NA"
    ]
    graphics [
      x 802.5144998161113
      y 1977.6236763137715
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_171"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18377"
      hgnc "NA"
      map_id "M123_85"
      name "NFK"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa11"
      uniprot "NA"
    ]
    graphics [
      x 622.5144998161113
      y 2015.4704384112736
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "M123_75"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa10"
      uniprot "NA"
    ]
    graphics [
      x 1270.4146427238684
      y 2262.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:hgnc:20910;urn:miriam:refseq:NM_001145526;urn:miriam:ec-code:3.5.1.9;urn:miriam:uniprot:Q63HM1;urn:miriam:hgnc.symbol:AFMID;urn:miriam:ncbigene:125061;urn:miriam:ensembl:ENSG00000183077;urn:miriam:hgnc.symbol:AFMID;urn:miriam:ncbigene:125061"
      hgnc "HGNC_SYMBOL:AFMID"
      map_id "M123_92"
      name "AFMID"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa18"
      uniprot "UNIPROT:Q63HM1"
    ]
    graphics [
      x 1297.530909446052
      y 2472.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30751"
      hgnc "NA"
      map_id "M123_89"
      name "HCOOH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa15"
      uniprot "NA"
    ]
    graphics [
      x 1490.0074341427699
      y 2412.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      annotation "PUBMED:30338242;PUBMED:32292563;PUBMED:29531094;PUBMED:28523098"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_42"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re3"
      uniprot "NA"
    ]
    graphics [
      x 1631.5658535358825
      y 2659.8734195242405
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re2"
      uniprot "NA"
    ]
    graphics [
      x 1091.4760702265748
      y 1067.2486959125001
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57912"
      hgnc "NA"
      map_id "M123_74"
      name "L_minus_Tryptophan"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa1"
      uniprot "NA"
    ]
    graphics [
      x 1720.4146427238684
      y 2226.313882011814
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15379"
      hgnc "NA"
      map_id "M123_235"
      name "O2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa4"
      uniprot "NA"
    ]
    graphics [
      x 1897.9673850663708
      y 1570.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_235"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:hgnc.symbol:IDO2;urn:miriam:hgnc.symbol:IDO2;urn:miriam:ensembl:ENSG00000188676;urn:miriam:ec-code:1.13.11.-;urn:miriam:uniprot:Q6ZQW0;urn:miriam:hgnc:27269;urn:miriam:ncbigene:169355;urn:miriam:ncbigene:169355;urn:miriam:refseq:NM_194294"
      hgnc "HGNC_SYMBOL:IDO2"
      map_id "M123_252"
      name "IDO2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa7"
      uniprot "UNIPROT:Q6ZQW0"
    ]
    graphics [
      x 1148.8536650413898
      y 833.0689340107122
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_252"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_13"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1"
      uniprot "NA"
    ]
    graphics [
      x 2020.4146427238684
      y 2233.818159539726
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57912"
      hgnc "NA"
      map_id "M123_105"
      name "L_minus_Tryptophan"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa2"
      uniprot "NA"
    ]
    graphics [
      x 1430.0074341427699
      y 2322.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:hgnc.symbol:SLC36A4;urn:miriam:hgnc.symbol:SLC36A4;urn:miriam:refseq:NM_001286139;urn:miriam:uniprot:Q6YBV0;urn:miriam:ensembl:ENSG00000180773;urn:miriam:ncbigene:120103;urn:miriam:ncbigene:120103;urn:miriam:hgnc:19660"
      hgnc "HGNC_SYMBOL:SLC36A4"
      map_id "M123_253"
      name "SLC36A4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa8"
      uniprot "UNIPROT:Q6YBV0"
    ]
    graphics [
      x 1450.4146427238684
      y 2262.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_253"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      annotation "PUBMED:21041655"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_25"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re111"
      uniprot "NA"
    ]
    graphics [
      x 951.6104592494069
      y 1816.6019746441584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.go:GO%3A1902483"
      hgnc "NA"
      map_id "M123_221"
      name "Effector_space_T_minus_cell_space_apoptosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa371"
      uniprot "NA"
    ]
    graphics [
      x 877.9673850663706
      y 1613.2421714207787
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_221"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:uniprot:P14902;urn:miriam:obo.chebi:CHEBI%3A30413;urn:miriam:hgnc:6059;urn:miriam:hgnc.symbol:IDO1;urn:miriam:uniprot:P14902;urn:miriam:uniprot:P14902;urn:miriam:hgnc.symbol:IDO1;urn:miriam:ensembl:ENSG00000131203;urn:miriam:refseq:NM_002164;urn:miriam:ncbigene:3620;urn:miriam:ncbigene:3620;urn:miriam:ec-code:1.13.11.52"
      hgnc "HGNC_SYMBOL:IDO1"
      map_id "M123_12"
      name "IDO1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa39"
      uniprot "UNIPROT:P14902"
    ]
    graphics [
      x 1683.5980350148664
      y 2723.1600997617616
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_185"
      name "Epacadostat"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa3"
      uniprot "NA"
    ]
    graphics [
      x 1960.4146427238684
      y 2237.606843746084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_185"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      annotation "PUBMED:32292563;PUBMED:29531094;PUBMED:28523098"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_24"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re110"
      uniprot "NA"
    ]
    graphics [
      x 1547.3197119766157
      y 2669.916342950497
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30413"
      hgnc "NA"
      map_id "M123_213"
      name "Heme"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa355"
      uniprot "NA"
    ]
    graphics [
      x 2020.4146427238684
      y 2136.313744265185
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_213"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:uniprot:P14902;urn:miriam:hgnc.symbol:IDO1;urn:miriam:ncbigene:3620;urn:miriam:ec-code:1.13.11.52"
      hgnc "HGNC_SYMBOL:IDO1"
      map_id "M123_244"
      name "apo_minus_IDO1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa5"
      uniprot "UNIPROT:P14902"
    ]
    graphics [
      x 1640.0074341427699
      y 2382.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_244"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_41"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re28"
      uniprot "NA"
    ]
    graphics [
      x 2227.9673850663708
      y 1640.5567968186347
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:hgnc:6059;urn:miriam:hgnc.symbol:IDO1;urn:miriam:uniprot:P14902;urn:miriam:ensembl:ENSG00000131203;urn:miriam:refseq:NM_002164;urn:miriam:ncbigene:3620"
      hgnc "HGNC_SYMBOL:IDO1"
      map_id "M123_251"
      name "IDO1"
      node_subtype "RNA"
      node_type "species"
      org_id "sa65"
      uniprot "UNIPROT:P14902"
    ]
    graphics [
      x 1571.4760702265748
      y 1122.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_251"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      annotation "PUBMED:15249210;PUBMED:9126284;PUBMED:23476103"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_37"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re22"
      uniprot "NA"
    ]
    graphics [
      x 742.5144998161113
      y 1925.224839764994
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:hgnc:6059;urn:miriam:hgnc.symbol:IDO1;urn:miriam:uniprot:P14902;urn:miriam:ensembl:ENSG00000131203;urn:miriam:refseq:NM_002164;urn:miriam:ncbigene:3620"
      hgnc "HGNC_SYMBOL:IDO1"
      map_id "M123_246"
      name "IDO1"
      node_subtype "GENE"
      node_type "species"
      org_id "sa58"
      uniprot "UNIPROT:P14902"
    ]
    graphics [
      x 397.96738506637075
      y 1862.5572932182372
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_246"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16480"
      hgnc "NA"
      map_id "M123_250"
      name "NO"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa64"
      uniprot "NA"
    ]
    graphics [
      x 757.9673850663706
      y 1643.1178987887638
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_250"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:uniprot:Q14609"
      hgnc "NA"
      map_id "M123_249"
      name "IFN_minus_G"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa62"
      uniprot "UNIPROT:Q14609"
    ]
    graphics [
      x 592.5144998161113
      y 2005.5058093658079
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_249"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      annotation "PUBMED:15249210;PUBMED:9126284"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_38"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re23"
      uniprot "NA"
    ]
    graphics [
      x 1625.6948985592885
      y 2502.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:ncbigene:4843"
      hgnc "NA"
      map_id "M123_245"
      name "iNOS"
      node_subtype "GENE"
      node_type "species"
      org_id "sa57"
      uniprot "NA"
    ]
    graphics [
      x 1570.4146427238684
      y 2172.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_245"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:uniprot:Q00653;urn:miriam:uniprot:P19838;urn:miriam:uniprot:Q04206;urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998;urn:miriam:hgnc:7795;urn:miriam:refseq:NM_001077494;urn:miriam:hgnc.symbol:NFKB2;urn:miriam:ncbigene:4791;urn:miriam:hgnc.symbol:NFKB2;urn:miriam:ncbigene:4791;urn:miriam:ensembl:ENSG00000077150;urn:miriam:uniprot:Q00653;urn:miriam:uniprot:Q00653;urn:miriam:hgnc:9955;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ncbigene:5970;urn:miriam:ncbigene:5970;urn:miriam:refseq:NM_021975;urn:miriam:hgnc.symbol:RELA;urn:miriam:hgnc.symbol:RELA;urn:miriam:uniprot:Q04206;urn:miriam:uniprot:Q04206"
      hgnc "HGNC_SYMBOL:NFKB1;HGNC_SYMBOL:NFKB2;HGNC_SYMBOL:RELA"
      map_id "M123_10"
      name "Nf_minus_KB_space_Complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa36"
      uniprot "UNIPROT:Q00653;UNIPROT:P19838;UNIPROT:Q04206"
    ]
    graphics [
      x 1792.5144998161113
      y 1853.4279058676175
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17245"
      hgnc "NA"
      map_id "M123_219"
      name "CO"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa362"
      uniprot "NA"
    ]
    graphics [
      x 1717.9673850663708
      y 1589.740177398237
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_219"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:ncbigene:4348"
      hgnc "NA"
      map_id "M123_247"
      name "iNOS"
      node_subtype "RNA"
      node_type "species"
      org_id "sa59"
      uniprot "NA"
    ]
    graphics [
      x 2030.0730886186202
      y 2517.07025542237
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_247"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_40"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re25"
      uniprot "NA"
    ]
    graphics [
      x 880.1566501106137
      y 2355.3540088806026
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:hgnc.symbol:NOS2;urn:miriam:uniprot:P35228;urn:miriam:ncbigene:4843;urn:miriam:ec-code:1.14.13.39"
      hgnc "HGNC_SYMBOL:NOS2"
      map_id "M123_248"
      name "iNOS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa60"
      uniprot "UNIPROT:P35228"
    ]
    graphics [
      x 832.5144998161113
      y 1921.5045073277029
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_248"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      annotation "PUBMED:15249210;PUBMED:11463332"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_39"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re24"
      uniprot "NA"
    ]
    graphics [
      x 1486.6057727547209
      y 1002.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16467"
      hgnc "NA"
      map_id "M123_227"
      name "L_minus_Arginine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa378"
      uniprot "NA"
    ]
    graphics [
      x 1207.9673850663708
      y 1599.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_227"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16474"
      hgnc "NA"
      map_id "M123_228"
      name "NADPH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa379"
      uniprot "NA"
    ]
    graphics [
      x 1031.4760702265748
      y 1352.3785906698167
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_228"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15379"
      hgnc "NA"
      map_id "M123_233"
      name "O2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa383"
      uniprot "NA"
    ]
    graphics [
      x 2471.476070226575
      y 1145.925356895868
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_233"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16349"
      hgnc "NA"
      map_id "M123_232"
      name "Citrulline"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa382"
      uniprot "NA"
    ]
    graphics [
      x 1631.4760702265748
      y 1277.5725709100786
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_232"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18009"
      hgnc "NA"
      map_id "M123_230"
      name "NADP_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa380"
      uniprot "NA"
    ]
    graphics [
      x 2018.8536650413898
      y 794.5882051510855
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_230"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "M123_231"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa381"
      uniprot "NA"
    ]
    graphics [
      x 1088.8536650413898
      y 1014.9632755122113
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_231"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_20"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re106"
      uniprot "NA"
    ]
    graphics [
      x 1912.5144998161113
      y 2080.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      annotation "PUBMED:15249210"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_22"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re109"
      uniprot "NA"
    ]
    graphics [
      x 2021.4760702265748
      y 1485.3143546505783
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:uniprot:Q00653;urn:miriam:uniprot:P19838;urn:miriam:uniprot:Q04206;urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998;urn:miriam:hgnc:7795;urn:miriam:refseq:NM_001077494;urn:miriam:hgnc.symbol:NFKB2;urn:miriam:ncbigene:4791;urn:miriam:hgnc.symbol:NFKB2;urn:miriam:ncbigene:4791;urn:miriam:ensembl:ENSG00000077150;urn:miriam:uniprot:Q00653;urn:miriam:uniprot:Q00653;urn:miriam:hgnc:9955;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ncbigene:5970;urn:miriam:ncbigene:5970;urn:miriam:refseq:NM_021975;urn:miriam:hgnc.symbol:RELA;urn:miriam:hgnc.symbol:RELA;urn:miriam:uniprot:Q04206;urn:miriam:uniprot:Q04206"
      hgnc "HGNC_SYMBOL:NFKB1;HGNC_SYMBOL:NFKB2;HGNC_SYMBOL:RELA"
      map_id "M123_11"
      name "Nf_minus_KB_space_Complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa38"
      uniprot "UNIPROT:Q00653;UNIPROT:P19838;UNIPROT:Q04206"
    ]
    graphics [
      x 1571.4760702265748
      y 1182.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15379"
      hgnc "NA"
      map_id "M123_216"
      name "O2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa358"
      uniprot "NA"
    ]
    graphics [
      x 2482.5144998161113
      y 1813.3552697885598
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_216"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16474"
      hgnc "NA"
      map_id "M123_214"
      name "NADPH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa356"
      uniprot "NA"
    ]
    graphics [
      x 1792.5935627164836
      y 2274.6594484469133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_214"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000100292;urn:miriam:hgnc:5013;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:refseq:NM_002133;urn:miriam:ncbigene:3162;urn:miriam:uniprot:P09601;urn:miriam:uniprot:P09601;urn:miriam:ncbigene:3162;urn:miriam:ec-code:1.14.14.18"
      hgnc "HGNC_SYMBOL:HMOX1"
      map_id "M123_210"
      name "HMOX1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa352"
      uniprot "UNIPROT:P09601"
    ]
    graphics [
      x 1751.4760702265748
      y 1405.288563922968
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_210"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17033"
      hgnc "NA"
      map_id "M123_218"
      name "Biliverdin"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa361"
      uniprot "NA"
    ]
    graphics [
      x 1180.4146427238684
      y 2169.0166269831952
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_218"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "M123_211"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa353"
      uniprot "NA"
    ]
    graphics [
      x 1192.5144998161113
      y 1989.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_211"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29033"
      hgnc "NA"
      map_id "M123_215"
      name "Fe2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa357"
      uniprot "NA"
    ]
    graphics [
      x 1882.5144998161113
      y 1870.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_215"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18009"
      hgnc "NA"
      map_id "M123_212"
      name "NADP_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa354"
      uniprot "NA"
    ]
    graphics [
      x 2200.4146427238684
      y 2184.832357576839
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_212"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_21"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re107"
      uniprot "NA"
    ]
    graphics [
      x 2228.85366504139
      y 979.1996183844692
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000100292;urn:miriam:hgnc:5013;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:refseq:NM_002133;urn:miriam:ncbigene:3162;urn:miriam:uniprot:P09601;urn:miriam:uniprot:P09601;urn:miriam:ncbigene:3162;urn:miriam:ec-code:1.14.14.18"
      hgnc "HGNC_SYMBOL:HMOX1"
      map_id "M123_209"
      name "HMOX1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa351"
      uniprot "UNIPROT:P09601"
    ]
    graphics [
      x 2411.476070226575
      y 1398.0883274205173
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_209"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:uniprot:P0DTC3;urn:miriam:ncbigene:43740569;urn:miriam:ncbiprotein:BCD58754"
      hgnc "NA"
      map_id "M123_208"
      name "Orf3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa350"
      uniprot "UNIPROT:P0DTC3"
    ]
    graphics [
      x 1241.4760702265748
      y 1152.7306625718877
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_208"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_19"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re104"
      uniprot "NA"
    ]
    graphics [
      x 2587.9673850663708
      y 1699.3414961842766
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000100292;urn:miriam:hgnc:5013;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:refseq:NM_002133;urn:miriam:ncbigene:3162;urn:miriam:uniprot:P09601"
      hgnc "HGNC_SYMBOL:HMOX1"
      map_id "M123_206"
      name "HMOX1"
      node_subtype "RNA"
      node_type "species"
      org_id "sa341"
      uniprot "UNIPROT:P09601"
    ]
    graphics [
      x 1721.4760702265748
      y 1409.0814698311804
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_206"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000100292;urn:miriam:hgnc:5013;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:refseq:NM_002133;urn:miriam:ncbigene:3162;urn:miriam:uniprot:P09601"
      hgnc "HGNC_SYMBOL:HMOX1"
      map_id "M123_205"
      name "HMOX1"
      node_subtype "GENE"
      node_type "species"
      org_id "sa340"
      uniprot "UNIPROT:P09601"
    ]
    graphics [
      x 1430.5824814748505
      y 2772.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_205"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0035744"
      hgnc "NA"
      map_id "M123_223"
      name "Th1_space_cell_space_cytokine_space_production"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa374"
      uniprot "NA"
    ]
    graphics [
      x 1660.4146427238684
      y 2262.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_223"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29108"
      hgnc "NA"
      map_id "M123_222"
      name "CA"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa373"
      uniprot "NA"
    ]
    graphics [
      x 1612.5144998161113
      y 1902.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_222"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      annotation "PUBMED:17476692"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_29"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re117"
      uniprot "NA"
    ]
    graphics [
      x 1868.8536650413898
      y 932.6254878021133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0070231"
      hgnc "NA"
      map_id "M123_224"
      name "T_minus_cell_space_apoptosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa375"
      uniprot "NA"
    ]
    graphics [
      x 2198.85366504139
      y 924.1134000477189
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_224"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "M123_198"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa33"
      uniprot "NA"
    ]
    graphics [
      x 1960.4146427238684
      y 2149.6902755837036
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_198"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:uniprot:Q16719;urn:miriam:hgnc:6469;urn:miriam:ensembl:ENSG00000115919;urn:miriam:ncbigene:8942;urn:miriam:refseq:NM_001032998;urn:miriam:ncbigene:8942;urn:miriam:hgnc.symbol:KYNU;urn:miriam:hgnc.symbol:KYNU;urn:miriam:ec-code:3.7.1.3"
      hgnc "HGNC_SYMBOL:KYNU"
      map_id "M123_204"
      name "KYNU"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa34"
      uniprot "UNIPROT:Q16719"
    ]
    graphics [
      x 1114.973418093434
      y 2622.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_204"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A32544"
      hgnc "NA"
      map_id "M123_95"
      name "NCA"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa189"
      uniprot "NA"
    ]
    graphics [
      x 1330.4146427238684
      y 2352.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17111"
      hgnc "NA"
      map_id "M123_93"
      name "PRPP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa187"
      uniprot "NA"
    ]
    graphics [
      x 1642.411307227782
      y 2575.6637543524857
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "M123_97"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa190"
      uniprot "NA"
    ]
    graphics [
      x 1630.4146427238684
      y 2262.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:ec-code:6.3.4.21;urn:miriam:uniprot:Q6XQN6;urn:miriam:ncbigene:93100;urn:miriam:hgnc.symbol:NAPRT"
      hgnc "HGNC_SYMBOL:NAPRT"
      map_id "M123_94"
      name "NAPRT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa188"
      uniprot "UNIPROT:Q6XQN6"
    ]
    graphics [
      x 1405.2508552528402
      y 2592.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18420"
      hgnc "NA"
      map_id "M123_98"
      name "Mg2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa191"
      uniprot "NA"
    ]
    graphics [
      x 2258.3585713076195
      y 2577.2215464613982
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29888"
      hgnc "NA"
      map_id "M123_99"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa192"
      uniprot "NA"
    ]
    graphics [
      x 1571.4760702265748
      y 1242.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_70"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re89"
      uniprot "NA"
    ]
    graphics [
      x 1930.4146427238684
      y 2140.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A32544"
      hgnc "NA"
      map_id "M123_172"
      name "NCA"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa271"
      uniprot "NA"
    ]
    graphics [
      x 1806.1225433048294
      y 2603.1930220621825
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_172"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:hgnc.symbol:SLC22A13;urn:miriam:refseq:NM_004256;urn:miriam:hgnc.symbol:SLC22A13;urn:miriam:ensembl:ENSG00000172940;urn:miriam:uniprot:Q9Y226;urn:miriam:hgnc:8494;urn:miriam:ncbigene:9390;urn:miriam:ncbigene:9390"
      hgnc "HGNC_SYMBOL:SLC22A13"
      map_id "M123_169"
      name "SLC22A13"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa268"
      uniprot "UNIPROT:Q9Y226"
    ]
    graphics [
      x 1850.0074341427699
      y 2380.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_169"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A58527"
      hgnc "NA"
      map_id "M123_132"
      name "NAR"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa224"
      uniprot "NA"
    ]
    graphics [
      x 820.4146427238683
      y 2113.853912881765
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30616"
      hgnc "NA"
      map_id "M123_125"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa218"
      uniprot "NA"
    ]
    graphics [
      x 1747.9673850663708
      y 1615.9277406352314
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_125"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:hgnc:17871;urn:miriam:ec-code:2.7.1.173;urn:miriam:refseq:NM_014446;urn:miriam:ensembl:ENSG00000077009;urn:miriam:ec-code:2.7.1.22;urn:miriam:ncbigene:27231;urn:miriam:ncbigene:27231;urn:miriam:uniprot:Q9NPI5;urn:miriam:refseq:NM_170678;urn:miriam:hgnc.symbol:NMRK2;urn:miriam:hgnc.symbol:NMRK2"
      hgnc "HGNC_SYMBOL:NMRK2"
      map_id "M123_122"
      name "NMRK2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa215"
      uniprot "UNIPROT:Q9NPI5"
    ]
    graphics [
      x 1630.4146427238684
      y 2172.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "M123_126"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa219"
      uniprot "NA"
    ]
    graphics [
      x 449.6992862145157
      y 2182.5960704039285
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 167
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A456216"
      hgnc "NA"
      map_id "M123_133"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa226"
      uniprot "NA"
    ]
    graphics [
      x 161.47607022657485
      y 1391.0281475497238
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_133"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 168
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15414"
      hgnc "NA"
      map_id "M123_139"
      name "AdoMet"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa232"
      uniprot "NA"
    ]
    graphics [
      x 1642.5144998161113
      y 1872.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_139"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 169
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000166741;urn:miriam:ec-code:2.1.1.1;urn:miriam:hgnc:7861;urn:miriam:refseq:NM_006169;urn:miriam:ncbigene:4837;urn:miriam:ncbigene:4837;urn:miriam:hgnc.symbol:NNMT;urn:miriam:hgnc.symbol:NNMT;urn:miriam:uniprot:P40261"
      hgnc "HGNC_SYMBOL:NNMT"
      map_id "M123_121"
      name "NNMT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa214"
      uniprot "UNIPROT:P40261"
    ]
    graphics [
      x 2362.5144998161113
      y 1948.1103888903936
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 170
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16797"
      hgnc "NA"
      map_id "M123_128"
      name "MNA"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa220"
      uniprot "NA"
    ]
    graphics [
      x 1402.5144998161113
      y 2112.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 171
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16680"
      hgnc "NA"
      map_id "M123_138"
      name "AdoHcy"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa231"
      uniprot "NA"
    ]
    graphics [
      x 1611.6104592494069
      y 1722.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_138"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 172
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_15"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re100"
      uniprot "NA"
    ]
    graphics [
      x 955.9109426104355
      y 2250.416026433729
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 173
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A27647"
      hgnc "NA"
      map_id "M123_195"
      name "PGG2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa321"
      uniprot "NA"
    ]
    graphics [
      x 1671.6104592494069
      y 1769.0814698311804
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_195"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 174
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A10545"
      hgnc "NA"
      map_id "M123_194"
      name "e_minus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa320"
      uniprot "NA"
    ]
    graphics [
      x 721.8044040920078
      y 2601.3671891029867
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_194"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 175
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "M123_192"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa319"
      uniprot "NA"
    ]
    graphics [
      x 700.4146427238683
      y 2243.075857668091
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_192"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 176
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000073756;urn:miriam:hgnc:9605;urn:miriam:hgnc.symbol:PTGS2;urn:miriam:hgnc.symbol:PTGS2;urn:miriam:uniprot:P35354;urn:miriam:refseq:NM_000963;urn:miriam:ncbigene:5743;urn:miriam:ncbigene:5743;urn:miriam:ec-code:1.14.99.1"
      hgnc "HGNC_SYMBOL:PTGS2"
      map_id "M123_188"
      name "PTGS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa315"
      uniprot "UNIPROT:P35354"
    ]
    graphics [
      x 850.4146427238683
      y 2165.6390909233837
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_188"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 177
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:uniprot:P35354;urn:miriam:obo.chebi:CHEBI%3A41423;urn:miriam:ensembl:ENSG00000073756;urn:miriam:hgnc:9605;urn:miriam:hgnc.symbol:PTGS2;urn:miriam:hgnc.symbol:PTGS2;urn:miriam:uniprot:P35354;urn:miriam:refseq:NM_000963;urn:miriam:ncbigene:5743;urn:miriam:ncbigene:5743;urn:miriam:ec-code:1.14.99.1"
      hgnc "HGNC_SYMBOL:PTGS2"
      map_id "M123_8"
      name "PTCS2:celecoxib"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa34"
      uniprot "UNIPROT:P35354"
    ]
    graphics [
      x 1361.4760702265748
      y 1463.8937412551359
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 178
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15554"
      hgnc "NA"
      map_id "M123_190"
      name "PGH2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa317"
      uniprot "NA"
    ]
    graphics [
      x 1391.4760702265748
      y 1433.8937412551359
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_190"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 179
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "M123_191"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa318"
      uniprot "NA"
    ]
    graphics [
      x 1610.0074341427699
      y 2412.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_191"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 180
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_16"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re101"
      uniprot "NA"
    ]
    graphics [
      x 1646.090149807047
      y 402.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 181
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:ec-code:5.3.99.4;urn:miriam:ec-code:4.2.1.152;urn:miriam:ensembl:ENSG00000124212;urn:miriam:hgnc:9603;urn:miriam:ncbigene:5740;urn:miriam:ncbigene:5740;urn:miriam:refseq:NM_000961;urn:miriam:hgnc.symbol:PTGIS;urn:miriam:hgnc.symbol:PTGIS;urn:miriam:uniprot:Q16647"
      hgnc "HGNC_SYMBOL:PTGIS"
      map_id "M123_203"
      name "PTGIS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa339"
      uniprot "UNIPROT:Q16647"
    ]
    graphics [
      x 1267.2756173319829
      y 252.06502830998443
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_203"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 182
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15552"
      hgnc "NA"
      map_id "M123_189"
      name "PGI2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa316"
      uniprot "NA"
    ]
    graphics [
      x 1742.7823693789387
      y 560.335832891702
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_189"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 183
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30616"
      hgnc "NA"
      map_id "M123_134"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa227"
      uniprot "NA"
    ]
    graphics [
      x 1550.0074341427699
      y 2352.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 184
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29105;urn:miriam:uniprot:O95544;urn:miriam:hgnc:29831;urn:miriam:ncbigene:65220;urn:miriam:refseq:NM_023018;urn:miriam:ncbigene:65220;urn:miriam:ec-code:2.7.1.23;urn:miriam:uniprot:O95544;urn:miriam:uniprot:O95544;urn:miriam:hgnc.symbol:NADK;urn:miriam:hgnc.symbol:NADK;urn:miriam:ensembl:ENSG00000008130;urn:miriam:obo.chebi:CHEBI%3A29105"
      hgnc "HGNC_SYMBOL:NADK"
      map_id "M123_3"
      name "NADK:Zn2_plus__space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa17"
      uniprot "UNIPROT:O95544"
    ]
    graphics [
      x 1640.0074341427699
      y 2292.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 185
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18009"
      hgnc "NA"
      map_id "M123_149"
      name "NADP_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa241"
      uniprot "NA"
    ]
    graphics [
      x 1400.5824814748505
      y 2742.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_149"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 186
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A456216"
      hgnc "NA"
      map_id "M123_148"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa240"
      uniprot "NA"
    ]
    graphics [
      x 651.2226069096338
      y 2352.5634880180496
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_148"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 187
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30616"
      hgnc "NA"
      map_id "M123_111"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa204"
      uniprot "NA"
    ]
    graphics [
      x 543.5925437269425
      y 2252.762846717169
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 188
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A58359"
      hgnc "NA"
      map_id "M123_103"
      name "L_minus_Gln"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa197"
      uniprot "NA"
    ]
    graphics [
      x 730.4146427238683
      y 2151.7978200699217
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 189
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "M123_112"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa205"
      uniprot "NA"
    ]
    graphics [
      x 517.9673850663706
      y 1655.261006004971
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 190
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:ncbigene:55191;urn:miriam:ncbigene:55191;urn:miriam:hgnc.symbol:NADSYN1;urn:miriam:hgnc.symbol:NADSYN1;urn:miriam:ec-code:6.3.5.1;urn:miriam:uniprot:Q6IA69;urn:miriam:hgnc:29832;urn:miriam:ensembl:ENSG00000172890;urn:miriam:refseq:NM_018161"
      hgnc "HGNC_SYMBOL:NADSYN1"
      map_id "M123_115"
      name "NADSYN1_space_hexamer"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa208"
      uniprot "UNIPROT:Q6IA69"
    ]
    graphics [
      x 852.1217595630603
      y 2545.472292498066
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 191
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29985"
      hgnc "NA"
      map_id "M123_120"
      name "L_minus_Glu"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa212"
      uniprot "NA"
    ]
    graphics [
      x 611.3286445212872
      y 2453.3892365229017
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 192
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16027"
      hgnc "NA"
      map_id "M123_123"
      name "AMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa216"
      uniprot "NA"
    ]
    graphics [
      x 471.61045924940686
      y 1859.5430905254466
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 193
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29888"
      hgnc "NA"
      map_id "M123_119"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa211"
      uniprot "NA"
    ]
    graphics [
      x 1525.2508552528402
      y 2562.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 194
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:hgnc:17871;urn:miriam:ec-code:2.7.1.173;urn:miriam:refseq:NM_014446;urn:miriam:ensembl:ENSG00000077009;urn:miriam:ec-code:2.7.1.22;urn:miriam:ncbigene:27231;urn:miriam:ncbigene:27231;urn:miriam:uniprot:Q9NPI5;urn:miriam:refseq:NM_170678;urn:miriam:hgnc.symbol:NMRK2;urn:miriam:hgnc.symbol:NMRK2"
      hgnc "HGNC_SYMBOL:NMRK2"
      map_id "M123_144"
      name "NMRK2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa237"
      uniprot "UNIPROT:Q9NPI5"
    ]
    graphics [
      x 1210.4146427238684
      y 2202.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_144"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 195
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_145"
      target_id "M123_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 1
    target 3
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_145"
      target_id "M123_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 4
    target 2
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_146"
      target_id "M123_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 194
    target 2
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_144"
      target_id "M123_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 2
    target 6
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_57"
      target_id "M123_131"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 2
    target 7
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_57"
      target_id "M123_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 2
    target 8
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_57"
      target_id "M123_142"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 4
    target 3
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_146"
      target_id "M123_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 5
    target 3
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_143"
      target_id "M123_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 3
    target 6
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_55"
      target_id "M123_131"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 3
    target 7
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_55"
      target_id "M123_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 3
    target 8
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_55"
      target_id "M123_142"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 5
    target 27
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_143"
      target_id "M123_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 6
    target 9
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_131"
      target_id "M123_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 10
    target 9
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_136"
      target_id "M123_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 11
    target 9
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_2"
      target_id "M123_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 9
    target 12
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_54"
      target_id "M123_130"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 9
    target 13
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_54"
      target_id "M123_135"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 11
    target 31
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_2"
      target_id "M123_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 14
    target 12
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_53"
      target_id "M123_130"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 12
    target 15
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_130"
      target_id "M123_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 12
    target 16
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_130"
      target_id "M123_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 32
    target 14
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_107"
      target_id "M123_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 187
    target 14
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_111"
      target_id "M123_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 188
    target 14
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_103"
      target_id "M123_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 189
    target 14
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_112"
      target_id "M123_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 190
    target 14
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_115"
      target_id "M123_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 14
    target 191
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_53"
      target_id "M123_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 14
    target 192
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_53"
      target_id "M123_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 14
    target 193
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_53"
      target_id "M123_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 183
    target 15
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_134"
      target_id "M123_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 184
    target 15
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_3"
      target_id "M123_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 15
    target 185
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_63"
      target_id "M123_149"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 15
    target 186
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_63"
      target_id "M123_148"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 17
    target 16
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_140"
      target_id "M123_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 18
    target 16
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_116"
      target_id "M123_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 16
    target 19
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_59"
      target_id "M123_129"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 16
    target 20
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_59"
      target_id "M123_141"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 19
    target 21
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_129"
      target_id "M123_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 19
    target 22
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_129"
      target_id "M123_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 168
    target 21
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_139"
      target_id "M123_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 169
    target 21
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_121"
      target_id "M123_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 21
    target 170
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_61"
      target_id "M123_128"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 21
    target 171
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_61"
      target_id "M123_138"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 239
    source 23
    target 22
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_118"
      target_id "M123_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 240
    source 24
    target 22
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_113"
      target_id "M123_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 241
    source 22
    target 25
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_60"
      target_id "M123_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 242
    source 22
    target 26
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_60"
      target_id "M123_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 243
    source 27
    target 26
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_51"
      target_id "M123_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 244
    source 28
    target 26
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_52"
      target_id "M123_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 245
    source 29
    target 26
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_58"
      target_id "M123_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 246
    source 30
    target 26
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_62"
      target_id "M123_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 247
    source 26
    target 31
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_104"
      target_id "M123_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 248
    source 163
    target 27
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_132"
      target_id "M123_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 249
    source 164
    target 27
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_125"
      target_id "M123_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 250
    source 27
    target 166
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_51"
      target_id "M123_126"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 251
    source 27
    target 167
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_51"
      target_id "M123_133"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 252
    source 163
    target 28
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_132"
      target_id "M123_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 253
    source 164
    target 28
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_125"
      target_id "M123_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 254
    source 165
    target 28
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_122"
      target_id "M123_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 255
    source 28
    target 166
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_52"
      target_id "M123_126"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 256
    source 28
    target 167
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_52"
      target_id "M123_133"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 257
    source 154
    target 29
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_95"
      target_id "M123_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 258
    source 155
    target 29
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_93"
      target_id "M123_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 259
    source 156
    target 29
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_97"
      target_id "M123_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 260
    source 157
    target 29
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_94"
      target_id "M123_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 261
    source 158
    target 29
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M123_98"
      target_id "M123_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 262
    source 29
    target 159
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_58"
      target_id "M123_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 263
    source 34
    target 30
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_237"
      target_id "M123_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 264
    source 35
    target 30
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_109"
      target_id "M123_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 265
    source 36
    target 30
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_102"
      target_id "M123_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 266
    source 37
    target 30
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_114"
      target_id "M123_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 267
    source 30
    target 38
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_62"
      target_id "M123_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 268
    source 30
    target 39
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_62"
      target_id "M123_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 269
    source 31
    target 32
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_50"
      target_id "M123_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 270
    source 31
    target 33
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_50"
      target_id "M123_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 271
    source 40
    target 34
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_32"
      target_id "M123_237"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 272
    source 41
    target 40
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_229"
      target_id "M123_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 273
    source 40
    target 42
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_32"
      target_id "M123_217"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 274
    source 43
    target 41
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_14"
      target_id "M123_229"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 275
    source 41
    target 44
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_229"
      target_id "M123_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 276
    source 54
    target 43
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_187"
      target_id "M123_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 277
    source 55
    target 43
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_234"
      target_id "M123_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 278
    source 56
    target 43
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_186"
      target_id "M123_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 45
    target 44
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_220"
      target_id "M123_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 44
    target 46
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_23"
      target_id "M123_242"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 44
    target 47
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_23"
      target_id "M123_236"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 46
    target 48
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_242"
      target_id "M123_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 49
    target 48
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_241"
      target_id "M123_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 50
    target 48
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_240"
      target_id "M123_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 48
    target 51
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_31"
      target_id "M123_243"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 48
    target 52
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_31"
      target_id "M123_239"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 48
    target 53
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_31"
      target_id "M123_238"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 57
    target 54
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_71"
      target_id "M123_187"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 54
    target 58
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_187"
      target_id "M123_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 54
    target 59
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_187"
      target_id "M123_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 54
    target 60
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_187"
      target_id "M123_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 54
    target 61
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_187"
      target_id "M123_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 54
    target 62
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_187"
      target_id "M123_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 54
    target 63
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_187"
      target_id "M123_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 64
    target 57
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_193"
      target_id "M123_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 152
    target 57
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_198"
      target_id "M123_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 153
    target 57
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_204"
      target_id "M123_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 57
    target 84
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_71"
      target_id "M123_180"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 64
    target 58
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_193"
      target_id "M123_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 67
    target 58
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_91"
      target_id "M123_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 58
    target 151
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_28"
      target_id "M123_224"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 59
    target 149
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_26"
      target_id "M123_222"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 60
    target 81
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_35"
      target_id "M123_207"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 304
    source 61
    target 148
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_27"
      target_id "M123_223"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 305
    source 147
    target 62
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_205"
      target_id "M123_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 306
    source 117
    target 62
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_249"
      target_id "M123_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 307
    source 116
    target 62
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_250"
      target_id "M123_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 308
    source 62
    target 146
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_18"
      target_id "M123_206"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 309
    source 64
    target 63
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_193"
      target_id "M123_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 310
    source 63
    target 65
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_30"
      target_id "M123_225"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 311
    source 66
    target 64
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_64"
      target_id "M123_193"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 312
    source 67
    target 66
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_91"
      target_id "M123_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 313
    source 68
    target 66
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_127"
      target_id "M123_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 314
    source 69
    target 66
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_147"
      target_id "M123_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 315
    source 70
    target 66
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_151"
      target_id "M123_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 316
    source 71
    target 66
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_226"
      target_id "M123_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 317
    source 66
    target 72
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_64"
      target_id "M123_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 318
    source 66
    target 73
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_64"
      target_id "M123_137"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 319
    source 74
    target 67
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_44"
      target_id "M123_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 320
    source 67
    target 75
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_91"
      target_id "M123_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 321
    source 67
    target 76
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_91"
      target_id "M123_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 322
    source 67
    target 77
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_91"
      target_id "M123_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 323
    source 93
    target 74
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_85"
      target_id "M123_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 324
    source 94
    target 74
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_75"
      target_id "M123_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 325
    source 95
    target 74
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_92"
      target_id "M123_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 326
    source 74
    target 96
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_44"
      target_id "M123_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 327
    source 82
    target 75
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_106"
      target_id "M123_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 328
    source 85
    target 75
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_1"
      target_id "M123_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 329
    source 86
    target 75
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "INHIBITION"
      source_id "M123_87"
      target_id "M123_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 330
    source 87
    target 75
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "INHIBITION"
      source_id "M123_90"
      target_id "M123_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 331
    source 88
    target 75
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "INHIBITION"
      source_id "M123_88"
      target_id "M123_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 332
    source 75
    target 89
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_48"
      target_id "M123_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 333
    source 75
    target 84
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_48"
      target_id "M123_180"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 334
    source 82
    target 76
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_106"
      target_id "M123_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 335
    source 83
    target 76
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_96"
      target_id "M123_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 336
    source 76
    target 84
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_49"
      target_id "M123_180"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 337
    source 78
    target 77
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_162"
      target_id "M123_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 338
    source 77
    target 79
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_33"
      target_id "M123_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 339
    source 79
    target 80
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_4"
      target_id "M123_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 340
    source 80
    target 81
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_36"
      target_id "M123_207"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 341
    source 89
    target 90
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_86"
      target_id "M123_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 342
    source 90
    target 91
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_56"
      target_id "M123_173"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 343
    source 90
    target 92
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_56"
      target_id "M123_171"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 344
    source 97
    target 93
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_42"
      target_id "M123_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 345
    source 98
    target 93
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_34"
      target_id "M123_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 346
    source 99
    target 97
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_74"
      target_id "M123_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 347
    source 100
    target 97
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_235"
      target_id "M123_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 348
    source 107
    target 97
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_12"
      target_id "M123_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 349
    source 108
    target 97
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "INHIBITION"
      source_id "M123_185"
      target_id "M123_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 350
    source 99
    target 98
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_74"
      target_id "M123_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 351
    source 100
    target 98
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_235"
      target_id "M123_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 352
    source 101
    target 98
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_252"
      target_id "M123_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 353
    source 102
    target 99
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_13"
      target_id "M123_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 354
    source 103
    target 102
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_105"
      target_id "M123_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 355
    source 104
    target 102
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_253"
      target_id "M123_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 356
    source 103
    target 105
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_105"
      target_id "M123_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 357
    source 105
    target 106
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_25"
      target_id "M123_221"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 358
    source 109
    target 107
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_24"
      target_id "M123_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 359
    source 110
    target 109
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_213"
      target_id "M123_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 360
    source 111
    target 109
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_244"
      target_id "M123_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 361
    source 110
    target 132
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_213"
      target_id "M123_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 362
    source 112
    target 111
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_41"
      target_id "M123_244"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 363
    source 113
    target 112
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_251"
      target_id "M123_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 364
    source 114
    target 113
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_37"
      target_id "M123_251"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 365
    source 115
    target 114
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_246"
      target_id "M123_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 366
    source 116
    target 114
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "INHIBITION"
      source_id "M123_250"
      target_id "M123_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 367
    source 117
    target 114
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_249"
      target_id "M123_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 368
    source 125
    target 116
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_39"
      target_id "M123_250"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 369
    source 117
    target 118
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_249"
      target_id "M123_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 370
    source 119
    target 118
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_245"
      target_id "M123_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 371
    source 120
    target 118
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_10"
      target_id "M123_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 372
    source 121
    target 118
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "INHIBITION"
      source_id "M123_219"
      target_id "M123_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 373
    source 118
    target 122
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_38"
      target_id "M123_247"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 374
    source 133
    target 120
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_22"
      target_id "M123_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 375
    source 132
    target 121
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_20"
      target_id "M123_219"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 376
    source 121
    target 133
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "INHIBITION"
      source_id "M123_219"
      target_id "M123_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 377
    source 122
    target 123
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_247"
      target_id "M123_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 378
    source 123
    target 124
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_40"
      target_id "M123_248"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 379
    source 124
    target 125
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_248"
      target_id "M123_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 380
    source 126
    target 125
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_227"
      target_id "M123_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 381
    source 127
    target 125
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_228"
      target_id "M123_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 382
    source 128
    target 125
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_233"
      target_id "M123_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 383
    source 125
    target 129
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_39"
      target_id "M123_232"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 384
    source 125
    target 130
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_39"
      target_id "M123_230"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 385
    source 125
    target 131
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_39"
      target_id "M123_231"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 386
    source 135
    target 132
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_216"
      target_id "M123_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 387
    source 136
    target 132
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_214"
      target_id "M123_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 388
    source 137
    target 132
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_210"
      target_id "M123_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 389
    source 132
    target 138
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_20"
      target_id "M123_218"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 390
    source 132
    target 139
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_20"
      target_id "M123_211"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 391
    source 132
    target 140
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_20"
      target_id "M123_215"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 392
    source 132
    target 141
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_20"
      target_id "M123_212"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 393
    source 134
    target 133
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_11"
      target_id "M123_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 394
    source 142
    target 137
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_21"
      target_id "M123_210"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 395
    source 143
    target 142
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_209"
      target_id "M123_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 396
    source 144
    target 142
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "MODULATION"
      source_id "M123_208"
      target_id "M123_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 397
    source 145
    target 143
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_19"
      target_id "M123_209"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 398
    source 146
    target 145
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_206"
      target_id "M123_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 399
    source 149
    target 150
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_222"
      target_id "M123_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 400
    source 150
    target 151
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_29"
      target_id "M123_224"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 401
    source 160
    target 154
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_70"
      target_id "M123_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 402
    source 161
    target 160
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_172"
      target_id "M123_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 403
    source 162
    target 160
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_169"
      target_id "M123_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 404
    source 170
    target 172
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M123_128"
      target_id "M123_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 405
    source 173
    target 172
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_195"
      target_id "M123_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 406
    source 174
    target 172
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_194"
      target_id "M123_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 407
    source 175
    target 172
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_192"
      target_id "M123_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 408
    source 176
    target 172
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_188"
      target_id "M123_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 409
    source 177
    target 172
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "INHIBITION"
      source_id "M123_8"
      target_id "M123_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 410
    source 172
    target 178
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_15"
      target_id "M123_190"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 411
    source 172
    target 179
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_15"
      target_id "M123_191"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 412
    source 178
    target 180
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_190"
      target_id "M123_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 413
    source 181
    target 180
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_203"
      target_id "M123_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 414
    source 180
    target 182
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_16"
      target_id "M123_189"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
