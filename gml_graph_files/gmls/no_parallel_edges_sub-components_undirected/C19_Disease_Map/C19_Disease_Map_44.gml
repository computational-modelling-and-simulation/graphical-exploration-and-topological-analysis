# generated with VANTED V2.8.2 at Fri Mar 04 10:04:38 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57540"
      hgnc "NA"
      map_id "M123_84"
      name "NAD_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa108"
      uniprot "NA"
    ]
    graphics [
      x 2318.85366504139
      y 964.6214271563429
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_46"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re41"
      uniprot "NA"
    ]
    graphics [
      x 1868.8536650413898
      y 860.9451979825509
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30616"
      hgnc "NA"
      map_id "M123_83"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa107"
      uniprot "NA"
    ]
    graphics [
      x 968.8536650413897
      y 711.257728501933
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:ncbigene:133686;urn:miriam:ncbigene:133686;urn:miriam:refseq:NM_153013;urn:miriam:ensembl:ENSG00000152620;urn:miriam:hgnc:26404;urn:miriam:hgnc.symbol:NADK2;urn:miriam:hgnc.symbol:NADK2;urn:miriam:ec-code:2.7.1.23;urn:miriam:uniprot:Q4G0N4"
      hgnc "HGNC_SYMBOL:NADK2"
      map_id "M123_196"
      name "NADK2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa324"
      uniprot "UNIPROT:Q4G0N4"
    ]
    graphics [
      x 1151.4760702265748
      y 1428.5455333831699
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_196"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18009"
      hgnc "NA"
      map_id "M123_80"
      name "NADP_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa104"
      uniprot "NA"
    ]
    graphics [
      x 1568.8536650413898
      y 702.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A456216"
      hgnc "NA"
      map_id "M123_259"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa99"
      uniprot "NA"
    ]
    graphics [
      x 2241.6104592494066
      y 1802.2571790533568
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_259"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "M123_258"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa98"
      uniprot "NA"
    ]
    graphics [
      x 2527.9673850663708
      y 1760.2858415881262
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_258"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re40"
      uniprot "NA"
    ]
    graphics [
      x 2375.518478280632
      y 2108.221696520471
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A64084"
      hgnc "NA"
      map_id "M123_77"
      name "S_minus_NADPHX"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa101"
      uniprot "NA"
    ]
    graphics [
      x 1898.8536650413898
      y 804.1331209245378
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30616"
      hgnc "NA"
      map_id "M123_79"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa103"
      uniprot "NA"
    ]
    graphics [
      x 1910.0074341427699
      y 2380.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:hgnc.symbol:NAXD;urn:miriam:uniprot:Q8IW45;urn:miriam:ec-code:4.2.1.93;urn:miriam:ncbigene:55739"
      hgnc "HGNC_SYMBOL:NAXD"
      map_id "M123_82"
      name "CARKD"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa106"
      uniprot "UNIPROT:Q8IW45"
    ]
    graphics [
      x 1335.5674546932569
      y 2052.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16474"
      hgnc "NA"
      map_id "M123_76"
      name "NADPH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa100"
      uniprot "NA"
    ]
    graphics [
      x 1612.5144998161113
      y 1872.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "M123_81"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa105"
      uniprot "NA"
    ]
    graphics [
      x 1969.8548263367193
      y 2455.8999114957323
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M123_43"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re39"
      uniprot "NA"
    ]
    graphics [
      x 2288.85366504139
      y 732.5883256062364
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A64085"
      hgnc "NA"
      map_id "M123_78"
      name "R_minus_NADPHX"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa102"
      uniprot "NA"
    ]
    graphics [
      x 2227.9673850663708
      y 1545.6982431349254
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Kynurenine synthesis pathway"
      full_annotation "urn:miriam:ncbigene:65220;urn:miriam:ec-code:2.7.1.23;urn:miriam:uniprot:O95544;urn:miriam:hgnc.symbol:NADK"
      hgnc "HGNC_SYMBOL:NADK"
      map_id "M123_197"
      name "APOA1BP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa329"
      uniprot "UNIPROT:O95544"
    ]
    graphics [
      x 2171.476070226575
      y 1518.7342851305455
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M123_197"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 17
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_84"
      target_id "M123_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 18
    source 3
    target 2
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_83"
      target_id "M123_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 19
    source 4
    target 2
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_196"
      target_id "M123_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 20
    source 2
    target 5
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_46"
      target_id "M123_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 21
    source 2
    target 6
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_46"
      target_id "M123_259"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 22
    source 2
    target 7
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_46"
      target_id "M123_258"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 23
    source 8
    target 6
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_45"
      target_id "M123_259"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 24
    source 8
    target 7
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_45"
      target_id "M123_258"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 25
    source 9
    target 8
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_77"
      target_id "M123_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 26
    source 10
    target 8
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_79"
      target_id "M123_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 27
    source 11
    target 8
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_82"
      target_id "M123_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 28
    source 8
    target 12
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_45"
      target_id "M123_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 29
    source 8
    target 13
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_45"
      target_id "M123_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 30
    source 14
    target 9
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "PRODUCTION"
      source_id "M123_43"
      target_id "M123_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 31
    source 15
    target 14
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CONSPUMPTION"
      source_id "M123_78"
      target_id "M123_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 32
    source 16
    target 14
    cd19dm [
      diagram "C19DMap:Kynurenine synthesis pathway"
      edge_type "CATALYSIS"
      source_id "M123_197"
      target_id "M123_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
