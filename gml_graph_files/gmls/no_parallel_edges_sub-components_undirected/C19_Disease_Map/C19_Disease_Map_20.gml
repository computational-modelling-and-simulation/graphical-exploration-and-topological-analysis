# generated with VANTED V2.8.2 at Fri Mar 04 10:04:38 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:doi:10.1101/2020.03.31.019216;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:P0C6X7;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:P0C6X7;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:hgnc.symbol:NEMF;urn:miriam:hgnc.symbol:NEMF;urn:miriam:ncbigene:9147;urn:miriam:ncbigene:9147;urn:miriam:ensembl:ENSG00000165525;urn:miriam:hgnc:10663;urn:miriam:uniprot:O60524;urn:miriam:refseq:NM_004713"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:NEMF"
      map_id "M115_101"
      name "pathogen"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa85"
      uniprot "UNIPROT:P0C6X7;UNIPROT:P0DTD1;UNIPROT:O60524"
    ]
    graphics [
      x 1331.4760702265748
      y 1512.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_187"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10166"
      uniprot "NA"
    ]
    graphics [
      x 1208.8536650413898
      y 939.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_187"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_193"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10172"
      uniprot "NA"
    ]
    graphics [
      x 1991.4760702265748
      y 1174.8841075166547
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_193"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_314"
      name "Neutrophil_underscore_activation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa1278"
      uniprot "NA"
    ]
    graphics [
      x 1787.2023363703734
      y 1021.7974116997934
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_314"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_307"
      name "Nsp10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1257"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1512.5458652406987
      y 1662.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_307"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_1"
      name "Nsp9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a7c94"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 2078.85366504139
      y 689.4637214441526
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:P0C6X7"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_345"
      name "Nsp16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1374"
      uniprot "UNIPROT:P0C6X7"
    ]
    graphics [
      x 1594.2550305160507
      y 1032.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_345"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:NEMF;urn:miriam:hgnc.symbol:NEMF;urn:miriam:ncbigene:9147;urn:miriam:ncbigene:9147;urn:miriam:ensembl:ENSG00000165525;urn:miriam:hgnc:10663;urn:miriam:uniprot:O60524;urn:miriam:refseq:NM_004713"
      hgnc "HGNC_SYMBOL:NEMF"
      map_id "M115_346"
      name "NEMF"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1376"
      uniprot "UNIPROT:O60524"
    ]
    graphics [
      x 1721.4760702265748
      y 1186.8134512202714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_346"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:P0C6X7"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_347"
      name "Nsp14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1378"
      uniprot "UNIPROT:P0C6X7"
    ]
    graphics [
      x 938.8536650413897
      y 614.5732036787322
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_347"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_231"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10210"
      uniprot "NA"
    ]
    graphics [
      x 1222.5144998161113
      y 2049.0166269831952
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_231"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_234"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10213"
      uniprot "NA"
    ]
    graphics [
      x 1537.8559794510443
      y 162.06502830998443
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_234"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:23089;urn:miriam:ncbigene:23089;urn:miriam:hgnc:14005;urn:miriam:refseq:NM_015068;urn:miriam:ensembl:ENSG00000242265;urn:miriam:uniprot:Q86TG7;urn:miriam:hgnc.symbol:PEG10;urn:miriam:hgnc.symbol:PEG10"
      hgnc "HGNC_SYMBOL:PEG10"
      map_id "M115_361"
      name "PEG10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1418"
      uniprot "UNIPROT:Q86TG7"
    ]
    graphics [
      x 1683.4446667892148
      y 81.90905706776857
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_361"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:doi:10.1101/2020.06.17.156455;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:P0C6X7;urn:miriam:ncbigene:23089;urn:miriam:ncbigene:23089;urn:miriam:hgnc:14005;urn:miriam:refseq:NM_015068;urn:miriam:ensembl:ENSG00000242265;urn:miriam:uniprot:Q86TG7;urn:miriam:hgnc.symbol:PEG10;urn:miriam:hgnc.symbol:PEG10"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:PEG10"
      map_id "M115_35"
      name "PEG10comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa130"
      uniprot "UNIPROT:P0C6X7;UNIPROT:Q86TG7"
    ]
    graphics [
      x 2201.476070226575
      y 1170.5867359968995
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:Q9NXA8;urn:miriam:ec-code:2.3.1.-;urn:miriam:hgnc.symbol:SIRT5;urn:miriam:hgnc.symbol:SIRT5;urn:miriam:hgnc:14933;urn:miriam:ncbigene:23408;urn:miriam:ensembl:ENSG00000124523;urn:miriam:ncbigene:23408;urn:miriam:refseq:NM_001193267"
      hgnc "HGNC_SYMBOL:SIRT5"
      map_id "M115_358"
      name "SIRT5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1408"
      uniprot "UNIPROT:Q9NXA8"
    ]
    graphics [
      x 2197.9673850663708
      y 1579.1063115500313
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_358"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:uniprot:Q9NXA8;urn:miriam:ec-code:2.3.1.-;urn:miriam:hgnc.symbol:SIRT5;urn:miriam:hgnc.symbol:SIRT5;urn:miriam:hgnc:14933;urn:miriam:ncbigene:23408;urn:miriam:ensembl:ENSG00000124523;urn:miriam:ncbigene:23408;urn:miriam:refseq:NM_001193267;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:P0C6X7"
      hgnc "HGNC_SYMBOL:SIRT5;HGNC_SYMBOL:rep"
      map_id "M115_31"
      name "SIRT5comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa127"
      uniprot "UNIPROT:Q9NXA8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 862.5144998161113
      y 2075.6390909233837
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "PUBMED:17355872"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_232"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10211"
      uniprot "NA"
    ]
    graphics [
      x 1746.2506324725077
      y 2376.313882011814
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_232"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:5361"
      hgnc "NA"
      map_id "M115_359"
      name "Suramin"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1412"
      uniprot "NA"
    ]
    graphics [
      x 1754.0477541582409
      y 2256.1584713992374
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_359"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:17355872;urn:miriam:pubchem.compound:5361;urn:miriam:uniprot:Q9NXA8;urn:miriam:ec-code:2.3.1.-;urn:miriam:hgnc.symbol:SIRT5;urn:miriam:hgnc.symbol:SIRT5;urn:miriam:hgnc:14933;urn:miriam:ncbigene:23408;urn:miriam:ensembl:ENSG00000124523;urn:miriam:ncbigene:23408;urn:miriam:refseq:NM_001193267"
      hgnc "HGNC_SYMBOL:SIRT5"
      map_id "M115_32"
      name "SScomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa128"
      uniprot "UNIPROT:Q9NXA8"
    ]
    graphics [
      x 1702.9931034425704
      y 2573.2843804966874
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_149"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10125"
      uniprot "NA"
    ]
    graphics [
      x 2651.476070226575
      y 1389.6505255276968
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_149"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_127"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10103"
      uniprot "NA"
    ]
    graphics [
      x 2737.9673850663708
      y 1576.177349195188
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_127"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "PUBMED:19153232"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_185"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10164"
      uniprot "NA"
    ]
    graphics [
      x 1653.4446667892148
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_185"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "PUBMED:14962394"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_131"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10107"
      uniprot "NA"
    ]
    graphics [
      x 2621.476070226575
      y 1471.7464025950017
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_131"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_385"
      name "Nsp8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa997"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1817.2023363703734
      y 986.3123323415327
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_385"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32296183;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_23"
      name "NspComp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa12"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 2381.476070226575
      y 1400.6873388578365
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_211"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10190"
      uniprot "NA"
    ]
    graphics [
      x 1418.8536650413896
      y 882.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_211"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_217"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10196"
      uniprot "NA"
    ]
    graphics [
      x 701.4760702265747
      y 1353.3762122382575
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_217"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "PUBMED:24778252;PUBMED:32353859;PUBMED:29845934;PUBMED:18483487;PUBMED:26725010;PUBMED:17643375"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_218"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10197"
      uniprot "NA"
    ]
    graphics [
      x 1211.8433826377968
      y 282.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_218"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_208"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10187"
      uniprot "NA"
    ]
    graphics [
      x 1758.8323727979182
      y 2624.0632066061426
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_208"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_209"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10188"
      uniprot "NA"
    ]
    graphics [
      x 2561.476070226575
      y 1062.895331264368
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_209"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_164"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10141"
      uniprot "NA"
    ]
    graphics [
      x 2498.85366504139
      y 840.67750405828
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_164"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_220"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10199"
      uniprot "NA"
    ]
    graphics [
      x 1751.4760702265748
      y 1156.8134512202714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_220"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_210"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10189"
      uniprot "NA"
    ]
    graphics [
      x 1405.2508552528402
      y 2562.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_210"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_206"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10185"
      uniprot "NA"
    ]
    graphics [
      x 2741.476070226575
      y 1280.21569942541
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_206"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_215"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10194"
      uniprot "NA"
    ]
    graphics [
      x 1841.4760702265748
      y 1182.4281379137565
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_215"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_207"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10186"
      uniprot "NA"
    ]
    graphics [
      x 788.8536650413897
      y 1086.9425226151097
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_207"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_219"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10198"
      uniprot "NA"
    ]
    graphics [
      x 2231.476070226575
      y 1395.8636147371178
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_219"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_205"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10184"
      uniprot "NA"
    ]
    graphics [
      x 1807.9673850663708
      y 1573.8597507391748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_205"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_213"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10192"
      uniprot "NA"
    ]
    graphics [
      x 2531.476070226575
      y 1456.2604329323347
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_213"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000115761;urn:miriam:ncbigene:79954;urn:miriam:refseq:NM_024894;urn:miriam:ncbigene:79954;urn:miriam:uniprot:Q9BSC4;urn:miriam:hgnc:25862;urn:miriam:hgnc.symbol:NOL10;urn:miriam:hgnc.symbol:NOL10"
      hgnc "HGNC_SYMBOL:NOL10"
      map_id "M115_334"
      name "NOL10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1343"
      uniprot "UNIPROT:Q9BSC4"
    ]
    graphics [
      x 2261.476070226575
      y 1176.5080320248062
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_334"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ensembl:ENSG00000115761;urn:miriam:ncbigene:79954;urn:miriam:refseq:NM_024894;urn:miriam:ncbigene:79954;urn:miriam:uniprot:Q9BSC4;urn:miriam:hgnc:25862;urn:miriam:hgnc.symbol:NOL10;urn:miriam:hgnc.symbol:NOL10"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:NOL10"
      map_id "M115_13"
      name "NOL10comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa110"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q9BSC4"
    ]
    graphics [
      x 2621.476070226575
      y 1316.4653703417496
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:MPHOSPH10;urn:miriam:hgnc.symbol:MPHOSPH10;urn:miriam:ncbigene:10199;urn:miriam:ncbigene:10199;urn:miriam:hgnc:7213;urn:miriam:ensembl:ENSG00000124383;urn:miriam:uniprot:O00566;urn:miriam:refseq:NM_005791"
      hgnc "HGNC_SYMBOL:MPHOSPH10"
      map_id "M115_323"
      name "MPHOSPH10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1312"
      uniprot "UNIPROT:O00566"
    ]
    graphics [
      x 2437.9673850663708
      y 1653.474901332917
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_323"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:hgnc.symbol:MPHOSPH10;urn:miriam:hgnc.symbol:MPHOSPH10;urn:miriam:ncbigene:10199;urn:miriam:ncbigene:10199;urn:miriam:hgnc:7213;urn:miriam:ensembl:ENSG00000124383;urn:miriam:uniprot:O00566;urn:miriam:refseq:NM_005791"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:MPHOSPH10"
      map_id "M115_5"
      name "MPHOSPHcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa102"
      uniprot "UNIPROT:P0DTD1;UNIPROT:O00566"
    ]
    graphics [
      x 1811.4760702265748
      y 1324.8138321341212
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000178105;urn:miriam:hgnc.symbol:DDX10;urn:miriam:hgnc.symbol:DDX10;urn:miriam:refseq:NM_004398;urn:miriam:ec-code:3.6.4.13;urn:miriam:hgnc:2735;urn:miriam:ncbigene:1662;urn:miriam:ncbigene:1662;urn:miriam:uniprot:Q13206"
      hgnc "HGNC_SYMBOL:DDX10"
      map_id "M115_342"
      name "DDX10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1366"
      uniprot "UNIPROT:Q13206"
    ]
    graphics [
      x 1702.5144998161113
      y 1822.3457253896618
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_342"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ensembl:ENSG00000178105;urn:miriam:hgnc.symbol:DDX10;urn:miriam:hgnc.symbol:DDX10;urn:miriam:refseq:NM_004398;urn:miriam:ec-code:3.6.4.13;urn:miriam:hgnc:2735;urn:miriam:ncbigene:1662;urn:miriam:ncbigene:1662;urn:miriam:uniprot:Q13206;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:DDX10;HGNC_SYMBOL:rep"
      map_id "M115_19"
      name "DDX10comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa116"
      uniprot "UNIPROT:Q13206;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1898.8536650413898
      y 837.1067204666158
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:FOCAD;urn:miriam:hgnc.symbol:FOCAD;urn:miriam:uniprot:Q5VW36;urn:miriam:refseq:NM_017794;urn:miriam:hgnc:23377;urn:miriam:ensembl:ENSG00000188352;urn:miriam:ncbigene:54914;urn:miriam:ncbigene:54914"
      hgnc "HGNC_SYMBOL:FOCAD"
      map_id "M115_325"
      name "FOCAD"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1318"
      uniprot "UNIPROT:Q5VW36"
    ]
    graphics [
      x 1322.112491134918
      y 792.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_325"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_283"
      name "Nsp7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1183"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1814.852372920114
      y 586.8134512202714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_283"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:doi:10.1101/2020.06.17.156455;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:hgnc.symbol:FOCAD;urn:miriam:hgnc.symbol:FOCAD;urn:miriam:uniprot:Q5VW36;urn:miriam:refseq:NM_017794;urn:miriam:hgnc:23377;urn:miriam:ensembl:ENSG00000188352;urn:miriam:ncbigene:54914;urn:miriam:ncbigene:54914;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:FOCAD"
      map_id "M115_7"
      name "FOCADcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa104"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q5VW36"
    ]
    graphics [
      x 881.4760702265747
      y 1375.488055210296
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_178"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10156"
      uniprot "NA"
    ]
    graphics [
      x 1451.4760702265748
      y 1032.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_178"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:1371;urn:miriam:uniprot:O43570;urn:miriam:ncbigene:771;urn:miriam:refseq:NM_001218;urn:miriam:ncbigene:771;urn:miriam:ec-code:4.2.1.1;urn:miriam:hgnc.symbol:CA12;urn:miriam:hgnc.symbol:CA12;urn:miriam:ensembl:ENSG00000074410"
      hgnc "HGNC_SYMBOL:CA12"
      map_id "M115_299"
      name "CA12"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1228"
      uniprot "UNIPROT:O43570"
    ]
    graphics [
      x 2441.476070226575
      y 1412.9308892638833
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_299"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:doi:10.1101/2020.06.17.156455;urn:miriam:hgnc:1371;urn:miriam:uniprot:O43570;urn:miriam:ncbigene:771;urn:miriam:refseq:NM_001218;urn:miriam:ncbigene:771;urn:miriam:ec-code:4.2.1.1;urn:miriam:hgnc.symbol:CA12;urn:miriam:hgnc.symbol:CA12;urn:miriam:ensembl:ENSG00000074410;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:CA12;HGNC_SYMBOL:rep"
      map_id "M115_91"
      name "CA12comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa76"
      uniprot "UNIPROT:O43570;UNIPROT:P0DTD1"
    ]
    graphics [
      x 611.4760702265747
      y 1422.9213497817198
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "PUBMED:17582922;PUBMED:18537527;PUBMED:17762320;PUBMED:19703035;PUBMED:18782051"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_179"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10157"
      uniprot "NA"
    ]
    graphics [
      x 2351.476070226575
      y 1316.883873182828
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_179"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "PUBMED:20185318"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_180"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10158"
      uniprot "NA"
    ]
    graphics [
      x 1911.6104592494069
      y 1750.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_180"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "PUBMED:19119014"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_181"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10159"
      uniprot "NA"
    ]
    graphics [
      x 1935.0496494273596
      y 272.7686655077657
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_181"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      annotation "PUBMED:19119014"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_182"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10161"
      uniprot "NA"
    ]
    graphics [
      x 1538.8536650413898
      y 552.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_182"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "PUBMED:19119014"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_183"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10162"
      uniprot "NA"
    ]
    graphics [
      x 2500.595114251623
      y 615.6965458018606
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_183"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:2343"
      hgnc "NA"
      map_id "M115_304"
      name "Benzthiazide"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1244"
      uniprot "NA"
    ]
    graphics [
      x 2313.826162608179
      y 416.49646840170726
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_304"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:19119014;urn:miriam:pubchem.compound:2343;urn:miriam:hgnc:1371;urn:miriam:uniprot:O43570;urn:miriam:ncbigene:771;urn:miriam:refseq:NM_001218;urn:miriam:ncbigene:771;urn:miriam:ec-code:4.2.1.1;urn:miriam:hgnc.symbol:CA12;urn:miriam:hgnc.symbol:CA12;urn:miriam:ensembl:ENSG00000074410"
      hgnc "HGNC_SYMBOL:CA12"
      map_id "M115_97"
      name "BZcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa81"
      uniprot "UNIPROT:O43570"
    ]
    graphics [
      x 2288.85366504139
      y 689.8668438235965
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:70876165"
      hgnc "NA"
      map_id "M115_303"
      name "Hydroflumethiazide"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1241"
      uniprot "NA"
    ]
    graphics [
      x 1655.5335816614925
      y 762.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_303"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:19119014;urn:miriam:pubchem.compound:70876165;urn:miriam:hgnc:1371;urn:miriam:uniprot:O43570;urn:miriam:ncbigene:771;urn:miriam:refseq:NM_001218;urn:miriam:ncbigene:771;urn:miriam:ec-code:4.2.1.1;urn:miriam:hgnc.symbol:CA12;urn:miriam:hgnc.symbol:CA12;urn:miriam:ensembl:ENSG00000074410"
      hgnc "HGNC_SYMBOL:CA12"
      map_id "M115_96"
      name "HFTcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa80"
      uniprot "UNIPROT:O43570"
    ]
    graphics [
      x 1028.8536650413898
      y 1041.5374917954796
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:3639"
      hgnc "NA"
      map_id "M115_302"
      name "Hydrochlorothiazide"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1237"
      uniprot "NA"
    ]
    graphics [
      x 1526.090149807047
      y 372.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_302"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:19119014;urn:miriam:pubchem.compound:3639;urn:miriam:hgnc:1371;urn:miriam:uniprot:O43570;urn:miriam:ncbigene:771;urn:miriam:refseq:NM_001218;urn:miriam:ncbigene:771;urn:miriam:ec-code:4.2.1.1;urn:miriam:hgnc.symbol:CA12;urn:miriam:hgnc.symbol:CA12;urn:miriam:ensembl:ENSG00000074410"
      hgnc "HGNC_SYMBOL:CA12"
      map_id "M115_94"
      name "HCTcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa79"
      uniprot "UNIPROT:O43570"
    ]
    graphics [
      x 1994.9594763279729
      y 243.89310821217487
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:5281855"
      hgnc "NA"
      map_id "M115_301"
      name "Ellagic_space_Acid"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1234"
      uniprot "NA"
    ]
    graphics [
      x 2021.4760702265748
      y 1222.73118595318
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_301"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:20185318;urn:miriam:hgnc:1371;urn:miriam:uniprot:O43570;urn:miriam:ncbigene:771;urn:miriam:refseq:NM_001218;urn:miriam:ncbigene:771;urn:miriam:ec-code:4.2.1.1;urn:miriam:hgnc.symbol:CA12;urn:miriam:hgnc.symbol:CA12;urn:miriam:ensembl:ENSG00000074410;urn:miriam:pubchem.compound:5281855"
      hgnc "HGNC_SYMBOL:CA12"
      map_id "M115_93"
      name "EAcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa78"
      uniprot "UNIPROT:O43570"
    ]
    graphics [
      x 1420.4146427238684
      y 2262.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.substance:5734"
      hgnc "NA"
      map_id "M115_300"
      name "Zonisamide"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1231"
      uniprot "NA"
    ]
    graphics [
      x 2452.5144998161113
      y 1934.5551036346828
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_300"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:19703035;urn:miriam:hgnc:1371;urn:miriam:uniprot:O43570;urn:miriam:ncbigene:771;urn:miriam:refseq:NM_001218;urn:miriam:ncbigene:771;urn:miriam:ec-code:4.2.1.1;urn:miriam:hgnc.symbol:CA12;urn:miriam:hgnc.symbol:CA12;urn:miriam:ensembl:ENSG00000074410;urn:miriam:pubchem.substance:5734"
      hgnc "HGNC_SYMBOL:CA12"
      map_id "M115_92"
      name "ZonisamideComp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa77"
      uniprot "UNIPROT:O43570"
    ]
    graphics [
      x 1871.4760702265748
      y 1243.234341461012
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:26274;urn:miriam:uniprot:Q96I59;urn:miriam:hgnc.symbol:NARS2;urn:miriam:ensembl:ENSG00000137513;urn:miriam:hgnc.symbol:NARS2;urn:miriam:ncbigene:79731;urn:miriam:ncbigene:79731;urn:miriam:refseq:NM_024678;urn:miriam:ec-code:6.1.1.22"
      hgnc "HGNC_SYMBOL:NARS2"
      map_id "M115_336"
      name "NARS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1349"
      uniprot "UNIPROT:Q96I59"
    ]
    graphics [
      x 1731.6104592494069
      y 1736.8388397460556
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_336"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:doi:10.1101/2020.03.31.019216;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:hgnc:26274;urn:miriam:uniprot:Q96I59;urn:miriam:hgnc.symbol:NARS2;urn:miriam:ensembl:ENSG00000137513;urn:miriam:hgnc.symbol:NARS2;urn:miriam:ncbigene:79731;urn:miriam:ncbigene:79731;urn:miriam:refseq:NM_024678;urn:miriam:ec-code:6.1.1.22"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:NARS2"
      map_id "M115_15"
      name "NARS2comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa112"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q96I59"
    ]
    graphics [
      x 2288.85366504139
      y 904.7434574767584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      annotation "PUBMED:12874385;PUBMED:16753178"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_216"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10195"
      uniprot "NA"
    ]
    graphics [
      x 1271.4760702265748
      y 1452.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_216"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:6267;urn:miriam:obo.chebi:CHEBI%3A17196"
      hgnc "NA"
      map_id "M115_337"
      name "L_minus_Asparagine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa1353"
      uniprot "NA"
    ]
    graphics [
      x 761.4760702265747
      y 1162.5286933952282
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_337"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:16753178;urn:miriam:hgnc:26274;urn:miriam:uniprot:Q96I59;urn:miriam:hgnc.symbol:NARS2;urn:miriam:ensembl:ENSG00000137513;urn:miriam:hgnc.symbol:NARS2;urn:miriam:ncbigene:79731;urn:miriam:ncbigene:79731;urn:miriam:refseq:NM_024678;urn:miriam:ec-code:6.1.1.22;urn:miriam:pubchem.compound:6267;urn:miriam:obo.chebi:CHEBI%3A17196"
      hgnc "HGNC_SYMBOL:NARS2"
      map_id "M115_16"
      name "NLcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa113"
      uniprot "UNIPROT:Q96I59"
    ]
    graphics [
      x 1631.4760702265748
      y 1032.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:Q9H6F5;urn:miriam:hgnc.symbol:CCDC86;urn:miriam:hgnc.symbol:CCDC86;urn:miriam:refseq:NM_024098;urn:miriam:hgnc:28359;urn:miriam:ncbigene:79080;urn:miriam:ncbigene:79080;urn:miriam:ensembl:ENSG00000110104"
      hgnc "HGNC_SYMBOL:CCDC86"
      map_id "M115_324"
      name "CCDC86"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1315"
      uniprot "UNIPROT:Q9H6F5"
    ]
    graphics [
      x 1941.6104592494069
      y 1780.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_324"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:Q9H6F5;urn:miriam:hgnc.symbol:CCDC86;urn:miriam:hgnc.symbol:CCDC86;urn:miriam:refseq:NM_024098;urn:miriam:hgnc:28359;urn:miriam:ncbigene:79080;urn:miriam:ncbigene:79080;urn:miriam:ensembl:ENSG00000110104"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:CCDC86"
      map_id "M115_6"
      name "CCDCcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa103"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q9H6F5"
    ]
    graphics [
      x 2827.9673850663708
      y 1612.0744328063608
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:19235;urn:miriam:ensembl:ENSG00000275700;urn:miriam:uniprot:Q9NY61;urn:miriam:ncbigene:26574;urn:miriam:ncbigene:26574;urn:miriam:hgnc.symbol:AATF;urn:miriam:hgnc.symbol:AATF;urn:miriam:refseq:NM_012138"
      hgnc "HGNC_SYMBOL:AATF"
      map_id "M115_329"
      name "AATF"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1330"
      uniprot "UNIPROT:Q9NY61"
    ]
    graphics [
      x 1849.8548263367193
      y 2470.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_329"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:hgnc:19235;urn:miriam:ensembl:ENSG00000275700;urn:miriam:uniprot:Q9NY61;urn:miriam:ncbigene:26574;urn:miriam:ncbigene:26574;urn:miriam:hgnc.symbol:AATF;urn:miriam:hgnc.symbol:AATF;urn:miriam:refseq:NM_012138;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:AATF;HGNC_SYMBOL:rep"
      map_id "M115_10"
      name "AATFcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa107"
      uniprot "UNIPROT:Q9NY61;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1330.4075582174767
      y 2502.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:refseq:NM_003136;urn:miriam:hgnc:11301;urn:miriam:ncbigene:6729;urn:miriam:ncbigene:6729;urn:miriam:ensembl:ENSG00000100883;urn:miriam:hgnc.symbol:SRP54;urn:miriam:hgnc.symbol:SRP54;urn:miriam:uniprot:P61011;urn:miriam:ec-code:3.6.5.-"
      hgnc "HGNC_SYMBOL:SRP54"
      map_id "M115_343"
      name "SRP54"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1369"
      uniprot "UNIPROT:P61011"
    ]
    graphics [
      x 998.8536650413897
      y 833.2541535098845
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_343"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:6728;urn:miriam:ncbigene:6728;urn:miriam:refseq:NM_003135;urn:miriam:hgnc.symbol:SRP19;urn:miriam:ensembl:ENSG00000153037;urn:miriam:hgnc.symbol:SRP19;urn:miriam:hgnc:11300;urn:miriam:uniprot:P09132"
      hgnc "HGNC_SYMBOL:SRP19"
      map_id "M115_344"
      name "SRP19"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1372"
      uniprot "UNIPROT:P09132"
    ]
    graphics [
      x 1544.422992321934
      y 1662.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_344"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:refseq:NM_003136;urn:miriam:hgnc:11301;urn:miriam:ncbigene:6729;urn:miriam:ncbigene:6729;urn:miriam:ensembl:ENSG00000100883;urn:miriam:hgnc.symbol:SRP54;urn:miriam:hgnc.symbol:SRP54;urn:miriam:uniprot:P61011;urn:miriam:ec-code:3.6.5.-;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ncbigene:6728;urn:miriam:ncbigene:6728;urn:miriam:refseq:NM_003135;urn:miriam:hgnc.symbol:SRP19;urn:miriam:ensembl:ENSG00000153037;urn:miriam:hgnc.symbol:SRP19;urn:miriam:hgnc:11300;urn:miriam:uniprot:P09132"
      hgnc "HGNC_SYMBOL:SRP54;HGNC_SYMBOL:rep;HGNC_SYMBOL:SRP19"
      map_id "M115_20"
      name "SRP54comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa117"
      uniprot "UNIPROT:P61011;UNIPROT:P0DTD1;UNIPROT:P09132"
    ]
    graphics [
      x 1492.5144998161113
      y 1902.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_284"
      name "Nsp12"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1184"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1988.8536650413898
      y 938.5267273646541
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_284"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:doi:10.1101/2020.03.16.993386;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_78"
      name "Nsp7812"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa63"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1871.4760702265748
      y 1113.9430771427117
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      annotation "PUBMED:32405421"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_165"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10142"
      uniprot "NA"
    ]
    graphics [
      x 2111.476070226575
      y 1172.577263483342
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_165"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:8673700"
      hgnc "NA"
      map_id "M115_286"
      name "RdRpassembled"
      node_subtype "RNA"
      node_type "species"
      org_id "sa1189"
      uniprot "NA"
    ]
    graphics [
      x 2261.476070226575
      y 1206.5080320248062
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_286"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:121304016"
      hgnc "NA"
      map_id "M115_285"
      name "remdesivir_space_"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1188"
      uniprot "NA"
    ]
    graphics [
      x 1601.4760702265748
      y 1182.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_285"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32620147;urn:miriam:pubchem.compound:492405"
      hgnc "NA"
      map_id "M115_375"
      name "favipiravir"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1435"
      uniprot "NA"
    ]
    graphics [
      x 2411.476070226575
      y 1227.7361723586291
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_375"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:doi:10.1126/science.abc1560;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ncbigene:8673700;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_79"
      name "Nsp7812"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa64"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 2647.9673850663708
      y 1741.7464025950017
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_166"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10143"
      uniprot "NA"
    ]
    graphics [
      x 2711.476070226575
      y 1250.1339051066395
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_166"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:doi:10.1126/science.abc1560"
      hgnc "NA"
      map_id "M115_287"
      name "virus_underscore_replication"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa1194"
      uniprot "NA"
    ]
    graphics [
      x 2677.9673850663708
      y 1649.6492573955252
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_287"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_168"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10145"
      uniprot "NA"
    ]
    graphics [
      x 848.8536650413897
      y 946.0055613274181
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_168"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_173"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10150"
      uniprot "NA"
    ]
    graphics [
      x 2362.5144998161113
      y 1978.1103888903936
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_173"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_174"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10151"
      uniprot "NA"
    ]
    graphics [
      x 2318.85366504139
      y 688.1775798480458
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_174"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_167"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10144"
      uniprot "NA"
    ]
    graphics [
      x 2462.594728590883
      y 723.6041051185921
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_167"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_172"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10149"
      uniprot "NA"
    ]
    graphics [
      x 2168.85366504139
      y 827.2667739529358
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_172"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_170"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10147"
      uniprot "NA"
    ]
    graphics [
      x 2647.9673850663708
      y 1777.0179366142534
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_170"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_175"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10152"
      uniprot "NA"
    ]
    graphics [
      x 2047.9673850663708
      y 1529.1206388522583
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_175"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_171"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10148"
      uniprot "NA"
    ]
    graphics [
      x 1988.8536650413898
      y 603.435893128261
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_171"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000140262;urn:miriam:hgnc:11623;urn:miriam:ncbigene:6938;urn:miriam:ncbigene:6938;urn:miriam:uniprot:Q99081;urn:miriam:hgnc.symbol:TCF12;urn:miriam:hgnc.symbol:TCF12;urn:miriam:refseq:NM_003205"
      hgnc "HGNC_SYMBOL:TCF12"
      map_id "M115_292"
      name "TCF12"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1207"
      uniprot "UNIPROT:Q99081"
    ]
    graphics [
      x 2300.1736034760024
      y 528.3741058043094
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_292"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ensembl:ENSG00000140262;urn:miriam:hgnc:11623;urn:miriam:ncbigene:6938;urn:miriam:ncbigene:6938;urn:miriam:uniprot:Q99081;urn:miriam:hgnc.symbol:TCF12;urn:miriam:hgnc.symbol:TCF12;urn:miriam:refseq:NM_003205"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:TCF12"
      map_id "M115_84"
      name "TCFcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa69"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q99081"
    ]
    graphics [
      x 1928.8536650413898
      y 962.6254878021133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:2.3.2.33;urn:miriam:uniprot:O75592;urn:miriam:refseq:NM_015057;urn:miriam:ncbigene:23077;urn:miriam:ncbigene:23077;urn:miriam:hgnc.symbol:MYCBP2;urn:miriam:ensembl:ENSG00000005810;urn:miriam:hgnc.symbol:MYCBP2;urn:miriam:hgnc:23386"
      hgnc "HGNC_SYMBOL:MYCBP2"
      map_id "M115_296"
      name "MYCBP2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1219"
      uniprot "UNIPROT:O75592"
    ]
    graphics [
      x 2441.476070226575
      y 1122.6099766847087
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_296"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ec-code:2.3.2.33;urn:miriam:uniprot:O75592;urn:miriam:refseq:NM_015057;urn:miriam:ncbigene:23077;urn:miriam:ncbigene:23077;urn:miriam:hgnc.symbol:MYCBP2;urn:miriam:ensembl:ENSG00000005810;urn:miriam:hgnc.symbol:MYCBP2;urn:miriam:hgnc:23386"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:MYCBP2"
      map_id "M115_88"
      name "MYCBPcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa73"
      uniprot "UNIPROT:P0DTD1;UNIPROT:O75592"
    ]
    graphics [
      x 2272.5144998161113
      y 1989.2456562806215
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:refseq:NM_014345;urn:miriam:uniprot:Q5VUA4;urn:miriam:hgnc.symbol:ZNF318;urn:miriam:hgnc.symbol:ZNF318;urn:miriam:hgnc:13578;urn:miriam:ensembl:ENSG00000171467;urn:miriam:ncbigene:24149;urn:miriam:ncbigene:24149"
      hgnc "HGNC_SYMBOL:ZNF318"
      map_id "M115_291"
      name "ZNF318"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1204"
      uniprot "UNIPROT:Q5VUA4"
    ]
    graphics [
      x 2771.476070226575
      y 1500.111162086949
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_291"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:refseq:NM_014345;urn:miriam:uniprot:Q5VUA4;urn:miriam:hgnc.symbol:ZNF318;urn:miriam:hgnc.symbol:ZNF318;urn:miriam:hgnc:13578;urn:miriam:ensembl:ENSG00000171467;urn:miriam:ncbigene:24149;urn:miriam:ncbigene:24149;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:ZNF318;HGNC_SYMBOL:rep"
      map_id "M115_83"
      name "ZNFcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa68"
      uniprot "UNIPROT:Q5VUA4;UNIPROT:P0DTD1"
    ]
    graphics [
      x 2050.4146427238684
      y 2226.313744265185
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:25617;urn:miriam:ensembl:ENSG00000089682;urn:miriam:refseq:NM_018301;urn:miriam:uniprot:Q96IZ5;urn:miriam:hgnc.symbol:RBM41;urn:miriam:ncbigene:55285;urn:miriam:hgnc.symbol:RBM41;urn:miriam:ncbigene:55285"
      hgnc "HGNC_SYMBOL:RBM41"
      map_id "M115_293"
      name "RBM41"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1210"
      uniprot "UNIPROT:Q96IZ5"
    ]
    graphics [
      x 1657.9673850663708
      y 1633.101456871948
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_293"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:hgnc:25617;urn:miriam:ensembl:ENSG00000089682;urn:miriam:refseq:NM_018301;urn:miriam:uniprot:Q96IZ5;urn:miriam:hgnc.symbol:RBM41;urn:miriam:ncbigene:55285;urn:miriam:hgnc.symbol:RBM41;urn:miriam:ncbigene:55285"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:RBM41"
      map_id "M115_85"
      name "RBMcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa70"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q96IZ5"
    ]
    graphics [
      x 2258.85366504139
      y 617.1433361096879
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:55206;urn:miriam:ncbigene:55206;urn:miriam:hgnc:22973;urn:miriam:hgnc.symbol:SBNO1;urn:miriam:hgnc.symbol:SBNO1;urn:miriam:ensembl:ENSG00000139697;urn:miriam:uniprot:A3KN83;urn:miriam:refseq:NM_018183"
      hgnc "HGNC_SYMBOL:SBNO1"
      map_id "M115_288"
      name "SBNO1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1195"
      uniprot "UNIPROT:A3KN83"
    ]
    graphics [
      x 2647.9673850663708
      y 1621.7464025950017
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_288"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ncbigene:55206;urn:miriam:ncbigene:55206;urn:miriam:hgnc:22973;urn:miriam:hgnc.symbol:SBNO1;urn:miriam:hgnc.symbol:SBNO1;urn:miriam:ensembl:ENSG00000139697;urn:miriam:uniprot:A3KN83;urn:miriam:refseq:NM_018183"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:SBNO1"
      map_id "M115_80"
      name "SBNOcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa65"
      uniprot "UNIPROT:P0DTD1;UNIPROT:A3KN83"
    ]
    graphics [
      x 2076.56303131958
      y 424.09135356408933
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:ZC3H7A;urn:miriam:hgnc.symbol:ZC3H7A;urn:miriam:refseq:NM_014153;urn:miriam:uniprot:Q8IWR0;urn:miriam:ensembl:ENSG00000122299;urn:miriam:ncbigene:29066;urn:miriam:ncbigene:29066;urn:miriam:hgnc:30959"
      hgnc "HGNC_SYMBOL:ZC3H7A"
      map_id "M115_295"
      name "ZC3H7A_space_"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1216"
      uniprot "UNIPROT:Q8IWR0"
    ]
    graphics [
      x 2531.476070226575
      y 1169.050987926539
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_295"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:hgnc.symbol:ZC3H7A;urn:miriam:hgnc.symbol:ZC3H7A;urn:miriam:refseq:NM_014153;urn:miriam:uniprot:Q8IWR0;urn:miriam:ensembl:ENSG00000122299;urn:miriam:ncbigene:29066;urn:miriam:ncbigene:29066;urn:miriam:hgnc:30959"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:ZC3H7A"
      map_id "M115_87"
      name "ZC3H7Acomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa72"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q8IWR0"
    ]
    graphics [
      x 2381.476070226575
      y 1238.1105328022936
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:Q92615;urn:miriam:refseq:NM_015155;urn:miriam:ncbigene:23185;urn:miriam:ncbigene:23185;urn:miriam:hgnc:28987;urn:miriam:ensembl:ENSG00000107929;urn:miriam:hgnc.symbol:LARP4B;urn:miriam:hgnc.symbol:LARP4B"
      hgnc "HGNC_SYMBOL:LARP4B"
      map_id "M115_294"
      name "LARP4B_space_"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1213"
      uniprot "UNIPROT:Q92615"
    ]
    graphics [
      x 2497.9673850663708
      y 1728.8975095895871
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_294"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:Q92615;urn:miriam:refseq:NM_015155;urn:miriam:ncbigene:23185;urn:miriam:ncbigene:23185;urn:miriam:hgnc:28987;urn:miriam:ensembl:ENSG00000107929;urn:miriam:hgnc.symbol:LARP4B;urn:miriam:hgnc.symbol:LARP4B"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:LARP4B"
      map_id "M115_86"
      name "LARPcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa71"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q92615"
    ]
    graphics [
      x 1961.4760702265748
      y 1457.606843746084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:2.7.11.4;urn:miriam:uniprot:O14874;urn:miriam:hgnc.symbol:BCKDK;urn:miriam:hgnc.symbol:BCKDK;urn:miriam:ncbigene:10295;urn:miriam:refseq:NM_005881;urn:miriam:ncbigene:10295;urn:miriam:ensembl:ENSG00000103507;urn:miriam:hgnc:16902"
      hgnc "HGNC_SYMBOL:BCKDK"
      map_id "M115_289"
      name "BCKDK"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1198"
      uniprot "UNIPROT:O14874"
    ]
    graphics [
      x 1042.5144998161113
      y 1887.9705867362836
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_289"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ec-code:2.7.11.4;urn:miriam:uniprot:O14874;urn:miriam:hgnc.symbol:BCKDK;urn:miriam:hgnc.symbol:BCKDK;urn:miriam:ncbigene:10295;urn:miriam:refseq:NM_005881;urn:miriam:ncbigene:10295;urn:miriam:ensembl:ENSG00000103507;urn:miriam:hgnc:16902"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:BCKDK"
      map_id "M115_81"
      name "s389"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa66"
      uniprot "UNIPROT:P0DTD1;UNIPROT:O14874"
    ]
    graphics [
      x 698.8536650413897
      y 471.4799954029306
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      annotation "PUBMED:17016423;PUBMED:17139284;PUBMED:10592235"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_169"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10146"
      uniprot "NA"
    ]
    graphics [
      x 1001.4760702265747
      y 1378.6039843924614
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_169"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:6022;urn:miriam:obo.chebi:CHEBI%3A16761"
      hgnc "NA"
      map_id "M115_290"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa1201"
      uniprot "NA"
    ]
    graphics [
      x 1691.4760702265748
      y 1269.571063044361
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_290"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:17016423;urn:miriam:pubchem.compound:6022;urn:miriam:obo.chebi:CHEBI%3A16761;urn:miriam:ec-code:2.7.11.4;urn:miriam:uniprot:O14874;urn:miriam:hgnc.symbol:BCKDK;urn:miriam:hgnc.symbol:BCKDK;urn:miriam:ncbigene:10295;urn:miriam:refseq:NM_005881;urn:miriam:ncbigene:10295;urn:miriam:ensembl:ENSG00000103507;urn:miriam:hgnc:16902"
      hgnc "HGNC_SYMBOL:BCKDK"
      map_id "M115_82"
      name "ADPcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa67"
      uniprot "UNIPROT:O14874"
    ]
    graphics [
      x 668.8536650413897
      y 872.5418600657114
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000144029;urn:miriam:uniprot:P82675;urn:miriam:uniprot:P82675;urn:miriam:hgnc:14498;urn:miriam:hgnc.symbol:MRPS5;urn:miriam:hgnc.symbol:MRPS5;urn:miriam:refseq:NM_031902;urn:miriam:ncbigene:64969;urn:miriam:ncbigene:64969"
      hgnc "HGNC_SYMBOL:MRPS5"
      map_id "M115_327"
      name "MRPS5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1325"
      uniprot "UNIPROT:P82675"
    ]
    graphics [
      x 1987.9673850663708
      y 1577.606843746084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_327"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:14495;urn:miriam:refseq:NM_001371401;urn:miriam:ncbigene:51116;urn:miriam:ensembl:ENSG00000122140;urn:miriam:ncbigene:51116;urn:miriam:hgnc.symbol:MRPS2;urn:miriam:hgnc.symbol:MRPS2;urn:miriam:uniprot:Q9Y399"
      hgnc "HGNC_SYMBOL:MRPS2"
      map_id "M115_328"
      name "MRPS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1328"
      uniprot "UNIPROT:Q9Y399"
    ]
    graphics [
      x 2599.1058255273197
      y 688.0428822179254
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_328"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:hgnc:14495;urn:miriam:refseq:NM_001371401;urn:miriam:ncbigene:51116;urn:miriam:ensembl:ENSG00000122140;urn:miriam:ncbigene:51116;urn:miriam:hgnc.symbol:MRPS2;urn:miriam:hgnc.symbol:MRPS2;urn:miriam:uniprot:Q9Y399;urn:miriam:ensembl:ENSG00000144029;urn:miriam:uniprot:P82675;urn:miriam:uniprot:P82675;urn:miriam:hgnc:14498;urn:miriam:hgnc.symbol:MRPS5;urn:miriam:hgnc.symbol:MRPS5;urn:miriam:refseq:NM_031902;urn:miriam:ncbigene:64969;urn:miriam:ncbigene:64969"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:MRPS2;HGNC_SYMBOL:MRPS5"
      map_id "M115_9"
      name "MRPScomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa106"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q9Y399;UNIPROT:P82675"
    ]
    graphics [
      x 2512.5144998161113
      y 1846.4538334694291
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:EXOSC2;urn:miriam:hgnc.symbol:EXOSC2;urn:miriam:refseq:NM_014285;urn:miriam:uniprot:Q13868;urn:miriam:hgnc:17097;urn:miriam:ncbigene:23404;urn:miriam:ensembl:ENSG00000130713;urn:miriam:ncbigene:23404"
      hgnc "HGNC_SYMBOL:EXOSC2"
      map_id "M115_326"
      name "EXOSC2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1322"
      uniprot "UNIPROT:Q13868"
    ]
    graphics [
      x 1930.4146427238684
      y 2170.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_326"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:EXOSC3;urn:miriam:hgnc.symbol:EXOSC3;urn:miriam:uniprot:Q9NQT5;urn:miriam:hgnc:17944;urn:miriam:refseq:NM_016042;urn:miriam:ncbigene:51010;urn:miriam:ensembl:ENSG00000107371;urn:miriam:ncbigene:51010"
      hgnc "HGNC_SYMBOL:EXOSC3"
      map_id "M115_330"
      name "EXOSC3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1333"
      uniprot "UNIPROT:Q9NQT5"
    ]
    graphics [
      x 1836.1225433048294
      y 2587.0228321916534
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_330"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:Q9NQT4;urn:miriam:hgnc.symbol:EXOSC5;urn:miriam:hgnc.symbol:EXOSC5;urn:miriam:hgnc:24662;urn:miriam:refseq:NM_020158;urn:miriam:ensembl:ENSG00000077348;urn:miriam:ncbigene:56915;urn:miriam:ncbigene:56915"
      hgnc "HGNC_SYMBOL:EXOSC5"
      map_id "M115_331"
      name "EXOSC5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1335"
      uniprot "UNIPROT:Q9NQT4"
    ]
    graphics [
      x 1851.6104592494069
      y 1810.950805484014
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_331"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:17035;urn:miriam:ensembl:ENSG00000120699;urn:miriam:ncbigene:11340;urn:miriam:ncbigene:11340;urn:miriam:hgnc.symbol:EXOSC8;urn:miriam:hgnc.symbol:EXOSC8;urn:miriam:refseq:NM_181503;urn:miriam:uniprot:Q96B26"
      hgnc "HGNC_SYMBOL:EXOSC8"
      map_id "M115_338"
      name "EXOSC8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1356"
      uniprot "UNIPROT:Q96B26"
    ]
    graphics [
      x 1351.6305234852287
      y 2682.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_338"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:pubmed:28514442;urn:miriam:hgnc.symbol:EXOSC3;urn:miriam:hgnc.symbol:EXOSC3;urn:miriam:uniprot:Q9NQT5;urn:miriam:hgnc:17944;urn:miriam:refseq:NM_016042;urn:miriam:ncbigene:51010;urn:miriam:ensembl:ENSG00000107371;urn:miriam:ncbigene:51010;urn:miriam:hgnc.symbol:EXOSC2;urn:miriam:hgnc.symbol:EXOSC2;urn:miriam:refseq:NM_014285;urn:miriam:uniprot:Q13868;urn:miriam:hgnc:17097;urn:miriam:ncbigene:23404;urn:miriam:ensembl:ENSG00000130713;urn:miriam:ncbigene:23404;urn:miriam:hgnc:17035;urn:miriam:ensembl:ENSG00000120699;urn:miriam:ncbigene:11340;urn:miriam:ncbigene:11340;urn:miriam:hgnc.symbol:EXOSC8;urn:miriam:hgnc.symbol:EXOSC8;urn:miriam:refseq:NM_181503;urn:miriam:uniprot:Q96B26;urn:miriam:uniprot:Q9NQT4;urn:miriam:hgnc.symbol:EXOSC5;urn:miriam:hgnc.symbol:EXOSC5;urn:miriam:hgnc:24662;urn:miriam:refseq:NM_020158;urn:miriam:ensembl:ENSG00000077348;urn:miriam:ncbigene:56915;urn:miriam:ncbigene:56915;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:EXOSC3;HGNC_SYMBOL:EXOSC2;HGNC_SYMBOL:EXOSC8;HGNC_SYMBOL:EXOSC5;HGNC_SYMBOL:rep"
      map_id "M115_8"
      name "EXOCcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa105"
      uniprot "UNIPROT:Q9NQT5;UNIPROT:Q13868;UNIPROT:Q96B26;UNIPROT:Q9NQT4;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1280.5824814748505
      y 2712.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:MEPCE;urn:miriam:hgnc.symbol:MEPCE;urn:miriam:ensembl:ENSG00000146834;urn:miriam:ec-code:2.1.1.-;urn:miriam:hgnc:20247;urn:miriam:ncbigene:56257;urn:miriam:ncbigene:56257;urn:miriam:refseq:NM_001194990;urn:miriam:uniprot:Q7L2J0"
      hgnc "HGNC_SYMBOL:MEPCE"
      map_id "M115_340"
      name "MEPCE"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1361"
      uniprot "UNIPROT:Q7L2J0"
    ]
    graphics [
      x 1391.4760702265748
      y 1062.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_340"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:24912;urn:miriam:ensembl:ENSG00000174720;urn:miriam:hgnc.symbol:LARP7;urn:miriam:ncbigene:51574;urn:miriam:hgnc.symbol:LARP7;urn:miriam:ncbigene:51574;urn:miriam:uniprot:Q4G0J3;urn:miriam:refseq:NM_016648"
      hgnc "HGNC_SYMBOL:LARP7"
      map_id "M115_341"
      name "LARP7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1364"
      uniprot "UNIPROT:Q4G0J3"
    ]
    graphics [
      x 1088.238667383822
      y 480.55431331226714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_341"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:29845934;urn:miriam:hgnc.symbol:MEPCE;urn:miriam:hgnc.symbol:MEPCE;urn:miriam:ensembl:ENSG00000146834;urn:miriam:ec-code:2.1.1.-;urn:miriam:hgnc:20247;urn:miriam:ncbigene:56257;urn:miriam:ncbigene:56257;urn:miriam:refseq:NM_001194990;urn:miriam:uniprot:Q7L2J0;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:hgnc:24912;urn:miriam:ensembl:ENSG00000174720;urn:miriam:hgnc.symbol:LARP7;urn:miriam:ncbigene:51574;urn:miriam:hgnc.symbol:LARP7;urn:miriam:ncbigene:51574;urn:miriam:uniprot:Q4G0J3;urn:miriam:refseq:NM_016648"
      hgnc "HGNC_SYMBOL:MEPCE;HGNC_SYMBOL:rep;HGNC_SYMBOL:LARP7"
      map_id "M115_18"
      name "MEPCEcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa115"
      uniprot "UNIPROT:Q7L2J0;UNIPROT:P0DTD1;UNIPROT:Q4G0J3"
    ]
    graphics [
      x 1238.8536650413898
      y 757.8410579313129
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:25983;urn:miriam:ncbigene:25983;urn:miriam:uniprot:Q8NEJ9;urn:miriam:hgnc.symbol:NGDN;urn:miriam:hgnc.symbol:NGDN;urn:miriam:refseq:NM_001042635;urn:miriam:hgnc:20271;urn:miriam:ensembl:ENSG00000129460"
      hgnc "HGNC_SYMBOL:NGDN"
      map_id "M115_339"
      name "NGDN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1358"
      uniprot "UNIPROT:Q8NEJ9"
    ]
    graphics [
      x 341.47607022657485
      y 1246.400744015673
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_339"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ncbigene:25983;urn:miriam:ncbigene:25983;urn:miriam:uniprot:Q8NEJ9;urn:miriam:hgnc.symbol:NGDN;urn:miriam:hgnc.symbol:NGDN;urn:miriam:refseq:NM_001042635;urn:miriam:hgnc:20271;urn:miriam:ensembl:ENSG00000129460"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:NGDN"
      map_id "M115_17"
      name "NGDNcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa114"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q8NEJ9"
    ]
    graphics [
      x 1661.4760702265748
      y 1305.3879570411846
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:2.9.1.2;urn:miriam:hgnc:30605;urn:miriam:uniprot:Q9HD40;urn:miriam:hgnc.symbol:SEPSECS;urn:miriam:ncbigene:51091;urn:miriam:refseq:NM_016955;urn:miriam:ensembl:ENSG00000109618;urn:miriam:hgnc.symbol:SEPSECS;urn:miriam:ncbigene:51091"
      hgnc "HGNC_SYMBOL:SEPSECS"
      map_id "M115_332"
      name "SEPSECS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1337"
      uniprot "UNIPROT:Q9HD40"
    ]
    graphics [
      x 1448.0206575778361
      y 252.06502830998443
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_332"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:2.9.1.2;urn:miriam:hgnc:30605;urn:miriam:uniprot:Q9HD40;urn:miriam:hgnc.symbol:SEPSECS;urn:miriam:ncbigene:51091;urn:miriam:refseq:NM_016955;urn:miriam:ensembl:ENSG00000109618;urn:miriam:hgnc.symbol:SEPSECS;urn:miriam:ncbigene:51091;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:SEPSECS;HGNC_SYMBOL:rep"
      map_id "M115_11"
      name "SEPSECScomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa108"
      uniprot "UNIPROT:Q9HD40;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1988.8536650413898
      y 637.7819934294499
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      annotation "PUBMED:17194211;PUBMED:11481605"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_212"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10191"
      uniprot "NA"
    ]
    graphics [
      x 1568.8536650413898
      y 582.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_212"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18405;urn:miriam:pubchem.compound:1051"
      hgnc "NA"
      map_id "M115_333"
      name "Pyridoxal_space_phosphate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa1340"
      uniprot "NA"
    ]
    graphics [
      x 1328.8536650413898
      y 672.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_333"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:17194211;urn:miriam:obo.chebi:CHEBI%3A18405;urn:miriam:pubchem.compound:1051;urn:miriam:ec-code:2.9.1.2;urn:miriam:hgnc:30605;urn:miriam:uniprot:Q9HD40;urn:miriam:hgnc.symbol:SEPSECS;urn:miriam:ncbigene:51091;urn:miriam:refseq:NM_016955;urn:miriam:ensembl:ENSG00000109618;urn:miriam:hgnc.symbol:SEPSECS;urn:miriam:ncbigene:51091"
      hgnc "HGNC_SYMBOL:SEPSECS"
      map_id "M115_12"
      name "SPcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa109"
      uniprot "UNIPROT:Q9HD40"
    ]
    graphics [
      x 2138.85366504139
      y 730.3339145171445
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:19153232;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_99"
      name "homodimer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa83"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1392.680593163498
      y 282.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      annotation "PUBMED:19153232"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_186"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10165"
      uniprot "NA"
    ]
    graphics [
      x 2041.2966489538196
      y 210.43038295457632
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_186"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:19153232"
      hgnc "NA"
      map_id "M115_306"
      name "ssRNAoligo"
      node_subtype "RNA"
      node_type "species"
      org_id "sa1253"
      uniprot "NA"
    ]
    graphics [
      x 2234.13999194938
      y 386.57049515366975
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_306"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:19153232;urn:miriam:pubmed:19153232;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_100"
      name "RNArecognition"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa84"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1568.8536650413898
      y 672.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:10644686;urn:miriam:pubmed:23425511;urn:miriam:pubmed:25075345;urn:miriam:ncbigene:27430;urn:miriam:ncbigene:27430;urn:miriam:uniprot:Q9NZL9;urn:miriam:hgnc.symbol:MAT2B;urn:miriam:hgnc.symbol:MAT2B;urn:miriam:pubmed:23189196;urn:miriam:refseq:NM_013283;urn:miriam:ensembl:ENSG00000038274;urn:miriam:hgnc:6905"
      hgnc "HGNC_SYMBOL:MAT2B"
      map_id "M115_376"
      name "MAT2B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa962"
      uniprot "UNIPROT:Q9NZL9"
    ]
    graphics [
      x 2797.9673850663708
      y 1602.0043406416269
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_376"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:pubmed:10644686;urn:miriam:pubmed:23425511;urn:miriam:pubmed:25075345;urn:miriam:ncbigene:27430;urn:miriam:ncbigene:27430;urn:miriam:uniprot:Q9NZL9;urn:miriam:hgnc.symbol:MAT2B;urn:miriam:hgnc.symbol:MAT2B;urn:miriam:pubmed:23189196;urn:miriam:refseq:NM_013283;urn:miriam:ensembl:ENSG00000038274;urn:miriam:hgnc:6905"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:MAT2B"
      map_id "M115_36"
      name "mat2bcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa15"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q9NZL9"
    ]
    graphics [
      x 1899.8874714005258
      y 1960.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      annotation "PUBMED:25544563"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_129"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10105"
      uniprot "NA"
    ]
    graphics [
      x 2651.476070226575
      y 1134.8561281759994
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_129"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      annotation "PUBMED:21516116;PUBMED:12671891;PUBMED:26186194;PUBMED:28514442;PUBMED:27548429;PUBMED:25416956;PUBMED:22863883"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_125"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10097"
      uniprot "NA"
    ]
    graphics [
      x 1841.4760702265748
      y 1526.5104349591088
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_125"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:2.5.1.6;urn:miriam:ensembl:ENSG00000151224;urn:miriam:hgnc:6903;urn:miriam:hgnc.symbol:MAT1A;urn:miriam:hgnc.symbol:MAT1A;urn:miriam:ncbigene:4143;urn:miriam:ncbigene:4143;urn:miriam:uniprot:Q00266;urn:miriam:refseq:NM_000429"
      hgnc "HGNC_SYMBOL:MAT1A"
      map_id "M115_266"
      name "MAT1A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1123"
      uniprot "UNIPROT:Q00266"
    ]
    graphics [
      x 2438.85366504139
      y 837.0726208995825
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_266"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:P31153;urn:miriam:ec-code:2.5.1.6;urn:miriam:ensembl:ENSG00000168906;urn:miriam:hgnc:6904;urn:miriam:hgnc.symbol:MAT2A;urn:miriam:hgnc.symbol:MAT2A;urn:miriam:ncbigene:4144;urn:miriam:ncbigene:4144;urn:miriam:refseq:NM_005911"
      hgnc "HGNC_SYMBOL:MAT2A"
      map_id "M115_265"
      name "MAT2A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1122"
      uniprot "UNIPROT:P31153"
    ]
    graphics [
      x 561.6104592494069
      y 1904.8967611372123
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_265"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:12671891;urn:miriam:uniprot:P31153;urn:miriam:ec-code:2.5.1.6;urn:miriam:ensembl:ENSG00000168906;urn:miriam:hgnc:6904;urn:miriam:hgnc.symbol:MAT2A;urn:miriam:hgnc.symbol:MAT2A;urn:miriam:ncbigene:4144;urn:miriam:ncbigene:4144;urn:miriam:refseq:NM_005911;urn:miriam:ec-code:2.5.1.6;urn:miriam:ensembl:ENSG00000151224;urn:miriam:hgnc:6903;urn:miriam:hgnc.symbol:MAT1A;urn:miriam:hgnc.symbol:MAT1A;urn:miriam:ncbigene:4143;urn:miriam:ncbigene:4143;urn:miriam:uniprot:Q00266;urn:miriam:refseq:NM_000429;urn:miriam:pubmed:10644686;urn:miriam:pubmed:23425511;urn:miriam:pubmed:25075345;urn:miriam:ncbigene:27430;urn:miriam:ncbigene:27430;urn:miriam:uniprot:Q9NZL9;urn:miriam:hgnc.symbol:MAT2B;urn:miriam:hgnc.symbol:MAT2B;urn:miriam:pubmed:23189196;urn:miriam:refseq:NM_013283;urn:miriam:ensembl:ENSG00000038274;urn:miriam:hgnc:6905"
      hgnc "HGNC_SYMBOL:MAT2A;HGNC_SYMBOL:MAT1A;HGNC_SYMBOL:MAT2B"
      map_id "M115_62"
      name "MAT"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa47"
      uniprot "UNIPROT:P31153;UNIPROT:Q00266;UNIPROT:Q9NZL9"
    ]
    graphics [
      x 1901.4760702265748
      y 1171.4707703304423
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      annotation "PUBMED:12023972;PUBMED:12660248;PUBMED:11596649"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_155"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10132"
      uniprot "NA"
    ]
    graphics [
      x 371.47607022657485
      y 1453.2890912784148
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_155"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:34755"
      hgnc "NA"
      map_id "M115_273"
      name "S_minus_Adenosylmethionine"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1146"
      uniprot "NA"
    ]
    graphics [
      x 1211.4760702265748
      y 1089.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_273"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:12023972;urn:miriam:pubmed:12660248;urn:miriam:pubmed:11596649;urn:miriam:pubchem.compound:34755;urn:miriam:uniprot:P31153;urn:miriam:ec-code:2.5.1.6;urn:miriam:ensembl:ENSG00000168906;urn:miriam:hgnc:6904;urn:miriam:hgnc.symbol:MAT2A;urn:miriam:hgnc.symbol:MAT2A;urn:miriam:ncbigene:4144;urn:miriam:ncbigene:4144;urn:miriam:refseq:NM_005911"
      hgnc "HGNC_SYMBOL:MAT2A"
      map_id "M115_70"
      name "SAdComp2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa55"
      uniprot "UNIPROT:P31153"
    ]
    graphics [
      x 1181.4760702265748
      y 1381.4212669072997
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      annotation "PUBMED:12060674;PUBMED:11301045;PUBMED:12660248;PUBMED:12631701"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_153"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10130"
      uniprot "NA"
    ]
    graphics [
      x 2228.85366504139
      y 899.8465236387651
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_153"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:12060674;urn:miriam:pubmed:11301045;urn:miriam:pubmed:12660248;urn:miriam:pubmed:12631701;urn:miriam:ec-code:2.5.1.6;urn:miriam:ensembl:ENSG00000151224;urn:miriam:hgnc:6903;urn:miriam:hgnc.symbol:MAT1A;urn:miriam:hgnc.symbol:MAT1A;urn:miriam:ncbigene:4143;urn:miriam:ncbigene:4143;urn:miriam:uniprot:Q00266;urn:miriam:refseq:NM_000429;urn:miriam:pubchem.compound:34755"
      hgnc "HGNC_SYMBOL:MAT1A"
      map_id "M115_68"
      name "SAdComp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa53"
      uniprot "UNIPROT:Q00266"
    ]
    graphics [
      x 1991.4760702265748
      y 1144.8841075166547
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:8743;urn:miriam:uniprot:P29120;urn:miriam:uniprot:P29120;urn:miriam:hgnc.symbol:PCSK1;urn:miriam:hgnc.symbol:PCSK1;urn:miriam:ensembl:ENSG00000175426;urn:miriam:ec-code:3.4.21.93;urn:miriam:ncbigene:5122;urn:miriam:refseq:NM_000439;urn:miriam:ncbigene:5122"
      hgnc "HGNC_SYMBOL:PCSK1"
      map_id "M115_268"
      name "PCSK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1132"
      uniprot "UNIPROT:P29120"
    ]
    graphics [
      x 2711.476070226575
      y 1466.5142649245092
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_268"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:25544563;urn:miriam:pubmed:10644686;urn:miriam:pubmed:23425511;urn:miriam:pubmed:25075345;urn:miriam:ncbigene:27430;urn:miriam:ncbigene:27430;urn:miriam:uniprot:Q9NZL9;urn:miriam:hgnc.symbol:MAT2B;urn:miriam:hgnc.symbol:MAT2B;urn:miriam:pubmed:23189196;urn:miriam:refseq:NM_013283;urn:miriam:ensembl:ENSG00000038274;urn:miriam:hgnc:6905;urn:miriam:hgnc:8743;urn:miriam:uniprot:P29120;urn:miriam:uniprot:P29120;urn:miriam:hgnc.symbol:PCSK1;urn:miriam:hgnc.symbol:PCSK1;urn:miriam:ensembl:ENSG00000175426;urn:miriam:ec-code:3.4.21.93;urn:miriam:ncbigene:5122;urn:miriam:refseq:NM_000439;urn:miriam:ncbigene:5122"
      hgnc "HGNC_SYMBOL:MAT2B;HGNC_SYMBOL:PCSK1"
      map_id "M115_64"
      name "NEC1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa49"
      uniprot "UNIPROT:Q9NZL9;UNIPROT:P29120"
    ]
    graphics [
      x 1538.8536650413898
      y 672.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      annotation "PUBMED:8940009"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_157"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10134"
      uniprot "NA"
    ]
    graphics [
      x 2078.85366504139
      y 624.8606125107739
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_157"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      annotation "PUBMED:17016423;PUBMED:17139284"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_156"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10133"
      uniprot "NA"
    ]
    graphics [
      x 2648.85366504139
      y 1021.9658073952011
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_156"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:INS;urn:miriam:ncbigene:3630;urn:miriam:uniprot:P01308"
      hgnc "HGNC_SYMBOL:INS"
      map_id "M115_276"
      name "Insulin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1160"
      uniprot "UNIPROT:P01308"
    ]
    graphics [
      x 2681.476070226575
      y 1354.3347986776794
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_276"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:17016423;urn:miriam:pubmed:17139284;urn:miriam:hgnc.symbol:INS;urn:miriam:ncbigene:3630;urn:miriam:uniprot:P01308;urn:miriam:hgnc:8743;urn:miriam:uniprot:P29120;urn:miriam:uniprot:P29120;urn:miriam:hgnc.symbol:PCSK1;urn:miriam:hgnc.symbol:PCSK1;urn:miriam:ensembl:ENSG00000175426;urn:miriam:ec-code:3.4.21.93;urn:miriam:ncbigene:5122;urn:miriam:refseq:NM_000439;urn:miriam:ncbigene:5122"
      hgnc "HGNC_SYMBOL:INS;HGNC_SYMBOL:PCSK1"
      map_id "M115_71"
      name "NECINsComp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa56"
      uniprot "UNIPROT:P01308;UNIPROT:P29120"
    ]
    graphics [
      x 1598.8536650413898
      y 732.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:env;urn:miriam:uniprot:A0A517FIL8;urn:miriam:taxonomy:11676"
      hgnc "HGNC_SYMBOL:env"
      map_id "M115_277"
      name "ENV"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1163"
      uniprot "UNIPROT:A0A517FIL8"
    ]
    graphics [
      x 2011.2966489538196
      y 203.05632154972682
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_277"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:8940009;urn:miriam:hgnc:8743;urn:miriam:uniprot:P29120;urn:miriam:uniprot:P29120;urn:miriam:hgnc.symbol:PCSK1;urn:miriam:hgnc.symbol:PCSK1;urn:miriam:ensembl:ENSG00000175426;urn:miriam:ec-code:3.4.21.93;urn:miriam:ncbigene:5122;urn:miriam:refseq:NM_000439;urn:miriam:ncbigene:5122;urn:miriam:hgnc.symbol:env;urn:miriam:uniprot:A0A517FIL8;urn:miriam:taxonomy:11676"
      hgnc "HGNC_SYMBOL:PCSK1;HGNC_SYMBOL:env"
      map_id "M115_72"
      name "NECENVComp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa57"
      uniprot "UNIPROT:P29120;UNIPROT:A0A517FIL8"
    ]
    graphics [
      x 1811.4760702265748
      y 1354.8138321341212
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000166147;urn:miriam:uniprot:P35555;urn:miriam:uniprot:P35555;urn:miriam:hgnc.symbol:FBN1;urn:miriam:hgnc.symbol:FBN1;urn:miriam:hgnc:3603;urn:miriam:ncbigene:2200;urn:miriam:ncbigene:2200;urn:miriam:refseq:NM_000138"
      hgnc "HGNC_SYMBOL:FBN1"
      map_id "M115_378"
      name "FBN1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa965"
      uniprot "UNIPROT:P35555"
    ]
    graphics [
      x 2360.606315757607
      y 2168.221696520471
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_378"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:FBLN5;urn:miriam:refseq:NM_001384158;urn:miriam:hgnc.symbol:FBLN5;urn:miriam:hgnc:3602;urn:miriam:ensembl:ENSG00000140092;urn:miriam:uniprot:Q9UBX5;urn:miriam:uniprot:Q9UBX5;urn:miriam:ncbigene:10516;urn:miriam:ncbigene:10516"
      hgnc "HGNC_SYMBOL:FBLN5"
      map_id "M115_270"
      name "FBLN5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1138"
      uniprot "UNIPROT:Q9UBX5"
    ]
    graphics [
      x 2348.85366504139
      y 961.3530460448193
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_270"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:PLG;urn:miriam:hgnc.symbol:PLG;urn:miriam:ec-code:3.4.21.7;urn:miriam:ensembl:ENSG00000122194;urn:miriam:ncbigene:5340;urn:miriam:ncbigene:5340;urn:miriam:hgnc:9071;urn:miriam:refseq:NM_000301;urn:miriam:uniprot:P00747;urn:miriam:uniprot:P00747"
      hgnc "HGNC_SYMBOL:PLG"
      map_id "M115_373"
      name "PLG"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1433"
      uniprot "UNIPROT:P00747"
    ]
    graphics [
      x 1553.016277435571
      y 1032.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_373"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:hgnc.symbol:FBLN5;urn:miriam:refseq:NM_001384158;urn:miriam:hgnc.symbol:FBLN5;urn:miriam:hgnc:3602;urn:miriam:ensembl:ENSG00000140092;urn:miriam:uniprot:Q9UBX5;urn:miriam:uniprot:Q9UBX5;urn:miriam:ncbigene:10516;urn:miriam:ncbigene:10516;urn:miriam:ensembl:ENSG00000166147;urn:miriam:uniprot:P35555;urn:miriam:uniprot:P35555;urn:miriam:hgnc.symbol:FBN1;urn:miriam:hgnc.symbol:FBN1;urn:miriam:hgnc:3603;urn:miriam:ncbigene:2200;urn:miriam:ncbigene:2200;urn:miriam:refseq:NM_000138;urn:miriam:hgnc.symbol:FBLN2;urn:miriam:hgnc.symbol:FBLN2;urn:miriam:uniprot:P98095;urn:miriam:uniprot:P98095;urn:miriam:refseq:NM_001004019;urn:miriam:ensembl:ENSG00000163520;urn:miriam:hgnc:3601;urn:miriam:ncbigene:2199;urn:miriam:ncbigene:2199;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:FBLN5;HGNC_SYMBOL:FBN1;HGNC_SYMBOL:FBLN2;HGNC_SYMBOL:rep"
      map_id "M115_38"
      name "Fibrillincomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa17"
      uniprot "UNIPROT:Q9UBX5;UNIPROT:P35555;UNIPROT:P98095;UNIPROT:P0DTD1"
    ]
    graphics [
      x 2317.9673850663708
      y 1688.8953058816028
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      annotation "PUBMED:5006793"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_236"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10215"
      uniprot "NA"
    ]
    graphics [
      x 1404.0407232757284
      y 1782.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_236"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      annotation "PUBMED:21001709;PUBMED:14745449"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_126"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10102"
      uniprot "NA"
    ]
    graphics [
      x 1401.6104592494069
      y 1722.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      annotation "PUBMED:21001709;PUBMED:10544250;PUBMED:10825173"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_152"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10128"
      uniprot "NA"
    ]
    graphics [
      x 878.8536650413897
      y 588.4951717093902
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_152"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:8702639;urn:miriam:hgnc.symbol:FBLN2;urn:miriam:hgnc.symbol:FBLN2;urn:miriam:uniprot:P98095;urn:miriam:uniprot:P98095;urn:miriam:refseq:NM_001004019;urn:miriam:ensembl:ENSG00000163520;urn:miriam:hgnc:3601;urn:miriam:ncbigene:2199;urn:miriam:ncbigene:2199;urn:miriam:ensembl:ENSG00000166147;urn:miriam:uniprot:P35555;urn:miriam:uniprot:P35555;urn:miriam:hgnc.symbol:FBN1;urn:miriam:hgnc.symbol:FBN1;urn:miriam:hgnc:3603;urn:miriam:ncbigene:2200;urn:miriam:ncbigene:2200;urn:miriam:refseq:NM_000138"
      hgnc "HGNC_SYMBOL:FBLN2;HGNC_SYMBOL:FBN1"
      map_id "M115_95"
      name "Fibrillin"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa8"
      uniprot "UNIPROT:P98095;UNIPROT:P35555"
    ]
    graphics [
      x 1331.4760702265748
      y 1452.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:2006;urn:miriam:ncbigene:2006;urn:miriam:uniprot:P15502;urn:miriam:hgnc:3327;urn:miriam:ensembl:ENSG00000049540;urn:miriam:hgnc.symbol:ELN;urn:miriam:hgnc.symbol:ELN;urn:miriam:refseq:NM_000501"
      hgnc "HGNC_SYMBOL:ELN"
      map_id "M115_271"
      name "ELN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1139"
      uniprot "UNIPROT:P15502"
    ]
    graphics [
      x 881.9871269542564
      y 332.0586106524322
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_271"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:8702639;urn:miriam:pubmed:10544250;urn:miriam:pubmed:10825173;urn:miriam:ensembl:ENSG00000166147;urn:miriam:uniprot:P35555;urn:miriam:uniprot:P35555;urn:miriam:hgnc.symbol:FBN1;urn:miriam:hgnc.symbol:FBN1;urn:miriam:hgnc:3603;urn:miriam:ncbigene:2200;urn:miriam:ncbigene:2200;urn:miriam:refseq:NM_000138;urn:miriam:ncbigene:2006;urn:miriam:ncbigene:2006;urn:miriam:uniprot:P15502;urn:miriam:hgnc:3327;urn:miriam:ensembl:ENSG00000049540;urn:miriam:hgnc.symbol:ELN;urn:miriam:hgnc.symbol:ELN;urn:miriam:refseq:NM_000501;urn:miriam:hgnc.symbol:FBLN2;urn:miriam:hgnc.symbol:FBLN2;urn:miriam:uniprot:P98095;urn:miriam:uniprot:P98095;urn:miriam:refseq:NM_001004019;urn:miriam:ensembl:ENSG00000163520;urn:miriam:hgnc:3601;urn:miriam:ncbigene:2199;urn:miriam:ncbigene:2199"
      hgnc "HGNC_SYMBOL:FBN1;HGNC_SYMBOL:ELN;HGNC_SYMBOL:FBLN2"
      map_id "M115_66"
      name "Fibrillin"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa51"
      uniprot "UNIPROT:P35555;UNIPROT:P15502;UNIPROT:P98095"
    ]
    graphics [
      x 515.028489728988
      y 726.5540586052853
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      annotation "PUBMED:17620346;PUBMED:16679386"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_163"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10140"
      uniprot "NA"
    ]
    graphics [
      x 1442.112491134918
      y 792.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_163"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 167
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:5090"
      hgnc "NA"
      map_id "M115_272"
      name "Rofecoxib"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1143"
      uniprot "NA"
    ]
    graphics [
      x 1601.7832061129418
      y 282.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_272"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 168
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:17620346;urn:miriam:pubmed:16679386;urn:miriam:ncbigene:2006;urn:miriam:ncbigene:2006;urn:miriam:uniprot:P15502;urn:miriam:hgnc:3327;urn:miriam:ensembl:ENSG00000049540;urn:miriam:hgnc.symbol:ELN;urn:miriam:hgnc.symbol:ELN;urn:miriam:refseq:NM_000501;urn:miriam:pubchem.compound:5090"
      hgnc "HGNC_SYMBOL:ELN"
      map_id "M115_67"
      name "RofecoxibComp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa52"
      uniprot "UNIPROT:P15502"
    ]
    graphics [
      x 2021.4760702265748
      y 1120.9809222758117
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 169
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:6665;urn:miriam:refseq:NM_005576;urn:miriam:ensembl:ENSG00000129038;urn:miriam:uniprot:Q08397;urn:miriam:hgnc.symbol:LOXL1;urn:miriam:hgnc.symbol:LOXL1;urn:miriam:ec-code:1.4.3.-;urn:miriam:ncbigene:4016;urn:miriam:ncbigene:4016"
      hgnc "HGNC_SYMBOL:LOXL1"
      map_id "M115_267"
      name "LOXL1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1127"
      uniprot "UNIPROT:Q08397"
    ]
    graphics [
      x 892.5144998161113
      y 2084.413146423103
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_267"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 170
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:14745449;urn:miriam:hgnc.symbol:FBLN5;urn:miriam:refseq:NM_001384158;urn:miriam:hgnc.symbol:FBLN5;urn:miriam:hgnc:3602;urn:miriam:ensembl:ENSG00000140092;urn:miriam:uniprot:Q9UBX5;urn:miriam:uniprot:Q9UBX5;urn:miriam:ncbigene:10516;urn:miriam:ncbigene:10516;urn:miriam:hgnc:6665;urn:miriam:refseq:NM_005576;urn:miriam:ensembl:ENSG00000129038;urn:miriam:uniprot:Q08397;urn:miriam:hgnc.symbol:LOXL1;urn:miriam:hgnc.symbol:LOXL1;urn:miriam:ec-code:1.4.3.-;urn:miriam:ncbigene:4016;urn:miriam:ncbigene:4016"
      hgnc "HGNC_SYMBOL:FBLN5;HGNC_SYMBOL:LOXL1"
      map_id "M115_63"
      name "LOXcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa48"
      uniprot "UNIPROT:Q9UBX5;UNIPROT:Q08397"
    ]
    graphics [
      x 1149.0903364967949
      y 2292.3418650109697
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 171
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:PLG;urn:miriam:hgnc.symbol:PLG;urn:miriam:ec-code:3.4.21.7;urn:miriam:ensembl:ENSG00000122194;urn:miriam:ncbigene:5340;urn:miriam:ncbigene:5340;urn:miriam:hgnc:9071;urn:miriam:refseq:NM_000301;urn:miriam:uniprot:P00747;urn:miriam:uniprot:P00747"
      hgnc "HGNC_SYMBOL:PLG"
      map_id "M115_371"
      name "PLG"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1431"
      uniprot "UNIPROT:P00747"
    ]
    graphics [
      x 1972.5144998161113
      y 1967.606843746084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_371"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 172
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:3.4.21.68;urn:miriam:ensembl:ENSG00000104368;urn:miriam:hgnc:9051;urn:miriam:hgnc.symbol:PLAT;urn:miriam:hgnc.symbol:PLAT;urn:miriam:uniprot:P00750;urn:miriam:uniprot:P00750;urn:miriam:ncbigene:5327;urn:miriam:ncbigene:5327;urn:miriam:refseq:NM_000930"
      hgnc "HGNC_SYMBOL:PLAT"
      map_id "M115_372"
      name "PLAT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1432"
      uniprot "UNIPROT:P00750"
    ]
    graphics [
      x 1117.9673850663708
      y 1632.154598018466
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_372"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 173
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32620147;urn:miriam:pubchem.compound:4413"
      hgnc "NA"
      map_id "M115_374"
      name "Nafamostat"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1434"
      uniprot "NA"
    ]
    graphics [
      x 937.9673850663706
      y 1606.6019746441584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_374"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 174
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_225"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10204"
      uniprot "NA"
    ]
    graphics [
      x 428.8536650413897
      y 782.5572932182372
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_225"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 175
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_221"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10200"
      uniprot "NA"
    ]
    graphics [
      x 1871.4760702265748
      y 1476.456442012669
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_221"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 176
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:Q96CW1;urn:miriam:refseq:NM_004068;urn:miriam:hgnc.symbol:AP2M1;urn:miriam:hgnc.symbol:AP2M1;urn:miriam:ncbigene:1173;urn:miriam:ensembl:ENSG00000161203;urn:miriam:ncbigene:1173;urn:miriam:hgnc:564"
      hgnc "HGNC_SYMBOL:AP2M1"
      map_id "M115_348"
      name "AP2M1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1380"
      uniprot "UNIPROT:Q96CW1"
    ]
    graphics [
      x 2108.85366504139
      y 943.1270120257964
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_348"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 177
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:uniprot:Q96CW1;urn:miriam:refseq:NM_004068;urn:miriam:hgnc.symbol:AP2M1;urn:miriam:hgnc.symbol:AP2M1;urn:miriam:ncbigene:1173;urn:miriam:ensembl:ENSG00000161203;urn:miriam:ncbigene:1173;urn:miriam:hgnc:564;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:AP2M1;HGNC_SYMBOL:rep"
      map_id "M115_21"
      name "AP2M1comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa118"
      uniprot "UNIPROT:Q96CW1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1371.6104592494069
      y 1752.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 178
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:AP2A2;urn:miriam:hgnc.symbol:AP2A2;urn:miriam:uniprot:O94973;urn:miriam:refseq:NM_012305;urn:miriam:ncbigene:161;urn:miriam:ensembl:ENSG00000183020;urn:miriam:ncbigene:161;urn:miriam:hgnc:562"
      hgnc "HGNC_SYMBOL:AP2A2"
      map_id "M115_352"
      name "AP2A2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1392"
      uniprot "UNIPROT:O94973"
    ]
    graphics [
      x 311.47607022657485
      y 1484.399915765673
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_352"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 179
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:hgnc.symbol:AP2A2;urn:miriam:hgnc.symbol:AP2A2;urn:miriam:uniprot:O94973;urn:miriam:refseq:NM_012305;urn:miriam:ncbigene:161;urn:miriam:ensembl:ENSG00000183020;urn:miriam:ncbigene:161;urn:miriam:hgnc:562"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:AP2A2"
      map_id "M115_26"
      name "AP2A2comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa122"
      uniprot "UNIPROT:P0DTD1;UNIPROT:O94973"
    ]
    graphics [
      x 849.6688434433706
      y 384.13405527617147
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 180
    source 2
    target 1
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_187"
      target_id "M115_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 181
    source 1
    target 3
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_101"
      target_id "M115_193"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 182
    source 5
    target 2
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_307"
      target_id "M115_187"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 6
    target 2
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_1"
      target_id "M115_187"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 7
    target 2
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_345"
      target_id "M115_187"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 8
    target 2
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_346"
      target_id "M115_187"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 9
    target 2
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_347"
      target_id "M115_187"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 3
    target 4
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_193"
      target_id "M115_314"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 5
    target 174
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_307"
      target_id "M115_225"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 5
    target 175
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_307"
      target_id "M115_221"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 6
    target 19
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_1"
      target_id "M115_149"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 6
    target 20
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_1"
      target_id "M115_127"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 6
    target 21
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_1"
      target_id "M115_185"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 6
    target 22
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_1"
      target_id "M115_131"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 9
    target 10
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_347"
      target_id "M115_231"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 9
    target 11
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_347"
      target_id "M115_234"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 14
    target 10
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_358"
      target_id "M115_231"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 10
    target 15
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_231"
      target_id "M115_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 12
    target 11
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_361"
      target_id "M115_234"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 11
    target 13
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_234"
      target_id "M115_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 14
    target 16
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_358"
      target_id "M115_232"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 17
    target 16
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_359"
      target_id "M115_232"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 16
    target 18
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_232"
      target_id "M115_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 156
    target 19
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_378"
      target_id "M115_149"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 157
    target 19
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_270"
      target_id "M115_149"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 158
    target 19
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CATALYSIS"
      source_id "M115_373"
      target_id "M115_149"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 19
    target 159
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_149"
      target_id "M115_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 136
    target 20
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_376"
      target_id "M115_127"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 20
    target 137
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_127"
      target_id "M115_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 21
    target 132
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_185"
      target_id "M115_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 23
    target 22
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_385"
      target_id "M115_131"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 22
    target 24
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_131"
      target_id "M115_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 23
    target 25
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_385"
      target_id "M115_211"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 23
    target 26
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_385"
      target_id "M115_217"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 23
    target 27
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_385"
      target_id "M115_218"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 23
    target 28
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_385"
      target_id "M115_208"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 23
    target 29
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_385"
      target_id "M115_209"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 23
    target 30
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_385"
      target_id "M115_164"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 23
    target 31
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_385"
      target_id "M115_220"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 23
    target 32
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_385"
      target_id "M115_210"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 23
    target 33
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_385"
      target_id "M115_206"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 23
    target 34
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_385"
      target_id "M115_215"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 23
    target 35
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_385"
      target_id "M115_207"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 23
    target 36
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_385"
      target_id "M115_219"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 23
    target 37
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_385"
      target_id "M115_205"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 23
    target 38
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_385"
      target_id "M115_213"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 127
    target 25
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_332"
      target_id "M115_211"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 25
    target 128
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_211"
      target_id "M115_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 125
    target 26
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_339"
      target_id "M115_217"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 26
    target 126
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_217"
      target_id "M115_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 122
    target 27
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_340"
      target_id "M115_218"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 123
    target 27
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_341"
      target_id "M115_218"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 27
    target 124
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_218"
      target_id "M115_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 117
    target 28
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_326"
      target_id "M115_208"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 118
    target 28
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_330"
      target_id "M115_208"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 119
    target 28
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_331"
      target_id "M115_208"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 120
    target 28
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_338"
      target_id "M115_208"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 28
    target 121
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_208"
      target_id "M115_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 114
    target 29
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_327"
      target_id "M115_209"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 239
    source 115
    target 29
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_328"
      target_id "M115_209"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 240
    source 29
    target 116
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_209"
      target_id "M115_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 241
    source 46
    target 30
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_283"
      target_id "M115_164"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 242
    source 78
    target 30
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_284"
      target_id "M115_164"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 243
    source 30
    target 79
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_164"
      target_id "M115_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 244
    source 75
    target 31
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_343"
      target_id "M115_220"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 245
    source 76
    target 31
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_344"
      target_id "M115_220"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 246
    source 31
    target 77
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_220"
      target_id "M115_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 247
    source 73
    target 32
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_329"
      target_id "M115_210"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 248
    source 32
    target 74
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_210"
      target_id "M115_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 249
    source 71
    target 33
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_324"
      target_id "M115_206"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 250
    source 33
    target 72
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_206"
      target_id "M115_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 251
    source 66
    target 34
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_336"
      target_id "M115_215"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 252
    source 34
    target 67
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_215"
      target_id "M115_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 253
    source 45
    target 35
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_325"
      target_id "M115_207"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 254
    source 46
    target 35
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_283"
      target_id "M115_207"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 255
    source 35
    target 47
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_207"
      target_id "M115_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 256
    source 43
    target 36
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_342"
      target_id "M115_219"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 257
    source 36
    target 44
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_219"
      target_id "M115_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 258
    source 41
    target 37
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_323"
      target_id "M115_205"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 259
    source 37
    target 42
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_205"
      target_id "M115_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 260
    source 39
    target 38
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_334"
      target_id "M115_213"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 261
    source 38
    target 40
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_213"
      target_id "M115_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 262
    source 46
    target 48
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_283"
      target_id "M115_178"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 263
    source 49
    target 48
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_299"
      target_id "M115_178"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 264
    source 48
    target 50
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_178"
      target_id "M115_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 265
    source 49
    target 51
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_299"
      target_id "M115_179"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 266
    source 49
    target 52
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_299"
      target_id "M115_180"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 267
    source 49
    target 53
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_299"
      target_id "M115_181"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 268
    source 49
    target 54
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_299"
      target_id "M115_182"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 269
    source 49
    target 55
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_299"
      target_id "M115_183"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 270
    source 64
    target 51
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_300"
      target_id "M115_179"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 271
    source 51
    target 65
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_179"
      target_id "M115_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 272
    source 62
    target 52
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_301"
      target_id "M115_180"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 273
    source 52
    target 63
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_180"
      target_id "M115_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 274
    source 60
    target 53
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_302"
      target_id "M115_181"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 275
    source 53
    target 61
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_181"
      target_id "M115_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 276
    source 58
    target 54
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_303"
      target_id "M115_182"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 277
    source 54
    target 59
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_182"
      target_id "M115_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 278
    source 56
    target 55
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_304"
      target_id "M115_183"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 55
    target 57
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_183"
      target_id "M115_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 66
    target 68
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_336"
      target_id "M115_216"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 69
    target 68
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_337"
      target_id "M115_216"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 68
    target 70
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_216"
      target_id "M115_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 78
    target 87
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_284"
      target_id "M115_168"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 78
    target 88
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_284"
      target_id "M115_173"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 78
    target 89
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_284"
      target_id "M115_174"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 78
    target 90
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_284"
      target_id "M115_167"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 78
    target 91
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_284"
      target_id "M115_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 78
    target 92
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_284"
      target_id "M115_170"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 78
    target 93
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_284"
      target_id "M115_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 78
    target 94
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_284"
      target_id "M115_171"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 79
    target 80
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_78"
      target_id "M115_165"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 81
    target 80
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_286"
      target_id "M115_165"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 82
    target 80
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CATALYSIS"
      source_id "M115_285"
      target_id "M115_165"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 83
    target 80
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CATALYSIS"
      source_id "M115_375"
      target_id "M115_165"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 80
    target 84
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_165"
      target_id "M115_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 84
    target 85
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_79"
      target_id "M115_166"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 85
    target 86
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_166"
      target_id "M115_287"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 109
    target 87
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_289"
      target_id "M115_168"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 87
    target 110
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_168"
      target_id "M115_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 107
    target 88
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_294"
      target_id "M115_173"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 88
    target 108
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_173"
      target_id "M115_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 105
    target 89
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_295"
      target_id "M115_174"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 89
    target 106
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_174"
      target_id "M115_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 304
    source 103
    target 90
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_288"
      target_id "M115_167"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 305
    source 90
    target 104
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_167"
      target_id "M115_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 306
    source 101
    target 91
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_293"
      target_id "M115_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 307
    source 91
    target 102
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_172"
      target_id "M115_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 308
    source 99
    target 92
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_291"
      target_id "M115_170"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 309
    source 92
    target 100
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_170"
      target_id "M115_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 310
    source 97
    target 93
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_296"
      target_id "M115_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 311
    source 93
    target 98
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_175"
      target_id "M115_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 312
    source 95
    target 94
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_292"
      target_id "M115_171"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 313
    source 94
    target 96
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_171"
      target_id "M115_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 314
    source 109
    target 111
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_289"
      target_id "M115_169"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 315
    source 112
    target 111
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_290"
      target_id "M115_169"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 316
    source 111
    target 113
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_169"
      target_id "M115_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 317
    source 127
    target 129
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_332"
      target_id "M115_212"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 318
    source 130
    target 129
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_333"
      target_id "M115_212"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 319
    source 129
    target 131
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_212"
      target_id "M115_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 320
    source 132
    target 133
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_99"
      target_id "M115_186"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 321
    source 134
    target 133
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_306"
      target_id "M115_186"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 322
    source 133
    target 135
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_186"
      target_id "M115_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 323
    source 136
    target 138
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_376"
      target_id "M115_129"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 324
    source 136
    target 139
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_376"
      target_id "M115_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 325
    source 148
    target 138
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_268"
      target_id "M115_129"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 326
    source 138
    target 149
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_129"
      target_id "M115_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 327
    source 140
    target 139
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_266"
      target_id "M115_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 328
    source 141
    target 139
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_265"
      target_id "M115_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 329
    source 139
    target 142
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_125"
      target_id "M115_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 330
    source 140
    target 146
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_266"
      target_id "M115_153"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 331
    source 141
    target 143
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_265"
      target_id "M115_155"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 332
    source 144
    target 143
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_273"
      target_id "M115_155"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 333
    source 143
    target 145
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_155"
      target_id "M115_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 334
    source 144
    target 146
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_273"
      target_id "M115_153"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 335
    source 146
    target 147
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_153"
      target_id "M115_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 336
    source 148
    target 150
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_268"
      target_id "M115_157"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 337
    source 148
    target 151
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_268"
      target_id "M115_156"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 338
    source 154
    target 150
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_277"
      target_id "M115_157"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 339
    source 150
    target 155
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_157"
      target_id "M115_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 340
    source 152
    target 151
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_276"
      target_id "M115_156"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 341
    source 151
    target 153
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_156"
      target_id "M115_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 342
    source 157
    target 161
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_270"
      target_id "M115_126"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 343
    source 160
    target 158
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_236"
      target_id "M115_373"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 344
    source 158
    target 161
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CATALYSIS"
      source_id "M115_373"
      target_id "M115_126"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 345
    source 158
    target 162
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CATALYSIS"
      source_id "M115_373"
      target_id "M115_152"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 346
    source 171
    target 160
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_371"
      target_id "M115_236"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 347
    source 172
    target 160
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "M115_372"
      target_id "M115_236"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 348
    source 173
    target 160
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "M115_374"
      target_id "M115_236"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 349
    source 169
    target 161
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_267"
      target_id "M115_126"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 350
    source 161
    target 170
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_126"
      target_id "M115_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 351
    source 163
    target 162
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_95"
      target_id "M115_152"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 352
    source 164
    target 162
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_271"
      target_id "M115_152"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 353
    source 162
    target 165
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_152"
      target_id "M115_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 354
    source 164
    target 166
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_271"
      target_id "M115_163"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 355
    source 167
    target 166
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_272"
      target_id "M115_163"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 356
    source 166
    target 168
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_163"
      target_id "M115_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 357
    source 178
    target 174
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_352"
      target_id "M115_225"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 358
    source 174
    target 179
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_225"
      target_id "M115_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 359
    source 176
    target 175
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_348"
      target_id "M115_221"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 360
    source 175
    target 177
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_221"
      target_id "M115_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
