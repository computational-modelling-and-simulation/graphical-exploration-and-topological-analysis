# generated with VANTED V2.8.2 at Fri Mar 04 10:04:38 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:BCL2;urn:miriam:refseq:NM_000657;urn:miriam:ncbigene:596;urn:miriam:uniprot:P10415;urn:miriam:ensembl:ENSG00000171791;urn:miriam:refseq:NM_000633;urn:miriam:hgnc:990"
      hgnc "HGNC_SYMBOL:BCL2"
      map_id "M118_188"
      name "BCL2"
      node_subtype "GENE"
      node_type "species"
      org_id "path_0_sa55"
      uniprot "UNIPROT:P10415"
    ]
    graphics [
      x 1837.9673850663708
      y 1634.5829811232447
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_188"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:30662442;PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_85"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "path_0_re38"
      uniprot "NA"
    ]
    graphics [
      x 1175.693942958764
      y 2472.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:DDIT3;urn:miriam:ncbigene:1649;urn:miriam:ncbigene:1649;urn:miriam:uniprot:P35638;urn:miriam:uniprot:P35638;urn:miriam:hgnc:2726;urn:miriam:refseq:NM_004083;urn:miriam:ensembl:ENSG00000175197;urn:miriam:uniprot:P0DPQ6;urn:miriam:uniprot:P0DPQ6"
      hgnc "HGNC_SYMBOL:DDIT3"
      map_id "M118_175"
      name "DDIT3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa46"
      uniprot "UNIPROT:P35638;UNIPROT:P0DPQ6"
    ]
    graphics [
      x 802.5144998161113
      y 1887.6236763137715
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_175"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:BCL2;urn:miriam:refseq:NM_000657;urn:miriam:ncbigene:596;urn:miriam:uniprot:P10415;urn:miriam:ensembl:ENSG00000171791;urn:miriam:refseq:NM_000633;urn:miriam:hgnc:990"
      hgnc "HGNC_SYMBOL:BCL2"
      map_id "M118_187"
      name "BCL2"
      node_subtype "RNA"
      node_type "species"
      org_id "path_0_sa54"
      uniprot "UNIPROT:P10415"
    ]
    graphics [
      x 970.6095281573112
      y 2696.5089124167416
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_187"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "PUBMED:26137585;PUBMED:23850759;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_49"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re32"
      uniprot "NA"
    ]
    graphics [
      x 1365.5674546932569
      y 2052.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:DDIT3;urn:miriam:ncbigene:1649;urn:miriam:ncbigene:1649;urn:miriam:uniprot:P35638;urn:miriam:uniprot:P35638;urn:miriam:hgnc:2726;urn:miriam:refseq:NM_004083;urn:miriam:ensembl:ENSG00000175197;urn:miriam:uniprot:P0DPQ6;urn:miriam:uniprot:P0DPQ6"
      hgnc "HGNC_SYMBOL:DDIT3"
      map_id "M118_219"
      name "DDIT3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa69"
      uniprot "UNIPROT:P35638;UNIPROT:P0DPQ6"
    ]
    graphics [
      x 1901.4760702265748
      y 1420.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_219"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ec-code:2.7.11.24;urn:miriam:ensembl:ENSG00000112062;urn:miriam:hgnc:6876;urn:miriam:uniprot:Q16539;urn:miriam:uniprot:Q16539;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:ncbigene:1432;urn:miriam:refseq:NM_001315"
      hgnc "HGNC_SYMBOL:MAPK14"
      map_id "M118_209"
      name "MAPK14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa620"
      uniprot "UNIPROT:Q16539"
    ]
    graphics [
      x 1455.2810327361162
      y 2502.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_209"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "PUBMED:26137585;PUBMED:23850759;PUBMED:17991856"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_90"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "path_0_re49"
      uniprot "NA"
    ]
    graphics [
      x 1991.4760702265748
      y 1054.8841075166547
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:25387528"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_101"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "path_0_re76"
      uniprot "NA"
    ]
    graphics [
      x 2231.476070226575
      y 1352.394822733405
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:26584763"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_98"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "path_0_re67"
      uniprot "NA"
    ]
    graphics [
      x 791.4760702265747
      y 1266.3585216247106
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "PUBMED:23430059;PUBMED:18940792"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_41"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "path_0_re252"
      uniprot "NA"
    ]
    graphics [
      x 1481.4760702265748
      y 1479.8671897798747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000101255;urn:miriam:ncbigene:57761;urn:miriam:hgnc.symbol:TRIB3;urn:miriam:hgnc:16228;urn:miriam:refseq:NM_021158;urn:miriam:uniprot:Q96RU7"
      hgnc "HGNC_SYMBOL:TRIB3"
      map_id "M118_178"
      name "TRIB3"
      node_subtype "GENE"
      node_type "species"
      org_id "path_0_sa484"
      uniprot "UNIPROT:Q96RU7"
    ]
    graphics [
      x 1061.4760702265748
      y 1314.417231836038
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_178"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000101255;urn:miriam:ncbigene:57761;urn:miriam:hgnc.symbol:TRIB3;urn:miriam:hgnc:16228;urn:miriam:refseq:NM_021158;urn:miriam:uniprot:Q96RU7"
      hgnc "HGNC_SYMBOL:TRIB3"
      map_id "M118_179"
      name "TRIB3"
      node_subtype "RNA"
      node_type "species"
      org_id "path_0_sa485"
      uniprot "UNIPROT:Q96RU7"
    ]
    graphics [
      x 1897.9673850663708
      y 1600.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_179"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_42"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "path_0_re253"
      uniprot "NA"
    ]
    graphics [
      x 1727.2023363703734
      y 1014.2418672257081
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000101255;urn:miriam:ncbigene:57761;urn:miriam:ncbigene:57761;urn:miriam:hgnc.symbol:TRIB3;urn:miriam:hgnc:16228;urn:miriam:refseq:NM_021158;urn:miriam:uniprot:Q96RU7;urn:miriam:uniprot:Q96RU7"
      hgnc "HGNC_SYMBOL:TRIB3"
      map_id "M118_180"
      name "TRIB3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa486"
      uniprot "UNIPROT:Q96RU7"
    ]
    graphics [
      x 1868.8536650413898
      y 743.3558977543838
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_180"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:PPP1R15A;urn:miriam:hgnc:14375;urn:miriam:refseq:NM_014330;urn:miriam:ensembl:ENSG00000087074;urn:miriam:uniprot:O75807;urn:miriam:ncbigene:23645"
      hgnc "HGNC_SYMBOL:PPP1R15A"
      map_id "M118_116"
      name "PPP1R15A"
      node_subtype "GENE"
      node_type "species"
      org_id "path_0_sa109"
      uniprot "UNIPROT:O75807"
    ]
    graphics [
      x 518.8536650413897
      y 811.7606652634609
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:786;urn:miriam:refseq:NM_001675;urn:miriam:ncbigene:468;urn:miriam:ncbigene:468;urn:miriam:hgnc.symbol:ATF4;urn:miriam:uniprot:P18848;urn:miriam:uniprot:P18848;urn:miriam:ensembl:ENSG00000128272"
      hgnc "HGNC_SYMBOL:ATF4"
      map_id "M118_111"
      name "ATF4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa104"
      uniprot "UNIPROT:P18848"
    ]
    graphics [
      x 1071.6104592494069
      y 1749.749802637834
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:PPP1R15A;urn:miriam:hgnc:14375;urn:miriam:refseq:NM_014330;urn:miriam:ensembl:ENSG00000087074;urn:miriam:uniprot:O75807;urn:miriam:ncbigene:23645"
      hgnc "HGNC_SYMBOL:PPP1R15A"
      map_id "M118_115"
      name "PPP1R15A"
      node_subtype "RNA"
      node_type "species"
      org_id "path_0_sa108"
      uniprot "UNIPROT:O75807"
    ]
    graphics [
      x 1491.6104592494069
      y 1752.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_99"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "path_0_re68"
      uniprot "NA"
    ]
    graphics [
      x 1405.6716312959122
      y 492.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:PPP1R15A;urn:miriam:hgnc:14375;urn:miriam:refseq:NM_014330;urn:miriam:ensembl:ENSG00000087074;urn:miriam:uniprot:O75807;urn:miriam:uniprot:O75807;urn:miriam:ncbigene:23645;urn:miriam:ncbigene:23645"
      hgnc "HGNC_SYMBOL:PPP1R15A"
      map_id "M118_114"
      name "PPP1R15A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa107"
      uniprot "UNIPROT:O75807"
    ]
    graphics [
      x 1091.0597571849028
      y 376.1091751150544
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:12667446;PUBMED:12601012"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_103"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re80"
      uniprot "NA"
    ]
    graphics [
      x 1916.090149807047
      y 572.8646454446412
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:EIF2S1;urn:miriam:uniprot:P05198;urn:miriam:uniprot:P05198;urn:miriam:hgnc:3265;urn:miriam:ncbigene:1965;urn:miriam:ncbigene:1965;urn:miriam:ensembl:ENSG00000134001;urn:miriam:refseq:NM_004094"
      hgnc "HGNC_SYMBOL:EIF2S1"
      map_id "M118_222"
      name "EIF2S1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa87"
      uniprot "UNIPROT:P05198"
    ]
    graphics [
      x 2351.476070226575
      y 1496.6548737082785
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_222"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:Q9BQI3;urn:miriam:uniprot:Q9BQI3;urn:miriam:refseq:NM_014413;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:EIF2AK1;urn:miriam:hgnc:24921;urn:miriam:ncbigene:27102;urn:miriam:ncbigene:27102;urn:miriam:ensembl:ENSG00000086232"
      hgnc "HGNC_SYMBOL:EIF2AK1"
      map_id "M118_155"
      name "EIF2AK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa290"
      uniprot "UNIPROT:Q9BQI3"
    ]
    graphics [
      x 2038.8936421442643
      y 145.51720987443105
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_155"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:EIF2S1;urn:miriam:uniprot:P05198;urn:miriam:uniprot:P05198;urn:miriam:hgnc:3265;urn:miriam:ncbigene:1965;urn:miriam:ncbigene:1965;urn:miriam:ensembl:ENSG00000134001;urn:miriam:refseq:NM_004094"
      hgnc "HGNC_SYMBOL:EIF2S1"
      map_id "M118_224"
      name "EIF2S1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa90"
      uniprot "UNIPROT:P05198"
    ]
    graphics [
      x 818.8536650413897
      y 1029.5752808432774
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_224"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:12667446;PUBMED:12601012"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_93"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_0_re62"
      uniprot "NA"
    ]
    graphics [
      x 428.8536650413897
      y 842.5572932182373
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:26587781;PUBMED:12667446;PUBMED:12601012;PUBMED:18360008"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_92"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re61"
      uniprot "NA"
    ]
    graphics [
      x 742.5144998161113
      y 1955.224839764994
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:3255;urn:miriam:ensembl:ENSG00000172071;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:EIF2AK3;urn:miriam:uniprot:Q9NZJ5;urn:miriam:uniprot:Q9NZJ5;urn:miriam:ncbigene:9451;urn:miriam:ncbigene:9451;urn:miriam:refseq:NM_004836"
      hgnc "HGNC_SYMBOL:EIF2AK3"
      map_id "M118_8"
      name "EIF2AK3:EIF2AK3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa21"
      uniprot "UNIPROT:Q9NZJ5"
    ]
    graphics [
      x 1402.5144998161113
      y 1872.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15422;urn:miriam:hgnc:19687;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:EIF2AK4;urn:miriam:refseq:NM_001013703;urn:miriam:hgnc.symbol:EIF2AK4;urn:miriam:ncbigene:440275;urn:miriam:ncbigene:440275;urn:miriam:ensembl:ENSG00000128829;urn:miriam:uniprot:Q9P2K8;urn:miriam:uniprot:Q9P2K8"
      hgnc "HGNC_SYMBOL:EIF2AK4"
      map_id "M118_15"
      name "GCN2:ATP"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa35"
      uniprot "UNIPROT:Q9P2K8"
    ]
    graphics [
      x 322.5144998161113
      y 1922.4533125444054
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:9437;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc.symbol:EIF2AK2;urn:miriam:uniprot:P19525;urn:miriam:uniprot:P19525;urn:miriam:refseq:NM_002759;urn:miriam:ncbigene:5610;urn:miriam:ensembl:ENSG00000055332;urn:miriam:ncbigene:5610"
      hgnc "HGNC_SYMBOL:EIF2AK2"
      map_id "M118_140"
      name "EIF2AK2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa210"
      uniprot "UNIPROT:P19525"
    ]
    graphics [
      x 651.6104592494069
      y 1842.1662202339423
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_140"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000102580;urn:miriam:hgnc.symbol:DNAJC3;urn:miriam:hgnc:9439;urn:miriam:refseq:NM_006260;urn:miriam:uniprot:Q13217;urn:miriam:uniprot:Q13217;urn:miriam:ncbigene:5611;urn:miriam:ncbigene:5611"
      hgnc "HGNC_SYMBOL:DNAJC3"
      map_id "M118_184"
      name "DNAJC3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa515"
      uniprot "UNIPROT:Q13217"
    ]
    graphics [
      x 667.9673850663706
      y 1649.691377665889
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_184"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "PUBMED:12601012;PUBMED:18360008"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_44"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "path_0_re274"
      uniprot "NA"
    ]
    graphics [
      x 1281.6104592494069
      y 1782.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000102580;urn:miriam:hgnc.symbol:DNAJC3;urn:miriam:hgnc:9439;urn:miriam:refseq:NM_006260;urn:miriam:uniprot:Q13217;urn:miriam:ncbigene:5611"
      hgnc "HGNC_SYMBOL:DNAJC3"
      map_id "M118_186"
      name "DNAJC3"
      node_subtype "RNA"
      node_type "species"
      org_id "path_0_sa517"
      uniprot "UNIPROT:Q13217"
    ]
    graphics [
      x 1751.4760702265748
      y 1315.288563922968
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_186"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "PUBMED:12601012;PUBMED:18360008"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_43"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "path_0_re273"
      uniprot "NA"
    ]
    graphics [
      x 1586.090149807047
      y 432.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000102580;urn:miriam:hgnc.symbol:DNAJC3;urn:miriam:hgnc:9439;urn:miriam:refseq:NM_006260;urn:miriam:uniprot:Q13217;urn:miriam:ncbigene:5611"
      hgnc "HGNC_SYMBOL:DNAJC3"
      map_id "M118_185"
      name "DNAJC3"
      node_subtype "GENE"
      node_type "species"
      org_id "path_0_sa516"
      uniprot "UNIPROT:Q13217"
    ]
    graphics [
      x 1676.090149807047
      y 550.9402230096953
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_185"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:22926;urn:miriam:ensembl:ENSG00000118217;urn:miriam:ncbigene:22926;urn:miriam:refseq:NM_007348;urn:miriam:uniprot:P18850;urn:miriam:uniprot:P18850;urn:miriam:hgnc.symbol:ATF6;urn:miriam:hgnc:791"
      hgnc "HGNC_SYMBOL:ATF6"
      map_id "M118_218"
      name "ATF6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa65"
      uniprot "UNIPROT:P18850"
    ]
    graphics [
      x 1322.112491134918
      y 762.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_218"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:17991856;PUBMED:26587781;PUBMED:18360008"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_89"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "path_0_re46"
      uniprot "NA"
    ]
    graphics [
      x 1312.5144998161113
      y 1902.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "PUBMED:26587781;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_33"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "path_0_re18"
      uniprot "NA"
    ]
    graphics [
      x 1676.090149807047
      y 409.25361504304146
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "PUBMED:22802018;PUBMED:17991856;PUBMED:26587781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_91"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "path_0_re50"
      uniprot "NA"
    ]
    graphics [
      x 2587.9673850663708
      y 1661.7134980770413
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "PUBMED:18360008"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_54"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "path_0_re327"
      uniprot "NA"
    ]
    graphics [
      x 608.8536650413897
      y 691.3894547160434
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_006389;urn:miriam:hgnc:16931;urn:miriam:ncbigene:10525;urn:miriam:ensembl:ENSG00000149428;urn:miriam:uniprot:Q9Y4L1;urn:miriam:hgnc.symbol:HYOU1"
      hgnc "HGNC_SYMBOL:HYOU1"
      map_id "M118_203"
      name "HYOU1"
      node_subtype "GENE"
      node_type "species"
      org_id "path_0_sa605"
      uniprot "UNIPROT:Q9Y4L1"
    ]
    graphics [
      x 261.93582865917097
      y 1081.4904104782772
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_203"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_006389;urn:miriam:hgnc:16931;urn:miriam:ncbigene:10525;urn:miriam:ensembl:ENSG00000149428;urn:miriam:uniprot:Q9Y4L1;urn:miriam:hgnc.symbol:HYOU1"
      hgnc "HGNC_SYMBOL:HYOU1"
      map_id "M118_202"
      name "HYOU1"
      node_subtype "RNA"
      node_type "species"
      org_id "path_0_sa604"
      uniprot "UNIPROT:Q9Y4L1"
    ]
    graphics [
      x 1222.5144998161113
      y 1869.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_202"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "PUBMED:18360008"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_55"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "path_0_re328"
      uniprot "NA"
    ]
    graphics [
      x 573.5925437269425
      y 2263.243905175061
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_006389;urn:miriam:hgnc:16931;urn:miriam:ncbigene:10525;urn:miriam:ncbigene:10525;urn:miriam:ensembl:ENSG00000149428;urn:miriam:uniprot:Q9Y4L1;urn:miriam:uniprot:Q9Y4L1;urn:miriam:hgnc.symbol:HYOU1"
      hgnc "HGNC_SYMBOL:HYOU1"
      map_id "M118_204"
      name "HYOU1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa606"
      uniprot "UNIPROT:Q9Y4L1"
    ]
    graphics [
      x 358.8018943801087
      y 2190.9138911037776
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_204"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:DDIT3;urn:miriam:ncbigene:1649;urn:miriam:uniprot:P35638;urn:miriam:hgnc:2726;urn:miriam:refseq:NM_004083;urn:miriam:ensembl:ENSG00000175197;urn:miriam:uniprot:P0DPQ6"
      hgnc "HGNC_SYMBOL:DDIT3"
      map_id "M118_220"
      name "DDIT3"
      node_subtype "GENE"
      node_type "species"
      org_id "path_0_sa70"
      uniprot "UNIPROT:P35638;UNIPROT:P0DPQ6"
    ]
    graphics [
      x 2561.476070226575
      y 1348.3897028750896
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_220"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0034976"
      hgnc "NA"
      map_id "M118_216"
      name "Persistant_space_ER_space_Stress"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa633"
      uniprot "NA"
    ]
    graphics [
      x 1702.5144998161113
      y 1925.5046666730118
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_216"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_005080;urn:miriam:hgnc:12801;urn:miriam:ensembl:ENSG00000100219;urn:miriam:hgnc.symbol:XBP1;urn:miriam:ncbigene:7494;urn:miriam:ncbigene:7494;urn:miriam:uniprot:P17861;urn:miriam:uniprot:P17861"
      hgnc "HGNC_SYMBOL:XBP1"
      map_id "M118_149"
      name "XBP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa26"
      uniprot "UNIPROT:P17861"
    ]
    graphics [
      x 2321.476070226575
      y 1164.2194441704191
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_149"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:DDIT3;urn:miriam:ncbigene:1649;urn:miriam:uniprot:P35638;urn:miriam:hgnc:2726;urn:miriam:refseq:NM_004083;urn:miriam:ensembl:ENSG00000175197;urn:miriam:uniprot:P0DPQ6"
      hgnc "HGNC_SYMBOL:DDIT3"
      map_id "M118_221"
      name "DDIT3"
      node_subtype "RNA"
      node_type "species"
      org_id "path_0_sa71"
      uniprot "UNIPROT:P35638;UNIPROT:P0DPQ6"
    ]
    graphics [
      x 2452.5144998161113
      y 1843.8214117411178
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_221"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:26587781;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_34"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "path_0_re19"
      uniprot "NA"
    ]
    graphics [
      x 1761.6104592494069
      y 1765.9277406352314
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_005080;urn:miriam:hgnc:12801;urn:miriam:ensembl:ENSG00000100219;urn:miriam:hgnc.symbol:XBP1;urn:miriam:ncbigene:7494;urn:miriam:uniprot:P17861"
      hgnc "HGNC_SYMBOL:XBP1"
      map_id "M118_139"
      name "XBP1"
      node_subtype "RNA"
      node_type "species"
      org_id "path_0_sa206"
      uniprot "UNIPROT:P17861"
    ]
    graphics [
      x 2048.85366504139
      y 700.1506240017071
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_139"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      annotation "PUBMED:17991856;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_39"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_0_re245"
      uniprot "NA"
    ]
    graphics [
      x 1822.5935627164836
      y 2274.6594484469133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:26587781;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_38"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_0_re244"
      uniprot "NA"
    ]
    graphics [
      x 1927.9673850663708
      y 1600.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:2081;urn:miriam:refseq:NM_001433;urn:miriam:ncbigene:2081;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:3.1.26.-;urn:miriam:uniprot:O75460;urn:miriam:uniprot:O75460;urn:miriam:hgnc:3449;urn:miriam:ensembl:ENSG00000178607;urn:miriam:hgnc.symbol:ERN1"
      hgnc "HGNC_SYMBOL:ERN1"
      map_id "M118_206"
      name "ERN1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa617"
      uniprot "UNIPROT:O75460"
    ]
    graphics [
      x 1552.5144998161113
      y 2082.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_206"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_001204106;urn:miriam:hgnc:994;urn:miriam:hgnc.symbol:BCL2L11;urn:miriam:ncbigene:10018;urn:miriam:ensembl:ENSG00000153094;urn:miriam:ncbigene:10018;urn:miriam:uniprot:O43521;urn:miriam:uniprot:O43521"
      hgnc "HGNC_SYMBOL:BCL2L11"
      map_id "M118_176"
      name "BCL2L11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa475"
      uniprot "UNIPROT:O43521"
    ]
    graphics [
      x 1387.9673850663708
      y 1662.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_176"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_001204106;urn:miriam:hgnc:994;urn:miriam:hgnc.symbol:BCL2L11;urn:miriam:ncbigene:10018;urn:miriam:ensembl:ENSG00000153094;urn:miriam:ncbigene:10018;urn:miriam:uniprot:O43521;urn:miriam:uniprot:O43521;urn:miriam:ncbigene:2081;urn:miriam:refseq:NM_001433;urn:miriam:ncbigene:2081;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:3.1.26.-;urn:miriam:uniprot:O75460;urn:miriam:uniprot:O75460;urn:miriam:hgnc:3449;urn:miriam:ensembl:ENSG00000178607;urn:miriam:hgnc.symbol:ERN1"
      hgnc "HGNC_SYMBOL:BCL2L11;HGNC_SYMBOL:ERN1"
      map_id "M118_16"
      name "ERN1:BCL2L11"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa50"
      uniprot "UNIPROT:O43521;UNIPROT:O75460"
    ]
    graphics [
      x 2321.476070226575
      y 1262.8129202704126
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:Q9BXH1;urn:miriam:uniprot:Q9BXH1;urn:miriam:ncbigene:27113;urn:miriam:ncbigene:27113;urn:miriam:hgnc:17868;urn:miriam:ensembl:ENSG00000105327;urn:miriam:hgnc.symbol:BBC3;urn:miriam:refseq:NM_014417;urn:miriam:uniprot:Q96PG8;urn:miriam:uniprot:Q96PG8"
      hgnc "HGNC_SYMBOL:BBC3"
      map_id "M118_177"
      name "BBC3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa476"
      uniprot "UNIPROT:Q9BXH1;UNIPROT:Q96PG8"
    ]
    graphics [
      x 2029.8548263367193
      y 2447.5840889377423
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_177"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:2081;urn:miriam:refseq:NM_001433;urn:miriam:ncbigene:2081;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:3.1.26.-;urn:miriam:uniprot:O75460;urn:miriam:uniprot:O75460;urn:miriam:hgnc:3449;urn:miriam:ensembl:ENSG00000178607;urn:miriam:hgnc.symbol:ERN1;urn:miriam:uniprot:Q9BXH1;urn:miriam:uniprot:Q9BXH1;urn:miriam:ncbigene:27113;urn:miriam:ncbigene:27113;urn:miriam:hgnc:17868;urn:miriam:ensembl:ENSG00000105327;urn:miriam:hgnc.symbol:BBC3;urn:miriam:refseq:NM_014417;urn:miriam:uniprot:Q96PG8;urn:miriam:uniprot:Q96PG8"
      hgnc "HGNC_SYMBOL:ERN1;HGNC_SYMBOL:BBC3"
      map_id "M118_17"
      name "ERN1:BBC3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa51"
      uniprot "UNIPROT:O75460;UNIPROT:Q9BXH1;UNIPROT:Q96PG8"
    ]
    graphics [
      x 1687.2168861041923
      y 1029.571063044361
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_005080;urn:miriam:hgnc:12801;urn:miriam:ensembl:ENSG00000100219;urn:miriam:hgnc.symbol:XBP1;urn:miriam:ncbigene:7494;urn:miriam:uniprot:P17861"
      hgnc "HGNC_SYMBOL:XBP1"
      map_id "M118_152"
      name "XBP1"
      node_subtype "GENE"
      node_type "species"
      org_id "path_0_sa28"
      uniprot "UNIPROT:P17861"
    ]
    graphics [
      x 1649.357908862937
      y 222.06502830998443
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_152"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:2081;urn:miriam:refseq:NM_001433;urn:miriam:ncbigene:2081;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:3.1.26.-;urn:miriam:uniprot:O75460;urn:miriam:uniprot:O75460;urn:miriam:hgnc:3449;urn:miriam:ensembl:ENSG00000178607;urn:miriam:hgnc.symbol:ERN1;urn:miriam:ec-code:2.3.2.27;urn:miriam:hgnc.symbol:TRAF2;urn:miriam:ncbigene:7186;urn:miriam:ncbigene:7186;urn:miriam:ensembl:ENSG00000127191;urn:miriam:refseq:NM_021138;urn:miriam:uniprot:Q12933;urn:miriam:uniprot:Q12933;urn:miriam:hgnc:12032"
      hgnc "HGNC_SYMBOL:ERN1;HGNC_SYMBOL:TRAF2"
      map_id "M118_19"
      name "TRAF2:ERN1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa6"
      uniprot "UNIPROT:O75460;UNIPROT:Q12933"
    ]
    graphics [
      x 2531.476070226575
      y 1272.6818614454626
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      annotation "PUBMED:30773986;PUBMED:23850759;PUBMED:26587781;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_32"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re17"
      uniprot "NA"
    ]
    graphics [
      x 1730.0074341427699
      y 2286.313882011814
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re28"
      uniprot "NA"
    ]
    graphics [
      x 2017.9673850663708
      y 1565.7137326697948
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_100"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re75"
      uniprot "NA"
    ]
    graphics [
      x 1676.090149807047
      y 520.9402230096953
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_72"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re358"
      uniprot "NA"
    ]
    graphics [
      x 2332.5144998161113
      y 1836.1026909472607
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0006986"
      hgnc "NA"
      map_id "M118_159"
      name "UPR"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa36"
      uniprot "NA"
    ]
    graphics [
      x 1297.9673850663708
      y 1582.8933381685517
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_159"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_71"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re357"
      uniprot "NA"
    ]
    graphics [
      x 1791.6104592494069
      y 1663.8597507391748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:17991856;PUBMED:26587781;PUBMED:18360008"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_73"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re359"
      uniprot "NA"
    ]
    graphics [
      x 670.4146427238683
      y 2235.102169776212
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_70"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re356"
      uniprot "NA"
    ]
    graphics [
      x 1181.4760702265748
      y 1531.4212669072997
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:26587781;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_57"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re331"
      uniprot "NA"
    ]
    graphics [
      x 1526.090149807047
      y 432.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_005080;urn:miriam:hgnc:12801;urn:miriam:ensembl:ENSG00000100219;urn:miriam:hgnc.symbol:XBP1;urn:miriam:ncbigene:7494;urn:miriam:ncbigene:7494;urn:miriam:uniprot:P17861;urn:miriam:uniprot:P17861"
      hgnc "HGNC_SYMBOL:XBP1"
      map_id "M118_212"
      name "XBP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa626"
      uniprot "UNIPROT:P17861"
    ]
    graphics [
      x 1838.8536650413898
      y 749.8150054156808
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_212"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0006986"
      hgnc "NA"
      map_id "M118_158"
      name "accumulation_space_of_space_misfolded_space_protein_space_in_space_ER"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa336"
      uniprot "NA"
    ]
    graphics [
      x 1601.4760702265748
      y 1362.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_158"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_81"
      name "NA"
      node_subtype "MODULATION"
      node_type "reaction"
      org_id "path_0_re371"
      uniprot "NA"
    ]
    graphics [
      x 1476.0498757586736
      y 1782.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_68"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re353"
      uniprot "NA"
    ]
    graphics [
      x 788.8536650413897
      y 775.7583197686541
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_69"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re355"
      uniprot "NA"
    ]
    graphics [
      x 1028.8536650413898
      y 701.7180859122824
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0034976"
      hgnc "NA"
      map_id "M118_190"
      name "ER_space_Stress"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa58"
      uniprot "NA"
    ]
    graphics [
      x 938.8536650413897
      y 917.0681807869641
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_190"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      annotation "PUBMED:17991856"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_65"
      name "NA"
      node_subtype "MODULATION"
      node_type "reaction"
      org_id "path_0_re349"
      uniprot "NA"
    ]
    graphics [
      x 1586.090149807047
      y 402.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_106"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "path_0_re94"
      uniprot "NA"
    ]
    graphics [
      x 221.47607022657485
      y 1437.865638347216
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      annotation "PUBMED:17991856;PUBMED:26587781;PUBMED:18360008"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_86"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "path_0_re42"
      uniprot "NA"
    ]
    graphics [
      x 2288.85366504139
      y 762.5883256062364
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:22926;urn:miriam:ensembl:ENSG00000118217;urn:miriam:ncbigene:22926;urn:miriam:refseq:NM_007348;urn:miriam:uniprot:P18850;urn:miriam:uniprot:P18850;urn:miriam:hgnc.symbol:ATF6;urn:miriam:hgnc:791"
      hgnc "HGNC_SYMBOL:ATF6"
      map_id "M118_157"
      name "ATF6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa3"
      uniprot "UNIPROT:P18850"
    ]
    graphics [
      x 1751.055469358758
      y 916.8134512202714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_157"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0030970"
      hgnc "NA"
      map_id "M118_130"
      name "retrograde_space_transport_space_from_space_ER_space_to_space_cytosol"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa15"
      uniprot "NA"
    ]
    graphics [
      x 2138.85366504139
      y 597.226356263042
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_130"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:22926;urn:miriam:ensembl:ENSG00000118217;urn:miriam:ncbigene:22926;urn:miriam:refseq:NM_007348;urn:miriam:uniprot:P18850;urn:miriam:uniprot:P18850;urn:miriam:hgnc.symbol:ATF6;urn:miriam:hgnc:791"
      hgnc "HGNC_SYMBOL:ATF6"
      map_id "M118_189"
      name "ATF6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa57"
      uniprot "UNIPROT:P18850"
    ]
    graphics [
      x 1630.5931184092035
      y 342.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_189"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:17991856;PUBMED:26587781;PUBMED:18360008"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_87"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re44"
      uniprot "NA"
    ]
    graphics [
      x 938.8536650413897
      y 975.3799076516095
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:15456;urn:miriam:refseq:NM_003791;urn:miriam:ec-code:3.4.21.112;urn:miriam:hgnc.symbol:MBTPS1;urn:miriam:ncbigene:8720;urn:miriam:ncbigene:8720;urn:miriam:ensembl:ENSG00000140943;urn:miriam:uniprot:Q14703;urn:miriam:uniprot:Q14703"
      hgnc "HGNC_SYMBOL:MBTPS1"
      map_id "M118_199"
      name "MBTPS1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa60"
      uniprot "UNIPROT:Q14703"
    ]
    graphics [
      x 697.9673850663706
      y 1732.4666947456517
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_199"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:22926;urn:miriam:ensembl:ENSG00000118217;urn:miriam:ncbigene:22926;urn:miriam:refseq:NM_007348;urn:miriam:uniprot:P18850;urn:miriam:uniprot:P18850;urn:miriam:hgnc.symbol:ATF6;urn:miriam:hgnc:791"
      hgnc "HGNC_SYMBOL:ATF6"
      map_id "M118_208"
      name "ATF6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa62"
      uniprot "UNIPROT:P18850"
    ]
    graphics [
      x 1582.5144998161113
      y 1992.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_208"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:17991856;PUBMED:26587781;PUBMED:18360008"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_88"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re45"
      uniprot "NA"
    ]
    graphics [
      x 1510.72341549797
      y 2592.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_015884;urn:miriam:hgnc:15455;urn:miriam:ec-code:3.4.24.85;urn:miriam:hgnc.symbol:MBTPS2;urn:miriam:ncbigene:51360;urn:miriam:ncbigene:51360;urn:miriam:ensembl:ENSG00000012174;urn:miriam:uniprot:O43462;urn:miriam:uniprot:O43462"
      hgnc "HGNC_SYMBOL:MBTPS2"
      map_id "M118_205"
      name "MBTPS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa61"
      uniprot "UNIPROT:O43462"
    ]
    graphics [
      x 1582.5144998161113
      y 1872.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_205"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:22926;urn:miriam:ensembl:ENSG00000118217;urn:miriam:ncbigene:22926;urn:miriam:refseq:NM_007348;urn:miriam:uniprot:P18850;urn:miriam:uniprot:P18850;urn:miriam:hgnc.symbol:ATF6;urn:miriam:hgnc:791"
      hgnc "HGNC_SYMBOL:ATF6"
      map_id "M118_217"
      name "ATF6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa64"
      uniprot "UNIPROT:P18850"
    ]
    graphics [
      x 919.5727845368849
      y 2548.7023623069654
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_217"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      annotation "PUBMED:17991856;PUBMED:26587781;PUBMED:18360008"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_64"
      name "NA"
      node_subtype "MODULATION"
      node_type "reaction"
      org_id "path_0_re344"
      uniprot "NA"
    ]
    graphics [
      x 1496.090149807047
      y 432.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      annotation "PUBMED:30773986;PUBMED:23850759;PUBMED:12847084"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_24"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re11"
      uniprot "NA"
    ]
    graphics [
      x 2441.476070226575
      y 1313.7130410073787
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_26"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re13"
      uniprot "NA"
    ]
    graphics [
      x 1271.4760702265748
      y 1092.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0006511"
      hgnc "NA"
      map_id "M118_133"
      name "protein_space_ubiquitination_space_and_space_destruction"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa16"
      uniprot "NA"
    ]
    graphics [
      x 1371.6104592494069
      y 1782.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_133"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:17090218"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_22"
      name "NA"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "path_0_re1"
      uniprot "NA"
    ]
    graphics [
      x 1198.965509607095
      y 609.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:22926;urn:miriam:ensembl:ENSG00000118217;urn:miriam:ncbigene:22926;urn:miriam:refseq:NM_007348;urn:miriam:uniprot:P18850;urn:miriam:uniprot:P18850;urn:miriam:hgnc.symbol:ATF6;urn:miriam:hgnc:791;urn:miriam:hgnc.symbol:HSPA5;urn:miriam:ensembl:ENSG00000044574;urn:miriam:hgnc.symbol:HSPA5;urn:miriam:refseq:NM_005347;urn:miriam:hgnc:5238;urn:miriam:ncbigene:3309;urn:miriam:ncbigene:3309;urn:miriam:uniprot:P11021;urn:miriam:uniprot:P11021;urn:miriam:ec-code:3.6.4.10"
      hgnc "HGNC_SYMBOL:ATF6;HGNC_SYMBOL:HSPA5"
      map_id "M118_4"
      name "ATF6:HSPA5"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa1"
      uniprot "UNIPROT:P18850;UNIPROT:P11021"
    ]
    graphics [
      x 551.4760702265747
      y 1425.5551199408903
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_207"
      name "Unfolded_space_protein"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa618"
      uniprot "NA"
    ]
    graphics [
      x 848.8536650413897
      y 1097.7080626965108
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_207"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:HSPA5;urn:miriam:ensembl:ENSG00000044574;urn:miriam:hgnc.symbol:HSPA5;urn:miriam:refseq:NM_005347;urn:miriam:hgnc:5238;urn:miriam:ncbigene:3309;urn:miriam:ncbigene:3309;urn:miriam:uniprot:P11021;urn:miriam:uniprot:P11021;urn:miriam:ec-code:3.6.4.10"
      hgnc "HGNC_SYMBOL:HSPA5"
      map_id "M118_163"
      name "HSPA5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa4"
      uniprot "UNIPROT:P11021"
    ]
    graphics [
      x 1128.7679917669943
      y 165.98158322008976
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_163"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:CDK5;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc:1774;urn:miriam:ensembl:ENSG00000164885;urn:miriam:ncbigene:1020;urn:miriam:ncbigene:1020;urn:miriam:uniprot:Q00535;urn:miriam:uniprot:Q00535;urn:miriam:refseq:NM_001164410"
      hgnc "HGNC_SYMBOL:CDK5"
      map_id "M118_129"
      name "CDK5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa147"
      uniprot "UNIPROT:Q00535"
    ]
    graphics [
      x 491.47607022657473
      y 1048.3548907253055
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_129"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:CDK5;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc:1774;urn:miriam:ensembl:ENSG00000164885;urn:miriam:ncbigene:1020;urn:miriam:ncbigene:1020;urn:miriam:uniprot:Q00535;urn:miriam:uniprot:Q00535;urn:miriam:refseq:NM_001164410"
      hgnc "HGNC_SYMBOL:CDK5"
      map_id "M118_128"
      name "CDK5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa146"
      uniprot "UNIPROT:Q00535"
    ]
    graphics [
      x 817.9673850663706
      y 1506.3585216247106
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_105"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re93"
      uniprot "NA"
    ]
    graphics [
      x 1871.4760702265748
      y 1079.148371537528
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_001291958;urn:miriam:hgnc:6856;urn:miriam:hgnc.symbol:MAP3K4;urn:miriam:uniprot:Q9Y6R4;urn:miriam:uniprot:Q9Y6R4;urn:miriam:ncbigene:4216;urn:miriam:ncbigene:4216;urn:miriam:ensembl:ENSG00000085511;urn:miriam:ec-code:2.7.11.25"
      hgnc "HGNC_SYMBOL:MAP3K4"
      map_id "M118_126"
      name "MAP3K4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa144"
      uniprot "UNIPROT:Q9Y6R4"
    ]
    graphics [
      x 2288.85366504139
      y 959.4218429091999
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_001291958;urn:miriam:hgnc:6856;urn:miriam:hgnc.symbol:MAP3K4;urn:miriam:uniprot:Q9Y6R4;urn:miriam:uniprot:Q9Y6R4;urn:miriam:ncbigene:4216;urn:miriam:ncbigene:4216;urn:miriam:ensembl:ENSG00000085511;urn:miriam:ec-code:2.7.11.25"
      hgnc "HGNC_SYMBOL:MAP3K4"
      map_id "M118_127"
      name "MAP3K4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa145"
      uniprot "UNIPROT:Q9Y6R4"
    ]
    graphics [
      x 2138.85366504139
      y 881.7484197732449
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_127"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:26587781;PUBMED:18191217"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_46"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re29"
      uniprot "NA"
    ]
    graphics [
      x 1361.4760702265748
      y 1253.5527283689848
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ec-code:2.7.11.24;urn:miriam:refseq:NM_001278547;urn:miriam:ensembl:ENSG00000107643;urn:miriam:hgnc.symbol:MAPK8;urn:miriam:uniprot:P45983;urn:miriam:uniprot:P45983;urn:miriam:hgnc:6881;urn:miriam:ncbigene:5599;urn:miriam:ncbigene:5599"
      hgnc "HGNC_SYMBOL:MAPK8"
      map_id "M118_164"
      name "MAPK8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa40"
      uniprot "UNIPROT:P45983"
    ]
    graphics [
      x 461.47607022657473
      y 1383.9352012039044
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_164"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:Q99683;urn:miriam:uniprot:Q99683;urn:miriam:ncbigene:4217;urn:miriam:ncbigene:4217;urn:miriam:hgnc:6857;urn:miriam:refseq:NM_005923;urn:miriam:hgnc.symbol:MAP3K5;urn:miriam:ensembl:ENSG00000197442;urn:miriam:ec-code:2.7.11.25"
      hgnc "HGNC_SYMBOL:MAP3K5"
      map_id "M118_162"
      name "MAP3K5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa39"
      uniprot "UNIPROT:Q99683"
    ]
    graphics [
      x 547.9673850663706
      y 1604.6907010335744
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_162"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ec-code:2.7.11.24;urn:miriam:refseq:NM_001278547;urn:miriam:ensembl:ENSG00000107643;urn:miriam:hgnc.symbol:MAPK8;urn:miriam:uniprot:P45983;urn:miriam:uniprot:P45983;urn:miriam:hgnc:6881;urn:miriam:ncbigene:5599;urn:miriam:ncbigene:5599"
      hgnc "HGNC_SYMBOL:MAPK8"
      map_id "M118_165"
      name "MAPK8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa41"
      uniprot "UNIPROT:P45983"
    ]
    graphics [
      x 2411.476070226575
      y 1534.494966456387
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_165"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re221"
      uniprot "NA"
    ]
    graphics [
      x 1820.0074341427699
      y 2334.6594484469133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      annotation "PUBMED:18191217;PUBMED:23430059;PUBMED:29450140"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_104"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re81"
      uniprot "NA"
    ]
    graphics [
      x 2411.476070226575
      y 1056.9900002875893
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:26587781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_28"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re154"
      uniprot "NA"
    ]
    graphics [
      x 878.8536650413897
      y 916.1506228271123
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      annotation "PUBMED:23430059;PUBMED:11583631"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_37"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re222"
      uniprot "NA"
    ]
    graphics [
      x 1781.4760702265748
      y 1096.8134512202714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:BID;urn:miriam:ncbigene:637;urn:miriam:ncbigene:637;urn:miriam:refseq:NM_197966;urn:miriam:uniprot:P55957;urn:miriam:uniprot:P55957;urn:miriam:ensembl:ENSG00000015475;urn:miriam:hgnc:1050"
      hgnc "HGNC_SYMBOL:BID"
      map_id "M118_170"
      name "BID"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa435"
      uniprot "UNIPROT:P55957"
    ]
    graphics [
      x 2111.476070226575
      y 1395.3734055475652
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_170"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:BID;urn:miriam:ncbigene:637;urn:miriam:ncbigene:637;urn:miriam:refseq:NM_197966;urn:miriam:uniprot:P55957;urn:miriam:uniprot:P55957;urn:miriam:ensembl:ENSG00000015475;urn:miriam:hgnc:1050"
      hgnc "HGNC_SYMBOL:BID"
      map_id "M118_171"
      name "BID"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa436"
      uniprot "UNIPROT:P55957"
    ]
    graphics [
      x 2108.85366504139
      y 634.4720194774611
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_171"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      annotation "PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_75"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re361"
      uniprot "NA"
    ]
    graphics [
      x 2321.476070226575
      y 1194.2194441704191
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0006921"
      hgnc "NA"
      map_id "M118_172"
      name "Apoptosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa44"
      uniprot "NA"
    ]
    graphics [
      x 1762.5144998161113
      y 1993.8597507391748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_172"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_77"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re363"
      uniprot "NA"
    ]
    graphics [
      x 2032.5144998161113
      y 1820.4619322870956
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_82"
      name "NA"
      node_subtype "MODULATION"
      node_type "reaction"
      org_id "path_0_re372"
      uniprot "NA"
    ]
    graphics [
      x 1601.4760702265748
      y 1242.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_59"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re333"
      uniprot "NA"
    ]
    graphics [
      x 1611.6104592494069
      y 1782.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      annotation "PUBMED:26587781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_66"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re350"
      uniprot "NA"
    ]
    graphics [
      x 1221.7595581209157
      y 2172.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      annotation "PUBMED:30662442;PUBMED:23850759;PUBMED:26587781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_60"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re334"
      uniprot "NA"
    ]
    graphics [
      x 1199.5317509839817
      y 2532.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_74"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re360"
      uniprot "NA"
    ]
    graphics [
      x 1267.9673850663708
      y 1632.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_78"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re364"
      uniprot "NA"
    ]
    graphics [
      x 337.96738506637075
      y 1604.3762398780618
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_62"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re338"
      uniprot "NA"
    ]
    graphics [
      x 2201.476070226575
      y 1409.6830592677404
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      annotation "PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_76"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re362"
      uniprot "NA"
    ]
    graphics [
      x 1121.4760702265748
      y 1278.9296873026296
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_80"
      name "NA"
      node_subtype "MODULATION"
      node_type "reaction"
      org_id "path_0_re367"
      uniprot "NA"
    ]
    graphics [
      x 1628.8536650413898
      y 732.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0043066"
      hgnc "NA"
      map_id "M118_125"
      name "Cell_space_survival"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa135"
      uniprot "NA"
    ]
    graphics [
      x 1661.8095395740024
      y 582.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_125"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000101255;urn:miriam:ncbigene:57761;urn:miriam:ncbigene:57761;urn:miriam:hgnc.symbol:TRIB3;urn:miriam:hgnc:16228;urn:miriam:refseq:NM_021158;urn:miriam:uniprot:Q96RU7;urn:miriam:uniprot:Q96RU7"
      hgnc "HGNC_SYMBOL:TRIB3"
      map_id "M118_342"
      name "TRIB3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa257"
      uniprot "UNIPROT:Q96RU7"
    ]
    graphics [
      x 1148.8536650413898
      y 893.0689340107122
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_342"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_138763;urn:miriam:hgnc:959;urn:miriam:ensembl:ENSG00000087088;urn:miriam:hgnc.symbol:BAX;urn:miriam:ncbigene:581;urn:miriam:uniprot:Q07812;urn:miriam:uniprot:Q07812;urn:miriam:ncbigene:581"
      hgnc "HGNC_SYMBOL:BAX"
      map_id "M118_123"
      name "BAX"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa127"
      uniprot "UNIPROT:Q07812"
    ]
    graphics [
      x 2471.476070226575
      y 1250.7202367733357
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_63"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re342"
      uniprot "NA"
    ]
    graphics [
      x 1222.5144998161113
      y 1899.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0032471"
      hgnc "NA"
      map_id "M118_124"
      name "release_space_of_space_ER_space_Ca2_plus_"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa132"
      uniprot "NA"
    ]
    graphics [
      x 1145.6939429587637
      y 2381.9561233684817
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_124"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_56"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re329"
      uniprot "NA"
    ]
    graphics [
      x 1333.957571089838
      y 2772.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A1902656"
      hgnc "NA"
      map_id "M118_148"
      name "high_space_Ca2_plus__space_cytosolic_space_concentration"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa259"
      uniprot "NA"
    ]
    graphics [
      x 1762.5144998161113
      y 1963.8597507391748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_148"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      annotation "PUBMED:18955970"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_67"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re352"
      uniprot "NA"
    ]
    graphics [
      x 802.5144998161113
      y 1947.6236763137715
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      annotation "PUBMED:19931333"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_53"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_0_re324"
      uniprot "NA"
    ]
    graphics [
      x 1465.2508552528402
      y 2562.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:823;urn:miriam:ncbigene:823;urn:miriam:ensembl:ENSG00000014216;urn:miriam:hgnc:1476;urn:miriam:ec-code:3.4.22.52;urn:miriam:hgnc.symbol:CAPN1;urn:miriam:refseq:NM_001198868;urn:miriam:uniprot:P07384;urn:miriam:uniprot:P07384"
      hgnc "HGNC_SYMBOL:CAPN1"
      map_id "M118_200"
      name "CAPN1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa602"
      uniprot "UNIPROT:P07384"
    ]
    graphics [
      x 1180.4146427238684
      y 2199.0166269831952
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_200"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29108"
      hgnc "NA"
      map_id "M118_135"
      name "Ca2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "path_0_sa164"
      uniprot "NA"
    ]
    graphics [
      x 531.6104592494069
      y 1824.8660612677036
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29108;urn:miriam:ncbigene:823;urn:miriam:ncbigene:823;urn:miriam:ensembl:ENSG00000014216;urn:miriam:hgnc:1476;urn:miriam:ec-code:3.4.22.52;urn:miriam:hgnc.symbol:CAPN1;urn:miriam:refseq:NM_001198868;urn:miriam:uniprot:P07384;urn:miriam:uniprot:P07384"
      hgnc "HGNC_SYMBOL:CAPN1"
      map_id "M118_18"
      name "CAPN1:Ca2_plus_"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa56"
      uniprot "UNIPROT:P07384"
    ]
    graphics [
      x 2032.5144998161113
      y 2037.3681790319438
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      annotation "PUBMED:18955970;PUBMED:19931333;PUBMED:24373849"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_50"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re321"
      uniprot "NA"
    ]
    graphics [
      x 2651.476070226575
      y 1353.9222189709865
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ec-code:3.4.22.57;urn:miriam:ensembl:ENSG00000196954;urn:miriam:ncbigene:837;urn:miriam:ncbigene:837;urn:miriam:hgnc:1505;urn:miriam:refseq:NM_001225;urn:miriam:hgnc.symbol:CASP4;urn:miriam:uniprot:P49662;urn:miriam:uniprot:P49662"
      hgnc "HGNC_SYMBOL:CASP4"
      map_id "M118_193"
      name "CASP4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa594"
      uniprot "UNIPROT:P49662"
    ]
    graphics [
      x 2512.5144998161113
      y 1960.3003697824354
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_193"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ec-code:3.4.22.57;urn:miriam:ensembl:ENSG00000196954;urn:miriam:ncbigene:837;urn:miriam:ncbigene:837;urn:miriam:hgnc:1505;urn:miriam:refseq:NM_001225;urn:miriam:hgnc.symbol:CASP4;urn:miriam:uniprot:P49662;urn:miriam:uniprot:P49662"
      hgnc "HGNC_SYMBOL:CASP4"
      map_id "M118_194"
      name "cleaved~CASP4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa595"
      uniprot "UNIPROT:P49662"
    ]
    graphics [
      x 2232.5297403356776
      y 2282.522457289507
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_194"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      annotation "PUBMED:18955970;PUBMED:19931333;PUBMED:24373849"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_52"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re323"
      uniprot "NA"
    ]
    graphics [
      x 2055.0449368989925
      y 2194.8290808904762
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      annotation "PUBMED:18955970;PUBMED:19931333;PUBMED:24373849"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_51"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re322"
      uniprot "NA"
    ]
    graphics [
      x 1750.4146427238684
      y 2173.859750739175
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:836;urn:miriam:refseq:NM_004346;urn:miriam:ncbigene:836;urn:miriam:ec-code:3.4.22.56;urn:miriam:ensembl:ENSG00000164305;urn:miriam:hgnc:1504;urn:miriam:uniprot:P42574;urn:miriam:uniprot:P42574;urn:miriam:hgnc.symbol:CASP3"
      hgnc "HGNC_SYMBOL:CASP3"
      map_id "M118_195"
      name "CASP3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa596"
      uniprot "UNIPROT:P42574"
    ]
    graphics [
      x 2021.4760702265748
      y 1452.2903389012884
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_195"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:836;urn:miriam:refseq:NM_004346;urn:miriam:ncbigene:836;urn:miriam:ec-code:3.4.22.56;urn:miriam:ensembl:ENSG00000164305;urn:miriam:hgnc:1504;urn:miriam:uniprot:P42574;urn:miriam:uniprot:P42574;urn:miriam:hgnc.symbol:CASP3"
      hgnc "HGNC_SYMBOL:CASP3"
      map_id "M118_196"
      name "cleaved~CASP3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa597"
      uniprot "UNIPROT:P42574"
    ]
    graphics [
      x 1121.4760702265748
      y 1458.9296873026296
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_196"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:CASP9;urn:miriam:hgnc.symbol:CASP9;urn:miriam:refseq:NM_032996;urn:miriam:ncbigene:842;urn:miriam:ncbigene:842;urn:miriam:hgnc.symbol:CSAP9;urn:miriam:hgnc:1511;urn:miriam:ensembl:ENSG00000132906;urn:miriam:ec-code:3.4.22.62;urn:miriam:uniprot:P55211;urn:miriam:uniprot:P55211"
      hgnc "HGNC_SYMBOL:CASP9;HGNC_SYMBOL:CSAP9"
      map_id "M118_197"
      name "CASP9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa598"
      uniprot "UNIPROT:P55211"
    ]
    graphics [
      x 1852.5144998161113
      y 2028.108707658523
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_197"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:CASP9;urn:miriam:refseq:NM_032996;urn:miriam:ncbigene:842;urn:miriam:ncbigene:842;urn:miriam:hgnc:1511;urn:miriam:ensembl:ENSG00000132906;urn:miriam:ec-code:3.4.22.62;urn:miriam:uniprot:P55211;urn:miriam:uniprot:P55211"
      hgnc "HGNC_SYMBOL:CASP9"
      map_id "M118_198"
      name "cleaved~CASP9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa599"
      uniprot "UNIPROT:P55211"
    ]
    graphics [
      x 621.6104592494069
      y 1782.1662202339423
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_198"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_23"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "path_0_re107"
      uniprot "NA"
    ]
    graphics [
      x 1480.72341549797
      y 2622.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29108"
      hgnc "NA"
      map_id "M118_134"
      name "Ca2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "path_0_sa163"
      uniprot "NA"
    ]
    graphics [
      x 1661.4760702265748
      y 1367.5725709100786
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:11723;urn:miriam:ensembl:ENSG00000139644;urn:miriam:uniprot:P55061;urn:miriam:uniprot:P55061;urn:miriam:hgnc.symbol:TMBIM6;urn:miriam:ncbigene:7009;urn:miriam:ncbigene:7009;urn:miriam:refseq:NM_003217"
      hgnc "HGNC_SYMBOL:TMBIM6"
      map_id "M118_181"
      name "TMBIM6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa487"
      uniprot "UNIPROT:P55061"
    ]
    graphics [
      x 1657.6263482474933
      y 2352.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_181"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:6261;urn:miriam:refseq:NM_000540;urn:miriam:ncbigene:6261;urn:miriam:ensembl:ENSG00000196218;urn:miriam:hgnc:10483;urn:miriam:hgnc.symbol:RYR1;urn:miriam:uniprot:P21817;urn:miriam:uniprot:P21817"
      hgnc "HGNC_SYMBOL:RYR1"
      map_id "M118_182"
      name "RYR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa494"
      uniprot "UNIPROT:P21817"
    ]
    graphics [
      x 1460.0074341427699
      y 2412.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_182"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:ITPR1;urn:miriam:hgnc.symbol:ITPR3;urn:miriam:hgnc.symbol:ITPR2;urn:miriam:ensembl:ENSG00000123104;urn:miriam:ncbigene:3710;urn:miriam:ncbigene:3710;urn:miriam:refseq:NM_002223;urn:miriam:refseq:NM_002222;urn:miriam:uniprot:Q14571;urn:miriam:uniprot:Q14571;urn:miriam:refseq:NM_002224;urn:miriam:hgnc:6180;urn:miriam:uniprot:Q14573;urn:miriam:uniprot:Q14573;urn:miriam:ncbigene:3709;urn:miriam:ncbigene:3709;urn:miriam:ncbigene:3708;urn:miriam:ncbigene:3708;urn:miriam:ensembl:ENSG00000150995;urn:miriam:hgnc:6181;urn:miriam:uniprot:Q14643;urn:miriam:hgnc:6182;urn:miriam:uniprot:Q14643;urn:miriam:ensembl:ENSG00000096433"
      hgnc "HGNC_SYMBOL:ITPR1;HGNC_SYMBOL:ITPR3;HGNC_SYMBOL:ITPR2"
      map_id "M118_192"
      name "ITPR"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa583"
      uniprot "UNIPROT:Q14571;UNIPROT:Q14573;UNIPROT:Q14643"
    ]
    graphics [
      x 2059.8548263367193
      y 2439.9511278738273
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_192"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_48"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re317"
      uniprot "NA"
    ]
    graphics [
      x 1672.9931034425704
      y 2581.3119086148745
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:ITPR1;urn:miriam:hgnc.symbol:ITPR3;urn:miriam:hgnc.symbol:ITPR2;urn:miriam:ensembl:ENSG00000123104;urn:miriam:ncbigene:3710;urn:miriam:ncbigene:3710;urn:miriam:refseq:NM_002223;urn:miriam:refseq:NM_002222;urn:miriam:uniprot:Q14571;urn:miriam:uniprot:Q14571;urn:miriam:refseq:NM_002224;urn:miriam:hgnc:6180;urn:miriam:uniprot:Q14573;urn:miriam:uniprot:Q14573;urn:miriam:ncbigene:3709;urn:miriam:ncbigene:3709;urn:miriam:ncbigene:3708;urn:miriam:ncbigene:3708;urn:miriam:ensembl:ENSG00000150995;urn:miriam:hgnc:6181;urn:miriam:uniprot:Q14643;urn:miriam:hgnc:6182;urn:miriam:uniprot:Q14643;urn:miriam:ensembl:ENSG00000096433"
      hgnc "HGNC_SYMBOL:ITPR1;HGNC_SYMBOL:ITPR3;HGNC_SYMBOL:ITPR2"
      map_id "M118_191"
      name "ITPR"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa582"
      uniprot "UNIPROT:Q14571;UNIPROT:Q14573;UNIPROT:Q14643"
    ]
    graphics [
      x 1909.8548263367193
      y 2470.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_191"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:13280;urn:miriam:hgnc.symbol:ERO1A;urn:miriam:refseq:NM_014584;urn:miriam:uniprot:Q96HE7;urn:miriam:uniprot:Q96HE7;urn:miriam:ncbigene:30001;urn:miriam:ncbigene:30001;urn:miriam:ec-code:1.8.4.-;urn:miriam:ensembl:ENSG00000197930"
      hgnc "HGNC_SYMBOL:ERO1A"
      map_id "M118_211"
      name "ERO1A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa625"
      uniprot "UNIPROT:Q96HE7"
    ]
    graphics [
      x 801.6104592494069
      y 1812.8406551402936
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_211"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_001204106;urn:miriam:hgnc:994;urn:miriam:hgnc.symbol:BCL2L11;urn:miriam:ncbigene:10018;urn:miriam:ensembl:ENSG00000153094;urn:miriam:ncbigene:10018;urn:miriam:uniprot:O43521;urn:miriam:uniprot:O43521"
      hgnc "HGNC_SYMBOL:BCL2L11"
      map_id "M118_169"
      name "BCL2L11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa434"
      uniprot "UNIPROT:O43521"
    ]
    graphics [
      x 1761.6104592494069
      y 1735.9277406352314
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_169"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_40"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re249"
      uniprot "NA"
    ]
    graphics [
      x 1390.4075582174767
      y 2502.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:5515;urn:miriam:ncbigene:5515;urn:miriam:ensembl:ENSG00000113575;urn:miriam:refseq:NM_002715;urn:miriam:ec-code:3.1.3.16;urn:miriam:uniprot:P67775;urn:miriam:uniprot:P67775;urn:miriam:hgnc.symbol:PPP2CA;urn:miriam:hgnc:9299"
      hgnc "HGNC_SYMBOL:PPP2CA"
      map_id "M118_201"
      name "PPP2CA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa603"
      uniprot "UNIPROT:P67775"
    ]
    graphics [
      x 1177.1006829377097
      y 2442.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_201"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_001204106;urn:miriam:hgnc:994;urn:miriam:hgnc.symbol:BCL2L11;urn:miriam:ncbigene:10018;urn:miriam:ensembl:ENSG00000153094;urn:miriam:ncbigene:10018;urn:miriam:uniprot:O43521;urn:miriam:uniprot:O43521"
      hgnc "HGNC_SYMBOL:BCL2L11"
      map_id "M118_168"
      name "BCL2L11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa433"
      uniprot "UNIPROT:O43521"
    ]
    graphics [
      x 1012.5144998161113
      y 1898.4447255924251
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_168"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:DDIT3;urn:miriam:ncbigene:1649;urn:miriam:ncbigene:1649;urn:miriam:uniprot:P35638;urn:miriam:uniprot:P35638;urn:miriam:hgnc:2726;urn:miriam:refseq:NM_004083;urn:miriam:ensembl:ENSG00000175197;urn:miriam:uniprot:P0DPQ6;urn:miriam:uniprot:P0DPQ6"
      hgnc "HGNC_SYMBOL:DDIT3"
      map_id "M118_215"
      name "DDIT3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa630"
      uniprot "UNIPROT:P35638;UNIPROT:P0DPQ6"
    ]
    graphics [
      x 981.6104592494069
      y 1754.5973095489717
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_215"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      annotation "PUBMED:26137585;PUBMED:23850759;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_84"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re374"
      uniprot "NA"
    ]
    graphics [
      x 1628.8536650413898
      y 672.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:DDIT3;urn:miriam:ncbigene:1649;urn:miriam:ncbigene:1649;urn:miriam:uniprot:P35638;urn:miriam:uniprot:P35638;urn:miriam:hgnc:2726;urn:miriam:refseq:NM_004083;urn:miriam:ensembl:ENSG00000175197;urn:miriam:uniprot:P0DPQ6;urn:miriam:uniprot:P0DPQ6"
      hgnc "HGNC_SYMBOL:DDIT3"
      map_id "M118_214"
      name "DDIT3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa629"
      uniprot "UNIPROT:P35638;UNIPROT:P0DPQ6"
    ]
    graphics [
      x 758.8536650413897
      y 1047.5868356076667
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_214"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ec-code:2.7.11.24;urn:miriam:ensembl:ENSG00000112062;urn:miriam:hgnc:6876;urn:miriam:uniprot:Q16539;urn:miriam:uniprot:Q16539;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:ncbigene:1432;urn:miriam:refseq:NM_001315"
      hgnc "HGNC_SYMBOL:MAPK14"
      map_id "M118_167"
      name "MAPK14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa43"
      uniprot "UNIPROT:Q16539"
    ]
    graphics [
      x 1607.2023363703734
      y 972.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_167"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:12215209"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re30"
      uniprot "NA"
    ]
    graphics [
      x 311.47607022657485
      y 1374.3227618731576
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ec-code:2.7.11.24;urn:miriam:ensembl:ENSG00000112062;urn:miriam:hgnc:6876;urn:miriam:uniprot:Q16539;urn:miriam:uniprot:Q16539;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:ncbigene:1432;urn:miriam:refseq:NM_001315"
      hgnc "HGNC_SYMBOL:MAPK14"
      map_id "M118_166"
      name "MAPK14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa42"
      uniprot "UNIPROT:Q16539"
    ]
    graphics [
      x 458.8536650413897
      y 812.5572932182373
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_166"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:9474;urn:miriam:ensembl:ENSG00000057663;urn:miriam:ncbigene:9474;urn:miriam:hgnc:589;urn:miriam:uniprot:Q9H1Y0;urn:miriam:uniprot:Q9H1Y0;urn:miriam:hgnc.symbol:ATG5;urn:miriam:refseq:NM_004849;urn:miriam:hgnc:3573;urn:miriam:ncbigene:8772;urn:miriam:ncbigene:8772;urn:miriam:uniprot:Q13158;urn:miriam:uniprot:Q13158;urn:miriam:ensembl:ENSG00000168040;urn:miriam:refseq:NM_003824;urn:miriam:hgnc.symbol:FADD;urn:miriam:hgnc.symbol:FADD;urn:miriam:refseq:NM_181509;urn:miriam:ncbigene:84557;urn:miriam:ncbigene:84557;urn:miriam:hgnc.symbol:MAP1LC3A;urn:miriam:ensembl:ENSG00000101460;urn:miriam:hgnc:6838;urn:miriam:uniprot:Q9H492;urn:miriam:uniprot:Q9H492;urn:miriam:hgnc:1509;urn:miriam:hgnc.symbol:CASP8;urn:miriam:uniprot:Q14790;urn:miriam:uniprot:Q14790;urn:miriam:hgnc.symbol:CASP8;urn:miriam:ncbigene:841;urn:miriam:ncbigene:841;urn:miriam:ec-code:3.4.22.61;urn:miriam:refseq:NM_001228;urn:miriam:ensembl:ENSG00000064012"
      hgnc "HGNC_SYMBOL:ATG5;HGNC_SYMBOL:FADD;HGNC_SYMBOL:MAP1LC3A;HGNC_SYMBOL:CASP8"
      map_id "M118_10"
      name "CASP8:FADD:MAP1LC3A:SQSTM1:ATG5"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa26"
      uniprot "UNIPROT:Q9H1Y0;UNIPROT:Q13158;UNIPROT:Q9H492;UNIPROT:Q14790"
    ]
    graphics [
      x 1447.9673850663708
      y 1632.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      annotation "PUBMED:17991856"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_83"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re373"
      uniprot "NA"
    ]
    graphics [
      x 2211.6104592494066
      y 1760.5567968186347
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:1509;urn:miriam:hgnc.symbol:CASP8;urn:miriam:uniprot:Q14790;urn:miriam:uniprot:Q14790;urn:miriam:hgnc.symbol:CASP8;urn:miriam:ncbigene:841;urn:miriam:ncbigene:841;urn:miriam:ec-code:3.4.22.61;urn:miriam:refseq:NM_001228;urn:miriam:ensembl:ENSG00000064012;urn:miriam:hgnc:3573;urn:miriam:ncbigene:8772;urn:miriam:ncbigene:8772;urn:miriam:uniprot:Q13158;urn:miriam:uniprot:Q13158;urn:miriam:ensembl:ENSG00000168040;urn:miriam:refseq:NM_003824;urn:miriam:hgnc.symbol:FADD;urn:miriam:hgnc.symbol:FADD"
      hgnc "HGNC_SYMBOL:CASP8;HGNC_SYMBOL:FADD"
      map_id "M118_9"
      name "CASP8:CASP8_minus_ubq:FADD"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa25"
      uniprot "UNIPROT:Q14790;UNIPROT:Q13158"
    ]
    graphics [
      x 1958.8536650413898
      y 838.367004148693
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:11280;urn:miriam:ensembl:ENSG00000161011;urn:miriam:uniprot:Q13501;urn:miriam:uniprot:Q13501;urn:miriam:hgnc.symbol:SQSTM1;urn:miriam:ncbigene:8878;urn:miriam:refseq:NM_001142298;urn:miriam:ncbigene:8878"
      hgnc "HGNC_SYMBOL:SQSTM1"
      map_id "M118_144"
      name "SQSTM1_space_"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa238"
      uniprot "UNIPROT:Q13501"
    ]
    graphics [
      x 2140.4146427238684
      y 2185.77832712972
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_144"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_181509;urn:miriam:ncbigene:84557;urn:miriam:ncbigene:84557;urn:miriam:hgnc.symbol:MAP1LC3A;urn:miriam:ensembl:ENSG00000101460;urn:miriam:hgnc:6838;urn:miriam:uniprot:Q9H492;urn:miriam:uniprot:Q9H492"
      hgnc "HGNC_SYMBOL:MAP1LC3A"
      map_id "M118_145"
      name "MAP1LC3A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa239"
      uniprot "UNIPROT:Q9H492"
    ]
    graphics [
      x 2172.5297403356776
      y 2279.338956375759
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_145"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:9474;urn:miriam:ensembl:ENSG00000057663;urn:miriam:ncbigene:9474;urn:miriam:hgnc:589;urn:miriam:uniprot:Q9H1Y0;urn:miriam:uniprot:Q9H1Y0;urn:miriam:hgnc.symbol:ATG5;urn:miriam:refseq:NM_004849"
      hgnc "HGNC_SYMBOL:ATG5"
      map_id "M118_146"
      name "ATG5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa241"
      uniprot "UNIPROT:Q9H1Y0"
    ]
    graphics [
      x 2651.476070226575
      y 1459.5099628607657
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_146"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:9976;urn:miriam:ensembl:ENSG00000204977;urn:miriam:ec-code:2.3.2.27;urn:miriam:uniprot:O60858;urn:miriam:uniprot:O60858;urn:miriam:ncbigene:10206;urn:miriam:ncbigene:10206;urn:miriam:hgnc.symbol:TRIM13;urn:miriam:refseq:NM_001007278"
      hgnc "HGNC_SYMBOL:TRIM13"
      map_id "M118_147"
      name "TRIM13"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa254"
      uniprot "UNIPROT:O60858"
    ]
    graphics [
      x 2092.5144998161113
      y 2063.8489235874436
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_147"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 167
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0034976"
      hgnc "NA"
      map_id "M118_143"
      name "Persistant_space_ER_space_Stress"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa231"
      uniprot "NA"
    ]
    graphics [
      x 2081.476070226575
      y 1153.7082153288716
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_143"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 168
    zlevel -1

    cd19dm [
      annotation "PUBMED:17991856"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_25"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re129"
      uniprot "NA"
    ]
    graphics [
      x 641.4760702265747
      y 1244.0156442515633
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 169
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:3255;urn:miriam:ensembl:ENSG00000172071;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:EIF2AK3;urn:miriam:uniprot:Q9NZJ5;urn:miriam:uniprot:Q9NZJ5;urn:miriam:ncbigene:9451;urn:miriam:ncbigene:9451;urn:miriam:refseq:NM_004836"
      hgnc "HGNC_SYMBOL:EIF2AK3"
      map_id "M118_136"
      name "EIF2AK3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa2"
      uniprot "UNIPROT:Q9NZJ5"
    ]
    graphics [
      x 547.9673850663706
      y 1565.261006004971
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 170
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_183"
      name "Unfolded_space_protein"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa5"
      uniprot "NA"
    ]
    graphics [
      x 398.85366504138983
      y 662.5572932182373
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_183"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 171
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000125740;urn:miriam:hgnc.symbol:JUN;urn:miriam:refseq:NM_005252;urn:miriam:refseq:NM_005253;urn:miriam:uniprot:P01100;urn:miriam:uniprot:P01100;urn:miriam:ncbigene:3725;urn:miriam:ncbigene:3725;urn:miriam:hgnc.symbol:FOSL1;urn:miriam:ncbigene:3727;urn:miriam:ncbigene:3727;urn:miriam:hgnc.symbol:FOSL2;urn:miriam:ncbigene:3726;urn:miriam:ncbigene:3726;urn:miriam:uniprot:P17535;urn:miriam:ensembl:ENSG00000170345;urn:miriam:uniprot:P17535;urn:miriam:ncbigene:8061;urn:miriam:ncbigene:8061;urn:miriam:refseq:NM_002229;urn:miriam:refseq:NM_002228;urn:miriam:ensembl:ENSG00000075426;urn:miriam:hgnc:6206;urn:miriam:hgnc:6204;urn:miriam:hgnc:6205;urn:miriam:uniprot:P15407;urn:miriam:uniprot:P15407;urn:miriam:uniprot:P15408;urn:miriam:uniprot:P15408;urn:miriam:hgnc:13718;urn:miriam:ensembl:ENSG00000171223;urn:miriam:hgnc.symbol:FOSB;urn:miriam:hgnc.symbol:FOS;urn:miriam:hgnc.symbol:JUNB;urn:miriam:hgnc.symbol:JUND;urn:miriam:refseq:NM_005354;urn:miriam:uniprot:P05412;urn:miriam:uniprot:P05412;urn:miriam:uniprot:P17275;urn:miriam:refseq:NM_005438;urn:miriam:uniprot:P17275;urn:miriam:uniprot:P53539;urn:miriam:uniprot:P53539;urn:miriam:ensembl:ENSG00000130522;urn:miriam:hgnc:3796;urn:miriam:hgnc:3798;urn:miriam:hgnc:3797;urn:miriam:ncbigene:2353;urn:miriam:ncbigene:2353;urn:miriam:ncbigene:2355;urn:miriam:ncbigene:2355;urn:miriam:ncbigene:2354;urn:miriam:ncbigene:2354;urn:miriam:ensembl:ENSG00000175592;urn:miriam:refseq:NM_006732;urn:miriam:ensembl:ENSG00000177606"
      hgnc "HGNC_SYMBOL:JUN;HGNC_SYMBOL:FOSL1;HGNC_SYMBOL:FOSL2;HGNC_SYMBOL:FOSB;HGNC_SYMBOL:FOS;HGNC_SYMBOL:JUNB;HGNC_SYMBOL:JUND"
      map_id "M118_150"
      name "AP_minus_1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa260"
      uniprot "UNIPROT:P01100;UNIPROT:P17535;UNIPROT:P15407;UNIPROT:P15408;UNIPROT:P05412;UNIPROT:P17275;UNIPROT:P53539"
    ]
    graphics [
      x 1358.8536650413898
      y 582.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_150"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 172
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000125740;urn:miriam:hgnc.symbol:JUN;urn:miriam:refseq:NM_005252;urn:miriam:refseq:NM_005253;urn:miriam:uniprot:P01100;urn:miriam:uniprot:P01100;urn:miriam:ncbigene:3725;urn:miriam:ncbigene:3725;urn:miriam:hgnc.symbol:FOSL1;urn:miriam:ncbigene:3727;urn:miriam:ncbigene:3727;urn:miriam:hgnc.symbol:FOSL2;urn:miriam:ncbigene:3726;urn:miriam:ncbigene:3726;urn:miriam:uniprot:P17535;urn:miriam:ensembl:ENSG00000170345;urn:miriam:uniprot:P17535;urn:miriam:ncbigene:8061;urn:miriam:ncbigene:8061;urn:miriam:refseq:NM_002229;urn:miriam:refseq:NM_002228;urn:miriam:ensembl:ENSG00000075426;urn:miriam:hgnc:6206;urn:miriam:hgnc:6204;urn:miriam:hgnc:6205;urn:miriam:uniprot:P15407;urn:miriam:uniprot:P15407;urn:miriam:uniprot:P15408;urn:miriam:uniprot:P15408;urn:miriam:hgnc:13718;urn:miriam:ensembl:ENSG00000171223;urn:miriam:hgnc.symbol:FOSB;urn:miriam:hgnc.symbol:FOS;urn:miriam:hgnc.symbol:JUNB;urn:miriam:hgnc.symbol:JUND;urn:miriam:refseq:NM_005354;urn:miriam:uniprot:P05412;urn:miriam:uniprot:P05412;urn:miriam:uniprot:P17275;urn:miriam:refseq:NM_005438;urn:miriam:uniprot:P17275;urn:miriam:uniprot:P53539;urn:miriam:uniprot:P53539;urn:miriam:ensembl:ENSG00000130522;urn:miriam:hgnc:3796;urn:miriam:hgnc:3798;urn:miriam:hgnc:3797;urn:miriam:ncbigene:2353;urn:miriam:ncbigene:2353;urn:miriam:ncbigene:2355;urn:miriam:ncbigene:2355;urn:miriam:ncbigene:2354;urn:miriam:ncbigene:2354;urn:miriam:ensembl:ENSG00000175592;urn:miriam:refseq:NM_006732;urn:miriam:ensembl:ENSG00000177606"
      hgnc "HGNC_SYMBOL:JUN;HGNC_SYMBOL:FOSL1;HGNC_SYMBOL:FOSL2;HGNC_SYMBOL:FOSB;HGNC_SYMBOL:FOS;HGNC_SYMBOL:JUNB;HGNC_SYMBOL:JUND"
      map_id "M118_151"
      name "AP_minus_1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa261"
      uniprot "UNIPROT:P01100;UNIPROT:P17535;UNIPROT:P15407;UNIPROT:P15408;UNIPROT:P05412;UNIPROT:P17275;UNIPROT:P53539"
    ]
    graphics [
      x 281.47607022657485
      y 1320.79339615826
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_151"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 173
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_138763;urn:miriam:hgnc:959;urn:miriam:ensembl:ENSG00000087088;urn:miriam:hgnc.symbol:BAX;urn:miriam:ncbigene:581;urn:miriam:uniprot:Q07812;urn:miriam:uniprot:Q07812;urn:miriam:ncbigene:581"
      hgnc "HGNC_SYMBOL:BAX"
      map_id "M118_122"
      name "BAX"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa126"
      uniprot "UNIPROT:Q07812"
    ]
    graphics [
      x 1841.4760702265748
      y 1220.6183567721846
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 174
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0036503"
      hgnc "NA"
      map_id "M118_160"
      name "ERAD"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa37"
      uniprot "NA"
    ]
    graphics [
      x 971.4760702265747
      y 1108.1362075712395
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_160"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 175
    zlevel -1

    cd19dm [
      annotation "PUBMED:26137585;PUBMED:23850759;PUBMED:26587781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_58"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re332"
      uniprot "NA"
    ]
    graphics [
      x 1258.965509607095
      y 582.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 176
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_005080;urn:miriam:hgnc:12801;urn:miriam:ensembl:ENSG00000100219;urn:miriam:hgnc.symbol:XBP1;urn:miriam:ncbigene:7494;urn:miriam:ncbigene:7494;urn:miriam:uniprot:P17861;urn:miriam:uniprot:P17861"
      hgnc "HGNC_SYMBOL:XBP1"
      map_id "M118_213"
      name "XBP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa627"
      uniprot "UNIPROT:P17861"
    ]
    graphics [
      x 1661.4760702265748
      y 1176.7446462369894
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_213"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 177
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:Q99683;urn:miriam:uniprot:Q99683;urn:miriam:ncbigene:4217;urn:miriam:ncbigene:4217;urn:miriam:hgnc:6857;urn:miriam:refseq:NM_005923;urn:miriam:hgnc.symbol:MAP3K5;urn:miriam:ensembl:ENSG00000197442;urn:miriam:ec-code:2.7.11.25"
      hgnc "HGNC_SYMBOL:MAP3K5"
      map_id "M118_161"
      name "MAP3K5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa38"
      uniprot "UNIPROT:Q99683"
    ]
    graphics [
      x 1287.7231255590953
      y 402.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_161"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 178
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:hgnc.symbol:TRAF2;urn:miriam:ncbigene:7186;urn:miriam:ncbigene:7186;urn:miriam:ensembl:ENSG00000127191;urn:miriam:refseq:NM_021138;urn:miriam:uniprot:Q12933;urn:miriam:uniprot:Q12933;urn:miriam:hgnc:12032"
      hgnc "HGNC_SYMBOL:TRAF2"
      map_id "M118_117"
      name "TRAF2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa118"
      uniprot "UNIPROT:Q12933"
    ]
    graphics [
      x 2066.814922793581
      y 504.8606125107739
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 179
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:hgnc.symbol:TRAF2;urn:miriam:ncbigene:7186;urn:miriam:ncbigene:7186;urn:miriam:ensembl:ENSG00000127191;urn:miriam:refseq:NM_021138;urn:miriam:uniprot:Q12933;urn:miriam:uniprot:Q12933;urn:miriam:hgnc:12032"
      hgnc "HGNC_SYMBOL:TRAF2"
      map_id "M118_118"
      name "TRAF2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa119"
      uniprot "UNIPROT:Q12933"
    ]
    graphics [
      x 1430.0074341427699
      y 2352.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 180
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:2081;urn:miriam:refseq:NM_001433;urn:miriam:ncbigene:2081;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:3.1.26.-;urn:miriam:uniprot:O75460;urn:miriam:uniprot:O75460;urn:miriam:hgnc:3449;urn:miriam:ensembl:ENSG00000178607;urn:miriam:hgnc.symbol:ERN1"
      hgnc "HGNC_SYMBOL:ERN1"
      map_id "M118_6"
      name "ERN1:Unfolded_space_protein"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa2"
      uniprot "UNIPROT:O75460"
    ]
    graphics [
      x 1554.1900265952593
      y 1572.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 181
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:hgnc.symbol:TRAF2;urn:miriam:ncbigene:7186;urn:miriam:ncbigene:7186;urn:miriam:ensembl:ENSG00000127191;urn:miriam:refseq:NM_021138;urn:miriam:uniprot:Q12933;urn:miriam:uniprot:Q12933;urn:miriam:hgnc:12032"
      hgnc "HGNC_SYMBOL:TRAF2"
      map_id "M118_138"
      name "TRAF2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa204"
      uniprot "UNIPROT:Q12933"
    ]
    graphics [
      x 1653.5980350148664
      y 2716.16836388908
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_138"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 182
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_001188;urn:miriam:ensembl:ENSG00000030110;urn:miriam:uniprot:Q16611;urn:miriam:uniprot:Q16611;urn:miriam:hgnc.symbol:BAK1;urn:miriam:ncbigene:578;urn:miriam:ncbigene:578;urn:miriam:hgnc:949;urn:miriam:ncbigene:2081;urn:miriam:refseq:NM_001433;urn:miriam:ncbigene:2081;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:3.1.26.-;urn:miriam:uniprot:O75460;urn:miriam:uniprot:O75460;urn:miriam:hgnc:3449;urn:miriam:ensembl:ENSG00000178607;urn:miriam:hgnc.symbol:ERN1"
      hgnc "HGNC_SYMBOL:BAK1;HGNC_SYMBOL:ERN1"
      map_id "M118_5"
      name "BAK1:ERN1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa19"
      uniprot "UNIPROT:Q16611;UNIPROT:O75460"
    ]
    graphics [
      x 1777.9673850663708
      y 1633.8597507391748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 183
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:2081;urn:miriam:refseq:NM_001433;urn:miriam:ncbigene:2081;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:3.1.26.-;urn:miriam:uniprot:O75460;urn:miriam:uniprot:O75460;urn:miriam:hgnc:3449;urn:miriam:ensembl:ENSG00000178607;urn:miriam:hgnc.symbol:ERN1;urn:miriam:refseq:NM_138763;urn:miriam:hgnc:959;urn:miriam:ensembl:ENSG00000087088;urn:miriam:hgnc.symbol:BAX;urn:miriam:ncbigene:581;urn:miriam:uniprot:Q07812;urn:miriam:uniprot:Q07812;urn:miriam:ncbigene:581"
      hgnc "HGNC_SYMBOL:ERN1;HGNC_SYMBOL:BAX"
      map_id "M118_7"
      name "BAX:ERN1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa20"
      uniprot "UNIPROT:O75460;UNIPROT:Q07812"
    ]
    graphics [
      x 2154.080281687381
      y 2547.963300558853
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 184
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:BCL2;urn:miriam:refseq:NM_000657;urn:miriam:ncbigene:596;urn:miriam:ncbigene:596;urn:miriam:uniprot:P10415;urn:miriam:uniprot:P10415;urn:miriam:ensembl:ENSG00000171791;urn:miriam:refseq:NM_000633;urn:miriam:hgnc:990"
      hgnc "HGNC_SYMBOL:BCL2"
      map_id "M118_131"
      name "BCL2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa152"
      uniprot "UNIPROT:P10415"
    ]
    graphics [
      x 1432.5144998161113
      y 1872.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_131"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 185
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_138578;urn:miriam:ncbigene:598;urn:miriam:ncbigene:598;urn:miriam:ensembl:ENSG00000171552;urn:miriam:uniprot:Q07817;urn:miriam:uniprot:Q07817;urn:miriam:hgnc:992;urn:miriam:hgnc.symbol:BCL2L1"
      hgnc "HGNC_SYMBOL:BCL2L1"
      map_id "M118_132"
      name "BCL2L1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa153"
      uniprot "UNIPROT:Q07817"
    ]
    graphics [
      x 1882.5144998161113
      y 2080.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 186
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_138763;urn:miriam:hgnc:959;urn:miriam:ensembl:ENSG00000087088;urn:miriam:hgnc.symbol:BAX;urn:miriam:ncbigene:581;urn:miriam:uniprot:Q07812;urn:miriam:uniprot:Q07812;urn:miriam:ncbigene:581"
      hgnc "HGNC_SYMBOL:BAX"
      map_id "M118_173"
      name "BAX"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa458"
      uniprot "UNIPROT:Q07812"
    ]
    graphics [
      x 1642.5144998161113
      y 2112.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_173"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 187
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_001188;urn:miriam:ensembl:ENSG00000030110;urn:miriam:uniprot:Q16611;urn:miriam:uniprot:Q16611;urn:miriam:hgnc.symbol:BAK1;urn:miriam:ncbigene:578;urn:miriam:ncbigene:578;urn:miriam:hgnc:949"
      hgnc "HGNC_SYMBOL:BAK1"
      map_id "M118_174"
      name "BAK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa459"
      uniprot "UNIPROT:Q16611"
    ]
    graphics [
      x 891.6104592494069
      y 1801.4477491325204
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_174"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 188
    zlevel -1

    cd19dm [
      annotation "PUBMED:30773986;PUBMED:23850759;PUBMED:26587781;PUBMED:23430059"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_35"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_0_re2"
      uniprot "NA"
    ]
    graphics [
      x 2048.85366504139
      y 852.9547352166127
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 189
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:2081;urn:miriam:refseq:NM_001433;urn:miriam:ncbigene:2081;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:3.1.26.-;urn:miriam:uniprot:O75460;urn:miriam:uniprot:O75460;urn:miriam:hgnc:3449;urn:miriam:ensembl:ENSG00000178607;urn:miriam:hgnc.symbol:ERN1"
      hgnc "HGNC_SYMBOL:ERN1"
      map_id "M118_107"
      name "ERN1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa1"
      uniprot "UNIPROT:O75460"
    ]
    graphics [
      x 2621.476070226575
      y 1286.3405927730755
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 190
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_137"
      name "Unfolded_space_protein"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa203"
      uniprot "NA"
    ]
    graphics [
      x 818.8536650413897
      y 801.6075523575174
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_137"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 191
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:26587781;PUBMED:12601012"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_29"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re162"
      uniprot "NA"
    ]
    graphics [
      x 2257.9673850663708
      y 1612.0251788532216
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 192
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:9437;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc.symbol:EIF2AK2;urn:miriam:uniprot:P19525;urn:miriam:uniprot:P19525;urn:miriam:refseq:NM_002759;urn:miriam:ncbigene:5610;urn:miriam:ensembl:ENSG00000055332;urn:miriam:ncbigene:5610"
      hgnc "HGNC_SYMBOL:EIF2AK2"
      map_id "M118_153"
      name "EIF2AK2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa287"
      uniprot "UNIPROT:P19525"
    ]
    graphics [
      x 2602.5144998161113
      y 1835.7044831538688
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_153"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 193
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0071359"
      hgnc "NA"
      map_id "M118_154"
      name "presence_space_of_space_dsRNA"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa288"
      uniprot "NA"
    ]
    graphics [
      x 2512.5144998161113
      y 1816.1097759034521
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_154"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 194
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000102580;urn:miriam:hgnc.symbol:DNAJC3;urn:miriam:hgnc:9439;urn:miriam:refseq:NM_006260;urn:miriam:uniprot:Q13217;urn:miriam:uniprot:Q13217;urn:miriam:ncbigene:5611;urn:miriam:ncbigene:5611"
      hgnc "HGNC_SYMBOL:DNAJC3"
      map_id "M118_210"
      name "DNAJC3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa624"
      uniprot "UNIPROT:Q13217"
    ]
    graphics [
      x 1721.4760702265748
      y 1293.540416895229
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_210"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 195
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:3266;urn:miriam:hgnc.symbol:EIF2S2;urn:miriam:ncbigene:8894;urn:miriam:refseq:NM_003908;urn:miriam:ncbigene:8894;urn:miriam:uniprot:P20042;urn:miriam:uniprot:P20042;urn:miriam:ensembl:ENSG00000125977"
      hgnc "HGNC_SYMBOL:EIF2S2"
      map_id "M118_223"
      name "EIF2S2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa88"
      uniprot "UNIPROT:P20042"
    ]
    graphics [
      x 2111.476070226575
      y 1067.7871053251297
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_223"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 196
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A65180;urn:miriam:ensembl:ENSG00000130741;urn:miriam:hgnc.symbol:EIF2S3;urn:miriam:hgnc.symbol:EIF2S3;urn:miriam:hgnc:3267;urn:miriam:uniprot:P41091;urn:miriam:uniprot:P41091;urn:miriam:ec-code:3.6.5.3;urn:miriam:ncbigene:1968;urn:miriam:ncbigene:1968;urn:miriam:refseq:NM_001415"
      hgnc "HGNC_SYMBOL:EIF2S3"
      map_id "M118_12"
      name "EIF2S3:GDP"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa31"
      uniprot "UNIPROT:P41091"
    ]
    graphics [
      x 577.9673850663706
      y 1545.55511994089
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 197
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:EIF2S1;urn:miriam:uniprot:P05198;urn:miriam:uniprot:P05198;urn:miriam:hgnc:3265;urn:miriam:ncbigene:1965;urn:miriam:ncbigene:1965;urn:miriam:ensembl:ENSG00000134001;urn:miriam:refseq:NM_004094;urn:miriam:hgnc:3266;urn:miriam:hgnc.symbol:EIF2S2;urn:miriam:ncbigene:8894;urn:miriam:refseq:NM_003908;urn:miriam:ncbigene:8894;urn:miriam:uniprot:P20042;urn:miriam:uniprot:P20042;urn:miriam:ensembl:ENSG00000125977;urn:miriam:ensembl:ENSG00000130741;urn:miriam:hgnc.symbol:EIF2S3;urn:miriam:hgnc.symbol:EIF2S3;urn:miriam:hgnc:3267;urn:miriam:uniprot:P41091;urn:miriam:uniprot:P41091;urn:miriam:ec-code:3.6.5.3;urn:miriam:ncbigene:1968;urn:miriam:ncbigene:1968;urn:miriam:refseq:NM_001415;urn:miriam:obo.chebi:CHEBI%3A65180"
      hgnc "HGNC_SYMBOL:EIF2S1;HGNC_SYMBOL:EIF2S2;HGNC_SYMBOL:EIF2S3"
      map_id "M118_11"
      name "EIF2:GDP"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa30"
      uniprot "UNIPROT:P05198;UNIPROT:P20042;UNIPROT:P41091"
    ]
    graphics [
      x 136.15354929197088
      y 1105.5021177433189
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 198
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:12667446"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_30"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_0_re165"
      uniprot "NA"
    ]
    graphics [
      x 428.8536650413897
      y 692.5572932182373
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 199
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:34779;urn:miriam:hgnc.symbol:TRM-CAT3-1;urn:miriam:ncbigene:100189216"
      hgnc "HGNC_SYMBOL:TRM-CAT3-1"
      map_id "M118_156"
      name "Met_minus_tRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "path_0_sa293"
      uniprot "NA"
    ]
    graphics [
      x 881.4760702265747
      y 1405.488055210296
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_156"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 200
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:3266;urn:miriam:hgnc.symbol:EIF2S2;urn:miriam:ncbigene:8894;urn:miriam:refseq:NM_003908;urn:miriam:ncbigene:8894;urn:miriam:uniprot:P20042;urn:miriam:uniprot:P20042;urn:miriam:ensembl:ENSG00000125977;urn:miriam:ensembl:ENSG00000130741;urn:miriam:hgnc.symbol:EIF2S3;urn:miriam:hgnc.symbol:EIF2S3;urn:miriam:hgnc:3267;urn:miriam:uniprot:P41091;urn:miriam:uniprot:P41091;urn:miriam:ec-code:3.6.5.3;urn:miriam:ncbigene:1968;urn:miriam:ncbigene:1968;urn:miriam:refseq:NM_001415;urn:miriam:hgnc.symbol:EIF2S1;urn:miriam:uniprot:P05198;urn:miriam:uniprot:P05198;urn:miriam:hgnc:3265;urn:miriam:ncbigene:1965;urn:miriam:ncbigene:1965;urn:miriam:ensembl:ENSG00000134001;urn:miriam:refseq:NM_004094;urn:miriam:obo.chebi:CHEBI%3A65180;urn:miriam:hgnc:34779;urn:miriam:hgnc.symbol:TRM-CAT3-1;urn:miriam:ncbigene:100189216"
      hgnc "HGNC_SYMBOL:EIF2S2;HGNC_SYMBOL:EIF2S3;HGNC_SYMBOL:EIF2S1;HGNC_SYMBOL:TRM-CAT3-1"
      map_id "M118_20"
      name "EIF2:GDP:Met_minus_tRNA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa7"
      uniprot "UNIPROT:P20042;UNIPROT:P41091;UNIPROT:P05198"
    ]
    graphics [
      x 908.8536650413897
      y 635.563778928661
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 201
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_95"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re64"
      uniprot "NA"
    ]
    graphics [
      x 1721.4760702265748
      y 1439.0814698311804
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 202
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_94"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re63"
      uniprot "NA"
    ]
    graphics [
      x 2124.162741229908
      y 388.86122436149685
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 203
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57600"
      hgnc "NA"
      map_id "M118_109"
      name "GTP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "path_0_sa102"
      uniprot "NA"
    ]
    graphics [
      x 1921.6776964485557
      y 175.65296211192208
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 204
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000145191;urn:miriam:ncbigene:8893;urn:miriam:ncbigene:8893;urn:miriam:hgnc.symbol:EIF2B5;urn:miriam:refseq:NM_003907;urn:miriam:uniprot:Q13144;urn:miriam:uniprot:Q13144;urn:miriam:hgnc.symbol:EIF2B5;urn:miriam:hgnc:3261;urn:miriam:hgnc.symbol:EIF2B1;urn:miriam:hgnc.symbol:EIF2B1;urn:miriam:hgnc:3257;urn:miriam:ncbigene:1967;urn:miriam:ncbigene:1967;urn:miriam:ensembl:ENSG00000111361;urn:miriam:uniprot:Q14232;urn:miriam:uniprot:Q14232;urn:miriam:refseq:NM_001414;urn:miriam:ensembl:ENSG00000070785;urn:miriam:hgnc.symbol:EIF2B3;urn:miriam:hgnc.symbol:EIF2B3;urn:miriam:hgnc:3259;urn:miriam:ncbigene:8891;urn:miriam:ncbigene:8891;urn:miriam:refseq:NM_020365;urn:miriam:uniprot:Q9NR50;urn:miriam:uniprot:Q9NR50;urn:miriam:ncbigene:8890;urn:miriam:ncbigene:8890;urn:miriam:uniprot:Q9UI10;urn:miriam:uniprot:Q9UI10;urn:miriam:hgnc.symbol:EIF2B4;urn:miriam:hgnc.symbol:EIF2B4;urn:miriam:ensembl:ENSG00000115211;urn:miriam:hgnc:3260;urn:miriam:refseq:NM_001034116;urn:miriam:hgnc.symbol:EIF2B2;urn:miriam:hgnc.symbol:EIF2B2;urn:miriam:ncbigene:8892;urn:miriam:ncbigene:8892;urn:miriam:hgnc:3258;urn:miriam:uniprot:P49770;urn:miriam:uniprot:P49770;urn:miriam:ensembl:ENSG00000119718;urn:miriam:refseq:NM_014239"
      hgnc "HGNC_SYMBOL:EIF2B5;HGNC_SYMBOL:EIF2B1;HGNC_SYMBOL:EIF2B3;HGNC_SYMBOL:EIF2B4;HGNC_SYMBOL:EIF2B2"
      map_id "M118_14"
      name "EIF2B"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa34"
      uniprot "UNIPROT:Q13144;UNIPROT:Q14232;UNIPROT:Q9NR50;UNIPROT:Q9UI10;UNIPROT:P49770"
    ]
    graphics [
      x 1688.8536650413898
      y 879.5710630443609
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 205
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:EIF2S1;urn:miriam:uniprot:P05198;urn:miriam:uniprot:P05198;urn:miriam:hgnc:3265;urn:miriam:ncbigene:1965;urn:miriam:ncbigene:1965;urn:miriam:ensembl:ENSG00000134001;urn:miriam:refseq:NM_004094;urn:miriam:hgnc:3266;urn:miriam:hgnc.symbol:EIF2S2;urn:miriam:ncbigene:8894;urn:miriam:refseq:NM_003908;urn:miriam:ncbigene:8894;urn:miriam:uniprot:P20042;urn:miriam:uniprot:P20042;urn:miriam:ensembl:ENSG00000125977;urn:miriam:obo.chebi:CHEBI%3A57600;urn:miriam:ensembl:ENSG00000130741;urn:miriam:hgnc.symbol:EIF2S3;urn:miriam:hgnc.symbol:EIF2S3;urn:miriam:hgnc:3267;urn:miriam:uniprot:P41091;urn:miriam:uniprot:P41091;urn:miriam:ec-code:3.6.5.3;urn:miriam:ncbigene:1968;urn:miriam:ncbigene:1968;urn:miriam:refseq:NM_001415;urn:miriam:hgnc:34779;urn:miriam:hgnc.symbol:TRM-CAT3-1;urn:miriam:ncbigene:100189216"
      hgnc "HGNC_SYMBOL:EIF2S1;HGNC_SYMBOL:EIF2S2;HGNC_SYMBOL:EIF2S3;HGNC_SYMBOL:TRM-CAT3-1"
      map_id "M118_21"
      name "Ternary_space_Complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa8"
      uniprot "UNIPROT:P05198;UNIPROT:P20042;UNIPROT:P41091"
    ]
    graphics [
      x 2168.85366504139
      y 633.315186220209
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 206
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A65180"
      hgnc "NA"
      map_id "M118_108"
      name "GDP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "path_0_sa100"
      uniprot "NA"
    ]
    graphics [
      x 1636.8405824608653
      y 192.06502830998443
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 207
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_61"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re337"
      uniprot "NA"
    ]
    graphics [
      x 1331.4760702265748
      y 1032.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 208
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0006412"
      hgnc "NA"
      map_id "M118_226"
      name "Translation_space_initiation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa96"
      uniprot "NA"
    ]
    graphics [
      x 967.9673850663706
      y 1629.4280235401561
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_226"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 209
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A65180"
      hgnc "NA"
      map_id "M118_110"
      name "GDP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "path_0_sa103"
      uniprot "NA"
    ]
    graphics [
      x 2141.476070226575
      y 1333.9514256816515
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 210
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57600"
      hgnc "NA"
      map_id "M118_225"
      name "GTP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "path_0_sa95"
      uniprot "NA"
    ]
    graphics [
      x 1450.3379497221226
      y 1932.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_225"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 211
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:12667446;PUBMED:12601012"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_31"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_0_re166"
      uniprot "NA"
    ]
    graphics [
      x 1961.4760702265748
      y 1337.606843746084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 212
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000130741;urn:miriam:hgnc.symbol:EIF2S3;urn:miriam:hgnc.symbol:EIF2S3;urn:miriam:hgnc:3267;urn:miriam:uniprot:P41091;urn:miriam:uniprot:P41091;urn:miriam:ec-code:3.6.5.3;urn:miriam:ncbigene:1968;urn:miriam:ncbigene:1968;urn:miriam:refseq:NM_001415;urn:miriam:obo.chebi:CHEBI%3A65180;urn:miriam:hgnc:3266;urn:miriam:hgnc.symbol:EIF2S2;urn:miriam:ncbigene:8894;urn:miriam:refseq:NM_003908;urn:miriam:ncbigene:8894;urn:miriam:uniprot:P20042;urn:miriam:uniprot:P20042;urn:miriam:ensembl:ENSG00000125977;urn:miriam:hgnc.symbol:EIF2S1;urn:miriam:uniprot:P05198;urn:miriam:uniprot:P05198;urn:miriam:hgnc:3265;urn:miriam:ncbigene:1965;urn:miriam:ncbigene:1965;urn:miriam:ensembl:ENSG00000134001;urn:miriam:refseq:NM_004094"
      hgnc "HGNC_SYMBOL:EIF2S3;HGNC_SYMBOL:EIF2S2;HGNC_SYMBOL:EIF2S1"
      map_id "M118_13"
      name "EIF2_minus_P:GDP"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_0_csa33"
      uniprot "UNIPROT:P41091;UNIPROT:P20042;UNIPROT:P05198"
    ]
    graphics [
      x 1132.5144998161113
      y 1854.961185314934
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 213
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759;PUBMED:15277680"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_96"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_0_re65"
      uniprot "NA"
    ]
    graphics [
      x 929.0500325976689
      y 2631.3446155820525
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 214
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:786;urn:miriam:refseq:NM_001675;urn:miriam:ncbigene:468;urn:miriam:hgnc.symbol:ATF4;urn:miriam:uniprot:P18848;urn:miriam:ensembl:ENSG00000128272"
      hgnc "HGNC_SYMBOL:ATF4"
      map_id "M118_112"
      name "ATF4"
      node_subtype "GENE"
      node_type "species"
      org_id "path_0_sa105"
      uniprot "UNIPROT:P18848"
    ]
    graphics [
      x 1553.1354481841486
      y 2699.916342950497
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 215
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:786;urn:miriam:refseq:NM_001675;urn:miriam:ncbigene:468;urn:miriam:hgnc.symbol:ATF4;urn:miriam:uniprot:P18848;urn:miriam:ensembl:ENSG00000128272"
      hgnc "HGNC_SYMBOL:ATF4"
      map_id "M118_113"
      name "ATF4"
      node_subtype "RNA"
      node_type "species"
      org_id "path_0_sa106"
      uniprot "UNIPROT:P18848"
    ]
    graphics [
      x 610.4146427238683
      y 2130.8938577562612
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 216
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_97"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "path_0_re66"
      uniprot "NA"
    ]
    graphics [
      x 1057.9673850663708
      y 1719.749802637834
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 217
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:13280;urn:miriam:hgnc.symbol:ERO1A;urn:miriam:refseq:NM_014584;urn:miriam:uniprot:Q96HE7;urn:miriam:ncbigene:30001;urn:miriam:ensembl:ENSG00000197930"
      hgnc "HGNC_SYMBOL:ERO1A"
      map_id "M118_120"
      name "ERO1A"
      node_subtype "GENE"
      node_type "species"
      org_id "path_0_sa122"
      uniprot "UNIPROT:Q96HE7"
    ]
    graphics [
      x 2411.476070226575
      y 996.9900002875893
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 218
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:13280;urn:miriam:hgnc.symbol:ERO1A;urn:miriam:refseq:NM_014584;urn:miriam:uniprot:Q96HE7;urn:miriam:ncbigene:30001;urn:miriam:ensembl:ENSG00000197930"
      hgnc "HGNC_SYMBOL:ERO1A"
      map_id "M118_121"
      name "ERO1A"
      node_subtype "RNA"
      node_type "species"
      org_id "path_0_sa123"
      uniprot "UNIPROT:Q96HE7"
    ]
    graphics [
      x 1331.4760702265748
      y 1152.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 219
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_102"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "path_0_re77"
      uniprot "NA"
    ]
    graphics [
      x 1031.4760702265748
      y 1085.0238894408317
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 220
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:13280;urn:miriam:hgnc.symbol:ERO1A;urn:miriam:refseq:NM_014584;urn:miriam:uniprot:Q96HE7;urn:miriam:uniprot:Q96HE7;urn:miriam:ncbigene:30001;urn:miriam:ncbigene:30001;urn:miriam:ec-code:1.8.4.-;urn:miriam:ensembl:ENSG00000197930"
      hgnc "HGNC_SYMBOL:ERO1A"
      map_id "M118_119"
      name "ERO1A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa121"
      uniprot "UNIPROT:Q96HE7"
    ]
    graphics [
      x 1058.8536650413898
      y 637.9623721797412
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 221
    zlevel -1

    cd19dm [
      annotation "PUBMED:23850759"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_27"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "path_0_re133"
      uniprot "NA"
    ]
    graphics [
      x 1060.5699334766189
      y 572.1008787256268
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 222
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:13280;urn:miriam:hgnc.symbol:ERO1A;urn:miriam:refseq:NM_014584;urn:miriam:uniprot:Q96HE7;urn:miriam:uniprot:Q96HE7;urn:miriam:ncbigene:30001;urn:miriam:ncbigene:30001;urn:miriam:ec-code:1.8.4.-;urn:miriam:ensembl:ENSG00000197930"
      hgnc "HGNC_SYMBOL:ERO1A"
      map_id "M118_142"
      name "ERO1A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_0_sa212"
      uniprot "UNIPROT:Q96HE7"
    ]
    graphics [
      x 1121.4760702265748
      y 1398.9296873026296
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_142"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 223
    zlevel -1

    cd19dm [
      annotation "PUBMED:25387528;PUBMED:23027870"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_79"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_0_re366"
      uniprot "NA"
    ]
    graphics [
      x 221.47607022657485
      y 1552.4902358363702
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 224
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0018158"
      hgnc "NA"
      map_id "M118_141"
      name "hyperoxidation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_0_sa211"
      uniprot "NA"
    ]
    graphics [
      x 307.96738506637075
      y 1709.595429075633
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_141"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 225
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_188"
      target_id "M118_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 3
    target 2
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "M118_175"
      target_id "M118_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 2
    target 4
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_85"
      target_id "M118_187"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 5
    target 3
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_49"
      target_id "M118_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 6
    target 5
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_219"
      target_id "M118_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 7
    target 5
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_209"
      target_id "M118_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 8
    target 6
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_90"
      target_id "M118_219"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 6
    target 9
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_219"
      target_id "M118_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 6
    target 10
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_219"
      target_id "M118_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 6
    target 11
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_219"
      target_id "M118_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 47
    target 8
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_221"
      target_id "M118_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 217
    target 9
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_120"
      target_id "M118_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 35
    target 9
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_218"
      target_id "M118_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 9
    target 218
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_101"
      target_id "M118_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 239
    source 16
    target 10
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_116"
      target_id "M118_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 240
    source 17
    target 10
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_111"
      target_id "M118_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 241
    source 10
    target 18
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_98"
      target_id "M118_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 242
    source 12
    target 11
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_178"
      target_id "M118_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 243
    source 11
    target 13
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_41"
      target_id "M118_179"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 244
    source 13
    target 14
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_179"
      target_id "M118_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 245
    source 14
    target 15
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_42"
      target_id "M118_180"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 246
    source 216
    target 17
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_97"
      target_id "M118_111"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 247
    source 17
    target 38
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "M118_111"
      target_id "M118_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 248
    source 18
    target 19
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_115"
      target_id "M118_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 249
    source 19
    target 20
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_99"
      target_id "M118_114"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 250
    source 20
    target 21
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_114"
      target_id "M118_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 251
    source 22
    target 21
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_222"
      target_id "M118_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 252
    source 23
    target 21
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_155"
      target_id "M118_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 253
    source 21
    target 24
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_103"
      target_id "M118_224"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 254
    source 26
    target 22
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_92"
      target_id "M118_222"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 255
    source 22
    target 211
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_222"
      target_id "M118_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 256
    source 24
    target 25
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_224"
      target_id "M118_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 257
    source 24
    target 26
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_224"
      target_id "M118_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 258
    source 195
    target 25
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_223"
      target_id "M118_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 259
    source 196
    target 25
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_12"
      target_id "M118_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 260
    source 25
    target 197
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_93"
      target_id "M118_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 261
    source 27
    target 26
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_8"
      target_id "M118_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 262
    source 28
    target 26
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_15"
      target_id "M118_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 263
    source 29
    target 26
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_140"
      target_id "M118_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 264
    source 30
    target 26
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "M118_184"
      target_id "M118_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 265
    source 168
    target 27
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_25"
      target_id "M118_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 266
    source 27
    target 64
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_8"
      target_id "M118_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 267
    source 27
    target 87
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_8"
      target_id "M118_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 268
    source 191
    target 29
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_29"
      target_id "M118_140"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 269
    source 31
    target 30
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_44"
      target_id "M118_184"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 270
    source 32
    target 31
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_186"
      target_id "M118_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 271
    source 33
    target 32
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_43"
      target_id "M118_186"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 272
    source 34
    target 33
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_185"
      target_id "M118_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 273
    source 35
    target 33
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_218"
      target_id "M118_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 274
    source 36
    target 35
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_89"
      target_id "M118_218"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 275
    source 35
    target 37
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_218"
      target_id "M118_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 276
    source 35
    target 38
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_218"
      target_id "M118_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 277
    source 35
    target 39
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_218"
      target_id "M118_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 278
    source 85
    target 36
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_217"
      target_id "M118_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 57
    target 37
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_152"
      target_id "M118_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 58
    target 37
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_19"
      target_id "M118_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 56
    target 37
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_17"
      target_id "M118_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 54
    target 37
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_16"
      target_id "M118_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 37
    target 49
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_33"
      target_id "M118_139"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 44
    target 38
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_220"
      target_id "M118_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 45
    target 38
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "M118_216"
      target_id "M118_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 46
    target 38
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_149"
      target_id "M118_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 38
    target 47
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_91"
      target_id "M118_221"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 40
    target 39
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_203"
      target_id "M118_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 39
    target 41
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_54"
      target_id "M118_202"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 41
    target 42
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_202"
      target_id "M118_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 42
    target 43
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_55"
      target_id "M118_204"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 45
    target 50
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "M118_216"
      target_id "M118_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 45
    target 51
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "M118_216"
      target_id "M118_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 48
    target 46
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_34"
      target_id "M118_149"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 49
    target 48
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_139"
      target_id "M118_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 52
    target 50
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_206"
      target_id "M118_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 55
    target 50
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_177"
      target_id "M118_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 50
    target 56
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_39"
      target_id "M118_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 52
    target 51
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_206"
      target_id "M118_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 53
    target 51
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_176"
      target_id "M118_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 51
    target 54
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_38"
      target_id "M118_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 59
    target 58
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_32"
      target_id "M118_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 58
    target 60
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_19"
      target_id "M118_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 304
    source 58
    target 61
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_19"
      target_id "M118_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 305
    source 58
    target 62
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_19"
      target_id "M118_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 306
    source 180
    target 59
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_6"
      target_id "M118_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 307
    source 181
    target 59
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_138"
      target_id "M118_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 308
    source 182
    target 59
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_5"
      target_id "M118_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 309
    source 183
    target 59
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_7"
      target_id "M118_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 310
    source 184
    target 59
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "M118_131"
      target_id "M118_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 311
    source 185
    target 59
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "M118_132"
      target_id "M118_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 312
    source 167
    target 59
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "M118_143"
      target_id "M118_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 313
    source 186
    target 59
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_173"
      target_id "M118_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 314
    source 187
    target 59
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_174"
      target_id "M118_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 315
    source 179
    target 60
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_118"
      target_id "M118_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 316
    source 60
    target 178
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_45"
      target_id "M118_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 317
    source 177
    target 61
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_161"
      target_id "M118_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 318
    source 178
    target 61
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_117"
      target_id "M118_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 319
    source 61
    target 101
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_100"
      target_id "M118_162"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 320
    source 62
    target 63
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_72"
      target_id "M118_159"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 321
    source 64
    target 63
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_71"
      target_id "M118_159"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 322
    source 65
    target 63
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_73"
      target_id "M118_159"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 323
    source 66
    target 63
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_70"
      target_id "M118_159"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 324
    source 67
    target 63
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_57"
      target_id "M118_159"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 325
    source 85
    target 65
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_217"
      target_id "M118_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 326
    source 69
    target 66
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_158"
      target_id "M118_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 327
    source 68
    target 67
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_212"
      target_id "M118_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 328
    source 70
    target 69
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_81"
      target_id "M118_158"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 329
    source 71
    target 69
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_68"
      target_id "M118_158"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 330
    source 69
    target 72
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_158"
      target_id "M118_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 331
    source 174
    target 70
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_160"
      target_id "M118_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 332
    source 170
    target 71
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_183"
      target_id "M118_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 333
    source 72
    target 73
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_69"
      target_id "M118_190"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 334
    source 73
    target 74
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_190"
      target_id "M118_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 335
    source 73
    target 75
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_190"
      target_id "M118_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 336
    source 73
    target 76
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_190"
      target_id "M118_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 337
    source 74
    target 167
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_65"
      target_id "M118_143"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 338
    source 94
    target 75
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_129"
      target_id "M118_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 339
    source 75
    target 95
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_106"
      target_id "M118_128"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 340
    source 77
    target 76
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_157"
      target_id "M118_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 341
    source 78
    target 76
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "M118_130"
      target_id "M118_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 342
    source 76
    target 79
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_86"
      target_id "M118_189"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 343
    source 90
    target 77
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_22"
      target_id "M118_157"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 344
    source 77
    target 86
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_157"
      target_id "M118_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 345
    source 86
    target 78
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_64"
      target_id "M118_130"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 346
    source 87
    target 78
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_24"
      target_id "M118_130"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 347
    source 78
    target 88
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_130"
      target_id "M118_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 348
    source 79
    target 80
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_189"
      target_id "M118_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 349
    source 81
    target 80
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_199"
      target_id "M118_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 350
    source 80
    target 82
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_87"
      target_id "M118_208"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 351
    source 82
    target 83
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_208"
      target_id "M118_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 352
    source 84
    target 83
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_205"
      target_id "M118_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 353
    source 83
    target 85
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_88"
      target_id "M118_217"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 354
    source 88
    target 89
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_26"
      target_id "M118_133"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 355
    source 91
    target 90
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_4"
      target_id "M118_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 356
    source 92
    target 90
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_207"
      target_id "M118_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 357
    source 90
    target 93
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_22"
      target_id "M118_163"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 358
    source 95
    target 96
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_128"
      target_id "M118_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 359
    source 97
    target 96
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_126"
      target_id "M118_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 360
    source 96
    target 98
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_105"
      target_id "M118_127"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 361
    source 98
    target 99
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_127"
      target_id "M118_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 362
    source 100
    target 99
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_164"
      target_id "M118_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 363
    source 101
    target 99
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_162"
      target_id "M118_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 364
    source 99
    target 102
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_46"
      target_id "M118_165"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 365
    source 101
    target 158
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_162"
      target_id "M118_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 366
    source 102
    target 103
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_165"
      target_id "M118_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 367
    source 102
    target 104
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_165"
      target_id "M118_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 368
    source 102
    target 105
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_165"
      target_id "M118_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 369
    source 102
    target 106
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_165"
      target_id "M118_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 370
    source 153
    target 103
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_168"
      target_id "M118_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 371
    source 103
    target 150
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_36"
      target_id "M118_169"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 372
    source 173
    target 104
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_122"
      target_id "M118_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 373
    source 157
    target 104
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_167"
      target_id "M118_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 374
    source 104
    target 123
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_104"
      target_id "M118_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 375
    source 171
    target 105
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_150"
      target_id "M118_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 376
    source 156
    target 105
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_214"
      target_id "M118_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 377
    source 105
    target 172
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_28"
      target_id "M118_151"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 378
    source 107
    target 106
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_170"
      target_id "M118_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 379
    source 106
    target 108
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_37"
      target_id "M118_171"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 380
    source 108
    target 109
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_171"
      target_id "M118_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 381
    source 109
    target 110
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_75"
      target_id "M118_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 382
    source 111
    target 110
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_77"
      target_id "M118_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 383
    source 112
    target 110
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_82"
      target_id "M118_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 384
    source 113
    target 110
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_59"
      target_id "M118_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 385
    source 114
    target 110
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_66"
      target_id "M118_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 386
    source 115
    target 110
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_60"
      target_id "M118_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 387
    source 116
    target 110
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_74"
      target_id "M118_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 388
    source 117
    target 110
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_78"
      target_id "M118_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 389
    source 118
    target 110
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_62"
      target_id "M118_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 390
    source 119
    target 110
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_76"
      target_id "M118_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 391
    source 110
    target 120
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_172"
      target_id "M118_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 392
    source 139
    target 111
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_196"
      target_id "M118_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 393
    source 121
    target 112
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_125"
      target_id "M118_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 394
    source 157
    target 113
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_167"
      target_id "M118_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 395
    source 160
    target 114
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_10"
      target_id "M118_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 396
    source 154
    target 115
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_215"
      target_id "M118_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 397
    source 150
    target 116
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_169"
      target_id "M118_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 398
    source 141
    target 117
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_198"
      target_id "M118_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 399
    source 123
    target 118
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_123"
      target_id "M118_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 400
    source 122
    target 119
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_342"
      target_id "M118_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 401
    source 120
    target 121
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_80"
      target_id "M118_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 402
    source 123
    target 124
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_123"
      target_id "M118_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 403
    source 124
    target 125
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_63"
      target_id "M118_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 404
    source 125
    target 126
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_124"
      target_id "M118_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 405
    source 126
    target 127
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_56"
      target_id "M118_148"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 406
    source 128
    target 127
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_67"
      target_id "M118_148"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 407
    source 127
    target 129
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_148"
      target_id "M118_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 408
    source 131
    target 128
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_135"
      target_id "M118_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 409
    source 130
    target 129
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_200"
      target_id "M118_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 410
    source 131
    target 129
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_135"
      target_id "M118_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 411
    source 129
    target 132
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_53"
      target_id "M118_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 412
    source 142
    target 131
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_23"
      target_id "M118_135"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 413
    source 132
    target 133
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_18"
      target_id "M118_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 414
    source 134
    target 133
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_193"
      target_id "M118_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 415
    source 133
    target 135
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_50"
      target_id "M118_194"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 416
    source 135
    target 136
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_194"
      target_id "M118_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 417
    source 135
    target 137
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_194"
      target_id "M118_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 418
    source 140
    target 136
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_197"
      target_id "M118_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 419
    source 136
    target 141
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_52"
      target_id "M118_198"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 420
    source 138
    target 137
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_195"
      target_id "M118_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 421
    source 137
    target 139
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_51"
      target_id "M118_196"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 422
    source 143
    target 142
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_134"
      target_id "M118_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 423
    source 144
    target 142
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_181"
      target_id "M118_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 424
    source 145
    target 142
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_182"
      target_id "M118_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 425
    source 146
    target 142
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_192"
      target_id "M118_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 426
    source 144
    target 147
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_181"
      target_id "M118_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 427
    source 147
    target 146
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_48"
      target_id "M118_192"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 428
    source 148
    target 147
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_191"
      target_id "M118_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 429
    source 149
    target 147
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_211"
      target_id "M118_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 430
    source 150
    target 151
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_169"
      target_id "M118_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 431
    source 152
    target 151
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_201"
      target_id "M118_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 432
    source 151
    target 153
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_40"
      target_id "M118_168"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 433
    source 155
    target 154
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_84"
      target_id "M118_215"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 434
    source 156
    target 155
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_214"
      target_id "M118_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 435
    source 157
    target 155
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_167"
      target_id "M118_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 436
    source 158
    target 157
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_47"
      target_id "M118_167"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 437
    source 159
    target 158
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_166"
      target_id "M118_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 438
    source 161
    target 160
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_83"
      target_id "M118_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 439
    source 162
    target 161
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_9"
      target_id "M118_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 440
    source 163
    target 161
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_144"
      target_id "M118_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 441
    source 164
    target 161
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_145"
      target_id "M118_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 442
    source 165
    target 161
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_146"
      target_id "M118_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 443
    source 166
    target 161
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_147"
      target_id "M118_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 444
    source 167
    target 161
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_143"
      target_id "M118_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 445
    source 167
    target 168
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_143"
      target_id "M118_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 446
    source 169
    target 168
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_136"
      target_id "M118_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 447
    source 170
    target 168
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_183"
      target_id "M118_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 448
    source 175
    target 174
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_58"
      target_id "M118_160"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 449
    source 176
    target 175
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_213"
      target_id "M118_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 450
    source 188
    target 180
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_35"
      target_id "M118_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 451
    source 189
    target 188
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_107"
      target_id "M118_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 452
    source 190
    target 188
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_137"
      target_id "M118_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 453
    source 192
    target 191
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_153"
      target_id "M118_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 454
    source 193
    target 191
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_154"
      target_id "M118_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 455
    source 194
    target 191
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "M118_210"
      target_id "M118_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 456
    source 195
    target 211
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_223"
      target_id "M118_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 457
    source 196
    target 211
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_12"
      target_id "M118_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 458
    source 197
    target 198
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_11"
      target_id "M118_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 459
    source 199
    target 198
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_156"
      target_id "M118_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 460
    source 198
    target 200
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_30"
      target_id "M118_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 461
    source 201
    target 200
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_95"
      target_id "M118_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 462
    source 200
    target 202
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_20"
      target_id "M118_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 463
    source 205
    target 201
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_21"
      target_id "M118_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 464
    source 209
    target 201
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_110"
      target_id "M118_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 465
    source 201
    target 210
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_95"
      target_id "M118_225"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 466
    source 203
    target 202
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_109"
      target_id "M118_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 467
    source 204
    target 202
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_14"
      target_id "M118_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 468
    source 202
    target 205
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_94"
      target_id "M118_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 469
    source 202
    target 206
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_94"
      target_id "M118_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 470
    source 205
    target 207
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_21"
      target_id "M118_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 471
    source 207
    target 208
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_61"
      target_id "M118_226"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 472
    source 211
    target 212
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_31"
      target_id "M118_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 473
    source 212
    target 213
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_13"
      target_id "M118_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 474
    source 214
    target 213
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_112"
      target_id "M118_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 475
    source 213
    target 215
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_96"
      target_id "M118_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 476
    source 215
    target 216
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_113"
      target_id "M118_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 477
    source 218
    target 219
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_121"
      target_id "M118_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 478
    source 219
    target 220
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_102"
      target_id "M118_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 479
    source 220
    target 221
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_119"
      target_id "M118_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 480
    source 221
    target 222
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_27"
      target_id "M118_142"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 481
    source 222
    target 223
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_142"
      target_id "M118_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 482
    source 223
    target 224
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_79"
      target_id "M118_141"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
