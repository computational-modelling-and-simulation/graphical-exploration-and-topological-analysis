# generated with VANTED V2.8.2 at Fri Mar 04 10:04:38 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_9"
      name "p38_minus_NFkB"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa20"
      uniprot "NA"
    ]
    graphics [
      x 1358.8536650413896
      y 912.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:25045870"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_51"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re43"
      uniprot "NA"
    ]
    graphics [
      x 1568.8536650413898
      y 732.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:Q14653;urn:miriam:hgnc:6118;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661;urn:miriam:ncbigene:3661"
      hgnc "HGNC_SYMBOL:IRF3"
      map_id "M120_117"
      name "IRF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa89"
      uniprot "UNIPROT:Q14653"
    ]
    graphics [
      x 1601.4760702265748
      y 1482.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:Q14653;urn:miriam:hgnc:6118;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661;urn:miriam:ncbigene:3661"
      hgnc "HGNC_SYMBOL:IRF3"
      map_id "M120_20"
      name "p38_minus_NFkB"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa41"
      uniprot "UNIPROT:Q14653"
    ]
    graphics [
      x 2048.85366504139
      y 760.1506240017071
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_46"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re36"
      uniprot "NA"
    ]
    graphics [
      x 1481.4760702265748
      y 1414.644989943368
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_118"
      name "IFN_minus_III"
      node_subtype "GENE"
      node_type "species"
      org_id "sa90"
      uniprot "NA"
    ]
    graphics [
      x 1462.5144998161113
      y 1992.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ncbigene:3659;urn:miriam:ncbigene:3659;urn:miriam:hgnc:6116;urn:miriam:hgnc.symbol:IRF1;urn:miriam:hgnc.symbol:IRF1;urn:miriam:refseq:NM_002198;urn:miriam:uniprot:P10914;urn:miriam:uniprot:P10914;urn:miriam:ensembl:ENSG00000125347"
      hgnc "HGNC_SYMBOL:IRF1"
      map_id "M120_120"
      name "IRF1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa92"
      uniprot "UNIPROT:P10914"
    ]
    graphics [
      x 1747.9673850663708
      y 1645.9277406352314
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_119"
      name "IFN_minus_III"
      node_subtype "RNA"
      node_type "species"
      org_id "sa91"
      uniprot "NA"
    ]
    graphics [
      x 1208.8536650413898
      y 699.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_44"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re34"
      uniprot "NA"
    ]
    graphics [
      x 1151.4760702265748
      y 1045.0176816601365
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_124"
      name "IFN_minus_III"
      node_subtype "RNA"
      node_type "species"
      org_id "sa96"
      uniprot "NA"
    ]
    graphics [
      x 1391.4760702265748
      y 1523.8937412551359
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_124"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_45"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re35"
      uniprot "NA"
    ]
    graphics [
      x 941.4760702265747
      y 1188.1386971045129
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_125"
      name "IFN_minus_III"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa97"
      uniprot "NA"
    ]
    graphics [
      x 937.9673850663706
      y 1636.6019746441584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_125"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_47"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re37"
      uniprot "NA"
    ]
    graphics [
      x 958.0098036119548
      y 2348.0081658390973
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:Q8IZI9;urn:miriam:uniprot:Q8IZI9;urn:miriam:refseq:NM_172139;urn:miriam:ncbigene:282617;urn:miriam:ncbigene:282617;urn:miriam:hgnc.symbol:IFNL3;urn:miriam:hgnc.symbol:IFNL3;urn:miriam:hgnc:18365;urn:miriam:ensembl:ENSG00000197110;urn:miriam:refseq:NM_172138;urn:miriam:hgnc.symbol:IFNL2;urn:miriam:uniprot:Q8IZJ0;urn:miriam:uniprot:Q8IZJ0;urn:miriam:hgnc.symbol:IFNL2;urn:miriam:hgnc:18364;urn:miriam:ncbigene:282616;urn:miriam:ensembl:ENSG00000183709;urn:miriam:ncbigene:282616;urn:miriam:uniprot:Q8IU54;urn:miriam:uniprot:Q8IU54;urn:miriam:ncbigene:282618;urn:miriam:refseq:NM_172140;urn:miriam:ncbigene:282618;urn:miriam:ensembl:ENSG00000182393;urn:miriam:hgnc.symbol:IFNL1;urn:miriam:hgnc.symbol:IFNL1;urn:miriam:hgnc:18363;urn:miriam:ensembl:ENSG00000272395;urn:miriam:hgnc.symbol:IFNL4;urn:miriam:hgnc.symbol:IFNL4;urn:miriam:hgnc:44480;urn:miriam:ncbigene:101180976;urn:miriam:uniprot:K9M1U5;urn:miriam:uniprot:K9M1U5;urn:miriam:ncbigene:101180976;urn:miriam:refseq:NM_001276254"
      hgnc "HGNC_SYMBOL:IFNL3;HGNC_SYMBOL:IFNL2;HGNC_SYMBOL:IFNL1;HGNC_SYMBOL:IFNL4"
      map_id "M120_28"
      name "Ifn_space_lambda"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa60"
      uniprot "UNIPROT:Q8IZI9;UNIPROT:Q8IZJ0;UNIPROT:Q8IU54;UNIPROT:K9M1U5"
    ]
    graphics [
      x 1397.7845707541828
      y 2082.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_71"
      name "NA"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "re66"
      uniprot "NA"
    ]
    graphics [
      x 907.9673850663706
      y 1498.7829900398954
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_32"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re12"
      uniprot "NA"
    ]
    graphics [
      x 431.47607022657473
      y 1352.5572932182372
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:hgnc.symbol:JAK1;urn:miriam:hgnc.symbol:JAK1;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc:6190;urn:miriam:ncbigene:3716;urn:miriam:ncbigene:3716;urn:miriam:ensembl:ENSG00000162434;urn:miriam:uniprot:P23458;urn:miriam:uniprot:P23458;urn:miriam:refseq:NM_002227;urn:miriam:ec-code:2.7.10.2;urn:miriam:ncbigene:7297;urn:miriam:ncbigene:7297;urn:miriam:ensembl:ENSG00000105397;urn:miriam:refseq:NM_001385197;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc:12440;urn:miriam:uniprot:P29597;urn:miriam:uniprot:P29597;urn:miriam:ncbigene:163702;urn:miriam:ncbigene:163702;urn:miriam:hgnc.symbol:IFNLR1;urn:miriam:uniprot:Q8IU57;urn:miriam:uniprot:Q8IU57;urn:miriam:hgnc.symbol:IFNLR1;urn:miriam:ensembl:ENSG00000185436;urn:miriam:hgnc:18584;urn:miriam:refseq:NM_170743"
      hgnc "HGNC_SYMBOL:JAK1;HGNC_SYMBOL:TYK2;HGNC_SYMBOL:IFNLR1"
      map_id "M120_29"
      name "receptor_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa9"
      uniprot "UNIPROT:P23458;UNIPROT:P29597;UNIPROT:Q8IU57"
    ]
    graphics [
      x 938.8536650413897
      y 818.9376478735267
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ncbigene:163702;urn:miriam:ncbigene:163702;urn:miriam:hgnc.symbol:IFNLR1;urn:miriam:uniprot:Q8IU57;urn:miriam:uniprot:Q8IU57;urn:miriam:hgnc.symbol:IFNLR1;urn:miriam:ensembl:ENSG00000185436;urn:miriam:hgnc:18584;urn:miriam:refseq:NM_170743;urn:miriam:hgnc.symbol:JAK1;urn:miriam:hgnc.symbol:JAK1;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc:6190;urn:miriam:ncbigene:3716;urn:miriam:ncbigene:3716;urn:miriam:ensembl:ENSG00000162434;urn:miriam:uniprot:P23458;urn:miriam:uniprot:P23458;urn:miriam:refseq:NM_002227;urn:miriam:ec-code:2.7.10.2;urn:miriam:ncbigene:7297;urn:miriam:ncbigene:7297;urn:miriam:ensembl:ENSG00000105397;urn:miriam:refseq:NM_001385197;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc:12440;urn:miriam:uniprot:P29597;urn:miriam:uniprot:P29597"
      hgnc "HGNC_SYMBOL:IFNLR1;HGNC_SYMBOL:JAK1;HGNC_SYMBOL:TYK2"
      map_id "M120_1"
      name "receptor_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa10"
      uniprot "UNIPROT:Q8IU57;UNIPROT:P23458;UNIPROT:P29597"
    ]
    graphics [
      x 521.4760702265747
      y 1064.0694246691999
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_65"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re59"
      uniprot "NA"
    ]
    graphics [
      x 1272.850723140486
      y 462.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_64"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re58"
      uniprot "NA"
    ]
    graphics [
      x 1151.4760702265748
      y 1362.428070072417
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:O14543;urn:miriam:uniprot:O14543;urn:miriam:hgnc:19391;urn:miriam:ncbigene:9021;urn:miriam:ncbigene:9021;urn:miriam:ensembl:ENSG00000184557;urn:miriam:refseq:NM_001378932;urn:miriam:hgnc.symbol:SOCS3;urn:miriam:hgnc.symbol:SOCS3"
      hgnc "HGNC_SYMBOL:SOCS3"
      map_id "M120_100"
      name "SOCS3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa46"
      uniprot "UNIPROT:O14543"
    ]
    graphics [
      x 1508.8536650413898
      y 882.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:O14543;urn:miriam:uniprot:O14543;urn:miriam:hgnc:19391;urn:miriam:ncbigene:9021;urn:miriam:ncbigene:9021;urn:miriam:ensembl:ENSG00000184557;urn:miriam:refseq:NM_001378932;urn:miriam:hgnc.symbol:SOCS3;urn:miriam:hgnc.symbol:SOCS3;urn:miriam:ec-code:2.7.10.2;urn:miriam:ncbigene:7297;urn:miriam:ncbigene:7297;urn:miriam:ensembl:ENSG00000105397;urn:miriam:refseq:NM_001385197;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc:12440;urn:miriam:uniprot:P29597;urn:miriam:uniprot:P29597;urn:miriam:hgnc.symbol:JAK1;urn:miriam:hgnc.symbol:JAK1;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc:6190;urn:miriam:ncbigene:3716;urn:miriam:ncbigene:3716;urn:miriam:ensembl:ENSG00000162434;urn:miriam:uniprot:P23458;urn:miriam:uniprot:P23458;urn:miriam:refseq:NM_002227;urn:miriam:ncbigene:163702;urn:miriam:ncbigene:163702;urn:miriam:hgnc.symbol:IFNLR1;urn:miriam:uniprot:Q8IU57;urn:miriam:uniprot:Q8IU57;urn:miriam:hgnc.symbol:IFNLR1;urn:miriam:ensembl:ENSG00000185436;urn:miriam:hgnc:18584;urn:miriam:refseq:NM_170743"
      hgnc "HGNC_SYMBOL:SOCS3;HGNC_SYMBOL:TYK2;HGNC_SYMBOL:JAK1;HGNC_SYMBOL:IFNLR1"
      map_id "M120_2"
      name "receptor_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa11"
      uniprot "UNIPROT:O14543;UNIPROT:P29597;UNIPROT:P23458;UNIPROT:Q8IU57"
    ]
    graphics [
      x 742.5144998161113
      y 2031.2063392817126
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_68"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re62"
      uniprot "NA"
    ]
    graphics [
      x 1228.3879559344089
      y 2322.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_90"
      name "s227"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa138"
      uniprot "NA"
    ]
    graphics [
      x 1150.4146427238684
      y 2139.0166269831952
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:O15524;urn:miriam:uniprot:O15524;urn:miriam:ncbigene:8651;urn:miriam:ncbigene:8651;urn:miriam:ensembl:ENSG00000185338;urn:miriam:hgnc:19383;urn:miriam:hgnc.symbol:SOCS1;urn:miriam:hgnc.symbol:SOCS1;urn:miriam:refseq:NM_003745"
      hgnc "HGNC_SYMBOL:SOCS1"
      map_id "M120_99"
      name "SOCS1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa45"
      uniprot "UNIPROT:O15524"
    ]
    graphics [
      x 1271.4760702265748
      y 1242.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:hgnc.symbol:JAK1;urn:miriam:hgnc.symbol:JAK1;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc:6190;urn:miriam:ncbigene:3716;urn:miriam:ncbigene:3716;urn:miriam:ensembl:ENSG00000162434;urn:miriam:uniprot:P23458;urn:miriam:uniprot:P23458;urn:miriam:refseq:NM_002227;urn:miriam:ec-code:2.7.10.2;urn:miriam:ncbigene:7297;urn:miriam:ncbigene:7297;urn:miriam:ensembl:ENSG00000105397;urn:miriam:refseq:NM_001385197;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc:12440;urn:miriam:uniprot:P29597;urn:miriam:uniprot:P29597;urn:miriam:ncbigene:163702;urn:miriam:ncbigene:163702;urn:miriam:hgnc.symbol:IFNLR1;urn:miriam:uniprot:Q8IU57;urn:miriam:uniprot:Q8IU57;urn:miriam:hgnc.symbol:IFNLR1;urn:miriam:ensembl:ENSG00000185436;urn:miriam:hgnc:18584;urn:miriam:refseq:NM_170743;urn:miriam:uniprot:O15524;urn:miriam:uniprot:O15524;urn:miriam:ncbigene:8651;urn:miriam:ncbigene:8651;urn:miriam:ensembl:ENSG00000185338;urn:miriam:hgnc:19383;urn:miriam:hgnc.symbol:SOCS1;urn:miriam:hgnc.symbol:SOCS1;urn:miriam:refseq:NM_003745"
      hgnc "HGNC_SYMBOL:JAK1;HGNC_SYMBOL:TYK2;HGNC_SYMBOL:IFNLR1;HGNC_SYMBOL:SOCS1"
      map_id "M120_14"
      name "receptor_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa34"
      uniprot "UNIPROT:P23458;UNIPROT:P29597;UNIPROT:Q8IU57;UNIPROT:O15524"
    ]
    graphics [
      x 1418.8536650413896
      y 762.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_67"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re61"
      uniprot "NA"
    ]
    graphics [
      x 848.8536650413897
      y 584.3289684540376
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_89"
      name "s226"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa137"
      uniprot "NA"
    ]
    graphics [
      x 578.8536650413897
      y 623.4568412849098
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_57"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re49"
      uniprot "NA"
    ]
    graphics [
      x 1988.8536650413898
      y 968.5267273646541
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ncbigene:3659;urn:miriam:ncbigene:3659;urn:miriam:hgnc:6116;urn:miriam:hgnc.symbol:IRF1;urn:miriam:hgnc.symbol:IRF1;urn:miriam:refseq:NM_002198;urn:miriam:uniprot:P10914;urn:miriam:uniprot:P10914;urn:miriam:ensembl:ENSG00000125347"
      hgnc "HGNC_SYMBOL:IRF1"
      map_id "M120_86"
      name "IRF1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa127"
      uniprot "UNIPROT:P10914"
    ]
    graphics [
      x 1766.090149807047
      y 413.5226115847969
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_56"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re48"
      uniprot "NA"
    ]
    graphics [
      x 1186.8842999622784
      y 369.7251104092877
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ncbigene:3659;urn:miriam:ncbigene:3659;urn:miriam:hgnc:6116;urn:miriam:hgnc.symbol:IRF1;urn:miriam:hgnc.symbol:IRF1;urn:miriam:refseq:NM_002198;urn:miriam:uniprot:P10914;urn:miriam:uniprot:P10914;urn:miriam:ensembl:ENSG00000125347"
      hgnc "HGNC_SYMBOL:IRF1"
      map_id "M120_123"
      name "IRF1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa95"
      uniprot "UNIPROT:P10914"
    ]
    graphics [
      x 971.4760702265747
      y 1138.6039843924614
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "PUBMED:25045870"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_43"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re32"
      uniprot "NA"
    ]
    graphics [
      x 820.4146427238683
      y 2203.853912881765
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ncbigene:3659;urn:miriam:ncbigene:3659;urn:miriam:hgnc:6116;urn:miriam:hgnc.symbol:IRF1;urn:miriam:hgnc.symbol:IRF1;urn:miriam:refseq:NM_002198;urn:miriam:uniprot:P10914;urn:miriam:uniprot:P10914;urn:miriam:ensembl:ENSG00000125347"
      hgnc "HGNC_SYMBOL:IRF1"
      map_id "M120_122"
      name "IRF1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa94"
      uniprot "UNIPROT:P10914"
    ]
    graphics [
      x 1025.6939429587637
      y 2401.959114654641
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ncbigene:23586;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:hgnc.symbol:DDX58;urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746"
      hgnc "HGNC_SYMBOL:DDX58;HGNC_SYMBOL:MAVS"
      map_id "M120_10"
      name "RIG_minus_I_minus_MAVS"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa21"
      uniprot "UNIPROT:O95786;UNIPROT:Q7Z434"
    ]
    graphics [
      x 1492.5144998161113
      y 2172.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "PUBMED:25636800"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_42"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re31"
      uniprot "NA"
    ]
    graphics [
      x 1942.5144998161113
      y 1972.9511141719918
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ncbigene:23586;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:hgnc.symbol:DDX58;urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746"
      hgnc "HGNC_SYMBOL:DDX58;HGNC_SYMBOL:MAVS"
      map_id "M120_11"
      name "RIG_minus_I_minus_MAVS"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa22"
      uniprot "UNIPROT:O95786;UNIPROT:Q7Z434"
    ]
    graphics [
      x 2350.4146427238684
      y 2234.7512179084115
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_121"
      name "dsRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa93"
      uniprot "NA"
    ]
    graphics [
      x 1460.0074341427699
      y 2322.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "PUBMED:25636800"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_55"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re47"
      uniprot "NA"
    ]
    graphics [
      x 1642.5144998161113
      y 2022.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:Q14653;urn:miriam:hgnc:6118;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661;urn:miriam:ncbigene:3661"
      hgnc "HGNC_SYMBOL:IRF3"
      map_id "M120_85"
      name "IRF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa126"
      uniprot "UNIPROT:Q14653"
    ]
    graphics [
      x 742.5144998161113
      y 1895.224839764994
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "PUBMED:25636800"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_54"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re46"
      uniprot "NA"
    ]
    graphics [
      x 1612.5144998161113
      y 1842.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:Q14653;urn:miriam:hgnc:6118;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661;urn:miriam:ncbigene:3661"
      hgnc "HGNC_SYMBOL:IRF3"
      map_id "M120_116"
      name "IRF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa88"
      uniprot "UNIPROT:Q14653"
    ]
    graphics [
      x 2557.9673850663708
      y 1707.9464015952776
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "PUBMED:25636800"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_40"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re28"
      uniprot "NA"
    ]
    graphics [
      x 2737.9673850663708
      y 1785.8427066162358
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:Q14653;urn:miriam:hgnc:6118;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661;urn:miriam:ncbigene:3661"
      hgnc "HGNC_SYMBOL:IRF3"
      map_id "M120_115"
      name "IRF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa87"
      uniprot "UNIPROT:Q14653"
    ]
    graphics [
      x 1990.4146427238684
      y 2267.606843746084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:hgnc.symbol:CHUK;urn:miriam:ec-code:2.7.11.10;urn:miriam:ec-code:2.7.11.1;urn:miriam:uniprot:O14920;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:ncbigene:3551;urn:miriam:uniprot:O15111;urn:miriam:ncbigene:1147;urn:miriam:ncbigene:23586;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:hgnc.symbol:DDX58;urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110;urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746"
      hgnc "HGNC_SYMBOL:CHUK;HGNC_SYMBOL:IKBKB;HGNC_SYMBOL:DDX58;HGNC_SYMBOL:TBK1;HGNC_SYMBOL:MAVS"
      map_id "M120_7"
      name "RIG_minus_I_minus_MAVS"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa17"
      uniprot "UNIPROT:O14920;UNIPROT:O15111;UNIPROT:O95786;UNIPROT:Q9UHD2;UNIPROT:Q7Z434"
    ]
    graphics [
      x 2648.85366504139
      y 980.1666353558861
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      annotation "PUBMED:25636800"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_50"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re40"
      uniprot "NA"
    ]
    graphics [
      x 2711.476070226575
      y 1280.1339051066395
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746;urn:miriam:ncbigene:23586;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:hgnc.symbol:DDX58"
      hgnc "HGNC_SYMBOL:MAVS;HGNC_SYMBOL:DDX58"
      map_id "M120_12"
      name "RIG_minus_I_minus_mitoMAVS"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa23"
      uniprot "UNIPROT:Q7Z434;UNIPROT:O95786"
    ]
    graphics [
      x 2152.5144998161113
      y 1904.7194631037569
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:hgnc.symbol:CHUK;urn:miriam:ec-code:2.7.11.10;urn:miriam:ec-code:2.7.11.1;urn:miriam:uniprot:O14920;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:ncbigene:3551;urn:miriam:uniprot:O15111;urn:miriam:ncbigene:1147;urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110"
      hgnc "HGNC_SYMBOL:CHUK;HGNC_SYMBOL:IKBKB;HGNC_SYMBOL:TBK1"
      map_id "M120_8"
      name "IKK_minus_TBK1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa18"
      uniprot "UNIPROT:O14920;UNIPROT:O15111;UNIPROT:Q9UHD2"
    ]
    graphics [
      x 1667.2023363703734
      y 999.5710630443609
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "PUBMED:25636800"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_39"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re27"
      uniprot "NA"
    ]
    graphics [
      x 2392.5144998161113
      y 1895.2368603913133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:hgnc.symbol:CHUK;urn:miriam:ec-code:2.7.11.10;urn:miriam:ec-code:2.7.11.1;urn:miriam:uniprot:O14920;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:ncbigene:3551;urn:miriam:uniprot:O15111;urn:miriam:ncbigene:1147"
      hgnc "HGNC_SYMBOL:CHUK;HGNC_SYMBOL:IKBKB"
      map_id "M120_113"
      name "IKK"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa85"
      uniprot "UNIPROT:O14920;UNIPROT:O15111"
    ]
    graphics [
      x 2062.5144998161113
      y 2063.3608844567657
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110"
      hgnc "HGNC_SYMBOL:TBK1"
      map_id "M120_114"
      name "TBK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa86"
      uniprot "UNIPROT:Q9UHD2"
    ]
    graphics [
      x 1957.9673850663708
      y 1660.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "PUBMED:25045870"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_73"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re69"
      uniprot "NA"
    ]
    graphics [
      x 997.9673850663706
      y 1559.2696849278589
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ncbigene:23586;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:hgnc.symbol:DDX58"
      hgnc "HGNC_SYMBOL:DDX58"
      map_id "M120_17"
      name "RIG_minus_I:dsRNA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa37"
      uniprot "UNIPROT:O95786"
    ]
    graphics [
      x 787.9673850663705
      y 1577.1654913580128
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746"
      hgnc "HGNC_SYMBOL:MAVS"
      map_id "M120_80"
      name "MAVS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa100"
      uniprot "UNIPROT:Q7Z434"
    ]
    graphics [
      x 1118.8536650413898
      y 990.5543133122671
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "PUBMED:22390971"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_75"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re70"
      uniprot "NA"
    ]
    graphics [
      x 1252.5144998161113
      y 1902.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "PUBMED:25554382"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_76"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re71"
      uniprot "NA"
    ]
    graphics [
      x 2171.476070226575
      y 1216.3972847319633
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M120_81"
      name "PLPro"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa101"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 2591.476070226575
      y 1375.4579532328696
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ncbigene:23586;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:hgnc.symbol:DDX58"
      hgnc "HGNC_SYMBOL:DDX58"
      map_id "M120_16"
      name "RIG_minus_I:dsRNA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa36"
      uniprot "UNIPROT:O95786"
    ]
    graphics [
      x 1777.9673850663708
      y 1601.4892767681847
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      annotation "PUBMED:25045870"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_48"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re38"
      uniprot "NA"
    ]
    graphics [
      x 1701.6104592494069
      y 1784.5562612679125
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ncbigene:23586;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:hgnc.symbol:DDX58"
      hgnc "HGNC_SYMBOL:DDX58"
      map_id "M120_126"
      name "RIG_minus_I"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa99"
      uniprot "UNIPROT:O95786"
    ]
    graphics [
      x 1211.4760702265748
      y 1179.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_112"
      name "dsRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa82"
      uniprot "NA"
    ]
    graphics [
      x 2321.476070226575
      y 1449.2054894529092
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_53"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re45"
      uniprot "NA"
    ]
    graphics [
      x 1631.4760702265748
      y 1307.5725709100786
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_58"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re50"
      uniprot "NA"
    ]
    graphics [
      x 1058.8536650413898
      y 979.5029478087847
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_52"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re44"
      uniprot "NA"
    ]
    graphics [
      x 2407.9673850663708
      y 1635.902082728633
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_84"
      name "dsRNA_underscore_vesicle"
      node_subtype "RNA"
      node_type "species"
      org_id "sa125"
      uniprot "NA"
    ]
    graphics [
      x 1121.4760702265748
      y 1308.9296873026296
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_87"
      name "RNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa128"
      uniprot "NA"
    ]
    graphics [
      x 818.8536650413897
      y 571.8558123594818
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      annotation "PUBMED:32626922"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_72"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re68"
      uniprot "NA"
    ]
    graphics [
      x 878.8536650413897
      y 642.6549842307106
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "M120_82"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa103"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 1257.7231255590953
      y 342.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5;urn:miriam:ncbigene:23586;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:hgnc.symbol:DDX58"
      hgnc "HGNC_SYMBOL:DDX58"
      map_id "M120_15"
      name "RIG_minus_1_space_M_minus_Protein"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa35"
      uniprot "UNIPROT:P0DTC5;UNIPROT:O95786"
    ]
    graphics [
      x 984.105505353552
      y 313.4119138153844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:uniprot:Q8IUD6;urn:miriam:uniprot:Q8IUD6;urn:miriam:hgnc:21158;urn:miriam:refseq:NM_032322;urn:miriam:hgnc.symbol:RNF135;urn:miriam:hgnc.symbol:RNF135;urn:miriam:ncbigene:84282;urn:miriam:ncbigene:84282;urn:miriam:ensembl:ENSG00000181481;urn:miriam:ncbigene:7706;urn:miriam:ensembl:ENSG00000121060;urn:miriam:ncbigene:7706;urn:miriam:hgnc.symbol:TRIM25;urn:miriam:hgnc.symbol:TRIM25;urn:miriam:ec-code:2.3.2.27;urn:miriam:hgnc:12932;urn:miriam:uniprot:Q14258;urn:miriam:uniprot:Q14258;urn:miriam:refseq:NM_005082"
      hgnc "HGNC_SYMBOL:RNF135;HGNC_SYMBOL:TRIM25"
      map_id "M120_18"
      name "Riplet:TRIM25"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa38"
      uniprot "UNIPROT:Q8IUD6;UNIPROT:Q14258"
    ]
    graphics [
      x 1124.750361052548
      y 2427.2469542692015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 71
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_9"
      target_id "M120_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 72
    source 3
    target 2
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_117"
      target_id "M120_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 73
    source 2
    target 4
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_51"
      target_id "M120_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 74
    source 39
    target 3
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_55"
      target_id "M120_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 75
    source 4
    target 5
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M120_20"
      target_id "M120_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 76
    source 6
    target 5
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_118"
      target_id "M120_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 77
    source 7
    target 5
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M120_120"
      target_id "M120_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 78
    source 5
    target 8
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_46"
      target_id "M120_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 79
    source 29
    target 7
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_57"
      target_id "M120_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 8
    target 9
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_119"
      target_id "M120_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 81
    source 9
    target 10
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_44"
      target_id "M120_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 82
    source 10
    target 11
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_124"
      target_id "M120_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 83
    source 11
    target 12
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_45"
      target_id "M120_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 84
    source 12
    target 13
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_125"
      target_id "M120_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 85
    source 13
    target 14
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_47"
      target_id "M120_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 86
    source 15
    target 14
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_71"
      target_id "M120_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 87
    source 14
    target 16
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M120_28"
      target_id "M120_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 88
    source 18
    target 15
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_1"
      target_id "M120_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 89
    source 15
    target 17
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_71"
      target_id "M120_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 90
    source 17
    target 16
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_29"
      target_id "M120_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 91
    source 16
    target 18
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_32"
      target_id "M120_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 92
    source 18
    target 19
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_1"
      target_id "M120_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 93
    source 18
    target 20
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_1"
      target_id "M120_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 94
    source 25
    target 19
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_99"
      target_id "M120_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 95
    source 19
    target 26
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_65"
      target_id "M120_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 96
    source 21
    target 20
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_100"
      target_id "M120_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 97
    source 20
    target 22
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_64"
      target_id "M120_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 98
    source 22
    target 23
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_2"
      target_id "M120_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 99
    source 23
    target 24
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_68"
      target_id "M120_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 100
    source 26
    target 27
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_14"
      target_id "M120_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 27
    target 28
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_67"
      target_id "M120_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 30
    target 29
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_86"
      target_id "M120_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 31
    target 30
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_56"
      target_id "M120_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 32
    target 31
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_123"
      target_id "M120_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 33
    target 32
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_43"
      target_id "M120_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 106
    source 34
    target 33
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_122"
      target_id "M120_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 107
    source 35
    target 33
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CATALYSIS"
      source_id "M120_10"
      target_id "M120_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 108
    source 36
    target 35
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_42"
      target_id "M120_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 109
    source 37
    target 36
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_11"
      target_id "M120_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 110
    source 38
    target 36
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "TRIGGER"
      source_id "M120_121"
      target_id "M120_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 111
    source 40
    target 39
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_85"
      target_id "M120_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 112
    source 41
    target 40
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_54"
      target_id "M120_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 113
    source 42
    target 41
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_116"
      target_id "M120_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 114
    source 43
    target 42
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_40"
      target_id "M120_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 44
    target 43
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_115"
      target_id "M120_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 45
    target 43
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M120_7"
      target_id "M120_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 46
    target 45
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_50"
      target_id "M120_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 47
    target 46
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_12"
      target_id "M120_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 48
    target 46
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_8"
      target_id "M120_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 52
    target 47
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_73"
      target_id "M120_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 49
    target 48
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_39"
      target_id "M120_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 50
    target 49
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_113"
      target_id "M120_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 51
    target 49
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_114"
      target_id "M120_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 53
    target 52
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_17"
      target_id "M120_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 54
    target 52
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_80"
      target_id "M120_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 126
    source 55
    target 53
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_75"
      target_id "M120_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 127
    source 53
    target 56
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_17"
      target_id "M120_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 128
    source 58
    target 55
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_16"
      target_id "M120_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 70
    target 55
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CATALYSIS"
      source_id "M120_18"
      target_id "M120_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 130
    source 57
    target 56
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CATALYSIS"
      source_id "M120_81"
      target_id "M120_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 131
    source 56
    target 58
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_76"
      target_id "M120_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 132
    source 59
    target 58
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_48"
      target_id "M120_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 133
    source 60
    target 59
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_126"
      target_id "M120_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 134
    source 61
    target 59
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "TRIGGER"
      source_id "M120_112"
      target_id "M120_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 135
    source 60
    target 67
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_126"
      target_id "M120_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 136
    source 62
    target 61
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_53"
      target_id "M120_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 137
    source 63
    target 61
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_58"
      target_id "M120_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 138
    source 61
    target 64
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_112"
      target_id "M120_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 139
    source 65
    target 62
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_84"
      target_id "M120_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 66
    target 63
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_87"
      target_id "M120_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 64
    target 65
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_52"
      target_id "M120_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 68
    target 67
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_82"
      target_id "M120_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 67
    target 69
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_72"
      target_id "M120_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
