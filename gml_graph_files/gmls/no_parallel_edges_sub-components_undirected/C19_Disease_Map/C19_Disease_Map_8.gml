# generated with VANTED V2.8.2 at Fri Mar 04 10:04:37 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ncbigene:23476;urn:miriam:ncbigene:23476;urn:miriam:hgnc:13575;urn:miriam:refseq:NM_058243;urn:miriam:hgnc.symbol:BRD4;urn:miriam:uniprot:O60885;urn:miriam:uniprot:O60885;urn:miriam:hgnc.symbol:BRD4;urn:miriam:ensembl:ENSG00000141867"
      hgnc "HGNC_SYMBOL:BRD4"
      map_id "M17_38"
      name "BRD4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa12"
      uniprot "UNIPROT:O60885"
    ]
    graphics [
      x 698.8536650413897
      y 984.0301123317071
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re4"
      uniprot "NA"
    ]
    graphics [
      x 924.878708038158
      y 377.0151621343947
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ncbiprotein:BCD58755;urn:miriam:uniprot:E"
      hgnc "NA"
      map_id "M17_39"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa13"
      uniprot "UNIPROT:E"
    ]
    graphics [
      x 1718.8536650413898
      y 805.9784770270978
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ncbigene:23476;urn:miriam:ncbigene:23476;urn:miriam:hgnc:13575;urn:miriam:refseq:NM_058243;urn:miriam:hgnc.symbol:BRD4;urn:miriam:uniprot:O60885;urn:miriam:uniprot:O60885;urn:miriam:hgnc.symbol:BRD4;urn:miriam:ensembl:ENSG00000141867"
      hgnc "HGNC_SYMBOL:BRD4"
      map_id "M17_42"
      name "BRD4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa16"
      uniprot "UNIPROT:O60885"
    ]
    graphics [
      x 1331.4559534885318
      y 252.06502830998443
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_9"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1"
      uniprot "NA"
    ]
    graphics [
      x 2168.85366504139
      y 525.3807920432863
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_19"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re2"
      uniprot "NA"
    ]
    graphics [
      x 311.47607022657485
      y 1404.3227618731576
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_23"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re3"
      uniprot "NA"
    ]
    graphics [
      x 703.1254403246198
      y 535.3958501019754
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ncbigene:8350;urn:miriam:uniprot:P68431;urn:miriam:uniprot:P68431;urn:miriam:ncbigene:8350;urn:miriam:hgnc:4766;urn:miriam:refseq:NM_003529;urn:miriam:hgnc.symbol:H3C1;urn:miriam:ensembl:ENSG00000275714;urn:miriam:hgnc.symbol:H3C1;urn:miriam:ensembl:ENSG00000278637;urn:miriam:hgnc:4781;urn:miriam:ncbigene:8359;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:refseq:NM_003538;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:hgnc:4794;urn:miriam:ncbigene:8370;urn:miriam:ensembl:ENSG00000270882;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:refseq:NM_003548;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:hgnc.symbol:H4C14;urn:miriam:interpro:IPR002119;urn:miriam:hgnc.symbol:H3C15;urn:miriam:hgnc.symbol:H3C15;urn:miriam:ncbigene:333932;urn:miriam:ncbigene:126961;urn:miriam:hgnc:20505;urn:miriam:uniprot:Q71DI3;urn:miriam:uniprot:Q71DI3;urn:miriam:ensembl:ENSG00000203852;urn:miriam:refseq:NM_001005464;urn:miriam:hgnc:4760;urn:miriam:hgnc.symbol:H2BC21;urn:miriam:hgnc.symbol:H2BC21;urn:miriam:ensembl:ENSG00000184678;urn:miriam:refseq:NM_003528;urn:miriam:uniprot:Q16778;urn:miriam:uniprot:Q16778;urn:miriam:ncbigene:8349;urn:miriam:ncbigene:8349;urn:miriam:hgnc:4793;urn:miriam:ncbigene:8294;urn:miriam:ensembl:ENSG00000276180;urn:miriam:hgnc.symbol:H4C9;urn:miriam:refseq:NM_003495;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:ensembl:ENSG00000197837;urn:miriam:hgnc:20510;urn:miriam:refseq:NM_175054;urn:miriam:hgnc.symbol:H4-16;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:ncbigene:121504"
      hgnc "HGNC_SYMBOL:H3C1;HGNC_SYMBOL:H4C1;HGNC_SYMBOL:H4C14;HGNC_SYMBOL:H3C15;HGNC_SYMBOL:H2BC21;HGNC_SYMBOL:H4C9;HGNC_SYMBOL:H4-16"
      map_id "M17_3"
      name "histone"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa2"
      uniprot "UNIPROT:P68431;UNIPROT:P62805;UNIPROT:Q71DI3;UNIPROT:Q16778"
    ]
    graphics [
      x 817.9673850663708
      y 1671.4667846338425
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:refseq:NM_001113182;urn:miriam:hgnc:1103;urn:miriam:uniprot:P25440;urn:miriam:uniprot:P25440;urn:miriam:ncbigene:6046;urn:miriam:ncbigene:6046;urn:miriam:ensembl:ENSG00000204256;urn:miriam:hgnc.symbol:BRD2;urn:miriam:hgnc.symbol:BRD2"
      hgnc "HGNC_SYMBOL:BRD2"
      map_id "M17_48"
      name "BRD2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa21"
      uniprot "UNIPROT:P25440"
    ]
    graphics [
      x 878.8536650413897
      y 745.4426193277158
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc:4760;urn:miriam:hgnc.symbol:H2BC21;urn:miriam:hgnc.symbol:H2BC21;urn:miriam:ensembl:ENSG00000184678;urn:miriam:refseq:NM_003528;urn:miriam:uniprot:Q16778;urn:miriam:uniprot:Q16778;urn:miriam:ncbigene:8349;urn:miriam:ncbigene:8349;urn:miriam:ncbigene:8350;urn:miriam:uniprot:P68431;urn:miriam:uniprot:P68431;urn:miriam:ncbigene:8350;urn:miriam:hgnc:4766;urn:miriam:refseq:NM_003529;urn:miriam:hgnc.symbol:H3C1;urn:miriam:ensembl:ENSG00000275714;urn:miriam:hgnc.symbol:H3C1;urn:miriam:hgnc:4793;urn:miriam:ncbigene:8294;urn:miriam:ensembl:ENSG00000276180;urn:miriam:hgnc.symbol:H4C9;urn:miriam:refseq:NM_003495;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:ensembl:ENSG00000278637;urn:miriam:hgnc:4781;urn:miriam:ncbigene:8359;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:refseq:NM_003538;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:ensembl:ENSG00000197837;urn:miriam:hgnc:20510;urn:miriam:refseq:NM_175054;urn:miriam:hgnc.symbol:H4-16;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:ncbigene:121504;urn:miriam:hgnc.symbol:H3C15;urn:miriam:hgnc.symbol:H3C15;urn:miriam:ncbigene:333932;urn:miriam:ncbigene:126961;urn:miriam:hgnc:20505;urn:miriam:uniprot:Q71DI3;urn:miriam:uniprot:Q71DI3;urn:miriam:ensembl:ENSG00000203852;urn:miriam:refseq:NM_001005464;urn:miriam:hgnc:4794;urn:miriam:ncbigene:8370;urn:miriam:ensembl:ENSG00000270882;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:refseq:NM_003548;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:hgnc.symbol:H4C14;urn:miriam:interpro:IPR002119"
      hgnc "HGNC_SYMBOL:H2BC21;HGNC_SYMBOL:H3C1;HGNC_SYMBOL:H4C9;HGNC_SYMBOL:H4C1;HGNC_SYMBOL:H4-16;HGNC_SYMBOL:H3C15;HGNC_SYMBOL:H4C14"
      map_id "M17_1"
      name "histone"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa1"
      uniprot "UNIPROT:Q16778;UNIPROT:P68431;UNIPROT:P62805;UNIPROT:Q71DI3"
    ]
    graphics [
      x 1478.8536650413898
      y 642.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "PUBMED:18406326"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_24"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re30"
      uniprot "NA"
    ]
    graphics [
      x 1051.9082624265661
      y 210.30069258591152
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_73"
      name "Chromatin_space_organization"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa78"
      uniprot "NA"
    ]
    graphics [
      x 998.5783791234117
      y 403.1259168585731
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_33"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re6"
      uniprot "NA"
    ]
    graphics [
      x 1762.4878382418135
      y 946.8134512202714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re7"
      uniprot "NA"
    ]
    graphics [
      x 938.8536650413897
      y 884.1812572720856
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000112592;urn:miriam:ncbigene:6908;urn:miriam:ncbigene:6908;urn:miriam:uniprot:P20226;urn:miriam:uniprot:P20226;urn:miriam:hgnc:11588;urn:miriam:hgnc.symbol:TBP;urn:miriam:hgnc.symbol:TBP;urn:miriam:refseq:NM_003194"
      hgnc "HGNC_SYMBOL:TBP"
      map_id "M17_44"
      name "TBP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa18"
      uniprot "UNIPROT:P20226"
    ]
    graphics [
      x 1377.7231255590953
      y 342.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000112592;urn:miriam:ncbigene:6908;urn:miriam:ncbigene:6908;urn:miriam:uniprot:P20226;urn:miriam:uniprot:P20226;urn:miriam:hgnc:11588;urn:miriam:hgnc.symbol:TBP;urn:miriam:hgnc.symbol:TBP;urn:miriam:refseq:NM_003194"
      hgnc "HGNC_SYMBOL:TBP"
      map_id "M17_43"
      name "TBP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa17"
      uniprot "UNIPROT:P20226"
    ]
    graphics [
      x 908.8536650413897
      y 743.5627285309354
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:refseq:NM_001113182;urn:miriam:hgnc:1103;urn:miriam:uniprot:P25440;urn:miriam:uniprot:P25440;urn:miriam:ncbigene:6046;urn:miriam:ncbigene:6046;urn:miriam:ensembl:ENSG00000204256;urn:miriam:hgnc.symbol:BRD2;urn:miriam:hgnc.symbol:BRD2"
      hgnc "HGNC_SYMBOL:BRD2"
      map_id "M17_47"
      name "BRD2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa20"
      uniprot "UNIPROT:P25440"
    ]
    graphics [
      x 1841.4760702265748
      y 1494.7285609291819
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ncbiprotein:BCD58755;urn:miriam:uniprot:E"
      hgnc "NA"
      map_id "M17_45"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa19"
      uniprot "UNIPROT:E"
    ]
    graphics [
      x 1843.6413058760115
      y 466.8134512202714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:pubchem.compound:46907787"
      hgnc "NA"
      map_id "M17_74"
      name "JQ_minus_1"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa79"
      uniprot "NA"
    ]
    graphics [
      x 1748.8536650413898
      y 826.8134512202714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_32"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re5"
      uniprot "NA"
    ]
    graphics [
      x 1331.4760702265748
      y 1092.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc:4794;urn:miriam:ncbigene:8370;urn:miriam:ensembl:ENSG00000270882;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:refseq:NM_003548;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:hgnc.symbol:H4C14"
      hgnc "HGNC_SYMBOL:H4C1;HGNC_SYMBOL:H4C14"
      map_id "M17_76"
      name "H4C14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa9"
      uniprot "UNIPROT:P62805"
    ]
    graphics [
      x 731.4760702265747
      y 1170.3776647691739
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000197837;urn:miriam:hgnc:20510;urn:miriam:refseq:NM_175054;urn:miriam:hgnc.symbol:H4-16;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504;urn:miriam:ncbigene:121504"
      hgnc "HGNC_SYMBOL:H4-16;HGNC_SYMBOL:H4C1"
      map_id "M17_75"
      name "H4_minus_16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa8"
      uniprot "UNIPROT:P62805"
    ]
    graphics [
      x 1058.8536650413898
      y 726.3826300445392
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000278637;urn:miriam:hgnc:4781;urn:miriam:ncbigene:8359;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:refseq:NM_003538;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504"
      hgnc "HGNC_SYMBOL:H4C1"
      map_id "M17_46"
      name "H4C1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2"
      uniprot "UNIPROT:P62805"
    ]
    graphics [
      x 1061.4760702265748
      y 1189.5029478087847
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:H3C15;urn:miriam:hgnc.symbol:H3C15;urn:miriam:ncbigene:333932;urn:miriam:ncbigene:126961;urn:miriam:hgnc:20505;urn:miriam:uniprot:Q71DI3;urn:miriam:uniprot:Q71DI3;urn:miriam:ensembl:ENSG00000203852;urn:miriam:refseq:NM_001005464"
      hgnc "HGNC_SYMBOL:H3C15"
      map_id "M17_56"
      name "H3C15"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa3"
      uniprot "UNIPROT:Q71DI3"
    ]
    graphics [
      x 1031.4760702265748
      y 1275.8442223297948
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ncbigene:8350;urn:miriam:uniprot:P68431;urn:miriam:uniprot:P68431;urn:miriam:ncbigene:8350;urn:miriam:hgnc:4766;urn:miriam:refseq:NM_003529;urn:miriam:hgnc.symbol:H3C1;urn:miriam:ensembl:ENSG00000275714;urn:miriam:hgnc.symbol:H3C1"
      hgnc "HGNC_SYMBOL:H3C1"
      map_id "M17_35"
      name "H3C1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1"
      uniprot "UNIPROT:P68431"
    ]
    graphics [
      x 911.4760702265747
      y 1368.436020814792
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc:4793;urn:miriam:ncbigene:8294;urn:miriam:ensembl:ENSG00000276180;urn:miriam:hgnc.symbol:H4C9;urn:miriam:refseq:NM_003495;urn:miriam:uniprot:P62805;urn:miriam:uniprot:P62805;urn:miriam:hgnc.symbol:H4C1;urn:miriam:ncbigene:121504"
      hgnc "HGNC_SYMBOL:H4C9;HGNC_SYMBOL:H4C1"
      map_id "M17_63"
      name "H4C9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa5"
      uniprot "UNIPROT:P62805"
    ]
    graphics [
      x 1403.9358708651494
      y 732.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc:4760;urn:miriam:hgnc.symbol:H2BC21;urn:miriam:hgnc.symbol:H2BC21;urn:miriam:ensembl:ENSG00000184678;urn:miriam:refseq:NM_003528;urn:miriam:uniprot:Q16778;urn:miriam:uniprot:Q16778;urn:miriam:ncbigene:8349;urn:miriam:ncbigene:8349"
      hgnc "HGNC_SYMBOL:H2BC21"
      map_id "M17_69"
      name "H2BC21"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa7"
      uniprot "UNIPROT:Q16778"
    ]
    graphics [
      x 1991.4760702265748
      y 1247.606843746084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:interpro:IPR002119"
      hgnc "NA"
      map_id "M17_64"
      name "H2A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa6"
      uniprot "NA"
    ]
    graphics [
      x 2048.85366504139
      y 790.1506240017071
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:CCNT1;urn:miriam:hgnc.symbol:CCNT1;urn:miriam:refseq:NM_001240;urn:miriam:ensembl:ENSG00000129315;urn:miriam:hgnc:1599;urn:miriam:ncbigene:904;urn:miriam:ncbigene:904;urn:miriam:uniprot:O60563;urn:miriam:uniprot:O60563"
      hgnc "HGNC_SYMBOL:CCNT1"
      map_id "M17_37"
      name "CCNT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa11"
      uniprot "UNIPROT:O60563"
    ]
    graphics [
      x 637.9673850663706
      y 1662.1662202339423
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:CCNT1;urn:miriam:hgnc.symbol:CCNT1;urn:miriam:refseq:NM_001240;urn:miriam:ensembl:ENSG00000129315;urn:miriam:hgnc:1599;urn:miriam:ncbigene:904;urn:miriam:ncbigene:904;urn:miriam:uniprot:O60563;urn:miriam:uniprot:O60563"
      hgnc "HGNC_SYMBOL:CCNT1"
      map_id "M17_40"
      name "CCNT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa14"
      uniprot "UNIPROT:O60563"
    ]
    graphics [
      x 517.9673850663706
      y 1701.7903918322108
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_21"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re28"
      uniprot "NA"
    ]
    graphics [
      x 998.8536650413897
      y 1016.2719416604734
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ec-code:2.7.11.23;urn:miriam:hgnc:1780;urn:miriam:ensembl:ENSG00000136807;urn:miriam:ec-code:2.7.11.22;urn:miriam:refseq:NM_001261;urn:miriam:uniprot:P50750;urn:miriam:uniprot:P50750;urn:miriam:hgnc.symbol:CDK9;urn:miriam:hgnc.symbol:CDK9;urn:miriam:ncbigene:1025;urn:miriam:ncbigene:1025"
      hgnc "HGNC_SYMBOL:CDK9"
      map_id "M17_41"
      name "CDK9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa15"
      uniprot "UNIPROT:P50750"
    ]
    graphics [
      x 1746.9940839999704
      y 362.27844572114736
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ec-code:2.7.11.23;urn:miriam:hgnc:1780;urn:miriam:ensembl:ENSG00000136807;urn:miriam:ec-code:2.7.11.22;urn:miriam:refseq:NM_001261;urn:miriam:uniprot:P50750;urn:miriam:uniprot:P50750;urn:miriam:hgnc.symbol:CDK9;urn:miriam:hgnc.symbol:CDK9;urn:miriam:ncbigene:1025;urn:miriam:ncbigene:1025;urn:miriam:hgnc.symbol:CCNT1;urn:miriam:hgnc.symbol:CCNT1;urn:miriam:refseq:NM_001240;urn:miriam:ensembl:ENSG00000129315;urn:miriam:hgnc:1599;urn:miriam:ncbigene:904;urn:miriam:ncbigene:904;urn:miriam:uniprot:O60563;urn:miriam:uniprot:O60563"
      hgnc "HGNC_SYMBOL:CDK9;HGNC_SYMBOL:CCNT1"
      map_id "M17_8"
      name "P_minus_TEFb"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa8"
      uniprot "UNIPROT:P50750;UNIPROT:O60563"
    ]
    graphics [
      x 1126.8325726048322
      y 400.432509533711
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_22"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re29"
      uniprot "NA"
    ]
    graphics [
      x 859.716283832669
      y 455.77029668376804
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M17_72"
      name "RNA_space_Polymerase_space_II_minus_dependent_space_Transcription_space_"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa77"
      uniprot "NA"
    ]
    graphics [
      x 1331.4760702265748
      y 1332.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:E protein interactions"
      full_annotation "urn:miriam:ec-code:2.7.11.23;urn:miriam:hgnc:1780;urn:miriam:ensembl:ENSG00000136807;urn:miriam:ec-code:2.7.11.22;urn:miriam:refseq:NM_001261;urn:miriam:uniprot:P50750;urn:miriam:uniprot:P50750;urn:miriam:hgnc.symbol:CDK9;urn:miriam:hgnc.symbol:CDK9;urn:miriam:ncbigene:1025;urn:miriam:ncbigene:1025"
      hgnc "HGNC_SYMBOL:CDK9"
      map_id "M17_36"
      name "CDK9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa10"
      uniprot "UNIPROT:P50750"
    ]
    graphics [
      x 1211.4760702265748
      y 1239.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M17_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 37
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_38"
      target_id "M17_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 38
    source 3
    target 2
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "INHIBITION"
      source_id "M17_39"
      target_id "M17_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 39
    source 2
    target 4
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_31"
      target_id "M17_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 40
    source 4
    target 5
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CATALYSIS"
      source_id "M17_42"
      target_id "M17_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 41
    source 4
    target 6
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CATALYSIS"
      source_id "M17_42"
      target_id "M17_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 42
    source 4
    target 7
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CATALYSIS"
      source_id "M17_42"
      target_id "M17_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 43
    source 36
    target 5
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_36"
      target_id "M17_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 44
    source 5
    target 32
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_9"
      target_id "M17_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 45
    source 29
    target 6
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_37"
      target_id "M17_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 46
    source 6
    target 30
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_19"
      target_id "M17_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 47
    source 8
    target 7
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_3"
      target_id "M17_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 48
    source 9
    target 7
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CATALYSIS"
      source_id "M17_48"
      target_id "M17_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 49
    source 7
    target 10
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_23"
      target_id "M17_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 50
    source 20
    target 8
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_32"
      target_id "M17_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 51
    source 13
    target 9
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_33"
      target_id "M17_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 52
    source 9
    target 14
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CATALYSIS"
      source_id "M17_48"
      target_id "M17_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 53
    source 10
    target 11
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_1"
      target_id "M17_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 54
    source 11
    target 12
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_24"
      target_id "M17_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 55
    source 17
    target 13
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_47"
      target_id "M17_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 56
    source 18
    target 13
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "INHIBITION"
      source_id "M17_45"
      target_id "M17_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 57
    source 19
    target 13
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "INHIBITION"
      source_id "M17_74"
      target_id "M17_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 58
    source 15
    target 14
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_44"
      target_id "M17_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 59
    source 14
    target 16
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_34"
      target_id "M17_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 60
    source 21
    target 20
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_76"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 61
    source 22
    target 20
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_75"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 62
    source 23
    target 20
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_46"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 63
    source 24
    target 20
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_56"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 64
    source 25
    target 20
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_35"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 26
    target 20
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_63"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 27
    target 20
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_69"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 28
    target 20
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_64"
      target_id "M17_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 30
    target 31
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_40"
      target_id "M17_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 69
    source 32
    target 31
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_41"
      target_id "M17_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 70
    source 31
    target 33
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_21"
      target_id "M17_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 71
    source 33
    target 34
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M17_8"
      target_id "M17_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 72
    source 34
    target 35
    cd19dm [
      diagram "C19DMap:E protein interactions"
      edge_type "PRODUCTION"
      source_id "M17_22"
      target_id "M17_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
