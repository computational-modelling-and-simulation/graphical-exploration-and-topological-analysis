# generated with VANTED V2.8.2 at Fri Mar 04 10:04:37 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:interpro:IPR002552"
      hgnc "NA"
      map_id "M18_188"
      name "S2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2188"
      uniprot "NA"
    ]
    graphics [
      x 1267.9673850663708
      y 1602.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_188"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:32142651"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_107"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re843"
      uniprot "NA"
    ]
    graphics [
      x 1451.4760702265748
      y 1182.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "PUBMED:32047258"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_121"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re981"
      uniprot "NA"
    ]
    graphics [
      x 1251.6104592494069
      y 1722.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:interpro:IPR002552"
      hgnc "NA"
      map_id "M18_164"
      name "S2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2052"
      uniprot "NA"
    ]
    graphics [
      x 1961.4760702265748
      y 1173.720481646816
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_164"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "PUBMED:32047258;PUBMED:32142651"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_120"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re979"
      uniprot "NA"
    ]
    graphics [
      x 2049.3019144296195
      y 376.2454985512343
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_3"
      name "Nucleocapsid"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa365"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 2018.8536650413898
      y 668.0367077674176
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570;urn:miriam:hgnc.symbol:E;urn:miriam:uniprot:P0DTC4"
      hgnc "HGNC_SYMBOL:E"
      map_id "M18_167"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2062"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 2348.85366504139
      y 644.1213577093912
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_167"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740573;urn:miriam:uniprot:P0DTC7"
      hgnc "NA"
      map_id "M18_165"
      name "Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2060"
      uniprot "UNIPROT:P0DTC7"
    ]
    graphics [
      x 1382.112491134918
      y 792.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_165"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "M18_166"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2061"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 1466.090149807047
      y 462.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_166"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_5"
      name "Nucleocapsid"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa369"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 758.8536650413897
      y 698.15125963201
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740573;urn:miriam:uniprot:P0DTC7"
      hgnc "NA"
      map_id "M18_182"
      name "Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2114"
      uniprot "UNIPROT:P0DTC7"
    ]
    graphics [
      x 2081.548300900389
      y 534.8606125107739
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_182"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570;urn:miriam:hgnc.symbol:E;urn:miriam:uniprot:P0DTC4"
      hgnc "HGNC_SYMBOL:E"
      map_id "M18_183"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2115"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 2535.048870682949
      y 814.7288715512948
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_183"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "M18_184"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2116"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 2138.85366504139
      y 531.0197557525325
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_184"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "PUBMED:32047258;PUBMED:32142651;PUBMED:32944968;PUBMED:32094589"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_109"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re855"
      uniprot "NA"
    ]
    graphics [
      x 398.85366504138983
      y 722.5572932182373
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_110"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re859"
      uniprot "NA"
    ]
    graphics [
      x 1811.4760702265748
      y 1388.1793367792513
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:refseq:NC_045512"
      hgnc "NA"
      map_id "M18_134"
      name "(_plus_)ss_space_gRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa1675"
      uniprot "NA"
    ]
    graphics [
      x 2291.476070226575
      y 1234.1444038133232
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_133"
      name "N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1667"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 1373.9358708651494
      y 642.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_133"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_113"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re908"
      uniprot "NA"
    ]
    graphics [
      x 1748.8536650413898
      y 616.8134512202714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_155"
      name "s2919"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa1920"
      uniprot "NA"
    ]
    graphics [
      x 1886.1117585348948
      y 213.35548031794383
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_155"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_99"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1115"
      uniprot "NA"
    ]
    graphics [
      x 2588.85366504139
      y 923.5430832004365
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_98"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1113"
      uniprot "NA"
    ]
    graphics [
      x 2801.476070226575
      y 1478.9962792733013
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_53"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1063"
      uniprot "NA"
    ]
    graphics [
      x 1988.8536650413898
      y 690.4795298929273
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_12"
      name "Replication_space_transcription_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa410"
      uniprot "NA"
    ]
    graphics [
      x 1698.531479206491
      y 368.04494384980626
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:refseq:NC_045512"
      hgnc "NA"
      map_id "M18_245"
      name "(_minus_)ss_space_gRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2328"
      uniprot "NA"
    ]
    graphics [
      x 2708.85366504139
      y 960.6319905844202
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_245"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_64"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1076"
      uniprot "NA"
    ]
    graphics [
      x 2351.476070226575
      y 1286.883873182828
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "PUBMED:8830530"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_103"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1123"
      uniprot "NA"
    ]
    graphics [
      x 2201.476070226575
      y 1200.5867359968995
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_235"
      name "Orf7b_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2318"
      uniprot "NA"
    ]
    graphics [
      x 1736.090149807047
      y 492.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_235"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740575"
      hgnc "NA"
      map_id "M18_267"
      name "N_space__space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2353"
      uniprot "NA"
    ]
    graphics [
      x 2446.7296668460585
      y 648.2564973503984
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_267"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571"
      hgnc "NA"
      map_id "M18_244"
      name "M_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2327"
      uniprot "NA"
    ]
    graphics [
      x 2201.476070226575
      y 1080.5867359968995
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_244"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570"
      hgnc "NA"
      map_id "M18_243"
      name "E_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2326"
      uniprot "NA"
    ]
    graphics [
      x 1288.965509607095
      y 552.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_243"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_242"
      name "S_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2325"
      uniprot "NA"
    ]
    graphics [
      x 2467.9673850663708
      y 1630.2835095222708
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_242"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_241"
      name "Orf7a_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2324"
      uniprot "NA"
    ]
    graphics [
      x 2288.85366504139
      y 645.6396371924621
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_241"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_240"
      name "Orf6_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2323"
      uniprot "NA"
    ]
    graphics [
      x 967.9673850663706
      y 1531.1222129336036
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_240"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_239"
      name "Orf3a_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2322"
      uniprot "NA"
    ]
    graphics [
      x 608.8536650413897
      y 999.8351640363044
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_239"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_238"
      name "Orf8_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2321"
      uniprot "NA"
    ]
    graphics [
      x 2561.476070226575
      y 1226.2296608124486
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_238"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_237"
      name "Orf9b_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2320"
      uniprot "NA"
    ]
    graphics [
      x 2678.85366504139
      y 1033.1132360013019
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_237"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_236"
      name "Orf14_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2319"
      uniprot "NA"
    ]
    graphics [
      x 1988.8536650413898
      y 818.5267273646541
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_236"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_73"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1085"
      uniprot "NA"
    ]
    graphics [
      x 1028.8536650413898
      y 951.5374917954795
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_265"
      name "Orf14_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2351"
      uniprot "NA"
    ]
    graphics [
      x 1148.8536650413898
      y 1003.12752813305
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_265"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_84"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1097"
      uniprot "NA"
    ]
    graphics [
      x 1270.9744346231273
      y 432.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_231"
      name "Orf14_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2301"
      uniprot "NA"
    ]
    graphics [
      x 1478.8536650413898
      y 882.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_231"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_178"
      name "Orf14_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2110"
      uniprot "NA"
    ]
    graphics [
      x 991.6988802963682
      y 493.1259168585731
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_178"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_49"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1030"
      uniprot "NA"
    ]
    graphics [
      x 1889.1092687353323
      y 393.2143435620635
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0070992"
      hgnc "NA"
      map_id "M18_14"
      name "Host_space_translation_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa428"
      uniprot "NA"
    ]
    graphics [
      x 1619.637228383377
      y 252.06502830998443
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTD3"
      hgnc "NA"
      map_id "M18_148"
      name "Orf14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1877"
      uniprot "UNIPROT:P0DTD3"
    ]
    graphics [
      x 1901.4760702265748
      y 1022.6254878021133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_148"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_38"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1019"
      uniprot "NA"
    ]
    graphics [
      x 1928.8536650413898
      y 902.6254878021133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTD3"
      hgnc "NA"
      map_id "M18_220"
      name "Orf14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2250"
      uniprot "UNIPROT:P0DTD3"
    ]
    graphics [
      x 1466.090149807047
      y 492.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_220"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_44"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1025"
      uniprot "NA"
    ]
    graphics [
      x 1087.9673850663708
      y 1556.6837483244312
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_50"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1031"
      uniprot "NA"
    ]
    graphics [
      x 1847.2023363703734
      y 975.0807242957932
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_41"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1022"
      uniprot "NA"
    ]
    graphics [
      x 1361.4760702265748
      y 1145.4561336291283
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_42"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1023"
      uniprot "NA"
    ]
    graphics [
      x 1476.8814471766686
      y 312.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_48"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1029"
      uniprot "NA"
    ]
    graphics [
      x 1297.2835609334998
      y 162.06502830998443
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1026"
      uniprot "NA"
    ]
    graphics [
      x 2438.85366504139
      y 926.4498083987946
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_46"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1027"
      uniprot "NA"
    ]
    graphics [
      x 2471.476070226575
      y 1042.0912766124413
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1028"
      uniprot "NA"
    ]
    graphics [
      x 1666.8405824608653
      y 192.06502830998443
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_43"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1024"
      uniprot "NA"
    ]
    graphics [
      x 578.8536650413897
      y 746.5391582985717
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_180"
      name "S_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2112"
      uniprot "NA"
    ]
    graphics [
      x 758.8536650413897
      y 582.8170653407797
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_180"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "M18_138"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1688"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 671.4760702265747
      y 1424.2788863566473
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_138"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_32"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1013"
      uniprot "NA"
    ]
    graphics [
      x 1751.4760702265748
      y 1435.288563922968
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "M18_154"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1893"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 2497.9673850663708
      y 1686.7676583327213
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_154"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_29"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1010"
      uniprot "NA"
    ]
    graphics [
      x 2332.5144998161113
      y 1963.1312787731117
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "M18_143"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1859"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 2452.5144998161113
      y 1813.8214117411178
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_143"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_78"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1091"
      uniprot "NA"
    ]
    graphics [
      x 1526.1908805408093
      y 106.50027264388359
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_259"
      name "S_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2345"
      uniprot "NA"
    ]
    graphics [
      x 2398.459295281623
      y 771.5175868173752
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_259"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_225"
      name "S_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2295"
      uniprot "NA"
    ]
    graphics [
      x 1377.3206553350792
      y 222.06502830998443
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_225"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_56"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1067"
      uniprot "NA"
    ]
    graphics [
      x 518.8536650413897
      y 610.7981541171979
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_248"
      name "sa2295_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2332"
      uniprot "NA"
    ]
    graphics [
      x 461.47607022657473
      y 1134.824039608759
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_248"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      annotation "PUBMED:11142;PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_67"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1079"
      uniprot "NA"
    ]
    graphics [
      x 2471.476070226575
      y 1386.8237772012344
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_175"
      name "Orf8_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2107"
      uniprot "NA"
    ]
    graphics [
      x 1944.9791263462503
      y 302.7686655077657
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_175"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC8;urn:miriam:ncbigene:43740577"
      hgnc "NA"
      map_id "M18_150"
      name "Orf8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1879"
      uniprot "UNIPROT:P0DTC8"
    ]
    graphics [
      x 1857.1796026810903
      y 436.8134512202714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_150"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1017"
      uniprot "NA"
    ]
    graphics [
      x 908.8536650413897
      y 562.7388267282037
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC8;urn:miriam:ncbigene:43740577"
      hgnc "NA"
      map_id "M18_218"
      name "Orf8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2248"
      uniprot "UNIPROT:P0DTC8"
    ]
    graphics [
      x 968.8536650413897
      y 1067.3372940987897
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_218"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_82"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1095"
      uniprot "NA"
    ]
    graphics [
      x 1757.2023363703734
      y 1006.8134512202714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_263"
      name "Orf8_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2349"
      uniprot "NA"
    ]
    graphics [
      x 1451.4760702265748
      y 1272.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_263"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_229"
      name "Orf8_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2299"
      uniprot "NA"
    ]
    graphics [
      x 2287.9673850663708
      y 1735.676905472525
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_229"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_60"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1072"
      uniprot "NA"
    ]
    graphics [
      x 2527.9673850663708
      y 1713.6392519372603
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_252"
      name "sa2299_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2337"
      uniprot "NA"
    ]
    graphics [
      x 2362.5144998161113
      y 2076.8557137111584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_252"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_71"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1083"
      uniprot "NA"
    ]
    graphics [
      x 2471.476070226575
      y 1283.4033913564936
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_172"
      name "Orf3a_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2104"
      uniprot "NA"
    ]
    graphics [
      x 2411.476070226575
      y 1196.1044994176964
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_172"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC3;urn:miriam:ncbigene:43740569"
      hgnc "NA"
      map_id "M18_144"
      name "Orf3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1873"
      uniprot "UNIPROT:P0DTC3"
    ]
    graphics [
      x 1976.090149807047
      y 520.3261619378375
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_144"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_35"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1016"
      uniprot "NA"
    ]
    graphics [
      x 1162.1276023508237
      y 418.3062325880667
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC3;urn:miriam:ncbigene:43740569"
      hgnc "NA"
      map_id "M18_217"
      name "Orf3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2247"
      uniprot "UNIPROT:P0DTC3"
    ]
    graphics [
      x 1298.8536650413898
      y 912.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_217"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_81"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1094"
      uniprot "NA"
    ]
    graphics [
      x 1598.8536650413898
      y 702.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_262"
      name "Orf3a_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2348"
      uniprot "NA"
    ]
    graphics [
      x 608.8536650413897
      y 630.4427130880699
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_262"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_228"
      name "Orf3a_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2298"
      uniprot "NA"
    ]
    graphics [
      x 818.8536650413897
      y 1059.5752808432774
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_228"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_59"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1070"
      uniprot "NA"
    ]
    graphics [
      x 938.8536650413897
      y 698.4049804982296
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_251"
      name "sa2298_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2335"
      uniprot "NA"
    ]
    graphics [
      x 1838.8536650413898
      y 712.6230783403096
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_251"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_70"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1082"
      uniprot "NA"
    ]
    graphics [
      x 548.8536650413897
      y 674.683668528955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_176"
      name "Orf6_space__space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2108"
      uniprot "NA"
    ]
    graphics [
      x 2561.476070226575
      y 1116.9729351159822
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_176"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740572;urn:miriam:uniprot:P0DTC6"
      hgnc "NA"
      map_id "M18_145"
      name "Orf6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1874"
      uniprot "UNIPROT:P0DTC6"
    ]
    graphics [
      x 2291.476070226575
      y 1534.1444038133232
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_145"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1015"
      uniprot "NA"
    ]
    graphics [
      x 2152.5144998161113
      y 2012.6744075548015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740572;urn:miriam:uniprot:P0DTC6"
      hgnc "NA"
      map_id "M18_216"
      name "Orf6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2246"
      uniprot "UNIPROT:P0DTC6"
    ]
    graphics [
      x 2512.5144998161113
      y 1917.0941111195048
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_216"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_80"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1093"
      uniprot "NA"
    ]
    graphics [
      x 1607.2023363703734
      y 942.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_261"
      name "Orf6_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2347"
      uniprot "NA"
    ]
    graphics [
      x 1088.8536650413898
      y 866.5781614917847
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_261"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_227"
      name "Orf6_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2297"
      uniprot "NA"
    ]
    graphics [
      x 518.8536650413897
      y 841.7606652634609
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_227"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_58"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1069"
      uniprot "NA"
    ]
    graphics [
      x 758.8536650413897
      y 728.15125963201
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_250"
      name "sa2297_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2334"
      uniprot "NA"
    ]
    graphics [
      x 1088.8536650413898
      y 693.3122672958201
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_250"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_69"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1081"
      uniprot "NA"
    ]
    graphics [
      x 1346.090149807047
      y 372.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_177"
      name "Orf9b_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2109"
      uniprot "NA"
    ]
    graphics [
      x 1466.090149807047
      y 402.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_177"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTD2"
      hgnc "NA"
      map_id "M18_149"
      name "Orf9b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1878"
      uniprot "UNIPROT:P0DTD2"
    ]
    graphics [
      x 1331.4760702265748
      y 1122.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_149"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_37"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1018"
      uniprot "NA"
    ]
    graphics [
      x 1881.6104592494069
      y 1761.145821463369
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTD2"
      hgnc "NA"
      map_id "M18_219"
      name "Orf9b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2249"
      uniprot "UNIPROT:P0DTD2"
    ]
    graphics [
      x 1372.5144998161113
      y 2142.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_219"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_83"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1096"
      uniprot "NA"
    ]
    graphics [
      x 1516.6057727547209
      y 972.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_264"
      name "Orf9b_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2350"
      uniprot "NA"
    ]
    graphics [
      x 1991.4760702265748
      y 1114.8841075166547
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_264"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_230"
      name "Orf9b_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2300"
      uniprot "NA"
    ]
    graphics [
      x 968.8536650413897
      y 1037.3372940987897
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_230"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_61"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1073"
      uniprot "NA"
    ]
    graphics [
      x 1238.8536650413898
      y 939.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_253"
      name "sa2300_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2338"
      uniprot "NA"
    ]
    graphics [
      x 1376.090149807047
      y 372.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_253"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_72"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1084"
      uniprot "NA"
    ]
    graphics [
      x 2561.476070226575
      y 1179.5226978125067
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_179"
      name "E_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2111"
      uniprot "NA"
    ]
    graphics [
      x 1338.5732784769903
      y 102.06502830998443
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_179"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570;urn:miriam:hgnc.symbol:E;urn:miriam:uniprot:P0DTC4"
      hgnc "HGNC_SYMBOL:E"
      map_id "M18_137"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1687"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 2318.85366504139
      y 783.9475755283014
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_137"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1012"
      uniprot "NA"
    ]
    graphics [
      x 1868.8536650413898
      y 894.1227669961524
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570;urn:miriam:hgnc.symbol:E;urn:miriam:uniprot:P0DTC4"
      hgnc "HGNC_SYMBOL:E"
      map_id "M18_153"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1892"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 1706.2295630253866
      y 147.5869409604959
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_153"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_28"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1009"
      uniprot "NA"
    ]
    graphics [
      x 1771.3750514758176
      y 192.96143556291986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570;urn:miriam:hgnc.symbol:E;urn:miriam:uniprot:P0DTC4"
      hgnc "HGNC_SYMBOL:E"
      map_id "M18_142"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1858"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 1268.8536650413898
      y 522.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_142"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_77"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1090"
      uniprot "NA"
    ]
    graphics [
      x 1466.090149807047
      y 432.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_258"
      name "E_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2344"
      uniprot "NA"
    ]
    graphics [
      x 1001.4760702265747
      y 1468.6039843924614
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_258"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570"
      hgnc "NA"
      map_id "M18_224"
      name "E_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2294"
      uniprot "NA"
    ]
    graphics [
      x 1928.8536650413898
      y 645.1765135034507
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_224"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_55"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1066"
      uniprot "NA"
    ]
    graphics [
      x 2351.476070226575
      y 1051.3530460448192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_247"
      name "sa2294_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2331"
      uniprot "NA"
    ]
    graphics [
      x 1901.4760702265748
      y 1118.6835654610654
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_247"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      annotation "PUBMED:11142;PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_66"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1078"
      uniprot "NA"
    ]
    graphics [
      x 578.8536650413897
      y 846.68758464586
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_181"
      name "M_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2113"
      uniprot "NA"
    ]
    graphics [
      x 767.4689502103041
      y 539.2476508012253
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_181"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "M18_136"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1686"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 1541.4760702265748
      y 1392.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_30"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1011"
      uniprot "NA"
    ]
    graphics [
      x 1901.4760702265748
      y 1321.4707703304423
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "M18_152"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1891"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 1586.090149807047
      y 372.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_152"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_27"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1008"
      uniprot "NA"
    ]
    graphics [
      x 1069.4742383916903
      y 302.30684225815935
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "M18_141"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1857"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 758.8536650413897
      y 758.15125963201
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_141"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_76"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1089"
      uniprot "NA"
    ]
    graphics [
      x 1037.7823754701792
      y 261.2828084579903
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_257"
      name "M_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2343"
      uniprot "NA"
    ]
    graphics [
      x 698.8536650413897
      y 565.3958501019754
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_257"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571"
      hgnc "NA"
      map_id "M18_223"
      name "M_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2292"
      uniprot "NA"
    ]
    graphics [
      x 2018.8536650413898
      y 627.9205205109306
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_223"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1064"
      uniprot "NA"
    ]
    graphics [
      x 2467.9673850663708
      y 1569.7415077782575
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_246"
      name "sa2292_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2329"
      uniprot "NA"
    ]
    graphics [
      x 2501.476070226575
      y 1200.2360156051782
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_246"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_65"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1077"
      uniprot "NA"
    ]
    graphics [
      x 968.5783791234117
      y 403.3267239738991
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_174"
      name "Orf7b_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2106"
      uniprot "NA"
    ]
    graphics [
      x 1911.6104592494069
      y 1690.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_174"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740574;urn:miriam:uniprot:P0DTD8"
      hgnc "NA"
      map_id "M18_147"
      name "Orf7b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1876"
      uniprot "UNIPROT:P0DTD8"
    ]
    graphics [
      x 1859.2104285107553
      y 505.2505497326599
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_147"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_39"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1020"
      uniprot "NA"
    ]
    graphics [
      x 2678.85366504139
      y 990.5791681742426
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740574;urn:miriam:uniprot:P0DTD8"
      hgnc "NA"
      map_id "M18_221"
      name "Orf7b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2251"
      uniprot "UNIPROT:P0DTD8"
    ]
    graphics [
      x 2771.476070226575
      y 1269.1295423174286
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_221"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_85"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1098"
      uniprot "NA"
    ]
    graphics [
      x 1942.5144998161113
      y 2067.188237333221
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_266"
      name "Orf7b_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2352"
      uniprot "NA"
    ]
    graphics [
      x 1961.4760702265748
      y 1141.5169836300006
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_266"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_232"
      name "Orf7b_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2302"
      uniprot "NA"
    ]
    graphics [
      x 2261.476070226575
      y 1497.9463324561953
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_232"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_63"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1075"
      uniprot "NA"
    ]
    graphics [
      x 2857.9673850663708
      y 1560.667762998949
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_255"
      name "sa2302_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2340"
      uniprot "NA"
    ]
    graphics [
      x 2561.476070226575
      y 1443.61129803534
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_255"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_74"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1086"
      uniprot "NA"
    ]
    graphics [
      x 2348.85366504139
      y 679.8454321803358
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_173"
      name "Orf7a_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2105"
      uniprot "NA"
    ]
    graphics [
      x 1361.4760702265748
      y 1402.196068670434
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_173"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740573;urn:miriam:uniprot:P0DTC7"
      hgnc "NA"
      map_id "M18_146"
      name "Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1875"
      uniprot "UNIPROT:P0DTC7"
    ]
    graphics [
      x 1871.4760702265748
      y 1147.2331305713067
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_146"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_33"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1014"
      uniprot "NA"
    ]
    graphics [
      x 2114.975251678283
      y 358.86122436149685
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740573;urn:miriam:uniprot:P0DTC7"
      hgnc "NA"
      map_id "M18_215"
      name "Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2245"
      uniprot "UNIPROT:P0DTC7"
    ]
    graphics [
      x 1898.8536650413898
      y 602.8646454446412
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_215"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_40"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1021"
      uniprot "NA"
    ]
    graphics [
      x 1892.5728176983032
      y 363.2143435620635
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740573;urn:miriam:uniprot:P0DTC7"
      hgnc "NA"
      map_id "M18_158"
      name "Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1986"
      uniprot "UNIPROT:P0DTC7"
    ]
    graphics [
      x 1646.090149807047
      y 492.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_158"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_79"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1092"
      uniprot "NA"
    ]
    graphics [
      x 2048.85366504139
      y 981.5721788099976
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_260"
      name "Orf7a_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2346"
      uniprot "NA"
    ]
    graphics [
      x 2441.476070226575
      y 1175.4012878813085
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_260"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_226"
      name "Orf7a_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2296"
      uniprot "NA"
    ]
    graphics [
      x 1151.4760702265748
      y 1075.0176816601365
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_226"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_57"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1068"
      uniprot "NA"
    ]
    graphics [
      x 761.4760702265747
      y 1245.7621802877984
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_249"
      name "sa2296_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2333"
      uniprot "NA"
    ]
    graphics [
      x 638.8536650413897
      y 941.710136427816
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_249"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_68"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1080"
      uniprot "NA"
    ]
    graphics [
      x 1700.982406436663
      y 582.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_62"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1074"
      uniprot "NA"
    ]
    graphics [
      x 1871.4760702265748
      y 1344.7436141671978
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_254"
      name "sa2301_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2339"
      uniprot "NA"
    ]
    graphics [
      x 1117.9673850663708
      y 1692.154598018466
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_254"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_86"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1099"
      uniprot "NA"
    ]
    graphics [
      x 2322.4852046174688
      y 288.56613708486543
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_268"
      name "N_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2354"
      uniprot "NA"
    ]
    graphics [
      x 2198.85366504139
      y 710.3924160902806
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_268"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_87"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1100"
      uniprot "NA"
    ]
    graphics [
      x 2106.56303131958
      y 425.69686909255665
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_269"
      name "N_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2355"
      uniprot "NA"
    ]
    graphics [
      x 1837.7792772981743
      y 253.44586856661226
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_269"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740575"
      hgnc "NA"
      map_id "M18_157"
      name "N_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa1962"
      uniprot "NA"
    ]
    graphics [
      x 1196.090149807047
      y 579.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_157"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_115"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re940"
      uniprot "NA"
    ]
    graphics [
      x 1688.8536650413898
      y 642.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0070992"
      hgnc "NA"
      map_id "M18_15"
      name "Host_space_translation_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa429"
      uniprot "NA"
    ]
    graphics [
      x 1808.8536650413898
      y 706.8134512202714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_135"
      name "N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1685"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 2558.85366504139
      y 929.557714613718
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      annotation "PUBMED:28720894"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_114"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re932"
      uniprot "NA"
    ]
    graphics [
      x 2231.476070226575
      y 1425.8636147371178
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_151"
      name "N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1887"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 1121.4760702265748
      y 1097.885660630759
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_151"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 167
    zlevel -1

    cd19dm [
      annotation "PUBMED:28720894"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_52"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1061"
      uniprot "NA"
    ]
    graphics [
      x 1328.8536650413898
      y 702.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 168
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_11"
      name "Replication_space_transcription_space_complex:N_space_oligomer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa398"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 2171.476070226575
      y 1308.7342851305455
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 169
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_102"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1120"
      uniprot "NA"
    ]
    graphics [
      x 2378.85366504139
      y 604.5342757755901
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 170
    zlevel -1

    cd19dm [
      annotation "PUBMED:28720894"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_119"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re972"
      uniprot "NA"
    ]
    graphics [
      x 2301.6104592494066
      y 1822.963030263297
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 171
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:refseq:NC_045512"
      hgnc "NA"
      map_id "M18_185"
      name "(_plus_)ss_space_gRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2153"
      uniprot "NA"
    ]
    graphics [
      x 2347.9673850663708
      y 1711.500231752103
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_185"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 172
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_10"
      name "Nucleocapsid"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa397"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 2617.9673850663708
      y 1713.3368118287563
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 173
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_20"
      name "Replication_space_transcription_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa440"
      uniprot "NA"
    ]
    graphics [
      x 1058.8536650413898
      y 859.5029478087847
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 174
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_101"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1119"
      uniprot "NA"
    ]
    graphics [
      x 1363.2134271408518
      y 182.4480845100427
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 175
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_116"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re948"
      uniprot "NA"
    ]
    graphics [
      x 2351.476070226575
      y 1247.0644534228556
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 176
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_9"
      name "Nucleocapsid"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa391"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 2032.5144998161113
      y 1850.4619322870956
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 177
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_112"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re895"
      uniprot "NA"
    ]
    graphics [
      x 1271.4760702265748
      y 1512.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 178
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_8"
      name "Nucleocapsid"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa389"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 698.8536650413897
      y 868.7012399288863
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 179
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_111"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re894"
      uniprot "NA"
    ]
    graphics [
      x 1238.8536650413898
      y 672.1788891215108
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 180
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_7"
      name "Nucleocapsid"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa387"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 1361.4760702265748
      y 1059.7135025377963
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 181
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_75"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1088"
      uniprot "NA"
    ]
    graphics [
      x 1508.8536650413898
      y 672.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 182
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:refseq:NC_045512"
      hgnc "NA"
      map_id "M18_256"
      name "ds_space_gRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2342"
      uniprot "NA"
    ]
    graphics [
      x 1760.0383469037017
      y 462.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_256"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 183
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:refseq:NC_045512"
      hgnc "NA"
      map_id "M18_233"
      name "(_minus_)ss_space_gRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2314"
      uniprot "NA"
    ]
    graphics [
      x 1058.8536650413898
      y 683.1222849066672
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_233"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 184
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_51"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1057"
      uniprot "NA"
    ]
    graphics [
      x 1811.4760702265748
      y 1264.8138321341212
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 185
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_234"
      name "sa2314_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2315"
      uniprot "NA"
    ]
    graphics [
      x 2231.476070226575
      y 1079.5869171055397
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_234"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 186
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_100"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1118"
      uniprot "NA"
    ]
    graphics [
      x 1598.8536650413898
      y 672.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 187
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_282"
      name "sa2355_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2368"
      uniprot "NA"
    ]
    graphics [
      x 1301.4760702265748
      y 988.1356953218692
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_282"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 188
    zlevel -1

    cd19dm [
      annotation "PUBMED:11907209;PUBMED:30632963"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_97"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1112"
      uniprot "NA"
    ]
    graphics [
      x 1361.4760702265748
      y 1029.7135025377963
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 189
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725306"
      hgnc "NA"
      map_id "M18_192"
      name "Nsp10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2200"
      uniprot "NA"
    ]
    graphics [
      x 1088.8536650413898
      y 723.3122672958201
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_192"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 190
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725311"
      hgnc "NA"
      map_id "M18_191"
      name "Nsp16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2199"
      uniprot "NA"
    ]
    graphics [
      x 1091.4760702265748
      y 1127.5375698961307
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_191"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 191
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725310"
      hgnc "NA"
      map_id "M18_197"
      name "Nsp15"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2205"
      uniprot "NA"
    ]
    graphics [
      x 968.8536650413897
      y 943.8329489969877
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_197"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 192
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725309"
      hgnc "NA"
      map_id "M18_198"
      name "Nsp14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2206"
      uniprot "NA"
    ]
    graphics [
      x 2051.476070226575
      y 1011.5721788099976
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_198"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 193
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725308"
      hgnc "NA"
      map_id "M18_190"
      name "Nsp13"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2198"
      uniprot "NA"
    ]
    graphics [
      x 1496.090149807047
      y 372.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_190"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 194
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725307"
      hgnc "NA"
      map_id "M18_189"
      name "Nsp12"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2197"
      uniprot "NA"
    ]
    graphics [
      x 1121.4760702265748
      y 1127.885660630759
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_189"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 195
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725305"
      hgnc "NA"
      map_id "M18_193"
      name "Nsp9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2201"
      uniprot "NA"
    ]
    graphics [
      x 1637.2023363703734
      y 972.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_193"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 196
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725304"
      hgnc "NA"
      map_id "M18_194"
      name "Nsp8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2202"
      uniprot "NA"
    ]
    graphics [
      x 1206.0379387088174
      y 458.7157868722718
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_194"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 197
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725303"
      hgnc "NA"
      map_id "M18_195"
      name "Nsp7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2203"
      uniprot "NA"
    ]
    graphics [
      x 1178.8536650413898
      y 851.5054151801744
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_195"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 198
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725302"
      hgnc "NA"
      map_id "M18_196"
      name "Nsp6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2204"
      uniprot "NA"
    ]
    graphics [
      x 637.9673850663706
      y 1722.1662202339423
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_196"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 199
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725299;urn:miriam:uniprot:Nsp3"
      hgnc "NA"
      map_id "M18_203"
      name "Nsp3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2222"
      uniprot "UNIPROT:Nsp3"
    ]
    graphics [
      x 611.4760702265747
      y 1130.7419547629404
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_203"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 200
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725300"
      hgnc "NA"
      map_id "M18_205"
      name "Nsp4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2228"
      uniprot "NA"
    ]
    graphics [
      x 1577.2023363703734
      y 912.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_205"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 201
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725302"
      hgnc "NA"
      map_id "M18_281"
      name "Nsp6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2367"
      uniprot "NA"
    ]
    graphics [
      x 2111.476070226575
      y 1485.3734055475652
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_281"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 202
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725303"
      hgnc "NA"
      map_id "M18_277"
      name "Nsp7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2363"
      uniprot "NA"
    ]
    graphics [
      x 2227.9673850663708
      y 1575.6982431349254
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_277"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 203
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725304"
      hgnc "NA"
      map_id "M18_276"
      name "Nsp8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2362"
      uniprot "NA"
    ]
    graphics [
      x 1658.8536650413898
      y 702.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_276"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 204
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725305"
      hgnc "NA"
      map_id "M18_275"
      name "Nsp9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2361"
      uniprot "NA"
    ]
    graphics [
      x 1693.75410768239
      y 1529.0814698311804
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_275"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 205
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725306"
      hgnc "NA"
      map_id "M18_274"
      name "Nsp10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2360"
      uniprot "NA"
    ]
    graphics [
      x 2381.476070226575
      y 1208.1105328022936
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_274"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 206
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725298"
      hgnc "NA"
      map_id "M18_200"
      name "Nsp2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2217"
      uniprot "NA"
    ]
    graphics [
      x 307.96738506637075
      y 1775.8196661388638
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_200"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 207
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725298"
      hgnc "NA"
      map_id "M18_271"
      name "Nsp2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2357"
      uniprot "NA"
    ]
    graphics [
      x 1808.8536650413898
      y 646.8134512202714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_271"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 208
    zlevel -1

    cd19dm [
      annotation "PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_89"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1104"
      uniprot "NA"
    ]
    graphics [
      x 2021.4760702265748
      y 1150.9809222758117
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 209
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTC1;urn:miriam:ec-code:3.4.22.-;urn:miriam:ncbigene:43740578"
      hgnc "NA"
      map_id "M18_139"
      name "pp1a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1789"
      uniprot "UNIPROT:P0DTC1"
    ]
    graphics [
      x 1931.4760702265748
      y 1022.6254878021133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_139"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 210
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTC1;urn:miriam:ec-code:3.4.22.-;urn:miriam:ncbigene:43740578"
      hgnc "NA"
      map_id "M18_201"
      name "pp1a_space_Nsp3_minus_11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2220"
      uniprot "UNIPROT:P0DTC1"
    ]
    graphics [
      x 2351.476070226575
      y 1346.883873182828
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_201"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 211
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725297"
      hgnc "NA"
      map_id "M18_270"
      name "Nsp1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2356"
      uniprot "NA"
    ]
    graphics [
      x 2261.476070226575
      y 1045.9542133851683
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_270"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 212
    zlevel -1

    cd19dm [
      annotation "PUBMED:23035226"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_90"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re1105"
      uniprot "NA"
    ]
    graphics [
      x 2801.476070226575
      y 1180.7457435088988
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 213
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0006412"
      hgnc "NA"
      map_id "M18_272"
      name "Host_space_translation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa2358"
      uniprot "NA"
    ]
    graphics [
      x 2711.476070226575
      y 1436.5142649245092
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_272"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 214
    zlevel -1

    cd19dm [
      annotation "PUBMED:21203998"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_23"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1001"
      uniprot "NA"
    ]
    graphics [
      x 2741.476070226575
      y 1480.5309794073562
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 215
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M18_210"
      name "pp1a_space_Nsp3_minus_11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2240"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1961.4760702265748
      y 1097.9853441981315
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_210"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 216
    zlevel -1

    cd19dm [
      annotation "PUBMED:21203998;PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_122"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re989"
      uniprot "NA"
    ]
    graphics [
      x 1343.4814210342438
      y 432.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 217
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTC1;urn:miriam:ec-code:3.4.22.-;urn:miriam:ncbigene:43740578"
      hgnc "NA"
      map_id "M18_204"
      name "pp1a_space_Nsp6_minus_11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2224"
      uniprot "UNIPROT:P0DTC1"
    ]
    graphics [
      x 2021.4760702265748
      y 1090.9809222758117
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_204"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 218
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725300;urn:miriam:ncbiprotein:YP_009725299"
      hgnc "NA"
      map_id "M18_273"
      name "Nsp3_minus_4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2359"
      uniprot "NA"
    ]
    graphics [
      x 908.8536650413897
      y 991.8262641726626
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_273"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 219
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725301"
      hgnc "NA"
      map_id "M18_280"
      name "Nsp5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2366"
      uniprot "NA"
    ]
    graphics [
      x 1448.8536650413898
      y 642.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_280"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 220
    zlevel -1

    cd19dm [
      annotation "PUBMED:11907209"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_94"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1109"
      uniprot "NA"
    ]
    graphics [
      x 2318.85366504139
      y 903.9475755283013
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 221
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725312"
      hgnc "NA"
      map_id "M18_278"
      name "Nsp11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2364"
      uniprot "NA"
    ]
    graphics [
      x 2492.102709332617
      y 572.6541314669723
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_278"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 222
    zlevel -1

    cd19dm [
      annotation "PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_91"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1106"
      uniprot "NA"
    ]
    graphics [
      x 1208.8536650413898
      y 639.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 223
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725300;urn:miriam:ncbiprotein:YP_009725299"
      hgnc "NA"
      map_id "M18_279"
      name "Nsp3_minus_4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2365"
      uniprot "NA"
    ]
    graphics [
      x 2168.85366504139
      y 903.6675636343114
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_279"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 224
    zlevel -1

    cd19dm [
      annotation "PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_92"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1107"
      uniprot "NA"
    ]
    graphics [
      x 1931.4760702265748
      y 1330.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 225
    zlevel -1

    cd19dm [
      annotation "PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_88"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1102"
      uniprot "NA"
    ]
    graphics [
      x 221.47607022657485
      y 1217.8056327225354
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 226
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M18_140"
      name "pp1ab"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1790"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1397.2023363703734
      y 942.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_140"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 227
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M18_202"
      name "pp1ab_space_Nsp3_minus_16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2221"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 431.47607022657473
      y 1112.5572932182372
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_202"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 228
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725297"
      hgnc "NA"
      map_id "M18_199"
      name "Nsp1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2216"
      uniprot "NA"
    ]
    graphics [
      x 608.8536650413897
      y 879.8351640363044
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_199"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 229
    zlevel -1

    cd19dm [
      annotation "PUBMED:23035226"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_22"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re1000"
      uniprot "NA"
    ]
    graphics [
      x 941.4760702265747
      y 1218.1386971045129
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 230
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0006412"
      hgnc "NA"
      map_id "M18_207"
      name "Host_space_translation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa2237"
      uniprot "NA"
    ]
    graphics [
      x 1211.4760702265748
      y 999.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_207"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 231
    zlevel -1

    cd19dm [
      annotation "PUBMED:21203998"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_25"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1004"
      uniprot "NA"
    ]
    graphics [
      x 728.8536650413897
      y 603.8334702875894
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 232
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M18_214"
      name "pp1ab_space_Nsp3_minus_16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2244"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 671.4760702265747
      y 1394.2788863566473
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_214"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 233
    zlevel -1

    cd19dm [
      annotation "PUBMED:21203998;PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_26"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1006"
      uniprot "NA"
    ]
    graphics [
      x 1582.5144998161113
      y 2082.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 234
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M18_206"
      name "pp1ab_space_nsp6_minus_16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2229"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 2197.9673850663708
      y 1548.7342851305455
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_206"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 235
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725301"
      hgnc "NA"
      map_id "M18_211"
      name "Nsp5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2241"
      uniprot "NA"
    ]
    graphics [
      x 967.9673850663706
      y 1689.4280235401561
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_211"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 236
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725300;urn:miriam:ncbiprotein:YP_009725299"
      hgnc "NA"
      map_id "M18_212"
      name "Nsp3_minus_4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2242"
      uniprot "NA"
    ]
    graphics [
      x 1241.4760702265748
      y 1416.5060637260106
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_212"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 237
    zlevel -1

    cd19dm [
      annotation "PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_24"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1002"
      uniprot "NA"
    ]
    graphics [
      x 971.4760702265747
      y 1198.6039843924614
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 238
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725300;urn:miriam:ncbiprotein:YP_009725299"
      hgnc "NA"
      map_id "M18_213"
      name "Nsp3_minus_4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2243"
      uniprot "NA"
    ]
    graphics [
      x 431.47607022657473
      y 932.5572932182373
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_213"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 239
    zlevel -1

    cd19dm [
      annotation "PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_93"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1108"
      uniprot "NA"
    ]
    graphics [
      x 878.8536650413897
      y 554.968337584568
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 240
    zlevel -1

    cd19dm [
      annotation "PUBMED:11907209"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_95"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1110"
      uniprot "NA"
    ]
    graphics [
      x 1388.8536650413898
      y 912.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 241
    zlevel -1

    cd19dm [
      annotation "PUBMED:23943763"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_123"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re999"
      uniprot "NA"
    ]
    graphics [
      x 1222.5144998161113
      y 1929.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 242
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0005783"
      hgnc "NA"
      map_id "M18_19"
      name "Endoplasmic_space_reticulum"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa439"
      uniprot "NA"
    ]
    graphics [
      x 2171.476070226575
      y 1398.7342851305455
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 243
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0039718"
      hgnc "NA"
      map_id "M18_18"
      name "Double_minus_membrane_space_vesicle"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa438"
      uniprot "NA"
    ]
    graphics [
      x 937.9673850663706
      y 1486.2857918888562
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 244
    zlevel -1

    cd19dm [
      annotation "PUBMED:28484023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_96"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re1111"
      uniprot "NA"
    ]
    graphics [
      x 1457.2023363703734
      y 912.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 245
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0071360"
      hgnc "NA"
      map_id "M18_222"
      name "cellular_space_response_space_to_space_exogenous_space_dsRNA"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa2291"
      uniprot "NA"
    ]
    graphics [
      x 1241.4760702265748
      y 1212.7306625718877
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_222"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 246
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0070992"
      hgnc "NA"
      map_id "M18_13"
      name "Host_space_translation_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa427"
      uniprot "NA"
    ]
    graphics [
      x 2651.476070226575
      y 1499.483351106661
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 247
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_6"
      name "Nucleocapsid"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa374"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 818.8536650413897
      y 719.7317107292829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 248
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570;urn:miriam:hgnc.symbol:E;urn:miriam:uniprot:P0DTC4"
      hgnc "HGNC_SYMBOL:E"
      map_id "M18_160"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2023"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 337.96738506637075
      y 1666.299901217506
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_160"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 249
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "M18_161"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2024"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 1301.4760702265748
      y 1288.5004699543695
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_161"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 250
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740573;urn:miriam:uniprot:P0DTC7"
      hgnc "NA"
      map_id "M18_162"
      name "Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2025"
      uniprot "UNIPROT:P0DTC7"
    ]
    graphics [
      x 1407.3427285901885
      y 1122.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_162"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 251
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:interpro:IPR002552"
      hgnc "NA"
      map_id "M18_132"
      name "S2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1601"
      uniprot "NA"
    ]
    graphics [
      x 910.2610572070367
      y 256.1851383261592
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 252
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16113"
      hgnc "NA"
      map_id "M18_286"
      name "cholesterol"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa2372"
      uniprot "NA"
    ]
    graphics [
      x 401.47607022657485
      y 1232.5572932182372
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_286"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 253
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:interpro:IPR002551;urn:miriam:ncbigene:8829;urn:miriam:ncbigene:8829;urn:miriam:hgnc:8004;urn:miriam:uniprot:O14786;urn:miriam:uniprot:O14786;urn:miriam:ensembl:ENSG00000099250;urn:miriam:refseq:NM_001024628;urn:miriam:hgnc.symbol:NRP1;urn:miriam:hgnc.symbol:NRP1"
      hgnc "HGNC_SYMBOL:NRP1"
      map_id "M18_21"
      name "S1:NRP1_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa441"
      uniprot "UNIPROT:O14786"
    ]
    graphics [
      x 1336.6057727547209
      y 942.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 254
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570;urn:miriam:hgnc.symbol:E;urn:miriam:uniprot:P0DTC4"
      hgnc "HGNC_SYMBOL:E"
      map_id "M18_169"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2065"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 728.8536650413897
      y 700.5239291616379
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_169"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 255
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "M18_170"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2066"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 338.85366504138983
      y 750.8677471435551
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_170"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 256
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740573;urn:miriam:uniprot:P0DTC7"
      hgnc "NA"
      map_id "M18_171"
      name "Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2067"
      uniprot "UNIPROT:P0DTC7"
    ]
    graphics [
      x 161.47607022657485
      y 1481.0281475497238
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_171"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 257
    zlevel -1

    cd19dm [
      annotation "PUBMED:33082294;PUBMED:33082293"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_104"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1124"
      uniprot "NA"
    ]
    graphics [
      x 281.47607022657485
      y 1211.9364104770766
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 258
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:interpro:IPR002551"
      hgnc "NA"
      map_id "M18_129"
      name "S1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1539"
      uniprot "NA"
    ]
    graphics [
      x 818.8536650413897
      y 749.9643373045923
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_129"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 259
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:8829;urn:miriam:ncbigene:8829;urn:miriam:hgnc:8004;urn:miriam:uniprot:O14786;urn:miriam:uniprot:O14786;urn:miriam:ensembl:ENSG00000099250;urn:miriam:refseq:NM_001024628;urn:miriam:hgnc.symbol:NRP1;urn:miriam:hgnc.symbol:NRP1"
      hgnc "HGNC_SYMBOL:NRP1"
      map_id "M18_285"
      name "NRP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2371"
      uniprot "UNIPROT:O14786"
    ]
    graphics [
      x 461.47607022657473
      y 1413.9352012039044
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_285"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 260
    zlevel -1

    cd19dm [
      annotation "PUBMED:32142651;PUBMED:32362314"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_117"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re950"
      uniprot "NA"
    ]
    graphics [
      x 851.4760702265747
      y 1202.359520848128
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 261
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S;urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:S;HGNC_SYMBOL:ACE2"
      map_id "M18_4"
      name "ACE2:SPIKE_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa368"
      uniprot "UNIPROT:P0DTC2;UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1136.090149807047
      y 540.5543133122671
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 262
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:hgnc:8568;urn:miriam:ensembl:ENSG00000140564;urn:miriam:ec-code:3.4.21.75;urn:miriam:uniprot:P09958;urn:miriam:uniprot:P09958;urn:miriam:ncbigene:5045;urn:miriam:ncbigene:5045;urn:miriam:hgnc.symbol:FURIN;urn:miriam:hgnc.symbol:FURIN;urn:miriam:refseq:NM_002569"
      hgnc "HGNC_SYMBOL:FURIN"
      map_id "M18_156"
      name "FURIN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1921"
      uniprot "UNIPROT:P09958"
    ]
    graphics [
      x 1192.5144998161113
      y 1899.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_156"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 263
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:hgnc:11876;urn:miriam:hgnc.symbol:TMPRSS2;urn:miriam:uniprot:O15393;urn:miriam:uniprot:O15393;urn:miriam:hgnc.symbol:TMPRSS2;urn:miriam:ncbigene:7113;urn:miriam:ncbigene:7113;urn:miriam:ec-code:3.4.21.-;urn:miriam:ensembl:ENSG00000184012;urn:miriam:refseq:NM_001135099"
      hgnc "HGNC_SYMBOL:TMPRSS2"
      map_id "M18_128"
      name "TMPRSS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1537"
      uniprot "UNIPROT:O15393"
    ]
    graphics [
      x 968.8536650413897
      y 604.7293358100384
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 264
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:pubchem.compound:2536"
      hgnc "NA"
      map_id "M18_130"
      name "Camostat_space_mesylate"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1544"
      uniprot "NA"
    ]
    graphics [
      x 581.4760702265747
      y 1272.451730092263
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_130"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 265
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:interpro:IPR002552"
      hgnc "NA"
      map_id "M18_168"
      name "S2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2063"
      uniprot "NA"
    ]
    graphics [
      x 1327.9673850663708
      y 1722.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_168"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 266
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:ACE2"
      map_id "M18_209"
      name "ACE2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2239"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1208.8536650413898
      y 669.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_209"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 267
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:ACE2"
      map_id "M18_208"
      name "ACE2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2238"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 997.9673850663706
      y 1498.6039843924614
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_208"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 268
    zlevel -1

    cd19dm [
      annotation "PUBMED:32047258"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_118"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re952"
      uniprot "NA"
    ]
    graphics [
      x 1352.112491134918
      y 822.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 269
    zlevel -1

    cd19dm [
      annotation "PUBMED:32970989;PUBMED:32142651;PUBMED:32155444;PUBMED:32094589"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_108"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re852"
      uniprot "NA"
    ]
    graphics [
      x 1436.090149807047
      y 432.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 270
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "M18_159"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2009"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 1508.8536650413898
      y 852.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_159"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 271
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:ACE2"
      map_id "M18_131"
      name "ACE2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1545"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1834.732072508272
      y 222.97772031167983
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_131"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 272
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "M18_186"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2173"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 1991.4760702265748
      y 1397.606843746084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_186"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 273
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A28815"
      hgnc "NA"
      map_id "M18_284"
      name "Heparan_space_sulfate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa2370"
      uniprot "NA"
    ]
    graphics [
      x 1778.8536650413898
      y 722.5262609320248
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_284"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 274
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S;urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:S;HGNC_SYMBOL:ACE2"
      map_id "M18_17"
      name "ACE2:SPIKE_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa431"
      uniprot "UNIPROT:P0DTC2;UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1057.9673850663708
      y 1533.6936536421204
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 275
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ensembl:ENSG00000164733;urn:miriam:ncbigene:1508;urn:miriam:ncbigene:1508;urn:miriam:refseq:NM_147780;urn:miriam:uniprot:P07858;urn:miriam:uniprot:P07858;urn:miriam:hgnc:2527;urn:miriam:ec-code:3.4.22.1;urn:miriam:hgnc.symbol:CTSB;urn:miriam:hgnc.symbol:CTSB"
      hgnc "HGNC_SYMBOL:CTSB"
      map_id "M18_126"
      name "CTSB"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1524"
      uniprot "UNIPROT:P07858"
    ]
    graphics [
      x 1808.8536650413898
      y 856.8134512202714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 276
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.22.15;urn:miriam:hgnc.symbol:CTSL;urn:miriam:ncbigene:1514;urn:miriam:hgnc.symbol:CTSL;urn:miriam:ncbigene:1514;urn:miriam:uniprot:P07711;urn:miriam:uniprot:P07711;urn:miriam:ensembl:ENSG00000135047;urn:miriam:refseq:NM_001912;urn:miriam:hgnc:2537"
      hgnc "HGNC_SYMBOL:CTSL"
      map_id "M18_127"
      name "CTSL"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1525"
      uniprot "UNIPROT:P07711"
    ]
    graphics [
      x 1747.9673850663708
      y 1555.9277406352314
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_127"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 277
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:interpro:IPR002551"
      hgnc "NA"
      map_id "M18_125"
      name "S1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1516"
      uniprot "NA"
    ]
    graphics [
      x 1028.8536650413898
      y 830.911115411976
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_125"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 278
    source 2
    target 1
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_107"
      target_id "M18_188"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 1
    target 3
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_188"
      target_id "M18_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 274
    target 2
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_17"
      target_id "M18_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 275
    target 2
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "M18_126"
      target_id "M18_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 276
    target 2
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "M18_127"
      target_id "M18_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 2
    target 277
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_107"
      target_id "M18_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 3
    target 4
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_121"
      target_id "M18_164"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 4
    target 5
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M18_164"
      target_id "M18_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 6
    target 5
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_3"
      target_id "M18_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 7
    target 5
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_167"
      target_id "M18_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 8
    target 5
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_165"
      target_id "M18_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 9
    target 5
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_166"
      target_id "M18_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 5
    target 10
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_120"
      target_id "M18_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 5
    target 11
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_120"
      target_id "M18_182"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 5
    target 12
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_120"
      target_id "M18_183"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 5
    target 13
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_120"
      target_id "M18_184"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 14
    target 10
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_109"
      target_id "M18_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 10
    target 15
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_5"
      target_id "M18_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 247
    target 14
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_6"
      target_id "M18_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 248
    target 14
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_160"
      target_id "M18_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 249
    target 14
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_161"
      target_id "M18_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 250
    target 14
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_162"
      target_id "M18_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 251
    target 14
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M18_132"
      target_id "M18_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 252
    target 14
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M18_286"
      target_id "M18_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 253
    target 14
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M18_21"
      target_id "M18_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 14
    target 254
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_109"
      target_id "M18_169"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 304
    source 14
    target 255
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_109"
      target_id "M18_170"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 305
    source 14
    target 256
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_109"
      target_id "M18_171"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 306
    source 15
    target 16
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_110"
      target_id "M18_134"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 307
    source 15
    target 17
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_110"
      target_id "M18_133"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 308
    source 16
    target 20
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_134"
      target_id "M18_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 309
    source 16
    target 21
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_134"
      target_id "M18_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 310
    source 16
    target 22
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_134"
      target_id "M18_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 311
    source 17
    target 18
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_133"
      target_id "M18_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 312
    source 18
    target 19
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_113"
      target_id "M18_155"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 313
    source 246
    target 20
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_13"
      target_id "M18_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 314
    source 20
    target 226
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_99"
      target_id "M18_140"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 315
    source 246
    target 21
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_13"
      target_id "M18_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 316
    source 21
    target 209
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_98"
      target_id "M18_139"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 317
    source 23
    target 22
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 318
    source 22
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_53"
      target_id "M18_245"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 319
    source 188
    target 23
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_97"
      target_id "M18_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 320
    source 174
    target 23
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_101"
      target_id "M18_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 321
    source 23
    target 108
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 322
    source 23
    target 169
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_12"
      target_id "M18_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 323
    source 23
    target 120
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 324
    source 23
    target 88
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 325
    source 23
    target 132
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 326
    source 23
    target 157
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 327
    source 23
    target 98
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 328
    source 23
    target 25
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 329
    source 23
    target 78
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 330
    source 23
    target 38
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 331
    source 23
    target 154
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 332
    source 23
    target 26
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 333
    source 23
    target 68
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 334
    source 23
    target 142
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 335
    source 24
    target 25
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_245"
      target_id "M18_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 336
    source 24
    target 26
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_245"
      target_id "M18_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 337
    source 25
    target 182
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_64"
      target_id "M18_256"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 338
    source 26
    target 27
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_235"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 339
    source 26
    target 28
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_267"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 340
    source 26
    target 29
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_244"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 341
    source 26
    target 30
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_243"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 342
    source 26
    target 31
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_242"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 343
    source 26
    target 32
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_241"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 344
    source 26
    target 33
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_240"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 345
    source 26
    target 34
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_239"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 346
    source 26
    target 35
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_238"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 347
    source 26
    target 36
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_237"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 348
    source 26
    target 37
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_236"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 349
    source 27
    target 142
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_235"
      target_id "M18_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 350
    source 28
    target 157
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_267"
      target_id "M18_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 351
    source 29
    target 132
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_244"
      target_id "M18_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 352
    source 30
    target 120
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_243"
      target_id "M18_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 353
    source 31
    target 68
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_242"
      target_id "M18_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 354
    source 32
    target 154
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_241"
      target_id "M18_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 355
    source 33
    target 98
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_240"
      target_id "M18_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 356
    source 34
    target 88
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_239"
      target_id "M18_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 357
    source 35
    target 78
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_238"
      target_id "M18_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 358
    source 36
    target 108
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_237"
      target_id "M18_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 359
    source 37
    target 38
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_236"
      target_id "M18_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 360
    source 38
    target 39
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_73"
      target_id "M18_265"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 361
    source 39
    target 40
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_265"
      target_id "M18_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 362
    source 40
    target 41
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_84"
      target_id "M18_231"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 363
    source 40
    target 42
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_84"
      target_id "M18_178"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 364
    source 41
    target 155
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_231"
      target_id "M18_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 365
    source 42
    target 43
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_178"
      target_id "M18_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 366
    source 44
    target 43
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 367
    source 43
    target 45
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_49"
      target_id "M18_148"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 368
    source 44
    target 48
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 369
    source 44
    target 49
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 370
    source 44
    target 50
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 371
    source 44
    target 51
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 372
    source 44
    target 52
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 373
    source 44
    target 53
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 374
    source 44
    target 54
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 375
    source 44
    target 55
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 376
    source 44
    target 56
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 377
    source 45
    target 46
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_148"
      target_id "M18_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 378
    source 46
    target 47
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_38"
      target_id "M18_220"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 379
    source 143
    target 48
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_173"
      target_id "M18_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 380
    source 48
    target 144
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_44"
      target_id "M18_146"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 381
    source 133
    target 49
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_174"
      target_id "M18_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 382
    source 49
    target 134
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_50"
      target_id "M18_147"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 383
    source 121
    target 50
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_181"
      target_id "M18_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 384
    source 50
    target 122
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_41"
      target_id "M18_136"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 385
    source 109
    target 51
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_179"
      target_id "M18_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 386
    source 51
    target 110
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_42"
      target_id "M18_137"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 387
    source 99
    target 52
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_177"
      target_id "M18_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 388
    source 52
    target 100
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_48"
      target_id "M18_149"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 389
    source 89
    target 53
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_176"
      target_id "M18_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 390
    source 53
    target 90
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_45"
      target_id "M18_145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 391
    source 79
    target 54
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_172"
      target_id "M18_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 392
    source 54
    target 80
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_46"
      target_id "M18_144"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 393
    source 69
    target 55
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_175"
      target_id "M18_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 394
    source 55
    target 70
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_47"
      target_id "M18_150"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 395
    source 57
    target 56
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_180"
      target_id "M18_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 396
    source 56
    target 58
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_43"
      target_id "M18_138"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 397
    source 63
    target 57
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_78"
      target_id "M18_180"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 398
    source 58
    target 59
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_138"
      target_id "M18_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 399
    source 59
    target 60
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_32"
      target_id "M18_154"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 400
    source 60
    target 61
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_154"
      target_id "M18_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 401
    source 61
    target 62
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_29"
      target_id "M18_143"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 402
    source 64
    target 63
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_259"
      target_id "M18_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 403
    source 63
    target 65
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_78"
      target_id "M18_225"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 404
    source 68
    target 64
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_67"
      target_id "M18_259"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 405
    source 65
    target 66
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_225"
      target_id "M18_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 406
    source 66
    target 67
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_56"
      target_id "M18_248"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 407
    source 73
    target 69
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_82"
      target_id "M18_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 408
    source 70
    target 71
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_150"
      target_id "M18_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 409
    source 71
    target 72
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_36"
      target_id "M18_218"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 410
    source 74
    target 73
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_263"
      target_id "M18_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 411
    source 73
    target 75
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_82"
      target_id "M18_229"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 412
    source 78
    target 74
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_71"
      target_id "M18_263"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 413
    source 75
    target 76
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_229"
      target_id "M18_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 414
    source 76
    target 77
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_60"
      target_id "M18_252"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 415
    source 83
    target 79
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_81"
      target_id "M18_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 416
    source 80
    target 81
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_144"
      target_id "M18_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 417
    source 81
    target 82
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_35"
      target_id "M18_217"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 418
    source 84
    target 83
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_262"
      target_id "M18_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 419
    source 83
    target 85
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_81"
      target_id "M18_228"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 420
    source 88
    target 84
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_70"
      target_id "M18_262"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 421
    source 85
    target 86
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_228"
      target_id "M18_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 422
    source 86
    target 87
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_59"
      target_id "M18_251"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 423
    source 93
    target 89
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_80"
      target_id "M18_176"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 424
    source 90
    target 91
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_145"
      target_id "M18_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 425
    source 91
    target 92
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_34"
      target_id "M18_216"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 426
    source 94
    target 93
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_261"
      target_id "M18_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 427
    source 93
    target 95
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_80"
      target_id "M18_227"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 428
    source 98
    target 94
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_69"
      target_id "M18_261"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 429
    source 95
    target 96
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_227"
      target_id "M18_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 430
    source 96
    target 97
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_58"
      target_id "M18_250"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 431
    source 103
    target 99
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_83"
      target_id "M18_177"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 432
    source 100
    target 101
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_149"
      target_id "M18_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 433
    source 101
    target 102
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_37"
      target_id "M18_219"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 434
    source 104
    target 103
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_264"
      target_id "M18_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 435
    source 103
    target 105
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_83"
      target_id "M18_230"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 436
    source 108
    target 104
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_72"
      target_id "M18_264"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 437
    source 105
    target 106
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_230"
      target_id "M18_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 438
    source 106
    target 107
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_61"
      target_id "M18_253"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 439
    source 115
    target 109
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_77"
      target_id "M18_179"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 440
    source 110
    target 111
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_137"
      target_id "M18_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 441
    source 111
    target 112
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_31"
      target_id "M18_153"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 442
    source 112
    target 113
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_153"
      target_id "M18_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 443
    source 113
    target 114
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_28"
      target_id "M18_142"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 444
    source 116
    target 115
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_258"
      target_id "M18_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 445
    source 115
    target 117
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_77"
      target_id "M18_224"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 446
    source 120
    target 116
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_66"
      target_id "M18_258"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 447
    source 117
    target 118
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_224"
      target_id "M18_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 448
    source 118
    target 119
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_55"
      target_id "M18_247"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 449
    source 127
    target 121
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_76"
      target_id "M18_181"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 450
    source 122
    target 123
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_136"
      target_id "M18_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 451
    source 123
    target 124
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_30"
      target_id "M18_152"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 452
    source 124
    target 125
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_152"
      target_id "M18_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 453
    source 125
    target 126
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_27"
      target_id "M18_141"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 454
    source 128
    target 127
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_257"
      target_id "M18_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 455
    source 127
    target 129
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_76"
      target_id "M18_223"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 456
    source 132
    target 128
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_65"
      target_id "M18_257"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 457
    source 129
    target 130
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_223"
      target_id "M18_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 458
    source 130
    target 131
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_54"
      target_id "M18_246"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 459
    source 137
    target 133
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_85"
      target_id "M18_174"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 460
    source 134
    target 135
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_147"
      target_id "M18_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 461
    source 135
    target 136
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_39"
      target_id "M18_221"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 462
    source 138
    target 137
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_266"
      target_id "M18_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 463
    source 137
    target 139
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_85"
      target_id "M18_232"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 464
    source 142
    target 138
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_74"
      target_id "M18_266"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 465
    source 139
    target 140
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_232"
      target_id "M18_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 466
    source 140
    target 141
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_63"
      target_id "M18_255"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 467
    source 149
    target 143
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_79"
      target_id "M18_173"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 468
    source 144
    target 145
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_146"
      target_id "M18_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 469
    source 145
    target 146
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_33"
      target_id "M18_215"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 470
    source 146
    target 147
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_215"
      target_id "M18_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 471
    source 147
    target 148
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_40"
      target_id "M18_158"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 472
    source 150
    target 149
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_260"
      target_id "M18_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 473
    source 149
    target 151
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_79"
      target_id "M18_226"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 474
    source 154
    target 150
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_68"
      target_id "M18_260"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 475
    source 151
    target 152
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_226"
      target_id "M18_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 476
    source 152
    target 153
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_57"
      target_id "M18_249"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 477
    source 155
    target 156
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_62"
      target_id "M18_254"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 478
    source 157
    target 158
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_86"
      target_id "M18_268"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 479
    source 158
    target 159
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_268"
      target_id "M18_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 480
    source 159
    target 160
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_87"
      target_id "M18_269"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 481
    source 159
    target 161
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_87"
      target_id "M18_157"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 482
    source 160
    target 186
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_269"
      target_id "M18_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 483
    source 161
    target 162
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_157"
      target_id "M18_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 484
    source 163
    target 162
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_15"
      target_id "M18_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 485
    source 162
    target 164
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_115"
      target_id "M18_135"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 486
    source 164
    target 165
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_135"
      target_id "M18_114"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 487
    source 165
    target 166
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_114"
      target_id "M18_151"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 488
    source 166
    target 167
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_151"
      target_id "M18_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 489
    source 167
    target 168
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_52"
      target_id "M18_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 490
    source 169
    target 168
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_102"
      target_id "M18_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 491
    source 168
    target 170
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_11"
      target_id "M18_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 492
    source 171
    target 170
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_185"
      target_id "M18_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 493
    source 170
    target 172
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_119"
      target_id "M18_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 494
    source 170
    target 173
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_119"
      target_id "M18_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 495
    source 181
    target 171
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_75"
      target_id "M18_185"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 496
    source 172
    target 175
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_10"
      target_id "M18_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 497
    source 173
    target 174
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_20"
      target_id "M18_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 498
    source 175
    target 176
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_116"
      target_id "M18_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 499
    source 176
    target 177
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_9"
      target_id "M18_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 500
    source 177
    target 178
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_112"
      target_id "M18_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 501
    source 178
    target 179
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_8"
      target_id "M18_111"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 502
    source 179
    target 180
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_111"
      target_id "M18_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 503
    source 182
    target 181
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_256"
      target_id "M18_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 504
    source 181
    target 183
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_75"
      target_id "M18_233"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 505
    source 183
    target 184
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_233"
      target_id "M18_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 506
    source 184
    target 185
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_51"
      target_id "M18_234"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 507
    source 186
    target 187
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_100"
      target_id "M18_282"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 508
    source 189
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_192"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 509
    source 190
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_191"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 510
    source 191
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_197"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 511
    source 192
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_198"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 512
    source 193
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_190"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 513
    source 194
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_189"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 514
    source 195
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_193"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 515
    source 196
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_194"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 516
    source 197
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_195"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 517
    source 198
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_196"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 518
    source 199
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_203"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 519
    source 200
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_205"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 520
    source 201
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_281"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 521
    source 202
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_277"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 522
    source 203
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_276"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 523
    source 204
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_275"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 524
    source 205
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_274"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 525
    source 206
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_200"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 526
    source 207
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_271"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 527
    source 240
    target 189
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_192"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 528
    source 240
    target 190
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_191"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 529
    source 240
    target 191
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_197"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 530
    source 191
    target 244
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_197"
      target_id "M18_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 531
    source 240
    target 192
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_198"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 532
    source 240
    target 193
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_190"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 533
    source 240
    target 194
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_189"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 534
    source 240
    target 195
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_193"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 535
    source 240
    target 196
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_194"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 536
    source 240
    target 197
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_195"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 537
    source 240
    target 198
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_196"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 538
    source 198
    target 241
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "MODULATION"
      source_id "M18_196"
      target_id "M18_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 539
    source 239
    target 199
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_93"
      target_id "M18_203"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 540
    source 224
    target 199
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_92"
      target_id "M18_203"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 541
    source 199
    target 241
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "MODULATION"
      source_id "M18_203"
      target_id "M18_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 542
    source 239
    target 200
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_93"
      target_id "M18_205"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 543
    source 224
    target 200
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_92"
      target_id "M18_205"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 544
    source 200
    target 241
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "MODULATION"
      source_id "M18_205"
      target_id "M18_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 545
    source 220
    target 201
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_94"
      target_id "M18_281"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 546
    source 201
    target 241
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "MODULATION"
      source_id "M18_281"
      target_id "M18_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 547
    source 220
    target 202
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_94"
      target_id "M18_277"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 548
    source 220
    target 203
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_94"
      target_id "M18_276"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 549
    source 220
    target 204
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_94"
      target_id "M18_275"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 550
    source 220
    target 205
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_94"
      target_id "M18_274"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 551
    source 225
    target 206
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_88"
      target_id "M18_200"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 552
    source 208
    target 207
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_89"
      target_id "M18_271"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 553
    source 209
    target 208
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_139"
      target_id "M18_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 554
    source 208
    target 210
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_89"
      target_id "M18_201"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 555
    source 208
    target 211
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_89"
      target_id "M18_270"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 556
    source 210
    target 214
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_201"
      target_id "M18_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 557
    source 211
    target 212
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_270"
      target_id "M18_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 558
    source 212
    target 213
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_90"
      target_id "M18_272"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 559
    source 214
    target 215
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_23"
      target_id "M18_210"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 560
    source 215
    target 216
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_210"
      target_id "M18_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 561
    source 216
    target 217
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_122"
      target_id "M18_204"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 562
    source 216
    target 218
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_122"
      target_id "M18_273"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 563
    source 216
    target 219
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_122"
      target_id "M18_280"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 564
    source 217
    target 220
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_204"
      target_id "M18_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 565
    source 218
    target 222
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_273"
      target_id "M18_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 566
    source 219
    target 220
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "M18_280"
      target_id "M18_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 567
    source 220
    target 221
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_94"
      target_id "M18_278"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 568
    source 222
    target 223
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_91"
      target_id "M18_279"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 569
    source 223
    target 224
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_279"
      target_id "M18_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 570
    source 226
    target 225
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_140"
      target_id "M18_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 571
    source 225
    target 227
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_88"
      target_id "M18_202"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 572
    source 225
    target 228
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_88"
      target_id "M18_199"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 573
    source 227
    target 231
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_202"
      target_id "M18_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 574
    source 228
    target 229
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_199"
      target_id "M18_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 575
    source 229
    target 230
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_22"
      target_id "M18_207"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 576
    source 231
    target 232
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_25"
      target_id "M18_214"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 577
    source 232
    target 233
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_214"
      target_id "M18_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 578
    source 233
    target 234
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_26"
      target_id "M18_206"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 579
    source 233
    target 235
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_26"
      target_id "M18_211"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 580
    source 233
    target 236
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_26"
      target_id "M18_212"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 581
    source 234
    target 240
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_206"
      target_id "M18_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 582
    source 235
    target 240
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "M18_211"
      target_id "M18_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 583
    source 236
    target 237
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_212"
      target_id "M18_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 584
    source 237
    target 238
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_24"
      target_id "M18_213"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 585
    source 238
    target 239
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_213"
      target_id "M18_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 586
    source 242
    target 241
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_19"
      target_id "M18_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 587
    source 241
    target 243
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_123"
      target_id "M18_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 588
    source 244
    target 245
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_96"
      target_id "M18_222"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 589
    source 268
    target 251
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_118"
      target_id "M18_132"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 590
    source 257
    target 253
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_104"
      target_id "M18_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 591
    source 258
    target 257
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_129"
      target_id "M18_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 592
    source 259
    target 257
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_285"
      target_id "M18_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 593
    source 260
    target 258
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_117"
      target_id "M18_129"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 594
    source 261
    target 260
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_4"
      target_id "M18_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 595
    source 262
    target 260
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "M18_156"
      target_id "M18_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 596
    source 263
    target 260
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "M18_128"
      target_id "M18_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 597
    source 264
    target 260
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "INHIBITION"
      source_id "M18_130"
      target_id "M18_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 598
    source 260
    target 265
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_117"
      target_id "M18_168"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 599
    source 260
    target 266
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_117"
      target_id "M18_209"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 600
    source 260
    target 267
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_117"
      target_id "M18_208"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 601
    source 269
    target 261
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_108"
      target_id "M18_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 602
    source 265
    target 268
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_168"
      target_id "M18_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 603
    source 270
    target 269
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_159"
      target_id "M18_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 604
    source 271
    target 269
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_131"
      target_id "M18_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 605
    source 272
    target 269
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_186"
      target_id "M18_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 606
    source 273
    target 269
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M18_284"
      target_id "M18_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
