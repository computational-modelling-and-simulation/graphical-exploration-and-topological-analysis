# generated with VANTED V2.8.2 at Fri Mar 04 10:04:38 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:ncbigene:3553"
      hgnc "HGNC_SYMBOL:IL1B"
      map_id "M122_246"
      name "proIL_minus_1B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa462"
      uniprot "UNIPROT:P01584"
    ]
    graphics [
      x 1331.4760702265748
      y 1182.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_246"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_68"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re175"
      uniprot "NA"
    ]
    graphics [
      x 1271.4760702265748
      y 1122.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M122_243"
      name "CASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa457"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 1118.8536650413898
      y 810.5543133122671
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_243"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:ncbigene:3553"
      hgnc "HGNC_SYMBOL:IL1B"
      map_id "M122_248"
      name "IL_minus_1B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa464"
      uniprot "UNIPROT:P01584"
    ]
    graphics [
      x 938.8536650413897
      y 668.4049804982296
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_248"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:ncbigene:3553"
      hgnc "HGNC_SYMBOL:IL1B"
      map_id "M122_244"
      name "proIL_minus_1B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa460"
      uniprot "UNIPROT:P01584"
    ]
    graphics [
      x 1641.6104592494069
      y 1752.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_244"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_69"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re176"
      uniprot "NA"
    ]
    graphics [
      x 641.4760702265747
      y 1124.0156442515633
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:ncbigene:3553"
      hgnc "HGNC_SYMBOL:IL1B"
      map_id "M122_250"
      name "IL_minus_1B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa466"
      uniprot "UNIPROT:P01584"
    ]
    graphics [
      x 1222.5144998161113
      y 2019.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_250"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 8
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_246"
      target_id "M122_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 9
    source 3
    target 2
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_243"
      target_id "M122_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 10
    source 2
    target 4
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_68"
      target_id "M122_248"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 11
    source 2
    target 5
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_68"
      target_id "M122_244"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 12
    source 4
    target 6
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_248"
      target_id "M122_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 13
    source 6
    target 7
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_69"
      target_id "M122_250"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
