# generated with VANTED V2.8.2 at Fri Mar 04 10:04:37 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:refseq:NM_198923;urn:miriam:ncbigene:116512;urn:miriam:ncbigene:116512;urn:miriam:hgnc.symbol:MRGPRD;urn:miriam:hgnc.symbol:MRGPRD;urn:miriam:uniprot:Q8TDS7;urn:miriam:uniprot:Q8TDS7;urn:miriam:hgnc:29626;urn:miriam:ensembl:ENSG00000172938"
      hgnc "HGNC_SYMBOL:MRGPRD"
      map_id "M12_184"
      name "MRGPRD"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa94"
      uniprot "UNIPROT:Q8TDS7"
    ]
    graphics [
      x 1045.628701993748
      y 2649.121530297373
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_184"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:23446738"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_88"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re90"
      uniprot "NA"
    ]
    graphics [
      x 1498.5031235907172
      y 2232.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "PUBMED:27217404"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_27"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re141"
      uniprot "NA"
    ]
    graphics [
      x 862.1792674198781
      y 2398.0723472521104
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A55438"
      hgnc "NA"
      map_id "M12_146"
      name "angiotensin_space_1_minus_7"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa24"
      uniprot "NA"
    ]
    graphics [
      x 1121.4760702265748
      y 1158.9296873026296
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_146"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:refseq:NM_198923;urn:miriam:ncbigene:116512;urn:miriam:ncbigene:116512;urn:miriam:hgnc.symbol:MRGPRD;urn:miriam:hgnc.symbol:MRGPRD;urn:miriam:uniprot:Q8TDS7;urn:miriam:uniprot:Q8TDS7;urn:miriam:hgnc:29626;urn:miriam:ensembl:ENSG00000172938"
      hgnc "HGNC_SYMBOL:MRGPRD"
      map_id "M12_168"
      name "MRGPRD"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa50"
      uniprot "UNIPROT:Q8TDS7"
    ]
    graphics [
      x 1492.5144998161113
      y 2202.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_168"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "PUBMED:23446738"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_31"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re146"
      uniprot "NA"
    ]
    graphics [
      x 1912.5144998161113
      y 1810.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "PUBMED:30918468"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_84"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re74"
      uniprot "NA"
    ]
    graphics [
      x 1432.5144998161113
      y 2172.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0006954"
      hgnc "NA"
      map_id "M12_172"
      name "inflammatory_space_response"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa61"
      uniprot "NA"
    ]
    graphics [
      x 1087.9673850663708
      y 1594.5193573312768
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_172"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "PUBMED:30918468"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_85"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re75"
      uniprot "NA"
    ]
    graphics [
      x 292.5144998161113
      y 1935.5610733755368
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "PUBMED:30404071"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_49"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re181"
      uniprot "NA"
    ]
    graphics [
      x 427.96738506637064
      y 1832.5572932182372
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "PUBMED:27038740"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_29"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re144"
      uniprot "NA"
    ]
    graphics [
      x 1211.4760702265748
      y 1389.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:ec-code:3.4.11.3;urn:miriam:refseq:NM_005575;urn:miriam:ensembl:ENSG00000113441;urn:miriam:hgnc:6656;urn:miriam:hgnc.symbol:LNPEP;urn:miriam:hgnc.symbol:LNPEP;urn:miriam:ncbigene:4012;urn:miriam:uniprot:Q9UIQ6;urn:miriam:uniprot:Q9UIQ6;urn:miriam:ncbigene:4012"
      hgnc "HGNC_SYMBOL:LNPEP"
      map_id "M12_112"
      name "LNPEP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa156"
      uniprot "UNIPROT:Q9UIQ6"
    ]
    graphics [
      x 1464.1900265952593
      y 1572.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "PUBMED:30934934"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re151"
      uniprot "NA"
    ]
    graphics [
      x 1958.8536650413898
      y 932.6254878021133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "PUBMED:11707427"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_13"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re122"
      uniprot "NA"
    ]
    graphics [
      x 1732.5144998161113
      y 1855.9277406352314
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "PUBMED:9493859"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_30"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re145"
      uniprot "NA"
    ]
    graphics [
      x 1088.8536650413898
      y 836.5781614917847
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "PUBMED:10234025;PUBMED:30934934"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_64"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re196"
      uniprot "NA"
    ]
    graphics [
      x 940.4146427238683
      y 2146.6019746441584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "PUBMED:32127770"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_53"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re185"
      uniprot "NA"
    ]
    graphics [
      x 1360.4146427238684
      y 2352.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:mesh:D009410"
      hgnc "NA"
      map_id "M12_127"
      name "neurodegeneration"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa190"
      uniprot "NA"
    ]
    graphics [
      x 1457.7845707541828
      y 2082.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_127"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:mesh:D003071"
      hgnc "NA"
      map_id "M12_133"
      name "cognition"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa197"
      uniprot "NA"
    ]
    graphics [
      x 989.1188547295466
      y 2320.489597348719
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_133"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "PUBMED:32127770;PUBMED:25124854"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_63"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re195"
      uniprot "NA"
    ]
    graphics [
      x 772.5144998161113
      y 2009.8375764008306
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:hgnc:336;urn:miriam:refseq:NM_000685;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:uniprot:P30556;urn:miriam:uniprot:P30556;urn:miriam:ensembl:ENSG00000144891;urn:miriam:ncbigene:185;urn:miriam:ncbigene:185"
      hgnc "HGNC_SYMBOL:AGTR1"
      map_id "M12_95"
      name "AGTR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa102"
      uniprot "UNIPROT:P30556"
    ]
    graphics [
      x 1097.232625398789
      y 2064.3039079152877
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "PUBMED:29928987"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_23"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re136"
      uniprot "NA"
    ]
    graphics [
      x 1750.4146427238684
      y 2222.5142956977406
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "PUBMED:24530803"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_22"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re135"
      uniprot "NA"
    ]
    graphics [
      x 671.4760702265747
      y 1256.649450965198
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "PUBMED:26497614;PUBMED:17138938;PUBMED:32333398;PUBMED:17630322"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_92"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re96"
      uniprot "NA"
    ]
    graphics [
      x 352.5144998161113
      y 2102.557293218237
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "PUBMED:16007097"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_69"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re202"
      uniprot "NA"
    ]
    graphics [
      x 1011.6104592494069
      y 1772.3785906698167
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "PUBMED:19834109"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_70"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re203"
      uniprot "NA"
    ]
    graphics [
      x 1109.1188547295465
      y 2336.175925318014
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "PUBMED:30404071;PUBMED:25666589"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_52"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re184"
      uniprot "NA"
    ]
    graphics [
      x 1391.4760702265748
      y 1269.9618625767896
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      annotation "PUBMED:12754187"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_50"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re182"
      uniprot "NA"
    ]
    graphics [
      x 1211.4760702265748
      y 1359.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "PUBMED:30404071"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_51"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re183"
      uniprot "NA"
    ]
    graphics [
      x 1376.775965560892
      y 2292.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "PUBMED:23884911"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_47"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re177"
      uniprot "NA"
    ]
    graphics [
      x 1012.286102159458
      y 2211.9577197196427
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:mesh:D013927"
      hgnc "NA"
      map_id "M12_124"
      name "thrombosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa172"
      uniprot "NA"
    ]
    graphics [
      x 457.96738506637064
      y 1726.793183776992
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_124"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "PUBMED:18026570"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_42"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re161"
      uniprot "NA"
    ]
    graphics [
      x 371.47607022657485
      y 1108.3467144743568
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:refseq:NM_002377;urn:miriam:ensembl:ENSG00000130368;urn:miriam:hgnc:6899;urn:miriam:ncbigene:4142;urn:miriam:ncbigene:4142;urn:miriam:hgnc.symbol:MAS1;urn:miriam:uniprot:P04201;urn:miriam:uniprot:P04201;urn:miriam:hgnc.symbol:MAS1"
      hgnc "HGNC_SYMBOL:MAS1"
      map_id "M12_149"
      name "MAS1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa27"
      uniprot "UNIPROT:P04201"
    ]
    graphics [
      x 247.96738506637075
      y 1583.575428471855
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_149"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "PUBMED:27660028"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_75"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re42"
      uniprot "NA"
    ]
    graphics [
      x 761.4760702265747
      y 1132.5286933952282
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "PUBMED:29287092"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_15"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re127"
      uniprot "NA"
    ]
    graphics [
      x 221.47607022657485
      y 1497.3898508990828
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "PUBMED:27217404"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_26"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re140"
      uniprot "NA"
    ]
    graphics [
      x 371.47607022657485
      y 1230.8746574339152
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "PUBMED:20581171"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_39"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re159"
      uniprot "NA"
    ]
    graphics [
      x 1121.4760702265748
      y 1368.9296873026296
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "PUBMED:12829792"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_28"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re142"
      uniprot "NA"
    ]
    graphics [
      x 787.9673850663706
      y 1607.1654913580128
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "PUBMED:20581171"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_41"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re160"
      uniprot "NA"
    ]
    graphics [
      x 1462.5144998161113
      y 1872.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0006979"
      hgnc "NA"
      map_id "M12_113"
      name "oxidative_space_stress"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa157"
      uniprot "NA"
    ]
    graphics [
      x 1297.9673850663708
      y 1662.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:mesh:D014661"
      hgnc "NA"
      map_id "M12_171"
      name "vasoconstriction"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa56"
      uniprot "NA"
    ]
    graphics [
      x 1661.4760702265748
      y 1397.5725709100786
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_171"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "PUBMED:30404071"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_86"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re77"
      uniprot "NA"
    ]
    graphics [
      x 2291.476070226575
      y 1294.1444038133232
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000180772;urn:miriam:hgnc.symbol:AGTR2;urn:miriam:hgnc.symbol:AGTR2;urn:miriam:hgnc:338;urn:miriam:refseq:NM_000686;urn:miriam:uniprot:P50052;urn:miriam:uniprot:P50052;urn:miriam:ncbigene:186;urn:miriam:ncbigene:186"
      hgnc "HGNC_SYMBOL:AGTR2"
      map_id "M12_147"
      name "AGTR2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa25"
      uniprot "UNIPROT:P50052"
    ]
    graphics [
      x 937.9673850663706
      y 1537.4637682042728
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_147"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "PUBMED:15767466"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_24"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re138"
      uniprot "NA"
    ]
    graphics [
      x 1505.5335816614925
      y 792.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "PUBMED:25014541"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_81"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re67"
      uniprot "NA"
    ]
    graphics [
      x 491.47607022657473
      y 1497.0918947654488
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      annotation "PUBMED:17138938;PUBMED:17630322"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_87"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re81"
      uniprot "NA"
    ]
    graphics [
      x 1091.4760702265748
      y 1397.5375698961307
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "PUBMED:30048754"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_25"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re139"
      uniprot "NA"
    ]
    graphics [
      x 264.6559435319234
      y 933.2110459180557
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      annotation "PUBMED:24463937"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_43"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re165"
      uniprot "NA"
    ]
    graphics [
      x 1201.843839019008
      y 2352.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:mesh:D005355"
      hgnc "NA"
      map_id "M12_123"
      name "fibrosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa171"
      uniprot "NA"
    ]
    graphics [
      x 2061.6104592494066
      y 1760.4619322870956
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000180772;urn:miriam:hgnc.symbol:AGTR2;urn:miriam:hgnc.symbol:AGTR2;urn:miriam:hgnc:338;urn:miriam:refseq:NM_000686;urn:miriam:uniprot:P50052;urn:miriam:uniprot:P50052;urn:miriam:ncbigene:186;urn:miriam:ncbigene:186"
      hgnc "HGNC_SYMBOL:AGTR2"
      map_id "M12_183"
      name "AGTR2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa79"
      uniprot "UNIPROT:P50052"
    ]
    graphics [
      x 311.47607022657485
      y 1310.2151007130335
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_183"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A80128"
      hgnc "NA"
      map_id "M12_144"
      name "angiotensin_space_1_minus_9"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa22"
      uniprot "NA"
    ]
    graphics [
      x 559.7628456162805
      y 496.13443982686226
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_144"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "PUBMED:10969042"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_46"
      name "PMID:10969042"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re17"
      uniprot "NA"
    ]
    graphics [
      x 818.8536650413897
      y 969.5752808432774
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "PUBMED:15283675"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_48"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re18"
      uniprot "NA"
    ]
    graphics [
      x 428.8536650413897
      y 752.5572932182373
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:hgnc:2707;urn:miriam:uniprot:P12821;urn:miriam:uniprot:P12821;urn:miriam:ec-code:3.4.15.1;urn:miriam:ncbigene:1636;urn:miriam:ncbigene:1636;urn:miriam:hgnc.symbol:ACE;urn:miriam:refseq:NM_000789;urn:miriam:hgnc.symbol:ACE;urn:miriam:ec-code:3.2.1.-;urn:miriam:ensembl:ENSG00000159640"
      hgnc "HGNC_SYMBOL:ACE"
      map_id "M12_94"
      name "ACE"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa100"
      uniprot "UNIPROT:P12821"
    ]
    graphics [
      x 1027.9673850663708
      y 1502.3785906698167
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000196549;urn:miriam:ncbigene:4311;urn:miriam:ncbigene:4311;urn:miriam:ec-code:3.4.24.11;urn:miriam:hgnc.symbol:MME;urn:miriam:hgnc.symbol:MME;urn:miriam:refseq:NM_000902;urn:miriam:uniprot:P08473;urn:miriam:uniprot:P08473;urn:miriam:hgnc:7154"
      hgnc "HGNC_SYMBOL:MME"
      map_id "M12_160"
      name "MME"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa39"
      uniprot "UNIPROT:P08473"
    ]
    graphics [
      x 363.30415619879955
      y 940.2539445990507
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_160"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "PUBMED:28174624"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_60"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re192"
      uniprot "NA"
    ]
    graphics [
      x 1232.112491134918
      y 849.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      annotation "PUBMED:22490446"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_8"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re114"
      uniprot "NA"
    ]
    graphics [
      x 341.47607022657485
      y 1111.00087570817
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      annotation "PUBMED:22490446"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_14"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re123"
      uniprot "NA"
    ]
    graphics [
      x 638.7695287713128
      y 556.0033865024698
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      annotation "PUBMED:15283675"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_74"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re30"
      uniprot "NA"
    ]
    graphics [
      x 847.9673850663706
      y 1753.1690082783348
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A2718"
      hgnc "NA"
      map_id "M12_143"
      name "angiotensin_space_I"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa21"
      uniprot "NA"
    ]
    graphics [
      x 521.4760702265747
      y 1128.9183785790879
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_143"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      annotation "PUBMED:6172448"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_17"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re129"
      uniprot "NA"
    ]
    graphics [
      x 1196.090149807047
      y 547.3292686556404
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "PUBMED:10585461"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_73"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re28"
      uniprot "NA"
    ]
    graphics [
      x 281.47607022657485
      y 1047.6051887061021
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "PUBMED:22490446"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_12"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re119"
      uniprot "NA"
    ]
    graphics [
      x 1154.2901456472646
      y 598.5688032356218
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "PUBMED:6555043"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_19"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re132"
      uniprot "NA"
    ]
    graphics [
      x 851.4760702265747
      y 1172.359520848128
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      annotation "PUBMED:24041943"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_20"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re133"
      uniprot "NA"
    ]
    graphics [
      x 1101.6104592494069
      y 1816.8414134939999
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "PUBMED:10969042"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_40"
      name "PMID:190881"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re16"
      uniprot "NA"
    ]
    graphics [
      x 341.47607022657485
      y 1307.7476127340383
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      annotation "PUBMED:2266130"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re131"
      uniprot "NA"
    ]
    graphics [
      x 802.5144998161113
      y 1917.6236763137715
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      annotation "PUBMED:1310484"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_21"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re134"
      uniprot "NA"
    ]
    graphics [
      x 1011.6104592494069
      y 1802.3785906698167
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:hgnc:9358;urn:miriam:hgnc.symbol:PREP;urn:miriam:hgnc.symbol:PREP;urn:miriam:ncbigene:5550;urn:miriam:ncbigene:5550;urn:miriam:refseq:NM_002726;urn:miriam:ensembl:ENSG00000085377;urn:miriam:uniprot:P48147;urn:miriam:uniprot:P48147;urn:miriam:ec-code:3.4.21.26"
      hgnc "HGNC_SYMBOL:PREP"
      map_id "M12_97"
      name "PREP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa120"
      uniprot "UNIPROT:P48147"
    ]
    graphics [
      x 727.9673850663706
      y 1526.1378443135568
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:hgnc:2097;urn:miriam:ncbigene:1215;urn:miriam:ncbigene:1215;urn:miriam:hgnc.symbol:CMA1;urn:miriam:hgnc.symbol:CMA1;urn:miriam:ensembl:ENSG00000092009;urn:miriam:refseq:NM_001836;urn:miriam:uniprot:P23946;urn:miriam:uniprot:P23946;urn:miriam:ec-code:3.4.21.39"
      hgnc "HGNC_SYMBOL:CMA1"
      map_id "M12_99"
      name "CMA1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa122"
      uniprot "UNIPROT:P23946"
    ]
    graphics [
      x 985.9109426104355
      y 2275.3779017504894
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A48432"
      hgnc "NA"
      map_id "M12_145"
      name "angiotensin_space_II"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa23"
      uniprot "NA"
    ]
    graphics [
      x 367.96738506637075
      y 1668.4542386545577
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_145"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      annotation "PUBMED:16007097;PUBMED:19375596"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_91"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re94"
      uniprot "NA"
    ]
    graphics [
      x 246.25122678247703
      y 1824.7574165568012
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      annotation "PUBMED:22180785"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_6"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re109"
      uniprot "NA"
    ]
    graphics [
      x 970.4146427238683
      y 2154.108586202008
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      annotation "PUBMED:8876246"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_79"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re61"
      uniprot "NA"
    ]
    graphics [
      x 1071.6104592494069
      y 1779.749802637834
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "PUBMED:23392115"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_57"
      name "PMID:22536270"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re19"
      uniprot "NA"
    ]
    graphics [
      x 611.4760702265747
      y 1287.9761726888562
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      annotation "PUBMED:15809376"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_10"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re116"
      uniprot "NA"
    ]
    graphics [
      x 1628.8536650413898
      y 552.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      annotation "PUBMED:25225202"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_93"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re98"
      uniprot "NA"
    ]
    graphics [
      x 771.6104592494069
      y 1861.929008352736
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      annotation "PUBMED:17138938"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_77"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re45"
      uniprot "NA"
    ]
    graphics [
      x 911.4760702265747
      y 1141.8262641726626
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      annotation "PUBMED:30934934"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_33"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re148"
      uniprot "NA"
    ]
    graphics [
      x 941.4760702265747
      y 1417.579394243707
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:pubmed:30934934"
      hgnc "NA"
      map_id "M12_115"
      name "angiotensin_space_3_minus_7"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa160"
      uniprot "NA"
    ]
    graphics [
      x 1118.8536650413898
      y 840.5543133122671
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      annotation "PUBMED:30934934"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_32"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re147"
      uniprot "NA"
    ]
    graphics [
      x 878.8536650413897
      y 677.2386096372829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      annotation "PUBMED:30934934"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re149"
      uniprot "NA"
    ]
    graphics [
      x 1361.4760702265748
      y 1286.665563048565
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A80127"
      hgnc "NA"
      map_id "M12_174"
      name "angiotensin_space_IV"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa63"
      uniprot "NA"
    ]
    graphics [
      x 877.9673850663706
      y 1734.0398688947985
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_174"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      annotation "PUBMED:8876246"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_80"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re62"
      uniprot "NA"
    ]
    graphics [
      x 970.4146427238683
      y 2091.2259740058394
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A89666"
      hgnc "NA"
      map_id "M12_173"
      name "angiotensin_space_III"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa62"
      uniprot "NA"
    ]
    graphics [
      x 882.2885602204384
      y 2428.0723472521104
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_173"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:ncbigene:290;urn:miriam:ncbigene:290;urn:miriam:hgnc:500;urn:miriam:hgnc.symbol:ANPEP;urn:miriam:refseq:NM_001150;urn:miriam:uniprot:P15144;urn:miriam:uniprot:P15144;urn:miriam:hgnc.symbol:ANPEP;urn:miriam:ensembl:ENSG00000166825;urn:miriam:ec-code:3.4.11.2"
      hgnc "HGNC_SYMBOL:ANPEP"
      map_id "M12_176"
      name "ANPEP_space_"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa65"
      uniprot "UNIPROT:P15144"
    ]
    graphics [
      x 1121.4760702265748
      y 1188.9296873026296
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_176"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:pubchem.compound:91691124;urn:miriam:kegg.compound:C20970"
      hgnc "NA"
      map_id "M12_169"
      name "angiotensin_space_A"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa51"
      uniprot "NA"
    ]
    graphics [
      x 817.9673850663706
      y 1737.6236763137715
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_169"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "PUBMED:23446738"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_78"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re46"
      uniprot "NA"
    ]
    graphics [
      x 311.47607022657485
      y 1249.6519192532382
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:ACE2"
      map_id "M12_152"
      name "ACE2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa30"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 551.4760702265747
      y 1365.442016775522
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_152"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:pubchem.compound:44192273"
      hgnc "NA"
      map_id "M12_167"
      name "alamandine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa49"
      uniprot "NA"
    ]
    graphics [
      x 1292.112491134918
      y 822.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_167"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      annotation "PUBMED:23446738"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_76"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re43"
      uniprot "NA"
    ]
    graphics [
      x 788.8536650413897
      y 805.7583197686541
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      annotation "PUBMED:26562171;PUBMED:28944831;PUBMED:19864379;PUBMED:32432918"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_38"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re153"
      uniprot "NA"
    ]
    graphics [
      x 548.8536650413897
      y 846.8180695960251
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      annotation "PUBMED:32432657"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_56"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re189"
      uniprot "NA"
    ]
    graphics [
      x 307.96738506637075
      y 1668.3815285861933
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      annotation "PUBMED:32408336"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re168"
      uniprot "NA"
    ]
    graphics [
      x 1538.8536650413898
      y 582.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      annotation "PUBMED:19034303;PUBMED:18403595"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_67"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re200"
      uniprot "NA"
    ]
    graphics [
      x 832.5144998161113
      y 1890.8836428135103
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      annotation "PUBMED:27965422;PUBMED:28174624"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_58"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re190"
      uniprot "NA"
    ]
    graphics [
      x 382.5144998161113
      y 2042.5572932182372
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      annotation "PUBMED:24227843;PUBMED:28512108;PUBMED:32333398"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_83"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re69"
      uniprot "NA"
    ]
    graphics [
      x 1478.8536650413898
      y 822.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      annotation "PUBMED:32275855"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_4"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re102"
      uniprot "NA"
    ]
    graphics [
      x 1031.4760702265748
      y 1382.3785906698167
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      annotation "PUBMED:26010093;PUBMED:26171856"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_90"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re93"
      uniprot "NA"
    ]
    graphics [
      x 1241.4760702265748
      y 1479.0802035377026
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:mesh:D008297"
      hgnc "NA"
      map_id "M12_163"
      name "sex,_space_male"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa44"
      uniprot "NA"
    ]
    graphics [
      x 1931.4760702265748
      y 1178.2078491722318
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_163"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:ACE2"
      map_id "M12_187"
      name "ACE2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa98"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 277.96738506637075
      y 1704.4474784456133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_187"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      annotation "PUBMED:28174624"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re186"
      uniprot "NA"
    ]
    graphics [
      x 1000.5102516647396
      y 1288.6039843924614
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:hgnc:2707;urn:miriam:uniprot:P12821;urn:miriam:uniprot:P12821;urn:miriam:ec-code:3.4.15.1;urn:miriam:ncbigene:1636;urn:miriam:ncbigene:1636;urn:miriam:hgnc.symbol:ACE;urn:miriam:refseq:NM_000789;urn:miriam:hgnc.symbol:ACE;urn:miriam:ec-code:3.2.1.-;urn:miriam:ensembl:ENSG00000159640"
      hgnc "HGNC_SYMBOL:ACE"
      map_id "M12_150"
      name "ACE"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa28"
      uniprot "UNIPROT:P12821"
    ]
    graphics [
      x 191.47607022657485
      y 1552.7645011842237
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_150"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0007568"
      hgnc "NA"
      map_id "M12_170"
      name "aging"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa54"
      uniprot "NA"
    ]
    graphics [
      x 131.47607022657485
      y 1279.5702899713663
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_170"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      annotation "PUBMED:31165585"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_61"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re193"
      uniprot "NA"
    ]
    graphics [
      x 581.4760702265747
      y 1332.451730092263
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      annotation "PUBMED:18403595"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_68"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re201"
      uniprot "NA"
    ]
    graphics [
      x 247.96738506637075
      y 1770.286023624849
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:mesh:D006973"
      hgnc "NA"
      map_id "M12_137"
      name "hypertension"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa202"
      uniprot "NA"
    ]
    graphics [
      x 491.47607022657473
      y 1305.6624883981624
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_137"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A27584"
      hgnc "NA"
      map_id "M12_131"
      name "aldosterone"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa195"
      uniprot "NA"
    ]
    graphics [
      x 458.8536650413897
      y 752.5572932182373
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_131"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      annotation "PUBMED:1338730"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_66"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re198"
      uniprot "NA"
    ]
    graphics [
      x 878.8536650413897
      y 1104.3784329311218
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      annotation "PUBMED:31165585"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_62"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re194"
      uniprot "NA"
    ]
    graphics [
      x 281.47607022657485
      y 1415.8315448445992
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_132"
      name "MAS1"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa196"
      uniprot "NA"
    ]
    graphics [
      x 701.4760702265747
      y 1413.3762122382575
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:refseq:NM_002377;urn:miriam:ensembl:ENSG00000130368;urn:miriam:hgnc:6899;urn:miriam:ncbigene:4142;urn:miriam:ncbigene:4142;urn:miriam:hgnc.symbol:MAS1;urn:miriam:uniprot:P04201;urn:miriam:uniprot:P04201;urn:miriam:hgnc.symbol:MAS1"
      hgnc "HGNC_SYMBOL:MAS1"
      map_id "M12_182"
      name "MAS1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa77"
      uniprot "UNIPROT:P04201"
    ]
    graphics [
      x 1161.6104592494069
      y 1792.7078987280793
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_182"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_140"
      name "aldosterone"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa205"
      uniprot "NA"
    ]
    graphics [
      x 971.4760702265747
      y 1228.6039843924614
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_140"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:hgnc:336;urn:miriam:refseq:NM_000685;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:uniprot:P30556;urn:miriam:uniprot:P30556;urn:miriam:ensembl:ENSG00000144891;urn:miriam:ncbigene:185;urn:miriam:ncbigene:185"
      hgnc "HGNC_SYMBOL:AGTR1"
      map_id "M12_139"
      name "AGTR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa204"
      uniprot "UNIPROT:P30556"
    ]
    graphics [
      x 1661.4760702265748
      y 1457.5725709100786
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_139"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "M12_159"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa38"
      uniprot "UNIPROT:P0DTC2;UNIPROT:P59594"
    ]
    graphics [
      x 607.9673850663706
      y 1682.6183265894208
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_159"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:pubmed:32275855;urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2;urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:ACE2;HGNC_SYMBOL:S"
      map_id "M12_2"
      name "ACE2_minus_Spike_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa5"
      uniprot "UNIPROT:Q9BYF1;UNIPROT:P0DTC2;UNIPROT:P59594"
    ]
    graphics [
      x 1087.9673850663708
      y 1702.6118367399172
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      annotation "PUBMED:32142651"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_35"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re150"
      uniprot "NA"
    ]
    graphics [
      x 1252.5144998161113
      y 1842.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      annotation "PUBMED:32275855"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_37"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re152"
      uniprot "NA"
    ]
    graphics [
      x 698.8536650413897
      y 828.8329395949797
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:mesh:D014779"
      hgnc "NA"
      map_id "M12_119"
      name "viral_space_replication_space_cycle"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa166"
      uniprot "NA"
    ]
    graphics [
      x 1220.5763054801535
      y 342.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A135632"
      hgnc "NA"
      map_id "M12_164"
      name "Camostat_space_mesilate"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa45"
      uniprot "NA"
    ]
    graphics [
      x 982.5144998161113
      y 2031.9577197196427
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_164"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:hgnc:11876;urn:miriam:hgnc.symbol:TMPRSS2;urn:miriam:uniprot:O15393;urn:miriam:uniprot:O15393;urn:miriam:hgnc.symbol:TMPRSS2;urn:miriam:ncbigene:7113;urn:miriam:ncbigene:7113;urn:miriam:ec-code:3.4.21.-;urn:miriam:ensembl:ENSG00000184012;urn:miriam:refseq:NM_001135099"
      hgnc "HGNC_SYMBOL:TMPRSS2"
      map_id "M12_161"
      name "TMPRSS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa40"
      uniprot "UNIPROT:O15393"
    ]
    graphics [
      x 1057.9673850663708
      y 1597.1131125514216
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_161"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      annotation "PUBMED:10485450"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_7"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re112"
      uniprot "NA"
    ]
    graphics [
      x 1821.6104592494069
      y 1812.4206517193227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:hgnc:11876;urn:miriam:hgnc.symbol:TMPRSS2;urn:miriam:uniprot:O15393;urn:miriam:ncbigene:7113;urn:miriam:ensembl:ENSG00000184012;urn:miriam:refseq:NM_001135099"
      hgnc "HGNC_SYMBOL:TMPRSS2"
      map_id "M12_100"
      name "TMPRSS2"
      node_subtype "GENE"
      node_type "species"
      org_id "sa130"
      uniprot "UNIPROT:O15393"
    ]
    graphics [
      x 1720.4146427238684
      y 2196.313882011814
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A50113"
      hgnc "NA"
      map_id "M12_101"
      name "androgen"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa132"
      uniprot "NA"
    ]
    graphics [
      x 2242.5144998161113
      y 2077.82400200343
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000151694;urn:miriam:ncbigene:6868;urn:miriam:ncbigene:6868;urn:miriam:refseq:NM_001382777;urn:miriam:ec-code:3.4.24.86;urn:miriam:hgnc:195;urn:miriam:uniprot:P78536;urn:miriam:uniprot:P78536;urn:miriam:hgnc.symbol:ADAM17;urn:miriam:hgnc.symbol:ADAM17"
      hgnc "HGNC_SYMBOL:ADAM17"
      map_id "M12_162"
      name "ADAM17"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa43"
      uniprot "UNIPROT:P78536"
    ]
    graphics [
      x 2348.85366504139
      y 901.3530460448194
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_162"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:hgnc:336;urn:miriam:refseq:NM_000685;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:uniprot:P30556;urn:miriam:uniprot:P30556;urn:miriam:ensembl:ENSG00000144891;urn:miriam:ncbigene:185;urn:miriam:ncbigene:185"
      hgnc "HGNC_SYMBOL:AGTR1"
      map_id "M12_120"
      name "AGTR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa167"
      uniprot "UNIPROT:P30556"
    ]
    graphics [
      x 851.4760702265747
      y 1353.5824269041595
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:ACE2"
      map_id "M12_181"
      name "ACE2,_space_soluble"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa73"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 608.8536650413897
      y 969.8351640363044
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_181"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      annotation "PUBMED:28512108"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_89"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re91"
      uniprot "NA"
    ]
    graphics [
      x 758.8536650413897
      y 818.15125963201
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A48432"
      hgnc "NA"
      map_id "M12_185"
      name "angiotensin_space_II"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa95"
      uniprot "NA"
    ]
    graphics [
      x 1297.7683686211287
      y 672.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_185"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A55438"
      hgnc "NA"
      map_id "M12_186"
      name "angiotensin_space_1_minus_7"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa96"
      uniprot "NA"
    ]
    graphics [
      x 1691.4760702265748
      y 1239.571063044361
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_186"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      annotation "PUBMED:32048163"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_5"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re103"
      uniprot "NA"
    ]
    graphics [
      x 848.8536650413897
      y 796.0055613274181
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_96"
      name "s134"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa113"
      uniprot "NA"
    ]
    graphics [
      x 1121.0597571849028
      y 370.432509533711
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:taxonomy:2697049;urn:miriam:mesh:C000657245"
      hgnc "NA"
      map_id "M12_118"
      name "SARS_minus_CoV_minus_2_space_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa165"
      uniprot "NA"
    ]
    graphics [
      x 941.4760702265747
      y 1255.5840093932588
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000130234;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:ACE2"
      map_id "M12_121"
      name "ACE2"
      node_subtype "GENE"
      node_type "species"
      org_id "sa168"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 821.4760702265747
      y 1226.9149485161324
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:mesh:D003924"
      hgnc "NA"
      map_id "M12_138"
      name "_space_Diabetes_space_mellitus,_space_type_space_II"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa203"
      uniprot "NA"
    ]
    graphics [
      x 1334.0431965695143
      y 2322.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_138"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_126"
      name "ACE2"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa175"
      uniprot "NA"
    ]
    graphics [
      x 1593.6762205609884
      y 462.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16469"
      hgnc "NA"
      map_id "M12_122"
      name "estradiol"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa169"
      uniprot "NA"
    ]
    graphics [
      x 1211.4760702265748
      y 1299.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17823"
      hgnc "NA"
      map_id "M12_136"
      name "Calcitriol"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa201"
      uniprot "NA"
    ]
    graphics [
      x 431.47607022657473
      y 1172.5572932182372
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:hgnc:336;urn:miriam:refseq:NM_000685;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:uniprot:P30556;urn:miriam:uniprot:P30556;urn:miriam:ensembl:ENSG00000144891;urn:miriam:ncbigene:185;urn:miriam:ncbigene:185"
      hgnc "HGNC_SYMBOL:AGTR1"
      map_id "M12_148"
      name "AGTR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa26"
      uniprot "UNIPROT:P30556"
    ]
    graphics [
      x 727.9673850663706
      y 1762.4666947456517
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_148"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:ACE2"
      map_id "M12_153"
      name "ACE2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa31"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 725.7334578546346
      y 2196.803118933805
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_153"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:pubmed:25225202;urn:miriam:hgnc:336;urn:miriam:refseq:NM_000685;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:uniprot:P30556;urn:miriam:uniprot:P30556;urn:miriam:ensembl:ENSG00000144891;urn:miriam:ncbigene:185;urn:miriam:ncbigene:185;urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:AGTR1;HGNC_SYMBOL:ACE2"
      map_id "M12_1"
      name "ACE2:AGTR1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa4"
      uniprot "UNIPROT:P30556;UNIPROT:Q9BYF1"
    ]
    graphics [
      x 581.4760702265747
      y 1302.451730092263
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:hgnc:336;urn:miriam:refseq:NM_000685;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:uniprot:P30556;urn:miriam:uniprot:P30556;urn:miriam:ensembl:ENSG00000144891;urn:miriam:ncbigene:185;urn:miriam:ncbigene:185"
      hgnc "HGNC_SYMBOL:AGTR1"
      map_id "M12_106"
      name "AGTR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa139"
      uniprot "UNIPROT:P30556"
    ]
    graphics [
      x 1180.9744346231273
      y 513.611315155501
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:pubmed:15809376;urn:miriam:hgnc:336;urn:miriam:refseq:NM_000685;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:uniprot:P30556;urn:miriam:uniprot:P30556;urn:miriam:ensembl:ENSG00000144891;urn:miriam:ncbigene:185;urn:miriam:ncbigene:185;urn:miriam:refseq:NM_002377;urn:miriam:ensembl:ENSG00000130368;urn:miriam:hgnc:6899;urn:miriam:ncbigene:4142;urn:miriam:ncbigene:4142;urn:miriam:hgnc.symbol:MAS1;urn:miriam:uniprot:P04201;urn:miriam:uniprot:P04201;urn:miriam:hgnc.symbol:MAS1"
      hgnc "HGNC_SYMBOL:AGTR1;HGNC_SYMBOL:MAS1"
      map_id "M12_3"
      name "MAS1:AGTR1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa9"
      uniprot "UNIPROT:P30556;UNIPROT:P04201"
    ]
    graphics [
      x 2168.85366504139
      y 721.6047821451132
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:hgnc:336;urn:miriam:refseq:NM_000685;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:uniprot:P30556;urn:miriam:uniprot:P30556;urn:miriam:ensembl:ENSG00000144891;urn:miriam:ncbigene:185;urn:miriam:ncbigene:185"
      hgnc "HGNC_SYMBOL:AGTR1"
      map_id "M12_107"
      name "AGTR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa140"
      uniprot "UNIPROT:P30556"
    ]
    graphics [
      x 968.8536650413897
      y 977.3372940987897
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      annotation "PUBMED:15809376"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_9"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re115"
      uniprot "NA"
    ]
    graphics [
      x 1301.4760702265748
      y 1048.1356953218692
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:refseq:NM_002377;urn:miriam:ensembl:ENSG00000130368;urn:miriam:hgnc:6899;urn:miriam:ncbigene:4142;urn:miriam:ncbigene:4142;urn:miriam:hgnc.symbol:MAS1;urn:miriam:uniprot:P04201;urn:miriam:uniprot:P04201;urn:miriam:hgnc.symbol:MAS1"
      hgnc "HGNC_SYMBOL:MAS1"
      map_id "M12_108"
      name "MAS1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa141"
      uniprot "UNIPROT:P04201"
    ]
    graphics [
      x 548.8536650413897
      y 972.8244432167741
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000137509;urn:miriam:hgnc.symbol:PRCP;urn:miriam:hgnc.symbol:PRCP;urn:miriam:ec-code:3.4.16.2;urn:miriam:uniprot:P42785;urn:miriam:uniprot:P42785;urn:miriam:hgnc:9344;urn:miriam:ncbigene:5547;urn:miriam:refseq:NM_005040;urn:miriam:ncbigene:5547"
      hgnc "HGNC_SYMBOL:PRCP"
      map_id "M12_165"
      name "PRCP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa47"
      uniprot "UNIPROT:P42785"
    ]
    graphics [
      x 1221.6104592494069
      y 1779.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_165"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000138792;urn:miriam:ncbigene:2028;urn:miriam:ncbigene:2028;urn:miriam:hgnc:3355;urn:miriam:hgnc.symbol:ENPEP;urn:miriam:hgnc.symbol:ENPEP;urn:miriam:refseq:NM_001379611;urn:miriam:ec-code:3.4.11.7;urn:miriam:uniprot:Q07075;urn:miriam:uniprot:Q07075"
      hgnc "HGNC_SYMBOL:ENPEP"
      map_id "M12_175"
      name "ENPEP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa64"
      uniprot "UNIPROT:Q07075"
    ]
    graphics [
      x 731.4760702265747
      y 1203.8373313193333
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_175"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      annotation "PUBMED:28174624"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_59"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re191"
      uniprot "NA"
    ]
    graphics [
      x 577.3909576415782
      y 591.4711392013417
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      annotation "PUBMED:22710644"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_16"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re128"
      uniprot "NA"
    ]
    graphics [
      x 333.55084190152184
      y 978.0550376764086
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000138792;urn:miriam:ncbigene:2028;urn:miriam:ncbigene:2028;urn:miriam:hgnc:3355;urn:miriam:hgnc.symbol:ENPEP;urn:miriam:hgnc.symbol:ENPEP;urn:miriam:refseq:NM_001379611;urn:miriam:ec-code:3.4.11.7;urn:miriam:uniprot:Q07075;urn:miriam:uniprot:Q07075"
      hgnc "HGNC_SYMBOL:ENPEP"
      map_id "M12_117"
      name "ENPEP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa162"
      uniprot "UNIPROT:Q07075"
    ]
    graphics [
      x 882.2678088773
      y 424.6728219866676
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:pubmed:24337978"
      hgnc "NA"
      map_id "M12_116"
      name "QGC001"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa161"
      uniprot "NA"
    ]
    graphics [
      x 221.47607022657485
      y 1289.3362933908695
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:mesh:D008297"
      hgnc "NA"
      map_id "M12_129"
      name "sex,_space_male"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa193"
      uniprot "NA"
    ]
    graphics [
      x 341.47607022657485
      y 1337.7476127340383
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_129"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:pubmed:27465904"
      hgnc "NA"
      map_id "M12_98"
      name "angiotensin_space_1_minus_12"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa121"
      uniprot "NA"
    ]
    graphics [
      x 851.4760702265747
      y 1134.3784329311218
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_188"
      name "s115"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa99"
      uniprot "NA"
    ]
    graphics [
      x 161.47607022657485
      y 1451.0281475497238
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_188"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:hgnc:2707;urn:miriam:uniprot:P12821;urn:miriam:uniprot:P12821;urn:miriam:ec-code:3.4.15.1;urn:miriam:ncbigene:1636;urn:miriam:ncbigene:1636;urn:miriam:hgnc.symbol:ACE;urn:miriam:refseq:NM_000789;urn:miriam:hgnc.symbol:ACE;urn:miriam:ec-code:3.2.1.-;urn:miriam:ensembl:ENSG00000159640"
      hgnc "HGNC_SYMBOL:ACE"
      map_id "M12_151"
      name "ACE"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa29"
      uniprot "UNIPROT:P12821"
    ]
    graphics [
      x 877.9673850663706
      y 1553.2421714207787
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_151"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      annotation "PUBMED:26562171;PUBMED:28944831"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_65"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re197"
      uniprot "NA"
    ]
    graphics [
      x 187.96738506637075
      y 1620.195585811822
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      annotation "PUBMED:2550696"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_11"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re117"
      uniprot "NA"
    ]
    graphics [
      x 1841.4760702265748
      y 1102.4350168156961
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      annotation "PUBMED:20066004;PUBMED:32343152;PUBMED:23937567;PUBMED:24803075"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_55"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re188"
      uniprot "NA"
    ]
    graphics [
      x 652.5144998161113
      y 1932.1662202339423
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:mesh:D000017"
      hgnc "NA"
      map_id "M12_128"
      name "ABO_space_blood_space_group_space_system"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa192"
      uniprot "NA"
    ]
    graphics [
      x 970.4146427238683
      y 2205.3945762566946
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:hgnc:2707;urn:miriam:uniprot:P12821;urn:miriam:uniprot:P12821;urn:miriam:ec-code:3.4.15.1;urn:miriam:ncbigene:1636;urn:miriam:ncbigene:1636;urn:miriam:hgnc.symbol:ACE;urn:miriam:refseq:NM_000789;urn:miriam:hgnc.symbol:ACE;urn:miriam:ec-code:3.2.1.-;urn:miriam:ensembl:ENSG00000159640"
      hgnc "HGNC_SYMBOL:ACE"
      map_id "M12_110"
      name "ACE"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa146"
      uniprot "UNIPROT:P12821"
    ]
    graphics [
      x 1442.112491134918
      y 822.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A43755"
      hgnc "NA"
      map_id "M12_177"
      name "Lisinopril"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa66"
      uniprot "NA"
    ]
    graphics [
      x 1271.4760702265748
      y 1032.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_177"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:hgnc:2707;urn:miriam:uniprot:P12821;urn:miriam:ncbigene:1636;urn:miriam:hgnc.symbol:ACE;urn:miriam:refseq:NM_000789;urn:miriam:ensembl:ENSG00000159640"
      hgnc "HGNC_SYMBOL:ACE"
      map_id "M12_134"
      name "ACE"
      node_subtype "GENE"
      node_type "species"
      org_id "sa199"
      uniprot "UNIPROT:P12821"
    ]
    graphics [
      x 472.5144998161113
      y 2025.366927568407
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16469"
      hgnc "NA"
      map_id "M12_135"
      name "estradiol"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa200"
      uniprot "NA"
    ]
    graphics [
      x 368.85366504138983
      y 984.7843436578598
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17823"
      hgnc "NA"
      map_id "M12_158"
      name "Calcitriol"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa37"
      uniprot "NA"
    ]
    graphics [
      x 577.9265353198558
      y 2293.243905175061
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_158"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      annotation "PUBMED:12122115"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_72"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re27"
      uniprot "NA"
    ]
    graphics [
      x 829.1714652815332
      y 2445.3376950561214
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 167
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:hgnc.symbol:REN;urn:miriam:uniprot:P00797;urn:miriam:hgnc:9958;urn:miriam:ensembl:ENSG00000143839;urn:miriam:ncbigene:5972;urn:miriam:refseq:NM_000537"
      hgnc "HGNC_SYMBOL:REN"
      map_id "M12_157"
      name "REN"
      node_subtype "GENE"
      node_type "species"
      org_id "sa36"
      uniprot "UNIPROT:P00797"
    ]
    graphics [
      x 1611.6104592494069
      y 1752.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_157"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 168
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:hgnc.symbol:REN;urn:miriam:hgnc.symbol:REN;urn:miriam:uniprot:P00797;urn:miriam:uniprot:P00797;urn:miriam:hgnc:9958;urn:miriam:ensembl:ENSG00000143839;urn:miriam:ncbigene:5972;urn:miriam:refseq:NM_000537;urn:miriam:ncbigene:5972;urn:miriam:ec-code:3.4.23.15"
      hgnc "HGNC_SYMBOL:REN"
      map_id "M12_156"
      name "REN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa35"
      uniprot "UNIPROT:P00797"
    ]
    graphics [
      x 1042.5144998161113
      y 1854.3655306986243
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_156"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 169
    zlevel -1

    cd19dm [
      annotation "PUBMED:32333398"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_82"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re68"
      uniprot "NA"
    ]
    graphics [
      x 2047.9673850663708
      y 1658.429936451232
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 170
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:hgnc.symbol:REN;urn:miriam:hgnc.symbol:REN;urn:miriam:uniprot:P00797;urn:miriam:uniprot:P00797;urn:miriam:hgnc:9958;urn:miriam:ensembl:ENSG00000143839;urn:miriam:ncbigene:5972;urn:miriam:refseq:NM_000537;urn:miriam:ncbigene:5972;urn:miriam:ec-code:3.4.23.15"
      hgnc "HGNC_SYMBOL:REN"
      map_id "M12_180"
      name "REN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa71"
      uniprot "UNIPROT:P00797"
    ]
    graphics [
      x 791.4760702265747
      y 1446.3585216247106
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_180"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 171
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:ncbigene:7064;urn:miriam:ncbigene:7064;urn:miriam:hgnc.symbol:THOP1;urn:miriam:ensembl:ENSG00000172009;urn:miriam:hgnc.symbol:THOP1;urn:miriam:uniprot:P52888;urn:miriam:uniprot:P52888;urn:miriam:hgnc:11793;urn:miriam:ec-code:3.4.24.15;urn:miriam:refseq:NM_003249"
      hgnc "HGNC_SYMBOL:THOP1"
      map_id "M12_166"
      name "THOP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa48"
      uniprot "UNIPROT:P52888"
    ]
    graphics [
      x 1481.4760702265748
      y 1449.8671897798747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_166"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 172
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:hgnc:6357;urn:miriam:ec-code:3.4.21.35;urn:miriam:refseq:NM_002257;urn:miriam:uniprot:P06870;urn:miriam:uniprot:P06870;urn:miriam:ncbigene:3816;urn:miriam:ncbigene:3816;urn:miriam:hgnc.symbol:KLK1;urn:miriam:hgnc.symbol:KLK1;urn:miriam:ensembl:ENSG00000167748"
      hgnc "HGNC_SYMBOL:KLK1"
      map_id "M12_109"
      name "KLK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa144"
      uniprot "UNIPROT:P06870"
    ]
    graphics [
      x 1568.8536650413898
      y 552.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 173
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:uniprot:P01019;urn:miriam:uniprot:P01019;urn:miriam:hgnc.symbol:AGT;urn:miriam:hgnc.symbol:AGT;urn:miriam:ensembl:ENSG00000135744;urn:miriam:hgnc:333;urn:miriam:ncbigene:183;urn:miriam:ncbigene:183;urn:miriam:refseq:NM_000029"
      hgnc "HGNC_SYMBOL:AGT"
      map_id "M12_155"
      name "AGT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa34"
      uniprot "UNIPROT:P01019"
    ]
    graphics [
      x 1178.8536650413898
      y 719.6470244694499
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_155"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 174
    zlevel -1

    cd19dm [
      annotation "PUBMED:8351287"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_44"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re167"
      uniprot "NA"
    ]
    graphics [
      x 2441.476070226575
      y 1539.7415077782575
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 175
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:uniprot:P01019;urn:miriam:hgnc.symbol:AGT;urn:miriam:ensembl:ENSG00000135744;urn:miriam:hgnc:333;urn:miriam:ncbigene:183;urn:miriam:refseq:NM_000029"
      hgnc "HGNC_SYMBOL:AGT"
      map_id "M12_125"
      name "AGT"
      node_subtype "GENE"
      node_type "species"
      org_id "sa174"
      uniprot "UNIPROT:P01019"
    ]
    graphics [
      x 1762.5144998161113
      y 2023.8597507391748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_125"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 176
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A4903"
      hgnc "NA"
      map_id "M12_102"
      name "ethynylestradiol"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa134"
      uniprot "NA"
    ]
    graphics [
      x 2602.5144998161113
      y 1919.250393733933
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 177
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:refseq:NM_001909;urn:miriam:ensembl:ENSG00000117984;urn:miriam:ec-code:3.4.23.5;urn:miriam:uniprot:P07339;urn:miriam:uniprot:P07339;urn:miriam:ncbigene:1509;urn:miriam:ncbigene:1509;urn:miriam:hgnc.symbol:CTSD;urn:miriam:hgnc.symbol:CTSD;urn:miriam:hgnc:2529"
      hgnc "HGNC_SYMBOL:CTSD"
      map_id "M12_103"
      name "CTSD"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa136"
      uniprot "UNIPROT:P07339"
    ]
    graphics [
      x 1508.8536650413898
      y 612.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 178
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:ncbigene:1511;urn:miriam:uniprot:P08311;urn:miriam:uniprot:P08311;urn:miriam:ncbigene:1511;urn:miriam:ec-code:3.4.21.20;urn:miriam:hgnc:2532;urn:miriam:hgnc.symbol:CTSG;urn:miriam:hgnc.symbol:CTSG;urn:miriam:refseq:NM_001911;urn:miriam:ensembl:ENSG00000100448"
      hgnc "HGNC_SYMBOL:CTSG"
      map_id "M12_104"
      name "CTSG"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa137"
      uniprot "UNIPROT:P08311"
    ]
    graphics [
      x 1811.4760702265748
      y 1452.7318995498533
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 179
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:pubmed:22490446"
      hgnc "NA"
      map_id "M12_105"
      name "angiotensin_space_1_minus_4"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa138"
      uniprot "NA"
    ]
    graphics [
      x 551.4760702265747
      y 1455.5551199408903
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 180
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000196549;urn:miriam:ncbigene:4311;urn:miriam:ncbigene:4311;urn:miriam:ec-code:3.4.24.11;urn:miriam:hgnc.symbol:MME;urn:miriam:hgnc.symbol:MME;urn:miriam:refseq:NM_000902;urn:miriam:uniprot:P08473;urn:miriam:uniprot:P08473;urn:miriam:hgnc:7154"
      hgnc "HGNC_SYMBOL:MME"
      map_id "M12_130"
      name "MME"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa194"
      uniprot "UNIPROT:P08473"
    ]
    graphics [
      x 1496.5256443239937
      y 522.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_130"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 181
    zlevel -1

    cd19dm [
      annotation "PUBMED:10969042"
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M12_71"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re25"
      uniprot "NA"
    ]
    graphics [
      x 998.8536650413897
      y 676.7985878765545
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 182
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A80129"
      hgnc "NA"
      map_id "M12_154"
      name "angiotensin_space_1_minus_5"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa32"
      uniprot "NA"
    ]
    graphics [
      x 1166.090149807047
      y 550.0479453485457
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_154"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 183
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A147302"
      hgnc "NA"
      map_id "M12_179"
      name "CGP42112A"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa70"
      uniprot "NA"
    ]
    graphics [
      x 908.8536650413897
      y 896.5342316377083
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_179"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 184
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:pubchem.compound:146025955"
      hgnc "NA"
      map_id "M12_114"
      name "AR234960"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa159"
      uniprot "NA"
    ]
    graphics [
      x 682.5144998161113
      y 2049.168415753376
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 185
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0070527"
      hgnc "NA"
      map_id "M12_142"
      name "platelet_space_aggregation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa209"
      uniprot "NA"
    ]
    graphics [
      x 517.9673850663706
      y 1625.261006004971
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_142"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 186
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:mesh:D011654"
      hgnc "NA"
      map_id "M12_141"
      name "pulmonary_space_edema"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa208"
      uniprot "NA"
    ]
    graphics [
      x 487.96738506637064
      y 1720.8631263328639
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_141"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 187
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A6541"
      hgnc "NA"
      map_id "M12_178"
      name "Losartan"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa69"
      uniprot "NA"
    ]
    graphics [
      x 921.6104592494069
      y 1778.944364236203
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_178"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 188
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Renin-angiotensin pathway"
      full_annotation "urn:miriam:ec-code:3.4.11.3;urn:miriam:refseq:NM_005575;urn:miriam:ensembl:ENSG00000113441;urn:miriam:hgnc:6656;urn:miriam:hgnc.symbol:LNPEP;urn:miriam:hgnc.symbol:LNPEP;urn:miriam:ncbigene:4012;urn:miriam:uniprot:Q9UIQ6;urn:miriam:uniprot:Q9UIQ6;urn:miriam:ncbigene:4012"
      hgnc "HGNC_SYMBOL:LNPEP"
      map_id "M12_111"
      name "LNPEP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa152"
      uniprot "UNIPROT:Q9UIQ6"
    ]
    graphics [
      x 1987.9673850663708
      y 1667.606843746084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M12_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 189
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_184"
      target_id "M12_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 1
    target 3
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_184"
      target_id "M12_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 90
    target 2
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M12_167"
      target_id "M12_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 2
    target 5
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_88"
      target_id "M12_168"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 4
    target 3
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M12_146"
      target_id "M12_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 3
    target 5
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_27"
      target_id "M12_168"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 75
    target 4
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_57"
      target_id "M12_146"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 58
    target 4
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_14"
      target_id "M12_146"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 59
    target 4
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_74"
      target_id "M12_146"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 65
    target 4
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_20"
      target_id "M12_146"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 53
    target 4
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_48"
      target_id "M12_146"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 68
    target 4
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_21"
      target_id "M12_146"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 4
    target 91
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_146"
      target_id "M12_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 4
    target 44
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M12_146"
      target_id "M12_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 4
    target 181
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_146"
      target_id "M12_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 4
    target 81
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_146"
      target_id "M12_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 4
    target 22
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "M12_146"
      target_id "M12_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 4
    target 36
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M12_146"
      target_id "M12_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 5
    target 6
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_168"
      target_id "M12_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 5
    target 7
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_168"
      target_id "M12_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 6
    target 41
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_31"
      target_id "M12_171"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 7
    target 8
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_84"
      target_id "M12_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 9
    target 8
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_85"
      target_id "M12_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 10
    target 8
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_49"
      target_id "M12_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 11
    target 8
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_29"
      target_id "M12_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 33
    target 9
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_149"
      target_id "M12_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 21
    target 10
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_95"
      target_id "M12_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 12
    target 11
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_112"
      target_id "M12_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 13
    target 12
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_36"
      target_id "M12_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 14
    target 12
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_13"
      target_id "M12_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 12
    target 15
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_112"
      target_id "M12_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 12
    target 16
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_112"
      target_id "M12_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 12
    target 17
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_112"
      target_id "M12_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 188
    target 13
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_111"
      target_id "M12_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 80
    target 13
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M12_115"
      target_id "M12_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 188
    target 14
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_111"
      target_id "M12_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 83
    target 14
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M12_174"
      target_id "M12_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 15
    target 41
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_30"
      target_id "M12_171"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 16
    target 19
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_64"
      target_id "M12_133"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 17
    target 18
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_53"
      target_id "M12_127"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 20
    target 19
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_63"
      target_id "M12_133"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 21
    target 20
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_95"
      target_id "M12_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 22
    target 21
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_23"
      target_id "M12_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 23
    target 21
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_22"
      target_id "M12_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 24
    target 21
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_92"
      target_id "M12_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 21
    target 25
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_95"
      target_id "M12_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 21
    target 26
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_95"
      target_id "M12_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 21
    target 27
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_95"
      target_id "M12_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 21
    target 28
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_95"
      target_id "M12_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 21
    target 29
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_95"
      target_id "M12_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 239
    source 21
    target 30
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_95"
      target_id "M12_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 240
    source 139
    target 22
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_148"
      target_id "M12_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 241
    source 139
    target 23
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_148"
      target_id "M12_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 242
    source 187
    target 23
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "M12_178"
      target_id "M12_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 243
    source 139
    target 24
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_148"
      target_id "M12_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 244
    source 71
    target 24
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M12_145"
      target_id "M12_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 245
    source 83
    target 24
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M12_174"
      target_id "M12_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 246
    source 87
    target 24
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M12_169"
      target_id "M12_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 247
    source 25
    target 186
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_69"
      target_id "M12_141"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 248
    source 26
    target 185
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_70"
      target_id "M12_142"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 249
    source 27
    target 40
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_52"
      target_id "M12_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 250
    source 28
    target 49
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_50"
      target_id "M12_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 251
    source 29
    target 41
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_51"
      target_id "M12_171"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 252
    source 30
    target 31
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_47"
      target_id "M12_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 253
    source 32
    target 31
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_42"
      target_id "M12_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 254
    source 33
    target 32
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_149"
      target_id "M12_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 255
    source 34
    target 33
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_75"
      target_id "M12_149"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 256
    source 35
    target 33
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_15"
      target_id "M12_149"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 257
    source 36
    target 33
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_26"
      target_id "M12_149"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 258
    source 33
    target 37
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_149"
      target_id "M12_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 259
    source 33
    target 38
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_149"
      target_id "M12_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 260
    source 33
    target 39
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_149"
      target_id "M12_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 261
    source 112
    target 34
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_182"
      target_id "M12_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 262
    source 182
    target 34
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M12_154"
      target_id "M12_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 263
    source 112
    target 35
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_182"
      target_id "M12_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 264
    source 184
    target 35
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M12_114"
      target_id "M12_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 265
    source 112
    target 36
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_182"
      target_id "M12_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 266
    source 37
    target 49
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_39"
      target_id "M12_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 267
    source 38
    target 41
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_28"
      target_id "M12_171"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 268
    source 39
    target 40
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_41"
      target_id "M12_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 269
    source 42
    target 41
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_86"
      target_id "M12_171"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 270
    source 43
    target 42
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_147"
      target_id "M12_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 271
    source 44
    target 43
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_24"
      target_id "M12_147"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 272
    source 45
    target 43
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_81"
      target_id "M12_147"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 273
    source 46
    target 43
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_87"
      target_id "M12_147"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 274
    source 47
    target 43
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_25"
      target_id "M12_147"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 275
    source 43
    target 48
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_147"
      target_id "M12_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 276
    source 50
    target 44
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_183"
      target_id "M12_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 277
    source 50
    target 45
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_183"
      target_id "M12_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 278
    source 183
    target 45
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M12_179"
      target_id "M12_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 50
    target 46
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_183"
      target_id "M12_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 71
    target 46
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M12_145"
      target_id "M12_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 87
    target 46
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M12_169"
      target_id "M12_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 50
    target 47
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_183"
      target_id "M12_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 51
    target 47
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M12_144"
      target_id "M12_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 48
    target 49
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_43"
      target_id "M12_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 52
    target 51
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_46"
      target_id "M12_144"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 51
    target 53
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_144"
      target_id "M12_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 60
    target 52
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_143"
      target_id "M12_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 89
    target 52
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "M12_152"
      target_id "M12_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 54
    target 53
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "M12_94"
      target_id "M12_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 55
    target 53
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "M12_160"
      target_id "M12_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 105
    target 54
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_61"
      target_id "M12_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 102
    target 54
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_54"
      target_id "M12_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 106
    target 54
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_68"
      target_id "M12_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 54
    target 181
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "M12_94"
      target_id "M12_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 55
    target 56
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_160"
      target_id "M12_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 55
    target 57
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "M12_160"
      target_id "M12_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 55
    target 58
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "M12_160"
      target_id "M12_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 55
    target 59
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "M12_160"
      target_id "M12_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 100
    target 56
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M12_163"
      target_id "M12_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 56
    target 180
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_60"
      target_id "M12_130"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 154
    target 57
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_98"
      target_id "M12_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 57
    target 179
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_8"
      target_id "M12_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 154
    target 58
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_98"
      target_id "M12_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 304
    source 60
    target 59
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_143"
      target_id "M12_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 305
    source 61
    target 60
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_17"
      target_id "M12_143"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 306
    source 62
    target 60
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_73"
      target_id "M12_143"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 307
    source 63
    target 60
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_12"
      target_id "M12_143"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 308
    source 60
    target 64
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_143"
      target_id "M12_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 309
    source 60
    target 65
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_143"
      target_id "M12_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 310
    source 60
    target 66
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_143"
      target_id "M12_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 311
    source 60
    target 67
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_143"
      target_id "M12_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 312
    source 60
    target 68
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_143"
      target_id "M12_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 313
    source 173
    target 61
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_155"
      target_id "M12_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 314
    source 177
    target 61
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "M12_103"
      target_id "M12_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 315
    source 178
    target 61
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "M12_104"
      target_id "M12_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 316
    source 173
    target 62
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_155"
      target_id "M12_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 317
    source 170
    target 62
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "M12_180"
      target_id "M12_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 318
    source 154
    target 63
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_98"
      target_id "M12_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 319
    source 156
    target 63
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "M12_151"
      target_id "M12_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 320
    source 172
    target 64
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "M12_109"
      target_id "M12_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 321
    source 64
    target 71
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_19"
      target_id "M12_145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 322
    source 171
    target 65
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "M12_166"
      target_id "M12_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 323
    source 156
    target 66
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "M12_151"
      target_id "M12_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 324
    source 66
    target 71
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_40"
      target_id "M12_145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 325
    source 70
    target 67
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "M12_99"
      target_id "M12_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 326
    source 67
    target 71
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_18"
      target_id "M12_145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 327
    source 69
    target 68
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "M12_97"
      target_id "M12_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 328
    source 70
    target 73
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "M12_99"
      target_id "M12_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 329
    source 72
    target 71
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_91"
      target_id "M12_145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 330
    source 73
    target 71
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_6"
      target_id "M12_145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 331
    source 71
    target 74
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_145"
      target_id "M12_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 332
    source 71
    target 75
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_145"
      target_id "M12_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 333
    source 71
    target 76
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M12_145"
      target_id "M12_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 334
    source 71
    target 77
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "M12_145"
      target_id "M12_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 335
    source 71
    target 78
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_145"
      target_id "M12_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 336
    source 71
    target 79
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_145"
      target_id "M12_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 337
    source 155
    target 72
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_188"
      target_id "M12_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 338
    source 115
    target 72
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M12_159"
      target_id "M12_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 339
    source 154
    target 73
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_98"
      target_id "M12_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 340
    source 148
    target 74
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "M12_175"
      target_id "M12_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 341
    source 74
    target 85
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_79"
      target_id "M12_173"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 342
    source 89
    target 75
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "M12_152"
      target_id "M12_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 343
    source 147
    target 75
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "M12_165"
      target_id "M12_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 344
    source 142
    target 76
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_106"
      target_id "M12_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 345
    source 143
    target 76
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "M12_3"
      target_id "M12_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 346
    source 76
    target 144
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_10"
      target_id "M12_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 347
    source 139
    target 77
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_148"
      target_id "M12_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 348
    source 140
    target 77
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_153"
      target_id "M12_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 349
    source 77
    target 141
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_93"
      target_id "M12_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 350
    source 78
    target 87
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_77"
      target_id "M12_169"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 351
    source 79
    target 80
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_33"
      target_id "M12_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 352
    source 81
    target 80
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_32"
      target_id "M12_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 353
    source 82
    target 80
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_34"
      target_id "M12_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 354
    source 83
    target 82
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_174"
      target_id "M12_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 355
    source 84
    target 83
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_80"
      target_id "M12_174"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 356
    source 85
    target 84
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_173"
      target_id "M12_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 357
    source 86
    target 84
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "M12_176"
      target_id "M12_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 358
    source 87
    target 88
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_169"
      target_id "M12_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 359
    source 89
    target 88
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "M12_152"
      target_id "M12_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 360
    source 88
    target 90
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_78"
      target_id "M12_167"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 361
    source 92
    target 89
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_38"
      target_id "M12_152"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 362
    source 93
    target 89
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_56"
      target_id "M12_152"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 363
    source 94
    target 89
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_45"
      target_id "M12_152"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 364
    source 95
    target 89
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_67"
      target_id "M12_152"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 365
    source 89
    target 96
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_152"
      target_id "M12_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 366
    source 89
    target 97
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_152"
      target_id "M12_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 367
    source 89
    target 98
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_152"
      target_id "M12_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 368
    source 89
    target 99
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_152"
      target_id "M12_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 369
    source 91
    target 90
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_76"
      target_id "M12_167"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 370
    source 134
    target 92
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_121"
      target_id "M12_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 371
    source 137
    target 92
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "MODULATION"
      source_id "M12_122"
      target_id "M12_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 372
    source 115
    target 92
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "M12_159"
      target_id "M12_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 373
    source 138
    target 92
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M12_136"
      target_id "M12_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 374
    source 134
    target 93
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_121"
      target_id "M12_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 375
    source 104
    target 93
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M12_170"
      target_id "M12_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 376
    source 136
    target 94
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_126"
      target_id "M12_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 377
    source 133
    target 94
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "M12_118"
      target_id "M12_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 378
    source 134
    target 95
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_121"
      target_id "M12_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 379
    source 107
    target 95
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "M12_137"
      target_id "M12_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 380
    source 135
    target 95
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "M12_138"
      target_id "M12_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 381
    source 104
    target 96
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M12_170"
      target_id "M12_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 382
    source 107
    target 96
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M12_137"
      target_id "M12_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 383
    source 96
    target 101
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_58"
      target_id "M12_187"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 384
    source 125
    target 97
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CATALYSIS"
      source_id "M12_162"
      target_id "M12_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 385
    source 126
    target 97
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M12_120"
      target_id "M12_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 386
    source 97
    target 127
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_83"
      target_id "M12_181"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 387
    source 115
    target 98
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_159"
      target_id "M12_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 388
    source 98
    target 116
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_4"
      target_id "M12_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 389
    source 100
    target 99
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M12_163"
      target_id "M12_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 390
    source 99
    target 101
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_90"
      target_id "M12_187"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 391
    source 100
    target 102
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "M12_163"
      target_id "M12_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 392
    source 103
    target 102
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_150"
      target_id "M12_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 393
    source 104
    target 102
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "M12_170"
      target_id "M12_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 394
    source 103
    target 105
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_150"
      target_id "M12_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 395
    source 103
    target 106
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_150"
      target_id "M12_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 396
    source 108
    target 105
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M12_131"
      target_id "M12_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 397
    source 107
    target 106
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M12_137"
      target_id "M12_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 398
    source 109
    target 108
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_66"
      target_id "M12_131"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 399
    source 108
    target 110
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "M12_131"
      target_id "M12_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 400
    source 113
    target 109
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_140"
      target_id "M12_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 401
    source 114
    target 109
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M12_139"
      target_id "M12_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 402
    source 111
    target 110
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_132"
      target_id "M12_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 403
    source 110
    target 112
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_62"
      target_id "M12_182"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 404
    source 115
    target 117
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_159"
      target_id "M12_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 405
    source 117
    target 116
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_35"
      target_id "M12_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 406
    source 116
    target 118
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_2"
      target_id "M12_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 407
    source 120
    target 117
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "M12_164"
      target_id "M12_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 408
    source 121
    target 117
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M12_161"
      target_id "M12_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 409
    source 118
    target 119
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_37"
      target_id "M12_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 410
    source 122
    target 121
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_7"
      target_id "M12_161"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 411
    source 123
    target 122
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_100"
      target_id "M12_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 412
    source 124
    target 122
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M12_101"
      target_id "M12_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 413
    source 127
    target 128
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M12_181"
      target_id "M12_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 414
    source 129
    target 128
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_185"
      target_id "M12_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 415
    source 128
    target 130
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_89"
      target_id "M12_186"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 416
    source 131
    target 129
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_5"
      target_id "M12_185"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 417
    source 132
    target 131
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_96"
      target_id "M12_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 418
    source 133
    target 131
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M12_118"
      target_id "M12_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 419
    source 145
    target 143
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_9"
      target_id "M12_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 420
    source 144
    target 145
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_107"
      target_id "M12_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 421
    source 146
    target 145
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_108"
      target_id "M12_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 422
    source 149
    target 148
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_59"
      target_id "M12_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 423
    source 150
    target 148
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_16"
      target_id "M12_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 424
    source 151
    target 149
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_117"
      target_id "M12_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 425
    source 153
    target 149
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "M12_129"
      target_id "M12_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 426
    source 151
    target 150
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_117"
      target_id "M12_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 427
    source 152
    target 150
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "M12_116"
      target_id "M12_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 428
    source 157
    target 156
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_65"
      target_id "M12_151"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 429
    source 156
    target 158
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_151"
      target_id "M12_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 430
    source 156
    target 159
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_151"
      target_id "M12_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 431
    source 163
    target 157
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_134"
      target_id "M12_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 432
    source 164
    target 157
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M12_135"
      target_id "M12_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 433
    source 165
    target 157
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "M12_158"
      target_id "M12_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 434
    source 162
    target 158
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "M12_177"
      target_id "M12_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 435
    source 158
    target 161
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_11"
      target_id "M12_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 436
    source 160
    target 159
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "MODULATION"
      source_id "M12_128"
      target_id "M12_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 437
    source 159
    target 161
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_55"
      target_id "M12_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 438
    source 165
    target 166
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "INHIBITION"
      source_id "M12_158"
      target_id "M12_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 439
    source 167
    target 166
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_157"
      target_id "M12_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 440
    source 166
    target 168
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_72"
      target_id "M12_156"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 441
    source 168
    target 169
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_156"
      target_id "M12_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 442
    source 169
    target 170
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_82"
      target_id "M12_180"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 443
    source 174
    target 173
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_44"
      target_id "M12_155"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 444
    source 175
    target 174
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "CONSPUMPTION"
      source_id "M12_125"
      target_id "M12_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 445
    source 176
    target 174
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M12_102"
      target_id "M12_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 446
    source 181
    target 182
    cd19dm [
      diagram "C19DMap:Renin-angiotensin pathway"
      edge_type "PRODUCTION"
      source_id "M12_71"
      target_id "M12_154"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
