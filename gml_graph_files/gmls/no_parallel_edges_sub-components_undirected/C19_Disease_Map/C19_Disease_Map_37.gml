# generated with VANTED V2.8.2 at Fri Mar 04 10:04:38 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:hgnc.symbol:OAS2;urn:miriam:hgnc.symbol:OAS2;urn:miriam:uniprot:P29728;urn:miriam:uniprot:P29728;urn:miriam:ncbigene:4939;urn:miriam:ncbigene:4939;urn:miriam:hgnc:8087;urn:miriam:ensembl:ENSG00000111335;urn:miriam:refseq:NM_001032731;urn:miriam:ec-code:2.7.7.84;urn:miriam:hgnc.symbol:OAS3;urn:miriam:hgnc.symbol:OAS3;urn:miriam:uniprot:Q9Y6K5;urn:miriam:uniprot:Q9Y6K5;urn:miriam:ensembl:ENSG00000111331;urn:miriam:refseq:NM_006187;urn:miriam:ec-code:2.7.7.84;urn:miriam:hgnc:8088;urn:miriam:ncbigene:4940;urn:miriam:ncbigene:4940;urn:miriam:ensembl:ENSG00000185745;urn:miriam:hgnc:5407;urn:miriam:hgnc.symbol:IFIT1;urn:miriam:uniprot:P09914;urn:miriam:uniprot:P09914;urn:miriam:hgnc.symbol:IFIT1;urn:miriam:refseq:NM_001548;urn:miriam:ncbigene:3434;urn:miriam:ncbigene:3434;urn:miriam:refseq:NM_080657;urn:miriam:hgnc:30908;urn:miriam:ncbigene:91543;urn:miriam:ncbigene:91543;urn:miriam:hgnc.symbol:RSAD2;urn:miriam:ensembl:ENSG00000134321;urn:miriam:hgnc.symbol:RSAD2;urn:miriam:uniprot:Q8WXG1;urn:miriam:uniprot:Q8WXG1;urn:miriam:uniprot:P00973;urn:miriam:uniprot:P00973;urn:miriam:ncbigene:4938;urn:miriam:ncbigene:4938;urn:miriam:refseq:NM_001032409;urn:miriam:hgnc.symbol:OAS1;urn:miriam:hgnc.symbol:OAS1;urn:miriam:hgnc:8086;urn:miriam:ec-code:2.7.7.84;urn:miriam:ensembl:ENSG00000089127;urn:miriam:ensembl:ENSG00000126709;urn:miriam:ncbigene:2537;urn:miriam:ncbigene:2537;urn:miriam:refseq:NM_022873;urn:miriam:uniprot:P09912;urn:miriam:uniprot:P09912;urn:miriam:hgnc.symbol:IFI6;urn:miriam:hgnc.symbol:IFI6;urn:miriam:hgnc:4054;urn:miriam:uniprot:Q92985;urn:miriam:uniprot:Q92985;urn:miriam:ensembl:ENSG00000185507;urn:miriam:refseq:NM_001572;urn:miriam:hgnc.symbol:IRF7;urn:miriam:hgnc.symbol:IRF7;urn:miriam:hgnc:6122;urn:miriam:ncbigene:3665;urn:miriam:ncbigene:3665;urn:miriam:ncbigene:3659;urn:miriam:ncbigene:3659;urn:miriam:hgnc:6116;urn:miriam:hgnc.symbol:IRF1;urn:miriam:hgnc.symbol:IRF1;urn:miriam:refseq:NM_002198;urn:miriam:uniprot:P10914;urn:miriam:uniprot:P10914;urn:miriam:ensembl:ENSG00000125347;urn:miriam:hgnc:9437;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc.symbol:EIF2AK2;urn:miriam:hgnc.symbol:EIF2AK2;urn:miriam:uniprot:P19525;urn:miriam:uniprot:P19525;urn:miriam:refseq:NM_002759;urn:miriam:ncbigene:5610;urn:miriam:ensembl:ENSG00000055332;urn:miriam:ncbigene:5610"
      hgnc "HGNC_SYMBOL:OAS2;HGNC_SYMBOL:OAS3;HGNC_SYMBOL:IFIT1;HGNC_SYMBOL:RSAD2;HGNC_SYMBOL:OAS1;HGNC_SYMBOL:IFI6;HGNC_SYMBOL:IRF7;HGNC_SYMBOL:IRF1;HGNC_SYMBOL:EIF2AK2"
      map_id "M120_26"
      name "antiviral_space_proteins"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa58"
      uniprot "UNIPROT:P29728;UNIPROT:Q9Y6K5;UNIPROT:P09914;UNIPROT:Q8WXG1;UNIPROT:P00973;UNIPROT:P09912;UNIPROT:Q92985;UNIPROT:P10914;UNIPROT:P19525"
    ]
    graphics [
      x 998.8536650413897
      y 761.9148662117439
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_78"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re9"
      uniprot "NA"
    ]
    graphics [
      x 1028.8536650413898
      y 921.5374917954795
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_31"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re11"
      uniprot "NA"
    ]
    graphics [
      x 1781.4760702265748
      y 1126.8134512202714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_27"
      name "antiviral_space_proteins"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa59"
      uniprot "NA"
    ]
    graphics [
      x 1301.4760702265748
      y 1018.1356953218692
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_92"
      name "ISGs"
      node_subtype "RNA"
      node_type "species"
      org_id "sa14"
      uniprot "NA"
    ]
    graphics [
      x 521.4760702265747
      y 1346.0389472803913
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_21"
      name "80S_space_ribosome"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa44"
      uniprot "NA"
    ]
    graphics [
      x 758.8536650413897
      y 987.5868356076667
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "PUBMED:32680882"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_59"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re52"
      uniprot "NA"
    ]
    graphics [
      x 251.47607022657485
      y 1303.117430481679
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "PUBMED:32680882"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_61"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re54"
      uniprot "NA"
    ]
    graphics [
      x 1762.5144998161113
      y 2053.859750739175
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_77"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re8"
      uniprot "NA"
    ]
    graphics [
      x 1540.6727711977805
      y 792.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_25"
      name "IFNs"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa54"
      uniprot "NA"
    ]
    graphics [
      x 1796.090149807047
      y 526.8134512202714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_63"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re56"
      uniprot "NA"
    ]
    graphics [
      x 2528.85366504139
      y 900.9092311575099
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_24"
      name "IFNs"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa53"
      uniprot "NA"
    ]
    graphics [
      x 2368.459295281623
      y 751.3530460448193
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ncbiprotein:YP_009725297;urn:miriam:uniprot:P0C6X7;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M120_104"
      name "Nsp1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa70"
      uniprot "UNIPROT:P0C6X7"
    ]
    graphics [
      x 1821.6104592494069
      y 1730.1472086870194
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ncbiprotein:YP_009725297;urn:miriam:uniprot:P0C6X7;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M120_6"
      name "inhibited_space_80S_space_ribosome"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa16"
      uniprot "UNIPROT:P0C6X7"
    ]
    graphics [
      x 963.8192465056015
      y 2487.4923970569316
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "PUBMED:32680882"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_69"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re63"
      uniprot "NA"
    ]
    graphics [
      x 233.29520615434694
      y 2008.4010228493871
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_91"
      name "s228"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa139"
      uniprot "NA"
    ]
    graphics [
      x 187.96738506637075
      y 1717.088586401235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "PUBMED:32680882"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_60"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re53"
      uniprot "NA"
    ]
    graphics [
      x 651.6104592494069
      y 1812.1662202339423
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_23"
      name "ribosomal_space_40S_space_subunit"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa48"
      uniprot "NA"
    ]
    graphics [
      x 247.96738506637075
      y 1736.8450714810047
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:doi:10.1101/2020.05.18.102467;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ncbiprotein:YP_009725297;urn:miriam:uniprot:P0C6X7;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M120_5"
      name "inhibited_space_ribosomal_space_40S_space_SU"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa15"
      uniprot "UNIPROT:P0C6X7"
    ]
    graphics [
      x 1061.4760702265748
      y 1353.6936536421204
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "PUBMED:32680882"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_70"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re65"
      uniprot "NA"
    ]
    graphics [
      x 1570.4146427238684
      y 2262.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_93"
      name "csa15_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa141"
      uniprot "NA"
    ]
    graphics [
      x 1870.4146427238684
      y 2140.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_22"
      name "ribosomal_space_60S_space_subunit"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa47"
      uniprot "NA"
    ]
    graphics [
      x 521.4760702265747
      y 1209.525496975937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_74"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re7"
      uniprot "NA"
    ]
    graphics [
      x 441.61045924940686
      y 2012.5572932182372
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_94"
      name "ISGs"
      node_subtype "RNA"
      node_type "species"
      org_id "sa15"
      uniprot "NA"
    ]
    graphics [
      x 401.47607022657485
      y 1322.5572932182372
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_35"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re2"
      uniprot "NA"
    ]
    graphics [
      x 938.8536650413897
      y 1005.3799076516095
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_95"
      name "IFN_minus_sensitive_minus_response_minus_element"
      node_subtype "GENE"
      node_type "species"
      org_id "sa16"
      uniprot "NA"
    ]
    graphics [
      x 1456.6057727547209
      y 972.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:P52630;urn:miriam:uniprot:P52630;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc:11363;urn:miriam:ncbigene:6773;urn:miriam:ncbigene:6773;urn:miriam:refseq:NM_005419;urn:miriam:ensembl:ENSG00000170581;urn:miriam:uniprot:Q00978;urn:miriam:uniprot:Q00978;urn:miriam:refseq:NM_001385400;urn:miriam:ensembl:ENSG00000213928;urn:miriam:hgnc.symbol:IRF9;urn:miriam:ncbigene:10379;urn:miriam:hgnc.symbol:IRF9;urn:miriam:ncbigene:10379;urn:miriam:hgnc:6131;urn:miriam:refseq:NM_007315;urn:miriam:hgnc.symbol:STAT1;urn:miriam:hgnc.symbol:STAT1;urn:miriam:uniprot:P42224;urn:miriam:uniprot:P42224;urn:miriam:ncbigene:6772;urn:miriam:ncbigene:6772;urn:miriam:hgnc:11362;urn:miriam:ensembl:ENSG00000115415"
      hgnc "HGNC_SYMBOL:STAT2;HGNC_SYMBOL:IRF9;HGNC_SYMBOL:STAT1"
      map_id "M120_19"
      name "ISGF3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa4"
      uniprot "UNIPROT:P52630;UNIPROT:Q00978;UNIPROT:P42224"
    ]
    graphics [
      x 787.9673850663706
      y 1637.1654913580128
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_30"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re1"
      uniprot "NA"
    ]
    graphics [
      x 311.47607022657485
      y 1198.6792815999938
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:refseq:NM_007315;urn:miriam:hgnc.symbol:STAT1;urn:miriam:hgnc.symbol:STAT1;urn:miriam:uniprot:P42224;urn:miriam:uniprot:P42224;urn:miriam:ncbigene:6772;urn:miriam:ncbigene:6772;urn:miriam:hgnc:11362;urn:miriam:ensembl:ENSG00000115415;urn:miriam:uniprot:Q00978;urn:miriam:uniprot:Q00978;urn:miriam:refseq:NM_001385400;urn:miriam:ensembl:ENSG00000213928;urn:miriam:hgnc.symbol:IRF9;urn:miriam:ncbigene:10379;urn:miriam:hgnc.symbol:IRF9;urn:miriam:ncbigene:10379;urn:miriam:hgnc:6131;urn:miriam:uniprot:P52630;urn:miriam:uniprot:P52630;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc:11363;urn:miriam:ncbigene:6773;urn:miriam:ncbigene:6773;urn:miriam:refseq:NM_005419;urn:miriam:ensembl:ENSG00000170581"
      hgnc "HGNC_SYMBOL:STAT1;HGNC_SYMBOL:IRF9;HGNC_SYMBOL:STAT2"
      map_id "M120_13"
      name "ISGF3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa3"
      uniprot "UNIPROT:P42224;UNIPROT:Q00978;UNIPROT:P52630"
    ]
    graphics [
      x 668.8536650413897
      y 1014.0766051577275
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_34"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re15"
      uniprot "NA"
    ]
    graphics [
      x 1010.7120538352644
      y 463.1259168585731
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:P52630;urn:miriam:uniprot:P52630;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc:11363;urn:miriam:ncbigene:6773;urn:miriam:ncbigene:6773;urn:miriam:refseq:NM_005419;urn:miriam:ensembl:ENSG00000170581;urn:miriam:refseq:NM_007315;urn:miriam:hgnc.symbol:STAT1;urn:miriam:hgnc.symbol:STAT1;urn:miriam:uniprot:P42224;urn:miriam:uniprot:P42224;urn:miriam:ncbigene:6772;urn:miriam:ncbigene:6772;urn:miriam:hgnc:11362;urn:miriam:ensembl:ENSG00000115415"
      hgnc "HGNC_SYMBOL:STAT2;HGNC_SYMBOL:STAT1"
      map_id "M120_3"
      name "ISGF3_space_precursor"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa12"
      uniprot "UNIPROT:P52630;UNIPROT:P42224"
    ]
    graphics [
      x 1148.8536650413898
      y 773.0689340107122
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:Q00978;urn:miriam:uniprot:Q00978;urn:miriam:refseq:NM_001385400;urn:miriam:ensembl:ENSG00000213928;urn:miriam:hgnc.symbol:IRF9;urn:miriam:ncbigene:10379;urn:miriam:hgnc.symbol:IRF9;urn:miriam:ncbigene:10379;urn:miriam:hgnc:6131"
      hgnc "HGNC_SYMBOL:IRF9"
      map_id "M120_79"
      name "IRF9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa10"
      uniprot "UNIPROT:Q00978"
    ]
    graphics [
      x 1226.6812540881106
      y 312.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_33"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re14"
      uniprot "NA"
    ]
    graphics [
      x 1929.5902401008523
      y 371.7495849305784
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_62"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re55"
      uniprot "NA"
    ]
    graphics [
      x 1988.8536650413898
      y 848.5267273646541
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000168610;urn:miriam:ncbigene:6774;urn:miriam:refseq:NM_139276;urn:miriam:uniprot:P40763;urn:miriam:uniprot:P40763;urn:miriam:ncbigene:6774;urn:miriam:hgnc:11364;urn:miriam:refseq:NM_003150;urn:miriam:hgnc.symbol:STAT3;urn:miriam:hgnc.symbol:STAT3"
      hgnc "HGNC_SYMBOL:STAT3"
      map_id "M120_83"
      name "STAT3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa11"
      uniprot "UNIPROT:P40763"
    ]
    graphics [
      x 2021.4760702265748
      y 1180.9809222758117
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:refseq:NM_007315;urn:miriam:hgnc.symbol:STAT1;urn:miriam:hgnc.symbol:STAT1;urn:miriam:uniprot:P42224;urn:miriam:uniprot:P42224;urn:miriam:ncbigene:6772;urn:miriam:ncbigene:6772;urn:miriam:hgnc:11362;urn:miriam:ensembl:ENSG00000115415;urn:miriam:uniprot:P52630;urn:miriam:uniprot:P52630;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc:11363;urn:miriam:ncbigene:6773;urn:miriam:ncbigene:6773;urn:miriam:refseq:NM_005419;urn:miriam:ensembl:ENSG00000170581;urn:miriam:ensembl:ENSG00000168610;urn:miriam:ncbigene:6774;urn:miriam:refseq:NM_139276;urn:miriam:uniprot:P40763;urn:miriam:uniprot:P40763;urn:miriam:ncbigene:6774;urn:miriam:hgnc:11364;urn:miriam:refseq:NM_003150;urn:miriam:hgnc.symbol:STAT3;urn:miriam:hgnc.symbol:STAT3"
      hgnc "HGNC_SYMBOL:STAT1;HGNC_SYMBOL:STAT2;HGNC_SYMBOL:STAT3"
      map_id "M120_4"
      name "Inhibited_space_ISGF3_space_precursor"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa14"
      uniprot "UNIPROT:P42224;UNIPROT:P52630;UNIPROT:P40763"
    ]
    graphics [
      x 2482.5144998161113
      y 1904.2422462208663
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_66"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re60"
      uniprot "NA"
    ]
    graphics [
      x 1989.0165131006045
      y 2581.598892144574
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_88"
      name "s225"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa136"
      uniprot "NA"
    ]
    graphics [
      x 1874.8815294551364
      y 2596.7421007175967
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:refseq:NM_007315;urn:miriam:hgnc.symbol:STAT1;urn:miriam:hgnc.symbol:STAT1;urn:miriam:uniprot:P42224;urn:miriam:uniprot:P42224;urn:miriam:ncbigene:6772;urn:miriam:ncbigene:6772;urn:miriam:hgnc:11362;urn:miriam:ensembl:ENSG00000115415"
      hgnc "HGNC_SYMBOL:STAT1"
      map_id "M120_101"
      name "STAT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa5"
      uniprot "UNIPROT:P42224"
    ]
    graphics [
      x 1421.4760702265748
      y 1092.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:P52630;urn:miriam:uniprot:P52630;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc:11363;urn:miriam:ncbigene:6773;urn:miriam:ncbigene:6773;urn:miriam:refseq:NM_005419;urn:miriam:ensembl:ENSG00000170581"
      hgnc "HGNC_SYMBOL:STAT2"
      map_id "M120_103"
      name "STAT2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa7"
      uniprot "UNIPROT:P52630"
    ]
    graphics [
      x 2381.8480497933933
      y 647.07685047968
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_49"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re4"
      uniprot "NA"
    ]
    graphics [
      x 1796.090149807047
      y 496.8134512202714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:P52630;urn:miriam:uniprot:P52630;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc:11363;urn:miriam:ncbigene:6773;urn:miriam:ncbigene:6773;urn:miriam:refseq:NM_005419;urn:miriam:ensembl:ENSG00000170581"
      hgnc "HGNC_SYMBOL:STAT2"
      map_id "M120_102"
      name "STAT2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa6"
      uniprot "UNIPROT:P52630"
    ]
    graphics [
      x 1612.4690905844193
      y 222.06502830998443
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ec-code:2.7.10.2;urn:miriam:ncbigene:7297;urn:miriam:ncbigene:7297;urn:miriam:ensembl:ENSG00000105397;urn:miriam:refseq:NM_001385197;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc:12440;urn:miriam:uniprot:P29597;urn:miriam:uniprot:P29597"
      hgnc "HGNC_SYMBOL:TYK2"
      map_id "M120_97"
      name "TYK2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa37"
      uniprot "UNIPROT:P29597"
    ]
    graphics [
      x 1376.090149807047
      y 462.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_41"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re3"
      uniprot "NA"
    ]
    graphics [
      x 1403.616856433232
      y 2232.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:refseq:NM_007315;urn:miriam:hgnc.symbol:STAT1;urn:miriam:hgnc.symbol:STAT1;urn:miriam:uniprot:P42224;urn:miriam:uniprot:P42224;urn:miriam:ncbigene:6772;urn:miriam:ncbigene:6772;urn:miriam:hgnc:11362;urn:miriam:ensembl:ENSG00000115415"
      hgnc "HGNC_SYMBOL:STAT1"
      map_id "M120_98"
      name "STAT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa4"
      uniprot "UNIPROT:P42224"
    ]
    graphics [
      x 1417.0895867210118
      y 2622.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:hgnc.symbol:JAK1;urn:miriam:hgnc.symbol:JAK1;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc:6190;urn:miriam:ncbigene:3716;urn:miriam:ncbigene:3716;urn:miriam:ensembl:ENSG00000162434;urn:miriam:uniprot:P23458;urn:miriam:uniprot:P23458;urn:miriam:refseq:NM_002227"
      hgnc "HGNC_SYMBOL:JAK1"
      map_id "M120_96"
      name "JAK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa36"
      uniprot "UNIPROT:P23458"
    ]
    graphics [
      x 1030.4146427238684
      y 2114.0966255337726
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 47
    source 2
    target 1
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_78"
      target_id "M120_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 48
    source 1
    target 3
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_26"
      target_id "M120_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 49
    source 5
    target 2
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_92"
      target_id "M120_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 50
    source 6
    target 2
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CATALYSIS"
      source_id "M120_21"
      target_id "M120_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 51
    source 3
    target 4
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_31"
      target_id "M120_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 52
    source 23
    target 5
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_74"
      target_id "M120_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 53
    source 5
    target 9
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_92"
      target_id "M120_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 54
    source 7
    target 6
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_59"
      target_id "M120_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 55
    source 6
    target 8
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_21"
      target_id "M120_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 56
    source 6
    target 9
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CATALYSIS"
      source_id "M120_21"
      target_id "M120_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 57
    source 22
    target 7
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_22"
      target_id "M120_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 58
    source 18
    target 7
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_23"
      target_id "M120_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 59
    source 13
    target 8
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_104"
      target_id "M120_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 60
    source 8
    target 14
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_61"
      target_id "M120_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 61
    source 9
    target 10
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_77"
      target_id "M120_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 62
    source 10
    target 11
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_25"
      target_id "M120_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 63
    source 11
    target 12
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_63"
      target_id "M120_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 64
    source 13
    target 17
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_104"
      target_id "M120_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 14
    target 15
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_6"
      target_id "M120_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 15
    target 16
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_69"
      target_id "M120_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 18
    target 17
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_23"
      target_id "M120_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 17
    target 19
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_60"
      target_id "M120_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 69
    source 19
    target 20
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_5"
      target_id "M120_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 70
    source 20
    target 21
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_70"
      target_id "M120_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 71
    source 24
    target 23
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_94"
      target_id "M120_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 72
    source 25
    target 24
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_35"
      target_id "M120_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 73
    source 26
    target 25
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_95"
      target_id "M120_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 74
    source 27
    target 25
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "TRIGGER"
      source_id "M120_19"
      target_id "M120_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 75
    source 28
    target 27
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_30"
      target_id "M120_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 76
    source 29
    target 28
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_13"
      target_id "M120_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 77
    source 30
    target 29
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_34"
      target_id "M120_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 78
    source 31
    target 30
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_3"
      target_id "M120_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 79
    source 32
    target 30
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_79"
      target_id "M120_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 33
    target 31
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_33"
      target_id "M120_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 81
    source 31
    target 34
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_3"
      target_id "M120_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 82
    source 39
    target 33
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_101"
      target_id "M120_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 83
    source 40
    target 33
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_103"
      target_id "M120_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 84
    source 35
    target 34
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_83"
      target_id "M120_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 85
    source 34
    target 36
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_62"
      target_id "M120_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 86
    source 36
    target 37
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_4"
      target_id "M120_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 87
    source 37
    target 38
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_66"
      target_id "M120_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 88
    source 44
    target 39
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_41"
      target_id "M120_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 89
    source 41
    target 40
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_49"
      target_id "M120_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 90
    source 42
    target 41
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_102"
      target_id "M120_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 91
    source 43
    target 41
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CATALYSIS"
      source_id "M120_97"
      target_id "M120_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 92
    source 45
    target 44
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_98"
      target_id "M120_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 93
    source 46
    target 44
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CATALYSIS"
      source_id "M120_96"
      target_id "M120_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
