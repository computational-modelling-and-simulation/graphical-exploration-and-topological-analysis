# generated with VANTED V2.8.2 at Fri Mar 04 10:04:38 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:uniprot:Q86YT6;urn:miriam:pubmed:24185901;urn:miriam:ncbigene:57534;urn:miriam:ncbigene:57534;urn:miriam:refseq:NM_020774;urn:miriam:hgnc.symbol:MIB1;urn:miriam:ensembl:ENSG00000101752;urn:miriam:hgnc.symbol:MIB1;urn:miriam:hgnc:21086"
      hgnc "HGNC_SYMBOL:MIB1"
      map_id "M115_380"
      name "MIB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa977"
      uniprot "UNIPROT:Q86YT6"
    ]
    graphics [
      x 850.4146427238683
      y 2105.6390909233837
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_380"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:21985982"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_128"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10104"
      uniprot "NA"
    ]
    graphics [
      x 1388.8536650413898
      y 612.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_143"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10119"
      uniprot "NA"
    ]
    graphics [
      x 863.1642722604455
      y 2225.6390909233837
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_143"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_368"
      name "Nsp9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1428"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1942.5144998161113
      y 1847.606843746084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_368"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:2.3.2.27;urn:miriam:uniprot:Q86YT6;urn:miriam:pubmed:24185901;urn:miriam:ncbigene:57534;urn:miriam:ncbigene:57534;urn:miriam:refseq:NM_020774;urn:miriam:hgnc.symbol:MIB1;urn:miriam:ensembl:ENSG00000101752;urn:miriam:hgnc.symbol:MIB1;urn:miriam:hgnc:21086;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:MIB1;HGNC_SYMBOL:rep"
      map_id "M115_45"
      name "mibcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa28"
      uniprot "UNIPROT:Q86YT6;UNIPROT:P0DTD1"
    ]
    graphics [
      x 547.9673850663706
      y 1712.5225943565201
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_147"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10123"
      uniprot "NA"
    ]
    graphics [
      x 2531.476070226575
      y 1368.7316918640336
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_147"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:23111;urn:miriam:ncbigene:23111;urn:miriam:hgnc.symbol:SPART;urn:miriam:hgnc.symbol:SPART;urn:miriam:ensembl:ENSG00000133104;urn:miriam:hgnc:18514;urn:miriam:refseq:NM_001142294;urn:miriam:uniprot:Q8N0X7"
      hgnc "HGNC_SYMBOL:SPART"
      map_id "M115_379"
      name "SPART"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa970"
      uniprot "UNIPROT:Q8N0X7"
    ]
    graphics [
      x 2258.85366504139
      y 925.9542133851683
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_379"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ncbigene:23111;urn:miriam:ncbigene:23111;urn:miriam:hgnc.symbol:SPART;urn:miriam:hgnc.symbol:SPART;urn:miriam:ensembl:ENSG00000133104;urn:miriam:hgnc:18514;urn:miriam:refseq:NM_001142294;urn:miriam:uniprot:Q8N0X7;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:SPART;HGNC_SYMBOL:rep"
      map_id "M115_41"
      name "spartcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa24"
      uniprot "UNIPROT:Q8N0X7;UNIPROT:P0DTD1"
    ]
    graphics [
      x 2411.476070226575
      y 1367.7071639733388
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "PUBMED:19765186"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_130"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10106"
      uniprot "NA"
    ]
    graphics [
      x 608.8536650413897
      y 1029.8351640363044
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_130"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:AIFM1;urn:miriam:hgnc.symbol:AIFM1;urn:miriam:hgnc:8768;urn:miriam:ncbigene:9131;urn:miriam:ncbigene:9131;urn:miriam:ec-code:1.6.99.-;urn:miriam:uniprot:O95831;urn:miriam:ensembl:ENSG00000156709;urn:miriam:refseq:NM_001130846"
      hgnc "HGNC_SYMBOL:AIFM1"
      map_id "M115_253"
      name "AIFM1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1071"
      uniprot "UNIPROT:O95831"
    ]
    graphics [
      x 682.5144998161113
      y 1962.9652168721805
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_253"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:19765186;urn:miriam:ncbigene:23111;urn:miriam:ncbigene:23111;urn:miriam:hgnc.symbol:SPART;urn:miriam:hgnc.symbol:SPART;urn:miriam:ensembl:ENSG00000133104;urn:miriam:hgnc:18514;urn:miriam:refseq:NM_001142294;urn:miriam:uniprot:Q8N0X7;urn:miriam:hgnc.symbol:AIFM1;urn:miriam:hgnc.symbol:AIFM1;urn:miriam:hgnc:8768;urn:miriam:ncbigene:9131;urn:miriam:ncbigene:9131;urn:miriam:ec-code:1.6.99.-;urn:miriam:uniprot:O95831;urn:miriam:ensembl:ENSG00000156709;urn:miriam:refseq:NM_001130846"
      hgnc "HGNC_SYMBOL:SPART;HGNC_SYMBOL:AIFM1"
      map_id "M115_61"
      name "SPARTcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa46"
      uniprot "UNIPROT:Q8N0X7;UNIPROT:O95831"
    ]
    graphics [
      x 231.93582865917097
      y 1069.8166073337368
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "PUBMED:17016423;PUBMED:17139284;PUBMED:10592235"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_158"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10135"
      uniprot "NA"
    ]
    graphics [
      x 940.4146427238683
      y 2116.6019746441584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_158"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:643975"
      hgnc "NA"
      map_id "M115_278"
      name "Flavin_space_adenine_space_dinucleotide"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1166"
      uniprot "NA"
    ]
    graphics [
      x 1269.0903364967949
      y 2412.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_278"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:17016423;urn:miriam:pubmed:17139284;urn:miriam:pubmed:10592235;urn:miriam:hgnc.symbol:AIFM1;urn:miriam:hgnc.symbol:AIFM1;urn:miriam:hgnc:8768;urn:miriam:ncbigene:9131;urn:miriam:ncbigene:9131;urn:miriam:ec-code:1.6.99.-;urn:miriam:uniprot:O95831;urn:miriam:ensembl:ENSG00000156709;urn:miriam:refseq:NM_001130846;urn:miriam:pubchem.compound:643975"
      hgnc "HGNC_SYMBOL:AIFM1"
      map_id "M115_73"
      name "AIFMFlaComp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa58"
      uniprot "UNIPROT:O95831"
    ]
    graphics [
      x 817.9673850663706
      y 1641.4667846338425
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:28514;urn:miriam:ensembl:ENSG00000198719;urn:miriam:ncbigene:28514;urn:miriam:hgnc:2908;urn:miriam:uniprot:O00548;urn:miriam:refseq:NM_005618;urn:miriam:hgnc.symbol:DLL1;urn:miriam:hgnc.symbol:DLL1"
      hgnc "HGNC_SYMBOL:DLL1"
      map_id "M115_269"
      name "DLL1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1135"
      uniprot "UNIPROT:O00548"
    ]
    graphics [
      x 1726.8405824608653
      y 230.0092148220631
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_269"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:21985982;urn:miriam:ncbigene:28514;urn:miriam:ensembl:ENSG00000198719;urn:miriam:ncbigene:28514;urn:miriam:hgnc:2908;urn:miriam:uniprot:O00548;urn:miriam:refseq:NM_005618;urn:miriam:hgnc.symbol:DLL1;urn:miriam:hgnc.symbol:DLL1;urn:miriam:ec-code:2.3.2.27;urn:miriam:uniprot:Q86YT6;urn:miriam:pubmed:24185901;urn:miriam:ncbigene:57534;urn:miriam:ncbigene:57534;urn:miriam:refseq:NM_020774;urn:miriam:hgnc.symbol:MIB1;urn:miriam:ensembl:ENSG00000101752;urn:miriam:hgnc.symbol:MIB1;urn:miriam:hgnc:21086"
      hgnc "HGNC_SYMBOL:DLL1;HGNC_SYMBOL:MIB1"
      map_id "M115_65"
      name "MIBcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa50"
      uniprot "UNIPROT:O00548;UNIPROT:Q86YT6"
    ]
    graphics [
      x 1527.9537954952352
      y 1422.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 17
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_380"
      target_id "M115_128"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 18
    source 1
    target 3
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_380"
      target_id "M115_143"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 19
    source 15
    target 2
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_269"
      target_id "M115_128"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 20
    source 2
    target 16
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_128"
      target_id "M115_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 21
    source 4
    target 3
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_368"
      target_id "M115_143"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 22
    source 3
    target 5
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_143"
      target_id "M115_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 23
    source 4
    target 6
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_368"
      target_id "M115_147"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 24
    source 7
    target 6
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_379"
      target_id "M115_147"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 25
    source 6
    target 8
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_147"
      target_id "M115_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 26
    source 7
    target 9
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_379"
      target_id "M115_130"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 27
    source 10
    target 9
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_253"
      target_id "M115_130"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 28
    source 9
    target 11
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_130"
      target_id "M115_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 29
    source 10
    target 12
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_253"
      target_id "M115_158"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 30
    source 13
    target 12
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_278"
      target_id "M115_158"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 31
    source 12
    target 14
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_158"
      target_id "M115_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
