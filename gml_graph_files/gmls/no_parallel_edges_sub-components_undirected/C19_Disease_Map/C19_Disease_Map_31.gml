# generated with VANTED V2.8.2 at Fri Mar 04 10:04:38 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:pubmed:25847972;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q9ULZ3;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033;urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746;urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3"
      hgnc "HGNC_SYMBOL:TRAF3;HGNC_SYMBOL:MAVS;HGNC_SYMBOL:PYCARD"
      map_id "M117_8"
      name "ASC:MAVS:TRAF3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa27"
      uniprot "UNIPROT:Q13114;UNIPROT:Q9ULZ3;UNIPROT:Q7Z434"
    ]
    graphics [
      x 1781.4760702265748
      y 1423.8597507391748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:25847972;PUBMED:31034780"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_41"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re52"
      uniprot "NA"
    ]
    graphics [
      x 2452.5144998161113
      y 1972.5354146823697
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "PUBMED:25847972"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_40"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re51"
      uniprot "NA"
    ]
    graphics [
      x 1988.8536650413898
      y 780.9283152308135
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3"
      hgnc "HGNC_SYMBOL:PYCARD"
      map_id "M117_58"
      name "ASC"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa103"
      uniprot "UNIPROT:Q9ULZ3"
    ]
    graphics [
      x 1344.4382418017099
      y 402.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033"
      hgnc "HGNC_SYMBOL:TRAF3"
      map_id "M117_113"
      name "TRAF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa99"
      uniprot "UNIPROT:Q13114"
    ]
    graphics [
      x 1777.9673850663708
      y 1571.4892767681847
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746"
      hgnc "HGNC_SYMBOL:MAVS"
      map_id "M117_57"
      name "MAVS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa102"
      uniprot "UNIPROT:Q7Z434"
    ]
    graphics [
      x 2321.476070226575
      y 1059.9137600303363
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "PUBMED:25847972"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_38"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re49"
      uniprot "NA"
    ]
    graphics [
      x 1930.4146427238684
      y 2230.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_52"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re66"
      uniprot "NA"
    ]
    graphics [
      x 1852.9968297881219
      y 386.28293020415833
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:1489679;urn:miriam:uniprot:P59636;urn:miriam:taxonomy:694009"
      hgnc "NA"
      map_id "M117_84"
      name "Orf9b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa165"
      uniprot "UNIPROT:P59636"
    ]
    graphics [
      x 1624.6943393561805
      y 1092.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_83"
      name "s878"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa164"
      uniprot "NA"
    ]
    graphics [
      x 1796.090149807047
      y 386.4188700408283
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_53"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re67"
      uniprot "NA"
    ]
    graphics [
      x 1148.8536650413898
      y 683.0689340107122
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3"
      hgnc "HGNC_SYMBOL:PYCARD"
      map_id "M117_59"
      name "ASC"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa104"
      uniprot "UNIPROT:Q9ULZ3"
    ]
    graphics [
      x 1027.9673850663708
      y 1562.3785906698167
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:Q9ULZ3;urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746;urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3"
      hgnc "HGNC_SYMBOL:MAVS;HGNC_SYMBOL:PYCARD"
      map_id "M117_9"
      name "ASC:MAVS"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa28"
      uniprot "UNIPROT:Q9ULZ3;UNIPROT:Q7Z434"
    ]
    graphics [
      x 1916.4030979934528
      y 2679.9461954535
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "PUBMED:25847972"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_39"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re50"
      uniprot "NA"
    ]
    graphics [
      x 2230.4146427238684
      y 2181.258966139932
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:pubmed:25847972;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q9ULZ3;urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033;urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746"
      hgnc "HGNC_SYMBOL:PYCARD;HGNC_SYMBOL:TRAF3;HGNC_SYMBOL:MAVS"
      map_id "M117_10"
      name "ASC:MAVS:TRAF3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa29"
      uniprot "UNIPROT:Q13114;UNIPROT:Q9ULZ3;UNIPROT:Q7Z434"
    ]
    graphics [
      x 1162.5144998161113
      y 2002.7078987280793
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_32"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re43"
      uniprot "NA"
    ]
    graphics [
      x 1072.5144998161113
      y 1981.7766046467766
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:ncbigene:1489669;urn:miriam:taxonomy:694009"
      hgnc "NA"
      map_id "M117_97"
      name "Orf3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa3"
      uniprot "UNIPROT:P59632"
    ]
    graphics [
      x 1000.4146427238683
      y 2121.9577197196427
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:uniprot:Q13114;urn:miriam:taxonomy:694009;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033;urn:miriam:uniprot:P59632;urn:miriam:ncbigene:1489669;urn:miriam:taxonomy:694009"
      hgnc "HGNC_SYMBOL:TRAF3"
      map_id "M117_7"
      name "TRAF3:SARS_space_Orf3a"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa24"
      uniprot "UNIPROT:P59632;UNIPROT:Q13114"
    ]
    graphics [
      x 2392.5144998161113
      y 1950.0666291700395
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "PUBMED:25847972"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_34"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re45"
      uniprot "NA"
    ]
    graphics [
      x 1058.8536650413898
      y 1039.5029478087847
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:NLRP3"
      map_id "M117_56"
      name "NLRP3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa101"
      uniprot "UNIPROT:Q96P20"
    ]
    graphics [
      x 1358.8536650413898
      y 612.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:Q9ULZ3;urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3"
      hgnc "HGNC_SYMBOL:NLRP3;HGNC_SYMBOL:PYCARD"
      map_id "M117_3"
      name "NLRP3_space_oligomer:ASC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa13"
      uniprot "UNIPROT:Q96P20;UNIPROT:Q9ULZ3"
    ]
    graphics [
      x 671.4760702265747
      y 1286.649450965198
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re19"
      uniprot "NA"
    ]
    graphics [
      x 1508.8536650413898
      y 582.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "PUBMED:25847972"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_26"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re27"
      uniprot "NA"
    ]
    graphics [
      x 817.9673850663706
      y 1596.3585216247106
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M117_55"
      name "CASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa100"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 607.9673850663706
      y 1521.7955934272911
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:P29466;urn:miriam:uniprot:Q9ULZ3;urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292;urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3;urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:CASP1;HGNC_SYMBOL:PYCARD;HGNC_SYMBOL:NLRP3"
      map_id "M117_2"
      name "NLRP3_space_oligomer:ASC:proCaspase1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa10"
      uniprot "UNIPROT:Q96P20;UNIPROT:P29466;UNIPROT:Q9ULZ3"
    ]
    graphics [
      x 1778.8536650413898
      y 826.8134512202714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P08311;urn:miriam:ncbigene:1511;urn:miriam:ncbigene:1511;urn:miriam:ec-code:3.4.21.20;urn:miriam:hgnc:2532;urn:miriam:hgnc.symbol:CTSG;urn:miriam:hgnc.symbol:CTSG;urn:miriam:refseq:NM_001911;urn:miriam:ensembl:ENSG00000100448"
      hgnc "HGNC_SYMBOL:CTSG"
      map_id "M117_96"
      name "CTSG"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa26"
      uniprot "UNIPROT:P08311"
    ]
    graphics [
      x 1166.6812540881106
      y 330.2807412073116
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M117_85"
      name "CASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa168"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 1477.9673850663708
      y 1632.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M117_88"
      name "CASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa170"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 788.8536650413897
      y 971.3549477797077
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M117_89"
      name "CASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa171"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 1506.8814471766686
      y 282.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M117_86"
      name "CASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa169"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 712.5144998161113
      y 1942.4666947456517
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_20"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re20"
      uniprot "NA"
    ]
    graphics [
      x 929.1188547295466
      y 2310.416026433729
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P29466;urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292;urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M117_16"
      name "CASP1(120_minus_197):CASP1(317_minus_404)"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa7"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 1147.9673850663708
      y 1672.7078987280793
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_21"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re21"
      uniprot "NA"
    ]
    graphics [
      x 1582.5144998161113
      y 2112.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P29466;urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292;urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M117_14"
      name "Caspase_minus_1_space_Tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa5"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 337.96738506637075
      y 1769.203400141255
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_24"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re25"
      uniprot "NA"
    ]
    graphics [
      x 727.9673850663706
      y 1586.1378443135568
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_22"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re22"
      uniprot "NA"
    ]
    graphics [
      x 611.4760702265747
      y 1392.9213497817198
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:ncbigene:3553"
      hgnc "HGNC_SYMBOL:IL1B"
      map_id "M117_70"
      name "proIL_minus_1B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa15"
      uniprot "UNIPROT:P01584"
    ]
    graphics [
      x 405.62935513004766
      y 2130.792119387916
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:ncbigene:3553"
      hgnc "HGNC_SYMBOL:IL1B"
      map_id "M117_87"
      name "IL_minus_1B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa17"
      uniprot "UNIPROT:P01584"
    ]
    graphics [
      x 1030.5699334766189
      y 552.4843179871757
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:ncbigene:3553"
      hgnc "HGNC_SYMBOL:IL1B"
      map_id "M117_90"
      name "proIL_minus_1B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa172"
      uniprot "UNIPROT:P01584"
    ]
    graphics [
      x 848.8536650413897
      y 916.0055613274181
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_23"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re23"
      uniprot "NA"
    ]
    graphics [
      x 1653.5246787545916
      y 312.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:ncbigene:3553"
      hgnc "HGNC_SYMBOL:IL1B"
      map_id "M117_95"
      name "IL_minus_1B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa21"
      uniprot "UNIPROT:P01584"
    ]
    graphics [
      x 1628.8536650413898
      y 702.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_43"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re55"
      uniprot "NA"
    ]
    graphics [
      x 754.7460326427671
      y 2529.9959686251036
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:3553"
      hgnc "NA"
      map_id "M117_110"
      name "proIL_minus_1B"
      node_subtype "RNA"
      node_type "species"
      org_id "sa94"
      uniprot "NA"
    ]
    graphics [
      x 1329.0903364967949
      y 2382.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_28"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re37"
      uniprot "NA"
    ]
    graphics [
      x 1911.6104592494069
      y 1720.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:3553"
      hgnc "NA"
      map_id "M117_112"
      name "proIL_minus_1B"
      node_subtype "GENE"
      node_type "species"
      org_id "sa96"
      uniprot "NA"
    ]
    graphics [
      x 1541.4760702265748
      y 1152.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:Q00653;urn:miriam:uniprot:P19838;urn:miriam:hgnc.symbol:NFKB2;urn:miriam:ncbigene:4791;urn:miriam:uniprot:Q00653;urn:miriam:ncbigene:4790;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:uniprot:P19838"
      hgnc "HGNC_SYMBOL:NFKB2;HGNC_SYMBOL:NFKB1"
      map_id "M117_6"
      name "Nf_minus_KB_space_Complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa23"
      uniprot "UNIPROT:Q00653;UNIPROT:P19838"
    ]
    graphics [
      x 2182.5144998161113
      y 1927.9130903024256
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re47"
      uniprot "NA"
    ]
    graphics [
      x 1251.7595581209157
      y 2172.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_29"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re38"
      uniprot "NA"
    ]
    graphics [
      x 2468.85366504139
      y 906.2652630185033
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:3606"
      hgnc "NA"
      map_id "M117_111"
      name "proIL_minus_18"
      node_subtype "GENE"
      node_type "species"
      org_id "sa95"
      uniprot "NA"
    ]
    graphics [
      x 2441.476070226575
      y 1466.618275280025
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:3606"
      hgnc "NA"
      map_id "M117_109"
      name "proIL_minus_18"
      node_subtype "RNA"
      node_type "species"
      org_id "sa93"
      uniprot "NA"
    ]
    graphics [
      x 2588.85366504139
      y 999.8910710989505
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_44"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re56"
      uniprot "NA"
    ]
    graphics [
      x 2081.476070226575
      y 1510.3237227534223
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:3606;urn:miriam:uniprot:Q14116;urn:miriam:hgnc.symbol:IL18"
      hgnc "HGNC_SYMBOL:IL18"
      map_id "M117_62"
      name "proIL_minus_18"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa14"
      uniprot "UNIPROT:Q14116"
    ]
    graphics [
      x 1971.6104592494069
      y 1727.606843746084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:Q00653;urn:miriam:uniprot:P19838;urn:miriam:hgnc.symbol:NFKB2;urn:miriam:ncbigene:4791;urn:miriam:uniprot:Q00653;urn:miriam:ncbigene:4790;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:uniprot:P19838"
      hgnc "HGNC_SYMBOL:NFKB2;HGNC_SYMBOL:NFKB1"
      map_id "M117_5"
      name "Nf_minus_KB_space_Complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa22"
      uniprot "UNIPROT:Q00653;UNIPROT:P19838"
    ]
    graphics [
      x 1522.7821805363424
      y 1542.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P19838;urn:miriam:hgnc.symbol:NFKB2;urn:miriam:ncbigene:4791;urn:miriam:uniprot:Q00653;urn:miriam:ncbigene:4790;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:uniprot:P19838"
      hgnc "HGNC_SYMBOL:NFKB2;HGNC_SYMBOL:NFKB1"
      map_id "M117_4"
      name "NF_minus_KB_space_COMPLEX:IKBA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa21"
      uniprot "UNIPROT:P19838;UNIPROT:Q00653"
    ]
    graphics [
      x 1000.4146427238683
      y 2181.9577197196427
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_35"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re46"
      uniprot "NA"
    ]
    graphics [
      x 1001.4760702265747
      y 1198.6039843924614
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_102"
      name "IKBA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa82"
      uniprot "NA"
    ]
    graphics [
      x 1487.2023363703734
      y 912.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re42"
      uniprot "NA"
    ]
    graphics [
      x 2531.476070226575
      y 1062.1387724977735
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:4790;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:uniprot:P19838"
      hgnc "HGNC_SYMBOL:NFKB1"
      map_id "M117_103"
      name "p50"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa83"
      uniprot "UNIPROT:P19838"
    ]
    graphics [
      x 2171.476070226575
      y 1368.7342851305455
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:hgnc.symbol:NFKB2;urn:miriam:ncbigene:4791;urn:miriam:uniprot:Q00653"
      hgnc "HGNC_SYMBOL:NFKB2"
      map_id "M117_105"
      name "p65"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa86"
      uniprot "UNIPROT:Q00653"
    ]
    graphics [
      x 2288.85366504139
      y 792.5883256062364
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_33"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re44"
      uniprot "NA"
    ]
    graphics [
      x 1241.4760702265748
      y 1296.5060637260106
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:4790;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:uniprot:P19838"
      hgnc "HGNC_SYMBOL:NFKB1"
      map_id "M117_104"
      name "p105"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa84"
      uniprot "UNIPROT:P19838"
    ]
    graphics [
      x 1451.499476749866
      y 1722.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_49"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re62"
      uniprot "NA"
    ]
    graphics [
      x 1601.4760702265748
      y 1422.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:4790;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:uniprot:P19838"
      hgnc "HGNC_SYMBOL:NFKB1"
      map_id "M117_71"
      name "p105"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa151"
      uniprot "UNIPROT:P19838"
    ]
    graphics [
      x 1791.6104592494069
      y 1783.8597507391748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:uniprot:Q13114;urn:miriam:taxonomy:694009;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033;urn:miriam:uniprot:P59632;urn:miriam:ncbigene:1489669;urn:miriam:taxonomy:694009"
      hgnc "HGNC_SYMBOL:TRAF3"
      map_id "M117_12"
      name "TRAF3:SARS_space_Orf3a"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa30"
      uniprot "UNIPROT:P59632;UNIPROT:Q13114"
    ]
    graphics [
      x 2198.85366504139
      y 822.1234903852876
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_48"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re61"
      uniprot "NA"
    ]
    graphics [
      x 2258.85366504139
      y 662.3197782540453
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:ncbigene:1489669;urn:miriam:taxonomy:694009"
      hgnc "NA"
      map_id "M117_68"
      name "Orf3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa147"
      uniprot "UNIPROT:P59632"
    ]
    graphics [
      x 1546.6057727547209
      y 942.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033"
      hgnc "HGNC_SYMBOL:TRAF3"
      map_id "M117_69"
      name "TRAF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa148"
      uniprot "UNIPROT:Q13114"
    ]
    graphics [
      x 1961.4760702265748
      y 1277.606843746084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:3606;urn:miriam:uniprot:Q14116;urn:miriam:hgnc.symbol:IL18"
      hgnc "HGNC_SYMBOL:IL18"
      map_id "M117_79"
      name "IL_minus_18"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa16"
      uniprot "UNIPROT:Q14116"
    ]
    graphics [
      x 1241.4760702265748
      y 1113.9185229461025
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:3606;urn:miriam:uniprot:Q14116;urn:miriam:hgnc.symbol:IL18"
      hgnc "HGNC_SYMBOL:IL18"
      map_id "M117_92"
      name "proIL_minus_18"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa18"
      uniprot "UNIPROT:Q14116"
    ]
    graphics [
      x 1162.5144998161113
      y 1912.7078987280793
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_25"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re26"
      uniprot "NA"
    ]
    graphics [
      x 1298.8536650413898
      y 732.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:3606;urn:miriam:uniprot:Q14116;urn:miriam:hgnc.symbol:IL18"
      hgnc "HGNC_SYMBOL:IL18"
      map_id "M117_94"
      name "IL_minus_18"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa20"
      uniprot "UNIPROT:Q14116"
    ]
    graphics [
      x 1331.4760702265748
      y 1392.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      annotation "PUBMED:25847972;PUBMED:25770182;PUBMED:26331680;PUBMED:29789363;PUBMED:28356568;PUBMED:28741645"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_37"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re48"
      uniprot "NA"
    ]
    graphics [
      x 1778.8536650413898
      y 586.8134512202714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:P08238;urn:miriam:uniprot:Q9Y2Z0;urn:miriam:hgnc:16987;urn:miriam:ncbigene:10910;urn:miriam:ncbigene:10910;urn:miriam:uniprot:Q9Y2Z0;urn:miriam:ensembl:ENSG00000165416;urn:miriam:refseq:NM_001130912;urn:miriam:hgnc.symbol:SUGT1;urn:miriam:hgnc.symbol:SUGT1;urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:SUGT1;HGNC_SYMBOL:NLRP3"
      map_id "M117_15"
      name "NLRP3:SUGT1:HSP90"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa6"
      uniprot "UNIPROT:Q96P20;UNIPROT:P08238;UNIPROT:Q9Y2Z0"
    ]
    graphics [
      x 861.6104592494069
      y 1843.1690082783348
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.go:GO%3A0002221"
      hgnc "NA"
      map_id "M117_107"
      name "DAMPs"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa91"
      uniprot "NA"
    ]
    graphics [
      x 1868.8536650413898
      y 619.8371658422358
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.go:GO%3A0002221"
      hgnc "NA"
      map_id "M117_106"
      name "PAMPs"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa90"
      uniprot "NA"
    ]
    graphics [
      x 908.8536650413897
      y 773.5627285309354
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A46661;urn:miriam:obo.chebi:CHEBI%3A30563;urn:miriam:obo.chebi:CHEBI%3A16336"
      hgnc "NA"
      map_id "M117_60"
      name "NLRP3_space_Elicitor_space_Small_space_Molecules"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa135"
      uniprot "NA"
    ]
    graphics [
      x 2201.476070226575
      y 1050.5867359968995
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:hgnc:16400;urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:Q96P20;urn:miriam:ensembl:ENSG00000162711;urn:miriam:ncbigene:351;urn:miriam:refseq:NM_004895;urn:miriam:uniprot:P05067;urn:miriam:hgnc.symbol:APP;urn:miriam:uniprot:P09616;urn:miriam:hgnc.symbol:hly;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:APP;HGNC_SYMBOL:hly;HGNC_SYMBOL:NLRP3"
      map_id "M117_61"
      name "NLRP3_space_Elicitor_space_Proteins"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa136"
      uniprot "UNIPROT:Q96P20;UNIPROT:P05067;UNIPROT:P09616"
    ]
    graphics [
      x 1577.2023363703734
      y 882.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A26523"
      hgnc "NA"
      map_id "M117_54"
      name "Reactive_space_Oxygen_space_Species"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa1"
      uniprot "NA"
    ]
    graphics [
      x 1958.8536650413898
      y 872.6254878021133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:Q9H3M7;urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548;urn:miriam:hgnc:16952;urn:miriam:ncbigene:10628;urn:miriam:ensembl:ENSG00000265972;urn:miriam:ncbigene:10628;urn:miriam:uniprot:Q9H3M7;urn:miriam:refseq:NM_006472;urn:miriam:hgnc.symbol:TXNIP;urn:miriam:hgnc.symbol:TXNIP"
      hgnc "HGNC_SYMBOL:NLRP3;HGNC_SYMBOL:TXNIP"
      map_id "M117_11"
      name "TXNIP:NLRP3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa3"
      uniprot "UNIPROT:Q96P20;UNIPROT:Q9H3M7"
    ]
    graphics [
      x 1058.8536650413898
      y 826.6180026711968
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29108"
      hgnc "NA"
      map_id "M117_65"
      name "Ca2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa142"
      uniprot "NA"
    ]
    graphics [
      x 2261.476070226575
      y 1356.5080320248062
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P08238;urn:miriam:uniprot:Q9Y2Z0;urn:miriam:ncbigene:3326;urn:miriam:ncbigene:3326;urn:miriam:uniprot:P08238;urn:miriam:hgnc:5258;urn:miriam:refseq:NM_007355;urn:miriam:ensembl:ENSG00000096384;urn:miriam:hgnc.symbol:HSP90AB1;urn:miriam:hgnc.symbol:HSP90AB1;urn:miriam:hgnc:16987;urn:miriam:ncbigene:10910;urn:miriam:ncbigene:10910;urn:miriam:uniprot:Q9Y2Z0;urn:miriam:ensembl:ENSG00000165416;urn:miriam:refseq:NM_001130912;urn:miriam:hgnc.symbol:SUGT1;urn:miriam:hgnc.symbol:SUGT1"
      hgnc "HGNC_SYMBOL:HSP90AB1;HGNC_SYMBOL:SUGT1"
      map_id "M117_13"
      name "SUGT1:HSP90AB1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa4"
      uniprot "UNIPROT:P08238;UNIPROT:Q9Y2Z0"
    ]
    graphics [
      x 1901.4760702265748
      y 1480.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      annotation "PUBMED:29712950;PUBMED:17435760"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_30"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re4"
      uniprot "NA"
    ]
    graphics [
      x 2242.5144998161113
      y 2047.82400200343
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_42"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re54"
      uniprot "NA"
    ]
    graphics [
      x 397.96738506637075
      y 1982.5572932182372
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:NLRP3"
      map_id "M117_108"
      name "NLRP3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa92"
      uniprot "UNIPROT:Q96P20"
    ]
    graphics [
      x 427.96738506637064
      y 1712.5572932182372
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_46"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re6"
      uniprot "NA"
    ]
    graphics [
      x 1691.4760702265748
      y 1329.571063044361
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:hgnc:16952;urn:miriam:ncbigene:10628;urn:miriam:ensembl:ENSG00000265972;urn:miriam:ncbigene:10628;urn:miriam:uniprot:Q9H3M7;urn:miriam:refseq:NM_006472;urn:miriam:hgnc.symbol:TXNIP;urn:miriam:hgnc.symbol:TXNIP"
      hgnc "HGNC_SYMBOL:TXNIP"
      map_id "M117_101"
      name "TXNIP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa8"
      uniprot "UNIPROT:Q9H3M7"
    ]
    graphics [
      x 1661.4760702265748
      y 1059.571063044361
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_17"
      name "NA"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "re1"
      uniprot "NA"
    ]
    graphics [
      x 1091.4760702265748
      y 1307.5375698961307
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_27"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re3"
      uniprot "NA"
    ]
    graphics [
      x 2452.5144998161113
      y 2019.0937705471872
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:7295;urn:miriam:ncbigene:7295;urn:miriam:uniprot:P10599;urn:miriam:hgnc:12435;urn:miriam:ensembl:ENSG00000136810;urn:miriam:refseq:NM_001244938;urn:miriam:hgnc.symbol:TXN;urn:miriam:hgnc.symbol:TXN"
      hgnc "HGNC_SYMBOL:TXN"
      map_id "M117_100"
      name "TXN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa7"
      uniprot "UNIPROT:P10599"
    ]
    graphics [
      x 2380.4146427238684
      y 2228.660225715749
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P10599;urn:miriam:uniprot:Q9H3M7;urn:miriam:ncbigene:7295;urn:miriam:ncbigene:7295;urn:miriam:uniprot:P10599;urn:miriam:hgnc:12435;urn:miriam:ensembl:ENSG00000136810;urn:miriam:refseq:NM_001244938;urn:miriam:hgnc.symbol:TXN;urn:miriam:hgnc.symbol:TXN;urn:miriam:hgnc:16952;urn:miriam:ncbigene:10628;urn:miriam:ensembl:ENSG00000265972;urn:miriam:ncbigene:10628;urn:miriam:uniprot:Q9H3M7;urn:miriam:refseq:NM_006472;urn:miriam:hgnc.symbol:TXNIP;urn:miriam:hgnc.symbol:TXNIP"
      hgnc "HGNC_SYMBOL:TXN;HGNC_SYMBOL:TXNIP"
      map_id "M117_1"
      name "Thioredoxin:TXNIP"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa1"
      uniprot "UNIPROT:P10599;UNIPROT:Q9H3M7"
    ]
    graphics [
      x 2002.5144998161113
      y 1926.4869840939841
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_19"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re2"
      uniprot "NA"
    ]
    graphics [
      x 2153.9845449193244
      y 2357.43201955095
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A26523"
      hgnc "NA"
      map_id "M117_93"
      name "Reactive_space_Oxygen_space_Species"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa2"
      uniprot "NA"
    ]
    graphics [
      x 1337.7845707541828
      y 2022.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:7295;urn:miriam:ncbigene:7295;urn:miriam:uniprot:P10599;urn:miriam:hgnc:12435;urn:miriam:ensembl:ENSG00000136810;urn:miriam:refseq:NM_001244938;urn:miriam:hgnc.symbol:TXN;urn:miriam:hgnc.symbol:TXN"
      hgnc "HGNC_SYMBOL:TXN"
      map_id "M117_91"
      name "TXN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa175"
      uniprot "UNIPROT:P10599"
    ]
    graphics [
      x 1492.5144998161113
      y 2022.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ncbigene:3326;urn:miriam:ncbigene:3326;urn:miriam:uniprot:P08238;urn:miriam:hgnc:5258;urn:miriam:refseq:NM_007355;urn:miriam:ensembl:ENSG00000096384;urn:miriam:hgnc.symbol:HSP90AB1;urn:miriam:hgnc.symbol:HSP90AB1"
      hgnc "HGNC_SYMBOL:HSP90AB1"
      map_id "M117_98"
      name "HSP90AB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa4"
      uniprot "UNIPROT:P08238"
    ]
    graphics [
      x 1966.4537615253853
      y 2117.606843746084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:hgnc:16987;urn:miriam:ncbigene:10910;urn:miriam:ncbigene:10910;urn:miriam:uniprot:Q9Y2Z0;urn:miriam:ensembl:ENSG00000165416;urn:miriam:refseq:NM_001130912;urn:miriam:hgnc.symbol:SUGT1;urn:miriam:hgnc.symbol:SUGT1"
      hgnc "HGNC_SYMBOL:SUGT1"
      map_id "M117_99"
      name "SUGT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa5"
      uniprot "UNIPROT:Q9Y2Z0"
    ]
    graphics [
      x 1395.8453706378386
      y 2442.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      annotation "PUBMED:26331680"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re59"
      uniprot "NA"
    ]
    graphics [
      x 2361.6104592494066
      y 1795.4103387353184
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      annotation "PUBMED:26331680"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re60"
      uniprot "NA"
    ]
    graphics [
      x 1087.0170235952316
      y 2194.810963633781
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29108"
      hgnc "NA"
      map_id "M117_66"
      name "Ca2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa144"
      uniprot "NA"
    ]
    graphics [
      x 921.6104592494069
      y 1814.8029037089432
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P59637;urn:miriam:taxonomy:694009;urn:miriam:ncbigene:1489671;urn:miriam:hgnc.symbol:E"
      hgnc "HGNC_SYMBOL:E"
      map_id "M117_67"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa146"
      uniprot "UNIPROT:P59637"
    ]
    graphics [
      x 472.5144998161113
      y 1948.7296889316008
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29108"
      hgnc "NA"
      map_id "M117_64"
      name "Ca2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa141"
      uniprot "NA"
    ]
    graphics [
      x 2660.1382490073115
      y 1834.569344016625
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:uniprot:P59637;urn:miriam:ncbigene:1489671;urn:miriam:hgnc.symbol:E"
      hgnc "HGNC_SYMBOL:E"
      map_id "M117_63"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa140"
      uniprot "UNIPROT:P59637"
    ]
    graphics [
      x 1301.4760702265748
      y 1344.3580893313576
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      annotation "PUBMED:25770182;PUBMED:28356568"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_51"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re65"
      uniprot "NA"
    ]
    graphics [
      x 2017.9673850663708
      y 1595.7137326697948
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_81"
      name "s876"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa162"
      uniprot "NA"
    ]
    graphics [
      x 1971.6104592494069
      y 1787.606843746084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17245"
      hgnc "NA"
      map_id "M117_78"
      name "CO"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa158"
      uniprot "NA"
    ]
    graphics [
      x 1237.9673850663708
      y 1569.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M117_50"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re63"
      uniprot "NA"
    ]
    graphics [
      x 2348.85366504139
      y 613.2552915907766
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30413"
      hgnc "NA"
      map_id "M117_72"
      name "Heme"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa152"
      uniprot "NA"
    ]
    graphics [
      x 2612.990744812699
      y 834.4149783853799
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15379"
      hgnc "NA"
      map_id "M117_74"
      name "O2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa154"
      uniprot "NA"
    ]
    graphics [
      x 2168.85366504139
      y 671.4844839994439
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16474"
      hgnc "NA"
      map_id "M117_73"
      name "NADPH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa153"
      uniprot "NA"
    ]
    graphics [
      x 1962.3286346647717
      y 443.5808632544606
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:ensembl:ENSG00000100292;urn:miriam:hgnc:5013;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:refseq:NM_002133;urn:miriam:uniprot:P09601;urn:miriam:ncbigene:3162;urn:miriam:ncbigene:3162;urn:miriam:ec-code:1.14.14.18"
      hgnc "HGNC_SYMBOL:HMOX1"
      map_id "M117_80"
      name "HMOX1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa161"
      uniprot "UNIPROT:P09601"
    ]
    graphics [
      x 2651.476070226575
      y 1228.2314171152118
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17033"
      hgnc "NA"
      map_id "M117_76"
      name "Biliverdin"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa156"
      uniprot "NA"
    ]
    graphics [
      x 1517.2023363703734
      y 912.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "M117_77"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa157"
      uniprot "NA"
    ]
    graphics [
      x 1448.8536650413898
      y 732.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18009"
      hgnc "NA"
      map_id "M117_75"
      name "NADP_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa155"
      uniprot "NA"
    ]
    graphics [
      x 1988.8536650413898
      y 878.5267273646541
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:NLRP3 inflammasome activation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29033"
      hgnc "NA"
      map_id "M117_82"
      name "Fe2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa163"
      uniprot "NA"
    ]
    graphics [
      x 2441.476070226575
      y 1046.600017157592
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M117_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 114
    source 2
    target 1
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_41"
      target_id "M117_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 1
    target 3
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_8"
      target_id "M117_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 15
    target 2
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_10"
      target_id "M117_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 18
    target 2
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "UNKNOWN_CATALYSIS"
      source_id "M117_7"
      target_id "M117_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 3
    target 4
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_40"
      target_id "M117_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 3
    target 5
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_40"
      target_id "M117_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 3
    target 6
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_40"
      target_id "M117_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 4
    target 19
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_58"
      target_id "M117_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 5
    target 14
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_113"
      target_id "M117_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 5
    target 11
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_113"
      target_id "M117_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 5
    target 16
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_113"
      target_id "M117_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 6
    target 7
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_57"
      target_id "M117_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 126
    source 6
    target 8
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_57"
      target_id "M117_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 127
    source 12
    target 7
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_59"
      target_id "M117_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 128
    source 7
    target 13
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_38"
      target_id "M117_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 9
    target 8
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "M117_84"
      target_id "M117_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 130
    source 8
    target 10
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_52"
      target_id "M117_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 131
    source 9
    target 11
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "M117_84"
      target_id "M117_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 132
    source 11
    target 10
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_53"
      target_id "M117_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 133
    source 13
    target 14
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_9"
      target_id "M117_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 134
    source 14
    target 15
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_39"
      target_id "M117_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 135
    source 17
    target 16
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_97"
      target_id "M117_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 136
    source 16
    target 18
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_32"
      target_id "M117_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 137
    source 20
    target 19
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_56"
      target_id "M117_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 138
    source 19
    target 21
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_34"
      target_id "M117_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 139
    source 72
    target 20
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_37"
      target_id "M117_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 22
    target 21
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_18"
      target_id "M117_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 21
    target 23
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_3"
      target_id "M117_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 25
    target 22
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_2"
      target_id "M117_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 26
    target 22
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "M117_96"
      target_id "M117_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 144
    source 22
    target 27
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_18"
      target_id "M117_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 145
    source 22
    target 28
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_18"
      target_id "M117_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 146
    source 22
    target 29
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_18"
      target_id "M117_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 147
    source 22
    target 30
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_18"
      target_id "M117_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 148
    source 24
    target 23
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_55"
      target_id "M117_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 149
    source 23
    target 25
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_26"
      target_id "M117_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 150
    source 27
    target 31
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_85"
      target_id "M117_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 151
    source 30
    target 31
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_86"
      target_id "M117_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 152
    source 31
    target 32
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_20"
      target_id "M117_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 153
    source 32
    target 33
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_16"
      target_id "M117_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 154
    source 33
    target 34
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_21"
      target_id "M117_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 155
    source 34
    target 35
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "M117_14"
      target_id "M117_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 156
    source 34
    target 36
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "M117_14"
      target_id "M117_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 157
    source 52
    target 35
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_62"
      target_id "M117_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 158
    source 35
    target 68
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_24"
      target_id "M117_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 159
    source 35
    target 69
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_24"
      target_id "M117_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 160
    source 37
    target 36
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_70"
      target_id "M117_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 161
    source 36
    target 38
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_22"
      target_id "M117_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 162
    source 36
    target 39
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_22"
      target_id "M117_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 163
    source 42
    target 37
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_43"
      target_id "M117_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 164
    source 38
    target 40
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_87"
      target_id "M117_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 165
    source 40
    target 41
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_23"
      target_id "M117_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 166
    source 43
    target 42
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_110"
      target_id "M117_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 167
    source 44
    target 43
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_28"
      target_id "M117_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 168
    source 45
    target 44
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_112"
      target_id "M117_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 169
    source 46
    target 44
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "M117_6"
      target_id "M117_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 170
    source 47
    target 46
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_36"
      target_id "M117_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 171
    source 46
    target 48
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "M117_6"
      target_id "M117_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 172
    source 53
    target 47
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_5"
      target_id "M117_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 173
    source 54
    target 47
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "INHIBITION"
      source_id "M117_4"
      target_id "M117_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 174
    source 49
    target 48
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_111"
      target_id "M117_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 175
    source 48
    target 50
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_29"
      target_id "M117_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 176
    source 50
    target 51
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_109"
      target_id "M117_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 177
    source 51
    target 52
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_44"
      target_id "M117_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 178
    source 57
    target 53
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_31"
      target_id "M117_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 179
    source 53
    target 55
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_5"
      target_id "M117_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 180
    source 55
    target 54
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_35"
      target_id "M117_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 181
    source 56
    target 55
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_102"
      target_id "M117_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 182
    source 58
    target 57
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_103"
      target_id "M117_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 59
    target 57
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_105"
      target_id "M117_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 60
    target 58
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_33"
      target_id "M117_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 61
    target 60
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_104"
      target_id "M117_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 62
    target 61
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_49"
      target_id "M117_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 63
    target 62
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_71"
      target_id "M117_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 64
    target 62
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "UNKNOWN_CATALYSIS"
      source_id "M117_12"
      target_id "M117_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 65
    target 64
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_48"
      target_id "M117_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 66
    target 65
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_68"
      target_id "M117_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 67
    target 65
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_69"
      target_id "M117_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 68
    target 70
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_79"
      target_id "M117_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 70
    target 71
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_25"
      target_id "M117_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 73
    target 72
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_15"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 74
    target 72
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M117_107"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 75
    target 72
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M117_106"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 76
    target 72
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M117_60"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 77
    target 72
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M117_61"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 78
    target 72
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M117_54"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 79
    target 72
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M117_11"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 80
    target 72
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M117_65"
      target_id "M117_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 72
    target 81
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_37"
      target_id "M117_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 83
    target 73
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_42"
      target_id "M117_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 102
    target 78
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_51"
      target_id "M117_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 85
    target 79
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_46"
      target_id "M117_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 96
    target 80
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_45"
      target_id "M117_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 97
    target 80
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_47"
      target_id "M117_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 82
    target 81
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_30"
      target_id "M117_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 81
    target 83
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_13"
      target_id "M117_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 94
    target 82
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_98"
      target_id "M117_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 95
    target 82
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_99"
      target_id "M117_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 84
    target 83
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_108"
      target_id "M117_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 84
    target 85
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_108"
      target_id "M117_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 86
    target 85
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_101"
      target_id "M117_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 87
    target 86
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_17"
      target_id "M117_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 86
    target 88
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_101"
      target_id "M117_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 90
    target 87
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_1"
      target_id "M117_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 87
    target 93
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_17"
      target_id "M117_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 89
    target 88
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_100"
      target_id "M117_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 88
    target 90
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_27"
      target_id "M117_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 89
    target 91
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_100"
      target_id "M117_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 92
    target 91
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "TRIGGER"
      source_id "M117_93"
      target_id "M117_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 91
    target 93
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_19"
      target_id "M117_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 100
    target 96
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_64"
      target_id "M117_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 101
    target 96
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "M117_63"
      target_id "M117_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 98
    target 97
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_66"
      target_id "M117_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 99
    target 97
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "M117_67"
      target_id "M117_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 103
    target 102
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_81"
      target_id "M117_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 104
    target 102
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "INHIBITION"
      source_id "M117_78"
      target_id "M117_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 105
    target 104
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_50"
      target_id "M117_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 106
    target 105
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_72"
      target_id "M117_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 107
    target 105
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_74"
      target_id "M117_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 108
    target 105
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CONSPUMPTION"
      source_id "M117_73"
      target_id "M117_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 109
    target 105
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "CATALYSIS"
      source_id "M117_80"
      target_id "M117_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 105
    target 110
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_50"
      target_id "M117_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 105
    target 111
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_50"
      target_id "M117_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 105
    target 112
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_50"
      target_id "M117_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 105
    target 113
    cd19dm [
      diagram "C19DMap:NLRP3 inflammasome activation"
      edge_type "PRODUCTION"
      source_id "M117_50"
      target_id "M117_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
