# generated with VANTED V2.8.2 at Fri Mar 04 10:04:38 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:60961;urn:miriam:obo.chebi:CHEBI%3A16335"
      hgnc "NA"
      map_id "M112_329"
      name "Adenosine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa333"
      uniprot "NA"
    ]
    graphics [
      x 1251.7194709906853
      y 402.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_329"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:16746659"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_79"
      name "adenosine 5'-monophosphate phosphohydrolase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re82"
      uniprot "NA"
    ]
    graphics [
      x 2051.476070226575
      y 1052.4690038303006
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "PUBMED:13405917"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_80"
      name "adenosine:phosphate alpha-D-ribosyltransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re83"
      uniprot "NA"
    ]
    graphics [
      x 1576.3502838742145
      y 252.06502830998443
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "PUBMED:14832298"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_78"
      name "Adenosine kinase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re81"
      uniprot "NA"
    ]
    graphics [
      x 1436.5256443239937
      y 522.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "PUBMED:14927650"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_77"
      name "Adenosine aminohydrolase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re80"
      uniprot "NA"
    ]
    graphics [
      x 2121.6104592494066
      y 1743.7316608476026
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962"
      hgnc "NA"
      map_id "M112_331"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa335"
      uniprot "NA"
    ]
    graphics [
      x 1961.4760702265748
      y 1307.606843746084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_331"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24636;urn:miriam:pubchem.compound:1038"
      hgnc "NA"
      map_id "M112_332"
      name "H"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa336"
      uniprot "NA"
    ]
    graphics [
      x 2242.5144998161113
      y 1925.6755993299353
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_332"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:refseq:NM_000022;urn:miriam:ensembl:ENSG00000196839;urn:miriam:hgnc.symbol:ADA;urn:miriam:hgnc.symbol:ADA;urn:miriam:ec-code:3.5.4.4;urn:miriam:hgnc:186;urn:miriam:ncbigene:100;urn:miriam:ncbigene:100;urn:miriam:uniprot:P00813;urn:miriam:uniprot:P00813"
      hgnc "HGNC_SYMBOL:ADA"
      map_id "M112_333"
      name "ADA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa337"
      uniprot "UNIPROT:P00813"
    ]
    graphics [
      x 1961.4760702265748
      y 1397.606843746084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_333"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:35398641;urn:miriam:obo.chebi:CHEBI%3A17596"
      hgnc "NA"
      map_id "M112_308"
      name "Inosine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa313"
      uniprot "NA"
    ]
    graphics [
      x 1147.9673850663708
      y 1522.7078987280793
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_308"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:223;urn:miriam:obo.chebi:CHEBI%3A28938"
      hgnc "NA"
      map_id "M112_330"
      name "Ammonium"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa334"
      uniprot "NA"
    ]
    graphics [
      x 2002.598890022713
      y 2101.134391322975
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_330"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "PUBMED:14444527"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_71"
      name "inosine 5'-monophosphate phosphohydrolase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re73"
      uniprot "NA"
    ]
    graphics [
      x 757.9673850663706
      y 1712.1429123880987
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "PUBMED:5768862"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_72"
      name "inosine:phosphate alpha-D-ribosyltransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re74"
      uniprot "NA"
    ]
    graphics [
      x 1259.2571183404593
      y 2022.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:1061;urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "M112_314"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa319"
      uniprot "NA"
    ]
    graphics [
      x 1897.9673850663708
      y 1630.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_314"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ensembl:ENSG00000198805;urn:miriam:hgnc.symbol:PNP;urn:miriam:hgnc.symbol:PNP;urn:miriam:ncbigene:4860;urn:miriam:ncbigene:4860;urn:miriam:ec-code:2.4.2.1;urn:miriam:refseq:NM_000270.2;urn:miriam:hgnc:7892;urn:miriam:uniprot:P00491;urn:miriam:uniprot:P00491"
      hgnc "HGNC_SYMBOL:PNP"
      map_id "M112_315"
      name "PNP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa320"
      uniprot "UNIPROT:P00491"
    ]
    graphics [
      x 1432.5144998161113
      y 2052.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_315"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:135398638;urn:miriam:obo.chebi:CHEBI%3A17368"
      hgnc "NA"
      map_id "M112_312"
      name "Hypoxanthine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa317"
      uniprot "NA"
    ]
    graphics [
      x 1147.9673850663708
      y 1612.7078987280793
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_312"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:439236;urn:miriam:obo.chebi:CHEBI%3A16300"
      hgnc "NA"
      map_id "M112_313"
      name "_alpha__minus_D_minus_Ribose_space_1_minus_phosphate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa318"
      uniprot "NA"
    ]
    graphics [
      x 1028.8536650413898
      y 775.5448252604996
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_313"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "PUBMED:13405917"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_83"
      name "Deoxyinosine:orthophosphate ribosyltransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re86"
      uniprot "NA"
    ]
    graphics [
      x 1672.5144998161113
      y 1878.1917700814608
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "PUBMED:18569334"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_73"
      name "inosine:phosphate alpha-D-ribosyltransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re75"
      uniprot "NA"
    ]
    graphics [
      x 1028.8536650413898
      y 623.2055662693324
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "PUBMED:14392175"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_75"
      name "hypoxanthine-guanine phosphoribosyltransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re78"
      uniprot "NA"
    ]
    graphics [
      x 1761.6104592494069
      y 1795.9277406352314
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17111;urn:miriam:pubchem.compound:7339"
      hgnc "NA"
      map_id "M112_326"
      name "5_minus_phospho_minus__alpha__minus_D_minus_ribose_space_1_minus_diphosphate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa330"
      uniprot "NA"
    ]
    graphics [
      x 1852.5144998161113
      y 1998.108707658523
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_326"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc:5157;urn:miriam:hgnc.symbol:HPRT1;urn:miriam:hgnc.symbol:HPRT1;urn:miriam:ec-code:2.4.2.8;urn:miriam:refseq:NM_000194;urn:miriam:ncbigene:3251;urn:miriam:ncbigene:3251;urn:miriam:ensembl:ENSG00000165704;urn:miriam:uniprot:P00492;urn:miriam:uniprot:P00492"
      hgnc "HGNC_SYMBOL:HPRT1"
      map_id "M112_328"
      name "HPRT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa332"
      uniprot "UNIPROT:P00492"
    ]
    graphics [
      x 1700.0074341427699
      y 2347.073204317262
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_328"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:135398640;urn:miriam:obo.chebi:CHEBI%3A17202"
      hgnc "NA"
      map_id "M112_195"
      name "IMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa202"
      uniprot "NA"
    ]
    graphics [
      x 2467.9673850663708
      y 1780.2835095222708
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_195"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:644102;urn:miriam:obo.chebi:CHEBI%3A18361"
      hgnc "NA"
      map_id "M112_327"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa331"
      uniprot "NA"
    ]
    graphics [
      x 1474.6943393561805
      y 1152.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_327"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "PUBMED:13463019"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_68"
      name "AMP deaminase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re70"
      uniprot "NA"
    ]
    graphics [
      x 2561.476070226575
      y 1378.3897028750896
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "PUBMED:13502325"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_49"
      name "IMP cyclohydrolase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re50"
      uniprot "NA"
    ]
    graphics [
      x 2347.9673850663708
      y 1618.4429995909788
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_66"
      name "GMP reductase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re69"
      uniprot "NA"
    ]
    graphics [
      x 1946.090149807047
      y 503.5808632544606
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "PUBMED:5799033;PUBMED:22555152"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_50"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re52"
      uniprot "NA"
    ]
    graphics [
      x 1990.4146427238684
      y 2237.606843746084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:5892;urn:miriam:obo.chebi:CHEBI%3A15846"
      hgnc "NA"
      map_id "M112_198"
      name "NAD"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa205"
      uniprot "NA"
    ]
    graphics [
      x 1790.0074341427699
      y 2325.6295995785204
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_198"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962"
      hgnc "NA"
      map_id "M112_201"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa208"
      uniprot "NA"
    ]
    graphics [
      x 1732.5144998161113
      y 1945.9277406352314
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_201"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ensembl:ENSG00000106348;urn:miriam:ncbigene:3614;urn:miriam:refseq:NM_000883;urn:miriam:ncbigene:3614;urn:miriam:hgnc.symbol:IMPDH1;urn:miriam:hgnc.symbol:IMPDH1;urn:miriam:ec-code:1.1.1.205;urn:miriam:hgnc:6052;urn:miriam:uniprot:P20839;urn:miriam:uniprot:P20839"
      hgnc "HGNC_SYMBOL:IMPDH1"
      map_id "M112_204"
      name "IMPDH1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa210"
      uniprot "UNIPROT:P20839"
    ]
    graphics [
      x 1642.5144998161113
      y 2052.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_204"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ncbigene:3615;urn:miriam:ncbigene:3615;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:ec-code:1.1.1.205;urn:miriam:uniprot:P12268;urn:miriam:uniprot:P12268;urn:miriam:refseq:NM_000884;urn:miriam:hgnc:6053;urn:miriam:ensembl:ENSG00000178035"
      hgnc "HGNC_SYMBOL:IMPDH2"
      map_id "M112_202"
      name "IMPDH2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa209"
      uniprot "UNIPROT:P12268"
    ]
    graphics [
      x 1365.8453706378386
      y 2412.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_202"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:doi:10.1101/2020.03.22.002386;urn:miriam:ncbiprotein:YP_009725309;urn:miriam:ncbigene:3615;urn:miriam:ncbigene:3615;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:ec-code:1.1.1.205;urn:miriam:uniprot:P12268;urn:miriam:uniprot:P12268;urn:miriam:refseq:NM_000884;urn:miriam:hgnc:6053;urn:miriam:ensembl:ENSG00000178035"
      hgnc "HGNC_SYMBOL:IMPDH2"
      map_id "M112_8"
      name "IMPDH2:Nsp14"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa8"
      uniprot "UNIPROT:P12268"
    ]
    graphics [
      x 1301.4760702265748
      y 1252.3184295819842
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubmed:5799033;urn:miriam:pubchem.compound:446541;urn:miriam:obo.chebi:CHEBI%3A168396"
      hgnc "NA"
      map_id "M112_206"
      name "Mycophenolic_space_acid"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa213"
      uniprot "NA"
    ]
    graphics [
      x 2062.5144998161113
      y 1887.72297262025
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_206"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:53241;urn:miriam:pubmed:10878288"
      hgnc "NA"
      map_id "M112_205"
      name "Merimepodib"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa212"
      uniprot "NA"
    ]
    graphics [
      x 2347.748789420236
      y 2278.044068262526
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_205"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A63580;urn:miriam:pubmed:22555152;urn:miriam:pubchem.compound:37542"
      hgnc "NA"
      map_id "M112_207"
      name "Ribavirin"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa214"
      uniprot "NA"
    ]
    graphics [
      x 1732.113487126412
      y 2466.313882011814
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_207"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15652;urn:miriam:pubchem.compound:73323"
      hgnc "NA"
      map_id "M112_197"
      name "XMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa204"
      uniprot "NA"
    ]
    graphics [
      x 1211.4760702265748
      y 1449.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_197"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16908;urn:miriam:pubchem.compound:439153"
      hgnc "NA"
      map_id "M112_200"
      name "NADH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa207"
      uniprot "NA"
    ]
    graphics [
      x 1912.5144998161113
      y 1870.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_200"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24636;urn:miriam:pubchem.compound:1038"
      hgnc "NA"
      map_id "M112_199"
      name "H"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa206"
      uniprot "NA"
    ]
    graphics [
      x 1327.9673850663708
      y 1542.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_199"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "PUBMED:13563458"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_51"
      name "GMP synthase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re53"
      uniprot "NA"
    ]
    graphics [
      x 1988.8536650413898
      y 731.6792194345431
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "PUBMED:16746659"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_69"
      name "XMP 5'-nucleotidase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re71"
      uniprot "NA"
    ]
    graphics [
      x 611.4760702265747
      y 1172.0203769945765
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962"
      hgnc "NA"
      map_id "M112_212"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa219"
      uniprot "NA"
    ]
    graphics [
      x 1791.6104592494069
      y 1723.8597507391748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_212"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ec-code:3.1.3.5;urn:miriam:uniprot:P21589;urn:miriam:uniprot:P21589;urn:miriam:pubmed:2848759;urn:miriam:hgnc:8021;urn:miriam:hgnc.symbol:NT5E;urn:miriam:ncbigene:4907;urn:miriam:hgnc.symbol:NT5E;urn:miriam:ncbigene:4907;urn:miriam:ensembl:ENSG00000135318;urn:miriam:refseq:NM_001204813"
      hgnc "HGNC_SYMBOL:NT5E"
      map_id "M112_267"
      name "NT5E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa275"
      uniprot "UNIPROT:P21589"
    ]
    graphics [
      x 578.8536650413897
      y 816.68758464586
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_267"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:64959;urn:miriam:obo.chebi:CHEBI%3A18107"
      hgnc "NA"
      map_id "M112_303"
      name "Xanthosine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa308"
      uniprot "NA"
    ]
    graphics [
      x 1031.4760702265748
      y 1244.9982948830293
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_303"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:1061;urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "M112_306"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa311"
      uniprot "NA"
    ]
    graphics [
      x 1061.4760702265748
      y 1413.6936536421204
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_306"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "PUBMED:13405917"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_70"
      name "Xanthosine phosphorlyase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re72"
      uniprot "NA"
    ]
    graphics [
      x 2107.9673850663708
      y 1694.4135856362425
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:1061;urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "M112_307"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa312"
      uniprot "NA"
    ]
    graphics [
      x 2171.476070226575
      y 1068.022409089093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_307"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:1188;urn:miriam:obo.chebi:CHEBI%3A15318"
      hgnc "NA"
      map_id "M112_304"
      name "Xanthine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa309"
      uniprot "NA"
    ]
    graphics [
      x 997.9673850663706
      y 1528.6039843924614
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_304"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:439236;urn:miriam:obo.chebi:CHEBI%3A16300"
      hgnc "NA"
      map_id "M112_264"
      name "_alpha__minus_D_minus_Ribose_space_1_minus_phosphate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa271"
      uniprot "NA"
    ]
    graphics [
      x 2356.521593033102
      y 2308.044068262526
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_264"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "PUBMED:5768862"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_61"
      name "guanosine:phosphate alpha-D-ribosyltransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re64"
      uniprot "NA"
    ]
    graphics [
      x 2362.5144998161113
      y 2046.8557137111584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:135398635;urn:miriam:obo.chebi:CHEBI%3A16750"
      hgnc "NA"
      map_id "M112_248"
      name "Guanosine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa257"
      uniprot "NA"
    ]
    graphics [
      x 2227.9673850663708
      y 1515.6982431349254
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_248"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:1061;urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "M112_263"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa270"
      uniprot "NA"
    ]
    graphics [
      x 2437.9673850663708
      y 1623.474901332917
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_263"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ensembl:ENSG00000198805;urn:miriam:hgnc.symbol:PNP;urn:miriam:hgnc.symbol:PNP;urn:miriam:ncbigene:4860;urn:miriam:ncbigene:4860;urn:miriam:ec-code:2.4.2.1;urn:miriam:refseq:NM_000270.2;urn:miriam:hgnc:7892;urn:miriam:uniprot:P00491;urn:miriam:uniprot:P00491"
      hgnc "HGNC_SYMBOL:PNP"
      map_id "M112_249"
      name "PNP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa258"
      uniprot "UNIPROT:P00491"
    ]
    graphics [
      x 1882.5144998161113
      y 1900.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_249"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:135398634;urn:miriam:obo.chebi:CHEBI%3A16235"
      hgnc "NA"
      map_id "M112_247"
      name "Guanine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa256"
      uniprot "NA"
    ]
    graphics [
      x 2080.4146427238684
      y 2123.3608844567657
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_247"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      annotation "PUBMED:5768862"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_58"
      name "Deoxyguanosine phosphorylase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re61"
      uniprot "NA"
    ]
    graphics [
      x 561.6104592494069
      y 1806.24509784493
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "PUBMED:16578130"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_74"
      name "Guanine deaminase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re77"
      uniprot "NA"
    ]
    graphics [
      x 1432.5144998161113
      y 1992.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "PUBMED:14392175"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_60"
      name "GMP:diphosphate 5-phospho-alpha-D-ribosyltransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re63"
      uniprot "NA"
    ]
    graphics [
      x 2572.5144998161113
      y 1800.9129755444785
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17111;urn:miriam:pubchem.compound:7339"
      hgnc "NA"
      map_id "M112_261"
      name "5_minus_phospho_minus__alpha__minus_D_minus_ribose_space_1_minus_diphosphate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa269"
      uniprot "NA"
    ]
    graphics [
      x 2662.3451507428053
      y 2124.2917182872307
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_261"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc:5157;urn:miriam:hgnc.symbol:HPRT1;urn:miriam:hgnc.symbol:HPRT1;urn:miriam:ec-code:2.4.2.8;urn:miriam:refseq:NM_000194;urn:miriam:ncbigene:3251;urn:miriam:ncbigene:3251;urn:miriam:ensembl:ENSG00000165704;urn:miriam:uniprot:P00492;urn:miriam:uniprot:P00492"
      hgnc "HGNC_SYMBOL:HPRT1"
      map_id "M112_259"
      name "HPRT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa267"
      uniprot "UNIPROT:P00492"
    ]
    graphics [
      x 2825.802106721339
      y 1983.4367587652828
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_259"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:135398631;urn:miriam:obo.chebi:CHEBI%3A17345"
      hgnc "NA"
      map_id "M112_208"
      name "GMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa215"
      uniprot "NA"
    ]
    graphics [
      x 2078.85366504139
      y 956.0221773185248
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_208"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:644102;urn:miriam:obo.chebi:CHEBI%3A18361"
      hgnc "NA"
      map_id "M112_260"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa268"
      uniprot "NA"
    ]
    graphics [
      x 1732.5144998161113
      y 1986.8884998692427
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_260"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      annotation "PUBMED:14953432"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_64"
      name "GTP diphosphohydrolase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re67"
      uniprot "NA"
    ]
    graphics [
      x 2048.85366504139
      y 820.1506240017071
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_65"
      name "GDP phosphohydrolase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re68"
      uniprot "NA"
    ]
    graphics [
      x 698.8536650413897
      y 727.2037470914684
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "PUBMED:4307347"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_52"
      name "Guanylate kinase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re54"
      uniprot "NA"
    ]
    graphics [
      x 2048.85366504139
      y 590.8365495134138
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "PUBMED:16746659"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_62"
      name "guanosine 5'-monophosphate phosphohydrolase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re65"
      uniprot "NA"
    ]
    graphics [
      x 701.4760702265747
      y 1452.8450205202755
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962"
      hgnc "NA"
      map_id "M112_265"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa272"
      uniprot "NA"
    ]
    graphics [
      x 608.8536650413897
      y 939.8351640363044
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_265"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:1061;urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "M112_266"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa273"
      uniprot "NA"
    ]
    graphics [
      x 401.47607022657485
      y 1202.5572932182372
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_266"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15422;urn:miriam:pubchem.compound:5957"
      hgnc "NA"
      map_id "M112_221"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa227"
      uniprot "NA"
    ]
    graphics [
      x 2411.476070226575
      y 1428.0883274205173
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_221"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc:4693;urn:miriam:uniprot:Q16774;urn:miriam:uniprot:Q16774;urn:miriam:ncbigene:2987;urn:miriam:ncbigene:2987;urn:miriam:ensembl:ENSG00000143774;urn:miriam:hgnc.symbol:GUK1;urn:miriam:hgnc.symbol:GUK1;urn:miriam:pubmed:8663313;urn:miriam:refseq:NM_000858;urn:miriam:ec-code:2.7.4.8"
      hgnc "HGNC_SYMBOL:GUK1"
      map_id "M112_220"
      name "GUK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa226"
      uniprot "UNIPROT:Q16774"
    ]
    graphics [
      x 968.8536650413897
      y 823.8329489969877
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_220"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17552;urn:miriam:pubchem.compound:135398619"
      hgnc "NA"
      map_id "M112_219"
      name "GDP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa225"
      uniprot "NA"
    ]
    graphics [
      x 900.5481680392254
      y 467.86483358482894
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_219"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:6022;urn:miriam:obo.chebi:CHEBI%3A16761"
      hgnc "NA"
      map_id "M112_222"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa228"
      uniprot "NA"
    ]
    graphics [
      x 2167.9673850663708
      y 1713.5382961725277
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_222"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      annotation "PUBMED:17291528"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_63"
      name "GTP phosphohydrolase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re66"
      uniprot "NA"
    ]
    graphics [
      x 1472.112491134918
      y 792.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      annotation "PUBMED:4543472"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_54"
      name "GDP reductase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re56"
      uniprot "NA"
    ]
    graphics [
      x 1444.6943393561805
      y 1152.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      annotation "PUBMED:13363863"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_53"
      name "ATP:GDP phosphotransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re55"
      uniprot "NA"
    ]
    graphics [
      x 221.47607022657485
      y 1319.3362933908695
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15422;urn:miriam:pubchem.compound:5957"
      hgnc "NA"
      map_id "M112_224"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa230"
      uniprot "NA"
    ]
    graphics [
      x 834.0051592879013
      y 2293.853912881765
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_224"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ensembl:ENSG00000103024;urn:miriam:hgnc:7851;urn:miriam:hgnc.symbol:NME3;urn:miriam:hgnc.symbol:NME3;urn:miriam:uniprot:Q13232;urn:miriam:uniprot:Q13232;urn:miriam:ec-code:2.7.4.6;urn:miriam:refseq:NM_002513;urn:miriam:ncbigene:4832;urn:miriam:ncbigene:4832"
      hgnc "HGNC_SYMBOL:NME3"
      map_id "M112_226"
      name "NME3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa232"
      uniprot "UNIPROT:Q13232"
    ]
    graphics [
      x 1347.7231255590953
      y 312.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_226"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc:7850;urn:miriam:uniprot:P22392;urn:miriam:uniprot:P22392;urn:miriam:ec-code:2.7.4.6;urn:miriam:hgnc.symbol:NME2;urn:miriam:ncbigene:4831;urn:miriam:hgnc.symbol:NME2;urn:miriam:ncbigene:4831;urn:miriam:refseq:NM_002512;urn:miriam:ensembl:ENSG00000243678;urn:miriam:ec-code:2.7.13.3;urn:miriam:ensembl:ENSG00000239672;urn:miriam:uniprot:P15531;urn:miriam:uniprot:P15531;urn:miriam:ec-code:2.7.4.6;urn:miriam:hgnc:7849;urn:miriam:hgnc.symbol:NME1;urn:miriam:refseq:NM_000269;urn:miriam:hgnc.symbol:NME1;urn:miriam:ncbigene:4830;urn:miriam:ncbigene:4830"
      hgnc "HGNC_SYMBOL:NME2;HGNC_SYMBOL:NME1"
      map_id "M112_2"
      name "Nucleoside_space_diphosphate_space_kinase"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa2"
      uniprot "UNIPROT:P22392;UNIPROT:P15531"
    ]
    graphics [
      x 300.88852994949707
      y 873.1341968246808
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc:7853;urn:miriam:hgnc.symbol:NME5;urn:miriam:hgnc.symbol:NME5;urn:miriam:ncbigene:8382;urn:miriam:ncbigene:8382;urn:miriam:refseq:NM_003551;urn:miriam:ensembl:ENSG00000112981;urn:miriam:uniprot:P56597;urn:miriam:uniprot:P56597"
      hgnc "HGNC_SYMBOL:NME5"
      map_id "M112_227"
      name "NME5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa235"
      uniprot "UNIPROT:P56597"
    ]
    graphics [
      x 487.96738506637064
      y 1793.4974761108222
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_227"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc:20567;urn:miriam:hgnc.symbol:NME6;urn:miriam:hgnc.symbol:NME6;urn:miriam:refseq:NM_005793;urn:miriam:ec-code:2.7.4.6;urn:miriam:ensembl:ENSG00000172113;urn:miriam:ncbigene:10201;urn:miriam:ncbigene:10201;urn:miriam:uniprot:O75414;urn:miriam:uniprot:O75414"
      hgnc "HGNC_SYMBOL:NME6"
      map_id "M112_228"
      name "NME6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa236"
      uniprot "UNIPROT:O75414"
    ]
    graphics [
      x 386.27387135629397
      y 602.5572932182373
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_228"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ncbigene:29922;urn:miriam:ncbigene:29922;urn:miriam:ensembl:ENSG00000143156;urn:miriam:uniprot:Q9Y5B8;urn:miriam:uniprot:Q9Y5B8;urn:miriam:refseq:NM_013330;urn:miriam:hgnc.symbol:NME7;urn:miriam:hgnc.symbol:NME7;urn:miriam:ec-code:2.7.4.6;urn:miriam:hgnc:20461"
      hgnc "HGNC_SYMBOL:NME7"
      map_id "M112_229"
      name "NME7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa237"
      uniprot "UNIPROT:Q9Y5B8"
    ]
    graphics [
      x 353.18458603575687
      y 698.7815359946447
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_229"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:35398633;urn:miriam:obo.chebi:CHEBI%3A15996"
      hgnc "NA"
      map_id "M112_223"
      name "GTP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa229"
      uniprot "NA"
    ]
    graphics [
      x 938.8536650413897
      y 578.6952627314087
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_223"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:6022;urn:miriam:obo.chebi:CHEBI%3A16761"
      hgnc "NA"
      map_id "M112_225"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa231"
      uniprot "NA"
    ]
    graphics [
      x 1451.4760702265748
      y 1452.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_225"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      annotation "PUBMED:13363863"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_55"
      name "ATP:dGDP phosphotransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re57"
      uniprot "NA"
    ]
    graphics [
      x 781.8475789774769
      y 412.2816829287011
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A28862;urn:miriam:pubchem.compound:135398595"
      hgnc "NA"
      map_id "M112_230"
      name "dGDP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa238"
      uniprot "NA"
    ]
    graphics [
      x 877.9673850663706
      y 1673.2421714207785
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_230"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15422;urn:miriam:pubchem.compound:5957"
      hgnc "NA"
      map_id "M112_236"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa246"
      uniprot "NA"
    ]
    graphics [
      x 668.8536650413897
      y 825.0463435036824
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_236"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:135398599;urn:miriam:obo.chebi:CHEBI%3A16497"
      hgnc "NA"
      map_id "M112_235"
      name "dGTP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa245"
      uniprot "NA"
    ]
    graphics [
      x 458.8536650413897
      y 902.5572932182373
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_235"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:6022;urn:miriam:obo.chebi:CHEBI%3A16761"
      hgnc "NA"
      map_id "M112_237"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa247"
      uniprot "NA"
    ]
    graphics [
      x 638.8536650413897
      y 754.5590535716939
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_237"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      annotation "PUBMED:4310599"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_59"
      name "2'-Deoxyguanosine 5'-triphosphate diphosphohydrolase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re62"
      uniprot "NA"
    ]
    graphics [
      x 848.8536650413897
      y 1006.0055613274181
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962"
      hgnc "NA"
      map_id "M112_255"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa263"
      uniprot "NA"
    ]
    graphics [
      x 1456.6057727547209
      y 1002.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_255"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc:3356;urn:miriam:ec-code:3.1.4.1;urn:miriam:uniprot:P22413;urn:miriam:uniprot:P22413;urn:miriam:ec-code:3.6.1.9;urn:miriam:ncbigene:5167;urn:miriam:ncbigene:5167;urn:miriam:ensembl:ENSG00000197594;urn:miriam:hgnc.symbol:ENPP1;urn:miriam:refseq:NM_006208;urn:miriam:hgnc.symbol:ENPP1"
      hgnc "HGNC_SYMBOL:ENPP1"
      map_id "M112_254"
      name "ENPP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa262"
      uniprot "UNIPROT:P22413"
    ]
    graphics [
      x 1208.8536650413898
      y 789.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_254"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ec-code:3.1.4.1;urn:miriam:hgnc:3358;urn:miriam:ensembl:ENSG00000154269;urn:miriam:ec-code:3.6.1.9;urn:miriam:refseq:NM_005021;urn:miriam:ncbigene:5169;urn:miriam:uniprot:O14638;urn:miriam:uniprot:O14638;urn:miriam:ncbigene:5169;urn:miriam:hgnc.symbol:ENPP3;urn:miriam:hgnc.symbol:ENPP3"
      hgnc "HGNC_SYMBOL:ENPP3"
      map_id "M112_253"
      name "ENPP3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa261"
      uniprot "UNIPROT:O14638"
    ]
    graphics [
      x 951.6104592494069
      y 1786.6019746441584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_253"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ncbigene:3704;urn:miriam:ncbigene:3704;urn:miriam:refseq:NM_033453;urn:miriam:ec-code:3.6.1.9;urn:miriam:ensembl:ENSG00000125877;urn:miriam:hgnc:6176;urn:miriam:hgnc.symbol:ITPA;urn:miriam:hgnc.symbol:ITPA;urn:miriam:uniprot:Q9BY32;urn:miriam:uniprot:Q9BY32"
      hgnc "HGNC_SYMBOL:ITPA"
      map_id "M112_256"
      name "ITPA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa264"
      uniprot "UNIPROT:Q9BY32"
    ]
    graphics [
      x 741.6104592494069
      y 1832.1429123880987
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_256"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:135398597;urn:miriam:obo.chebi:CHEBI%3A16192"
      hgnc "NA"
      map_id "M112_238"
      name "dGMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa248"
      uniprot "NA"
    ]
    graphics [
      x 131.47607022657485
      y 1311.8557680946265
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_238"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:644102;urn:miriam:obo.chebi:CHEBI%3A18361"
      hgnc "NA"
      map_id "M112_257"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa265"
      uniprot "NA"
    ]
    graphics [
      x 1515.589727969152
      y 462.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_257"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24636;urn:miriam:pubchem.compound:1038"
      hgnc "NA"
      map_id "M112_258"
      name "H"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa266"
      uniprot "NA"
    ]
    graphics [
      x 547.9673850663706
      y 1634.6907010335744
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_258"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      annotation "PUBMED:6260206"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_57"
      name "ATP:deoxyguanosine 5'-phosphotransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re59"
      uniprot "NA"
    ]
    graphics [
      x 149.4634596539604
      y 1075.5021177433189
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      annotation "PUBMED:14253449"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_56"
      name "ATP:dGMP phosphotransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re58"
      uniprot "NA"
    ]
    graphics [
      x 638.8536650413897
      y 881.710136427816
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15422;urn:miriam:pubchem.compound:5957"
      hgnc "NA"
      map_id "M112_239"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa249"
      uniprot "NA"
    ]
    graphics [
      x 1781.4760702265748
      y 1261.3808368566754
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_239"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:6022;urn:miriam:obo.chebi:CHEBI%3A16761"
      hgnc "NA"
      map_id "M112_241"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa250"
      uniprot "NA"
    ]
    graphics [
      x 161.47607022657485
      y 1298.1673956667682
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_241"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17172;urn:miriam:pubchem.compound:135398592"
      hgnc "NA"
      map_id "M112_242"
      name "Deoxyguanosine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa251"
      uniprot "NA"
    ]
    graphics [
      x 230.9828941747544
      y 1039.8166073337368
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_242"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15422;urn:miriam:pubchem.compound:5957"
      hgnc "NA"
      map_id "M112_243"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa252"
      uniprot "NA"
    ]
    graphics [
      x 217.96738506637075
      y 1599.6219288495151
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_243"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc.symbol:DCK;urn:miriam:ncbigene:1633;urn:miriam:hgnc.symbol:DCK;urn:miriam:ncbigene:1633;urn:miriam:ensembl:ENSG00000156136;urn:miriam:ec-code:2.7.1.76;urn:miriam:ec-code:2.7.1.113;urn:miriam:ec-code:2.7.1.74;urn:miriam:hgnc:2704;urn:miriam:uniprot:P27707;urn:miriam:uniprot:P27707;urn:miriam:refseq:NM_000788"
      hgnc "HGNC_SYMBOL:DCK"
      map_id "M112_246"
      name "DCK"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa255"
      uniprot "UNIPROT:P27707"
    ]
    graphics [
      x 131.47607022657485
      y 1179.0596955473493
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_246"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:6022;urn:miriam:obo.chebi:CHEBI%3A16761"
      hgnc "NA"
      map_id "M112_244"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa253"
      uniprot "NA"
    ]
    graphics [
      x 517.9673850663706
      y 1565.261006004971
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_244"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24636;urn:miriam:pubchem.compound:1038"
      hgnc "NA"
      map_id "M112_245"
      name "H"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa254"
      uniprot "NA"
    ]
    graphics [
      x 471.5874863892151
      y 692.5572932182373
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_245"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.substance:223441017;urn:miriam:pubchem.substance:3635;urn:miriam:obo.chebi:CHEBI%3A15033"
      hgnc "NA"
      map_id "M112_233"
      name "Thioredoxin"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa242"
      uniprot "NA"
    ]
    graphics [
      x 1898.8536650413898
      y 774.1331209245378
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_233"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:refseq:NM_001034;urn:miriam:ncbigene:6241;urn:miriam:ncbigene:6241;urn:miriam:hgnc:10452;urn:miriam:ec-code:1.17.4.1;urn:miriam:hgnc.symbol:RRM2;urn:miriam:hgnc.symbol:RRM2;urn:miriam:ensembl:ENSG00000171848;urn:miriam:uniprot:P31350;urn:miriam:uniprot:P31350;urn:miriam:ensembl:ENSG00000167325;urn:miriam:hgnc.symbol:RRM1;urn:miriam:uniprot:P23921;urn:miriam:uniprot:P23921;urn:miriam:hgnc.symbol:RRM1;urn:miriam:ncbigene:6240;urn:miriam:refseq:NM_001033;urn:miriam:ncbigene:6240;urn:miriam:hgnc:10451;urn:miriam:ec-code:1.17.4.1;urn:miriam:ncbigene:50484;urn:miriam:ncbigene:50484;urn:miriam:ensembl:ENSG00000048392;urn:miriam:hgnc.symbol:RRM2B;urn:miriam:hgnc.symbol:RRM2B;urn:miriam:uniprot:Q7LG56;urn:miriam:uniprot:Q7LG56;urn:miriam:hgnc:17296;urn:miriam:ec-code:1.17.4.1;urn:miriam:refseq:NM_001172477"
      hgnc "HGNC_SYMBOL:RRM2;HGNC_SYMBOL:RRM1;HGNC_SYMBOL:RRM2B"
      map_id "M112_3"
      name "ribonucleoside_space_reductase"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa3"
      uniprot "UNIPROT:P31350;UNIPROT:P23921;UNIPROT:Q7LG56"
    ]
    graphics [
      x 1358.8536650413898
      y 672.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18191;urn:miriam:pubchem.substance:11533266;urn:miriam:pubchem.substance:3636"
      hgnc "NA"
      map_id "M112_232"
      name "Thioredoxin_space_disulfide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa241"
      uniprot "NA"
    ]
    graphics [
      x 2051.476070226575
      y 1258.7306646226543
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_232"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962"
      hgnc "NA"
      map_id "M112_234"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa243"
      uniprot "NA"
    ]
    graphics [
      x 1480.4146427238684
      y 2262.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_234"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962"
      hgnc "NA"
      map_id "M112_276"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa284"
      uniprot "NA"
    ]
    graphics [
      x 1001.4760702265747
      y 1168.6039843924614
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_276"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ncbigene:954;urn:miriam:ncbigene:954;urn:miriam:hgnc:3364;urn:miriam:ec-code:3.6.1.-;urn:miriam:uniprot:Q9Y5L3;urn:miriam:uniprot:Q9Y5L3;urn:miriam:hgnc.symbol:ENTPD2;urn:miriam:hgnc.symbol:ENTPD2;urn:miriam:refseq:NM_203468;urn:miriam:ensembl:ENSG00000054179"
      hgnc "HGNC_SYMBOL:ENTPD2"
      map_id "M112_268"
      name "ENTPD2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa277"
      uniprot "UNIPROT:Q9Y5L3"
    ]
    graphics [
      x 1881.6104592494069
      y 1731.145821463369
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_268"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc:28204;urn:miriam:refseq:NM_032324;urn:miriam:ec-code:3.6.1.15;urn:miriam:uniprot:Q9BSD7;urn:miriam:uniprot:Q9BSD7;urn:miriam:ensembl:ENSG00000135778;urn:miriam:hgnc.symbol:NTPCR;urn:miriam:hgnc.symbol:NTPCR;urn:miriam:ncbigene:84284;urn:miriam:ncbigene:84284"
      hgnc "HGNC_SYMBOL:NTPCR"
      map_id "M112_275"
      name "NTPCR"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa283"
      uniprot "UNIPROT:Q9BSD7"
    ]
    graphics [
      x 2111.476070226575
      y 1097.7871053251297
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_275"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24636;urn:miriam:pubchem.compound:1038"
      hgnc "NA"
      map_id "M112_278"
      name "H"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa286"
      uniprot "NA"
    ]
    graphics [
      x 1778.8536650413898
      y 646.8134512202714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_278"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:1061;urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "M112_277"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa285"
      uniprot "NA"
    ]
    graphics [
      x 1886.090149807047
      y 537.7524189556523
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_277"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962"
      hgnc "NA"
      map_id "M112_279"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa287"
      uniprot "NA"
    ]
    graphics [
      x 637.9673850663706
      y 1752.1662202339423
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_279"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc:14573;urn:miriam:ec-code:3.6.1.6;urn:miriam:ensembl:ENSG00000197217;urn:miriam:hgnc.symbol:ENTPD4;urn:miriam:hgnc.symbol:ENTPD4;urn:miriam:ec-code:3.6.1.15;urn:miriam:ncbigene:9583;urn:miriam:ncbigene:9583;urn:miriam:ec-code:3.6.1.42;urn:miriam:uniprot:Q9Y227;urn:miriam:uniprot:Q9Y227;urn:miriam:refseq:NM_004901"
      hgnc "HGNC_SYMBOL:ENTPD4"
      map_id "M112_283"
      name "ENTPD4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa290"
      uniprot "UNIPROT:Q9Y227"
    ]
    graphics [
      x 191.47607022657485
      y 1167.4254647726366
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_283"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:uniprot:O75356;urn:miriam:uniprot:O75356;urn:miriam:ensembl:ENSG00000187097;urn:miriam:ncbigene:957;urn:miriam:ncbigene:957;urn:miriam:hgnc:3367;urn:miriam:ec-code:3.6.1.6;urn:miriam:hgnc.symbol:ENTPD5;urn:miriam:hgnc.symbol:ENTPD5;urn:miriam:ec-code:3.6.1.42;urn:miriam:refseq:NM_001249"
      hgnc "HGNC_SYMBOL:ENTPD5"
      map_id "M112_284"
      name "ENTPD5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa291"
      uniprot "UNIPROT:O75356"
    ]
    graphics [
      x 728.8536650413897
      y 853.7285167359257
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_284"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ncbigene:955;urn:miriam:ncbigene:955;urn:miriam:uniprot:O75354;urn:miriam:uniprot:O75354;urn:miriam:hgnc.symbol:ENTPD6;urn:miriam:hgnc.symbol:ENTPD6;urn:miriam:ec-code:3.6.1.6;urn:miriam:hgnc:3368;urn:miriam:refseq:NM_001114089;urn:miriam:ensembl:ENSG00000197586"
      hgnc "HGNC_SYMBOL:ENTPD6"
      map_id "M112_285"
      name "ENTPD6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa292"
      uniprot "UNIPROT:O75354"
    ]
    graphics [
      x 848.8536650413897
      y 826.0055613274181
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_285"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc:19721;urn:miriam:hgnc.symbol:CANT1;urn:miriam:hgnc.symbol:CANT1;urn:miriam:ncbigene:124583;urn:miriam:ncbigene:124583;urn:miriam:ec-code:3.6.1.6;urn:miriam:ensembl:ENSG00000171302;urn:miriam:refseq:NM_138793;urn:miriam:uniprot:Q8WVQ1;urn:miriam:uniprot:Q8WVQ1"
      hgnc "HGNC_SYMBOL:CANT1"
      map_id "M112_286"
      name "CANT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa293"
      uniprot "UNIPROT:Q8WVQ1"
    ]
    graphics [
      x 191.47607022657485
      y 1295.2067092086227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_286"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24636;urn:miriam:pubchem.compound:1038"
      hgnc "NA"
      map_id "M112_280"
      name "H"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa288"
      uniprot "NA"
    ]
    graphics [
      x 1685.5335816614925
      y 762.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_280"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:1061;urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "M112_281"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa289"
      uniprot "NA"
    ]
    graphics [
      x 1121.4760702265748
      y 1218.9296873026296
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_281"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962"
      hgnc "NA"
      map_id "M112_269"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa278"
      uniprot "NA"
    ]
    graphics [
      x 1706.090149807047
      y 492.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_269"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:1061;urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "M112_270"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa279"
      uniprot "NA"
    ]
    graphics [
      x 1706.090149807047
      y 427.43163547063716
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_270"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:1061;urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "M112_272"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa280"
      uniprot "NA"
    ]
    graphics [
      x 1736.090149807047
      y 522.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_272"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24636;urn:miriam:pubchem.compound:1038"
      hgnc "NA"
      map_id "M112_273"
      name "H"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa281"
      uniprot "NA"
    ]
    graphics [
      x 1991.4760702265748
      y 1367.606843746084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_273"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24636;urn:miriam:pubchem.compound:1038"
      hgnc "NA"
      map_id "M112_274"
      name "H"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa282"
      uniprot "NA"
    ]
    graphics [
      x 2681.476070226575
      y 1226.3787676955833
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_274"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962"
      hgnc "NA"
      map_id "M112_323"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa328"
      uniprot "NA"
    ]
    graphics [
      x 1060.4146427238684
      y 2134.810963633781
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_323"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24636;urn:miriam:pubchem.compound:1038"
      hgnc "NA"
      map_id "M112_322"
      name "H"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa327"
      uniprot "NA"
    ]
    graphics [
      x 682.5144998161113
      y 1932.9652168721805
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_322"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:refseq:NM_001242505;urn:miriam:hgnc:4212;urn:miriam:uniprot:Q9Y2T3;urn:miriam:uniprot:Q9Y2T3;urn:miriam:ensembl:ENSG00000119125;urn:miriam:hgnc.symbol:GDA;urn:miriam:ncbigene:9615;urn:miriam:hgnc.symbol:GDA;urn:miriam:ncbigene:9615;urn:miriam:ec-code:3.5.4.3"
      hgnc "HGNC_SYMBOL:GDA"
      map_id "M112_321"
      name "GDA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa326"
      uniprot "UNIPROT:Q9Y2T3"
    ]
    graphics [
      x 2182.5144998161113
      y 1851.3846992840236
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_321"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:223;urn:miriam:obo.chebi:CHEBI%3A28938"
      hgnc "NA"
      map_id "M112_324"
      name "Ammonium"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa329"
      uniprot "NA"
    ]
    graphics [
      x 1271.4760702265748
      y 1182.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_324"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:1061;urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "M112_250"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa259"
      uniprot "NA"
    ]
    graphics [
      x 728.8536650413897
      y 972.5426710540675
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_250"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:439287;urn:miriam:obo.chebi:CHEBI%3A28542"
      hgnc "NA"
      map_id "M112_252"
      name "2_minus_deoxy_minus__alpha__minus_D_minus_ribose_space_1_minus_phosphate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa260"
      uniprot "NA"
    ]
    graphics [
      x 787.9673850663706
      y 1731.9830528252005
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_252"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15422;urn:miriam:pubchem.compound:5957"
      hgnc "NA"
      map_id "M112_210"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa217"
      uniprot "NA"
    ]
    graphics [
      x 2171.476070226575
      y 1038.022409089093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_210"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18050;urn:miriam:pubchem.compound:5961"
      hgnc "NA"
      map_id "M112_211"
      name "L_minus_Glutamine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa218"
      uniprot "NA"
    ]
    graphics [
      x 2141.476070226575
      y 1090.7101851197485
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_211"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:refseq:NM_003875;urn:miriam:pubmed:8089153;urn:miriam:hgnc:4378;urn:miriam:uniprot:P49915;urn:miriam:uniprot:P49915;urn:miriam:ensembl:ENSG00000163655;urn:miriam:hgnc.symbol:GMPS;urn:miriam:hgnc.symbol:GMPS;urn:miriam:ec-code:6.3.5.2;urn:miriam:ncbigene:8833;urn:miriam:ncbigene:8833"
      hgnc "HGNC_SYMBOL:GMPS"
      map_id "M112_209"
      name "GMPS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa216"
      uniprot "UNIPROT:P49915"
    ]
    graphics [
      x 1871.4760702265748
      y 1274.7508489859797
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_209"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:644102;urn:miriam:obo.chebi:CHEBI%3A18361"
      hgnc "NA"
      map_id "M112_217"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa223"
      uniprot "NA"
    ]
    graphics [
      x 2501.476070226575
      y 1230.2360156051782
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_217"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16027;urn:miriam:pubchem.compound:6083"
      hgnc "NA"
      map_id "M112_218"
      name "AMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa224"
      uniprot "NA"
    ]
    graphics [
      x 1178.8536650413898
      y 817.402437657275
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_218"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16015;urn:miriam:pubchem.compound:33032"
      hgnc "NA"
      map_id "M112_214"
      name "L_minus_Glutamate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa220"
      uniprot "NA"
    ]
    graphics [
      x 2171.5964691162635
      y 308.14169569262936
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_214"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24636;urn:miriam:pubchem.compound:1038"
      hgnc "NA"
      map_id "M112_215"
      name "H"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa221"
      uniprot "NA"
    ]
    graphics [
      x 848.8536650413897
      y 886.0055613274181
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_215"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24636;urn:miriam:pubchem.compound:1038"
      hgnc "NA"
      map_id "M112_216"
      name "H"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa222"
      uniprot "NA"
    ]
    graphics [
      x 1781.4760702265748
      y 1066.8134512202714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_216"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_14"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re115"
      uniprot "NA"
    ]
    graphics [
      x 772.5144998161113
      y 2046.0882182784605
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:doi:10.1101/2020.03.22.002386;urn:miriam:ncbiprotein:YP_009725309"
      hgnc "NA"
      map_id "M112_386"
      name "Nsp14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa410"
      uniprot "NA"
    ]
    graphics [
      x 1144.973418093434
      y 2622.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_386"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_10"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "re110"
      uniprot "NA"
    ]
    graphics [
      x 561.6104592494069
      y 1874.8967611372123
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubmed:1969416"
      hgnc "NA"
      map_id "M112_387"
      name "Guanine_space_nucleotide_space_synthesis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa411"
      uniprot "NA"
    ]
    graphics [
      x 517.9673850663706
      y 1783.2388350232563
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_387"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:223;urn:miriam:obo.chebi:CHEBI%3A28938"
      hgnc "NA"
      map_id "M112_287"
      name "Ammonium"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa294"
      uniprot "NA"
    ]
    graphics [
      x 1378.504116330716
      y 252.06502830998443
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_287"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18009;urn:miriam:pubchem.compound:5886"
      hgnc "NA"
      map_id "M112_289"
      name "NADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa296"
      uniprot "NA"
    ]
    graphics [
      x 1737.2304313236964
      y 179.4142021905609
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_289"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ncbigene:2766;urn:miriam:ncbigene:2766;urn:miriam:refseq:NM_006877;urn:miriam:hgnc:4376;urn:miriam:ensembl:ENSG00000137198;urn:miriam:ec-code:1.7.1.7;urn:miriam:uniprot:P36959;urn:miriam:uniprot:P36959;urn:miriam:hgnc.symbol:GMPR;urn:miriam:hgnc.symbol:GMPR"
      hgnc "HGNC_SYMBOL:GMPR"
      map_id "M112_292"
      name "GMPR"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa299"
      uniprot "UNIPROT:P36959"
    ]
    graphics [
      x 1328.8536650413898
      y 912.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_292"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc:4377;urn:miriam:hgnc.symbol:GMPR2;urn:miriam:hgnc.symbol:GMPR2;urn:miriam:ec-code:1.7.1.7;urn:miriam:refseq:NM_016576;urn:miriam:ensembl:ENSG00000100938;urn:miriam:ncbigene:51292;urn:miriam:uniprot:Q9P2T1;urn:miriam:uniprot:Q9P2T1;urn:miriam:ncbigene:51292"
      hgnc "HGNC_SYMBOL:GMPR2"
      map_id "M112_295"
      name "GMPR2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa300"
      uniprot "UNIPROT:Q9P2T1"
    ]
    graphics [
      x 1928.8536650413898
      y 796.8428864369035
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_295"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16474;urn:miriam:pubchem.compound:5884"
      hgnc "NA"
      map_id "M112_288"
      name "NADPH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa295"
      uniprot "NA"
    ]
    graphics [
      x 1542.1148718529048
      y 282.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_288"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24636;urn:miriam:pubchem.compound:1038"
      hgnc "NA"
      map_id "M112_291"
      name "H"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa298"
      uniprot "NA"
    ]
    graphics [
      x 2048.85366504139
      y 730.1506240017071
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_291"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24636;urn:miriam:pubchem.compound:1038"
      hgnc "NA"
      map_id "M112_290"
      name "H"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa297"
      uniprot "NA"
    ]
    graphics [
      x 1601.4760702265748
      y 1332.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_290"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18381;urn:miriam:pubchem.compound:166760"
      hgnc "NA"
      map_id "M112_189"
      name "1_minus_(5'_minus_Phosphoribosyl)_minus_5_minus_formamido_minus_4_minus_imidazolecarboxamide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa198"
      uniprot "NA"
    ]
    graphics [
      x 2227.9673850663708
      y 1700.5567968186347
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_189"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ensembl:ENSG00000138363;urn:miriam:uniprot:P31939;urn:miriam:uniprot:P31939;urn:miriam:refseq:NM_004044;urn:miriam:ec-code:3.5.4.10;urn:miriam:hgnc:794;urn:miriam:hgnc.symbol:ATIC;urn:miriam:ncbigene:471;urn:miriam:hgnc.symbol:ATIC;urn:miriam:ncbigene:471;urn:miriam:ec-code:2.1.2.3"
      hgnc "HGNC_SYMBOL:ATIC"
      map_id "M112_190"
      name "ATIC"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa199"
      uniprot "UNIPROT:P31939"
    ]
    graphics [
      x 982.5144998161113
      y 2001.9577197196427
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_190"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962"
      hgnc "NA"
      map_id "M112_196"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa203"
      uniprot "NA"
    ]
    graphics [
      x 1642.5144998161113
      y 1962.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_196"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      annotation "PUBMED:13672969"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_47"
      name "phosphoribosylaminoimidazolecarboxamide formyltransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re49"
      uniprot "NA"
    ]
    graphics [
      x 411.61045924940686
      y 2012.5572932182372
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18406;urn:miriam:pubchem.compound:65110"
      hgnc "NA"
      map_id "M112_186"
      name "1_minus_(5'_minus_Phosphoribosyl)_minus_5_minus_amino_minus_4_minus_imidazolecarboxamide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa195"
      uniprot "NA"
    ]
    graphics [
      x 247.96738506637075
      y 1661.0093857881004
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_186"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:135450591;urn:miriam:obo.chebi:CHEBI%3A15637"
      hgnc "NA"
      map_id "M112_194"
      name "10_minus_Formyltetrahydrofolate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa201"
      uniprot "NA"
    ]
    graphics [
      x 758.8536650413897
      y 1079.8148066217807
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_194"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:135444742;urn:miriam:obo.chebi:CHEBI%3A67016"
      hgnc "NA"
      map_id "M112_193"
      name "Tetrahydrofolate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa200"
      uniprot "NA"
    ]
    graphics [
      x 697.9673850663706
      y 1762.4666947456517
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_193"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      annotation "PUBMED:13366975"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_46"
      name "adenoylsuccinate lyase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re48"
      uniprot "NA"
    ]
    graphics [
      x 1211.4760702265748
      y 1419.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18319;urn:miriam:pubchem.compound:160666"
      hgnc "NA"
      map_id "M112_179"
      name "1_minus_(5'_minus_Phosphoribosyl)_minus_5_minus_amino_minus_4_minus_(N_minus_succinocarboxamide)_minus_imidazole"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa189"
      uniprot "NA"
    ]
    graphics [
      x 2018.8536650413898
      y 830.581334977754
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_179"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc.symbol:ADSL;urn:miriam:hgnc.symbol:ADSL;urn:miriam:ncbigene:158;urn:miriam:ncbigene:158;urn:miriam:ec-code:4.3.2.2;urn:miriam:hgnc:291;urn:miriam:uniprot:P30566;urn:miriam:uniprot:P30566;urn:miriam:refseq:NM_000026;urn:miriam:ensembl:ENSG00000239900"
      hgnc "HGNC_SYMBOL:ADSL"
      map_id "M112_188"
      name "ADSL"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa197"
      uniprot "UNIPROT:P30566"
    ]
    graphics [
      x 1702.5144998161113
      y 1955.5046666730118
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_188"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:5460307;urn:miriam:obo.chebi:CHEBI%3A29806"
      hgnc "NA"
      map_id "M112_187"
      name "Fumarate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa196"
      uniprot "NA"
    ]
    graphics [
      x 581.4760702265747
      y 1362.451730092263
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_187"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      annotation "PUBMED:3036807"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_45"
      name "phosphoribosylaminoimidazole-succinocarboxamide synthase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re47"
      uniprot "NA"
    ]
    graphics [
      x 2122.5144998161113
      y 1913.3608844567657
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A28413;urn:miriam:pubchem.compound:165388"
      hgnc "NA"
      map_id "M112_174"
      name "1_minus_(5_minus_Phospho_minus_D_minus_ribosyl)_minus_5_minus_amino_minus_4_minus_imidazolecarboxylate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa184"
      uniprot "NA"
    ]
    graphics [
      x 2572.5144998161113
      y 1830.9129755444785
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_174"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15422;urn:miriam:pubchem.compound:5957"
      hgnc "NA"
      map_id "M112_182"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa191"
      uniprot "NA"
    ]
    graphics [
      x 1970.0074341427699
      y 2348.2141301067395
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_182"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17053;urn:miriam:pubchem.compound:5960"
      hgnc "NA"
      map_id "M112_185"
      name "L_minus_Aspartate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa194"
      uniprot "NA"
    ]
    graphics [
      x 2080.4146427238684
      y 2164.8290808904762
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_185"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:uniprot:P22234;urn:miriam:uniprot:P22234;urn:miriam:hgnc:8587;urn:miriam:ncbigene:10606;urn:miriam:ncbigene:10606;urn:miriam:refseq:NM_006452;urn:miriam:ensembl:ENSG00000128050;urn:miriam:hgnc.symbol:PAICS;urn:miriam:hgnc.symbol:PAICS;urn:miriam:ec-code:6.3.2.6;urn:miriam:ec-code:4.1.1.21"
      hgnc "HGNC_SYMBOL:PAICS"
      map_id "M112_175"
      name "PAICS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa185"
      uniprot "UNIPROT:P22234"
    ]
    graphics [
      x 1751.4760702265748
      y 1126.8134512202714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_175"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:6022;urn:miriam:obo.chebi:CHEBI%3A16761"
      hgnc "NA"
      map_id "M112_183"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa192"
      uniprot "NA"
    ]
    graphics [
      x 1312.5144998161113
      y 1842.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_183"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 167
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24636;urn:miriam:pubchem.compound:1038"
      hgnc "NA"
      map_id "M112_181"
      name "H"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa190"
      uniprot "NA"
    ]
    graphics [
      x 2081.476070226575
      y 1003.1270120257964
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_181"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 168
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:1061;urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "M112_184"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa193"
      uniprot "NA"
    ]
    graphics [
      x 1912.5144998161113
      y 2050.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_184"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 169
    zlevel -1

    cd19dm [
      annotation "PUBMED:9500840"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_44"
      name "phosphoribosylaminoimidazole carboxylase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re46"
      uniprot "NA"
    ]
    graphics [
      x 2378.85366504139
      y 809.1424673272155
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 170
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A138560;urn:miriam:pubchem.compound:161500"
      hgnc "NA"
      map_id "M112_168"
      name "Aminoimidazole_space_ribotide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa179"
      uniprot "NA"
    ]
    graphics [
      x 1637.2023363703734
      y 942.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_168"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 171
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16526;urn:miriam:pubchem.compound:280"
      hgnc "NA"
      map_id "M112_176"
      name "CO2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa186"
      uniprot "NA"
    ]
    graphics [
      x 2321.476070226575
      y 994.9055008807553
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_176"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 172
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24636;urn:miriam:pubchem.compound:1038"
      hgnc "NA"
      map_id "M112_178"
      name "H"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa188"
      uniprot "NA"
    ]
    graphics [
      x 2018.8536650413898
      y 962.0735964172806
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_178"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 173
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24636;urn:miriam:pubchem.compound:1038"
      hgnc "NA"
      map_id "M112_177"
      name "H"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa187"
      uniprot "NA"
    ]
    graphics [
      x 2348.85366504139
      y 871.3530460448193
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_177"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 174
    zlevel -1

    cd19dm [
      annotation "PUBMED:13405929"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_43"
      name "2-(Formamido)-N1-(5-phosphoribosyl)acetamidine cyclo-ligase "
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re45"
      uniprot "NA"
    ]
    graphics [
      x 1318.965509607095
      y 582.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 175
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18413;urn:miriam:pubchem.compound:5462266"
      hgnc "NA"
      map_id "M112_159"
      name "2_minus_(Formamido)_minus_N1_minus_(5'_minus_phosphoribosyl)acetamidine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa170"
      uniprot "NA"
    ]
    graphics [
      x 1807.9673850663708
      y 1633.8597507391748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_159"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 176
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15422;urn:miriam:pubchem.compound:5957"
      hgnc "NA"
      map_id "M112_170"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa180"
      uniprot "NA"
    ]
    graphics [
      x 758.8536650413897
      y 868.6538209041477
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_170"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 177
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:refseq:NM_000819;urn:miriam:ec-code:2.1.2.2;urn:miriam:uniprot:P22102;urn:miriam:uniprot:P22102;urn:miriam:ec-code:6.3.3.1;urn:miriam:ncbigene:2618;urn:miriam:ncbigene:2618;urn:miriam:ec-code:6.3.4.13;urn:miriam:hgnc:4163;urn:miriam:hgnc.symbol:GART;urn:miriam:ensembl:ENSG00000159131;urn:miriam:hgnc.symbol:GART"
      hgnc "HGNC_SYMBOL:GART"
      map_id "M112_149"
      name "GART"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa160"
      uniprot "UNIPROT:P22102"
    ]
    graphics [
      x 1118.8536650413898
      y 930.5543133122671
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_149"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 178
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:1061;urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "M112_171"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa181"
      uniprot "NA"
    ]
    graphics [
      x 491.47607022657473
      y 1116.561207681754
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_171"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 179
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24636;urn:miriam:pubchem.compound:1038"
      hgnc "NA"
      map_id "M112_173"
      name "H"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa183"
      uniprot "NA"
    ]
    graphics [
      x 1877.2023363703734
      y 962.6254878021133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_173"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 180
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:6022;urn:miriam:obo.chebi:CHEBI%3A16761"
      hgnc "NA"
      map_id "M112_172"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa182"
      uniprot "NA"
    ]
    graphics [
      x 1208.8536650413898
      y 969.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_172"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 181
    zlevel -1

    cd19dm [
      annotation "PUBMED:13563520"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_40"
      name "5-Phospho-D-ribosylamine:glycine ligase (ADP-forming)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re42"
      uniprot "NA"
    ]
    graphics [
      x 2231.476070226575
      y 1199.5869171055397
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 182
    zlevel -1

    cd19dm [
      annotation "PUBMED:2183217"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_41"
      name "10-formyltetrahydrofolate:5'-phosphoribosylglycinamide formyltransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re43"
      uniprot "NA"
    ]
    graphics [
      x 1102.5144998161113
      y 1870.4104138933783
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 183
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:129630972;urn:miriam:obo.chebi:CHEBI%3A143788"
      hgnc "NA"
      map_id "M112_148"
      name "5_minus_phospho_minus_beta_minus_D_minus_ribosylglycinamide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa159"
      uniprot "NA"
    ]
    graphics [
      x 1151.4760702265748
      y 1327.3786093973251
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_148"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 184
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:135450591;urn:miriam:obo.chebi:CHEBI%3A15637"
      hgnc "NA"
      map_id "M112_156"
      name "10_minus_Formyltetrahydrofolate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa167"
      uniprot "NA"
    ]
    graphics [
      x 1522.5144998161113
      y 1902.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_156"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 185
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:129652037"
      hgnc "NA"
      map_id "M112_155"
      name "5_minus_phosphoribosyl_minus_N_minus_formylglycinamide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa166"
      uniprot "NA"
    ]
    graphics [
      x 1721.4760702265748
      y 1216.8134512202714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_155"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 186
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:135444742;urn:miriam:obo.chebi:CHEBI%3A67016"
      hgnc "NA"
      map_id "M112_157"
      name "Tetrahydrofolate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa168"
      uniprot "NA"
    ]
    graphics [
      x 1821.6104592494069
      y 1779.1115275221719
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_157"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 187
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24636;urn:miriam:pubchem.compound:1038"
      hgnc "NA"
      map_id "M112_158"
      name "H"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa169"
      uniprot "NA"
    ]
    graphics [
      x 1690.4146427238684
      y 2259.399726072529
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_158"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 188
    zlevel -1

    cd19dm [
      annotation "PUBMED:13416226"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_42"
      name "5'-Phosphoribosylformylglycinamide:L-glutamine amido-ligase "
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re44"
      uniprot "NA"
    ]
    graphics [
      x 2110.4146427238684
      y 2093.8489235874436
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 189
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18050;urn:miriam:pubchem.compound:5961"
      hgnc "NA"
      map_id "M112_162"
      name "L_minus_Glutamine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa173"
      uniprot "NA"
    ]
    graphics [
      x 1331.4760702265748
      y 1302.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_162"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 190
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15422;urn:miriam:pubchem.compound:5957"
      hgnc "NA"
      map_id "M112_163"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa174"
      uniprot "NA"
    ]
    graphics [
      x 1517.5115202333882
      y 1962.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_163"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 191
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962"
      hgnc "NA"
      map_id "M112_161"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa172"
      uniprot "NA"
    ]
    graphics [
      x 1462.5144998161113
      y 2142.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_161"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 192
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc:8863;urn:miriam:refseq:NM_012393;urn:miriam:ncbigene:5198;urn:miriam:ncbigene:5198;urn:miriam:ensembl:ENSG00000178921;urn:miriam:uniprot:O15067;urn:miriam:uniprot:O15067;urn:miriam:ec-code:6.3.5.3;urn:miriam:hgnc.symbol:PFAS;urn:miriam:hgnc.symbol:PFAS"
      hgnc "HGNC_SYMBOL:PFAS"
      map_id "M112_160"
      name "PFAS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa171"
      uniprot "UNIPROT:O15067"
    ]
    graphics [
      x 1821.6104592494069
      y 1668.1145040299514
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_160"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 193
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16015;urn:miriam:pubchem.compound:33032"
      hgnc "NA"
      map_id "M112_166"
      name "L_minus_Glutamate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa177"
      uniprot "NA"
    ]
    graphics [
      x 1730.0074341427699
      y 2316.313882011814
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_166"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 194
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:1061;urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "M112_164"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa175"
      uniprot "NA"
    ]
    graphics [
      x 1930.4146427238684
      y 2200.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_164"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 195
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24636;urn:miriam:pubchem.compound:1038"
      hgnc "NA"
      map_id "M112_167"
      name "H"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa178"
      uniprot "NA"
    ]
    graphics [
      x 2167.9673850663708
      y 1594.4288994774415
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_167"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 196
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:6022;urn:miriam:obo.chebi:CHEBI%3A16761"
      hgnc "NA"
      map_id "M112_165"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa176"
      uniprot "NA"
    ]
    graphics [
      x 2303.6600943596754
      y 2484.112303301138
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_165"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 197
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A37737;urn:miriam:pubchem.compound:439905"
      hgnc "NA"
      map_id "M112_142"
      name "5_minus_phospho_minus_beta_minus_D_minus_ribosylamine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa153"
      uniprot "NA"
    ]
    graphics [
      x 1762.5144998161113
      y 1933.8597507391748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_142"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 198
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15422;urn:miriam:pubchem.compound:5957"
      hgnc "NA"
      map_id "M112_150"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa161"
      uniprot "NA"
    ]
    graphics [
      x 1581.6104592494069
      y 1782.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_150"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 199
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:750;urn:miriam:obo.chebi:CHEBI%3A15428"
      hgnc "NA"
      map_id "M112_151"
      name "Glycine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa162"
      uniprot "NA"
    ]
    graphics [
      x 1544.9275732149335
      y 612.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_151"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 200
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:6022;urn:miriam:obo.chebi:CHEBI%3A16761"
      hgnc "NA"
      map_id "M112_152"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa163"
      uniprot "NA"
    ]
    graphics [
      x 2242.5144998161113
      y 1855.676905472525
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_152"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 201
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24636;urn:miriam:pubchem.compound:1038"
      hgnc "NA"
      map_id "M112_153"
      name "H"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa164"
      uniprot "NA"
    ]
    graphics [
      x 2408.85366504139
      y 601.8490752347982
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_153"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 202
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:1061;urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "M112_154"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa165"
      uniprot "NA"
    ]
    graphics [
      x 2092.5144998161113
      y 2033.3608844567657
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_154"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 203
    zlevel -1

    cd19dm [
      annotation "PUBMED:14235537"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_39"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re41"
      uniprot "NA"
    ]
    graphics [
      x 1451.4760702265748
      y 1392.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 204
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17111;urn:miriam:pubchem.compound:7339"
      hgnc "NA"
      map_id "M112_102"
      name "5_minus_phospho_minus__alpha__minus_D_minus_ribose_space_1_minus_diphosphate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa110"
      uniprot "NA"
    ]
    graphics [
      x 2332.5144998161113
      y 2091.4036447431376
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 205
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962"
      hgnc "NA"
      map_id "M112_145"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa156"
      uniprot "NA"
    ]
    graphics [
      x 1721.4760702265748
      y 1156.8134512202714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_145"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 206
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18050;urn:miriam:pubchem.compound:5961"
      hgnc "NA"
      map_id "M112_144"
      name "L_minus_Glutamine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa155"
      uniprot "NA"
    ]
    graphics [
      x 1628.8536650413898
      y 882.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_144"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 207
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:uniprot:Q06203;urn:miriam:uniprot:Q06203;urn:miriam:hgnc:9238;urn:miriam:hgnc.symbol:PPAT;urn:miriam:hgnc.symbol:PPAT;urn:miriam:ncbigene:5471;urn:miriam:ensembl:ENSG00000128059;urn:miriam:ncbigene:5471;urn:miriam:refseq:NM_002703;urn:miriam:ec-code:2.4.2.14"
      hgnc "HGNC_SYMBOL:PPAT"
      map_id "M112_147"
      name "PPAT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa158"
      uniprot "UNIPROT:Q06203"
    ]
    graphics [
      x 848.8536650413896
      y 1067.7080626965108
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_147"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 208
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16015;urn:miriam:pubchem.compound:33032"
      hgnc "NA"
      map_id "M112_143"
      name "L_minus_Glutamate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa154"
      uniprot "NA"
    ]
    graphics [
      x 1222.5144998161113
      y 2079.0166269831952
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_143"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 209
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:644102;urn:miriam:obo.chebi:CHEBI%3A18361"
      hgnc "NA"
      map_id "M112_146"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa157"
      uniprot "NA"
    ]
    graphics [
      x 1208.8536650413898
      y 729.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_146"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 210
    zlevel -1

    cd19dm [
      annotation "PUBMED:4306285"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_38"
      name "ribose-phosphate pyrophosphokinase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re40"
      uniprot "NA"
    ]
    graphics [
      x 2118.8852511845735
      y 2235.4210909513736
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 211
    zlevel -1

    cd19dm [
      annotation "PUBMED:5320648;PUBMED:14165928"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_33"
      name "nicotinate-nucleotide pyrophosphorylase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re36"
      uniprot "NA"
    ]
    graphics [
      x 1181.4760702265748
      y 1247.997939433904
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 212
    zlevel -1

    cd19dm [
      annotation "PUBMED:4324895"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_34"
      name "nicotinate phosphoribosyltransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re37"
      uniprot "NA"
    ]
    graphics [
      x 2681.476070226575
      y 1474.3347986776794
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 213
    zlevel -1

    cd19dm [
      annotation "PUBMED:13416279"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_27"
      name "nicotinamide phosphoribosyltransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re29"
      uniprot "NA"
    ]
    graphics [
      x 1510.4146427238684
      y 2262.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 214
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17154;urn:miriam:pubchem.compound:936"
      hgnc "NA"
      map_id "M112_399"
      name "Nicotinamide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa66"
      uniprot "NA"
    ]
    graphics [
      x 1930.4146427238684
      y 2260.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_399"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 215
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc.symbol:NAMPT;urn:miriam:hgnc.symbol:NAMPT;urn:miriam:ec-code:2.4.2.12;urn:miriam:hgnc:30092;urn:miriam:uniprot:P43490;urn:miriam:uniprot:P43490;urn:miriam:ncbigene:10135;urn:miriam:refseq:NM_182790;urn:miriam:ncbigene:10135;urn:miriam:ensembl:ENSG00000105835"
      hgnc "HGNC_SYMBOL:NAMPT"
      map_id "M112_99"
      name "NAMPT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa108"
      uniprot "UNIPROT:P43490"
    ]
    graphics [
      x 1434.5606843061807
      y 2712.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 216
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:14180;urn:miriam:obo.chebi:CHEBI%3A16171"
      hgnc "NA"
      map_id "M112_401"
      name "Nicotinamide_space_D_minus_ribonucleotide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa69"
      uniprot "NA"
    ]
    graphics [
      x 1071.6104592494069
      y 1809.749802637834
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_401"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 217
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:644102;urn:miriam:obo.chebi:CHEBI%3A18361"
      hgnc "NA"
      map_id "M112_100"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa109"
      uniprot "NA"
    ]
    graphics [
      x 2001.6104592494069
      y 1736.9817561041968
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 218
    zlevel -1

    cd19dm [
      annotation "PUBMED:14907738"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_24"
      name "ATP:N-ribosylnicotinamide 5'-phosphotransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re25"
      uniprot "NA"
    ]
    graphics [
      x 2261.476070226575
      y 1075.9542133851683
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 219
    zlevel -1

    cd19dm [
      annotation "PUBMED:13428775"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_23"
      name "NAD+ phosphohydrolase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re24"
      uniprot "NA"
    ]
    graphics [
      x 1181.4760702265748
      y 1501.4212669072997
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 220
    zlevel -1

    cd19dm [
      annotation "PUBMED:16746659"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_25"
      name "nicotinamide ribonucleotide phosphohydrolase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re26"
      uniprot "NA"
    ]
    graphics [
      x 1346.090149807047
      y 462.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 221
    zlevel -1

    cd19dm [
      annotation "PUBMED:13684981"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_22"
      name "nicotinamide-nucleotide adenylyltransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re23"
      uniprot "NA"
    ]
    graphics [
      x 1162.5144998161113
      y 2032.7078987280793
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 222
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15422;urn:miriam:pubchem.compound:5957"
      hgnc "NA"
      map_id "M112_420"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa88"
      uniprot "NA"
    ]
    graphics [
      x 665.7334578546346
      y 2180.083104549983
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_420"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 223
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24636;urn:miriam:pubchem.compound:1038"
      hgnc "NA"
      map_id "M112_421"
      name "H"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa89"
      uniprot "NA"
    ]
    graphics [
      x 847.9673850663706
      y 1663.1690082783348
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_421"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 224
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:uniprot:Q9BZQ4;urn:miriam:uniprot:Q9BZQ4;urn:miriam:ncbigene:23057;urn:miriam:ncbigene:23057;urn:miriam:refseq:NM_015039;urn:miriam:ec-code:2.7.7.18;urn:miriam:hgnc:16789;urn:miriam:ec-code:2.7.7.1;urn:miriam:hgnc.symbol:NMNAT2;urn:miriam:hgnc.symbol:NMNAT2;urn:miriam:pubmed:12359228;urn:miriam:ensembl:ENSG00000157064"
      hgnc "HGNC_SYMBOL:NMNAT2"
      map_id "M112_424"
      name "NMNAT2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa91"
      uniprot "UNIPROT:Q9BZQ4"
    ]
    graphics [
      x 1268.8536650413898
      y 942.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_424"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 225
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ensembl:ENSG00000173614;urn:miriam:ec-code:2.7.7.18;urn:miriam:hgnc:17877;urn:miriam:hgnc.symbol:NMNAT1;urn:miriam:hgnc.symbol:NMNAT1;urn:miriam:ec-code:2.7.7.1;urn:miriam:refseq:NM_001297778;urn:miriam:pubmed:12359228;urn:miriam:ncbigene:64802;urn:miriam:ncbigene:64802;urn:miriam:uniprot:Q9HAN9;urn:miriam:uniprot:Q9HAN9"
      hgnc "HGNC_SYMBOL:NMNAT1"
      map_id "M112_425"
      name "NMNAT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa92"
      uniprot "UNIPROT:Q9HAN9"
    ]
    graphics [
      x 2170.4146427238684
      y 2128.25738796283
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_425"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 226
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ec-code:2.7.7.18;urn:miriam:ensembl:ENSG00000163864;urn:miriam:uniprot:Q96T66;urn:miriam:uniprot:Q96T66;urn:miriam:ec-code:2.7.7.1;urn:miriam:hgnc:20989;urn:miriam:hgnc.symbol:NMNAT3;urn:miriam:refseq:NM_178177;urn:miriam:hgnc.symbol:NMNAT3;urn:miriam:ncbigene:349565;urn:miriam:ncbigene:349565;urn:miriam:pubmed:17402747"
      hgnc "HGNC_SYMBOL:NMNAT3"
      map_id "M112_426"
      name "NMNAT3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa93"
      uniprot "UNIPROT:Q96T66"
    ]
    graphics [
      x 2647.9673850663708
      y 1681.7464025950017
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_426"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 227
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:5892;urn:miriam:obo.chebi:CHEBI%3A15846"
      hgnc "NA"
      map_id "M112_403"
      name "NAD"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa70"
      uniprot "NA"
    ]
    graphics [
      x 2377.9673850663708
      y 1730.9633486433554
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_403"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 228
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:644102;urn:miriam:obo.chebi:CHEBI%3A18361"
      hgnc "NA"
      map_id "M112_423"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa90"
      uniprot "NA"
    ]
    graphics [
      x 820.4146427238683
      y 2143.853912881765
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_423"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 229
    zlevel -1

    cd19dm [
      annotation "PUBMED:13717627"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_21"
      name "NAD synthetase 1"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re22"
      uniprot "NA"
    ]
    graphics [
      x 698.8536650413897
      y 697.2037470914684
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 230
    zlevel -1

    cd19dm [
      annotation "PUBMED:16756498"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_30"
      name "NAD-dependent deacetylase sirtuin-5"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re32"
      uniprot "NA"
    ]
    graphics [
      x 2621.476070226575
      y 1190.7901305573753
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 231
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_20"
      name "NAD(P) transhydrogenase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re21"
      uniprot "NA"
    ]
    graphics [
      x 1738.1783887978854
      y 2533.6361841316566
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 232
    zlevel -1

    cd19dm [
      annotation "PUBMED:11866528"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_31"
      name "NAD+ glycohydrolase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re33"
      uniprot "NA"
    ]
    graphics [
      x 2111.476070226575
      y 1007.7871053251297
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 233
    zlevel -1

    cd19dm [
      annotation "PUBMED:11594753"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_19"
      name "NAD kinase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re20"
      uniprot "NA"
    ]
    graphics [
      x 2452.5144998161113
      y 1903.8214117411178
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 234
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15422;urn:miriam:pubchem.compound:5957"
      hgnc "NA"
      map_id "M112_407"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa76"
      uniprot "NA"
    ]
    graphics [
      x 2542.5144998161113
      y 1959.1832103865559
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_407"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 235
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc:29831;urn:miriam:ncbigene:65220;urn:miriam:refseq:NM_023018;urn:miriam:ncbigene:65220;urn:miriam:ec-code:2.7.1.23;urn:miriam:uniprot:O95544;urn:miriam:uniprot:O95544;urn:miriam:hgnc.symbol:NADK;urn:miriam:hgnc.symbol:NADK;urn:miriam:ensembl:ENSG00000008130"
      hgnc "HGNC_SYMBOL:NADK"
      map_id "M112_406"
      name "NADK"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa75"
      uniprot "UNIPROT:O95544"
    ]
    graphics [
      x 2291.476070226575
      y 1414.1444038133232
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_406"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 236
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18009;urn:miriam:pubchem.compound:5886"
      hgnc "NA"
      map_id "M112_282"
      name "NADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa29"
      uniprot "NA"
    ]
    graphics [
      x 1241.4760702265748
      y 1266.5060637260106
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_282"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 237
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:6022;urn:miriam:obo.chebi:CHEBI%3A16761"
      hgnc "NA"
      map_id "M112_408"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa77"
      uniprot "NA"
    ]
    graphics [
      x 1881.6104592494069
      y 1701.145821463369
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_408"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 238
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24636;urn:miriam:pubchem.compound:1038"
      hgnc "NA"
      map_id "M112_409"
      name "H"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa78"
      uniprot "NA"
    ]
    graphics [
      x 1602.6788096431687
      y 1542.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_409"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 239
    zlevel -1

    cd19dm [
      annotation "PUBMED:30201105"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_76"
      name "galactitol:NAD+ 1-oxidoreductase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re8"
      uniprot "NA"
    ]
    graphics [
      x 607.9673850663706
      y 1551.7955934272911
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 240
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16813;urn:miriam:pubchem.compound:11850"
      hgnc "NA"
      map_id "M112_262"
      name "Galacitol"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa27"
      uniprot "NA"
    ]
    graphics [
      x 1031.4760702265748
      y 1472.3785906698167
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_262"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 241
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc.symbol:AKR1B1;urn:miriam:hgnc.symbol:AKR1B1;urn:miriam:ncbigene:231;urn:miriam:ncbigene:231;urn:miriam:hgnc:381;urn:miriam:ensembl:ENSG00000085662;urn:miriam:refseq:NM_001628;urn:miriam:ec-code:1.1.1.300;urn:miriam:uniprot:P15121;urn:miriam:uniprot:P15121;urn:miriam:ec-code:1.1.1.372;urn:miriam:ec-code:1.1.1.54;urn:miriam:ec-code:1.1.1.21"
      hgnc "HGNC_SYMBOL:AKR1B1"
      map_id "M112_271"
      name "AKR1B1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa28"
      uniprot "UNIPROT:P15121"
    ]
    graphics [
      x 971.4760702265747
      y 1258.6039843924614
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_271"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 242
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:439353;urn:miriam:obo.chebi:CHEBI%3A27667"
      hgnc "NA"
      map_id "M112_90"
      name "D_minus_Galactose"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa1"
      uniprot "NA"
    ]
    graphics [
      x 1520.0074341427699
      y 2472.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 243
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16474;urn:miriam:pubchem.compound:5884"
      hgnc "NA"
      map_id "M112_294"
      name "NADPH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa30"
      uniprot "NA"
    ]
    graphics [
      x 1221.3051045386849
      y 2562.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_294"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 244
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24636;urn:miriam:pubchem.compound:1038"
      hgnc "NA"
      map_id "M112_305"
      name "H"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa31"
      uniprot "NA"
    ]
    graphics [
      x 1178.8536650413898
      y 787.402437657275
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_305"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 245
    zlevel -1

    cd19dm [
      annotation "PUBMED:10866822;PUBMED:976079"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_17"
      name "Stachyose galactohydrolase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re18"
      uniprot "NA"
    ]
    graphics [
      x 2632.5144998161113
      y 1891.1572347262522
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 246
    zlevel -1

    cd19dm [
      annotation "PUBMED:5822067"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_67"
      name "Lactose galactohydrolase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re7"
      uniprot "NA"
    ]
    graphics [
      x 607.9673850663706
      y 1622.6183265894208
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 247
    zlevel -1

    cd19dm [
      annotation "PUBMED:10866822;PUBMED:976079"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_15"
      name "Raffinose galactohydrolase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re16"
      uniprot "NA"
    ]
    graphics [
      x 2407.9673850663708
      y 1564.494966456387
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 248
    zlevel -1

    cd19dm [
      annotation "PUBMED:16661511;PUBMED:10866822;PUBMED:976079"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_87"
      name "melibiose galactohydrolase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re9"
      uniprot "NA"
    ]
    graphics [
      x 2422.5144998161113
      y 1959.9188066978086
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 249
    zlevel -1

    cd19dm [
      annotation "PUBMED:15026423;PUBMED:9778377"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_9"
      name "galactose mutarotase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1"
      uniprot "NA"
    ]
    graphics [
      x 1341.6104592494069
      y 1782.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 250
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:refseq:NM_138801;urn:miriam:hgnc:24063;urn:miriam:ec-code:5.1.3.3;urn:miriam:uniprot:Q96C23;urn:miriam:uniprot:Q96C23;urn:miriam:ensembl:ENSG00000143891;urn:miriam:ncbigene:130589;urn:miriam:ncbigene:130589;urn:miriam:hgnc.symbol:GALM;urn:miriam:hgnc.symbol:GALM"
      hgnc "HGNC_SYMBOL:GALM"
      map_id "M112_293"
      name "GALM"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa3"
      uniprot "UNIPROT:Q96C23"
    ]
    graphics [
      x 681.6104592494069
      y 1814.1297408228247
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_293"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 251
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:439357;urn:miriam:obo.chebi:CHEBI%3A28061"
      hgnc "NA"
      map_id "M112_191"
      name "_alpha__minus_D_minus_Galactose"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa2"
      uniprot "NA"
    ]
    graphics [
      x 1840.4146427238684
      y 2151.214968968279
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_191"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 252
    zlevel -1

    cd19dm [
      annotation "PUBMED:14596685"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_18"
      name "galactokinase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re2"
      uniprot "NA"
    ]
    graphics [
      x 2382.8827390521583
      y 2358.4663127534286
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 253
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15422;urn:miriam:pubchem.compound:5957"
      hgnc "NA"
      map_id "M112_398"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa6"
      uniprot "NA"
    ]
    graphics [
      x 1940.0074341427699
      y 2336.3858532184854
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_398"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 254
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:refseq:NM_000154;urn:miriam:ensembl:ENSG00000108479;urn:miriam:hgnc.symbol:GALK1;urn:miriam:hgnc.symbol:GALK1;urn:miriam:hgnc:4118;urn:miriam:ncbigene:2584;urn:miriam:ncbigene:2584;urn:miriam:ec-code:2.7.1.6;urn:miriam:uniprot:P51570;urn:miriam:uniprot:P51570"
      hgnc "HGNC_SYMBOL:GALK1"
      map_id "M112_390"
      name "GALK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa5"
      uniprot "UNIPROT:P51570"
    ]
    graphics [
      x 2617.9673850663708
      y 1683.3368118287563
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_390"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 255
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:123912;urn:miriam:obo.chebi:CHEBI%3A17973"
      hgnc "NA"
      map_id "M112_384"
      name "_alpha__minus_D_minus_Galactose_minus_1P"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa4"
      uniprot "NA"
    ]
    graphics [
      x 1960.4146427238684
      y 2267.606843746084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_384"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 256
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:6022;urn:miriam:obo.chebi:CHEBI%3A16761"
      hgnc "NA"
      map_id "M112_402"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa7"
      uniprot "NA"
    ]
    graphics [
      x 2271.6104592494066
      y 1825.676905472525
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_402"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 257
    zlevel -1

    cd19dm [
      annotation "PUBMED:13260264"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_28"
      name "galactose-1-phosphate uridylyltransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re3"
      uniprot "NA"
    ]
    graphics [
      x 1851.6104592494069
      y 1679.1116134373628
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 258
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:8629;urn:miriam:obo.chebi:CHEBI%3A46229"
      hgnc "NA"
      map_id "M112_91"
      name "UDP_minus__alpha__minus_D_minus_Glucose"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa10"
      uniprot "NA"
    ]
    graphics [
      x 1776.90784890218
      y 2470.7807836775473
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 259
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:refseq:NM_000155;urn:miriam:ensembl:ENSG00000213930;urn:miriam:hgnc:4135;urn:miriam:ec-code:2.7.7.12;urn:miriam:hgnc.symbol:GALT;urn:miriam:hgnc.symbol:GALT;urn:miriam:ncbigene:2592;urn:miriam:ncbigene:2592;urn:miriam:uniprot:P07902;urn:miriam:uniprot:P07902"
      hgnc "HGNC_SYMBOL:GALT"
      map_id "M112_422"
      name "GALT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa9"
      uniprot "UNIPROT:P07902"
    ]
    graphics [
      x 2501.476070226575
      y 1302.9292228354896
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_422"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 260
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A66914;urn:miriam:pubchem.compound:18068"
      hgnc "NA"
      map_id "M112_411"
      name "UDP_minus__alpha__minus_D_minus_Galactose"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa8"
      uniprot "NA"
    ]
    graphics [
      x 1852.5144998161113
      y 1897.978844946098
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_411"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 261
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:439165;urn:miriam:obo.chebi:CHEBI%3A29042"
      hgnc "NA"
      map_id "M112_101"
      name "_alpha__minus_D_minus_Glucose_minus_1_minus_P"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa11"
      uniprot "NA"
    ]
    graphics [
      x 1870.4146427238684
      y 2230.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 262
    zlevel -1

    cd19dm [
      annotation "PUBMED:30816613"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_37"
      name "UDP glucose pyrophosphorylase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re4"
      uniprot "NA"
    ]
    graphics [
      x 1305.8701285391035
      y 2592.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 263
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:644102;urn:miriam:obo.chebi:CHEBI%3A18361"
      hgnc "NA"
      map_id "M112_169"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa18"
      uniprot "NA"
    ]
    graphics [
      x 1871.4760702265748
      y 1444.1640922027127
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_169"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 264
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc.symbol:UGP2;urn:miriam:ncbigene:7360;urn:miriam:uniprot:Q16851;urn:miriam:uniprot:Q16851;urn:miriam:hgnc.symbol:UGP2;urn:miriam:ncbigene:7360;urn:miriam:hgnc:12527;urn:miriam:ec-code:2.7.7.9;urn:miriam:ensembl:ENSG00000169764;urn:miriam:refseq:NM_006759"
      hgnc "HGNC_SYMBOL:UGP2"
      map_id "M112_110"
      name "UGP2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa12"
      uniprot "UNIPROT:Q16851"
    ]
    graphics [
      x 1320.6067871147031
      y 2682.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 265
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:6133;urn:miriam:obo.chebi:CHEBI%3A15713"
      hgnc "NA"
      map_id "M112_118"
      name "UTP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa13"
      uniprot "NA"
    ]
    graphics [
      x 1600.4146427238684
      y 2232.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 266
    zlevel -1

    cd19dm [
      annotation "PUBMED:31827638"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_48"
      name "UDP-galactose-4-epimerase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re5"
      uniprot "NA"
    ]
    graphics [
      x 1792.5144998161113
      y 1917.1275183490286
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 267
    zlevel -1

    cd19dm [
      annotation "PUBMED:11947697"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_16"
      name "beta-1,4-galactosyltransferase 1"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re17"
      uniprot "NA"
    ]
    graphics [
      x 1181.4760702265748
      y 1287.0237622123032
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 268
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:79025;urn:miriam:obo.chebi:CHEBI%3A28102"
      hgnc "NA"
      map_id "M112_203"
      name "_alpha__minus_D_minus_Glucose"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa21"
      uniprot "NA"
    ]
    graphics [
      x 1502.1817207292956
      y 1512.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_203"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 269
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:refseq:NM_002289;urn:miriam:ensembl:ENSG00000167531;urn:miriam:hgnc.symbol:LALBA;urn:miriam:hgnc.symbol:LALBA;urn:miriam:hgnc:6480;urn:miriam:ncbigene:3906;urn:miriam:uniprot:P00709;urn:miriam:uniprot:P00709;urn:miriam:ncbigene:3906;urn:miriam:ec-code:2.4.1.90;urn:miriam:uniprot:P15291;urn:miriam:uniprot:P15291;urn:miriam:ec-code:2.4.1.275;urn:miriam:hgnc.symbol:B4GALT1;urn:miriam:ensembl:ENSG00000086062;urn:miriam:hgnc.symbol:B4GALT1;urn:miriam:ncbigene:2683;urn:miriam:ncbigene:2683;urn:miriam:refseq:NM_001497;urn:miriam:hgnc:924;urn:miriam:ec-code:2.4.1.-;urn:miriam:ec-code:2.4.1.22;urn:miriam:ec-code:2.4.1.38"
      hgnc "HGNC_SYMBOL:LALBA;HGNC_SYMBOL:B4GALT1"
      map_id "M112_1"
      name "lactose_space_synthetase"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa1"
      uniprot "UNIPROT:P00709;UNIPROT:P15291"
    ]
    graphics [
      x 787.9673850663706
      y 1487.1654913580128
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 270
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:440995;urn:miriam:obo.chebi:CHEBI%3A17716"
      hgnc "NA"
      map_id "M112_192"
      name "Lactose"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa20"
      uniprot "NA"
    ]
    graphics [
      x 1541.4760702265748
      y 1242.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_192"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 271
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:6031;urn:miriam:obo.chebi:CHEBI%3A17659"
      hgnc "NA"
      map_id "M112_213"
      name "UDP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa22"
      uniprot "NA"
    ]
    graphics [
      x 1117.9673850663708
      y 1535.14879033049
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_213"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 272
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ec-code:5.1.3.2;urn:miriam:hgnc.symbol:GALE;urn:miriam:uniprot:Q14376;urn:miriam:uniprot:Q14376;urn:miriam:hgnc.symbol:GALE;urn:miriam:hgnc:4116;urn:miriam:ncbigene:2582;urn:miriam:ncbigene:2582;urn:miriam:ec-code:5.1.3.7;urn:miriam:refseq:NM_000403;urn:miriam:ensembl:ENSG00000117308"
      hgnc "HGNC_SYMBOL:GALE"
      map_id "M112_180"
      name "GALE"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa19"
      uniprot "UNIPROT:Q14376"
    ]
    graphics [
      x 2111.476070226575
      y 1127.7871053251297
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_180"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 273
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A28053;urn:miriam:pubchem.compound:440658"
      hgnc "NA"
      map_id "M112_325"
      name "Melibiose"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa33"
      uniprot "NA"
    ]
    graphics [
      x 1751.4760702265748
      y 1495.9277406352314
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_325"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 274
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962"
      hgnc "NA"
      map_id "M112_336"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa34"
      uniprot "NA"
    ]
    graphics [
      x 2677.9673850663708
      y 1613.0810686173336
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_336"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 275
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc.symbol:GLA;urn:miriam:hgnc.symbol:GLA;urn:miriam:ec-code:3.2.1.22;urn:miriam:ensembl:ENSG00000102393;urn:miriam:ec-code:3.2.1.47;urn:miriam:ncbigene:2717;urn:miriam:ncbigene:2717;urn:miriam:uniprot:P06280;urn:miriam:uniprot:P06280;urn:miriam:refseq:NM_000169;urn:miriam:hgnc:4296"
      hgnc "HGNC_SYMBOL:GLA"
      map_id "M112_395"
      name "GLA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa55"
      uniprot "UNIPROT:P06280"
    ]
    graphics [
      x 2817.299704135862
      y 1357.9004197813094
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_395"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 276
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:doi:10.1101/2020.03.22.002386;urn:miriam:ncbiprotein:YP_009725309;urn:miriam:hgnc.symbol:GLA;urn:miriam:hgnc.symbol:GLA;urn:miriam:ec-code:3.2.1.22;urn:miriam:ensembl:ENSG00000102393;urn:miriam:ec-code:3.2.1.47;urn:miriam:ncbigene:2717;urn:miriam:ncbigene:2717;urn:miriam:uniprot:P06280;urn:miriam:uniprot:P06280;urn:miriam:refseq:NM_000169;urn:miriam:hgnc:4296"
      hgnc "HGNC_SYMBOL:GLA"
      map_id "M112_7"
      name "GLA:Nsp14"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa7"
      uniprot "UNIPROT:P06280"
    ]
    graphics [
      x 2741.476070226575
      y 1395.5665014998212
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 277
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:176077;urn:miriam:pubmed:10866822;urn:miriam:obo.chebi:CHEBI%3A135923"
      hgnc "NA"
      map_id "M112_397"
      name "Migalastat"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa58"
      uniprot "NA"
    ]
    graphics [
      x 2298.9874734832797
      y 2142.945702486371
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_397"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 278
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:79025;urn:miriam:obo.chebi:CHEBI%3A28102"
      hgnc "NA"
      map_id "M112_251"
      name "_alpha__minus_D_minus_Glucose"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa26"
      uniprot "NA"
    ]
    graphics [
      x 910.4146427238683
      y 2194.693598851776
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_251"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 279
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_13"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re114"
      uniprot "NA"
    ]
    graphics [
      x 1916.090149807047
      y 512.8646454446412
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 280
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:doi:10.1101/2020.03.22.002386;urn:miriam:ncbiprotein:YP_009725309"
      hgnc "NA"
      map_id "M112_346"
      name "Nsp14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa35"
      uniprot "NA"
    ]
    graphics [
      x 1958.8536650413898
      y 718.373935738091
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_346"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 281
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16634;urn:miriam:pubchem.compound:439242"
      hgnc "NA"
      map_id "M112_389"
      name "Raffinose"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa49"
      uniprot "NA"
    ]
    graphics [
      x 2617.9673850663708
      y 1759.7862955967187
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_389"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 282
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962"
      hgnc "NA"
      map_id "M112_391"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa50"
      uniprot "NA"
    ]
    graphics [
      x 1421.4760702265748
      y 1373.5181932957498
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_391"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 283
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17992;urn:miriam:pubchem.compound:5988"
      hgnc "NA"
      map_id "M112_392"
      name "Sucrose"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa51"
      uniprot "NA"
    ]
    graphics [
      x 2261.476070226575
      y 1015.9542133851683
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_392"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 284
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962"
      hgnc "NA"
      map_id "M112_240"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa25"
      uniprot "NA"
    ]
    graphics [
      x 730.4146427238683
      y 2227.5625659585758
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_240"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 285
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc:4298;urn:miriam:ec-code:3.2.1.23;urn:miriam:hgnc.symbol:GLB1;urn:miriam:hgnc.symbol:GLB1;urn:miriam:refseq:NM_000404;urn:miriam:ensembl:ENSG00000170266;urn:miriam:uniprot:P16278;urn:miriam:uniprot:P16278;urn:miriam:ncbigene:2720;urn:miriam:ncbigene:2720"
      hgnc "HGNC_SYMBOL:GLB1"
      map_id "M112_231"
      name "GLB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa24"
      uniprot "UNIPROT:P16278"
    ]
    graphics [
      x 1688.8536650413898
      y 732.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_231"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 286
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:refseq:NM_002299;urn:miriam:ensembl:ENSG00000115850;urn:miriam:hgnc:6530;urn:miriam:hgnc.symbol:LCT;urn:miriam:hgnc.symbol:LCT;urn:miriam:ec-code:3.2.1.108;urn:miriam:ncbigene:3938;urn:miriam:ncbigene:3938;urn:miriam:uniprot:P09848;urn:miriam:uniprot:P09848;urn:miriam:ec-code:3.2.1.62"
      hgnc "HGNC_SYMBOL:LCT"
      map_id "M112_396"
      name "LCT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa57"
      uniprot "UNIPROT:P09848"
    ]
    graphics [
      x 281.47607022657485
      y 1181.9364104770766
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_396"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 287
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:439531;urn:miriam:obo.chebi:CHEBI%3A17164"
      hgnc "NA"
      map_id "M112_393"
      name "Stachyose"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa53"
      uniprot "NA"
    ]
    graphics [
      x 2268.9874734832797
      y 2144.4915423693533
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_393"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 288
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962"
      hgnc "NA"
      map_id "M112_394"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa54"
      uniprot "NA"
    ]
    graphics [
      x 1331.4760702265748
      y 1062.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_394"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 289
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962"
      hgnc "NA"
      map_id "M112_113"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa122"
      uniprot "NA"
    ]
    graphics [
      x 2335.1475189347593
      y 583.2552915907766
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 290
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubmed:16690024;urn:miriam:ncbigene:952;urn:miriam:ncbigene:952;urn:miriam:ec-code:3.2.2.6;urn:miriam:hgnc.symbol:CD38;urn:miriam:hgnc.symbol:CD38;urn:miriam:ensembl:ENSG00000004468;urn:miriam:ec-code:2.4.99.20;urn:miriam:hgnc:1667;urn:miriam:refseq:NM_001775;urn:miriam:uniprot:P28907;urn:miriam:uniprot:P28907"
      hgnc "HGNC_SYMBOL:CD38"
      map_id "M112_112"
      name "CD38"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa121"
      uniprot "UNIPROT:P28907"
    ]
    graphics [
      x 1568.505131058976
      y 492.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 291
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24636;urn:miriam:pubchem.compound:1038"
      hgnc "NA"
      map_id "M112_114"
      name "H"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa123"
      uniprot "NA"
    ]
    graphics [
      x 2228.85366504139
      y 593.3659551949684
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 292
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:33576;urn:miriam:obo.chebi:CHEBI%3A16960"
      hgnc "NA"
      map_id "M112_115"
      name "ADP_minus_D_minus_ribose"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa124"
      uniprot "NA"
    ]
    graphics [
      x 2380.9572843570436
      y 550.8175534957655
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 293
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc:7863;urn:miriam:ncbigene:23530;urn:miriam:ncbigene:23530;urn:miriam:ensembl:ENSG00000112992;urn:miriam:hgnc.symbol:NNT;urn:miriam:refseq:NM_182977;urn:miriam:hgnc.symbol:NNT;urn:miriam:uniprot:Q13423;urn:miriam:uniprot:Q13423;urn:miriam:ec-code:7.1.1.1"
      hgnc "HGNC_SYMBOL:NNT"
      map_id "M112_412"
      name "NNT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa80"
      uniprot "UNIPROT:Q13423"
    ]
    graphics [
      x 1280.5824814748505
      y 2742.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_412"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 294
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16908;urn:miriam:pubchem.compound:439153"
      hgnc "NA"
      map_id "M112_410"
      name "NADH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa79"
      uniprot "NA"
    ]
    graphics [
      x 1372.5144998161113
      y 1872.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_410"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 295
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.substance:5094"
      hgnc "NA"
      map_id "M112_106"
      name "Histone_space_N6_minus_acetyl_minus_L_minus_lysine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa115"
      uniprot "NA"
    ]
    graphics [
      x 2707.9673850663708
      y 1712.861065297253
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 296
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962"
      hgnc "NA"
      map_id "M112_109"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa119"
      uniprot "NA"
    ]
    graphics [
      x 1541.4760702265748
      y 1182.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 297
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:uniprot:Q9NXA8;urn:miriam:uniprot:Q9NXA8;urn:miriam:ec-code:2.3.1.-;urn:miriam:pubmed:17694089;urn:miriam:hgnc.symbol:SIRT5;urn:miriam:hgnc.symbol:SIRT5;urn:miriam:hgnc:14933;urn:miriam:ncbigene:23408;urn:miriam:ensembl:ENSG00000124523;urn:miriam:ncbigene:23408;urn:miriam:refseq:NM_001193267"
      hgnc "HGNC_SYMBOL:SIRT5"
      map_id "M112_385"
      name "SIRT5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa403"
      uniprot "UNIPROT:Q9NXA8"
    ]
    graphics [
      x 1646.090149807047
      y 432.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_385"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 298
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:doi:10.1101/2020.03.22.002386;urn:miriam:ncbiprotein:YP_009725309;urn:miriam:uniprot:Q9NXA8;urn:miriam:uniprot:Q9NXA8;urn:miriam:ec-code:2.3.1.-;urn:miriam:pubmed:17694089;urn:miriam:hgnc.symbol:SIRT5;urn:miriam:hgnc.symbol:SIRT5;urn:miriam:hgnc:14933;urn:miriam:ncbigene:23408;urn:miriam:ensembl:ENSG00000124523;urn:miriam:ncbigene:23408;urn:miriam:refseq:NM_001193267"
      hgnc "HGNC_SYMBOL:SIRT5"
      map_id "M112_6"
      name "SIRT5:Nsp14"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa6"
      uniprot "UNIPROT:Q9NXA8"
    ]
    graphics [
      x 1721.4760702265748
      y 1051.3330640269571
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 299
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:72193709;urn:miriam:obo.chebi:CHEBI%3A76279"
      hgnc "NA"
      map_id "M112_108"
      name "O_minus_Acetyl_minus_ADP_minus_ribose"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa117"
      uniprot "NA"
    ]
    graphics [
      x 2647.9673850663708
      y 1651.7464025950017
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 300
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A5738;urn:miriam:pubchem.substance:5447;urn:miriam:pubchem.substance:223439948"
      hgnc "NA"
      map_id "M112_107"
      name "Histone_minus_L_minus_lysine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa116"
      uniprot "NA"
    ]
    graphics [
      x 2381.476070226575
      y 1430.6873388578365
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 301
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_12"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re113"
      uniprot "NA"
    ]
    graphics [
      x 1417.9673850663708
      y 1572.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 302
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:doi:10.1101/2020.03.22.002386;urn:miriam:ncbiprotein:YP_009725309"
      hgnc "NA"
      map_id "M112_111"
      name "Nsp14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa120"
      uniprot "NA"
    ]
    graphics [
      x 892.5144998161113
      y 1918.7663212473685
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 303
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_11"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "re112"
      uniprot "NA"
    ]
    graphics [
      x 1128.4308390063427
      y 276.2838200658982
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 304
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_388"
      name "Urea_space_cycle"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa412"
      uniprot "NA"
    ]
    graphics [
      x 698.8536650413897
      y 1022.514339716394
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_388"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 305
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:135421870;urn:miriam:obo.chebi:CHEBI%3A18304"
      hgnc "NA"
      map_id "M112_404"
      name "Deamino_minus_NAD"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa71"
      uniprot "NA"
    ]
    graphics [
      x 1031.4760702265748
      y 1175.0238894408317
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_404"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 306
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15422;urn:miriam:pubchem.compound:5957"
      hgnc "NA"
      map_id "M112_413"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa81"
      uniprot "NA"
    ]
    graphics [
      x 578.8536650413897
      y 674.2157073442784
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_413"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 307
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18050;urn:miriam:pubchem.compound:5961"
      hgnc "NA"
      map_id "M112_417"
      name "L_minus_Glutamine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa85"
      uniprot "NA"
    ]
    graphics [
      x 1241.4760702265748
      y 1054.7921141676188
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_417"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 308
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962"
      hgnc "NA"
      map_id "M112_419"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa87"
      uniprot "NA"
    ]
    graphics [
      x 1124.2901456472646
      y 600.5543133122671
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_419"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 309
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ncbigene:55191;urn:miriam:ncbigene:55191;urn:miriam:hgnc.symbol:NADSYN1;urn:miriam:hgnc.symbol:NADSYN1;urn:miriam:ec-code:6.3.5.1;urn:miriam:hgnc:29832;urn:miriam:uniprot:Q6IA69;urn:miriam:uniprot:Q6IA69;urn:miriam:pubmed:12547821;urn:miriam:ensembl:ENSG00000172890;urn:miriam:refseq:NM_018161"
      hgnc "HGNC_SYMBOL:NADSYN1"
      map_id "M112_416"
      name "NADSYN1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa84"
      uniprot "UNIPROT:Q6IA69"
    ]
    graphics [
      x 368.85366504138983
      y 759.002492730596
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_416"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 310
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:6022;urn:miriam:obo.chebi:CHEBI%3A16761"
      hgnc "NA"
      map_id "M112_414"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa82"
      uniprot "NA"
    ]
    graphics [
      x 1087.0853710793103
      y 272.30684225815935
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_414"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 311
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24636;urn:miriam:pubchem.compound:1038"
      hgnc "NA"
      map_id "M112_415"
      name "H"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa83"
      uniprot "NA"
    ]
    graphics [
      x 521.3274523997884
      y 490.82883066780676
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_415"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 312
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16015;urn:miriam:pubchem.compound:33032"
      hgnc "NA"
      map_id "M112_418"
      name "L_minus_Glutamate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa86"
      uniprot "NA"
    ]
    graphics [
      x 621.6104592494069
      y 1872.1662202339423
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_418"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 313
    zlevel -1

    cd19dm [
      annotation "PUBMED:13717628"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_32"
      name "ATP:nicotinamide-nucleotide adenylyltransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re35"
      uniprot "NA"
    ]
    graphics [
      x 2591.476070226575
      y 1113.449976181946
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 314
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15763;urn:miriam:pubchem.compound:121992"
      hgnc "NA"
      map_id "M112_405"
      name "Nicotinate_space_D_minus_ribonucleotide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa72"
      uniprot "NA"
    ]
    graphics [
      x 2531.476070226575
      y 1242.6818614454628
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_405"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 315
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24636;urn:miriam:pubchem.compound:1038"
      hgnc "NA"
      map_id "M112_117"
      name "H"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa129"
      uniprot "NA"
    ]
    graphics [
      x 2107.9673850663708
      y 1557.7658795411958
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 316
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15422;urn:miriam:pubchem.compound:5957"
      hgnc "NA"
      map_id "M112_116"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa128"
      uniprot "NA"
    ]
    graphics [
      x 2348.85366504139
      y 841.3530460448193
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 317
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:644102;urn:miriam:obo.chebi:CHEBI%3A18361"
      hgnc "NA"
      map_id "M112_119"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa130"
      uniprot "NA"
    ]
    graphics [
      x 2108.85366504139
      y 564.8606125107739
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 318
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962"
      hgnc "NA"
      map_id "M112_431"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa98"
      uniprot "NA"
    ]
    graphics [
      x 1658.8536650413898
      y 732.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_431"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 319
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ec-code:3.1.3.5;urn:miriam:uniprot:P21589;urn:miriam:uniprot:P21589;urn:miriam:pubmed:2848759;urn:miriam:hgnc:8021;urn:miriam:hgnc.symbol:NT5E;urn:miriam:ncbigene:4907;urn:miriam:hgnc.symbol:NT5E;urn:miriam:ncbigene:4907;urn:miriam:ensembl:ENSG00000135318;urn:miriam:refseq:NM_001204813"
      hgnc "HGNC_SYMBOL:NT5E"
      map_id "M112_92"
      name "NT5E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa100"
      uniprot "UNIPROT:P21589"
    ]
    graphics [
      x 1898.8536650413898
      y 632.8646454446412
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 320
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15927;urn:miriam:pubchem.compound:439924"
      hgnc "NA"
      map_id "M112_400"
      name "N_minus_Ribosyl_minus_nicotinamide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa68"
      uniprot "NA"
    ]
    graphics [
      x 1238.8536650413898
      y 909.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_400"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 321
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:1061;urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "M112_432"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa99"
      uniprot "NA"
    ]
    graphics [
      x 1151.4760702265748
      y 1181.8945685944914
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_432"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 322
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_29"
      name "N-Ribosylnicotinamide:orthophosphate ribosyltransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re31"
      uniprot "NA"
    ]
    graphics [
      x 2231.476070226575
      y 1299.6082873047994
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 323
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:1061;urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "M112_103"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa111"
      uniprot "NA"
    ]
    graphics [
      x 2771.476070226575
      y 1300.2662894366786
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 324
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ensembl:ENSG00000198805;urn:miriam:hgnc.symbol:PNP;urn:miriam:hgnc.symbol:PNP;urn:miriam:ncbigene:4860;urn:miriam:ncbigene:4860;urn:miriam:ec-code:2.4.2.1;urn:miriam:refseq:NM_000270.2;urn:miriam:hgnc:7892;urn:miriam:uniprot:P00491;urn:miriam:uniprot:P00491"
      hgnc "HGNC_SYMBOL:PNP"
      map_id "M112_105"
      name "PNP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa113"
      uniprot "UNIPROT:P00491"
    ]
    graphics [
      x 1441.1528289246933
      y 702.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 325
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:439236;urn:miriam:obo.chebi:CHEBI%3A16300"
      hgnc "NA"
      map_id "M112_104"
      name "_alpha__minus_D_minus_Ribose_space_1_minus_phosphate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa112"
      uniprot "NA"
    ]
    graphics [
      x 1821.6104592494069
      y 1698.1145040299514
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 326
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962"
      hgnc "NA"
      map_id "M112_429"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa96"
      uniprot "NA"
    ]
    graphics [
      x 788.8536650413897
      y 941.3549477797077
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_429"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 327
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc:3356;urn:miriam:ec-code:3.1.4.1;urn:miriam:uniprot:P22413;urn:miriam:uniprot:P22413;urn:miriam:ec-code:3.6.1.9;urn:miriam:ncbigene:5167;urn:miriam:ncbigene:5167;urn:miriam:ensembl:ENSG00000197594;urn:miriam:hgnc.symbol:ENPP1;urn:miriam:refseq:NM_006208;urn:miriam:hgnc.symbol:ENPP1"
      hgnc "HGNC_SYMBOL:ENPP1"
      map_id "M112_427"
      name "ENPP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa94"
      uniprot "UNIPROT:P22413"
    ]
    graphics [
      x 1601.4760702265748
      y 1452.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_427"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 328
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ec-code:3.1.4.1;urn:miriam:hgnc:3358;urn:miriam:ensembl:ENSG00000154269;urn:miriam:ec-code:3.6.1.9;urn:miriam:refseq:NM_005021;urn:miriam:ncbigene:5169;urn:miriam:uniprot:O14638;urn:miriam:uniprot:O14638;urn:miriam:ncbigene:5169;urn:miriam:hgnc.symbol:ENPP3;urn:miriam:hgnc.symbol:ENPP3"
      hgnc "HGNC_SYMBOL:ENPP3"
      map_id "M112_428"
      name "ENPP3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa95"
      uniprot "UNIPROT:O14638"
    ]
    graphics [
      x 1811.4760702265748
      y 1294.8138321341212
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_428"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 329
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16027;urn:miriam:pubchem.compound:6083"
      hgnc "NA"
      map_id "M112_430"
      name "AMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa97"
      uniprot "NA"
    ]
    graphics [
      x 427.96738506637064
      y 1772.5572932182372
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_430"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 330
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15422;urn:miriam:pubchem.compound:5957"
      hgnc "NA"
      map_id "M112_93"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa101"
      uniprot "NA"
    ]
    graphics [
      x 2621.476070226575
      y 1501.7464025950017
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 331
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_96"
      name "NRK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa104"
      uniprot "NA"
    ]
    graphics [
      x 2471.476070226575
      y 1192.8696184345401
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 332
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:6022;urn:miriam:obo.chebi:CHEBI%3A16761"
      hgnc "NA"
      map_id "M112_94"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa102"
      uniprot "NA"
    ]
    graphics [
      x 2411.476070226575
      y 1144.225422605131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 333
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24636;urn:miriam:pubchem.compound:1038"
      hgnc "NA"
      map_id "M112_95"
      name "H"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa103"
      uniprot "NA"
    ]
    graphics [
      x 1267.9673850663708
      y 1692.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 334
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_35"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re38"
      uniprot "NA"
    ]
    graphics [
      x 1970.233733469292
      y 2207.606843746084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 335
    zlevel -1

    cd19dm [
      annotation "PUBMED:11829748"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_26"
      name "2'-phospho-ADP-ribosyl cyclase/2'-phospho-cyclic-ADP-ribose transferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re27"
      uniprot "NA"
    ]
    graphics [
      x 2077.9673850663708
      y 1677.1544028564542
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 336
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:937;urn:miriam:obo.chebi:CHEBI%3A32544"
      hgnc "NA"
      map_id "M112_126"
      name "Nicotinate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa137"
      uniprot "NA"
    ]
    graphics [
      x 1601.4760702265748
      y 1302.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 337
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18009;urn:miriam:pubchem.compound:5886"
      hgnc "NA"
      map_id "M112_97"
      name "NADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa106"
      uniprot "NA"
    ]
    graphics [
      x 2542.5144998161113
      y 1820.127905335228
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 338
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubmed:16690024;urn:miriam:ncbigene:952;urn:miriam:ncbigene:952;urn:miriam:ec-code:3.2.2.6;urn:miriam:hgnc.symbol:CD38;urn:miriam:hgnc.symbol:CD38;urn:miriam:ensembl:ENSG00000004468;urn:miriam:ec-code:2.4.99.20;urn:miriam:hgnc:1667;urn:miriam:refseq:NM_001775;urn:miriam:uniprot:P28907;urn:miriam:uniprot:P28907"
      hgnc "HGNC_SYMBOL:CD38"
      map_id "M112_133"
      name "CD38"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa144"
      uniprot "UNIPROT:P28907"
    ]
    graphics [
      x 1162.5144998161113
      y 1882.7078987280793
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_133"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 339
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:71768143;urn:miriam:obo.chebi:CHEBI%3A75967"
      hgnc "NA"
      map_id "M112_98"
      name "nicotinate_minus_adenine_space_dinucleotide_space_phosphate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa107"
      uniprot "NA"
    ]
    graphics [
      x 1751.4760702265748
      y 1246.8134512202714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 340
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962"
      hgnc "NA"
      map_id "M112_129"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa140"
      uniprot "NA"
    ]
    graphics [
      x 2348.85366504139
      y 721.3530460448193
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_129"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 341
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15422;urn:miriam:pubchem.compound:5957"
      hgnc "NA"
      map_id "M112_128"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa139"
      uniprot "NA"
    ]
    graphics [
      x 2441.476070226575
      y 1077.3923379235007
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 342
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_127"
      name "NAPRT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa138"
      uniprot "NA"
    ]
    graphics [
      x 2141.476070226575
      y 1406.270057164332
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_127"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 343
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:6022;urn:miriam:obo.chebi:CHEBI%3A16761"
      hgnc "NA"
      map_id "M112_130"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa141"
      uniprot "NA"
    ]
    graphics [
      x 1870.4146427238684
      y 2290.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_130"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 344
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:644102;urn:miriam:obo.chebi:CHEBI%3A18361"
      hgnc "NA"
      map_id "M112_131"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa142"
      uniprot "NA"
    ]
    graphics [
      x 1461.6104592494069
      y 1752.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_131"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 345
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:1061;urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "M112_132"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa143"
      uniprot "NA"
    ]
    graphics [
      x 2081.476070226575
      y 1455.8498967042349
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 346
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:1066;urn:miriam:obo.chebi:CHEBI%3A16675"
      hgnc "NA"
      map_id "M112_120"
      name "Quinolinate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa131"
      uniprot "NA"
    ]
    graphics [
      x 1901.4760702265748
      y 1052.6254878021132
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 347
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24636;urn:miriam:pubchem.compound:1038"
      hgnc "NA"
      map_id "M112_123"
      name "H"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa134"
      uniprot "NA"
    ]
    graphics [
      x 757.9673850663706
      y 1673.1178987887638
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 348
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24636;urn:miriam:pubchem.compound:1038"
      hgnc "NA"
      map_id "M112_122"
      name "H"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa133"
      uniprot "NA"
    ]
    graphics [
      x 2017.9673850663708
      y 1535.7137326697948
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 349
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc:9755;urn:miriam:hgnc.symbol:QPRT;urn:miriam:ncbigene:23475;urn:miriam:ensembl:ENSG00000103485;urn:miriam:hgnc.symbol:QPRT;urn:miriam:ncbigene:23475;urn:miriam:ec-code:2.4.2.19;urn:miriam:uniprot:Q15274;urn:miriam:uniprot:Q15274;urn:miriam:refseq:NM_014298"
      hgnc "HGNC_SYMBOL:QPRT"
      map_id "M112_121"
      name "QPRT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa132"
      uniprot "UNIPROT:Q15274"
    ]
    graphics [
      x 1841.4760702265748
      y 1250.6183567721846
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 350
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:644102;urn:miriam:obo.chebi:CHEBI%3A18361"
      hgnc "NA"
      map_id "M112_124"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa135"
      uniprot "NA"
    ]
    graphics [
      x 1061.4760702265748
      y 1099.5029478087847
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_124"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 351
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16526;urn:miriam:pubchem.compound:280"
      hgnc "NA"
      map_id "M112_125"
      name "CO2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa136"
      uniprot "NA"
    ]
    graphics [
      x 1211.4760702265748
      y 1119.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_125"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 352
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A78679;urn:miriam:pubchem.compound:439167"
      hgnc "NA"
      map_id "M112_135"
      name "D_minus_Ribose_space_5P"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa146"
      uniprot "NA"
    ]
    graphics [
      x 1527.2934117326045
      y 1212.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 353
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15422;urn:miriam:pubchem.compound:5957"
      hgnc "NA"
      map_id "M112_139"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa150"
      uniprot "NA"
    ]
    graphics [
      x 1901.4760702265748
      y 1510.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_139"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 354
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:refseq:NM_001204402;urn:miriam:hgnc:9462;urn:miriam:ensembl:ENSG00000147224;urn:miriam:ncbigene:5631;urn:miriam:ncbigene:5631;urn:miriam:ec-code:2.7.6.1;urn:miriam:hgnc.symbol:PRPS1;urn:miriam:uniprot:P60891;urn:miriam:uniprot:P60891;urn:miriam:hgnc.symbol:PRPS1"
      hgnc "HGNC_SYMBOL:PRPS1"
      map_id "M112_137"
      name "PRPS1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa148"
      uniprot "UNIPROT:P60891"
    ]
    graphics [
      x 1807.9673850663708
      y 1543.8597507391748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_137"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 355
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:uniprot:P11908;urn:miriam:uniprot:P11908;urn:miriam:ensembl:ENSG00000101911;urn:miriam:ec-code:2.7.6.1;urn:miriam:ncbigene:5634;urn:miriam:ncbigene:5634;urn:miriam:hgnc:9465;urn:miriam:hgnc.symbol:PRPS2;urn:miriam:refseq:NM_002765;urn:miriam:hgnc.symbol:PRPS2"
      hgnc "HGNC_SYMBOL:PRPS2"
      map_id "M112_138"
      name "PRPS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa149"
      uniprot "UNIPROT:P11908"
    ]
    graphics [
      x 1762.5144998161113
      y 2113.859750739175
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_138"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 356
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc.symbol:PRPS1L1;urn:miriam:hgnc.symbol:PRPS1L1;urn:miriam:ensembl:ENSG00000229937;urn:miriam:ncbigene:221823;urn:miriam:ncbigene:221823;urn:miriam:refseq:NM_175886;urn:miriam:uniprot:P21108;urn:miriam:uniprot:P21108;urn:miriam:ec-code:2.7.6.1;urn:miriam:hgnc:9463"
      hgnc "HGNC_SYMBOL:PRPS1L1"
      map_id "M112_140"
      name "PRPS1L1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa151"
      uniprot "UNIPROT:P21108"
    ]
    graphics [
      x 2123.9845449193244
      y 2350.866652300772
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_140"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 357
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16027;urn:miriam:pubchem.compound:6083"
      hgnc "NA"
      map_id "M112_141"
      name "AMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa152"
      uniprot "NA"
    ]
    graphics [
      x 1987.9673850663708
      y 1637.606843746084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_141"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 358
    zlevel -1

    cd19dm [
      annotation "PUBMED:4992818"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_36"
      name "phosphodeoxyribomutase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re39"
      uniprot "NA"
    ]
    graphics [
      x 1060.4146427238684
      y 2246.2349018995974
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 359
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:439236;urn:miriam:obo.chebi:CHEBI%3A16300"
      hgnc "NA"
      map_id "M112_134"
      name "_alpha_D_minus_Ribose_space_1P"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa145"
      uniprot "NA"
    ]
    graphics [
      x 412.5144998161113
      y 2042.5572932182372
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 360
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:refseq:NM_018290;urn:miriam:uniprot:Q96G03;urn:miriam:uniprot:Q96G03;urn:miriam:ec-code:5.4.2.7;urn:miriam:hgnc:8906;urn:miriam:ensembl:ENSG00000169299;urn:miriam:ec-code:5.4.2.2;urn:miriam:ncbigene:55276;urn:miriam:ncbigene:55276;urn:miriam:hgnc.symbol:PGM2;urn:miriam:hgnc.symbol:PGM2"
      hgnc "HGNC_SYMBOL:PGM2"
      map_id "M112_136"
      name "PGM2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa147"
      uniprot "UNIPROT:Q96G03"
    ]
    graphics [
      x 1542.190987958295
      y 2622.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 361
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16027;urn:miriam:pubchem.compound:6083"
      hgnc "NA"
      map_id "M112_296"
      name "AMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa301"
      uniprot "NA"
    ]
    graphics [
      x 2078.85366504139
      y 784.4203742692995
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_296"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 362
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24636;urn:miriam:pubchem.compound:1038"
      hgnc "NA"
      map_id "M112_297"
      name "H"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa302"
      uniprot "NA"
    ]
    graphics [
      x 1027.9673850663708
      y 1592.3785906698167
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_297"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 363
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962"
      hgnc "NA"
      map_id "M112_298"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa303"
      uniprot "NA"
    ]
    graphics [
      x 2081.476070226575
      y 1183.7082153288716
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_298"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 364
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_300"
      name "AMDP2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa305"
      uniprot "NA"
    ]
    graphics [
      x 2617.9673850663708
      y 1578.994925936687
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_300"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 365
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc:468;urn:miriam:ncbigene:270;urn:miriam:refseq:NM_000036;urn:miriam:ncbigene:270;urn:miriam:ec-code:3.5.4.6;urn:miriam:ensembl:ENSG00000116748;urn:miriam:uniprot:P23109;urn:miriam:uniprot:P23109;urn:miriam:hgnc.symbol:AMPD1;urn:miriam:hgnc.symbol:AMPD1"
      hgnc "HGNC_SYMBOL:AMPD1"
      map_id "M112_299"
      name "AMPD1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa304"
      uniprot "UNIPROT:P23109"
    ]
    graphics [
      x 1477.9673850663708
      y 1692.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_299"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 366
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc.symbol:AMPD3;urn:miriam:hgnc.symbol:AMPD3;urn:miriam:refseq:NM_000480;urn:miriam:ec-code:3.5.4.6;urn:miriam:hgnc:470;urn:miriam:uniprot:Q01432;urn:miriam:uniprot:Q01432;urn:miriam:ensembl:ENSG00000133805;urn:miriam:ncbigene:272;urn:miriam:ncbigene:272"
      hgnc "HGNC_SYMBOL:AMPD3"
      map_id "M112_301"
      name "AMPD3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa306"
      uniprot "UNIPROT:Q01432"
    ]
    graphics [
      x 2437.9673850663708
      y 1577.3236498966521
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_301"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 367
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:223;urn:miriam:obo.chebi:CHEBI%3A28938"
      hgnc "NA"
      map_id "M112_302"
      name "Ammonium"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa307"
      uniprot "NA"
    ]
    graphics [
      x 2438.85366504139
      y 867.6208355784091
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_302"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 368
    zlevel -1

    cd19dm [
      annotation "PUBMED:14392175"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_81"
      name "Adenine phosphoribosyltransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re84"
      uniprot "NA"
    ]
    graphics [
      x 1147.9673850663708
      y 1552.7078987280793
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 369
    zlevel -1

    cd19dm [
      annotation "PUBMED:13549414"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_82"
      name "Adenlyate kinase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re85"
      uniprot "NA"
    ]
    graphics [
      x 2381.476070226575
      y 1340.6873388578365
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 370
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15422;urn:miriam:pubchem.compound:5957"
      hgnc "NA"
      map_id "M112_350"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa354"
      uniprot "NA"
    ]
    graphics [
      x 2048.85366504139
      y 916.2074672174712
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_350"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 371
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc.symbol:AK5;urn:miriam:hgnc.symbol:AK5;urn:miriam:ncbigene:26289;urn:miriam:ncbigene:26289;urn:miriam:ensembl:ENSG00000154027;urn:miriam:ec-code:2.7.4.6;urn:miriam:ec-code:2.7.4.3;urn:miriam:hgnc:365;urn:miriam:uniprot:Q9Y6K8;urn:miriam:uniprot:Q9Y6K8;urn:miriam:refseq:NM_174858"
      hgnc "HGNC_SYMBOL:AK5"
      map_id "M112_380"
      name "AK5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa389"
      uniprot "UNIPROT:Q9Y6K8"
    ]
    graphics [
      x 2258.85366504139
      y 861.6132383117099
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_380"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 372
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:uniprot:Q96M32;urn:miriam:uniprot:Q96M32;urn:miriam:hgnc:20091;urn:miriam:hgnc.symbol:AK7;urn:miriam:hgnc.symbol:AK7;urn:miriam:ncbigene:122481;urn:miriam:ncbigene:122481;urn:miriam:ensembl:ENSG00000140057;urn:miriam:ec-code:2.7.4.6;urn:miriam:ec-code:2.7.4.3;urn:miriam:refseq:NM_001350888"
      hgnc "HGNC_SYMBOL:AK7"
      map_id "M112_382"
      name "AK7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa391"
      uniprot "UNIPROT:Q96M32"
    ]
    graphics [
      x 1912.5144998161113
      y 2020.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_382"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 373
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:uniprot:P00568;urn:miriam:uniprot:P00568;urn:miriam:refseq:NM_000476;urn:miriam:ensembl:ENSG00000106992;urn:miriam:ncbigene:203;urn:miriam:ncbigene:203;urn:miriam:ec-code:2.7.4.6;urn:miriam:hgnc:361;urn:miriam:ec-code:2.7.4.3;urn:miriam:hgnc.symbol:AK1;urn:miriam:hgnc.symbol:AK1"
      hgnc "HGNC_SYMBOL:AK1"
      map_id "M112_383"
      name "AK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa392"
      uniprot "UNIPROT:P00568"
    ]
    graphics [
      x 2201.476070226575
      y 1230.5867359968995
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_383"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 374
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ncbigene:158067;urn:miriam:ncbigene:158067;urn:miriam:hgnc.symbol:AK8;urn:miriam:ensembl:ENSG00000165695;urn:miriam:hgnc.symbol:AK8;urn:miriam:hgnc:26526;urn:miriam:refseq:NM_152572;urn:miriam:ec-code:2.7.4.6;urn:miriam:ec-code:2.7.4.3;urn:miriam:uniprot:Q96MA6;urn:miriam:uniprot:Q96MA6"
      hgnc "HGNC_SYMBOL:AK8"
      map_id "M112_381"
      name "AK8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa390"
      uniprot "UNIPROT:Q96MA6"
    ]
    graphics [
      x 2171.476070226575
      y 1458.7342851305455
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_381"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 375
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:6022;urn:miriam:obo.chebi:CHEBI%3A16761"
      hgnc "NA"
      map_id "M112_349"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa352"
      uniprot "NA"
    ]
    graphics [
      x 1181.4760702265748
      y 1441.4212669072997
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_349"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 376
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:6022;urn:miriam:obo.chebi:CHEBI%3A16761"
      hgnc "NA"
      map_id "M112_379"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa388"
      uniprot "NA"
    ]
    graphics [
      x 2200.4146427238684
      y 2100.897654208821
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_379"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 377
    zlevel -1

    cd19dm [
      annotation "PUBMED:4543472"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_89"
      name "2'-Deoxyadenosine 5'-diphosphate:oxidized-thioredoxin 2'-oxidoreductase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re91"
      uniprot "NA"
    ]
    graphics [
      x 825.5395656209118
      y 2233.853912881765
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 378
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.substance:223441017;urn:miriam:pubchem.substance:3635;urn:miriam:obo.chebi:CHEBI%3A15033"
      hgnc "NA"
      map_id "M112_372"
      name "Thioredoxin"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa376"
      uniprot "NA"
    ]
    graphics [
      x 1150.4146427238684
      y 2199.9418972920985
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_372"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 379
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:refseq:NM_001034;urn:miriam:ncbigene:6241;urn:miriam:ncbigene:6241;urn:miriam:hgnc:10452;urn:miriam:ec-code:1.17.4.1;urn:miriam:hgnc.symbol:RRM2;urn:miriam:hgnc.symbol:RRM2;urn:miriam:ensembl:ENSG00000171848;urn:miriam:uniprot:P31350;urn:miriam:uniprot:P31350;urn:miriam:ensembl:ENSG00000167325;urn:miriam:hgnc.symbol:RRM1;urn:miriam:uniprot:P23921;urn:miriam:uniprot:P23921;urn:miriam:hgnc.symbol:RRM1;urn:miriam:ncbigene:6240;urn:miriam:refseq:NM_001033;urn:miriam:ncbigene:6240;urn:miriam:hgnc:10451;urn:miriam:ec-code:1.17.4.1;urn:miriam:ncbigene:50484;urn:miriam:ncbigene:50484;urn:miriam:ensembl:ENSG00000048392;urn:miriam:hgnc.symbol:RRM2B;urn:miriam:hgnc.symbol:RRM2B;urn:miriam:uniprot:Q7LG56;urn:miriam:uniprot:Q7LG56;urn:miriam:hgnc:17296;urn:miriam:ec-code:1.17.4.1;urn:miriam:refseq:NM_001172477"
      hgnc "HGNC_SYMBOL:RRM2;HGNC_SYMBOL:RRM1;HGNC_SYMBOL:RRM2B"
      map_id "M112_4"
      name "ribonucleoside_space_reductase"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa4"
      uniprot "UNIPROT:P31350;UNIPROT:P23921;UNIPROT:Q7LG56"
    ]
    graphics [
      x 1340.5824814748505
      y 2742.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 380
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16174;urn:miriam:pubchem.compound:188966"
      hgnc "NA"
      map_id "M112_365"
      name "dADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa369"
      uniprot "NA"
    ]
    graphics [
      x 513.5925437269425
      y 2258.9904069238873
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_365"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 381
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18191;urn:miriam:pubchem.substance:11533266;urn:miriam:pubchem.substance:3636"
      hgnc "NA"
      map_id "M112_370"
      name "Thioredoxin_space_disulfide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa374"
      uniprot "NA"
    ]
    graphics [
      x 697.9673850663706
      y 1672.4666947456517
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_370"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 382
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962"
      hgnc "NA"
      map_id "M112_371"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa375"
      uniprot "NA"
    ]
    graphics [
      x 1177.9673850663708
      y 1561.4212669072997
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_371"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 383
    zlevel -1

    cd19dm [
      annotation "PUBMED:5862227"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_86"
      name "adenylate kinase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re89"
      uniprot "NA"
    ]
    graphics [
      x 952.5144998161113
      y 1921.3746439929469
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 384
    zlevel -1

    cd19dm [
      annotation "PUBMED:13211603"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_88"
      name "ATP:dADP phosphotransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re90"
      uniprot "NA"
    ]
    graphics [
      x 965.6939429587638
      y 2381.261126307005
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 385
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15422;urn:miriam:pubchem.compound:5957"
      hgnc "NA"
      map_id "M112_378"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa387"
      uniprot "NA"
    ]
    graphics [
      x 1120.4146427238684
      y 2194.336442634987
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_378"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 386
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc:7850;urn:miriam:uniprot:P22392;urn:miriam:uniprot:P22392;urn:miriam:ec-code:2.7.4.6;urn:miriam:hgnc.symbol:NME2;urn:miriam:ncbigene:4831;urn:miriam:hgnc.symbol:NME2;urn:miriam:ncbigene:4831;urn:miriam:refseq:NM_002512;urn:miriam:ensembl:ENSG00000243678;urn:miriam:ec-code:2.7.13.3;urn:miriam:ensembl:ENSG00000239672;urn:miriam:uniprot:P15531;urn:miriam:uniprot:P15531;urn:miriam:ec-code:2.7.4.6;urn:miriam:hgnc:7849;urn:miriam:hgnc.symbol:NME1;urn:miriam:refseq:NM_000269;urn:miriam:hgnc.symbol:NME1;urn:miriam:ncbigene:4830;urn:miriam:ncbigene:4830"
      hgnc "HGNC_SYMBOL:NME2;HGNC_SYMBOL:NME1"
      map_id "M112_5"
      name "Nucleoside_space_diphosphate_space_kinase"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa5"
      uniprot "UNIPROT:P22392;UNIPROT:P15531"
    ]
    graphics [
      x 1552.5144998161113
      y 1992.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 387
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc:7853;urn:miriam:hgnc.symbol:NME5;urn:miriam:hgnc.symbol:NME5;urn:miriam:ncbigene:8382;urn:miriam:ncbigene:8382;urn:miriam:refseq:NM_003551;urn:miriam:ensembl:ENSG00000112981;urn:miriam:uniprot:P56597;urn:miriam:uniprot:P56597"
      hgnc "HGNC_SYMBOL:NME5"
      map_id "M112_374"
      name "NME5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa381"
      uniprot "UNIPROT:P56597"
    ]
    graphics [
      x 1970.0074341427699
      y 2395.24519483994
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_374"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 388
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ensembl:ENSG00000103024;urn:miriam:hgnc:7851;urn:miriam:hgnc.symbol:NME3;urn:miriam:hgnc.symbol:NME3;urn:miriam:uniprot:Q13232;urn:miriam:uniprot:Q13232;urn:miriam:ec-code:2.7.4.6;urn:miriam:refseq:NM_002513;urn:miriam:ncbigene:4832;urn:miriam:ncbigene:4832"
      hgnc "HGNC_SYMBOL:NME3"
      map_id "M112_373"
      name "NME3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa380"
      uniprot "UNIPROT:Q13232"
    ]
    graphics [
      x 1329.7329175621924
      y 2712.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_373"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 389
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc:20567;urn:miriam:hgnc.symbol:NME6;urn:miriam:hgnc.symbol:NME6;urn:miriam:refseq:NM_005793;urn:miriam:ec-code:2.7.4.6;urn:miriam:ensembl:ENSG00000172113;urn:miriam:ncbigene:10201;urn:miriam:ncbigene:10201;urn:miriam:uniprot:O75414;urn:miriam:uniprot:O75414"
      hgnc "HGNC_SYMBOL:NME6"
      map_id "M112_375"
      name "NME6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa382"
      uniprot "UNIPROT:O75414"
    ]
    graphics [
      x 1457.7845707541828
      y 2112.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_375"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 390
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ncbigene:29922;urn:miriam:ncbigene:29922;urn:miriam:ensembl:ENSG00000143156;urn:miriam:uniprot:Q9Y5B8;urn:miriam:uniprot:Q9Y5B8;urn:miriam:refseq:NM_013330;urn:miriam:hgnc.symbol:NME7;urn:miriam:hgnc.symbol:NME7;urn:miriam:ec-code:2.7.4.6;urn:miriam:hgnc:20461"
      hgnc "HGNC_SYMBOL:NME7"
      map_id "M112_376"
      name "NME7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa383"
      uniprot "UNIPROT:Q9Y5B8"
    ]
    graphics [
      x 521.4760702265747
      y 1256.0389472803913
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_376"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 391
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16284;urn:miriam:pubchem.compound:15993"
      hgnc "NA"
      map_id "M112_369"
      name "dATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa373"
      uniprot "NA"
    ]
    graphics [
      x 967.9673850663706
      y 1659.4280235401561
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_369"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 392
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:6022;urn:miriam:obo.chebi:CHEBI%3A16761"
      hgnc "NA"
      map_id "M112_377"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa386"
      uniprot "NA"
    ]
    graphics [
      x 910.4146427238683
      y 2126.0222828231094
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_377"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 393
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17713;urn:miriam:pubchem.compound:12599"
      hgnc "NA"
      map_id "M112_360"
      name "dAMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa364"
      uniprot "NA"
    ]
    graphics [
      x 1238.8536650413898
      y 819.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_360"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 394
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15422;urn:miriam:pubchem.compound:5957"
      hgnc "NA"
      map_id "M112_367"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa371"
      uniprot "NA"
    ]
    graphics [
      x 442.5144998161113
      y 2072.557293218237
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_367"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 395
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc.symbol:AK5;urn:miriam:hgnc.symbol:AK5;urn:miriam:ncbigene:26289;urn:miriam:ncbigene:26289;urn:miriam:ensembl:ENSG00000154027;urn:miriam:ec-code:2.7.4.6;urn:miriam:ec-code:2.7.4.3;urn:miriam:hgnc:365;urn:miriam:uniprot:Q9Y6K8;urn:miriam:uniprot:Q9Y6K8;urn:miriam:refseq:NM_174858"
      hgnc "HGNC_SYMBOL:AK5"
      map_id "M112_366"
      name "AK5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa370"
      uniprot "UNIPROT:Q9Y6K8"
    ]
    graphics [
      x 2201.476070226575
      y 1363.267404476872
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_366"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 396
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:6022;urn:miriam:obo.chebi:CHEBI%3A16761"
      hgnc "NA"
      map_id "M112_368"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa372"
      uniprot "NA"
    ]
    graphics [
      x 501.61045924940686
      y 1873.8790673158567
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_368"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 397
    zlevel -1

    cd19dm [
      annotation "PUBMED:5667299"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_85"
      name "ATP:deoxyadenosine 5'-phosphotransferase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re88"
      uniprot "NA"
    ]
    graphics [
      x 2108.85366504139
      y 851.4263692502238
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 398
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:13730;urn:miriam:obo.chebi:CHEBI%3A17256"
      hgnc "NA"
      map_id "M112_355"
      name "Deoxyadenosine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa359"
      uniprot "NA"
    ]
    graphics [
      x 2738.85366504139
      y 986.7147759727644
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_355"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 399
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15422;urn:miriam:pubchem.compound:5957"
      hgnc "NA"
      map_id "M112_361"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa365"
      uniprot "NA"
    ]
    graphics [
      x 2548.458436281474
      y 844.7288715512948
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_361"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 400
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc.symbol:DCK;urn:miriam:ncbigene:1633;urn:miriam:hgnc.symbol:DCK;urn:miriam:ncbigene:1633;urn:miriam:ensembl:ENSG00000156136;urn:miriam:ec-code:2.7.1.76;urn:miriam:ec-code:2.7.1.113;urn:miriam:ec-code:2.7.1.74;urn:miriam:hgnc:2704;urn:miriam:uniprot:P27707;urn:miriam:uniprot:P27707;urn:miriam:refseq:NM_000788"
      hgnc "HGNC_SYMBOL:DCK"
      map_id "M112_364"
      name "DCK"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa368"
      uniprot "UNIPROT:P27707"
    ]
    graphics [
      x 2240.1736034760024
      y 527.2677944779601
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_364"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 401
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:6022;urn:miriam:obo.chebi:CHEBI%3A16761"
      hgnc "NA"
      map_id "M112_362"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa366"
      uniprot "NA"
    ]
    graphics [
      x 1919.2104285107553
      y 470.52405752769596
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_362"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 402
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24636;urn:miriam:pubchem.compound:1038"
      hgnc "NA"
      map_id "M112_363"
      name "H"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa367"
      uniprot "NA"
    ]
    graphics [
      x 2407.9673850663708
      y 1702.5551140936402
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_363"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 403
    zlevel -1

    cd19dm [
      annotation "PUBMED:14927650"
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "NA"
      hgnc "NA"
      map_id "M112_84"
      name "Deoxyadenosine aminohydrolase"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re87"
      uniprot "NA"
    ]
    graphics [
      x 2771.476070226575
      y 1437.0751964613532
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 404
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962"
      hgnc "NA"
      map_id "M112_357"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa361"
      uniprot "NA"
    ]
    graphics [
      x 1584.434735187801
      y 1572.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_357"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 405
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24636;urn:miriam:pubchem.compound:1038"
      hgnc "NA"
      map_id "M112_356"
      name "H"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa360"
      uniprot "NA"
    ]
    graphics [
      x 2771.476070226575
      y 1377.6521946854277
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_356"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 406
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:refseq:NM_000022;urn:miriam:ensembl:ENSG00000196839;urn:miriam:hgnc.symbol:ADA;urn:miriam:hgnc.symbol:ADA;urn:miriam:ec-code:3.5.4.4;urn:miriam:hgnc:186;urn:miriam:ncbigene:100;urn:miriam:ncbigene:100;urn:miriam:uniprot:P00813;urn:miriam:uniprot:P00813"
      hgnc "HGNC_SYMBOL:ADA"
      map_id "M112_359"
      name "ADA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa363"
      uniprot "UNIPROT:P00813"
    ]
    graphics [
      x 2819.23898100718
      y 1291.018426271527
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_359"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 407
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A28997;urn:miriam:pubchem.compound:135398593"
      hgnc "NA"
      map_id "M112_351"
      name "Deoxyinosine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa355"
      uniprot "NA"
    ]
    graphics [
      x 1751.4760702265748
      y 1465.288563922968
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_351"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 408
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:223;urn:miriam:obo.chebi:CHEBI%3A28938"
      hgnc "NA"
      map_id "M112_358"
      name "Ammonium"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa362"
      uniprot "NA"
    ]
    graphics [
      x 2618.85366504139
      y 893.8291605697455
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_358"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 409
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:190;urn:miriam:obo.chebi:CHEBI%3A16708"
      hgnc "NA"
      map_id "M112_342"
      name "Adenine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa346"
      uniprot "NA"
    ]
    graphics [
      x 908.8536650413897
      y 1081.8262641726626
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_342"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 410
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17111;urn:miriam:pubchem.compound:7339"
      hgnc "NA"
      map_id "M112_348"
      name "5_minus_phospho_minus__alpha__minus_D_minus_ribose_space_1_minus_diphosphate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa351"
      uniprot "NA"
    ]
    graphics [
      x 851.4760702265747
      y 1232.359520848128
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_348"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 411
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:refseq:NM_000485;urn:miriam:hgnc:626;urn:miriam:uniprot:P07741;urn:miriam:uniprot:P07741;urn:miriam:ensembl:ENSG00000198931;urn:miriam:ncbigene:353;urn:miriam:ncbigene:353;urn:miriam:hgnc.symbol:APRT;urn:miriam:hgnc.symbol:APRT;urn:miriam:ec-code:2.4.2.7"
      hgnc "HGNC_SYMBOL:APRT"
      map_id "M112_347"
      name "APRT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa350"
      uniprot "UNIPROT:P07741"
    ]
    graphics [
      x 637.9673850663706
      y 1602.1662202339423
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_347"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 412
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:644102;urn:miriam:obo.chebi:CHEBI%3A18361"
      hgnc "NA"
      map_id "M112_345"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa349"
      uniprot "NA"
    ]
    graphics [
      x 1342.5144998161113
      y 1992.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_345"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 413
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:5892;urn:miriam:obo.chebi:CHEBI%3A15846"
      hgnc "NA"
      map_id "M112_318"
      name "NAD"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa323"
      uniprot "NA"
    ]
    graphics [
      x 641.4760702265747
      y 1184.0156442515633
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_318"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 414
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962"
      hgnc "NA"
      map_id "M112_319"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa324"
      uniprot "NA"
    ]
    graphics [
      x 788.8536650413897
      y 745.7583197686541
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_319"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 415
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:hgnc.symbol:XDH;urn:miriam:hgnc.symbol:XDH;urn:miriam:uniprot:P47989;urn:miriam:uniprot:P47989;urn:miriam:ensembl:ENSG00000158125;urn:miriam:ncbigene:7498;urn:miriam:ncbigene:7498;urn:miriam:ec-code:1.17.1.4;urn:miriam:ec-code:1.17.3.2;urn:miriam:refseq:NM_000379;urn:miriam:hgnc:12805"
      hgnc "HGNC_SYMBOL:XDH"
      map_id "M112_316"
      name "XDH"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa321"
      uniprot "UNIPROT:P47989"
    ]
    graphics [
      x 1676.090149807047
      y 485.43305053167455
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_316"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 416
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24636;urn:miriam:pubchem.compound:1038"
      hgnc "NA"
      map_id "M112_320"
      name "H"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa325"
      uniprot "NA"
    ]
    graphics [
      x 1721.4760702265748
      y 1126.8134512202714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_320"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 417
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16908;urn:miriam:pubchem.compound:439153"
      hgnc "NA"
      map_id "M112_317"
      name "NADH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa322"
      uniprot "NA"
    ]
    graphics [
      x 1571.4760702265748
      y 1152.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_317"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 418
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:439287;urn:miriam:obo.chebi:CHEBI%3A28542"
      hgnc "NA"
      map_id "M112_353"
      name "2_minus_deoxy_minus__alpha__minus_D_minus_ribose_space_1_minus_phosphate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa357"
      uniprot "NA"
    ]
    graphics [
      x 1510.4146427238684
      y 2322.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_353"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 419
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ensembl:ENSG00000198805;urn:miriam:hgnc.symbol:PNP;urn:miriam:hgnc.symbol:PNP;urn:miriam:ncbigene:4860;urn:miriam:ncbigene:4860;urn:miriam:ec-code:2.4.2.1;urn:miriam:refseq:NM_000270.2;urn:miriam:hgnc:7892;urn:miriam:uniprot:P00491;urn:miriam:uniprot:P00491"
      hgnc "HGNC_SYMBOL:PNP"
      map_id "M112_354"
      name "PNP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa358"
      uniprot "UNIPROT:P00491"
    ]
    graphics [
      x 2077.9673850663708
      y 1545.56494811139
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_354"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 420
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:1061;urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "M112_352"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa356"
      uniprot "NA"
    ]
    graphics [
      x 1867.9673850663708
      y 1607.5195045806881
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_352"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 421
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962"
      hgnc "NA"
      map_id "M112_310"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa315"
      uniprot "NA"
    ]
    graphics [
      x 818.8536650413897
      y 939.5752808432774
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_310"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 422
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ec-code:3.1.3.5;urn:miriam:uniprot:P21589;urn:miriam:uniprot:P21589;urn:miriam:pubmed:2848759;urn:miriam:hgnc:8021;urn:miriam:hgnc.symbol:NT5E;urn:miriam:ncbigene:4907;urn:miriam:hgnc.symbol:NT5E;urn:miriam:ncbigene:4907;urn:miriam:ensembl:ENSG00000135318;urn:miriam:refseq:NM_001204813"
      hgnc "HGNC_SYMBOL:NT5E"
      map_id "M112_311"
      name "NT5E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa316"
      uniprot "UNIPROT:P21589"
    ]
    graphics [
      x 1552.5144998161113
      y 2052.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_311"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 423
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:1061;urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "M112_309"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa314"
      uniprot "NA"
    ]
    graphics [
      x 412.5144998161113
      y 2072.557293218237
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_309"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 424
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15422;urn:miriam:pubchem.compound:5957"
      hgnc "NA"
      map_id "M112_334"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa338"
      uniprot "NA"
    ]
    graphics [
      x 611.4760702265747
      y 1222.3768592927058
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_334"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 425
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:uniprot:P55263;urn:miriam:uniprot:P55263;urn:miriam:hgnc:257;urn:miriam:refseq:NM_006721;urn:miriam:ncbigene:132;urn:miriam:ncbigene:132;urn:miriam:ensembl:ENSG00000156110;urn:miriam:ec-code:2.7.1.20;urn:miriam:hgnc.symbol:ADK;urn:miriam:hgnc.symbol:ADK"
      hgnc "HGNC_SYMBOL:ADK"
      map_id "M112_338"
      name "ADK"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa342"
      uniprot "UNIPROT:P55263"
    ]
    graphics [
      x 1197.7231255590953
      y 400.7819020557358
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_338"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 426
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:6022;urn:miriam:obo.chebi:CHEBI%3A16761"
      hgnc "NA"
      map_id "M112_335"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa339"
      uniprot "NA"
    ]
    graphics [
      x 950.7120538352644
      y 435.8536138650678
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_335"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 427
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24636;urn:miriam:pubchem.compound:1038"
      hgnc "NA"
      map_id "M112_337"
      name "H"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa341"
      uniprot "NA"
    ]
    graphics [
      x 2138.85366504139
      y 953.7413193457967
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_337"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 428
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:1061;urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "M112_343"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa347"
      uniprot "NA"
    ]
    graphics [
      x 2348.85366504139
      y 781.3530460448193
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_343"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 429
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ensembl:ENSG00000198805;urn:miriam:hgnc.symbol:PNP;urn:miriam:hgnc.symbol:PNP;urn:miriam:ncbigene:4860;urn:miriam:ncbigene:4860;urn:miriam:ec-code:2.4.2.1;urn:miriam:refseq:NM_000270.2;urn:miriam:hgnc:7892;urn:miriam:uniprot:P00491;urn:miriam:uniprot:P00491"
      hgnc "HGNC_SYMBOL:PNP"
      map_id "M112_344"
      name "PNP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa348"
      uniprot "UNIPROT:P00491"
    ]
    graphics [
      x 2468.85366504139
      y 936.2652630185033
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_344"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 430
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:pubchem.compound:962"
      hgnc "NA"
      map_id "M112_340"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa344"
      uniprot "NA"
    ]
    graphics [
      x 1631.4760702265748
      y 1444.8976398879274
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_340"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 431
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:ec-code:3.1.3.5;urn:miriam:uniprot:P21589;urn:miriam:uniprot:P21589;urn:miriam:pubmed:2848759;urn:miriam:hgnc:8021;urn:miriam:hgnc.symbol:NT5E;urn:miriam:ncbigene:4907;urn:miriam:hgnc.symbol:NT5E;urn:miriam:ncbigene:4907;urn:miriam:ensembl:ENSG00000135318;urn:miriam:refseq:NM_001204813"
      hgnc "HGNC_SYMBOL:NT5E"
      map_id "M112_339"
      name "NT5E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa343"
      uniprot "UNIPROT:P21589"
    ]
    graphics [
      x 1397.7845707541828
      y 2052.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_339"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 432
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp14 and metabolism"
      full_annotation "urn:miriam:pubchem.compound:1061;urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "M112_341"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa345"
      uniprot "NA"
    ]
    graphics [
      x 2618.85366504139
      y 977.7320079204553
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M112_341"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 433
    source 2
    target 1
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_79"
      target_id "M112_329"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 434
    source 1
    target 3
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_329"
      target_id "M112_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 435
    source 1
    target 4
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_329"
      target_id "M112_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 436
    source 1
    target 5
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_329"
      target_id "M112_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 437
    source 361
    target 2
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_296"
      target_id "M112_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 438
    source 430
    target 2
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_340"
      target_id "M112_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 439
    source 431
    target 2
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_339"
      target_id "M112_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 440
    source 2
    target 432
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_79"
      target_id "M112_341"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 441
    source 428
    target 3
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_343"
      target_id "M112_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 442
    source 429
    target 3
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_344"
      target_id "M112_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 443
    source 3
    target 409
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_80"
      target_id "M112_342"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 444
    source 3
    target 16
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_80"
      target_id "M112_313"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 445
    source 424
    target 4
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_334"
      target_id "M112_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 446
    source 425
    target 4
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_338"
      target_id "M112_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 447
    source 4
    target 361
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_78"
      target_id "M112_296"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 448
    source 4
    target 426
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_78"
      target_id "M112_335"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 449
    source 4
    target 427
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_78"
      target_id "M112_337"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 450
    source 6
    target 5
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_331"
      target_id "M112_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 451
    source 7
    target 5
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_332"
      target_id "M112_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 452
    source 8
    target 5
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_333"
      target_id "M112_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 453
    source 5
    target 9
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_77"
      target_id "M112_308"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 454
    source 5
    target 10
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_77"
      target_id "M112_330"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 455
    source 11
    target 9
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_71"
      target_id "M112_308"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 456
    source 9
    target 12
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_308"
      target_id "M112_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 457
    source 22
    target 11
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_195"
      target_id "M112_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 458
    source 421
    target 11
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_310"
      target_id "M112_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 459
    source 422
    target 11
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_311"
      target_id "M112_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 460
    source 11
    target 423
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_71"
      target_id "M112_309"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 461
    source 13
    target 12
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_314"
      target_id "M112_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 462
    source 14
    target 12
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_315"
      target_id "M112_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 463
    source 12
    target 15
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_72"
      target_id "M112_312"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 464
    source 12
    target 16
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_72"
      target_id "M112_313"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 465
    source 14
    target 45
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_315"
      target_id "M112_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 466
    source 15
    target 17
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_312"
      target_id "M112_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 467
    source 15
    target 18
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_312"
      target_id "M112_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 468
    source 15
    target 19
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_312"
      target_id "M112_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 469
    source 418
    target 17
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_353"
      target_id "M112_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 470
    source 419
    target 17
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_354"
      target_id "M112_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 471
    source 17
    target 407
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_83"
      target_id "M112_351"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 472
    source 17
    target 420
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_83"
      target_id "M112_352"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 473
    source 413
    target 18
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_318"
      target_id "M112_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 474
    source 414
    target 18
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_319"
      target_id "M112_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 475
    source 415
    target 18
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_316"
      target_id "M112_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 476
    source 18
    target 47
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_73"
      target_id "M112_304"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 477
    source 18
    target 416
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_73"
      target_id "M112_320"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 478
    source 18
    target 417
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_73"
      target_id "M112_317"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 479
    source 20
    target 19
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_326"
      target_id "M112_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 480
    source 21
    target 19
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_328"
      target_id "M112_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 481
    source 19
    target 22
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_75"
      target_id "M112_195"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 482
    source 19
    target 23
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_75"
      target_id "M112_327"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 483
    source 24
    target 22
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_68"
      target_id "M112_195"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 484
    source 25
    target 22
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_49"
      target_id "M112_195"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 485
    source 22
    target 26
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_195"
      target_id "M112_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 486
    source 22
    target 27
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_195"
      target_id "M112_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 487
    source 361
    target 24
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_296"
      target_id "M112_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 488
    source 362
    target 24
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_297"
      target_id "M112_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 489
    source 363
    target 24
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_298"
      target_id "M112_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 490
    source 364
    target 24
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_300"
      target_id "M112_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 491
    source 365
    target 24
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_299"
      target_id "M112_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 492
    source 366
    target 24
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_301"
      target_id "M112_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 493
    source 24
    target 367
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_68"
      target_id "M112_302"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 494
    source 150
    target 25
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_189"
      target_id "M112_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 495
    source 151
    target 25
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_190"
      target_id "M112_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 496
    source 25
    target 152
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_49"
      target_id "M112_196"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 497
    source 143
    target 26
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_287"
      target_id "M112_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 498
    source 144
    target 26
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_289"
      target_id "M112_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 499
    source 145
    target 26
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_292"
      target_id "M112_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 500
    source 146
    target 26
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_295"
      target_id "M112_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 501
    source 26
    target 59
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_66"
      target_id "M112_208"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 502
    source 26
    target 147
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_66"
      target_id "M112_288"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 503
    source 26
    target 148
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_66"
      target_id "M112_291"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 504
    source 26
    target 149
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_66"
      target_id "M112_290"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 505
    source 28
    target 27
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_198"
      target_id "M112_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 506
    source 29
    target 27
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_201"
      target_id "M112_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 507
    source 30
    target 27
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_204"
      target_id "M112_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 508
    source 31
    target 27
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_202"
      target_id "M112_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 509
    source 32
    target 27
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "MODULATION"
      source_id "M112_8"
      target_id "M112_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 510
    source 33
    target 27
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "INHIBITION"
      source_id "M112_206"
      target_id "M112_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 511
    source 34
    target 27
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "INHIBITION"
      source_id "M112_205"
      target_id "M112_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 512
    source 35
    target 27
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "INHIBITION"
      source_id "M112_207"
      target_id "M112_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 513
    source 27
    target 36
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_50"
      target_id "M112_197"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 514
    source 27
    target 37
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_50"
      target_id "M112_200"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 515
    source 27
    target 38
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_50"
      target_id "M112_199"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 516
    source 31
    target 141
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_202"
      target_id "M112_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 517
    source 31
    target 139
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_202"
      target_id "M112_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 518
    source 139
    target 32
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_14"
      target_id "M112_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 519
    source 36
    target 39
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_197"
      target_id "M112_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 520
    source 36
    target 40
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_197"
      target_id "M112_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 521
    source 41
    target 39
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_212"
      target_id "M112_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 522
    source 131
    target 39
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_210"
      target_id "M112_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 523
    source 132
    target 39
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_211"
      target_id "M112_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 524
    source 133
    target 39
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_209"
      target_id "M112_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 525
    source 39
    target 59
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_51"
      target_id "M112_208"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 526
    source 39
    target 134
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_51"
      target_id "M112_217"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 527
    source 39
    target 135
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_51"
      target_id "M112_218"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 528
    source 39
    target 136
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_51"
      target_id "M112_214"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 529
    source 39
    target 137
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_51"
      target_id "M112_215"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 530
    source 39
    target 138
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_51"
      target_id "M112_216"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 531
    source 41
    target 40
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_212"
      target_id "M112_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 532
    source 42
    target 40
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_267"
      target_id "M112_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 533
    source 40
    target 43
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_69"
      target_id "M112_303"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 534
    source 40
    target 44
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_69"
      target_id "M112_306"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 535
    source 42
    target 64
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_267"
      target_id "M112_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 536
    source 43
    target 45
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_303"
      target_id "M112_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 537
    source 46
    target 45
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_307"
      target_id "M112_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 538
    source 45
    target 47
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_70"
      target_id "M112_304"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 539
    source 45
    target 48
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_70"
      target_id "M112_264"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 540
    source 55
    target 47
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_74"
      target_id "M112_304"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 541
    source 49
    target 48
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_61"
      target_id "M112_264"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 542
    source 50
    target 49
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_248"
      target_id "M112_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 543
    source 51
    target 49
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_263"
      target_id "M112_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 544
    source 52
    target 49
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_249"
      target_id "M112_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 545
    source 49
    target 53
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_61"
      target_id "M112_247"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 546
    source 64
    target 50
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_62"
      target_id "M112_248"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 547
    source 52
    target 54
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_249"
      target_id "M112_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 548
    source 54
    target 53
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_58"
      target_id "M112_247"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 549
    source 53
    target 55
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_247"
      target_id "M112_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 550
    source 53
    target 56
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_247"
      target_id "M112_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 551
    source 99
    target 54
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_242"
      target_id "M112_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 552
    source 129
    target 54
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_250"
      target_id "M112_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 553
    source 54
    target 130
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_58"
      target_id "M112_252"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 554
    source 125
    target 55
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_323"
      target_id "M112_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 555
    source 126
    target 55
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_322"
      target_id "M112_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 556
    source 127
    target 55
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_321"
      target_id "M112_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 557
    source 55
    target 128
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_74"
      target_id "M112_324"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 558
    source 57
    target 56
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_261"
      target_id "M112_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 559
    source 58
    target 56
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_259"
      target_id "M112_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 560
    source 56
    target 59
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_60"
      target_id "M112_208"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 561
    source 56
    target 60
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_60"
      target_id "M112_260"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 562
    source 61
    target 59
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_64"
      target_id "M112_208"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 563
    source 62
    target 59
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_65"
      target_id "M112_208"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 564
    source 59
    target 63
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_208"
      target_id "M112_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 565
    source 59
    target 64
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_208"
      target_id "M112_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 566
    source 80
    target 61
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_223"
      target_id "M112_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 567
    source 120
    target 61
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_269"
      target_id "M112_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 568
    source 109
    target 61
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_268"
      target_id "M112_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 569
    source 61
    target 121
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_64"
      target_id "M112_270"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 570
    source 61
    target 122
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_64"
      target_id "M112_272"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 571
    source 61
    target 123
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_64"
      target_id "M112_273"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 572
    source 61
    target 124
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_64"
      target_id "M112_274"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 573
    source 69
    target 62
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_219"
      target_id "M112_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 574
    source 113
    target 62
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_279"
      target_id "M112_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 575
    source 109
    target 62
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_268"
      target_id "M112_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 576
    source 114
    target 62
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_283"
      target_id "M112_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 577
    source 115
    target 62
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_284"
      target_id "M112_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 578
    source 116
    target 62
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_285"
      target_id "M112_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 579
    source 117
    target 62
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_286"
      target_id "M112_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 580
    source 62
    target 118
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_65"
      target_id "M112_280"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 581
    source 62
    target 119
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_65"
      target_id "M112_281"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 582
    source 67
    target 63
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_221"
      target_id "M112_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 583
    source 68
    target 63
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_220"
      target_id "M112_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 584
    source 63
    target 69
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_52"
      target_id "M112_219"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 585
    source 63
    target 70
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_52"
      target_id "M112_222"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 586
    source 65
    target 64
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_265"
      target_id "M112_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 587
    source 64
    target 66
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_62"
      target_id "M112_266"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 588
    source 68
    target 96
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_220"
      target_id "M112_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 589
    source 71
    target 69
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_63"
      target_id "M112_219"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 590
    source 69
    target 72
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_219"
      target_id "M112_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 591
    source 69
    target 73
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_219"
      target_id "M112_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 592
    source 80
    target 71
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_223"
      target_id "M112_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 593
    source 108
    target 71
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_276"
      target_id "M112_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 594
    source 109
    target 71
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_268"
      target_id "M112_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 595
    source 110
    target 71
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_275"
      target_id "M112_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 596
    source 71
    target 111
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_63"
      target_id "M112_278"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 597
    source 71
    target 112
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_63"
      target_id "M112_277"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 598
    source 104
    target 72
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_233"
      target_id "M112_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 599
    source 105
    target 72
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_3"
      target_id "M112_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 600
    source 72
    target 83
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_54"
      target_id "M112_230"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 601
    source 72
    target 106
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_54"
      target_id "M112_232"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 602
    source 72
    target 107
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_54"
      target_id "M112_234"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 603
    source 74
    target 73
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_224"
      target_id "M112_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 604
    source 75
    target 73
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_226"
      target_id "M112_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 605
    source 76
    target 73
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_2"
      target_id "M112_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 606
    source 77
    target 73
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_227"
      target_id "M112_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 607
    source 78
    target 73
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_228"
      target_id "M112_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 608
    source 79
    target 73
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_229"
      target_id "M112_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 609
    source 73
    target 80
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_53"
      target_id "M112_223"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 610
    source 73
    target 81
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_53"
      target_id "M112_225"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 611
    source 75
    target 82
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_226"
      target_id "M112_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 612
    source 76
    target 82
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_2"
      target_id "M112_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 613
    source 77
    target 82
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_227"
      target_id "M112_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 614
    source 78
    target 82
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_228"
      target_id "M112_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 615
    source 79
    target 82
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_229"
      target_id "M112_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 616
    source 83
    target 82
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_230"
      target_id "M112_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 617
    source 84
    target 82
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_236"
      target_id "M112_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 618
    source 82
    target 85
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_55"
      target_id "M112_235"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 619
    source 82
    target 86
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_55"
      target_id "M112_237"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 620
    source 96
    target 83
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_56"
      target_id "M112_230"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 621
    source 85
    target 87
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_235"
      target_id "M112_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 622
    source 88
    target 87
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_255"
      target_id "M112_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 623
    source 89
    target 87
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_254"
      target_id "M112_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 624
    source 90
    target 87
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_253"
      target_id "M112_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 625
    source 91
    target 87
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_256"
      target_id "M112_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 626
    source 87
    target 92
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_59"
      target_id "M112_238"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 627
    source 87
    target 93
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_59"
      target_id "M112_257"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 628
    source 87
    target 94
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_59"
      target_id "M112_258"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 629
    source 95
    target 92
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_57"
      target_id "M112_238"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 630
    source 92
    target 96
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_238"
      target_id "M112_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 631
    source 99
    target 95
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_242"
      target_id "M112_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 632
    source 100
    target 95
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_243"
      target_id "M112_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 633
    source 101
    target 95
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_246"
      target_id "M112_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 634
    source 95
    target 102
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_57"
      target_id "M112_244"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 635
    source 95
    target 103
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_57"
      target_id "M112_245"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 636
    source 97
    target 96
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_239"
      target_id "M112_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 637
    source 96
    target 98
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_56"
      target_id "M112_241"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 638
    source 140
    target 139
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_386"
      target_id "M112_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 639
    source 141
    target 142
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_10"
      target_id "M112_387"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 640
    source 153
    target 150
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_47"
      target_id "M112_189"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 641
    source 151
    target 153
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_190"
      target_id "M112_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 642
    source 154
    target 153
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_186"
      target_id "M112_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 643
    source 155
    target 153
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_194"
      target_id "M112_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 644
    source 153
    target 156
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_47"
      target_id "M112_193"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 645
    source 157
    target 154
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_46"
      target_id "M112_186"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 646
    source 158
    target 157
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_179"
      target_id "M112_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 647
    source 159
    target 157
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_188"
      target_id "M112_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 648
    source 157
    target 160
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_46"
      target_id "M112_187"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 649
    source 161
    target 158
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_45"
      target_id "M112_179"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 650
    source 162
    target 161
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_174"
      target_id "M112_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 651
    source 163
    target 161
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_182"
      target_id "M112_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 652
    source 164
    target 161
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_185"
      target_id "M112_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 653
    source 165
    target 161
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_175"
      target_id "M112_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 654
    source 161
    target 166
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_45"
      target_id "M112_183"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 655
    source 161
    target 167
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_45"
      target_id "M112_181"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 656
    source 161
    target 168
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_45"
      target_id "M112_184"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 657
    source 169
    target 162
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_44"
      target_id "M112_174"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 658
    source 165
    target 169
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_175"
      target_id "M112_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 659
    source 170
    target 169
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_168"
      target_id "M112_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 660
    source 171
    target 169
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_176"
      target_id "M112_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 661
    source 169
    target 172
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_44"
      target_id "M112_178"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 662
    source 169
    target 173
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_44"
      target_id "M112_177"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 663
    source 174
    target 170
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_43"
      target_id "M112_168"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 664
    source 175
    target 174
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_159"
      target_id "M112_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 665
    source 176
    target 174
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_170"
      target_id "M112_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 666
    source 177
    target 174
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_149"
      target_id "M112_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 667
    source 174
    target 178
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_43"
      target_id "M112_171"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 668
    source 174
    target 179
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_43"
      target_id "M112_173"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 669
    source 174
    target 180
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_43"
      target_id "M112_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 670
    source 188
    target 175
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_42"
      target_id "M112_159"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 671
    source 177
    target 181
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_149"
      target_id "M112_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 672
    source 177
    target 182
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_149"
      target_id "M112_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 673
    source 197
    target 181
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_142"
      target_id "M112_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 674
    source 198
    target 181
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_150"
      target_id "M112_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 675
    source 199
    target 181
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_151"
      target_id "M112_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 676
    source 181
    target 183
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_40"
      target_id "M112_148"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 677
    source 181
    target 200
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_40"
      target_id "M112_152"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 678
    source 181
    target 201
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_40"
      target_id "M112_153"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 679
    source 181
    target 202
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_40"
      target_id "M112_154"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 680
    source 183
    target 182
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_148"
      target_id "M112_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 681
    source 184
    target 182
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_156"
      target_id "M112_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 682
    source 182
    target 185
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_41"
      target_id "M112_155"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 683
    source 182
    target 186
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_41"
      target_id "M112_157"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 684
    source 182
    target 187
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_41"
      target_id "M112_158"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 685
    source 185
    target 188
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_155"
      target_id "M112_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 686
    source 189
    target 188
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_162"
      target_id "M112_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 687
    source 190
    target 188
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_163"
      target_id "M112_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 688
    source 191
    target 188
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_161"
      target_id "M112_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 689
    source 192
    target 188
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_160"
      target_id "M112_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 690
    source 188
    target 193
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_42"
      target_id "M112_166"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 691
    source 188
    target 194
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_42"
      target_id "M112_164"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 692
    source 188
    target 195
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_42"
      target_id "M112_167"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 693
    source 188
    target 196
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_42"
      target_id "M112_165"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 694
    source 203
    target 197
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_39"
      target_id "M112_142"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 695
    source 204
    target 203
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_102"
      target_id "M112_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 696
    source 205
    target 203
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_145"
      target_id "M112_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 697
    source 206
    target 203
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_144"
      target_id "M112_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 698
    source 207
    target 203
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_147"
      target_id "M112_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 699
    source 203
    target 208
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_39"
      target_id "M112_143"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 700
    source 203
    target 209
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_39"
      target_id "M112_146"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 701
    source 210
    target 204
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_38"
      target_id "M112_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 702
    source 204
    target 211
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_102"
      target_id "M112_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 703
    source 204
    target 212
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_102"
      target_id "M112_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 704
    source 204
    target 213
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_102"
      target_id "M112_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 705
    source 352
    target 210
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_135"
      target_id "M112_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 706
    source 353
    target 210
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_139"
      target_id "M112_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 707
    source 354
    target 210
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_137"
      target_id "M112_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 708
    source 355
    target 210
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_138"
      target_id "M112_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 709
    source 356
    target 210
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_140"
      target_id "M112_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 710
    source 210
    target 357
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_38"
      target_id "M112_141"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 711
    source 346
    target 211
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_120"
      target_id "M112_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 712
    source 347
    target 211
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_123"
      target_id "M112_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 713
    source 348
    target 211
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_122"
      target_id "M112_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 714
    source 349
    target 211
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_121"
      target_id "M112_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 715
    source 211
    target 314
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_33"
      target_id "M112_405"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 716
    source 211
    target 350
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_33"
      target_id "M112_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 717
    source 211
    target 351
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_33"
      target_id "M112_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 718
    source 336
    target 212
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_126"
      target_id "M112_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 719
    source 340
    target 212
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_129"
      target_id "M112_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 720
    source 341
    target 212
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_128"
      target_id "M112_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 721
    source 342
    target 212
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_127"
      target_id "M112_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 722
    source 212
    target 314
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_34"
      target_id "M112_405"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 723
    source 212
    target 343
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_34"
      target_id "M112_130"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 724
    source 212
    target 344
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_34"
      target_id "M112_131"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 725
    source 212
    target 345
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_34"
      target_id "M112_132"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 726
    source 214
    target 213
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_399"
      target_id "M112_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 727
    source 215
    target 213
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_99"
      target_id "M112_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 728
    source 213
    target 216
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_27"
      target_id "M112_401"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 729
    source 213
    target 217
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_27"
      target_id "M112_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 730
    source 334
    target 214
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_35"
      target_id "M112_399"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 731
    source 230
    target 214
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_30"
      target_id "M112_399"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 732
    source 232
    target 214
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_31"
      target_id "M112_399"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 733
    source 335
    target 214
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_26"
      target_id "M112_399"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 734
    source 322
    target 214
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_29"
      target_id "M112_399"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 735
    source 218
    target 216
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_24"
      target_id "M112_401"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 736
    source 219
    target 216
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_23"
      target_id "M112_401"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 737
    source 216
    target 220
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_401"
      target_id "M112_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 738
    source 216
    target 221
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_401"
      target_id "M112_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 739
    source 320
    target 218
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_400"
      target_id "M112_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 740
    source 330
    target 218
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_93"
      target_id "M112_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 741
    source 331
    target 218
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_96"
      target_id "M112_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 742
    source 218
    target 332
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_24"
      target_id "M112_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 743
    source 218
    target 333
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_24"
      target_id "M112_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 744
    source 227
    target 219
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_403"
      target_id "M112_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 745
    source 326
    target 219
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_429"
      target_id "M112_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 746
    source 327
    target 219
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_427"
      target_id "M112_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 747
    source 328
    target 219
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_428"
      target_id "M112_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 748
    source 219
    target 329
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_23"
      target_id "M112_430"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 749
    source 318
    target 220
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_431"
      target_id "M112_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 750
    source 319
    target 220
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_92"
      target_id "M112_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 751
    source 220
    target 320
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_25"
      target_id "M112_400"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 752
    source 220
    target 321
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_25"
      target_id "M112_432"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 753
    source 222
    target 221
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_420"
      target_id "M112_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 754
    source 223
    target 221
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_421"
      target_id "M112_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 755
    source 224
    target 221
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_424"
      target_id "M112_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 756
    source 225
    target 221
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_425"
      target_id "M112_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 757
    source 226
    target 221
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_426"
      target_id "M112_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 758
    source 221
    target 227
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_22"
      target_id "M112_403"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 759
    source 221
    target 228
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_22"
      target_id "M112_423"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 760
    source 224
    target 313
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_424"
      target_id "M112_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 761
    source 225
    target 313
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_425"
      target_id "M112_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 762
    source 226
    target 313
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_426"
      target_id "M112_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 763
    source 229
    target 227
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_21"
      target_id "M112_403"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 764
    source 227
    target 230
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_403"
      target_id "M112_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 765
    source 227
    target 231
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_403"
      target_id "M112_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 766
    source 227
    target 232
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_403"
      target_id "M112_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 767
    source 227
    target 233
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_403"
      target_id "M112_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 768
    source 305
    target 229
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_404"
      target_id "M112_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 769
    source 306
    target 229
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_413"
      target_id "M112_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 770
    source 307
    target 229
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_417"
      target_id "M112_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 771
    source 308
    target 229
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_419"
      target_id "M112_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 772
    source 309
    target 229
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_416"
      target_id "M112_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 773
    source 229
    target 310
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_21"
      target_id "M112_414"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 774
    source 229
    target 311
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_21"
      target_id "M112_415"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 775
    source 229
    target 312
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_21"
      target_id "M112_418"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 776
    source 295
    target 230
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_106"
      target_id "M112_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 777
    source 296
    target 230
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_109"
      target_id "M112_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 778
    source 297
    target 230
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_385"
      target_id "M112_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 779
    source 298
    target 230
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "MODULATION"
      source_id "M112_6"
      target_id "M112_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 780
    source 230
    target 299
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_30"
      target_id "M112_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 781
    source 230
    target 300
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_30"
      target_id "M112_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 782
    source 243
    target 231
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_294"
      target_id "M112_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 783
    source 293
    target 231
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_412"
      target_id "M112_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 784
    source 231
    target 294
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_20"
      target_id "M112_410"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 785
    source 231
    target 236
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_20"
      target_id "M112_282"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 786
    source 289
    target 232
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_113"
      target_id "M112_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 787
    source 290
    target 232
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_112"
      target_id "M112_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 788
    source 232
    target 291
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_31"
      target_id "M112_114"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 789
    source 232
    target 292
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_31"
      target_id "M112_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 790
    source 234
    target 233
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_407"
      target_id "M112_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 791
    source 235
    target 233
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_406"
      target_id "M112_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 792
    source 233
    target 236
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_19"
      target_id "M112_282"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 793
    source 233
    target 237
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_19"
      target_id "M112_408"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 794
    source 233
    target 238
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_19"
      target_id "M112_409"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 795
    source 236
    target 239
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_282"
      target_id "M112_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 796
    source 240
    target 239
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_262"
      target_id "M112_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 797
    source 241
    target 239
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_271"
      target_id "M112_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 798
    source 239
    target 242
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_76"
      target_id "M112_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 799
    source 239
    target 243
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_76"
      target_id "M112_294"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 800
    source 239
    target 244
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_76"
      target_id "M112_305"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 801
    source 245
    target 242
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_17"
      target_id "M112_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 802
    source 246
    target 242
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_67"
      target_id "M112_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 803
    source 247
    target 242
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_15"
      target_id "M112_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 804
    source 248
    target 242
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_87"
      target_id "M112_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 805
    source 242
    target 249
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_90"
      target_id "M112_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 806
    source 287
    target 245
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_393"
      target_id "M112_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 807
    source 288
    target 245
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_394"
      target_id "M112_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 808
    source 275
    target 245
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_395"
      target_id "M112_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 809
    source 276
    target 245
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "MODULATION"
      source_id "M112_7"
      target_id "M112_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 810
    source 277
    target 245
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "INHIBITION"
      source_id "M112_397"
      target_id "M112_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 811
    source 245
    target 281
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_17"
      target_id "M112_389"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 812
    source 270
    target 246
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_192"
      target_id "M112_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 813
    source 284
    target 246
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_240"
      target_id "M112_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 814
    source 285
    target 246
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_231"
      target_id "M112_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 815
    source 286
    target 246
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_396"
      target_id "M112_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 816
    source 246
    target 278
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_67"
      target_id "M112_251"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 817
    source 281
    target 247
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_389"
      target_id "M112_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 818
    source 282
    target 247
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_391"
      target_id "M112_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 819
    source 275
    target 247
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_395"
      target_id "M112_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 820
    source 276
    target 247
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "MODULATION"
      source_id "M112_7"
      target_id "M112_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 821
    source 277
    target 247
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "INHIBITION"
      source_id "M112_397"
      target_id "M112_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 822
    source 247
    target 283
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_15"
      target_id "M112_392"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 823
    source 273
    target 248
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_325"
      target_id "M112_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 824
    source 274
    target 248
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_336"
      target_id "M112_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 825
    source 275
    target 248
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_395"
      target_id "M112_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 826
    source 276
    target 248
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "MODULATION"
      source_id "M112_7"
      target_id "M112_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 827
    source 277
    target 248
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "INHIBITION"
      source_id "M112_397"
      target_id "M112_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 828
    source 248
    target 278
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_87"
      target_id "M112_251"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 829
    source 250
    target 249
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_293"
      target_id "M112_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 830
    source 249
    target 251
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_9"
      target_id "M112_191"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 831
    source 251
    target 252
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_191"
      target_id "M112_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 832
    source 253
    target 252
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_398"
      target_id "M112_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 833
    source 254
    target 252
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_390"
      target_id "M112_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 834
    source 252
    target 255
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_18"
      target_id "M112_384"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 835
    source 252
    target 256
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_18"
      target_id "M112_402"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 836
    source 255
    target 257
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_384"
      target_id "M112_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 837
    source 258
    target 257
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_91"
      target_id "M112_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 838
    source 259
    target 257
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_422"
      target_id "M112_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 839
    source 257
    target 260
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_28"
      target_id "M112_411"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 840
    source 257
    target 261
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_28"
      target_id "M112_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 841
    source 258
    target 266
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_91"
      target_id "M112_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 842
    source 258
    target 262
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_91"
      target_id "M112_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 843
    source 266
    target 260
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_48"
      target_id "M112_411"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 844
    source 260
    target 267
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_411"
      target_id "M112_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 845
    source 262
    target 261
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_37"
      target_id "M112_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 846
    source 263
    target 262
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_169"
      target_id "M112_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 847
    source 264
    target 262
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_110"
      target_id "M112_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 848
    source 262
    target 265
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_37"
      target_id "M112_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 849
    source 272
    target 266
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_180"
      target_id "M112_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 850
    source 268
    target 267
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_203"
      target_id "M112_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 851
    source 269
    target 267
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_1"
      target_id "M112_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 852
    source 267
    target 270
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_16"
      target_id "M112_192"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 853
    source 267
    target 271
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_16"
      target_id "M112_213"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 854
    source 275
    target 279
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_395"
      target_id "M112_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 855
    source 279
    target 276
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_13"
      target_id "M112_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 856
    source 280
    target 279
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_346"
      target_id "M112_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 857
    source 297
    target 303
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_385"
      target_id "M112_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 858
    source 297
    target 301
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_385"
      target_id "M112_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 859
    source 301
    target 298
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_12"
      target_id "M112_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 860
    source 302
    target 301
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_111"
      target_id "M112_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 861
    source 303
    target 304
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_11"
      target_id "M112_388"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 862
    source 313
    target 305
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_32"
      target_id "M112_404"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 863
    source 314
    target 313
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_405"
      target_id "M112_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 864
    source 315
    target 313
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_117"
      target_id "M112_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 865
    source 316
    target 313
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_116"
      target_id "M112_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 866
    source 313
    target 317
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_32"
      target_id "M112_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 867
    source 320
    target 322
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_400"
      target_id "M112_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 868
    source 323
    target 322
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_103"
      target_id "M112_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 869
    source 324
    target 322
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_105"
      target_id "M112_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 870
    source 322
    target 325
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_29"
      target_id "M112_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 871
    source 336
    target 334
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_126"
      target_id "M112_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 872
    source 336
    target 335
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_126"
      target_id "M112_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 873
    source 337
    target 335
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_97"
      target_id "M112_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 874
    source 338
    target 335
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_133"
      target_id "M112_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 875
    source 335
    target 339
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_26"
      target_id "M112_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 876
    source 358
    target 352
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_36"
      target_id "M112_135"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 877
    source 359
    target 358
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_134"
      target_id "M112_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 878
    source 360
    target 358
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_136"
      target_id "M112_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 879
    source 368
    target 361
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_81"
      target_id "M112_296"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 880
    source 361
    target 369
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_296"
      target_id "M112_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 881
    source 409
    target 368
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_342"
      target_id "M112_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 882
    source 410
    target 368
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_348"
      target_id "M112_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 883
    source 411
    target 368
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_347"
      target_id "M112_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 884
    source 368
    target 412
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_81"
      target_id "M112_345"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 885
    source 370
    target 369
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_350"
      target_id "M112_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 886
    source 371
    target 369
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_380"
      target_id "M112_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 887
    source 372
    target 369
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_382"
      target_id "M112_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 888
    source 373
    target 369
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_383"
      target_id "M112_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 889
    source 374
    target 369
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_381"
      target_id "M112_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 890
    source 369
    target 375
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_82"
      target_id "M112_349"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 891
    source 369
    target 376
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_82"
      target_id "M112_379"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 892
    source 375
    target 377
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_349"
      target_id "M112_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 893
    source 378
    target 377
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_372"
      target_id "M112_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 894
    source 379
    target 377
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_4"
      target_id "M112_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 895
    source 377
    target 380
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_89"
      target_id "M112_365"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 896
    source 377
    target 381
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_89"
      target_id "M112_370"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 897
    source 377
    target 382
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_89"
      target_id "M112_371"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 898
    source 383
    target 380
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_86"
      target_id "M112_365"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 899
    source 380
    target 384
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_365"
      target_id "M112_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 900
    source 393
    target 383
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_360"
      target_id "M112_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 901
    source 394
    target 383
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_367"
      target_id "M112_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 902
    source 395
    target 383
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_366"
      target_id "M112_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 903
    source 383
    target 396
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_86"
      target_id "M112_368"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 904
    source 385
    target 384
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_378"
      target_id "M112_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 905
    source 386
    target 384
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_5"
      target_id "M112_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 906
    source 387
    target 384
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_374"
      target_id "M112_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 907
    source 388
    target 384
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_373"
      target_id "M112_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 908
    source 389
    target 384
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_375"
      target_id "M112_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 909
    source 390
    target 384
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_376"
      target_id "M112_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 910
    source 384
    target 391
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_88"
      target_id "M112_369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 911
    source 384
    target 392
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_88"
      target_id "M112_377"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 912
    source 397
    target 393
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_85"
      target_id "M112_360"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 913
    source 398
    target 397
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_355"
      target_id "M112_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 914
    source 399
    target 397
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_361"
      target_id "M112_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 915
    source 400
    target 397
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_364"
      target_id "M112_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 916
    source 397
    target 401
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_85"
      target_id "M112_362"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 917
    source 397
    target 402
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_85"
      target_id "M112_363"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 918
    source 398
    target 403
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_355"
      target_id "M112_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 919
    source 404
    target 403
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_357"
      target_id "M112_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 920
    source 405
    target 403
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CONSPUMPTION"
      source_id "M112_356"
      target_id "M112_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 921
    source 406
    target 403
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "CATALYSIS"
      source_id "M112_359"
      target_id "M112_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 922
    source 403
    target 407
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_84"
      target_id "M112_351"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 923
    source 403
    target 408
    cd19dm [
      diagram "C19DMap:Nsp14 and metabolism"
      edge_type "PRODUCTION"
      source_id "M112_84"
      target_id "M112_358"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
