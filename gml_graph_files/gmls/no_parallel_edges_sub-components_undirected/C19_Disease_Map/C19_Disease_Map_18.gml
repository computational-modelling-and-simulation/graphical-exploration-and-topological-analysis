# generated with VANTED V2.8.2 at Fri Mar 04 10:04:38 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:hgnc:3573;urn:miriam:ncbigene:8772;urn:miriam:uniprot:Q13158;urn:miriam:ensembl:ENSG00000168040;urn:miriam:refseq:NM_003824;urn:miriam:hgnc.symbol:FADD"
      hgnc "HGNC_SYMBOL:FADD"
      map_id "M113_35"
      name "FADD"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa10"
      uniprot "UNIPROT:Q13158"
    ]
    graphics [
      x 1087.9673850663708
      y 1642.6118367399172
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_22"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re3"
      uniprot "NA"
    ]
    graphics [
      x 1671.6104592494069
      y 1709.0814698311804
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_14"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re16"
      uniprot "NA"
    ]
    graphics [
      x 1912.5144998161113
      y 1900.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:32555321"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_30"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re5"
      uniprot "NA"
    ]
    graphics [
      x 2077.9673850663708
      y 1647.1544028564542
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:hgnc:1509;urn:miriam:hgnc.symbol:CASP8;urn:miriam:uniprot:Q14790;urn:miriam:uniprot:Q14790;urn:miriam:hgnc.symbol:CASP8;urn:miriam:ncbigene:841;urn:miriam:ncbigene:841;urn:miriam:ec-code:3.4.22.61;urn:miriam:doi:10.1038/s41392-020-00334-0;urn:miriam:refseq:NM_001228;urn:miriam:ensembl:ENSG00000064012"
      hgnc "HGNC_SYMBOL:CASP8"
      map_id "M113_36"
      name "CASP8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa13"
      uniprot "UNIPROT:Q14790"
    ]
    graphics [
      x 1927.9673850663708
      y 1570.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:ncbigene:8717;urn:miriam:ensembl:ENSG00000102871;urn:miriam:refseq:NM_001323552;urn:miriam:uniprot:Q15628;urn:miriam:hgnc:12030;urn:miriam:hgnc.symbol:TRADD;urn:miriam:hgnc:3573;urn:miriam:ncbigene:8772;urn:miriam:uniprot:Q13158;urn:miriam:ensembl:ENSG00000168040;urn:miriam:refseq:NM_003824;urn:miriam:hgnc.symbol:FADD"
      hgnc "HGNC_SYMBOL:TRADD;HGNC_SYMBOL:FADD"
      map_id "M113_7"
      name "TRADD_slash_FADD"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa6"
      uniprot "UNIPROT:Q15628;UNIPROT:Q13158"
    ]
    graphics [
      x 1357.9673850663708
      y 1692.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:uniprot:P59637;urn:miriam:ncbiprotein:YP_009724391.1;urn:miriam:ncbigene:1489671;urn:miriam:hgnc.symbol:E;urn:miriam:pubmed:33100263;urn:miriam:pubmed:32555321"
      hgnc "HGNC_SYMBOL:E"
      map_id "M113_76"
      name "Orf3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa92"
      uniprot "UNIPROT:P59637"
    ]
    graphics [
      x 2392.5144998161113
      y 1988.2216965204711
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:hgnc:1509;urn:miriam:hgnc.symbol:CASP8;urn:miriam:uniprot:Q14790;urn:miriam:ncbigene:841;urn:miriam:ec-code:3.4.22.61;urn:miriam:doi:10.1038/s41392-020-00334-0;urn:miriam:refseq:NM_001228;urn:miriam:ensembl:ENSG00000064012"
      hgnc "HGNC_SYMBOL:CASP8"
      map_id "M113_37"
      name "CASP8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa14"
      uniprot "UNIPROT:Q14790"
    ]
    graphics [
      x 937.9673850663706
      y 1666.6019746441584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_33"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re8"
      uniprot "NA"
    ]
    graphics [
      x 965.6939429587638
      y 2412.8780691384495
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_11"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re12"
      uniprot "NA"
    ]
    graphics [
      x 1327.9673850663708
      y 1632.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re6"
      uniprot "NA"
    ]
    graphics [
      x 427.96738506637064
      y 1922.5572932182372
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:refseq:NM_004346;urn:miriam:ncbigene:836;urn:miriam:ec-code:3.4.22.56;urn:miriam:ensembl:ENSG00000164305;urn:miriam:hgnc:1504;urn:miriam:uniprot:P42574;urn:miriam:pubmed:32555321;urn:miriam:hgnc.symbol:CASP3"
      hgnc "HGNC_SYMBOL:CASP3"
      map_id "M113_38"
      name "CASP3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa15"
      uniprot "UNIPROT:P42574"
    ]
    graphics [
      x 1121.4760702265748
      y 1338.9296873026296
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:hgnc.symbol:CASP9;urn:miriam:refseq:NM_032996;urn:miriam:ncbigene:842;urn:miriam:hgnc:1511;urn:miriam:ensembl:ENSG00000132906;urn:miriam:ec-code:3.4.22.62;urn:miriam:uniprot:P55211"
      hgnc "HGNC_SYMBOL:CASP9"
      map_id "M113_41"
      name "CASP9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa19"
      uniprot "UNIPROT:P55211"
    ]
    graphics [
      x 1282.5144998161113
      y 1902.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:ncbigene:836;urn:miriam:refseq:NM_004346;urn:miriam:ncbigene:836;urn:miriam:ec-code:3.4.22.56;urn:miriam:ensembl:ENSG00000164305;urn:miriam:pubmed:32555321;urn:miriam:hgnc:1504;urn:miriam:uniprot:P42574;urn:miriam:uniprot:P42574;urn:miriam:hgnc.symbol:CASP3;urn:miriam:hgnc.symbol:CASP3"
      hgnc "HGNC_SYMBOL:CASP3"
      map_id "M113_39"
      name "CASP3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa17"
      uniprot "UNIPROT:P42574"
    ]
    graphics [
      x 981.6104592494069
      y 1802.3785906698167
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_28"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "re35"
      uniprot "NA"
    ]
    graphics [
      x 1672.5144998161113
      y 1822.2319470430075
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:mesh:D017209;urn:miriam:doi:10.1007/s10495-021-01656-2"
      hgnc "NA"
      map_id "M113_55"
      name "Apoptosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa41"
      uniprot "NA"
    ]
    graphics [
      x 1791.6104592494069
      y 1813.8597507391748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_24"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "re31"
      uniprot "NA"
    ]
    graphics [
      x 791.4760702265747
      y 1356.3585216247106
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_25"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "re32"
      uniprot "NA"
    ]
    graphics [
      x 701.4760702265747
      y 1150.3685945910092
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_26"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "re33"
      uniprot "NA"
    ]
    graphics [
      x 1538.8536650413898
      y 882.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_19"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "re27"
      uniprot "NA"
    ]
    graphics [
      x 1939.8548263367193
      y 2470.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_12"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "re14"
      uniprot "NA"
    ]
    graphics [
      x 1661.4760702265748
      y 1206.7446462369894
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_21"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "re29"
      uniprot "NA"
    ]
    graphics [
      x 2318.85366504139
      y 813.9475755283014
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_20"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "re28"
      uniprot "NA"
    ]
    graphics [
      x 2321.476070226575
      y 1400.0981118777831
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:uniprot:Q7TFA0;urn:miriam:ncbigene:1489676"
      hgnc "NA"
      map_id "M113_66"
      name "Orf8a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa73"
      uniprot "UNIPROT:Q7TFA0"
    ]
    graphics [
      x 1882.5144998161113
      y 1930.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:pubmed:32654247;urn:miriam:pubmed:33264373;urn:miriam:pubmed:32416961;urn:miriam:pubmed:16112641;urn:miriam:hgnc.symbol:N;urn:miriam:pubmed:32363136;urn:miriam:uniprot:P59595;urn:miriam:pubmed:16845612;urn:miriam:ncbigene:1489678"
      hgnc "HGNC_SYMBOL:N"
      map_id "M113_67"
      name "N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa74"
      uniprot "UNIPROT:P59595"
    ]
    graphics [
      x 2411.476070226575
      y 1026.9900002875893
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:hgnc:1508;urn:miriam:hgnc.symbol:CASP7;urn:miriam:ncbigene:840;urn:miriam:ec-code:3.4.22.60;urn:miriam:refseq:NM_033338;urn:miriam:ensembl:ENSG00000165806;urn:miriam:uniprot:P55210"
      hgnc "HGNC_SYMBOL:CASP7"
      map_id "M113_54"
      name "CASP7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa40"
      uniprot "UNIPROT:P55210"
    ]
    graphics [
      x 1391.4760702265748
      y 1299.9618625767896
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:uniprot:P59633;urn:miriam:ncbigene:1489670"
      hgnc "NA"
      map_id "M113_65"
      name "Orf3b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa72"
      uniprot "UNIPROT:P59633"
    ]
    graphics [
      x 1940.0730886186202
      y 2530.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:uniprot:P59634;urn:miriam:ncbigene:1489673"
      hgnc "NA"
      map_id "M113_68"
      name "Orf6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa75"
      uniprot "UNIPROT:P59634"
    ]
    graphics [
      x 1535.8492877187218
      y 312.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:ncbigene:1489679;urn:miriam:uniprot:P59636"
      hgnc "NA"
      map_id "M113_70"
      name "Orf9b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa77"
      uniprot "UNIPROT:P59636"
    ]
    graphics [
      x 368.85366504138983
      y 842.606688389798
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:ncbigene:1489668;urn:miriam:pubmed:32275855;urn:miriam:pubmed:32075877;urn:miriam:pubmed:32155444;urn:miriam:pubmed:32225176;urn:miriam:uniprot:P59594;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "M113_69"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa76"
      uniprot "UNIPROT:P59594"
    ]
    graphics [
      x 431.47607022657473
      y 992.5572932182373
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_32"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re7"
      uniprot "NA"
    ]
    graphics [
      x 1327.9673850663708
      y 1662.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:hgnc.symbol:CASP9;urn:miriam:refseq:NM_032996;urn:miriam:ncbigene:842;urn:miriam:hgnc:1511;urn:miriam:ensembl:ENSG00000132906;urn:miriam:ec-code:3.4.22.62;urn:miriam:uniprot:P55211"
      hgnc "HGNC_SYMBOL:CASP9"
      map_id "M113_40"
      name "CASP9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa18"
      uniprot "UNIPROT:P55211"
    ]
    graphics [
      x 761.4760702265747
      y 1363.9061230941254
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:hgnc:19986;urn:miriam:uniprot:P99999;urn:miriam:ncbigene:54205;urn:miriam:hgnc.symbol:CYCS;urn:miriam:ensembl:ENSG00000172115;urn:miriam:refseq:NM_018947;urn:miriam:hgnc.symbol:CASP9;urn:miriam:refseq:NM_032996;urn:miriam:ncbigene:842;urn:miriam:hgnc:1511;urn:miriam:ensembl:ENSG00000132906;urn:miriam:ec-code:3.4.22.62;urn:miriam:uniprot:P55211;urn:miriam:ncbigene:317;urn:miriam:hgnc:576;urn:miriam:refseq:NM_181861.1;urn:miriam:hgnc.symbol:APAF1;urn:miriam:uniprot:O14727;urn:miriam:ensembl:ENSG00000120868"
      hgnc "HGNC_SYMBOL:CYCS;HGNC_SYMBOL:CASP9;HGNC_SYMBOL:APAF1"
      map_id "M113_6"
      name "Apoptosome"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa5"
      uniprot "UNIPROT:P99999;UNIPROT:P55211;UNIPROT:O14727"
    ]
    graphics [
      x 457.96738506637064
      y 1666.793183776992
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:ncbigene:207;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc:391;urn:miriam:hgnc.symbol:AKT1;urn:miriam:refseq:NM_005163;urn:miriam:uniprot:P31749;urn:miriam:ensembl:ENSG00000142208"
      hgnc "HGNC_SYMBOL:AKT1"
      map_id "M113_49"
      name "AKT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa29"
      uniprot "UNIPROT:P31749"
    ]
    graphics [
      x 2422.5144998161113
      y 1816.8803162036775
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_13"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re15"
      uniprot "NA"
    ]
    graphics [
      x 2199.3094489355713
      y 2432.11215207005
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "PUBMED:15694340"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_27"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re34"
      uniprot "NA"
    ]
    graphics [
      x 1550.0074341427699
      y 2472.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_23"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re30"
      uniprot "NA"
    ]
    graphics [
      x 1368.039049592187
      y 1812.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:hgnc:19986;urn:miriam:uniprot:P99999;urn:miriam:ncbigene:54205;urn:miriam:hgnc.symbol:CYCS;urn:miriam:ensembl:ENSG00000172115;urn:miriam:refseq:NM_018947"
      hgnc "HGNC_SYMBOL:CYCS"
      map_id "M113_46"
      name "CYCS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa25"
      uniprot "UNIPROT:P99999"
    ]
    graphics [
      x 1497.3427285901885
      y 1122.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:ncbigene:317;urn:miriam:hgnc:576;urn:miriam:refseq:NM_181861.1;urn:miriam:hgnc.symbol:APAF1;urn:miriam:uniprot:O14727;urn:miriam:ensembl:ENSG00000120868"
      hgnc "HGNC_SYMBOL:APAF1"
      map_id "M113_47"
      name "APAF1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa27"
      uniprot "UNIPROT:O14727"
    ]
    graphics [
      x 1691.4760702265748
      y 1179.571063044361
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:hgnc.symbol:CASP9;urn:miriam:refseq:NM_032996;urn:miriam:ncbigene:842;urn:miriam:hgnc:1511;urn:miriam:ensembl:ENSG00000132906;urn:miriam:ec-code:3.4.22.62;urn:miriam:uniprot:P55211"
      hgnc "HGNC_SYMBOL:CASP9"
      map_id "M113_59"
      name "CASP9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa47"
      uniprot "UNIPROT:P55211"
    ]
    graphics [
      x 591.6104592494069
      y 1795.3513309131831
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_10"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re10"
      uniprot "NA"
    ]
    graphics [
      x 757.9673850663706
      y 1613.1178987887638
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:hgnc:19986;urn:miriam:uniprot:P99999;urn:miriam:ncbigene:54205;urn:miriam:hgnc.symbol:CYCS;urn:miriam:ensembl:ENSG00000172115;urn:miriam:refseq:NM_018947"
      hgnc "HGNC_SYMBOL:CYCS"
      map_id "M113_45"
      name "CYCS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa24"
      uniprot "UNIPROT:P99999"
    ]
    graphics [
      x 997.9673850663706
      y 1619.2696849278589
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:ec-code:2.7.11.24;urn:miriam:ensembl:ENSG00000112062;urn:miriam:hgnc:6876;urn:miriam:uniprot:Q16539;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:refseq:NM_001315"
      hgnc "HGNC_SYMBOL:MAPK14"
      map_id "M113_63"
      name "MAPK14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa50"
      uniprot "UNIPROT:Q16539"
    ]
    graphics [
      x 1271.4760702265748
      y 1002.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:refseq:NM_138763;urn:miriam:hgnc:959;urn:miriam:ensembl:ENSG00000087088;urn:miriam:hgnc.symbol:BAX;urn:miriam:ncbigene:581;urn:miriam:uniprot:Q07812"
      hgnc "HGNC_SYMBOL:BAX"
      map_id "M113_52"
      name "BAX"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa31"
      uniprot "UNIPROT:Q07812"
    ]
    graphics [
      x 555.4286082386425
      y 2346.0144235457656
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re9"
      uniprot "NA"
    ]
    graphics [
      x 1003.6913499693654
      y 2473.8637400484413
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:refseq:NM_138763;urn:miriam:hgnc:959;urn:miriam:ensembl:ENSG00000087088;urn:miriam:hgnc.symbol:BAX;urn:miriam:ncbigene:581;urn:miriam:uniprot:Q07812"
      hgnc "HGNC_SYMBOL:BAX"
      map_id "M113_51"
      name "BAX"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa30"
      uniprot "UNIPROT:Q07812"
    ]
    graphics [
      x 1298.0155655333583
      y 2502.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:refseq:NM_001204106;urn:miriam:hgnc:994;urn:miriam:hgnc.symbol:BCL2L11;urn:miriam:ncbigene:10018;urn:miriam:ensembl:ENSG00000153094;urn:miriam:uniprot:O43521;urn:miriam:refseq:NM_032989;urn:miriam:uniprot:Q92934;urn:miriam:ncbigene:572;urn:miriam:hgnc.symbol:BAD;urn:miriam:ensembl:ENSG00000002330;urn:miriam:hgnc:936;urn:miriam:uniprot:Q9BXH1;urn:miriam:ncbigene:27113;urn:miriam:hgnc:17868;urn:miriam:ensembl:ENSG00000105327;urn:miriam:hgnc.symbol:BBC3;urn:miriam:refseq:NM_014417;urn:miriam:uniprot:Q96PG8"
      hgnc "HGNC_SYMBOL:BCL2L11;HGNC_SYMBOL:BAD;HGNC_SYMBOL:BBC3"
      map_id "M113_5"
      name "BAD_slash_BBC3_slash_BCL2L11"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa3"
      uniprot "UNIPROT:O43521;UNIPROT:Q92934;UNIPROT:Q9BXH1;UNIPROT:Q96PG8"
    ]
    graphics [
      x 1019.1188547295466
      y 2301.9577197196427
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:refseq:NM_138578;urn:miriam:ncbigene:598;urn:miriam:ensembl:ENSG00000171552;urn:miriam:uniprot:Q07817;urn:miriam:hgnc:992;urn:miriam:hgnc.symbol:BCL2L1;urn:miriam:hgnc.symbol:BCL2;urn:miriam:refseq:NM_000657;urn:miriam:ncbigene:596;urn:miriam:uniprot:P10415;urn:miriam:ensembl:ENSG00000171791;urn:miriam:refseq:NM_000633;urn:miriam:hgnc:990;urn:miriam:ncbigene:4170;urn:miriam:uniprot:Q07820;urn:miriam:hgnc:6943;urn:miriam:refseq:NM_021960;urn:miriam:ensembl:ENSG00000143384;urn:miriam:hgnc.symbol:MCL1"
      hgnc "HGNC_SYMBOL:BCL2L1;HGNC_SYMBOL:BCL2;HGNC_SYMBOL:MCL1"
      map_id "M113_3"
      name "BCL2_slash_MCL1_slash_BCL2L1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa11"
      uniprot "UNIPROT:Q07817;UNIPROT:P10415;UNIPROT:Q07820"
    ]
    graphics [
      x 1335.4710402090845
      y 1752.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:hgnc.symbol:BID;urn:miriam:ncbigene:637;urn:miriam:refseq:NM_197966;urn:miriam:uniprot:P55957;urn:miriam:ensembl:ENSG00000015475;urn:miriam:hgnc:1050"
      hgnc "HGNC_SYMBOL:BID"
      map_id "M113_74"
      name "BID"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa89"
      uniprot "UNIPROT:P55957"
    ]
    graphics [
      x 1211.4760702265748
      y 1479.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_29"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re38"
      uniprot "NA"
    ]
    graphics [
      x 1807.9673850663708
      y 1603.8597507391748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:hgnc.symbol:BID;urn:miriam:ncbigene:637;urn:miriam:refseq:NM_197966;urn:miriam:uniprot:P55957;urn:miriam:ensembl:ENSG00000015475;urn:miriam:hgnc:1050"
      hgnc "HGNC_SYMBOL:BID"
      map_id "M113_44"
      name "BID"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa23"
      uniprot "UNIPROT:P55957"
    ]
    graphics [
      x 1850.0074341427699
      y 2350.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "PUBMED:15694340;PUBMED:17428862"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_17"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re23"
      uniprot "NA"
    ]
    graphics [
      x 1391.4760702265748
      y 1032.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_16"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re22"
      uniprot "NA"
    ]
    graphics [
      x 487.96738506637064
      y 1628.8328798263947
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:uniprot:Q9BXH1;urn:miriam:ncbigene:27113;urn:miriam:hgnc:17868;urn:miriam:ensembl:ENSG00000105327;urn:miriam:hgnc.symbol:BBC3;urn:miriam:refseq:NM_014417;urn:miriam:uniprot:Q96PG8;urn:miriam:refseq:NM_032989;urn:miriam:uniprot:Q92934;urn:miriam:ncbigene:572;urn:miriam:hgnc.symbol:BAD;urn:miriam:ensembl:ENSG00000002330;urn:miriam:hgnc:936;urn:miriam:refseq:NM_001204106;urn:miriam:hgnc:994;urn:miriam:hgnc.symbol:BCL2L11;urn:miriam:ncbigene:10018;urn:miriam:ensembl:ENSG00000153094;urn:miriam:uniprot:O43521"
      hgnc "HGNC_SYMBOL:BBC3;HGNC_SYMBOL:BAD;HGNC_SYMBOL:BCL2L11"
      map_id "M113_8"
      name "BAD_slash_BBC3_slash_BCL2L11"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa9"
      uniprot "UNIPROT:Q9BXH1;UNIPROT:Q96PG8;UNIPROT:Q92934;UNIPROT:O43521"
    ]
    graphics [
      x 577.9673850663706
      y 1515.55511994089
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:hgnc.symbol:BCL2;urn:miriam:refseq:NM_000657;urn:miriam:ncbigene:596;urn:miriam:uniprot:P10415;urn:miriam:ensembl:ENSG00000171791;urn:miriam:refseq:NM_000633;urn:miriam:hgnc:990;urn:miriam:ncbigene:4170;urn:miriam:uniprot:Q07820;urn:miriam:hgnc:6943;urn:miriam:refseq:NM_021960;urn:miriam:ensembl:ENSG00000143384;urn:miriam:hgnc.symbol:MCL1;urn:miriam:refseq:NM_138578;urn:miriam:ncbigene:598;urn:miriam:ensembl:ENSG00000171552;urn:miriam:uniprot:Q07817;urn:miriam:hgnc:992;urn:miriam:hgnc.symbol:BCL2L1"
      hgnc "HGNC_SYMBOL:BCL2;HGNC_SYMBOL:MCL1;HGNC_SYMBOL:BCL2L1"
      map_id "M113_2"
      name "BCL2_slash_MCL1_slash_BCL2L1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa10"
      uniprot "UNIPROT:P10415;UNIPROT:Q07820;UNIPROT:Q07817"
    ]
    graphics [
      x 1113.6998874166225
      y 570.5543133122671
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:hgnc.symbol:ORF7a;urn:miriam:uniprot:Q19QW4;urn:miriam:ncbigene:1489674"
      hgnc "HGNC_SYMBOL:ORF7a"
      map_id "M113_57"
      name "Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa43"
      uniprot "UNIPROT:Q19QW4"
    ]
    graphics [
      x 1958.8536650413898
      y 793.262413143089
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:pubmed:32785274;urn:miriam:pubmed:32818817;urn:miriam:uniprot:P59637;urn:miriam:ncbigene:1489671;urn:miriam:hgnc.symbol:E"
      hgnc "HGNC_SYMBOL:E"
      map_id "M113_64"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa69"
      uniprot "UNIPROT:P59637"
    ]
    graphics [
      x 1240.9744346231273
      y 432.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:refseq:NM_032989;urn:miriam:uniprot:Q92934;urn:miriam:ncbigene:572;urn:miriam:hgnc.symbol:BAD;urn:miriam:ensembl:ENSG00000002330;urn:miriam:hgnc:936"
      hgnc "HGNC_SYMBOL:BAD"
      map_id "M113_72"
      name "BAD"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa79"
      uniprot "UNIPROT:Q92934"
    ]
    graphics [
      x 1522.5144998161113
      y 2172.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_15"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re19"
      uniprot "NA"
    ]
    graphics [
      x 1192.5144998161113
      y 2049.0166269831952
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:ec-code:2.7.11.24;urn:miriam:ensembl:ENSG00000112062;urn:miriam:hgnc:6876;urn:miriam:uniprot:Q16539;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:refseq:NM_001315"
      hgnc "HGNC_SYMBOL:MAPK14"
      map_id "M113_61"
      name "MAPK14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa49"
      uniprot "UNIPROT:Q16539"
    ]
    graphics [
      x 1218.042859544955
      y 2502.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:uniprot:P59637;urn:miriam:ncbiprotein:YP_009724391.1;urn:miriam:ncbigene:1489671;urn:miriam:hgnc.symbol:E;urn:miriam:pubmed:33100263;urn:miriam:pubmed:32555321"
      hgnc "HGNC_SYMBOL:E"
      map_id "M113_60"
      name "Orf3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa48"
      uniprot "UNIPROT:P59637"
    ]
    graphics [
      x 1702.5144998161113
      y 1865.5046666730118
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:refseq:NM_032989;urn:miriam:uniprot:Q92934;urn:miriam:ncbigene:572;urn:miriam:hgnc.symbol:BAD;urn:miriam:ensembl:ENSG00000002330;urn:miriam:hgnc:936"
      hgnc "HGNC_SYMBOL:BAD"
      map_id "M113_71"
      name "BAD"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa78"
      uniprot "UNIPROT:Q92934"
    ]
    graphics [
      x 1311.6104592494069
      y 1782.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:ncbigene:207;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc:391;urn:miriam:hgnc.symbol:AKT1;urn:miriam:refseq:NM_005163;urn:miriam:uniprot:P31749;urn:miriam:ensembl:ENSG00000142208"
      hgnc "HGNC_SYMBOL:AKT1"
      map_id "M113_48"
      name "AKT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa28"
      uniprot "UNIPROT:P31749"
    ]
    graphics [
      x 2094.080281687381
      y 2567.891775710673
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:ncbiprotein:APO40582;urn:miriam:pubmed:16845612;urn:miriam:uniprot:M"
      hgnc "NA"
      map_id "M113_56"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa42"
      uniprot "UNIPROT:M"
    ]
    graphics [
      x 1542.8545281968368
      y 2412.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:hgnc:1508;urn:miriam:hgnc.symbol:CASP7;urn:miriam:ncbigene:840;urn:miriam:ec-code:3.4.22.60;urn:miriam:refseq:NM_033338;urn:miriam:ensembl:ENSG00000165806;urn:miriam:uniprot:P55210"
      hgnc "HGNC_SYMBOL:CASP7"
      map_id "M113_53"
      name "CASP7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa39"
      uniprot "UNIPROT:P55210"
    ]
    graphics [
      x 641.4760702265747
      y 1497.7170511520449
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:hgnc.symbol:BID;urn:miriam:ncbigene:637;urn:miriam:refseq:NM_197966;urn:miriam:uniprot:P55957;urn:miriam:ensembl:ENSG00000015475;urn:miriam:hgnc:1050"
      hgnc "HGNC_SYMBOL:BID"
      map_id "M113_43"
      name "BID"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa22"
      uniprot "UNIPROT:P55957"
    ]
    graphics [
      x 711.6104592494069
      y 1792.4666947456517
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:ncbigene:8717;urn:miriam:ensembl:ENSG00000102871;urn:miriam:refseq:NM_001323552;urn:miriam:uniprot:Q15628;urn:miriam:hgnc:12030;urn:miriam:hgnc.symbol:TRADD"
      hgnc "HGNC_SYMBOL:TRADD"
      map_id "M113_58"
      name "TRADD"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa44"
      uniprot "UNIPROT:Q15628"
    ]
    graphics [
      x 2290.4146427238684
      y 2246.258931287444
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:refseq:NM_000594;urn:miriam:hgnc.symbol:TNF;urn:miriam:uniprot:P01375;urn:miriam:hgnc:11892;urn:miriam:ncbigene:7124;urn:miriam:ensembl:ENSG00000232810;urn:miriam:ncbigene:7132;urn:miriam:refseq:NM_001065;urn:miriam:ensembl:ENSG00000067182;urn:miriam:uniprot:P19438;urn:miriam:hgnc.symbol:TNFRSF1A;urn:miriam:hgnc:11916"
      hgnc "HGNC_SYMBOL:TNF;HGNC_SYMBOL:TNFRSF1A"
      map_id "M113_1"
      name "TNF_slash_TNFRSF1A"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa1"
      uniprot "UNIPROT:P01375;UNIPROT:P19438"
    ]
    graphics [
      x 711.6104592494069
      y 1822.4666947456517
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_18"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re26"
      uniprot "NA"
    ]
    graphics [
      x 982.5144998161113
      y 1968.2206144704637
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:ncbigene:7132;urn:miriam:refseq:NM_001065;urn:miriam:ensembl:ENSG00000067182;urn:miriam:uniprot:P19438;urn:miriam:hgnc.symbol:TNFRSF1A;urn:miriam:hgnc:11916"
      hgnc "HGNC_SYMBOL:TNFRSF1A"
      map_id "M113_73"
      name "TNFRSF1A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa8"
      uniprot "UNIPROT:P19438"
    ]
    graphics [
      x 847.9673850663706
      y 1575.0908463925657
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:refseq:NM_000594;urn:miriam:hgnc.symbol:TNF;urn:miriam:uniprot:P01375;urn:miriam:hgnc:11892;urn:miriam:ncbigene:7124;urn:miriam:ensembl:ENSG00000232810"
      hgnc "HGNC_SYMBOL:TNF"
      map_id "M113_50"
      name "TNF"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa3"
      uniprot "UNIPROT:P01375"
    ]
    graphics [
      x 1261.6483396118758
      y 2442.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:hgnc:3573;urn:miriam:ncbigene:8772;urn:miriam:uniprot:Q13158;urn:miriam:ensembl:ENSG00000168040;urn:miriam:refseq:NM_003824;urn:miriam:hgnc.symbol:FADD"
      hgnc "HGNC_SYMBOL:FADD"
      map_id "M113_75"
      name "FADD"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa9"
      uniprot "UNIPROT:Q13158"
    ]
    graphics [
      x 2437.9673850663708
      y 1703.5280956864895
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:hgnc.symbol:FASLG;urn:miriam:ncbigene:356;urn:miriam:refseq:NM_000639;urn:miriam:ensembl:ENSG00000117560;urn:miriam:uniprot:P48023;urn:miriam:hgnc:11936;urn:miriam:hgnc:11920;urn:miriam:uniprot:P25445;urn:miriam:refseq:NM_000043;urn:miriam:ensembl:ENSG00000026103;urn:miriam:ncbigene:355;urn:miriam:hgnc.symbol:FAS"
      hgnc "HGNC_SYMBOL:FASLG;HGNC_SYMBOL:FAS"
      map_id "M113_4"
      name "FAS_slash_FASL"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa2"
      uniprot "UNIPROT:P48023;UNIPROT:P25445"
    ]
    graphics [
      x 1927.9673850663708
      y 1630.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M113_9"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re1"
      uniprot "NA"
    ]
    graphics [
      x 1328.0155655333583
      y 2472.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:hgnc.symbol:FASLG;urn:miriam:ncbigene:356;urn:miriam:refseq:NM_000639;urn:miriam:doi:10.1101/2020.12.04.412494;urn:miriam:ensembl:ENSG00000117560;urn:miriam:uniprot:P48023;urn:miriam:hgnc:11936"
      hgnc "HGNC_SYMBOL:FASLG"
      map_id "M113_42"
      name "FASLG"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2"
      uniprot "UNIPROT:P48023"
    ]
    graphics [
      x 1267.9673850663708
      y 1662.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Apoptosis pathway"
      full_annotation "urn:miriam:hgnc:11920;urn:miriam:uniprot:P25445;urn:miriam:refseq:NM_000043;urn:miriam:ensembl:ENSG00000026103;urn:miriam:ncbigene:355;urn:miriam:hgnc.symbol:FAS"
      hgnc "HGNC_SYMBOL:FAS"
      map_id "M113_62"
      name "FAS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa5"
      uniprot "UNIPROT:P25445"
    ]
    graphics [
      x 910.1566501106137
      y 2350.342369608098
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M113_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 77
    source 2
    target 1
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_22"
      target_id "M113_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 78
    source 1
    target 3
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "M113_35"
      target_id "M113_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 79
    source 1
    target 4
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CATALYSIS"
      source_id "M113_35"
      target_id "M113_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 72
    target 2
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "M113_75"
      target_id "M113_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 81
    source 73
    target 2
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CATALYSIS"
      source_id "M113_4"
      target_id "M113_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 82
    source 67
    target 3
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "M113_58"
      target_id "M113_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 83
    source 68
    target 3
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CATALYSIS"
      source_id "M113_1"
      target_id "M113_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 84
    source 3
    target 6
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_14"
      target_id "M113_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 85
    source 5
    target 4
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "M113_36"
      target_id "M113_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 86
    source 6
    target 4
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CATALYSIS"
      source_id "M113_7"
      target_id "M113_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 87
    source 7
    target 4
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CATALYSIS"
      source_id "M113_76"
      target_id "M113_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 88
    source 4
    target 8
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_30"
      target_id "M113_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 89
    source 8
    target 9
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CATALYSIS"
      source_id "M113_37"
      target_id "M113_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 90
    source 8
    target 10
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CATALYSIS"
      source_id "M113_37"
      target_id "M113_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 91
    source 8
    target 11
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CATALYSIS"
      source_id "M113_37"
      target_id "M113_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 92
    source 66
    target 9
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "M113_43"
      target_id "M113_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 93
    source 9
    target 51
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_33"
      target_id "M113_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 94
    source 65
    target 10
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "M113_53"
      target_id "M113_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 95
    source 13
    target 10
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CATALYSIS"
      source_id "M113_41"
      target_id "M113_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 96
    source 10
    target 26
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_11"
      target_id "M113_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 97
    source 12
    target 11
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "M113_38"
      target_id "M113_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 98
    source 13
    target 11
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CATALYSIS"
      source_id "M113_41"
      target_id "M113_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 99
    source 11
    target 14
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_31"
      target_id "M113_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 100
    source 31
    target 13
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_32"
      target_id "M113_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 14
    target 15
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "M113_39"
      target_id "M113_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 15
    target 16
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_28"
      target_id "M113_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 17
    target 16
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_24"
      target_id "M113_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 18
    target 16
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_25"
      target_id "M113_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 19
    target 16
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_26"
      target_id "M113_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 106
    source 20
    target 16
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_19"
      target_id "M113_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 107
    source 21
    target 16
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_12"
      target_id "M113_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 108
    source 22
    target 16
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_21"
      target_id "M113_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 109
    source 23
    target 16
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_20"
      target_id "M113_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 110
    source 30
    target 17
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "M113_69"
      target_id "M113_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 111
    source 29
    target 18
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "M113_70"
      target_id "M113_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 112
    source 28
    target 19
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "M113_68"
      target_id "M113_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 113
    source 27
    target 20
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "M113_65"
      target_id "M113_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 114
    source 26
    target 21
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "M113_54"
      target_id "M113_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 25
    target 22
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "M113_67"
      target_id "M113_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 24
    target 23
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "M113_66"
      target_id "M113_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 32
    target 31
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "M113_40"
      target_id "M113_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 33
    target 31
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CATALYSIS"
      source_id "M113_6"
      target_id "M113_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 34
    target 31
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "INHIBITION"
      source_id "M113_49"
      target_id "M113_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 37
    target 33
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_23"
      target_id "M113_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 35
    target 34
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_13"
      target_id "M113_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 34
    target 36
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "INHIBITION"
      source_id "M113_49"
      target_id "M113_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 34
    target 37
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "INHIBITION"
      source_id "M113_49"
      target_id "M113_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 63
    target 35
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "M113_48"
      target_id "M113_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 64
    target 35
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "INHIBITION"
      source_id "M113_56"
      target_id "M113_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 126
    source 62
    target 36
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "M113_71"
      target_id "M113_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 127
    source 36
    target 58
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_27"
      target_id "M113_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 128
    source 38
    target 37
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "M113_46"
      target_id "M113_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 39
    target 37
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "M113_47"
      target_id "M113_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 130
    source 40
    target 37
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "M113_59"
      target_id "M113_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 131
    source 41
    target 38
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_10"
      target_id "M113_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 132
    source 42
    target 41
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "M113_45"
      target_id "M113_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 133
    source 43
    target 41
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CATALYSIS"
      source_id "M113_63"
      target_id "M113_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 134
    source 44
    target 41
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CATALYSIS"
      source_id "M113_52"
      target_id "M113_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 135
    source 59
    target 43
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_15"
      target_id "M113_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 136
    source 45
    target 44
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_34"
      target_id "M113_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 137
    source 46
    target 45
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "M113_51"
      target_id "M113_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 138
    source 47
    target 45
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CATALYSIS"
      source_id "M113_5"
      target_id "M113_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 139
    source 48
    target 45
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "INHIBITION"
      source_id "M113_3"
      target_id "M113_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 49
    target 45
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CATALYSIS"
      source_id "M113_74"
      target_id "M113_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 53
    target 47
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_16"
      target_id "M113_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 52
    target 48
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_17"
      target_id "M113_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 48
    target 53
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "UNKNOWN_INHIBITION"
      source_id "M113_3"
      target_id "M113_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 144
    source 50
    target 49
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_29"
      target_id "M113_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 145
    source 51
    target 50
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "M113_44"
      target_id "M113_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 146
    source 55
    target 52
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "M113_2"
      target_id "M113_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 147
    source 56
    target 52
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "INHIBITION"
      source_id "M113_57"
      target_id "M113_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 148
    source 57
    target 52
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "UNKNOWN_INHIBITION"
      source_id "M113_64"
      target_id "M113_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 149
    source 58
    target 52
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "INHIBITION"
      source_id "M113_72"
      target_id "M113_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 150
    source 54
    target 53
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "M113_8"
      target_id "M113_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 151
    source 60
    target 59
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "M113_61"
      target_id "M113_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 152
    source 61
    target 59
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CATALYSIS"
      source_id "M113_60"
      target_id "M113_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 153
    source 69
    target 68
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_18"
      target_id "M113_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 154
    source 70
    target 69
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "M113_73"
      target_id "M113_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 155
    source 71
    target 69
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "M113_50"
      target_id "M113_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 156
    source 74
    target 73
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "PRODUCTION"
      source_id "M113_9"
      target_id "M113_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 157
    source 75
    target 74
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "M113_42"
      target_id "M113_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 158
    source 76
    target 74
    cd19dm [
      diagram "C19DMap:Apoptosis pathway"
      edge_type "CONSPUMPTION"
      source_id "M113_62"
      target_id "M113_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
