# generated with VANTED V2.8.2 at Fri Mar 04 10:04:38 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:O15455;urn:miriam:uniprot:O15455;urn:miriam:ensembl:ENSG00000164342;urn:miriam:refseq:NM_003265;urn:miriam:ncbigene:7098;urn:miriam:ncbigene:7098;urn:miriam:hgnc.symbol:TLR3;urn:miriam:hgnc.symbol:TLR3;urn:miriam:hgnc:11849"
      hgnc "HGNC_SYMBOL:TLR3"
      map_id "M116_75"
      name "TLR3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa45"
      uniprot "UNIPROT:O15455"
    ]
    graphics [
      x 1361.4760702265748
      y 1342.196068670434
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:23758787"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re67"
      uniprot "NA"
    ]
    graphics [
      x 1221.6104592494069
      y 1749.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_73"
      name "double_minus_stranded_space_RNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa41"
      uniprot "NA"
    ]
    graphics [
      x 1762.5144998161113
      y 1903.8597507391748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:O15455;urn:miriam:uniprot:O15455;urn:miriam:ensembl:ENSG00000164342;urn:miriam:refseq:NM_003265;urn:miriam:ncbigene:7098;urn:miriam:ncbigene:7098;urn:miriam:hgnc.symbol:TLR3;urn:miriam:hgnc.symbol:TLR3;urn:miriam:hgnc:11849"
      hgnc "HGNC_SYMBOL:TLR3"
      map_id "M116_97"
      name "TLR3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa93"
      uniprot "UNIPROT:O15455"
    ]
    graphics [
      x 637.9673850663706
      y 1572.1662202339423
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "PUBMED:22539786;PUBMED:23758787"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_35"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re68"
      uniprot "NA"
    ]
    graphics [
      x 1425.8453706378386
      y 2442.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:TICAM1;urn:miriam:uniprot:Q8IUC6;urn:miriam:uniprot:Q8IUC6;urn:miriam:hgnc.symbol:TICAM1;urn:miriam:ensembl:ENSG00000127666;urn:miriam:hgnc:18348;urn:miriam:refseq:NM_014261;urn:miriam:ncbigene:148022;urn:miriam:ncbigene:148022"
      hgnc "HGNC_SYMBOL:TICAM1"
      map_id "M116_98"
      name "TICAM1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa94"
      uniprot "UNIPROT:Q8IUC6"
    ]
    graphics [
      x 1552.5144998161113
      y 2022.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:TRIM38;urn:miriam:hgnc.symbol:TRIM38;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:10475;urn:miriam:ncbigene:10475;urn:miriam:ensembl:ENSG00000112343;urn:miriam:refseq:NM_006355;urn:miriam:hgnc:10059;urn:miriam:uniprot:O00635;urn:miriam:uniprot:O00635"
      hgnc "HGNC_SYMBOL:TRIM38"
      map_id "M116_78"
      name "TRIM38"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa50"
      uniprot "UNIPROT:O00635"
    ]
    graphics [
      x 2060.00743414277
      y 2286.313744265185
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:TICAM1;urn:miriam:uniprot:Q8IUC6;urn:miriam:uniprot:Q8IUC6;urn:miriam:hgnc.symbol:TICAM1;urn:miriam:ensembl:ENSG00000127666;urn:miriam:hgnc:18348;urn:miriam:refseq:NM_014261;urn:miriam:ncbigene:148022;urn:miriam:ncbigene:148022"
      hgnc "HGNC_SYMBOL:TICAM1"
      map_id "M116_76"
      name "TICAM1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa48"
      uniprot "UNIPROT:Q8IUC6"
    ]
    graphics [
      x 1487.7845707541828
      y 2112.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "PUBMED:28829373"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_43"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re92"
      uniprot "NA"
    ]
    graphics [
      x 577.9673850663706
      y 1669.7908629539697
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033"
      hgnc "HGNC_SYMBOL:TRAF3"
      map_id "M116_52"
      name "TRAF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa116"
      uniprot "UNIPROT:Q13114"
    ]
    graphics [
      x 982.5144998161113
      y 1832.3785906698167
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033"
      hgnc "HGNC_SYMBOL:TRAF3"
      map_id "M116_51"
      name "TRAF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa11"
      uniprot "UNIPROT:Q13114"
    ]
    graphics [
      x 998.8536650413897
      y 866.5735736847697
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "PUBMED:32172672"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_19"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re103"
      uniprot "NA"
    ]
    graphics [
      x 1748.8536650413898
      y 646.8134512202714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780;PUBMED:32172672"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re100"
      uniprot "NA"
    ]
    graphics [
      x 1691.4760702265748
      y 1119.571063044361
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:9955;urn:miriam:hgnc:7794;urn:miriam:hgnc:9955;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ncbigene:5970;urn:miriam:ncbigene:5970;urn:miriam:refseq:NM_021975;urn:miriam:hgnc.symbol:RELA;urn:miriam:hgnc.symbol:RELA;urn:miriam:uniprot:Q04206;urn:miriam:uniprot:Q04206;urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998"
      hgnc "HGNC_SYMBOL:RELA;HGNC_SYMBOL:NFKB1"
      map_id "M116_14"
      name "P65_slash_P015"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa6"
      uniprot "UNIPROT:Q04206;UNIPROT:P19838"
    ]
    graphics [
      x 1928.8536650413898
      y 606.822479949718
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:5960;urn:miriam:hgnc:5961;urn:miriam:hgnc:1974;urn:miriam:hgnc:5960;urn:miriam:ensembl:ENSG00000104365;urn:miriam:ec-code:2.7.11.10;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:uniprot:O14920;urn:miriam:uniprot:O14920;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:ncbigene:3551;urn:miriam:ncbigene:3551;urn:miriam:refseq:NM_001190720;urn:miriam:hgnc.symbol:CHUK;urn:miriam:hgnc.symbol:CHUK;urn:miriam:refseq:NM_001278;urn:miriam:ec-code:2.7.11.10;urn:miriam:ensembl:ENSG00000213341;urn:miriam:hgnc:1974;urn:miriam:uniprot:O15111;urn:miriam:uniprot:O15111;urn:miriam:ncbigene:1147;urn:miriam:ncbigene:1147;urn:miriam:hgnc:5961;urn:miriam:ensembl:ENSG00000269335;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:refseq:NM_003639;urn:miriam:ncbigene:8517;urn:miriam:ncbigene:8517;urn:miriam:uniprot:Q9Y6K9;urn:miriam:uniprot:Q9Y6K9"
      hgnc "HGNC_SYMBOL:IKBKB;HGNC_SYMBOL:CHUK;HGNC_SYMBOL:IKBKG"
      map_id "M116_13"
      name "IKBKG_slash_IKBKB_slash_CHUK"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa4"
      uniprot "UNIPROT:O14920;UNIPROT:O15111;UNIPROT:Q9Y6K9"
    ]
    graphics [
      x 1660.4146427238684
      y 2172.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:P0DTC3;urn:miriam:ncbigene:43740569"
      hgnc "NA"
      map_id "M116_47"
      name "Orf3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1"
      uniprot "UNIPROT:P0DTC3"
    ]
    graphics [
      x 2018.8536650413898
      y 860.581334977754
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:9955;urn:miriam:hgnc:7794;urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998;urn:miriam:hgnc:9955;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ncbigene:5970;urn:miriam:ncbigene:5970;urn:miriam:refseq:NM_021975;urn:miriam:hgnc.symbol:RELA;urn:miriam:hgnc.symbol:RELA;urn:miriam:uniprot:Q04206;urn:miriam:uniprot:Q04206"
      hgnc "HGNC_SYMBOL:NFKB1;HGNC_SYMBOL:RELA"
      map_id "M116_15"
      name "P65_slash_P015"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa7"
      uniprot "UNIPROT:P19838;UNIPROT:Q04206"
    ]
    graphics [
      x 2617.9673850663708
      y 1548.994925936687
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "PUBMED:32172672"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_23"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re107"
      uniprot "NA"
    ]
    graphics [
      x 2107.02608180093
      y 2466.86764178217
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "PUBMED:32172672"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_22"
      name "NA"
      node_subtype "POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "re106"
      uniprot "NA"
    ]
    graphics [
      x 2197.9673850663708
      y 1669.855187574485
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_32"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re61"
      uniprot "NA"
    ]
    graphics [
      x 2767.9673850663708
      y 1655.6793485570597
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:5992;urn:miriam:hgnc.symbol:IL1B;urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:uniprot:P01584;urn:miriam:refseq:NM_000576;urn:miriam:ncbigene:3553;urn:miriam:ncbigene:3553;urn:miriam:ensembl:ENSG00000125538"
      hgnc "HGNC_SYMBOL:IL1B"
      map_id "M116_54"
      name "IL1b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa127"
      uniprot "UNIPROT:P01584"
    ]
    graphics [
      x 2707.653736562923
      y 1868.4685117132708
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M116_69"
      name "CASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa25"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 2137.9673850663708
      y 1664.456656486569
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:E;urn:miriam:ncbiprotein:1796318600"
      hgnc "NA"
      map_id "M116_95"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa90"
      uniprot "UNIPROT:E"
    ]
    graphics [
      x 2018.8536650413898
      y 764.5882051510855
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:5992;urn:miriam:hgnc.symbol:IL1B;urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:uniprot:P01584;urn:miriam:refseq:NM_000576;urn:miriam:ncbigene:3553;urn:miriam:ncbigene:3553;urn:miriam:ensembl:ENSG00000125538"
      hgnc "HGNC_SYMBOL:IL1B"
      map_id "M116_96"
      name "IL1b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa92"
      uniprot "UNIPROT:P01584"
    ]
    graphics [
      x 1851.6104592494069
      y 1712.60971039914
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_29"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re24"
      uniprot "NA"
    ]
    graphics [
      x 2681.476070226575
      y 1186.8616679859908
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M116_90"
      name "CASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa82"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 1595.5335816614925
      y 762.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ncbigene:29108;urn:miriam:ncbigene:29108;urn:miriam:refseq:NM_013258;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:ensembl:ENSG00000103490;urn:miriam:pubmed:32172672;urn:miriam:hgnc:16608;urn:miriam:uniprot:Q9ULZ3;urn:miriam:uniprot:Q9ULZ3"
      hgnc "HGNC_SYMBOL:PYCARD"
      map_id "M116_62"
      name "PYCARD"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa14"
      uniprot "UNIPROT:Q9ULZ3"
    ]
    graphics [
      x 2498.85366504139
      y 999.1880552358441
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:Q96P20;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:NLRP3"
      map_id "M116_48"
      name "NLRP3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa10"
      uniprot "UNIPROT:Q96P20"
    ]
    graphics [
      x 2648.85366504139
      y 950.1666353558861
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "PUBMED:31231549"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_20"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re104"
      uniprot "NA"
    ]
    graphics [
      x 2797.9673850663708
      y 1530.111162086949
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:Q96P20;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:NLRP3"
      map_id "M116_57"
      name "NLRP3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa134"
      uniprot "UNIPROT:Q96P20"
    ]
    graphics [
      x 2362.5144998161113
      y 1830.7588357785644
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ncbiprotein:BCD58760"
      hgnc "NA"
      map_id "M116_56"
      name "ORF8b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa132"
      uniprot "NA"
    ]
    graphics [
      x 1607.2023363703734
      y 912.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:E;urn:miriam:ncbiprotein:1796318600"
      hgnc "NA"
      map_id "M116_91"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa83"
      uniprot "UNIPROT:E"
    ]
    graphics [
      x 1879.8548263367193
      y 2451.0293302513946
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "PUBMED:32172672"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_46"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re97"
      uniprot "NA"
    ]
    graphics [
      x 2201.3014951593823
      y 446.7055175317363
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "PUBMED:28829373"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re70"
      uniprot "NA"
    ]
    graphics [
      x 1967.6607346737514
      y 551.1101942544981
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "PUBMED:15316659;PUBMED:17715238;PUBMED:25375324;PUBMED:19590927"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_26"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re111"
      uniprot "NA"
    ]
    graphics [
      x 2411.476070226575
      y 1458.0883274205173
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_63"
      name "s197"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa140"
      uniprot "NA"
    ]
    graphics [
      x 2181.6104592494066
      y 1743.5382961725277
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:14583;urn:miriam:uniprot:J9TC74;urn:miriam:hgnc:5013;urn:miriam:hgnc:20266;urn:miriam:hgnc:24048;urn:miriam:hgnc:20593;urn:miriam:ncbigene:23155;urn:miriam:ncbigene:23155;urn:miriam:hgnc:29675;urn:miriam:refseq:NM_015127;urn:miriam:hgnc.symbol:CLCC1;urn:miriam:uniprot:Q96S66;urn:miriam:uniprot:Q96S66;urn:miriam:hgnc.symbol:CLCC1;urn:miriam:ensembl:ENSG00000121940;urn:miriam:uniprot:Q9UH99;urn:miriam:uniprot:Q9UH99;urn:miriam:hgnc:14210;urn:miriam:ensembl:ENSG00000100242;urn:miriam:refseq:NM_001199579;urn:miriam:ncbigene:25777;urn:miriam:ncbigene:25777;urn:miriam:hgnc.symbol:SUN2;urn:miriam:hgnc.symbol:SUN2;urn:miriam:ncbigene:151188;urn:miriam:ncbigene:151188;urn:miriam:hgnc:24048;urn:miriam:hgnc.symbol:ARL6IP6;urn:miriam:hgnc.symbol:ARL6IP6;urn:miriam:uniprot:Q8N6S5;urn:miriam:uniprot:Q8N6S5;urn:miriam:refseq:NM_152522;urn:miriam:ensembl:ENSG00000177917;urn:miriam:ensembl:ENSG00000100292;urn:miriam:hgnc:5013;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:refseq:NM_002133;urn:miriam:ncbigene:3162;urn:miriam:uniprot:P09601;urn:miriam:uniprot:P09601;urn:miriam:ncbigene:3162;urn:miriam:ec-code:1.14.14.18;urn:miriam:uniprot:Q96JC1;urn:miriam:uniprot:Q96JC1;urn:miriam:hgnc.symbol:VPS39;urn:miriam:refseq:NM_015289;urn:miriam:hgnc.symbol:VPS39;urn:miriam:ensembl:ENSG00000166887;urn:miriam:ncbigene:23339;urn:miriam:ncbigene:23339;urn:miriam:hgnc:20593;urn:miriam:hgnc:14583;urn:miriam:ncbigene:55823;urn:miriam:refseq:NM_021729;urn:miriam:ncbigene:55823;urn:miriam:ensembl:ENSG00000160695;urn:miriam:hgnc.symbol:VPS11;urn:miriam:hgnc.symbol:VPS11;urn:miriam:uniprot:Q9H270;urn:miriam:uniprot:Q9H270;urn:miriam:hgnc.symbol:ALG5;urn:miriam:hgnc.symbol:ALG5;urn:miriam:ensembl:ENSG00000120697;urn:miriam:hgnc:20266;urn:miriam:ncbigene:29880;urn:miriam:ncbigene:29880;urn:miriam:uniprot:Q9Y673;urn:miriam:uniprot:Q9Y673;urn:miriam:ec-code:2.4.1.117;urn:miriam:refseq:NM_013338"
      hgnc "HGNC_SYMBOL:CLCC1;HGNC_SYMBOL:SUN2;HGNC_SYMBOL:ARL6IP6;HGNC_SYMBOL:HMOX1;HGNC_SYMBOL:VPS39;HGNC_SYMBOL:VPS11;HGNC_SYMBOL:ALG5"
      map_id "M116_8"
      name "Hops_space_Complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa2"
      uniprot "UNIPROT:J9TC74;UNIPROT:Q96S66;UNIPROT:Q9UH99;UNIPROT:Q8N6S5;UNIPROT:P09601;UNIPROT:Q96JC1;UNIPROT:Q9H270;UNIPROT:Q9Y673"
    ]
    graphics [
      x 1781.4760702265748
      y 1303.8597507391748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:TRIM59;urn:miriam:hgnc.symbol:TRIM59;urn:miriam:uniprot:Q8IWR1;urn:miriam:uniprot:Q8IWR1;urn:miriam:ensembl:ENSG00000213186;urn:miriam:pubmed:22588174;urn:miriam:hgnc:30834;urn:miriam:ncbigene:286827;urn:miriam:refseq:NM_173084;urn:miriam:ncbigene:286827"
      hgnc "HGNC_SYMBOL:TRIM59"
      map_id "M116_99"
      name "TRIM59"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa96"
      uniprot "UNIPROT:Q8IWR1"
    ]
    graphics [
      x 2019.3019144296195
      y 394.7293256034625
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:TRIM59;urn:miriam:hgnc.symbol:TRIM59;urn:miriam:uniprot:Q8IWR1;urn:miriam:uniprot:Q8IWR1;urn:miriam:ensembl:ENSG00000213186;urn:miriam:pubmed:22588174;urn:miriam:hgnc:30834;urn:miriam:ncbigene:286827;urn:miriam:refseq:NM_173084;urn:miriam:ncbigene:286827"
      hgnc "HGNC_SYMBOL:TRIM59"
      map_id "M116_70"
      name "TRIM59"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa3"
      uniprot "UNIPROT:Q8IWR1"
    ]
    graphics [
      x 2111.476070226575
      y 1262.577263483342
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "PUBMED:22588174"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_28"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re113"
      uniprot "NA"
    ]
    graphics [
      x 2318.85366504139
      y 718.1775798480458
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ncbigene:4615;urn:miriam:ensembl:ENSG00000172936;urn:miriam:ncbigene:4615;urn:miriam:uniprot:Q99836;urn:miriam:uniprot:Q99836;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc:7562;urn:miriam:refseq:NM_002468"
      hgnc "HGNC_SYMBOL:MYD88"
      map_id "M116_67"
      name "MYD88"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa144"
      uniprot "UNIPROT:Q99836"
    ]
    graphics [
      x 2007.3780951939943
      y 312.34606812529705
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ncbigene:4615;urn:miriam:ensembl:ENSG00000172936;urn:miriam:ncbigene:4615;urn:miriam:uniprot:Q99836;urn:miriam:uniprot:Q99836;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc:7562;urn:miriam:refseq:NM_002468"
      hgnc "HGNC_SYMBOL:MYD88"
      map_id "M116_66"
      name "MYD88"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa143"
      uniprot "UNIPROT:Q99836"
    ]
    graphics [
      x 2651.476070226575
      y 1103.787242998976
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "PUBMED:28829373"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_27"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re112"
      uniprot "NA"
    ]
    graphics [
      x 2411.476070226575
      y 1337.5823895979697
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:refseq:NM_138554;urn:miriam:ncbigene:7099;urn:miriam:ncbigene:7099;urn:miriam:ec-code:3.2.2.6;urn:miriam:hgnc:11850;urn:miriam:uniprot:O00206;urn:miriam:uniprot:O00206;urn:miriam:hgnc.symbol:TLR4;urn:miriam:hgnc.symbol:TLR4;urn:miriam:ensembl:ENSG00000136869"
      hgnc "HGNC_SYMBOL:TLR4"
      map_id "M116_65"
      name "TLR4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa142"
      uniprot "UNIPROT:O00206"
    ]
    graphics [
      x 2141.476070226575
      y 1498.6291880342608
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:O14896;urn:miriam:uniprot:O14896;urn:miriam:refseq:NM_006147;urn:miriam:ensembl:ENSG00000117595;urn:miriam:hgnc:6121;urn:miriam:ncbigene:3664;urn:miriam:ncbigene:3664;urn:miriam:hgnc.symbol:IRF6;urn:miriam:hgnc.symbol:IRF6"
      hgnc "HGNC_SYMBOL:IRF6"
      map_id "M116_64"
      name "IRF6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa141"
      uniprot "UNIPROT:O14896"
    ]
    graphics [
      x 2227.9673850663708
      y 1610.5567968186347
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:11850;urn:miriam:hgnc:6121;urn:miriam:hgnc:7562;urn:miriam:uniprot:O14896;urn:miriam:uniprot:O14896;urn:miriam:refseq:NM_006147;urn:miriam:ensembl:ENSG00000117595;urn:miriam:hgnc:6121;urn:miriam:ncbigene:3664;urn:miriam:ncbigene:3664;urn:miriam:hgnc.symbol:IRF6;urn:miriam:hgnc.symbol:IRF6;urn:miriam:ncbigene:4615;urn:miriam:ensembl:ENSG00000172936;urn:miriam:ncbigene:4615;urn:miriam:uniprot:Q99836;urn:miriam:uniprot:Q99836;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc:7562;urn:miriam:refseq:NM_002468;urn:miriam:refseq:NM_138554;urn:miriam:ncbigene:7099;urn:miriam:ncbigene:7099;urn:miriam:ec-code:3.2.2.6;urn:miriam:hgnc:11850;urn:miriam:uniprot:O00206;urn:miriam:uniprot:O00206;urn:miriam:hgnc.symbol:TLR4;urn:miriam:hgnc.symbol:TLR4;urn:miriam:ensembl:ENSG00000136869"
      hgnc "HGNC_SYMBOL:IRF6;HGNC_SYMBOL:MYD88;HGNC_SYMBOL:TLR4"
      map_id "M116_16"
      name "LPS_slash_TLR4_slash_MYD88"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa8"
      uniprot "UNIPROT:O14896;UNIPROT:Q99836;UNIPROT:O00206"
    ]
    graphics [
      x 2531.476070226575
      y 1136.9032661139092
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "PUBMED:23758787"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_30"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re32"
      uniprot "NA"
    ]
    graphics [
      x 1311.630178766627
      y 192.06502830998443
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7189;urn:miriam:ncbigene:7189;urn:miriam:ensembl:ENSG00000175104;urn:miriam:uniprot:Q9Y4K3;urn:miriam:uniprot:Q9Y4K3;urn:miriam:hgnc:12036;urn:miriam:refseq:NM_145803;urn:miriam:hgnc.symbol:TRAF6;urn:miriam:hgnc.symbol:TRAF6"
      hgnc "HGNC_SYMBOL:TRAF6"
      map_id "M116_58"
      name "TRAF6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa135"
      uniprot "UNIPROT:Q9Y4K3"
    ]
    graphics [
      x 948.7231049997465
      y 249.93295747279194
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7189;urn:miriam:ncbigene:7189;urn:miriam:ensembl:ENSG00000175104;urn:miriam:uniprot:Q9Y4K3;urn:miriam:uniprot:Q9Y4K3;urn:miriam:hgnc:12036;urn:miriam:refseq:NM_145803;urn:miriam:hgnc.symbol:TRAF6;urn:miriam:hgnc.symbol:TRAF6"
      hgnc "HGNC_SYMBOL:TRAF6"
      map_id "M116_50"
      name "TRAF6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa101"
      uniprot "UNIPROT:Q9Y4K3"
    ]
    graphics [
      x 1826.090149807047
      y 526.8134512202714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      annotation "PUBMED:15361868;PUBMED:22539786;PUBMED:20724660"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_39"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re75"
      uniprot "NA"
    ]
    graphics [
      x 1691.4760702265748
      y 1449.571063044361
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ncbigene:4615;urn:miriam:ensembl:ENSG00000172936;urn:miriam:ncbigene:4615;urn:miriam:uniprot:Q99836;urn:miriam:uniprot:Q99836;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc:7562;urn:miriam:refseq:NM_002468"
      hgnc "HGNC_SYMBOL:MYD88"
      map_id "M116_77"
      name "MYD88"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa49"
      uniprot "UNIPROT:Q99836"
    ]
    graphics [
      x 1252.5144998161113
      y 1962.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:TRIM38;urn:miriam:hgnc.symbol:TRIM38;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:10475;urn:miriam:ncbigene:10475;urn:miriam:ensembl:ENSG00000112343;urn:miriam:refseq:NM_006355;urn:miriam:hgnc:10059;urn:miriam:uniprot:O00635;urn:miriam:uniprot:O00635"
      hgnc "HGNC_SYMBOL:TRIM38"
      map_id "M116_79"
      name "TRIM38"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa52"
      uniprot "UNIPROT:O00635"
    ]
    graphics [
      x 2317.9673850663708
      y 1741.6621827259057
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:uniprot:P36406;urn:miriam:uniprot:P36406;urn:miriam:ensembl:ENSG00000113595;urn:miriam:hgnc.symbol:TRIM23;urn:miriam:hgnc.symbol:TRIM23;urn:miriam:hgnc:660;urn:miriam:ncbigene:373;urn:miriam:refseq:NM_001656;urn:miriam:ncbigene:373"
      hgnc "HGNC_SYMBOL:TRIM23"
      map_id "M116_84"
      name "TRIM23"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa70"
      uniprot "UNIPROT:P36406"
    ]
    graphics [
      x 1494.1900265952593
      y 1572.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7189;urn:miriam:ncbigene:7189;urn:miriam:ensembl:ENSG00000175104;urn:miriam:uniprot:Q9Y4K3;urn:miriam:uniprot:Q9Y4K3;urn:miriam:hgnc:12036;urn:miriam:refseq:NM_145803;urn:miriam:hgnc.symbol:TRAF6;urn:miriam:hgnc.symbol:TRAF6"
      hgnc "HGNC_SYMBOL:TRAF6"
      map_id "M116_72"
      name "TRAF6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa38"
      uniprot "UNIPROT:Q9Y4K3"
    ]
    graphics [
      x 2108.85366504139
      y 973.1270120257964
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "PUBMED:18345001;PUBMED:25172371;PUBMED:23758787"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_40"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re88"
      uniprot "NA"
    ]
    graphics [
      x 2201.476070226575
      y 1110.5867359968995
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:6859;urn:miriam:hgnc:30681;urn:miriam:hgnc:17075;urn:miriam:hgnc.symbol:TAB2;urn:miriam:ensembl:ENSG00000055208;urn:miriam:hgnc.symbol:TAB2;urn:miriam:uniprot:Q9NYJ8;urn:miriam:uniprot:Q9NYJ8;urn:miriam:hgnc:17075;urn:miriam:ncbigene:23118;urn:miriam:refseq:NM_001292034;urn:miriam:ncbigene:23118;urn:miriam:uniprot:O43318;urn:miriam:uniprot:O43318;urn:miriam:ensembl:ENSG00000135341;urn:miriam:refseq:NM_145331;urn:miriam:hgnc:6859;urn:miriam:ncbigene:6885;urn:miriam:ncbigene:6885;urn:miriam:hgnc.symbol:MAP3K7;urn:miriam:hgnc.symbol:MAP3K7;urn:miriam:ec-code:2.7.11.25;urn:miriam:ensembl:ENSG00000157625;urn:miriam:ncbigene:257397;urn:miriam:ncbigene:257397;urn:miriam:hgnc.symbol:TAB3;urn:miriam:hgnc.symbol:TAB3;urn:miriam:uniprot:Q8N5C8;urn:miriam:uniprot:Q8N5C8;urn:miriam:hgnc:30681;urn:miriam:refseq:NM_152787"
      hgnc "HGNC_SYMBOL:TAB2;HGNC_SYMBOL:MAP3K7;HGNC_SYMBOL:TAB3"
      map_id "M116_7"
      name "TAB2_slash_TAB3_slash_TAK1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa19"
      uniprot "UNIPROT:Q9NYJ8;UNIPROT:O43318;UNIPROT:Q8N5C8"
    ]
    graphics [
      x 2006.090149807047
      y 507.65059505919703
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:P15533;urn:miriam:hgnc:10059;urn:miriam:hgnc.symbol:TRIM38;urn:miriam:hgnc.symbol:TRIM38;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:10475;urn:miriam:ncbigene:10475;urn:miriam:ensembl:ENSG00000112343;urn:miriam:refseq:NM_006355;urn:miriam:hgnc:10059;urn:miriam:uniprot:O00635;urn:miriam:uniprot:O00635"
      hgnc "HGNC_SYMBOL:TRIM38"
      map_id "M116_2"
      name "TRIM30a_slash_TRIM38"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa11"
      uniprot "UNIPROT:P15533;UNIPROT:O00635"
    ]
    graphics [
      x 1732.5144998161113
      y 1915.9277406352314
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:6859;urn:miriam:hgnc:30681;urn:miriam:hgnc:17075;urn:miriam:hgnc.symbol:TAB2;urn:miriam:ensembl:ENSG00000055208;urn:miriam:hgnc.symbol:TAB2;urn:miriam:uniprot:Q9NYJ8;urn:miriam:uniprot:Q9NYJ8;urn:miriam:hgnc:17075;urn:miriam:ncbigene:23118;urn:miriam:refseq:NM_001292034;urn:miriam:ncbigene:23118;urn:miriam:uniprot:O43318;urn:miriam:uniprot:O43318;urn:miriam:ensembl:ENSG00000135341;urn:miriam:refseq:NM_145331;urn:miriam:hgnc:6859;urn:miriam:ncbigene:6885;urn:miriam:ncbigene:6885;urn:miriam:hgnc.symbol:MAP3K7;urn:miriam:hgnc.symbol:MAP3K7;urn:miriam:ec-code:2.7.11.25;urn:miriam:ensembl:ENSG00000157625;urn:miriam:ncbigene:257397;urn:miriam:ncbigene:257397;urn:miriam:hgnc.symbol:TAB3;urn:miriam:hgnc.symbol:TAB3;urn:miriam:uniprot:Q8N5C8;urn:miriam:uniprot:Q8N5C8;urn:miriam:hgnc:30681;urn:miriam:refseq:NM_152787"
      hgnc "HGNC_SYMBOL:TAB2;HGNC_SYMBOL:MAP3K7;HGNC_SYMBOL:TAB3"
      map_id "M116_1"
      name "TAB2_slash_TAB3_slash_TAK1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa10"
      uniprot "UNIPROT:Q9NYJ8;UNIPROT:O43318;UNIPROT:Q8N5C8"
    ]
    graphics [
      x 1659.3007671489752
      y 2142.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      annotation "PUBMED:17706453"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_42"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re90"
      uniprot "NA"
    ]
    graphics [
      x 1237.9673850663708
      y 1599.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "PUBMED:27695001;PUBMED:26358190;PUBMED:23408607;PUBMED:23758787;PUBMED:24379373;PUBMED:20724660"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_41"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re89"
      uniprot "NA"
    ]
    graphics [
      x 1791.6104592494069
      y 1753.8597507391748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:5960;urn:miriam:hgnc:5961;urn:miriam:hgnc:1974;urn:miriam:hgnc.symbol:CHUK;urn:miriam:hgnc.symbol:CHUK;urn:miriam:refseq:NM_001278;urn:miriam:ec-code:2.7.11.10;urn:miriam:ensembl:ENSG00000213341;urn:miriam:hgnc:1974;urn:miriam:uniprot:O15111;urn:miriam:uniprot:O15111;urn:miriam:ncbigene:1147;urn:miriam:ncbigene:1147;urn:miriam:hgnc:5961;urn:miriam:ensembl:ENSG00000269335;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:refseq:NM_003639;urn:miriam:ncbigene:8517;urn:miriam:ncbigene:8517;urn:miriam:uniprot:Q9Y6K9;urn:miriam:uniprot:Q9Y6K9;urn:miriam:hgnc:5960;urn:miriam:ensembl:ENSG00000104365;urn:miriam:ec-code:2.7.11.10;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:uniprot:O14920;urn:miriam:uniprot:O14920;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:ncbigene:3551;urn:miriam:ncbigene:3551;urn:miriam:refseq:NM_001190720"
      hgnc "HGNC_SYMBOL:CHUK;HGNC_SYMBOL:IKBKG;HGNC_SYMBOL:IKBKB"
      map_id "M116_9"
      name "NEMO_slash_IKKA_slash_IKKB"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa20"
      uniprot "UNIPROT:O15111;UNIPROT:Q9Y6K9;UNIPROT:O14920"
    ]
    graphics [
      x 1987.9673850663708
      y 1517.606843746084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:hgnc:16379;urn:miriam:uniprot:Q8IYM9;urn:miriam:uniprot:Q8IYM9;urn:miriam:ensembl:ENSG00000132274;urn:miriam:refseq:NM_006074;urn:miriam:ncbigene:10346;urn:miriam:ncbigene:10346;urn:miriam:hgnc.symbol:TRIM22;urn:miriam:hgnc.symbol:TRIM22"
      hgnc "HGNC_SYMBOL:TRIM22"
      map_id "M116_80"
      name "TRIM22"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa56"
      uniprot "UNIPROT:Q8IYM9"
    ]
    graphics [
      x 1061.4760702265748
      y 1383.6936536421204
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ncbigene:23650;urn:miriam:ncbigene:23650;urn:miriam:hgnc.symbol:TRIM29;urn:miriam:refseq:NM_012101;urn:miriam:uniprot:Q14134;urn:miriam:uniprot:Q14134;urn:miriam:hgnc.symbol:TRIM29;urn:miriam:ensembl:ENSG00000137699;urn:miriam:hgnc:17274"
      hgnc "HGNC_SYMBOL:TRIM29"
      map_id "M116_83"
      name "TRIM29"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa67"
      uniprot "UNIPROT:Q14134"
    ]
    graphics [
      x 1511.4760702265748
      y 1392.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:16283;urn:miriam:hgnc:660;urn:miriam:uniprot:Q14142;urn:miriam:uniprot:Q14142;urn:miriam:hgnc.symbol:TRIM14;urn:miriam:ensembl:ENSG00000106785;urn:miriam:hgnc.symbol:TRIM14;urn:miriam:ncbigene:9830;urn:miriam:refseq:NM_014788;urn:miriam:ncbigene:9830;urn:miriam:hgnc:16283;urn:miriam:ec-code:2.3.2.27;urn:miriam:uniprot:P36406;urn:miriam:uniprot:P36406;urn:miriam:ensembl:ENSG00000113595;urn:miriam:hgnc.symbol:TRIM23;urn:miriam:hgnc.symbol:TRIM23;urn:miriam:hgnc:660;urn:miriam:ncbigene:373;urn:miriam:refseq:NM_001656;urn:miriam:ncbigene:373"
      hgnc "HGNC_SYMBOL:TRIM14;HGNC_SYMBOL:TRIM23"
      map_id "M116_5"
      name "TRIM14_slash_TRIM23"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa15"
      uniprot "UNIPROT:Q14142;UNIPROT:P36406"
    ]
    graphics [
      x 2066.7731733989112
      y 1973.3608844567657
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:9975;urn:miriam:hgnc:11312;urn:miriam:hgnc.symbol:TRIM27;urn:miriam:ensembl:ENSG00000204713;urn:miriam:hgnc.symbol:TRIM27;urn:miriam:hgnc:9975;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_030950;urn:miriam:ncbigene:5987;urn:miriam:uniprot:P14373;urn:miriam:uniprot:P14373;urn:miriam:ncbigene:5987;urn:miriam:uniprot:P19474;urn:miriam:uniprot:P19474;urn:miriam:ncbigene:6737;urn:miriam:ncbigene:6737;urn:miriam:ec-code:2.3.2.27;urn:miriam:hgnc:11312;urn:miriam:refseq:NM_003141;urn:miriam:hgnc.symbol:TRIM21;urn:miriam:hgnc.symbol:TRIM21;urn:miriam:ensembl:ENSG00000132109"
      hgnc "HGNC_SYMBOL:TRIM27;HGNC_SYMBOL:TRIM21"
      map_id "M116_4"
      name "TRIM27_slash_TRIM21"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa13"
      uniprot "UNIPROT:P14373;UNIPROT:P19474"
    ]
    graphics [
      x 1451.4760702265748
      y 1242.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:5960;urn:miriam:hgnc:5961;urn:miriam:hgnc:1974;urn:miriam:hgnc.symbol:CHUK;urn:miriam:hgnc.symbol:CHUK;urn:miriam:refseq:NM_001278;urn:miriam:ec-code:2.7.11.10;urn:miriam:ensembl:ENSG00000213341;urn:miriam:hgnc:1974;urn:miriam:uniprot:O15111;urn:miriam:uniprot:O15111;urn:miriam:ncbigene:1147;urn:miriam:ncbigene:1147;urn:miriam:hgnc:5961;urn:miriam:ensembl:ENSG00000269335;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:refseq:NM_003639;urn:miriam:ncbigene:8517;urn:miriam:ncbigene:8517;urn:miriam:uniprot:Q9Y6K9;urn:miriam:uniprot:Q9Y6K9;urn:miriam:hgnc:5960;urn:miriam:ensembl:ENSG00000104365;urn:miriam:ec-code:2.7.11.10;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:uniprot:O14920;urn:miriam:uniprot:O14920;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:ncbigene:3551;urn:miriam:ncbigene:3551;urn:miriam:refseq:NM_001190720"
      hgnc "HGNC_SYMBOL:CHUK;HGNC_SYMBOL:IKBKG;HGNC_SYMBOL:IKBKB"
      map_id "M116_3"
      name "NEMO_slash_IKKA_slash_IKKB"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa12"
      uniprot "UNIPROT:O15111;UNIPROT:Q9Y6K9;UNIPROT:O14920"
    ]
    graphics [
      x 1210.9744346231273
      y 509.6116938271982
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      annotation "PUBMED:21135871"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re50"
      uniprot "NA"
    ]
    graphics [
      x 1376.090149807047
      y 402.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:7797;urn:miriam:refseq:NM_020529;urn:miriam:ensembl:ENSG00000100906;urn:miriam:uniprot:P25963;urn:miriam:uniprot:P25963;urn:miriam:ncbigene:4792;urn:miriam:ncbigene:4792;urn:miriam:hgnc.symbol:NFKBIA;urn:miriam:hgnc.symbol:NFKBIA"
      hgnc "HGNC_SYMBOL:NFKBIA"
      map_id "M116_81"
      name "NFKBIA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa64"
      uniprot "UNIPROT:P25963"
    ]
    graphics [
      x 2111.476070226575
      y 1425.3734055475652
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000166167;urn:miriam:uniprot:Q9Y297;urn:miriam:uniprot:Q9Y297;urn:miriam:hgnc:1144;urn:miriam:hgnc.symbol:BTRC;urn:miriam:hgnc.symbol:BTRC;urn:miriam:ncbigene:8945;urn:miriam:ncbigene:8945;urn:miriam:refseq:NM_033637"
      hgnc "HGNC_SYMBOL:BTRC"
      map_id "M116_86"
      name "BTRC"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa75"
      uniprot "UNIPROT:Q9Y297"
    ]
    graphics [
      x 641.4760702265747
      y 1092.2639194815613
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:7797;urn:miriam:refseq:NM_020529;urn:miriam:ensembl:ENSG00000100906;urn:miriam:uniprot:P25963;urn:miriam:uniprot:P25963;urn:miriam:ncbigene:4792;urn:miriam:ncbigene:4792;urn:miriam:hgnc.symbol:NFKBIA;urn:miriam:hgnc.symbol:NFKBIA"
      hgnc "HGNC_SYMBOL:NFKBIA"
      map_id "M116_82"
      name "NFKBIA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa65"
      uniprot "UNIPROT:P25963"
    ]
    graphics [
      x 1928.8536650413898
      y 694.1393266859776
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      annotation "PUBMED:26999213"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_44"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re93"
      uniprot "NA"
    ]
    graphics [
      x 2621.476070226575
      y 1405.6020246465823
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998"
      hgnc "HGNC_SYMBOL:NFKB1"
      map_id "M116_89"
      name "NFKB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa81"
      uniprot "UNIPROT:P19838"
    ]
    graphics [
      x 2591.476070226575
      y 1432.9863831703642
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:CACTIN;urn:miriam:hgnc.symbol:CACTIN;urn:miriam:uniprot:Q8WUQ7;urn:miriam:uniprot:Q8WUQ7;urn:miriam:ensembl:ENSG00000105298;urn:miriam:refseq:NM_001080543;urn:miriam:hgnc:29938;urn:miriam:ncbigene:58509;urn:miriam:ncbigene:58509"
      hgnc "HGNC_SYMBOL:CACTIN"
      map_id "M116_60"
      name "CACTIN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa138"
      uniprot "UNIPROT:Q8WUQ7"
    ]
    graphics [
      x 2678.85366504139
      y 887.4312846096324
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:7797;urn:miriam:refseq:NM_020529;urn:miriam:ensembl:ENSG00000100906;urn:miriam:uniprot:P25963;urn:miriam:uniprot:P25963;urn:miriam:ncbigene:4792;urn:miriam:ncbigene:4792;urn:miriam:hgnc.symbol:NFKBIA;urn:miriam:hgnc.symbol:NFKBIA;urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998"
      hgnc "HGNC_SYMBOL:NFKBIA;HGNC_SYMBOL:NFKB1"
      map_id "M116_10"
      name "NFKB1:NFKNIA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa21"
      uniprot "UNIPROT:P25963;UNIPROT:P19838"
    ]
    graphics [
      x 1511.4760702265748
      y 1242.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "PUBMED:26999213"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_24"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re109"
      uniprot "NA"
    ]
    graphics [
      x 2557.1167571280357
      y 727.5089080905373
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:CACTIN;urn:miriam:hgnc.symbol:CACTIN;urn:miriam:uniprot:Q8WUQ7;urn:miriam:uniprot:Q8WUQ7;urn:miriam:ensembl:ENSG00000105298;urn:miriam:refseq:NM_001080543;urn:miriam:hgnc:29938;urn:miriam:ncbigene:58509;urn:miriam:ncbigene:58509"
      hgnc "HGNC_SYMBOL:CACTIN"
      map_id "M116_88"
      name "CACTIN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa77"
      uniprot "UNIPROT:Q8WUQ7"
    ]
    graphics [
      x 1661.4760702265748
      y 1236.7446462369894
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:refseq:NM_172016;urn:miriam:hgnc.symbol:TRIM39;urn:miriam:hgnc.symbol:TRIM39;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:56658;urn:miriam:ncbigene:56658;urn:miriam:hgnc:10065;urn:miriam:uniprot:Q9HCM9;urn:miriam:uniprot:Q9HCM9;urn:miriam:ensembl:ENSG00000204599"
      hgnc "HGNC_SYMBOL:TRIM39"
      map_id "M116_61"
      name "TRIM39"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa139"
      uniprot "UNIPROT:Q9HCM9"
    ]
    graphics [
      x 2738.85366504139
      y 1072.8004847563163
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      annotation "PUBMED:26999213"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_25"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re110"
      uniprot "NA"
    ]
    graphics [
      x 2228.85366504139
      y 940.9800341986177
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:refseq:NM_172016;urn:miriam:hgnc.symbol:TRIM39;urn:miriam:hgnc.symbol:TRIM39;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:56658;urn:miriam:ncbigene:56658;urn:miriam:hgnc:10065;urn:miriam:uniprot:Q9HCM9;urn:miriam:uniprot:Q9HCM9;urn:miriam:ensembl:ENSG00000204599"
      hgnc "HGNC_SYMBOL:TRIM39"
      map_id "M116_87"
      name "TRIM39"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa76"
      uniprot "UNIPROT:Q9HCM9"
    ]
    graphics [
      x 1931.4760702265748
      y 1052.6254878021132
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      annotation "PUBMED:21135871"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_21"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re105"
      uniprot "NA"
    ]
    graphics [
      x 1688.8536650413898
      y 672.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000166167;urn:miriam:uniprot:Q9Y297;urn:miriam:uniprot:Q9Y297;urn:miriam:hgnc:1144;urn:miriam:hgnc.symbol:BTRC;urn:miriam:hgnc.symbol:BTRC;urn:miriam:ncbigene:8945;urn:miriam:ncbigene:8945;urn:miriam:refseq:NM_033637"
      hgnc "HGNC_SYMBOL:BTRC"
      map_id "M116_59"
      name "BTRC"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa137"
      uniprot "UNIPROT:Q9Y297"
    ]
    graphics [
      x 2019.8576347084795
      y 467.19685796002045
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:16288;urn:miriam:ec-code:2.3.2.27;urn:miriam:ensembl:ENSG00000100505;urn:miriam:ncbigene:114088;urn:miriam:ncbigene:114088;urn:miriam:uniprot:Q9C026;urn:miriam:uniprot:Q9C026;urn:miriam:hgnc.symbol:TRIM9;urn:miriam:refseq:NM_015163;urn:miriam:hgnc.symbol:TRIM9"
      hgnc "HGNC_SYMBOL:TRIM9"
      map_id "M116_85"
      name "TRIM9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa74"
      uniprot "UNIPROT:Q9C026"
    ]
    graphics [
      x 2291.476070226575
      y 1264.1444038133232
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:P08697;urn:miriam:uniprot:P08697;urn:miriam:ensembl:ENSG00000167711;urn:miriam:ncbigene:5345;urn:miriam:ncbigene:5345;urn:miriam:hgnc:9075;urn:miriam:refseq:NM_000934;urn:miriam:hgnc.symbol:SERPINF2;urn:miriam:hgnc.symbol:SERPINF2"
      hgnc "HGNC_SYMBOL:SERPINF2"
      map_id "M116_92"
      name "SERPINF2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa86"
      uniprot "UNIPROT:P08697"
    ]
    graphics [
      x 1941.6104592494069
      y 1720.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:P08697;urn:miriam:uniprot:P08697;urn:miriam:ensembl:ENSG00000167711;urn:miriam:ncbigene:5345;urn:miriam:ncbigene:5345;urn:miriam:hgnc:9075;urn:miriam:refseq:NM_000934;urn:miriam:hgnc.symbol:SERPINF2;urn:miriam:hgnc.symbol:SERPINF2"
      hgnc "HGNC_SYMBOL:SERPINF2"
      map_id "M116_93"
      name "SERPINF2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa87"
      uniprot "UNIPROT:P08697"
    ]
    graphics [
      x 818.8536650413897
      y 603.0668730066709
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      annotation "PUBMED:17706453"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_33"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re64"
      uniprot "NA"
    ]
    graphics [
      x 1381.1268904536928
      y 432.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000171855;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc:5434;urn:miriam:uniprot:P01574;urn:miriam:uniprot:P01574;urn:miriam:refseq:NM_002176;urn:miriam:ncbigene:3456;urn:miriam:ncbigene:3456"
      hgnc "HGNC_SYMBOL:IFNB1"
      map_id "M116_94"
      name "IFNB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa88"
      uniprot "UNIPROT:P01574"
    ]
    graphics [
      x 1838.8536650413898
      y 639.1358858836597
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000171855;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc:5434;urn:miriam:uniprot:P01574;urn:miriam:uniprot:P01574;urn:miriam:refseq:NM_002176;urn:miriam:ncbigene:3456;urn:miriam:ncbigene:3456"
      hgnc "HGNC_SYMBOL:IFNB1"
      map_id "M116_68"
      name "IFNB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa147"
      uniprot "UNIPROT:P01574"
    ]
    graphics [
      x 1886.090149807047
      y 567.7524189556523
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "PUBMED:15361868"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_38"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re73"
      uniprot "NA"
    ]
    graphics [
      x 670.4146427238683
      y 2135.40497319987
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ncbigene:4615;urn:miriam:ensembl:ENSG00000172936;urn:miriam:ncbigene:4615;urn:miriam:uniprot:Q99836;urn:miriam:uniprot:Q99836;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc:7562;urn:miriam:refseq:NM_002468"
      hgnc "HGNC_SYMBOL:MYD88"
      map_id "M116_49"
      name "MYD88"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa100"
      uniprot "UNIPROT:Q99836"
    ]
    graphics [
      x 922.5144998161113
      y 1846.6019746441584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:15631;urn:miriam:hgnc:15632;urn:miriam:hgnc:15633;urn:miriam:uniprot:Q9NYK1;urn:miriam:uniprot:Q9NYK1;urn:miriam:hgnc:15631;urn:miriam:refseq:NM_016562;urn:miriam:hgnc.symbol:TLR7;urn:miriam:hgnc.symbol:TLR7;urn:miriam:ensembl:ENSG00000196664;urn:miriam:ncbigene:51284;urn:miriam:ncbigene:51284;urn:miriam:uniprot:Q9NR96;urn:miriam:uniprot:Q9NR96;urn:miriam:ncbigene:54106;urn:miriam:ncbigene:54106;urn:miriam:ensembl:ENSG00000239732;urn:miriam:hgnc.symbol:TLR9;urn:miriam:hgnc.symbol:TLR9;urn:miriam:refseq:NM_017442;urn:miriam:hgnc:15633;urn:miriam:refseq:NM_016610;urn:miriam:uniprot:Q9NR97;urn:miriam:uniprot:Q9NR97;urn:miriam:hgnc:15632;urn:miriam:ncbigene:51311;urn:miriam:ncbigene:51311;urn:miriam:hgnc.symbol:TLR8;urn:miriam:ensembl:ENSG00000101916;urn:miriam:hgnc.symbol:TLR8"
      hgnc "HGNC_SYMBOL:TLR7;HGNC_SYMBOL:TLR9;HGNC_SYMBOL:TLR8"
      map_id "M116_6"
      name "TLR7_slash_8_slash_9"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa17"
      uniprot "UNIPROT:Q9NYK1;UNIPROT:Q9NR96;UNIPROT:Q9NR97"
    ]
    graphics [
      x 877.9673850663706
      y 1486.9833268361392
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      annotation "PUBMED:21782231"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_37"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re72"
      uniprot "NA"
    ]
    graphics [
      x 1841.4760702265748
      y 1329.5038394876133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:15631;urn:miriam:hgnc:15632;urn:miriam:hgnc:15633;urn:miriam:refseq:NM_016610;urn:miriam:uniprot:Q9NR97;urn:miriam:uniprot:Q9NR97;urn:miriam:hgnc:15632;urn:miriam:ncbigene:51311;urn:miriam:ncbigene:51311;urn:miriam:hgnc.symbol:TLR8;urn:miriam:ensembl:ENSG00000101916;urn:miriam:hgnc.symbol:TLR8;urn:miriam:uniprot:Q9NR96;urn:miriam:uniprot:Q9NR96;urn:miriam:ncbigene:54106;urn:miriam:ncbigene:54106;urn:miriam:ensembl:ENSG00000239732;urn:miriam:hgnc.symbol:TLR9;urn:miriam:hgnc.symbol:TLR9;urn:miriam:refseq:NM_017442;urn:miriam:hgnc:15633;urn:miriam:uniprot:Q9NYK1;urn:miriam:uniprot:Q9NYK1;urn:miriam:hgnc:15631;urn:miriam:refseq:NM_016562;urn:miriam:hgnc.symbol:TLR7;urn:miriam:hgnc.symbol:TLR7;urn:miriam:ensembl:ENSG00000196664;urn:miriam:ncbigene:51284;urn:miriam:ncbigene:51284"
      hgnc "HGNC_SYMBOL:TLR8;HGNC_SYMBOL:TLR9;HGNC_SYMBOL:TLR7"
      map_id "M116_17"
      name "TLR7_slash_8_slash_9"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa9"
      uniprot "UNIPROT:Q9NR97;UNIPROT:Q9NR96;UNIPROT:Q9NYK1"
    ]
    graphics [
      x 1511.4760702265748
      y 1482.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_74"
      name "ssRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa42"
      uniprot "NA"
    ]
    graphics [
      x 1748.8536650413898
      y 796.8134512202714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_53"
      name "s187"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa124"
      uniprot "NA"
    ]
    graphics [
      x 1787.2023363703734
      y 991.7974116997934
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "M116_71"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa34"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 2332.808133289244
      y 751.3530460448193
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      annotation "PUBMED:32172672"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re96"
      uniprot "NA"
    ]
    graphics [
      x 2219.826876506911
      y 2342.522457289507
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:5960;urn:miriam:hgnc:5961;urn:miriam:hgnc:1974;urn:miriam:hgnc:5960;urn:miriam:ensembl:ENSG00000104365;urn:miriam:ec-code:2.7.11.10;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:uniprot:O14920;urn:miriam:uniprot:O14920;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:ncbigene:3551;urn:miriam:ncbigene:3551;urn:miriam:refseq:NM_001190720;urn:miriam:hgnc.symbol:CHUK;urn:miriam:hgnc.symbol:CHUK;urn:miriam:refseq:NM_001278;urn:miriam:ec-code:2.7.11.10;urn:miriam:ensembl:ENSG00000213341;urn:miriam:hgnc:1974;urn:miriam:uniprot:O15111;urn:miriam:uniprot:O15111;urn:miriam:ncbigene:1147;urn:miriam:ncbigene:1147;urn:miriam:hgnc:5961;urn:miriam:ensembl:ENSG00000269335;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:refseq:NM_003639;urn:miriam:ncbigene:8517;urn:miriam:ncbigene:8517;urn:miriam:uniprot:Q9Y6K9;urn:miriam:uniprot:Q9Y6K9"
      hgnc "HGNC_SYMBOL:IKBKB;HGNC_SYMBOL:CHUK;HGNC_SYMBOL:IKBKG"
      map_id "M116_11"
      name "IKBKG_slash_IKBKB_slash_CHUK"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa22"
      uniprot "UNIPROT:O14920;UNIPROT:O15111;UNIPROT:Q9Y6K9"
    ]
    graphics [
      x 2647.9673850663708
      y 1561.7464025950017
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:5991;urn:miriam:hgnc:6121;urn:miriam:hgnc:11916;urn:miriam:hgnc:5991;urn:miriam:refseq:NM_000575;urn:miriam:hgnc.symbol:IL1A;urn:miriam:hgnc.symbol:IL1A;urn:miriam:uniprot:P01583;urn:miriam:uniprot:P01583;urn:miriam:ensembl:ENSG00000115008;urn:miriam:ncbigene:3552;urn:miriam:ncbigene:3552;urn:miriam:ncbigene:7132;urn:miriam:ncbigene:7132;urn:miriam:refseq:NM_001065;urn:miriam:ensembl:ENSG00000067182;urn:miriam:uniprot:P19438;urn:miriam:uniprot:P19438;urn:miriam:hgnc.symbol:TNFRSF1A;urn:miriam:hgnc.symbol:TNFRSF1A;urn:miriam:hgnc:11916;urn:miriam:uniprot:O14896;urn:miriam:uniprot:O14896;urn:miriam:refseq:NM_006147;urn:miriam:ensembl:ENSG00000117595;urn:miriam:hgnc:6121;urn:miriam:ncbigene:3664;urn:miriam:ncbigene:3664;urn:miriam:hgnc.symbol:IRF6;urn:miriam:hgnc.symbol:IRF6"
      hgnc "HGNC_SYMBOL:IL1A;HGNC_SYMBOL:TNFRSF1A;HGNC_SYMBOL:IRF6"
      map_id "M116_12"
      name "LPS_slash_TNF_space__alpha__slash_IL_minus_1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa3"
      uniprot "UNIPROT:P01583;UNIPROT:P19438;UNIPROT:O14896"
    ]
    graphics [
      x 2182.5144998161113
      y 1881.3846992840236
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ncbigene:29108;urn:miriam:ncbigene:29108;urn:miriam:refseq:NM_013258;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:ensembl:ENSG00000103490;urn:miriam:pubmed:32172672;urn:miriam:hgnc:16608;urn:miriam:uniprot:Q9ULZ3;urn:miriam:uniprot:Q9ULZ3"
      hgnc "HGNC_SYMBOL:PYCARD"
      map_id "M116_55"
      name "PYCARD"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa131"
      uniprot "UNIPROT:Q9ULZ3"
    ]
    graphics [
      x 998.8536650413897
      y 613.9391752212337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 100
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_75"
      target_id "M116_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 3
    target 2
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_73"
      target_id "M116_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 2
    target 4
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_34"
      target_id "M116_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 4
    target 5
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_97"
      target_id "M116_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 6
    target 5
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_98"
      target_id "M116_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 7
    target 5
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "INHIBITION"
      source_id "M116_78"
      target_id "M116_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 106
    source 5
    target 8
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_35"
      target_id "M116_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 107
    source 8
    target 9
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_76"
      target_id "M116_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 108
    source 10
    target 9
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_52"
      target_id "M116_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 109
    source 9
    target 11
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_43"
      target_id "M116_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 110
    source 11
    target 12
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CATALYSIS"
      source_id "M116_51"
      target_id "M116_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 111
    source 11
    target 13
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CATALYSIS"
      source_id "M116_51"
      target_id "M116_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 112
    source 99
    target 12
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_55"
      target_id "M116_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 113
    source 16
    target 12
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_47"
      target_id "M116_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 114
    source 12
    target 27
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_19"
      target_id "M116_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 14
    target 13
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_14"
      target_id "M116_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 15
    target 13
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CATALYSIS"
      source_id "M116_13"
      target_id "M116_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 16
    target 13
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_47"
      target_id "M116_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 13
    target 17
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_18"
      target_id "M116_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 96
    target 15
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_45"
      target_id "M116_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 15
    target 18
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_13"
      target_id "M116_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 16
    target 33
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_47"
      target_id "M116_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 16
    target 34
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_47"
      target_id "M116_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 16
    target 35
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_47"
      target_id "M116_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 18
    target 17
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_23"
      target_id "M116_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 17
    target 19
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_15"
      target_id "M116_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 126
    source 17
    target 20
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M116_15"
      target_id "M116_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 127
    source 32
    target 18
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_91"
      target_id "M116_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 128
    source 19
    target 21
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_22"
      target_id "M116_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 21
    target 20
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_54"
      target_id "M116_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 130
    source 22
    target 20
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CATALYSIS"
      source_id "M116_69"
      target_id "M116_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 131
    source 23
    target 20
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CATALYSIS"
      source_id "M116_95"
      target_id "M116_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 132
    source 20
    target 24
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_32"
      target_id "M116_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 133
    source 25
    target 22
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_29"
      target_id "M116_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 134
    source 26
    target 25
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_90"
      target_id "M116_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 135
    source 27
    target 25
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_62"
      target_id "M116_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 136
    source 28
    target 25
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_48"
      target_id "M116_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 137
    source 29
    target 28
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_20"
      target_id "M116_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 138
    source 30
    target 29
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_57"
      target_id "M116_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 139
    source 31
    target 29
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_56"
      target_id "M116_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 94
    target 33
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_53"
      target_id "M116_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 33
    target 95
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_46"
      target_id "M116_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 38
    target 34
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_99"
      target_id "M116_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 34
    target 39
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_36"
      target_id "M116_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 144
    source 36
    target 35
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_63"
      target_id "M116_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 145
    source 35
    target 37
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_26"
      target_id "M116_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 146
    source 39
    target 40
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "INHIBITION"
      source_id "M116_70"
      target_id "M116_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 147
    source 41
    target 40
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_67"
      target_id "M116_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 148
    source 40
    target 42
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_28"
      target_id "M116_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 149
    source 42
    target 43
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_66"
      target_id "M116_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 150
    source 44
    target 43
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_65"
      target_id "M116_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 151
    source 45
    target 43
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_64"
      target_id "M116_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 152
    source 43
    target 46
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_27"
      target_id "M116_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 153
    source 46
    target 47
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CATALYSIS"
      source_id "M116_16"
      target_id "M116_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 154
    source 48
    target 47
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_58"
      target_id "M116_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 155
    source 47
    target 49
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_30"
      target_id "M116_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 156
    source 49
    target 50
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_50"
      target_id "M116_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 157
    source 51
    target 50
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_77"
      target_id "M116_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 158
    source 52
    target 50
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "INHIBITION"
      source_id "M116_79"
      target_id "M116_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 159
    source 53
    target 50
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_84"
      target_id "M116_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 160
    source 50
    target 54
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_39"
      target_id "M116_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 161
    source 88
    target 51
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_38"
      target_id "M116_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 162
    source 54
    target 55
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_72"
      target_id "M116_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 163
    source 56
    target 55
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_7"
      target_id "M116_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 164
    source 57
    target 55
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "INHIBITION"
      source_id "M116_2"
      target_id "M116_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 165
    source 55
    target 58
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_40"
      target_id "M116_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 166
    source 58
    target 59
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_1"
      target_id "M116_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 167
    source 58
    target 60
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_1"
      target_id "M116_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 168
    source 83
    target 59
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_92"
      target_id "M116_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 169
    source 59
    target 84
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_42"
      target_id "M116_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 170
    source 61
    target 60
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_9"
      target_id "M116_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 171
    source 62
    target 60
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "INHIBITION"
      source_id "M116_80"
      target_id "M116_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 172
    source 63
    target 60
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "INHIBITION"
      source_id "M116_83"
      target_id "M116_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 173
    source 64
    target 60
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_5"
      target_id "M116_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 174
    source 65
    target 60
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "INHIBITION"
      source_id "M116_4"
      target_id "M116_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 175
    source 60
    target 66
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_41"
      target_id "M116_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 176
    source 66
    target 67
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CATALYSIS"
      source_id "M116_3"
      target_id "M116_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 177
    source 68
    target 67
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_81"
      target_id "M116_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 178
    source 69
    target 67
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CATALYSIS"
      source_id "M116_86"
      target_id "M116_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 179
    source 67
    target 70
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_31"
      target_id "M116_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 180
    source 80
    target 69
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_21"
      target_id "M116_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 181
    source 70
    target 71
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_82"
      target_id "M116_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 182
    source 72
    target 71
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_89"
      target_id "M116_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 73
    target 71
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "INHIBITION"
      source_id "M116_60"
      target_id "M116_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 71
    target 74
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_44"
      target_id "M116_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 75
    target 73
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_24"
      target_id "M116_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 76
    target 75
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_88"
      target_id "M116_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 77
    target 75
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_61"
      target_id "M116_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 78
    target 77
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_25"
      target_id "M116_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 79
    target 78
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_87"
      target_id "M116_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 81
    target 80
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_59"
      target_id "M116_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 82
    target 80
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "INHIBITION"
      source_id "M116_85"
      target_id "M116_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 84
    target 85
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M116_93"
      target_id "M116_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 86
    target 85
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_94"
      target_id "M116_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 85
    target 87
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_33"
      target_id "M116_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 89
    target 88
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_49"
      target_id "M116_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 90
    target 88
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_6"
      target_id "M116_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 91
    target 90
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_37"
      target_id "M116_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 92
    target 91
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_17"
      target_id "M116_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 93
    target 91
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_74"
      target_id "M116_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 97
    target 96
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_11"
      target_id "M116_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 98
    target 96
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CATALYSIS"
      source_id "M116_12"
      target_id "M116_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
