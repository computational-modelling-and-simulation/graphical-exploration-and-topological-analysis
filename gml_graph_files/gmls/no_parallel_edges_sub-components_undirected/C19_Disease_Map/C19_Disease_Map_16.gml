# generated with VANTED V2.8.2 at Fri Mar 04 10:04:38 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbigene:23586;urn:miriam:ncbigene:23586;urn:miriam:refseq:NM_014314;urn:miriam:ensembl:ENSG00000107201;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:uniprot:O95786;urn:miriam:hgnc:19102;urn:miriam:hgnc.symbol:DDX58;urn:miriam:hgnc.symbol:DDX58"
      hgnc "HGNC_SYMBOL:DDX58"
      map_id "M111_102"
      name "DDX58"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa23"
      uniprot "UNIPROT:O95786"
    ]
    graphics [
      x 622.5144998161113
      y 1932.1662202339423
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:25581309;PUBMED:28148787"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_79"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re61"
      uniprot "NA"
    ]
    graphics [
      x 1221.6104592494069
      y 1719.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:uniprot:Q8IUD6;urn:miriam:uniprot:Q8IUD6;urn:miriam:hgnc:21158;urn:miriam:refseq:NM_032322;urn:miriam:hgnc.symbol:RNF135;urn:miriam:hgnc.symbol:RNF135;urn:miriam:ncbigene:84282;urn:miriam:ncbigene:84282;urn:miriam:ensembl:ENSG00000181481"
      hgnc "HGNC_SYMBOL:RNF135"
      map_id "M111_90"
      name "RNF135"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa123"
      uniprot "UNIPROT:Q8IUD6"
    ]
    graphics [
      x 1631.4760702265748
      y 1062.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbigene:7706;urn:miriam:ensembl:ENSG00000121060;urn:miriam:ncbigene:7706;urn:miriam:hgnc.symbol:TRIM25;urn:miriam:hgnc.symbol:TRIM25;urn:miriam:ec-code:2.3.2.27;urn:miriam:hgnc:12932;urn:miriam:uniprot:Q14258;urn:miriam:uniprot:Q14258;urn:miriam:refseq:NM_005082"
      hgnc "HGNC_SYMBOL:TRIM25"
      map_id "M111_89"
      name "TRIM25"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa122"
      uniprot "UNIPROT:Q14258"
    ]
    graphics [
      x 1582.5144998161113
      y 1902.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbiprotein:1798174255"
      hgnc "NA"
      map_id "M111_114"
      name "N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa352"
      uniprot "NA"
    ]
    graphics [
      x 1520.0074341427699
      y 2442.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbigene:23586;urn:miriam:ncbigene:23586;urn:miriam:refseq:NM_014314;urn:miriam:ensembl:ENSG00000107201;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:uniprot:O95786;urn:miriam:hgnc:19102;urn:miriam:hgnc.symbol:DDX58;urn:miriam:hgnc.symbol:DDX58"
      hgnc "HGNC_SYMBOL:DDX58"
      map_id "M111_22"
      name "DDX58:dsRNA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa92"
      uniprot "UNIPROT:O95786"
    ]
    graphics [
      x 697.9673850663706
      y 1702.4666947456517
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbigene:23586;urn:miriam:ncbigene:23586;urn:miriam:refseq:NM_014314;urn:miriam:ensembl:ENSG00000107201;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:uniprot:O95786;urn:miriam:hgnc:19102;urn:miriam:hgnc.symbol:DDX58;urn:miriam:hgnc.symbol:DDX58"
      hgnc "HGNC_SYMBOL:DDX58"
      map_id "M111_91"
      name "DDX58"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa124"
      uniprot "UNIPROT:O95786"
    ]
    graphics [
      x 907.9673850663706
      y 1748.944364236203
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "PUBMED:25581309"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_80"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re65"
      uniprot "NA"
    ]
    graphics [
      x 761.4760702265747
      y 1427.1654913580128
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:uniprot:Q7Z434;urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746"
      hgnc "HGNC_SYMBOL:MAVS"
      map_id "M111_92"
      name "MAVS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa127"
      uniprot "UNIPROT:Q7Z434"
    ]
    graphics [
      x 1820.0074341427699
      y 2364.6594484469133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc:18873;urn:miriam:ncbigene:64135;urn:miriam:ncbigene:64135;urn:miriam:refseq:NM_022168;urn:miriam:ensembl:ENSG00000115267;urn:miriam:uniprot:Q9BYX4;urn:miriam:uniprot:Q9BYX4;urn:miriam:ec-code:3.6.4.13;urn:miriam:hgnc.symbol:IFIH1;urn:miriam:hgnc.symbol:IFIH1"
      hgnc "HGNC_SYMBOL:IFIH1"
      map_id "M111_23"
      name "IFIH1:dsRNA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa93"
      uniprot "UNIPROT:Q9BYX4"
    ]
    graphics [
      x 1168.965509607095
      y 636.7609618579327
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:uniprot:Q7Z434;urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746"
      hgnc "HGNC_SYMBOL:MAVS"
      map_id "M111_93"
      name "MAVS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa128"
      uniprot "UNIPROT:Q7Z434"
    ]
    graphics [
      x 1148.8536650413898
      y 923.0689340107122
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_53"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re263"
      uniprot "NA"
    ]
    graphics [
      x 2108.85366504139
      y 750.1099789381244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "PUBMED:25581309"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_51"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re261"
      uniprot "NA"
    ]
    graphics [
      x 2681.476070226575
      y 1287.3934608989682
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:hgnc.symbol:TRAF2;urn:miriam:hgnc.symbol:TRAF2;urn:miriam:ncbigene:7186;urn:miriam:ncbigene:7186;urn:miriam:ensembl:ENSG00000127191;urn:miriam:refseq:NM_021138;urn:miriam:uniprot:Q12933;urn:miriam:uniprot:Q12933;urn:miriam:hgnc:12032"
      hgnc "HGNC_SYMBOL:TRAF2"
      map_id "M111_95"
      name "TRAF2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa130"
      uniprot "UNIPROT:Q12933"
    ]
    graphics [
      x 2171.476070226575
      y 1488.7342851305455
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc.symbol:TRAF5;urn:miriam:uniprot:O00463;urn:miriam:uniprot:O00463;urn:miriam:hgnc.symbol:TRAF5;urn:miriam:ensembl:ENSG00000082512;urn:miriam:ncbigene:7188;urn:miriam:ncbigene:7188;urn:miriam:refseq:NM_004619;urn:miriam:hgnc:12035"
      hgnc "HGNC_SYMBOL:TRAF5"
      map_id "M111_96"
      name "TRAF5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa131"
      uniprot "UNIPROT:O00463"
    ]
    graphics [
      x 1871.4760702265748
      y 1314.7436141671978
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7189;urn:miriam:ncbigene:7189;urn:miriam:ensembl:ENSG00000175104;urn:miriam:uniprot:Q9Y4K3;urn:miriam:uniprot:Q9Y4K3;urn:miriam:hgnc:12036;urn:miriam:refseq:NM_145803;urn:miriam:hgnc.symbol:TRAF6;urn:miriam:hgnc.symbol:TRAF6"
      hgnc "HGNC_SYMBOL:TRAF6"
      map_id "M111_94"
      name "TRAF6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa129"
      uniprot "UNIPROT:Q9Y4K3"
    ]
    graphics [
      x 2270.1736034760024
      y 514.8926092467668
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:uniprot:Q7Z434;urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746;urn:miriam:ec-code:2.3.2.27;urn:miriam:hgnc.symbol:TRAF2;urn:miriam:hgnc.symbol:TRAF2;urn:miriam:ncbigene:7186;urn:miriam:ncbigene:7186;urn:miriam:ensembl:ENSG00000127191;urn:miriam:refseq:NM_021138;urn:miriam:uniprot:Q12933;urn:miriam:uniprot:Q12933;urn:miriam:hgnc:12032;urn:miriam:hgnc.symbol:TRAF5;urn:miriam:uniprot:O00463;urn:miriam:uniprot:O00463;urn:miriam:hgnc.symbol:TRAF5;urn:miriam:ensembl:ENSG00000082512;urn:miriam:ncbigene:7188;urn:miriam:ncbigene:7188;urn:miriam:refseq:NM_004619;urn:miriam:hgnc:12035;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7189;urn:miriam:ncbigene:7189;urn:miriam:ensembl:ENSG00000175104;urn:miriam:uniprot:Q9Y4K3;urn:miriam:uniprot:Q9Y4K3;urn:miriam:hgnc:12036;urn:miriam:refseq:NM_145803;urn:miriam:hgnc.symbol:TRAF6;urn:miriam:hgnc.symbol:TRAF6"
      hgnc "HGNC_SYMBOL:MAVS;HGNC_SYMBOL:TRAF2;HGNC_SYMBOL:TRAF5;HGNC_SYMBOL:TRAF6"
      map_id "M111_6"
      name "MAVS:TRAF"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa32"
      uniprot "UNIPROT:Q7Z434;UNIPROT:Q12933;UNIPROT:O00463;UNIPROT:Q9Y4K3"
    ]
    graphics [
      x 2482.5144998161113
      y 1843.3552697885598
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_35"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re176"
      uniprot "NA"
    ]
    graphics [
      x 2032.5144998161113
      y 1965.3498758527048
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7189;urn:miriam:ncbigene:7189;urn:miriam:ensembl:ENSG00000175104;urn:miriam:uniprot:Q9Y4K3;urn:miriam:uniprot:Q9Y4K3;urn:miriam:hgnc:12036;urn:miriam:refseq:NM_145803;urn:miriam:hgnc.symbol:TRAF6;urn:miriam:hgnc.symbol:TRAF6"
      hgnc "HGNC_SYMBOL:TRAF6"
      map_id "M111_106"
      name "TRAF6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa265"
      uniprot "UNIPROT:Q9Y4K3"
    ]
    graphics [
      x 892.5144998161113
      y 2042.9284087115225
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ensembl:ENSG00000177889;urn:miriam:ec-code:2.3.2.23;urn:miriam:hgnc.symbol:UBE2N;urn:miriam:hgnc.symbol:UBE2N;urn:miriam:ncbigene:7334;urn:miriam:ncbigene:7334;urn:miriam:refseq:NM_003348;urn:miriam:hgnc:12492;urn:miriam:uniprot:P61088;urn:miriam:uniprot:P61088"
      hgnc "HGNC_SYMBOL:UBE2N"
      map_id "M111_108"
      name "UBE2N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa331"
      uniprot "UNIPROT:P61088"
    ]
    graphics [
      x 1690.4146427238684
      y 2188.4941137623227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7189;urn:miriam:ncbigene:7189;urn:miriam:ensembl:ENSG00000175104;urn:miriam:uniprot:Q9Y4K3;urn:miriam:uniprot:Q9Y4K3;urn:miriam:hgnc:12036;urn:miriam:refseq:NM_145803;urn:miriam:hgnc.symbol:TRAF6;urn:miriam:hgnc.symbol:TRAF6;urn:miriam:uniprot:Q9NWZ3;urn:miriam:uniprot:Q9NWZ3;urn:miriam:refseq:NM_001114182;urn:miriam:ec-code:2.7.11.1;urn:miriam:ncbigene:51135;urn:miriam:ncbigene:51135;urn:miriam:hgnc:17967;urn:miriam:hgnc.symbol:IRAK4;urn:miriam:hgnc.symbol:IRAK4;urn:miriam:hgnc:6112;urn:miriam:uniprot:P51617;urn:miriam:uniprot:P51617;urn:miriam:ec-code:2.7.11.1;urn:miriam:ensembl:ENSG00000184216;urn:miriam:refseq:NM_001025242;urn:miriam:hgnc.symbol:IRAK1;urn:miriam:ncbigene:3654;urn:miriam:hgnc.symbol:IRAK1;urn:miriam:ncbigene:3654;urn:miriam:ncbigene:4615;urn:miriam:ensembl:ENSG00000172936;urn:miriam:ncbigene:4615;urn:miriam:pubmed:19366914;urn:miriam:uniprot:Q99836;urn:miriam:uniprot:Q99836;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc:7562;urn:miriam:refseq:NM_002468"
      hgnc "HGNC_SYMBOL:TRAF6;HGNC_SYMBOL:IRAK4;HGNC_SYMBOL:IRAK1;HGNC_SYMBOL:MYD88"
      map_id "M111_7"
      name "MYD88:IRAK:TRAF"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa56"
      uniprot "UNIPROT:Q9Y4K3;UNIPROT:Q9NWZ3;UNIPROT:P51617;UNIPROT:Q99836"
    ]
    graphics [
      x 1269.0311139321143
      y 2082.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7189;urn:miriam:ncbigene:7189;urn:miriam:ensembl:ENSG00000175104;urn:miriam:uniprot:Q9Y4K3;urn:miriam:uniprot:Q9Y4K3;urn:miriam:hgnc:12036;urn:miriam:refseq:NM_145803;urn:miriam:hgnc.symbol:TRAF6;urn:miriam:hgnc.symbol:TRAF6"
      hgnc "HGNC_SYMBOL:TRAF6"
      map_id "M111_110"
      name "TRAF6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa337"
      uniprot "UNIPROT:Q9Y4K3"
    ]
    graphics [
      x 1091.4760702265748
      y 1337.5375698961307
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "PUBMED:20303872;PUBMED:19366914;PUBMED:694009;PUBMED:17705188"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_66"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re279"
      uniprot "NA"
    ]
    graphics [
      x 1172.4472510476567
      y 2109.0166269831952
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "PUBMED:27164085"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re265"
      uniprot "NA"
    ]
    graphics [
      x 577.9673850663706
      y 1609.7908629539697
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_50"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re260"
      uniprot "NA"
    ]
    graphics [
      x 1988.7695801560849
      y 473.5808632544606
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:uniprot:O43318;urn:miriam:uniprot:O43318;urn:miriam:ensembl:ENSG00000135341;urn:miriam:refseq:NM_145331;urn:miriam:hgnc:6859;urn:miriam:ncbigene:6885;urn:miriam:ncbigene:6885;urn:miriam:hgnc.symbol:MAP3K7;urn:miriam:hgnc.symbol:MAP3K7;urn:miriam:ec-code:2.7.11.25"
      hgnc "HGNC_SYMBOL:MAP3K7"
      map_id "M111_152"
      name "MAP3K7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa435"
      uniprot "UNIPROT:O43318"
    ]
    graphics [
      x 2231.476070226575
      y 1481.8428520596005
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_152"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc.symbol:TICAM1;urn:miriam:uniprot:Q8IUC6;urn:miriam:uniprot:Q8IUC6;urn:miriam:hgnc.symbol:TICAM1;urn:miriam:ensembl:ENSG00000127666;urn:miriam:hgnc:18348;urn:miriam:refseq:NM_014261;urn:miriam:ncbigene:148022;urn:miriam:ncbigene:148022;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7189;urn:miriam:ncbigene:7189;urn:miriam:ensembl:ENSG00000175104;urn:miriam:uniprot:Q9Y4K3;urn:miriam:uniprot:Q9Y4K3;urn:miriam:hgnc:12036;urn:miriam:refseq:NM_145803;urn:miriam:hgnc.symbol:TRAF6;urn:miriam:hgnc.symbol:TRAF6;urn:miriam:hgnc:10019;urn:miriam:hgnc.symbol:RIPK1;urn:miriam:ensembl:ENSG00000137275;urn:miriam:hgnc.symbol:RIPK1;urn:miriam:ec-code:2.7.11.1;urn:miriam:uniprot:Q13546;urn:miriam:uniprot:Q13546;urn:miriam:refseq:NM_003804;urn:miriam:ncbigene:8737;urn:miriam:ncbigene:8737"
      hgnc "HGNC_SYMBOL:TICAM1;HGNC_SYMBOL:TRAF6;HGNC_SYMBOL:RIPK1"
      map_id "M111_19"
      name "TICAM1:TRAF3:TRAF6"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa9"
      uniprot "UNIPROT:Q8IUC6;UNIPROT:Q9Y4K3;UNIPROT:Q13546"
    ]
    graphics [
      x 1928.8536650413898
      y 744.4029625690932
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:uniprot:O43318;urn:miriam:uniprot:O43318;urn:miriam:ensembl:ENSG00000135341;urn:miriam:refseq:NM_145331;urn:miriam:hgnc:6859;urn:miriam:ncbigene:6885;urn:miriam:ncbigene:6885;urn:miriam:hgnc.symbol:MAP3K7;urn:miriam:hgnc.symbol:MAP3K7;urn:miriam:ec-code:2.7.11.25"
      hgnc "HGNC_SYMBOL:MAP3K7"
      map_id "M111_124"
      name "MAP3K7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa367"
      uniprot "UNIPROT:O43318"
    ]
    graphics [
      x 1868.8536650413898
      y 692.4591520117286
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_124"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_59"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re271"
      uniprot "NA"
    ]
    graphics [
      x 1898.8536650413898
      y 930.0554331686938
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_58"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re270"
      uniprot "NA"
    ]
    graphics [
      x 2002.5144998161113
      y 2062.876338344345
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_61"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re273"
      uniprot "NA"
    ]
    graphics [
      x 2212.5144998161113
      y 1857.2403049490633
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_60"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re272"
      uniprot "NA"
    ]
    graphics [
      x 1316.090149807047
      y 462.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ec-code:2.7.12.2;urn:miriam:hgnc:6844;urn:miriam:ensembl:ENSG00000065559;urn:miriam:refseq:NM_001281435;urn:miriam:uniprot:P45985;urn:miriam:uniprot:P45985;urn:miriam:hgnc.symbol:MAP2K4;urn:miriam:hgnc.symbol:MAP2K4;urn:miriam:ncbigene:6416;urn:miriam:ncbigene:6416"
      hgnc "HGNC_SYMBOL:MAP2K4"
      map_id "M111_128"
      name "MAP2K4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa371"
      uniprot "UNIPROT:P45985"
    ]
    graphics [
      x 788.8536650413897
      y 895.7583197686541
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ec-code:2.7.12.2;urn:miriam:hgnc:6844;urn:miriam:ensembl:ENSG00000065559;urn:miriam:refseq:NM_001281435;urn:miriam:uniprot:P45985;urn:miriam:uniprot:P45985;urn:miriam:hgnc.symbol:MAP2K4;urn:miriam:hgnc.symbol:MAP2K4;urn:miriam:ncbigene:6416;urn:miriam:ncbigene:6416"
      hgnc "HGNC_SYMBOL:MAP2K4"
      map_id "M111_159"
      name "MAP2K4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa452"
      uniprot "UNIPROT:P45985"
    ]
    graphics [
      x 1671.6104592494069
      y 1679.0814698311804
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_159"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_56"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re268"
      uniprot "NA"
    ]
    graphics [
      x 1229.5317509839817
      y 2532.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ec-code:2.7.11.24;urn:miriam:hgnc.symbol:MAPK9;urn:miriam:ensembl:ENSG00000050748;urn:miriam:uniprot:P45984;urn:miriam:uniprot:P45984;urn:miriam:hgnc.symbol:MAPK9;urn:miriam:hgnc:6886;urn:miriam:ncbigene:5601;urn:miriam:ncbigene:5601;urn:miriam:refseq:NM_001135044;urn:miriam:ec-code:2.7.11.24;urn:miriam:refseq:NM_001278547;urn:miriam:ensembl:ENSG00000107643;urn:miriam:hgnc.symbol:MAPK8;urn:miriam:uniprot:P45983;urn:miriam:uniprot:P45983;urn:miriam:hgnc.symbol:MAPK8;urn:miriam:hgnc:6881;urn:miriam:ncbigene:5599;urn:miriam:ncbigene:5599"
      hgnc "HGNC_SYMBOL:MAPK9;HGNC_SYMBOL:MAPK8"
      map_id "M111_9"
      name "JNK"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa77"
      uniprot "UNIPROT:P45984;UNIPROT:P45983"
    ]
    graphics [
      x 1012.5144998161113
      y 2031.7460409926869
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ec-code:2.7.12.2;urn:miriam:ncbigene:5609;urn:miriam:ncbigene:5609;urn:miriam:uniprot:O14733;urn:miriam:uniprot:O14733;urn:miriam:ensembl:ENSG00000076984;urn:miriam:hgnc:6847;urn:miriam:refseq:NM_001297555;urn:miriam:hgnc.symbol:MAP2K7;urn:miriam:hgnc.symbol:MAP2K7"
      hgnc "HGNC_SYMBOL:MAP2K7"
      map_id "M111_160"
      name "MAP2K7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa453"
      uniprot "UNIPROT:O14733"
    ]
    graphics [
      x 2061.5853360770247
      y 2399.901000727861
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_160"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ec-code:2.7.11.24;urn:miriam:refseq:NM_001278547;urn:miriam:ensembl:ENSG00000107643;urn:miriam:hgnc.symbol:MAPK8;urn:miriam:uniprot:P45983;urn:miriam:uniprot:P45983;urn:miriam:hgnc.symbol:MAPK8;urn:miriam:hgnc:6881;urn:miriam:ncbigene:5599;urn:miriam:ncbigene:5599;urn:miriam:ec-code:2.7.11.24;urn:miriam:hgnc.symbol:MAPK9;urn:miriam:ensembl:ENSG00000050748;urn:miriam:uniprot:P45984;urn:miriam:uniprot:P45984;urn:miriam:hgnc.symbol:MAPK9;urn:miriam:hgnc:6886;urn:miriam:ncbigene:5601;urn:miriam:ncbigene:5601;urn:miriam:refseq:NM_001135044"
      hgnc "HGNC_SYMBOL:MAPK8;HGNC_SYMBOL:MAPK9"
      map_id "M111_24"
      name "JNK"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa94"
      uniprot "UNIPROT:P45983;UNIPROT:P45984"
    ]
    graphics [
      x 1300.4146427238684
      y 2292.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_55"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re267"
      uniprot "NA"
    ]
    graphics [
      x 952.5144998161113
      y 2039.5885043095277
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc:3796;urn:miriam:ensembl:ENSG00000170345;urn:miriam:refseq:NM_005252;urn:miriam:uniprot:P01100;urn:miriam:uniprot:P01100;urn:miriam:ncbigene:2353;urn:miriam:ncbigene:2353;urn:miriam:hgnc.symbol:FOS;urn:miriam:hgnc.symbol:FOS;urn:miriam:ncbigene:3725;urn:miriam:ncbigene:3725;urn:miriam:hgnc:6204;urn:miriam:uniprot:P05412;urn:miriam:uniprot:P05412;urn:miriam:hgnc.symbol:JUN;urn:miriam:hgnc.symbol:JUN;urn:miriam:ensembl:ENSG00000177606;urn:miriam:refseq:NM_002228"
      hgnc "HGNC_SYMBOL:FOS;HGNC_SYMBOL:JUN"
      map_id "M111_25"
      name "AP1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa95"
      uniprot "UNIPROT:P01100;UNIPROT:P05412"
    ]
    graphics [
      x 251.47607022657485
      y 1524.6913859985789
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ec-code:2.7.11.24;urn:miriam:ensembl:ENSG00000112062;urn:miriam:hgnc:6876;urn:miriam:uniprot:Q16539;urn:miriam:uniprot:Q16539;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:refseq:NM_001315"
      hgnc "HGNC_SYMBOL:MAPK14"
      map_id "M111_125"
      name "MAPK14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa368"
      uniprot "UNIPROT:Q16539"
    ]
    graphics [
      x 2287.9673850663708
      y 1607.1332646769645
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_125"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbigene:3725;urn:miriam:ncbigene:3725;urn:miriam:hgnc:6204;urn:miriam:uniprot:P05412;urn:miriam:uniprot:P05412;urn:miriam:hgnc.symbol:JUN;urn:miriam:hgnc.symbol:JUN;urn:miriam:ensembl:ENSG00000177606;urn:miriam:refseq:NM_002228;urn:miriam:hgnc:3796;urn:miriam:ensembl:ENSG00000170345;urn:miriam:refseq:NM_005252;urn:miriam:uniprot:P01100;urn:miriam:uniprot:P01100;urn:miriam:ncbigene:2353;urn:miriam:ncbigene:2353;urn:miriam:hgnc.symbol:FOS;urn:miriam:hgnc.symbol:FOS"
      hgnc "HGNC_SYMBOL:JUN;HGNC_SYMBOL:FOS"
      map_id "M111_10"
      name "AP1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa79"
      uniprot "UNIPROT:P05412;UNIPROT:P01100"
    ]
    graphics [
      x 1517.5115202333882
      y 1992.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_68"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re281"
      uniprot "NA"
    ]
    graphics [
      x 1610.0074341427699
      y 2292.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbigene:3725;urn:miriam:ncbigene:3725;urn:miriam:hgnc:6204;urn:miriam:uniprot:P05412;urn:miriam:uniprot:P05412;urn:miriam:hgnc.symbol:JUN;urn:miriam:hgnc.symbol:JUN;urn:miriam:ensembl:ENSG00000177606;urn:miriam:refseq:NM_002228;urn:miriam:hgnc:3796;urn:miriam:ensembl:ENSG00000170345;urn:miriam:refseq:NM_005252;urn:miriam:uniprot:P01100;urn:miriam:uniprot:P01100;urn:miriam:ncbigene:2353;urn:miriam:ncbigene:2353;urn:miriam:hgnc.symbol:FOS;urn:miriam:hgnc.symbol:FOS"
      hgnc "HGNC_SYMBOL:JUN;HGNC_SYMBOL:FOS"
      map_id "M111_4"
      name "AP1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa104"
      uniprot "UNIPROT:P05412;UNIPROT:P01100"
    ]
    graphics [
      x 2212.5144998161113
      y 1899.627644174377
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_69"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re282"
      uniprot "NA"
    ]
    graphics [
      x 1882.5144998161113
      y 1840.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_166"
      name "transcription_space_of_space_proinflammatory_space_proteins"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa480"
      uniprot "NA"
    ]
    graphics [
      x 1271.4760702265748
      y 1302.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_166"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_72"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re285"
      uniprot "NA"
    ]
    graphics [
      x 1136.6812540881106
      y 319.2068069053712
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_70"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re283"
      uniprot "NA"
    ]
    graphics [
      x 2048.85366504139
      y 882.9547352166127
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_71"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re284"
      uniprot "NA"
    ]
    graphics [
      x 831.6104592494069
      y 1827.6236763137715
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_73"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re286"
      uniprot "NA"
    ]
    graphics [
      x 1642.5144998161113
      y 1992.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc:6118;urn:miriam:uniprot:Q14653;urn:miriam:uniprot:Q14653;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661;urn:miriam:ncbigene:3661"
      hgnc "HGNC_SYMBOL:IRF3"
      map_id "M111_104"
      name "IRF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa256"
      uniprot "UNIPROT:Q14653"
    ]
    graphics [
      x 1991.4760702265748
      y 1457.606843746084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "PUBMED:20303872"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_32"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re134"
      uniprot "NA"
    ]
    graphics [
      x 1613.888298663446
      y 792.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc:6118;urn:miriam:uniprot:Q14653;urn:miriam:uniprot:Q14653;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661;urn:miriam:ncbigene:3661"
      hgnc "HGNC_SYMBOL:IRF3"
      map_id "M111_87"
      name "IRF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa119"
      uniprot "UNIPROT:Q14653"
    ]
    graphics [
      x 1631.4760702265748
      y 1475.8463691234692
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:17761676"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_30"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re132"
      uniprot "NA"
    ]
    graphics [
      x 1551.6104592494069
      y 1722.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc:6118;urn:miriam:uniprot:Q14653;urn:miriam:uniprot:Q14653;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661;urn:miriam:ncbigene:3661"
      hgnc "HGNC_SYMBOL:IRF3"
      map_id "M111_112"
      name "IRF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa348"
      uniprot "UNIPROT:Q14653"
    ]
    graphics [
      x 1690.4146427238684
      y 2158.4941137623227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:uniprot:Q9UHD2;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110"
      hgnc "HGNC_SYMBOL:TBK1"
      map_id "M111_85"
      name "TBK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa117"
      uniprot "UNIPROT:Q9UHD2"
    ]
    graphics [
      x 2681.476070226575
      y 1504.3347986776794
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbiprotein:1802476807;urn:miriam:uniprot:Nsp3"
      hgnc "NA"
      map_id "M111_119"
      name "Nsp3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa357"
      uniprot "UNIPROT:Nsp3"
    ]
    graphics [
      x 1697.2023363703734
      y 999.5710630443609
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:uniprot:P59633;urn:miriam:ncbigene:1489670"
      hgnc "NA"
      map_id "M111_118"
      name "Orf3b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa356"
      uniprot "UNIPROT:P59633"
    ]
    graphics [
      x 1450.6962061871427
      y 1302.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbiprotein:1796318604"
      hgnc "NA"
      map_id "M111_117"
      name "Orf8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa355"
      uniprot "NA"
    ]
    graphics [
      x 1744.993508966593
      y 2346.313882011814
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "PUBMED:20303872"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_44"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re253"
      uniprot "NA"
    ]
    graphics [
      x 2591.476070226575
      y 1473.6070432560246
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:uniprot:Q9UHD2;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110"
      hgnc "HGNC_SYMBOL:TBK1"
      map_id "M111_146"
      name "TBK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa426"
      uniprot "UNIPROT:Q9UHD2"
    ]
    graphics [
      x 1971.6104592494069
      y 1817.606843746084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_146"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033;urn:miriam:uniprot:Q14164;urn:miriam:uniprot:Q14164;urn:miriam:ncbigene:9641;urn:miriam:ncbigene:9641;urn:miriam:hgnc:14552;urn:miriam:ec-code:2.7.11.10;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:refseq:NM_001193321;urn:miriam:ensembl:ENSG00000263528;urn:miriam:ncbigene:10010;urn:miriam:ncbigene:10010;urn:miriam:ensembl:ENSG00000136560;urn:miriam:refseq:NM_133484;urn:miriam:uniprot:Q92844;urn:miriam:uniprot:Q92844;urn:miriam:hgnc:11562;urn:miriam:hgnc.symbol:TANK;urn:miriam:hgnc.symbol:TANK"
      hgnc "HGNC_SYMBOL:TRAF3;HGNC_SYMBOL:IKBKE;HGNC_SYMBOL:TANK"
      map_id "M111_13"
      name "TANK:TRAF3:IKBKE"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa83"
      uniprot "UNIPROT:Q13114;UNIPROT:Q14164;UNIPROT:Q92844"
    ]
    graphics [
      x 1791.6104592494069
      y 1693.8597507391748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914;PUBMED:19380580"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_43"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re252"
      uniprot "NA"
    ]
    graphics [
      x 2032.5144998161113
      y 1880.4619322870956
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:20303872"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re254"
      uniprot "NA"
    ]
    graphics [
      x 1624.6943393561805
      y 1122.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:uniprot:Q14164;urn:miriam:uniprot:Q14164;urn:miriam:ncbigene:9641;urn:miriam:ncbigene:9641;urn:miriam:hgnc:14552;urn:miriam:ec-code:2.7.11.10;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:refseq:NM_001193321;urn:miriam:ensembl:ENSG00000263528"
      hgnc "HGNC_SYMBOL:IKBKE"
      map_id "M111_147"
      name "IKBKE"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa427"
      uniprot "UNIPROT:Q14164"
    ]
    graphics [
      x 911.4760702265747
      y 1171.8262641726626
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_147"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbiprotein:1802476807;urn:miriam:uniprot:Nsp3"
      hgnc "NA"
      map_id "M111_113"
      name "Nsp3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa349"
      uniprot "UNIPROT:Nsp3"
    ]
    graphics [
      x 1031.4760702265748
      y 1145.0238894408317
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:uniprot:Q14164;urn:miriam:uniprot:Q14164;urn:miriam:ncbigene:9641;urn:miriam:ncbigene:9641;urn:miriam:hgnc:14552;urn:miriam:ec-code:2.7.11.10;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:refseq:NM_001193321;urn:miriam:ensembl:ENSG00000263528"
      hgnc "HGNC_SYMBOL:IKBKE"
      map_id "M111_86"
      name "IKBKE"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa118"
      uniprot "UNIPROT:Q14164"
    ]
    graphics [
      x 1481.4760702265748
      y 1272.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      annotation "PUBMED:20303872"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re133"
      uniprot "NA"
    ]
    graphics [
      x 2182.5144998161113
      y 1997.674659090071
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:uniprot:Q92985;urn:miriam:uniprot:Q92985;urn:miriam:ensembl:ENSG00000185507;urn:miriam:refseq:NM_001572;urn:miriam:hgnc.symbol:IRF7;urn:miriam:hgnc.symbol:IRF7;urn:miriam:hgnc:6122;urn:miriam:ncbigene:3665;urn:miriam:ncbigene:3665"
      hgnc "HGNC_SYMBOL:IRF7"
      map_id "M111_111"
      name "IRF7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa347"
      uniprot "UNIPROT:Q92985"
    ]
    graphics [
      x 1852.5144998161113
      y 2059.9076082928045
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:uniprot:Q92985;urn:miriam:uniprot:Q92985;urn:miriam:ensembl:ENSG00000185507;urn:miriam:refseq:NM_001572;urn:miriam:hgnc.symbol:IRF7;urn:miriam:hgnc.symbol:IRF7;urn:miriam:hgnc:6122;urn:miriam:ncbigene:3665;urn:miriam:ncbigene:3665"
      hgnc "HGNC_SYMBOL:IRF7"
      map_id "M111_88"
      name "IRF7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa120"
      uniprot "UNIPROT:Q92985"
    ]
    graphics [
      x 2512.5144998161113
      y 1990.3003697824354
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      annotation "PUBMED:20303872"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_33"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re135"
      uniprot "NA"
    ]
    graphics [
      x 2468.85366504139
      y 973.323503322771
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:uniprot:Q92985;urn:miriam:uniprot:Q92985;urn:miriam:ensembl:ENSG00000185507;urn:miriam:refseq:NM_001572;urn:miriam:hgnc.symbol:IRF7;urn:miriam:hgnc.symbol:IRF7;urn:miriam:hgnc:6122;urn:miriam:ncbigene:3665;urn:miriam:ncbigene:3665"
      hgnc "HGNC_SYMBOL:IRF7"
      map_id "M111_103"
      name "IRF7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa255"
      uniprot "UNIPROT:Q92985"
    ]
    graphics [
      x 1636.8405824608653
      y 162.06502830998443
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbigene:10010;urn:miriam:ncbigene:10010;urn:miriam:ensembl:ENSG00000136560;urn:miriam:refseq:NM_133484;urn:miriam:uniprot:Q92844;urn:miriam:uniprot:Q92844;urn:miriam:hgnc:11562;urn:miriam:hgnc.symbol:TANK;urn:miriam:hgnc.symbol:TANK;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033;urn:miriam:uniprot:Q14164;urn:miriam:uniprot:Q14164;urn:miriam:ncbigene:9641;urn:miriam:ncbigene:9641;urn:miriam:hgnc:14552;urn:miriam:ec-code:2.7.11.10;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:refseq:NM_001193321;urn:miriam:ensembl:ENSG00000263528"
      hgnc "HGNC_SYMBOL:TANK;HGNC_SYMBOL:TRAF3;HGNC_SYMBOL:IKBKE"
      map_id "M111_18"
      name "TANK:TRAF3:IKBKE"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa89"
      uniprot "UNIPROT:Q92844;UNIPROT:Q13114;UNIPROT:Q14164"
    ]
    graphics [
      x 1852.5144998161113
      y 1927.978844946098
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbiprotein:1796318601;urn:miriam:uniprot:M"
      hgnc "NA"
      map_id "M111_135"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa403"
      uniprot "UNIPROT:M"
    ]
    graphics [
      x 1301.4760702265748
      y 1222.3184295819842
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_78"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re291"
      uniprot "NA"
    ]
    graphics [
      x 1208.8536650413898
      y 819.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033"
      hgnc "HGNC_SYMBOL:TRAF3"
      map_id "M111_176"
      name "TRAF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa493"
      uniprot "UNIPROT:Q13114"
    ]
    graphics [
      x 1409.76808960802
      y 582.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_176"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbigene:10010;urn:miriam:ncbigene:10010;urn:miriam:ensembl:ENSG00000136560;urn:miriam:refseq:NM_133484;urn:miriam:uniprot:Q92844;urn:miriam:uniprot:Q92844;urn:miriam:hgnc:11562;urn:miriam:hgnc.symbol:TANK;urn:miriam:hgnc.symbol:TANK"
      hgnc "HGNC_SYMBOL:TANK"
      map_id "M111_175"
      name "TANK"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa492"
      uniprot "UNIPROT:Q92844"
    ]
    graphics [
      x 1751.4760702265748
      y 1216.8134512202714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_175"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:uniprot:Q14164;urn:miriam:uniprot:Q14164;urn:miriam:ncbigene:9641;urn:miriam:ncbigene:9641;urn:miriam:hgnc:14552;urn:miriam:ec-code:2.7.11.10;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:refseq:NM_001193321;urn:miriam:ensembl:ENSG00000263528"
      hgnc "HGNC_SYMBOL:IKBKE"
      map_id "M111_174"
      name "IKBKE"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa491"
      uniprot "UNIPROT:Q14164"
    ]
    graphics [
      x 878.8536650413897
      y 1067.6988244239385
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_174"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc:6118;urn:miriam:uniprot:Q14653;urn:miriam:uniprot:Q14653;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661;urn:miriam:ncbigene:3661"
      hgnc "HGNC_SYMBOL:IRF3"
      map_id "M111_101"
      name "IRF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa157"
      uniprot "UNIPROT:Q14653"
    ]
    graphics [
      x 1057.9673850663708
      y 1629.749802637834
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      annotation "PUBMED:25581309"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_84"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re76"
      uniprot "NA"
    ]
    graphics [
      x 487.96738506637064
      y 1527.0918947654488
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc:6118;urn:miriam:uniprot:Q14653;urn:miriam:uniprot:Q14653;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661;urn:miriam:ncbigene:3661"
      hgnc "HGNC_SYMBOL:IRF3"
      map_id "M111_100"
      name "IRF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa156"
      uniprot "UNIPROT:Q14653"
    ]
    graphics [
      x 1041.6104592494069
      y 1778.3037729112039
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      annotation "PUBMED:25581309"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_83"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re75"
      uniprot "NA"
    ]
    graphics [
      x 1331.4760702265748
      y 1212.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc:6118;urn:miriam:uniprot:Q14653;urn:miriam:uniprot:Q14653;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661;urn:miriam:ncbigene:3661"
      hgnc "HGNC_SYMBOL:IRF3"
      map_id "M111_130"
      name "IRF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa379"
      uniprot "UNIPROT:Q14653"
    ]
    graphics [
      x 1748.8536650413898
      y 766.8134512202714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_130"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:uniprot:Q9UHD2;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110"
      hgnc "HGNC_SYMBOL:TBK1"
      map_id "M111_99"
      name "TBK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa154"
      uniprot "UNIPROT:Q9UHD2"
    ]
    graphics [
      x 2062.5144998161113
      y 1837.6529641749933
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_77"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re290"
      uniprot "NA"
    ]
    graphics [
      x 2617.9673850663708
      y 1648.3670883574946
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:uniprot:Q9UHD2;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110"
      hgnc "HGNC_SYMBOL:TBK1"
      map_id "M111_173"
      name "TBK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa490"
      uniprot "UNIPROT:Q9UHD2"
    ]
    graphics [
      x 2738.85366504139
      y 1042.8004847563163
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_173"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033;urn:miriam:uniprot:Q14164;urn:miriam:uniprot:Q14164;urn:miriam:ncbigene:9641;urn:miriam:ncbigene:9641;urn:miriam:hgnc:14552;urn:miriam:ec-code:2.7.11.10;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:refseq:NM_001193321;urn:miriam:ensembl:ENSG00000263528;urn:miriam:ncbigene:10010;urn:miriam:ncbigene:10010;urn:miriam:ensembl:ENSG00000136560;urn:miriam:refseq:NM_133484;urn:miriam:uniprot:Q92844;urn:miriam:uniprot:Q92844;urn:miriam:hgnc:11562;urn:miriam:hgnc.symbol:TANK;urn:miriam:hgnc.symbol:TANK"
      hgnc "HGNC_SYMBOL:TRAF3;HGNC_SYMBOL:IKBKE;HGNC_SYMBOL:TANK"
      map_id "M111_12"
      name "TANK:TRAF3:IKBKE"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa82"
      uniprot "UNIPROT:Q13114;UNIPROT:Q14164;UNIPROT:Q92844"
    ]
    graphics [
      x 2025.0449368989925
      y 2196.313744265185
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914;PUBMED:19380580;PUBMED:27164085"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_76"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re289"
      uniprot "NA"
    ]
    graphics [
      x 2272.5144998161113
      y 2032.959745525941
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbigene:10010;urn:miriam:ncbigene:10010;urn:miriam:ensembl:ENSG00000136560;urn:miriam:refseq:NM_133484;urn:miriam:uniprot:Q92844;urn:miriam:uniprot:Q92844;urn:miriam:hgnc:11562;urn:miriam:hgnc.symbol:TANK;urn:miriam:hgnc.symbol:TANK;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033;urn:miriam:uniprot:Q14164;urn:miriam:uniprot:Q14164;urn:miriam:ncbigene:9641;urn:miriam:ncbigene:9641;urn:miriam:hgnc:14552;urn:miriam:ec-code:2.7.11.10;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:refseq:NM_001193321;urn:miriam:ensembl:ENSG00000263528"
      hgnc "HGNC_SYMBOL:TANK;HGNC_SYMBOL:TRAF3;HGNC_SYMBOL:IKBKE"
      map_id "M111_5"
      name "TANK:TRAF3:IKBKE"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa105"
      uniprot "UNIPROT:Q92844;UNIPROT:Q13114;UNIPROT:Q14164"
    ]
    graphics [
      x 1918.0032135630324
      y 2641.523414100048
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:uniprot:Q7Z434;urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746;urn:miriam:ncbigene:8717;urn:miriam:ensembl:ENSG00000102871;urn:miriam:ncbigene:8717;urn:miriam:refseq:NM_001323552;urn:miriam:uniprot:Q15628;urn:miriam:uniprot:Q15628;urn:miriam:hgnc:12030;urn:miriam:hgnc.symbol:TRADD;urn:miriam:hgnc.symbol:TRADD"
      hgnc "HGNC_SYMBOL:MAVS;HGNC_SYMBOL:TRADD"
      map_id "M111_15"
      name "MAVS:TRADD"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa86"
      uniprot "UNIPROT:Q7Z434;UNIPROT:Q15628"
    ]
    graphics [
      x 2557.9673850663708
      y 1590.3681188423423
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbiprotein:1802476807;urn:miriam:uniprot:Nsp3"
      hgnc "NA"
      map_id "M111_121"
      name "Nsp3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa361"
      uniprot "UNIPROT:Nsp3"
    ]
    graphics [
      x 2047.9673850663708
      y 1597.526658648621
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbiprotein:1796318601;urn:miriam:uniprot:M"
      hgnc "NA"
      map_id "M111_140"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa408"
      uniprot "UNIPROT:M"
    ]
    graphics [
      x 1811.8318792312245
      y 2394.6594484469133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_140"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_63"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re275"
      uniprot "NA"
    ]
    graphics [
      x 1532.8215329262537
      y 2202.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc:3573;urn:miriam:ncbigene:8772;urn:miriam:ncbigene:8772;urn:miriam:uniprot:Q13158;urn:miriam:uniprot:Q13158;urn:miriam:ensembl:ENSG00000168040;urn:miriam:refseq:NM_003824;urn:miriam:hgnc.symbol:FADD;urn:miriam:hgnc.symbol:FADD;urn:miriam:hgnc:10019;urn:miriam:hgnc.symbol:RIPK1;urn:miriam:ensembl:ENSG00000137275;urn:miriam:hgnc.symbol:RIPK1;urn:miriam:ec-code:2.7.11.1;urn:miriam:uniprot:Q13546;urn:miriam:uniprot:Q13546;urn:miriam:refseq:NM_003804;urn:miriam:ncbigene:8737;urn:miriam:ncbigene:8737"
      hgnc "HGNC_SYMBOL:FADD;HGNC_SYMBOL:RIPK1"
      map_id "M111_27"
      name "RIPK1:FADD"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa97"
      uniprot "UNIPROT:Q13158;UNIPROT:Q13546"
    ]
    graphics [
      x 1209.0311139321143
      y 2109.0166269831952
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc:10019;urn:miriam:hgnc.symbol:RIPK1;urn:miriam:ensembl:ENSG00000137275;urn:miriam:hgnc.symbol:RIPK1;urn:miriam:ec-code:2.7.11.1;urn:miriam:uniprot:Q13546;urn:miriam:uniprot:Q13546;urn:miriam:refseq:NM_003804;urn:miriam:ncbigene:8737;urn:miriam:ncbigene:8737;urn:miriam:hgnc:3573;urn:miriam:ncbigene:8772;urn:miriam:ncbigene:8772;urn:miriam:uniprot:Q13158;urn:miriam:uniprot:Q13158;urn:miriam:ensembl:ENSG00000168040;urn:miriam:refseq:NM_003824;urn:miriam:hgnc.symbol:FADD;urn:miriam:hgnc.symbol:FADD"
      hgnc "HGNC_SYMBOL:RIPK1;HGNC_SYMBOL:FADD"
      map_id "M111_14"
      name "RIPK1:FADD"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa84"
      uniprot "UNIPROT:Q13546;UNIPROT:Q13158"
    ]
    graphics [
      x 757.9673850663706
      y 1492.5713166070652
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_64"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re276"
      uniprot "NA"
    ]
    graphics [
      x 281.47607022657485
      y 1454.5857030040736
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      annotation "PUBMED:20303872"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_65"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re277"
      uniprot "NA"
    ]
    graphics [
      x 1697.2023363703734
      y 969.5710630443609
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc.symbol:CASP10;urn:miriam:refseq:NM_032977;urn:miriam:hgnc.symbol:CASP10;urn:miriam:uniprot:Q92851;urn:miriam:uniprot:Q92851;urn:miriam:ncbigene:843;urn:miriam:ncbigene:843;urn:miriam:ec-code:3.4.22.63;urn:miriam:hgnc:1500;urn:miriam:ensembl:ENSG00000003400"
      hgnc "HGNC_SYMBOL:CASP10"
      map_id "M111_164"
      name "CASP10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa463"
      uniprot "UNIPROT:Q92851"
    ]
    graphics [
      x 1778.8536650413898
      y 796.8134512202714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_164"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc.symbol:CASP10;urn:miriam:refseq:NM_032977;urn:miriam:hgnc.symbol:CASP10;urn:miriam:uniprot:Q92851;urn:miriam:uniprot:Q92851;urn:miriam:ncbigene:843;urn:miriam:ncbigene:843;urn:miriam:ec-code:3.4.22.63;urn:miriam:hgnc:1500;urn:miriam:ensembl:ENSG00000003400"
      hgnc "HGNC_SYMBOL:CASP10"
      map_id "M111_132"
      name "CASP10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa389"
      uniprot "UNIPROT:Q92851"
    ]
    graphics [
      x 1031.4760702265748
      y 1213.384979153474
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc:1509;urn:miriam:hgnc.symbol:CASP8;urn:miriam:uniprot:Q14790;urn:miriam:uniprot:Q14790;urn:miriam:hgnc.symbol:CASP8;urn:miriam:ncbigene:841;urn:miriam:ncbigene:841;urn:miriam:ec-code:3.4.22.61;urn:miriam:refseq:NM_001228;urn:miriam:ensembl:ENSG00000064012"
      hgnc "HGNC_SYMBOL:CASP8"
      map_id "M111_163"
      name "CASP8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa459"
      uniprot "UNIPROT:Q14790"
    ]
    graphics [
      x 908.8536650413897
      y 713.5627285309354
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_163"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc:1509;urn:miriam:hgnc.symbol:CASP8;urn:miriam:uniprot:Q14790;urn:miriam:uniprot:Q14790;urn:miriam:hgnc.symbol:CASP8;urn:miriam:ncbigene:841;urn:miriam:ncbigene:841;urn:miriam:ec-code:3.4.22.61;urn:miriam:refseq:NM_001228;urn:miriam:ensembl:ENSG00000064012"
      hgnc "HGNC_SYMBOL:CASP8"
      map_id "M111_133"
      name "CASP8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa390"
      uniprot "UNIPROT:Q14790"
    ]
    graphics [
      x 1012.5144998161113
      y 1948.3287053897677
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_133"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998;urn:miriam:hgnc:9955;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ncbigene:5970;urn:miriam:ncbigene:5970;urn:miriam:refseq:NM_021975;urn:miriam:hgnc.symbol:RELA;urn:miriam:hgnc.symbol:RELA;urn:miriam:uniprot:Q04206;urn:miriam:uniprot:Q04206"
      hgnc "HGNC_SYMBOL:NFKB1;HGNC_SYMBOL:RELA"
      map_id "M111_3"
      name "NFKB"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa103"
      uniprot "UNIPROT:P19838;UNIPROT:Q04206"
    ]
    graphics [
      x 1508.8536650413898
      y 552.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      annotation "PUBMED:20303872"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_81"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re72"
      uniprot "NA"
    ]
    graphics [
      x 998.8536650413897
      y 791.9148662117439
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998;urn:miriam:hgnc:9955;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ncbigene:5970;urn:miriam:ncbigene:5970;urn:miriam:refseq:NM_021975;urn:miriam:hgnc.symbol:RELA;urn:miriam:hgnc.symbol:RELA;urn:miriam:uniprot:Q04206;urn:miriam:uniprot:Q04206"
      hgnc "HGNC_SYMBOL:NFKB1;HGNC_SYMBOL:RELA"
      map_id "M111_11"
      name "NFKB"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa80"
      uniprot "UNIPROT:P19838;UNIPROT:Q04206"
    ]
    graphics [
      x 878.8536650413897
      y 1037.6988244239385
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      annotation "PUBMED:16143815"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_67"
      name "NA"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "re280"
      uniprot "NA"
    ]
    graphics [
      x 1718.8536650413898
      y 738.9527755204929
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc:9955;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ncbigene:5970;urn:miriam:ncbigene:5970;urn:miriam:refseq:NM_021975;urn:miriam:hgnc.symbol:RELA;urn:miriam:hgnc.symbol:RELA;urn:miriam:uniprot:Q04206;urn:miriam:uniprot:Q04206;urn:miriam:hgnc:7797;urn:miriam:refseq:NM_020529;urn:miriam:ensembl:ENSG00000100906;urn:miriam:uniprot:P25963;urn:miriam:uniprot:P25963;urn:miriam:ncbigene:4792;urn:miriam:ncbigene:4792;urn:miriam:hgnc.symbol:NFKBIA;urn:miriam:hgnc.symbol:NFKBIA;urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998"
      hgnc "HGNC_SYMBOL:RELA;HGNC_SYMBOL:NFKBIA;HGNC_SYMBOL:NFKB1"
      map_id "M111_2"
      name "NFKB:NFKBIA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa102"
      uniprot "UNIPROT:Q04206;UNIPROT:P25963;UNIPROT:P19838"
    ]
    graphics [
      x 1328.8536650413898
      y 492.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbiprotein:1798174255"
      hgnc "NA"
      map_id "M111_131"
      name "N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa381"
      uniprot "NA"
    ]
    graphics [
      x 1731.6104592494069
      y 1780.2515137006171
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_131"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc:7797;urn:miriam:refseq:NM_020529;urn:miriam:ensembl:ENSG00000100906;urn:miriam:uniprot:P25963;urn:miriam:uniprot:P25963;urn:miriam:ncbigene:4792;urn:miriam:ncbigene:4792;urn:miriam:hgnc.symbol:NFKBIA;urn:miriam:hgnc.symbol:NFKBIA"
      hgnc "HGNC_SYMBOL:NFKBIA"
      map_id "M111_165"
      name "NFKBIA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa475"
      uniprot "UNIPROT:P25963"
    ]
    graphics [
      x 1406.090149807047
      y 402.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_165"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      annotation "PUBMED:20303872"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_82"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re73"
      uniprot "NA"
    ]
    graphics [
      x 1601.4760702265748
      y 1212.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_16"
      name "ubiquitin_minus_proteasome_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa87"
      uniprot "NA"
    ]
    graphics [
      x 1841.4760702265748
      y 1141.3036009705227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_98"
      name "Degradation_space_of_space_NFKBIA"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa146"
      uniprot "NA"
    ]
    graphics [
      x 1667.2023363703734
      y 969.5710630443609
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re183"
      uniprot "NA"
    ]
    graphics [
      x 611.4760702265747
      y 1091.710136427816
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998;urn:miriam:hgnc:9955;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ncbigene:5970;urn:miriam:ncbigene:5970;urn:miriam:refseq:NM_021975;urn:miriam:hgnc.symbol:RELA;urn:miriam:hgnc.symbol:RELA;urn:miriam:uniprot:Q04206;urn:miriam:uniprot:Q04206;urn:miriam:hgnc:7797;urn:miriam:refseq:NM_020529;urn:miriam:ensembl:ENSG00000100906;urn:miriam:uniprot:P25963;urn:miriam:uniprot:P25963;urn:miriam:ncbigene:4792;urn:miriam:ncbigene:4792;urn:miriam:hgnc.symbol:NFKBIA;urn:miriam:hgnc.symbol:NFKBIA"
      hgnc "HGNC_SYMBOL:NFKB1;HGNC_SYMBOL:RELA;HGNC_SYMBOL:NFKBIA"
      map_id "M111_1"
      name "NFKB:NFKBIA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa100"
      uniprot "UNIPROT:P19838;UNIPROT:Q04206;UNIPROT:P25963"
    ]
    graphics [
      x 911.4760702265747
      y 1430.093014992105
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc.symbol:CHUK;urn:miriam:hgnc.symbol:CHUK;urn:miriam:refseq:NM_001278;urn:miriam:ec-code:2.7.11.10;urn:miriam:ensembl:ENSG00000213341;urn:miriam:hgnc:1974;urn:miriam:uniprot:O15111;urn:miriam:uniprot:O15111;urn:miriam:ncbigene:1147;urn:miriam:ncbigene:1147;urn:miriam:hgnc:5961;urn:miriam:ensembl:ENSG00000269335;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:refseq:NM_003639;urn:miriam:ncbigene:8517;urn:miriam:ncbigene:8517;urn:miriam:uniprot:Q9Y6K9;urn:miriam:uniprot:Q9Y6K9;urn:miriam:hgnc:5960;urn:miriam:ensembl:ENSG00000104365;urn:miriam:ec-code:2.7.11.10;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:uniprot:O14920;urn:miriam:uniprot:O14920;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:ncbigene:3551;urn:miriam:ncbigene:3551;urn:miriam:refseq:NM_001190720"
      hgnc "HGNC_SYMBOL:CHUK;HGNC_SYMBOL:IKBKG;HGNC_SYMBOL:IKBKB"
      map_id "M111_28"
      name "IKK_space_Complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa99"
      uniprot "UNIPROT:O15111;UNIPROT:Q9Y6K9;UNIPROT:O14920"
    ]
    graphics [
      x 1228.7557590654712
      y 2412.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_74"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re287"
      uniprot "NA"
    ]
    graphics [
      x 711.6104592494069
      y 1882.4666947456517
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc:7797;urn:miriam:refseq:NM_020529;urn:miriam:ensembl:ENSG00000100906;urn:miriam:uniprot:P25963;urn:miriam:uniprot:P25963;urn:miriam:ncbigene:4792;urn:miriam:ncbigene:4792;urn:miriam:hgnc.symbol:NFKBIA;urn:miriam:hgnc.symbol:NFKBIA"
      hgnc "HGNC_SYMBOL:NFKBIA"
      map_id "M111_167"
      name "NFKBIA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa481"
      uniprot "UNIPROT:P25963"
    ]
    graphics [
      x 678.7215148542178
      y 2319.8891532189577
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_167"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc:9955;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ncbigene:5970;urn:miriam:ncbigene:5970;urn:miriam:refseq:NM_021975;urn:miriam:hgnc.symbol:RELA;urn:miriam:hgnc.symbol:RELA;urn:miriam:uniprot:Q04206;urn:miriam:uniprot:Q04206"
      hgnc "HGNC_SYMBOL:RELA"
      map_id "M111_168"
      name "RELA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa482"
      uniprot "UNIPROT:Q04206"
    ]
    graphics [
      x 1270.4146427238684
      y 2232.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_168"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998"
      hgnc "HGNC_SYMBOL:NFKB1"
      map_id "M111_169"
      name "NFKB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa483"
      uniprot "UNIPROT:P19838"
    ]
    graphics [
      x 1358.9314900584518
      y 2232.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_169"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_57"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re269"
      uniprot "NA"
    ]
    graphics [
      x 2358.9874734832797
      y 2138.221696520471
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ec-code:2.7.11.24;urn:miriam:ensembl:ENSG00000112062;urn:miriam:hgnc:6876;urn:miriam:uniprot:Q16539;urn:miriam:uniprot:Q16539;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:refseq:NM_001315"
      hgnc "HGNC_SYMBOL:MAPK14"
      map_id "M111_156"
      name "MAPK14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa445"
      uniprot "UNIPROT:Q16539"
    ]
    graphics [
      x 2064.080281687381
      y 2575.9109795819522
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_156"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ec-code:2.7.12.2;urn:miriam:ensembl:ENSG00000108984;urn:miriam:ncbigene:5608;urn:miriam:ncbigene:5608;urn:miriam:uniprot:P52564;urn:miriam:uniprot:P52564;urn:miriam:refseq:NM_002758;urn:miriam:hgnc:6846;urn:miriam:hgnc.symbol:MAP2K6;urn:miriam:hgnc.symbol:MAP2K6"
      hgnc "HGNC_SYMBOL:MAP2K6"
      map_id "M111_157"
      name "MAP2K6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa450"
      uniprot "UNIPROT:P52564"
    ]
    graphics [
      x 2090.00743414277
      y 2324.455047131421
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_157"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbigene:5606;urn:miriam:ncbigene:5606;urn:miriam:ec-code:2.7.12.2;urn:miriam:uniprot:P46734;urn:miriam:uniprot:P46734;urn:miriam:hgnc:6843;urn:miriam:refseq:NM_145109;urn:miriam:hgnc.symbol:MAP2K3;urn:miriam:hgnc.symbol:MAP2K3;urn:miriam:ensembl:ENSG00000034152"
      hgnc "HGNC_SYMBOL:MAP2K3"
      map_id "M111_158"
      name "MAP2K3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa451"
      uniprot "UNIPROT:P46734"
    ]
    graphics [
      x 1718.8536650413898
      y 897.7984070335631
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_158"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ec-code:2.7.12.2;urn:miriam:ncbigene:5609;urn:miriam:ncbigene:5609;urn:miriam:uniprot:O14733;urn:miriam:uniprot:O14733;urn:miriam:ensembl:ENSG00000076984;urn:miriam:hgnc:6847;urn:miriam:refseq:NM_001297555;urn:miriam:hgnc.symbol:MAP2K7;urn:miriam:hgnc.symbol:MAP2K7"
      hgnc "HGNC_SYMBOL:MAP2K7"
      map_id "M111_129"
      name "MAP2K7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa372"
      uniprot "UNIPROT:O14733"
    ]
    graphics [
      x 1822.5144998161113
      y 1872.4206517193227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_129"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ec-code:2.7.12.2;urn:miriam:ensembl:ENSG00000108984;urn:miriam:ncbigene:5608;urn:miriam:ncbigene:5608;urn:miriam:uniprot:P52564;urn:miriam:uniprot:P52564;urn:miriam:refseq:NM_002758;urn:miriam:hgnc:6846;urn:miriam:hgnc.symbol:MAP2K6;urn:miriam:hgnc.symbol:MAP2K6"
      hgnc "HGNC_SYMBOL:MAP2K6"
      map_id "M111_127"
      name "MAP2K6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa370"
      uniprot "UNIPROT:P52564"
    ]
    graphics [
      x 2392.5144998161113
      y 1835.2368603913133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_127"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc:9437;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc.symbol:EIF2AK2;urn:miriam:hgnc.symbol:EIF2AK2;urn:miriam:uniprot:P19525;urn:miriam:uniprot:P19525;urn:miriam:refseq:NM_002759;urn:miriam:ncbigene:5610;urn:miriam:ensembl:ENSG00000055332;urn:miriam:ncbigene:5610"
      hgnc "HGNC_SYMBOL:EIF2AK2"
      map_id "M111_26"
      name "EIF2AK2:dsRNA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa96"
      uniprot "UNIPROT:P19525"
    ]
    graphics [
      x 1871.4760702265748
      y 1213.234341461012
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914;PUBMED:28484023"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_62"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re274"
      uniprot "NA"
    ]
    graphics [
      x 2351.476070226575
      y 1150.4850879710662
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_162"
      name "dsRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa456"
      uniprot "NA"
    ]
    graphics [
      x 2141.476070226575
      y 1162.799581371932
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_162"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc:9437;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc.symbol:EIF2AK2;urn:miriam:hgnc.symbol:EIF2AK2;urn:miriam:uniprot:P19525;urn:miriam:uniprot:P19525;urn:miriam:refseq:NM_002759;urn:miriam:ncbigene:5610;urn:miriam:ensembl:ENSG00000055332;urn:miriam:ncbigene:5610"
      hgnc "HGNC_SYMBOL:EIF2AK2"
      map_id "M111_161"
      name "EIF2AK2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa454"
      uniprot "UNIPROT:P19525"
    ]
    graphics [
      x 1851.6104592494069
      y 1742.60971039914
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_161"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbiprotein:1802476818"
      hgnc "NA"
      map_id "M111_142"
      name "Nsp15"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa416"
      uniprot "NA"
    ]
    graphics [
      x 2257.9673850663708
      y 1530.4603613372324
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_142"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbigene:5606;urn:miriam:ncbigene:5606;urn:miriam:ec-code:2.7.12.2;urn:miriam:uniprot:P46734;urn:miriam:uniprot:P46734;urn:miriam:hgnc:6843;urn:miriam:refseq:NM_145109;urn:miriam:hgnc.symbol:MAP2K3;urn:miriam:hgnc.symbol:MAP2K3;urn:miriam:ensembl:ENSG00000034152"
      hgnc "HGNC_SYMBOL:MAP2K3"
      map_id "M111_126"
      name "MAP2K3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa369"
      uniprot "UNIPROT:P46734"
    ]
    graphics [
      x 2618.85366504139
      y 1010.6875962743773
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_42"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re251"
      uniprot "NA"
    ]
    graphics [
      x 1688.8380080444476
      y 269.8938938745571
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc.symbol:TICAM1;urn:miriam:uniprot:Q8IUC6;urn:miriam:uniprot:Q8IUC6;urn:miriam:hgnc.symbol:TICAM1;urn:miriam:ensembl:ENSG00000127666;urn:miriam:hgnc:18348;urn:miriam:refseq:NM_014261;urn:miriam:ncbigene:148022;urn:miriam:ncbigene:148022"
      hgnc "HGNC_SYMBOL:TICAM1"
      map_id "M111_181"
      name "TICAM1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa91"
      uniprot "UNIPROT:Q8IUC6"
    ]
    graphics [
      x 2141.476070226575
      y 999.5179330273552
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_181"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7189;urn:miriam:ncbigene:7189;urn:miriam:ensembl:ENSG00000175104;urn:miriam:uniprot:Q9Y4K3;urn:miriam:uniprot:Q9Y4K3;urn:miriam:hgnc:12036;urn:miriam:refseq:NM_145803;urn:miriam:hgnc.symbol:TRAF6;urn:miriam:hgnc.symbol:TRAF6"
      hgnc "HGNC_SYMBOL:TRAF6"
      map_id "M111_145"
      name "TRAF6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa422"
      uniprot "UNIPROT:Q9Y4K3"
    ]
    graphics [
      x 1466.090149807047
      y 372.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_145"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc.symbol:TICAM1;urn:miriam:uniprot:Q8IUC6;urn:miriam:uniprot:Q8IUC6;urn:miriam:hgnc.symbol:TICAM1;urn:miriam:ensembl:ENSG00000127666;urn:miriam:hgnc:18348;urn:miriam:refseq:NM_014261;urn:miriam:ncbigene:148022;urn:miriam:ncbigene:148022"
      hgnc "HGNC_SYMBOL:TICAM1"
      map_id "M111_144"
      name "TICAM1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa419"
      uniprot "UNIPROT:Q8IUC6"
    ]
    graphics [
      x 1306.6057727547209
      y 942.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_144"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      annotation "PUBMED:20303872"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_40"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re249"
      uniprot "NA"
    ]
    graphics [
      x 2287.9673850663708
      y 1564.1444038133232
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc.symbol:TICAM1;urn:miriam:uniprot:Q8IUC6;urn:miriam:uniprot:Q8IUC6;urn:miriam:hgnc.symbol:TICAM1;urn:miriam:ensembl:ENSG00000127666;urn:miriam:hgnc:18348;urn:miriam:refseq:NM_014261;urn:miriam:ncbigene:148022;urn:miriam:ncbigene:148022"
      hgnc "HGNC_SYMBOL:TICAM1"
      map_id "M111_143"
      name "TICAM1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa418"
      uniprot "UNIPROT:Q8IUC6"
    ]
    graphics [
      x 1910.0074341427699
      y 2350.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_143"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:uniprot:O15455;urn:miriam:uniprot:O15455;urn:miriam:ensembl:ENSG00000164342;urn:miriam:refseq:NM_003265;urn:miriam:ncbigene:7098;urn:miriam:ncbigene:7098;urn:miriam:hgnc.symbol:TLR3;urn:miriam:hgnc.symbol:TLR3;urn:miriam:hgnc:11849"
      hgnc "HGNC_SYMBOL:TLR3"
      map_id "M111_17"
      name "TLR3:dsRNA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa88"
      uniprot "UNIPROT:O15455"
    ]
    graphics [
      x 1328.8536650413898
      y 642.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      annotation "PUBMED:20303872"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_41"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re250"
      uniprot "NA"
    ]
    graphics [
      x 1148.8536650413898
      y 863.0689340107122
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:uniprot:O15455;urn:miriam:uniprot:O15455;urn:miriam:ensembl:ENSG00000164342;urn:miriam:refseq:NM_003265;urn:miriam:ncbigene:7098;urn:miriam:ncbigene:7098;urn:miriam:hgnc.symbol:TLR3;urn:miriam:hgnc.symbol:TLR3;urn:miriam:hgnc:11849"
      hgnc "HGNC_SYMBOL:TLR3"
      map_id "M111_177"
      name "TLR3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa5"
      uniprot "UNIPROT:O15455"
    ]
    graphics [
      x 1496.090149807047
      y 402.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_177"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_178"
      name "dsRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa6"
      uniprot "NA"
    ]
    graphics [
      x 1251.6104592494069
      y 1752.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_178"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbiprotein:1802476807;urn:miriam:uniprot:Nsp3"
      hgnc "NA"
      map_id "M111_123"
      name "Nsp3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa363"
      uniprot "UNIPROT:Nsp3"
    ]
    graphics [
      x 401.47607022657485
      y 1142.5572932182372
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc:5961;urn:miriam:ensembl:ENSG00000269335;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:refseq:NM_003639;urn:miriam:ncbigene:8517;urn:miriam:ncbigene:8517;urn:miriam:uniprot:Q9Y6K9;urn:miriam:uniprot:Q9Y6K9;urn:miriam:hgnc.symbol:CHUK;urn:miriam:hgnc.symbol:CHUK;urn:miriam:refseq:NM_001278;urn:miriam:ec-code:2.7.11.10;urn:miriam:ensembl:ENSG00000213341;urn:miriam:hgnc:1974;urn:miriam:uniprot:O15111;urn:miriam:uniprot:O15111;urn:miriam:ncbigene:1147;urn:miriam:ncbigene:1147;urn:miriam:hgnc:5960;urn:miriam:ensembl:ENSG00000104365;urn:miriam:ec-code:2.7.11.10;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:uniprot:O14920;urn:miriam:uniprot:O14920;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:ncbigene:3551;urn:miriam:ncbigene:3551;urn:miriam:refseq:NM_001190720"
      hgnc "HGNC_SYMBOL:IKBKG;HGNC_SYMBOL:CHUK;HGNC_SYMBOL:IKBKB"
      map_id "M111_8"
      name "IKK_space_Complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa75"
      uniprot "UNIPROT:Q9Y6K9;UNIPROT:O15111;UNIPROT:O14920"
    ]
    graphics [
      x 1391.4760702265748
      y 1092.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbiprotein:1796318601;urn:miriam:uniprot:M"
      hgnc "NA"
      map_id "M111_120"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa359"
      uniprot "UNIPROT:M"
    ]
    graphics [
      x 1670.0074341427699
      y 2292.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_75"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re288"
      uniprot "NA"
    ]
    graphics [
      x 2168.85366504139
      y 787.8354163586905
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc:5961;urn:miriam:ensembl:ENSG00000269335;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:refseq:NM_003639;urn:miriam:ncbigene:8517;urn:miriam:ncbigene:8517;urn:miriam:uniprot:Q9Y6K9;urn:miriam:uniprot:Q9Y6K9"
      hgnc "HGNC_SYMBOL:IKBKG"
      map_id "M111_170"
      name "IKBKG"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa484"
      uniprot "UNIPROT:Q9Y6K9"
    ]
    graphics [
      x 1357.9673850663708
      y 1632.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_170"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc.symbol:CHUK;urn:miriam:hgnc.symbol:CHUK;urn:miriam:refseq:NM_001278;urn:miriam:ec-code:2.7.11.10;urn:miriam:ensembl:ENSG00000213341;urn:miriam:hgnc:1974;urn:miriam:uniprot:O15111;urn:miriam:uniprot:O15111;urn:miriam:ncbigene:1147;urn:miriam:ncbigene:1147"
      hgnc "HGNC_SYMBOL:CHUK"
      map_id "M111_171"
      name "CHUK"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa485"
      uniprot "UNIPROT:O15111"
    ]
    graphics [
      x 1961.4760702265748
      y 1067.9853441981315
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_171"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc:5960;urn:miriam:ensembl:ENSG00000104365;urn:miriam:ec-code:2.7.11.10;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:uniprot:O14920;urn:miriam:uniprot:O14920;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:ncbigene:3551;urn:miriam:ncbigene:3551;urn:miriam:refseq:NM_001190720"
      hgnc "HGNC_SYMBOL:IKBKB"
      map_id "M111_172"
      name "IKBKB"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa486"
      uniprot "UNIPROT:O14920"
    ]
    graphics [
      x 2157.80425126645
      y 399.77500143851694
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_172"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      annotation "PUBMED:15383579"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_46"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re255"
      uniprot "NA"
    ]
    graphics [
      x 531.6104592494069
      y 1884.8660612677036
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbigene:4615;urn:miriam:ensembl:ENSG00000172936;urn:miriam:ncbigene:4615;urn:miriam:pubmed:19366914;urn:miriam:uniprot:Q99836;urn:miriam:uniprot:Q99836;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc:7562;urn:miriam:refseq:NM_002468"
      hgnc "HGNC_SYMBOL:MYD88"
      map_id "M111_179"
      name "MYD88"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa77"
      uniprot "UNIPROT:Q99836"
    ]
    graphics [
      x 1012.5144998161113
      y 1989.6386331035148
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_179"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc:6112;urn:miriam:uniprot:P51617;urn:miriam:uniprot:P51617;urn:miriam:ec-code:2.7.11.1;urn:miriam:ensembl:ENSG00000184216;urn:miriam:refseq:NM_001025242;urn:miriam:hgnc.symbol:IRAK1;urn:miriam:ncbigene:3654;urn:miriam:hgnc.symbol:IRAK1;urn:miriam:ncbigene:3654"
      hgnc "HGNC_SYMBOL:IRAK1"
      map_id "M111_180"
      name "IRAK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa80"
      uniprot "UNIPROT:P51617"
    ]
    graphics [
      x 1072.5144998161113
      y 1852.29086407284
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_180"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:uniprot:Q9NWZ3;urn:miriam:uniprot:Q9NWZ3;urn:miriam:refseq:NM_001114182;urn:miriam:ec-code:2.7.11.1;urn:miriam:ncbigene:51135;urn:miriam:ncbigene:51135;urn:miriam:hgnc:17967;urn:miriam:hgnc.symbol:IRAK4;urn:miriam:hgnc.symbol:IRAK4;urn:miriam:ensembl:ENSG00000198001"
      hgnc "HGNC_SYMBOL:IRAK4"
      map_id "M111_109"
      name "IRAK4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa333"
      uniprot "UNIPROT:Q9NWZ3"
    ]
    graphics [
      x 281.47607022657485
      y 1385.8315448445992
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7189;urn:miriam:ncbigene:7189;urn:miriam:ensembl:ENSG00000175104;urn:miriam:uniprot:Q9Y4K3;urn:miriam:uniprot:Q9Y4K3;urn:miriam:hgnc:12036;urn:miriam:refseq:NM_145803;urn:miriam:hgnc.symbol:TRAF6;urn:miriam:hgnc.symbol:TRAF6"
      hgnc "HGNC_SYMBOL:TRAF6"
      map_id "M111_148"
      name "TRAF6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa428"
      uniprot "UNIPROT:Q9Y4K3"
    ]
    graphics [
      x 585.4286082386425
      y 2323.243905175061
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_148"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_49"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re259"
      uniprot "NA"
    ]
    graphics [
      x 1867.9673850663708
      y 1645.5616158879839
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbigene:4615;urn:miriam:ensembl:ENSG00000172936;urn:miriam:ncbigene:4615;urn:miriam:pubmed:19366914;urn:miriam:uniprot:Q99836;urn:miriam:uniprot:Q99836;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc:7562;urn:miriam:refseq:NM_002468"
      hgnc "HGNC_SYMBOL:MYD88"
      map_id "M111_151"
      name "MYD88"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa433"
      uniprot "UNIPROT:Q99836"
    ]
    graphics [
      x 2152.5144998161113
      y 1817.7056484700797
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_151"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:uniprot:Q9NYK1;urn:miriam:uniprot:Q9NYK1;urn:miriam:hgnc:15631;urn:miriam:refseq:NM_016562;urn:miriam:hgnc.symbol:TLR7;urn:miriam:hgnc.symbol:TLR7;urn:miriam:ensembl:ENSG00000196664;urn:miriam:ncbigene:51284;urn:miriam:ncbigene:51284"
      hgnc "HGNC_SYMBOL:TLR7"
      map_id "M111_20"
      name "TLR7:ssRNA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa90"
      uniprot "UNIPROT:Q9NYK1"
    ]
    graphics [
      x 847.9673850663706
      y 1545.0908463925657
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:refseq:NM_016610;urn:miriam:uniprot:Q9NR97;urn:miriam:uniprot:Q9NR97;urn:miriam:hgnc:15632;urn:miriam:ncbigene:51311;urn:miriam:ncbigene:51311;urn:miriam:hgnc.symbol:TLR8;urn:miriam:ensembl:ENSG00000101916;urn:miriam:hgnc.symbol:TLR8"
      hgnc "HGNC_SYMBOL:TLR8"
      map_id "M111_21"
      name "TLR8:ssRNA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa91"
      uniprot "UNIPROT:Q9NR97"
    ]
    graphics [
      x 1357.9673850663708
      y 1602.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re256"
      uniprot "NA"
    ]
    graphics [
      x 1361.4760702265748
      y 1205.4746169734021
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:refseq:NM_016610;urn:miriam:uniprot:Q9NR97;urn:miriam:uniprot:Q9NR97;urn:miriam:hgnc:15632;urn:miriam:ncbigene:51311;urn:miriam:ncbigene:51311;urn:miriam:hgnc.symbol:TLR8;urn:miriam:ensembl:ENSG00000101916;urn:miriam:hgnc.symbol:TLR8"
      hgnc "HGNC_SYMBOL:TLR8"
      map_id "M111_149"
      name "TLR8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa429"
      uniprot "UNIPROT:Q9NR97"
    ]
    graphics [
      x 1901.4760702265748
      y 1261.4707703304423
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_149"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_97"
      name "ssRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa14"
      uniprot "NA"
    ]
    graphics [
      x 698.8536650413897
      y 1076.649450965198
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      annotation "PUBMED:19366914"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_48"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re257"
      uniprot "NA"
    ]
    graphics [
      x 488.8536650413897
      y 996.1337073060629
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:uniprot:Q9NYK1;urn:miriam:uniprot:Q9NYK1;urn:miriam:hgnc:15631;urn:miriam:refseq:NM_016562;urn:miriam:hgnc.symbol:TLR7;urn:miriam:hgnc.symbol:TLR7;urn:miriam:ensembl:ENSG00000196664;urn:miriam:ncbigene:51284;urn:miriam:ncbigene:51284"
      hgnc "HGNC_SYMBOL:TLR7"
      map_id "M111_150"
      name "TLR7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa430"
      uniprot "UNIPROT:Q9NYK1"
    ]
    graphics [
      x 221.47607022657485
      y 1166.012794142974
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_150"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      annotation "PUBMED:25135833"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_39"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re243"
      uniprot "NA"
    ]
    graphics [
      x 1436.090149807047
      y 462.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:uniprot:Q96J02;urn:miriam:uniprot:Q96J02;urn:miriam:ec-code:2.3.2.26;urn:miriam:hgnc.symbol:ITCH;urn:miriam:hgnc.symbol:ITCH;urn:miriam:ensembl:ENSG00000078747;urn:miriam:refseq:NM_001257137;urn:miriam:ncbigene:83737;urn:miriam:ncbigene:83737;urn:miriam:hgnc:13890"
      hgnc "HGNC_SYMBOL:ITCH"
      map_id "M111_137"
      name "ITCH"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa405"
      uniprot "UNIPROT:Q96J02"
    ]
    graphics [
      x 1058.8536650413898
      y 756.3826300445392
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_137"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_138"
      name "s551"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa406"
      uniprot "NA"
    ]
    graphics [
      x 788.8536650413897
      y 504.70413857018787
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_138"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      annotation "PUBMED:25135833"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_52"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re262"
      uniprot "NA"
    ]
    graphics [
      x 371.47607022657485
      y 1078.3467144743568
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      annotation "PUBMED:25135833"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_37"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re241"
      uniprot "NA"
    ]
    graphics [
      x 1972.5144998161113
      y 1847.606843746084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 167
    zlevel -1

    cd19dm [
      annotation "PUBMED:25135833"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_38"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re242"
      uniprot "NA"
    ]
    graphics [
      x 2168.85366504139
      y 963.6675636343114
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 168
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_136"
      name "s550"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa404"
      uniprot "NA"
    ]
    graphics [
      x 2141.476070226575
      y 1050.2232722298047
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 169
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_139"
      name "s552"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa407"
      uniprot "NA"
    ]
    graphics [
      x 1811.4760702265748
      y 1513.8597507391748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_139"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 170
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:uniprot:Q96J02;urn:miriam:uniprot:Q96J02;urn:miriam:ec-code:2.3.2.26;urn:miriam:hgnc.symbol:ITCH;urn:miriam:hgnc.symbol:ITCH;urn:miriam:ensembl:ENSG00000078747;urn:miriam:refseq:NM_001257137;urn:miriam:ncbigene:83737;urn:miriam:ncbigene:83737;urn:miriam:hgnc:13890"
      hgnc "HGNC_SYMBOL:ITCH"
      map_id "M111_153"
      name "ITCH"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa436"
      uniprot "UNIPROT:Q96J02"
    ]
    graphics [
      x 671.4760702265747
      y 1497.7170511520449
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_153"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 171
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbigene:1489679;urn:miriam:uniprot:P59636"
      hgnc "NA"
      map_id "M111_122"
      name "Orf9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa362"
      uniprot "UNIPROT:P59636"
    ]
    graphics [
      x 491.47607022657473
      y 1335.6624883981624
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 172
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbigene:8717;urn:miriam:ensembl:ENSG00000102871;urn:miriam:ncbigene:8717;urn:miriam:refseq:NM_001323552;urn:miriam:uniprot:Q15628;urn:miriam:uniprot:Q15628;urn:miriam:hgnc:12030;urn:miriam:hgnc.symbol:TRADD;urn:miriam:hgnc.symbol:TRADD"
      hgnc "HGNC_SYMBOL:TRADD"
      map_id "M111_134"
      name "TRADD"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa400"
      uniprot "UNIPROT:Q15628"
    ]
    graphics [
      x 1691.055469358758
      y 939.5710630443609
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 173
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:28484023;PUBMED:19380580"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re14"
      uniprot "NA"
    ]
    graphics [
      x 2108.85366504139
      y 604.4720194774611
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 174
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_107"
      name "dsRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa27"
      uniprot "NA"
    ]
    graphics [
      x 2228.85366504139
      y 814.9569149281664
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 175
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:hgnc:18873;urn:miriam:ncbigene:64135;urn:miriam:ncbigene:64135;urn:miriam:refseq:NM_022168;urn:miriam:ensembl:ENSG00000115267;urn:miriam:uniprot:Q9BYX4;urn:miriam:uniprot:Q9BYX4;urn:miriam:ec-code:3.6.4.13;urn:miriam:hgnc.symbol:IFIH1;urn:miriam:hgnc.symbol:IFIH1"
      hgnc "HGNC_SYMBOL:IFIH1"
      map_id "M111_154"
      name "IFIH1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa439"
      uniprot "UNIPROT:Q9BYX4"
    ]
    graphics [
      x 2468.85366504139
      y 867.435702969883
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_154"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 176
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbiprotein:1796318601;urn:miriam:uniprot:M"
      hgnc "NA"
      map_id "M111_115"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa353"
      uniprot "UNIPROT:M"
    ]
    graphics [
      x 551.4760702265747
      y 1086.06127345672
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 177
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbiprotein:1802476807;urn:miriam:uniprot:Nsp3"
      hgnc "NA"
      map_id "M111_116"
      name "Nsp3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa354"
      uniprot "UNIPROT:Nsp3"
    ]
    graphics [
      x 1091.4760702265748
      y 1448.1096402055994
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 178
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbiprotein:1802476818"
      hgnc "NA"
      map_id "M111_141"
      name "Nsp15"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa415"
      uniprot "NA"
    ]
    graphics [
      x 2263.616842335231
      y 261.29689895980914
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_141"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 179
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:20303872;PUBMED:19380580"
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_29"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re13"
      uniprot "NA"
    ]
    graphics [
      x 277.96738506637075
      y 1638.1307880559607
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 180
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M111_105"
      name "dsRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa26"
      uniprot "NA"
    ]
    graphics [
      x 1210.4146427238684
      y 2262.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 181
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:PAMP signalling"
      full_annotation "urn:miriam:ncbigene:23586;urn:miriam:ncbigene:23586;urn:miriam:refseq:NM_014314;urn:miriam:ensembl:ENSG00000107201;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:uniprot:O95786;urn:miriam:hgnc:19102;urn:miriam:hgnc.symbol:DDX58;urn:miriam:hgnc.symbol:DDX58"
      hgnc "HGNC_SYMBOL:DDX58"
      map_id "M111_155"
      name "DDX58"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa440"
      uniprot "UNIPROT:O95786"
    ]
    graphics [
      x 551.4760702265747
      y 1325.5368588576707
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M111_155"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 182
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_102"
      target_id "M111_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 3
    target 2
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M111_90"
      target_id "M111_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 4
    target 2
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M111_89"
      target_id "M111_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 5
    target 2
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "INHIBITION"
      source_id "M111_114"
      target_id "M111_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 6
    target 2
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "M111_22"
      target_id "M111_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 2
    target 7
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_79"
      target_id "M111_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 179
    target 6
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_29"
      target_id "M111_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 7
    target 8
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "M111_91"
      target_id "M111_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 9
    target 8
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_92"
      target_id "M111_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 10
    target 8
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "M111_23"
      target_id "M111_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 8
    target 11
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_80"
      target_id "M111_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 9
    target 166
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_92"
      target_id "M111_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 173
    target 10
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_34"
      target_id "M111_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 11
    target 12
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_93"
      target_id "M111_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 11
    target 13
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_93"
      target_id "M111_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 172
    target 12
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_134"
      target_id "M111_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 12
    target 90
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_53"
      target_id "M111_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 14
    target 13
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_95"
      target_id "M111_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 15
    target 13
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_96"
      target_id "M111_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 16
    target 13
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_94"
      target_id "M111_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 13
    target 17
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_51"
      target_id "M111_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 15
    target 167
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_96"
      target_id "M111_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 16
    target 162
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_94"
      target_id "M111_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 17
    target 18
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "M111_6"
      target_id "M111_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 19
    target 18
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_106"
      target_id "M111_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 20
    target 18
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "M111_108"
      target_id "M111_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 21
    target 18
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "M111_7"
      target_id "M111_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 18
    target 22
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_35"
      target_id "M111_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 24
    target 19
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_54"
      target_id "M111_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 148
    target 21
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_46"
      target_id "M111_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 21
    target 68
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "M111_7"
      target_id "M111_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 22
    target 23
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M111_110"
      target_id "M111_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 22
    target 24
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_110"
      target_id "M111_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 22
    target 25
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M111_110"
      target_id "M111_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 142
    target 23
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_8"
      target_id "M111_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 101
    target 23
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M111_133"
      target_id "M111_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 143
    target 23
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "INHIBITION"
      source_id "M111_120"
      target_id "M111_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 99
    target 23
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M111_132"
      target_id "M111_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 23
    target 114
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_66"
      target_id "M111_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 141
    target 24
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "M111_123"
      target_id "M111_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 26
    target 25
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_152"
      target_id "M111_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 27
    target 25
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M111_19"
      target_id "M111_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 25
    target 28
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_50"
      target_id "M111_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 131
    target 27
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_42"
      target_id "M111_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 27
    target 63
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M111_19"
      target_id "M111_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 28
    target 29
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "M111_124"
      target_id "M111_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 28
    target 30
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "M111_124"
      target_id "M111_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 28
    target 31
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "M111_124"
      target_id "M111_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 28
    target 32
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "M111_124"
      target_id "M111_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 130
    target 29
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_126"
      target_id "M111_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 29
    target 122
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_59"
      target_id "M111_158"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 124
    target 30
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_127"
      target_id "M111_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 125
    target 30
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M111_26"
      target_id "M111_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 30
    target 121
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_58"
      target_id "M111_157"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 123
    target 31
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_129"
      target_id "M111_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 31
    target 37
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_61"
      target_id "M111_160"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 33
    target 32
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_128"
      target_id "M111_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 239
    source 32
    target 34
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_60"
      target_id "M111_159"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 240
    source 34
    target 35
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "M111_159"
      target_id "M111_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 241
    source 36
    target 35
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_9"
      target_id "M111_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 242
    source 37
    target 35
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "M111_160"
      target_id "M111_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 243
    source 35
    target 38
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_56"
      target_id "M111_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 244
    source 38
    target 39
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M111_24"
      target_id "M111_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 245
    source 40
    target 39
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_25"
      target_id "M111_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 246
    source 41
    target 39
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M111_125"
      target_id "M111_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 247
    source 39
    target 42
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_55"
      target_id "M111_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 248
    source 119
    target 41
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_57"
      target_id "M111_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 249
    source 42
    target 43
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_10"
      target_id "M111_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 250
    source 43
    target 44
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_68"
      target_id "M111_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 251
    source 44
    target 45
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_4"
      target_id "M111_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 252
    source 45
    target 46
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_69"
      target_id "M111_166"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 253
    source 47
    target 46
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_72"
      target_id "M111_166"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 254
    source 48
    target 46
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_70"
      target_id "M111_166"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 255
    source 49
    target 46
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_71"
      target_id "M111_166"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 256
    source 50
    target 46
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_73"
      target_id "M111_166"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 257
    source 72
    target 47
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_103"
      target_id "M111_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 258
    source 102
    target 48
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_3"
      target_id "M111_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 259
    source 79
    target 49
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_101"
      target_id "M111_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 260
    source 51
    target 50
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_104"
      target_id "M111_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 261
    source 52
    target 51
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_32"
      target_id "M111_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 262
    source 53
    target 52
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_87"
      target_id "M111_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 263
    source 54
    target 53
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_30"
      target_id "M111_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 264
    source 55
    target 54
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_112"
      target_id "M111_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 265
    source 56
    target 54
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "M111_85"
      target_id "M111_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 266
    source 57
    target 54
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "INHIBITION"
      source_id "M111_119"
      target_id "M111_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 267
    source 58
    target 54
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "UNKNOWN_INHIBITION"
      source_id "M111_118"
      target_id "M111_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 268
    source 59
    target 54
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "UNKNOWN_INHIBITION"
      source_id "M111_117"
      target_id "M111_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 269
    source 60
    target 56
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_44"
      target_id "M111_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 270
    source 61
    target 60
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_146"
      target_id "M111_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 271
    source 62
    target 60
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M111_13"
      target_id "M111_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 272
    source 63
    target 62
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_43"
      target_id "M111_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 273
    source 62
    target 64
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M111_13"
      target_id "M111_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 274
    source 73
    target 63
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_18"
      target_id "M111_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 275
    source 74
    target 63
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "INHIBITION"
      source_id "M111_135"
      target_id "M111_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 276
    source 65
    target 64
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_147"
      target_id "M111_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 277
    source 66
    target 64
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "INHIBITION"
      source_id "M111_113"
      target_id "M111_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 278
    source 64
    target 67
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_45"
      target_id "M111_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 67
    target 68
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "M111_86"
      target_id "M111_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 69
    target 68
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_111"
      target_id "M111_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 68
    target 70
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_31"
      target_id "M111_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 70
    target 71
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_88"
      target_id "M111_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 71
    target 72
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_33"
      target_id "M111_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 75
    target 73
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_78"
      target_id "M111_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 76
    target 75
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_176"
      target_id "M111_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 77
    target 75
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_175"
      target_id "M111_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 78
    target 75
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_174"
      target_id "M111_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 80
    target 79
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_84"
      target_id "M111_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 81
    target 80
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_100"
      target_id "M111_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 82
    target 81
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_83"
      target_id "M111_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 83
    target 82
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_130"
      target_id "M111_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 84
    target 82
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "M111_99"
      target_id "M111_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 85
    target 84
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_77"
      target_id "M111_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 86
    target 85
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_173"
      target_id "M111_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 87
    target 85
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M111_12"
      target_id "M111_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 88
    target 87
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_76"
      target_id "M111_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 89
    target 88
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_5"
      target_id "M111_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 90
    target 88
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M111_15"
      target_id "M111_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 91
    target 88
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "INHIBITION"
      source_id "M111_121"
      target_id "M111_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 92
    target 88
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "INHIBITION"
      source_id "M111_140"
      target_id "M111_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 90
    target 93
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M111_15"
      target_id "M111_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 94
    target 93
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_27"
      target_id "M111_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 93
    target 95
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_63"
      target_id "M111_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 304
    source 95
    target 96
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M111_14"
      target_id "M111_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 305
    source 95
    target 97
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M111_14"
      target_id "M111_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 306
    source 100
    target 96
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_163"
      target_id "M111_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 307
    source 96
    target 101
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_64"
      target_id "M111_133"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 308
    source 98
    target 97
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_164"
      target_id "M111_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 309
    source 97
    target 99
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_65"
      target_id "M111_132"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 310
    source 103
    target 102
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_81"
      target_id "M111_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 311
    source 104
    target 103
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_11"
      target_id "M111_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 312
    source 105
    target 104
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_67"
      target_id "M111_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 313
    source 106
    target 105
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_2"
      target_id "M111_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 314
    source 107
    target 105
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M111_131"
      target_id "M111_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 315
    source 105
    target 108
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_67"
      target_id "M111_165"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 316
    source 112
    target 106
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_36"
      target_id "M111_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 317
    source 108
    target 109
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_165"
      target_id "M111_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 318
    source 110
    target 109
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "M111_16"
      target_id "M111_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 319
    source 109
    target 111
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_82"
      target_id "M111_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 320
    source 113
    target 112
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_1"
      target_id "M111_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 321
    source 114
    target 112
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "M111_28"
      target_id "M111_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 322
    source 115
    target 113
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_74"
      target_id "M111_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 323
    source 116
    target 115
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_167"
      target_id "M111_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 324
    source 117
    target 115
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_168"
      target_id "M111_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 325
    source 118
    target 115
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_169"
      target_id "M111_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 326
    source 120
    target 119
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_156"
      target_id "M111_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 327
    source 121
    target 119
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "M111_157"
      target_id "M111_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 328
    source 122
    target 119
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "M111_158"
      target_id "M111_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 329
    source 126
    target 125
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_62"
      target_id "M111_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 330
    source 127
    target 126
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_162"
      target_id "M111_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 331
    source 128
    target 126
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_161"
      target_id "M111_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 332
    source 129
    target 126
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "INHIBITION"
      source_id "M111_142"
      target_id "M111_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 333
    source 132
    target 131
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_181"
      target_id "M111_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 334
    source 133
    target 131
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_145"
      target_id "M111_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 335
    source 134
    target 131
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_144"
      target_id "M111_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 336
    source 135
    target 132
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_40"
      target_id "M111_181"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 337
    source 136
    target 135
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_143"
      target_id "M111_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 338
    source 137
    target 135
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M111_17"
      target_id "M111_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 339
    source 138
    target 137
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_41"
      target_id "M111_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 340
    source 139
    target 138
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_177"
      target_id "M111_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 341
    source 140
    target 138
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_178"
      target_id "M111_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 342
    source 144
    target 142
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_75"
      target_id "M111_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 343
    source 145
    target 144
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_170"
      target_id "M111_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 344
    source 146
    target 144
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_171"
      target_id "M111_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 345
    source 147
    target 144
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_172"
      target_id "M111_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 346
    source 149
    target 148
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_179"
      target_id "M111_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 347
    source 150
    target 148
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_180"
      target_id "M111_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 348
    source 151
    target 148
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_109"
      target_id "M111_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 349
    source 152
    target 148
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_148"
      target_id "M111_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 350
    source 153
    target 149
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_49"
      target_id "M111_179"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 351
    source 154
    target 153
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_151"
      target_id "M111_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 352
    source 155
    target 153
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M111_20"
      target_id "M111_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 353
    source 156
    target 153
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M111_21"
      target_id "M111_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 354
    source 160
    target 155
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_48"
      target_id "M111_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 355
    source 157
    target 156
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_47"
      target_id "M111_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 356
    source 158
    target 157
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_149"
      target_id "M111_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 357
    source 159
    target 157
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_97"
      target_id "M111_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 358
    source 159
    target 160
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_97"
      target_id "M111_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 359
    source 161
    target 160
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_150"
      target_id "M111_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 360
    source 163
    target 162
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "M111_137"
      target_id "M111_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 361
    source 162
    target 164
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_39"
      target_id "M111_138"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 362
    source 165
    target 163
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_52"
      target_id "M111_137"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 363
    source 163
    target 166
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "M111_137"
      target_id "M111_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 364
    source 163
    target 167
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CATALYSIS"
      source_id "M111_137"
      target_id "M111_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 365
    source 170
    target 165
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_153"
      target_id "M111_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 366
    source 171
    target 165
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M111_122"
      target_id "M111_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 367
    source 166
    target 169
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_37"
      target_id "M111_139"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 368
    source 167
    target 168
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "PRODUCTION"
      source_id "M111_38"
      target_id "M111_136"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 369
    source 174
    target 173
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_107"
      target_id "M111_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 370
    source 175
    target 173
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_154"
      target_id "M111_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 371
    source 176
    target 173
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "INHIBITION"
      source_id "M111_115"
      target_id "M111_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 372
    source 177
    target 173
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "INHIBITION"
      source_id "M111_116"
      target_id "M111_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 373
    source 178
    target 173
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "INHIBITION"
      source_id "M111_141"
      target_id "M111_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 374
    source 176
    target 179
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "INHIBITION"
      source_id "M111_115"
      target_id "M111_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 375
    source 177
    target 179
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "INHIBITION"
      source_id "M111_116"
      target_id "M111_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 376
    source 180
    target 179
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_105"
      target_id "M111_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 377
    source 181
    target 179
    cd19dm [
      diagram "C19DMap:PAMP signalling"
      edge_type "CONSPUMPTION"
      source_id "M111_155"
      target_id "M111_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
