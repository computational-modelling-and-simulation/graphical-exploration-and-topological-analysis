# generated with VANTED V2.8.2 at Fri Mar 04 10:04:38 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3"
      hgnc "HGNC_SYMBOL:PYCARD"
      map_id "M122_230"
      name "ASC"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa375"
      uniprot "UNIPROT:Q9ULZ3"
    ]
    graphics [
      x 251.47607022657485
      y 1273.117430481679
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_230"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_62"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re164"
      uniprot "NA"
    ]
    graphics [
      x 431.47607022657473
      y 1532.5572932182372
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_60"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re162"
      uniprot "NA"
    ]
    graphics [
      x 771.6104592494069
      y 1812.8406551402936
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:NLRP3"
      map_id "M122_252"
      name "NLRP3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa468"
      uniprot "UNIPROT:Q96P20"
    ]
    graphics [
      x 307.96738506637075
      y 1638.3815285861933
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_252"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q9ULZ3;urn:miriam:obo.chebi:CHEBI%3A36080;urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3"
      hgnc "HGNC_SYMBOL:NLRP3;HGNC_SYMBOL:PYCARD"
      map_id "M122_31"
      name "NLRP3_space_oligomer:ASC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa81"
      uniprot "UNIPROT:Q9ULZ3;UNIPROT:Q96P20"
    ]
    graphics [
      x 561.6104592494069
      y 1840.8651572750396
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_65"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re172"
      uniprot "NA"
    ]
    graphics [
      x 431.47607022657473
      y 1082.5572932182372
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_61"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re163"
      uniprot "NA"
    ]
    graphics [
      x 1810.4146427238684
      y 2214.6594484469133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M122_231"
      name "CASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa376"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 2124.080281687381
      y 2555.299179124996
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_231"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P29466;urn:miriam:uniprot:Q9ULZ3;urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3;urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:PYCARD;HGNC_SYMBOL:NLRP3;HGNC_SYMBOL:CASP1"
      map_id "M122_29"
      name "NLRP3_space_oligomer:ASC:Caspase1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa78"
      uniprot "UNIPROT:P29466;UNIPROT:Q9ULZ3;UNIPROT:Q96P20"
    ]
    graphics [
      x 1451.4760702265748
      y 1332.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P08311;urn:miriam:ncbigene:1511;urn:miriam:ncbigene:1511;urn:miriam:ec-code:3.4.21.20;urn:miriam:hgnc:2532;urn:miriam:hgnc.symbol:CTSG;urn:miriam:hgnc.symbol:CTSG;urn:miriam:refseq:NM_001911;urn:miriam:ensembl:ENSG00000100448"
      hgnc "HGNC_SYMBOL:CTSG"
      map_id "M122_234"
      name "CTSG"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa381"
      uniprot "UNIPROT:P08311"
    ]
    graphics [
      x 698.8536650413897
      y 798.8329395949797
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_234"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M122_237"
      name "CASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa384"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 1211.4760702265748
      y 1149.0166269831955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_237"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M122_238"
      name "CASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa385"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 505.88713694682804
      y 696.5540586052853
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_238"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M122_235"
      name "CASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa382"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 532.5144998161113
      y 1990.2171093599814
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_235"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M122_236"
      name "CASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa383"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 1471.9539059138592
      y 1512.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_236"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_66"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re173"
      uniprot "NA"
    ]
    graphics [
      x 1357.9673850663708
      y 1722.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P29466;urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292;urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M122_36"
      name "CASP1(120_minus_197):CASP1(317_minus_404)"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa94"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 1027.9673850663708
      y 1682.3785906698167
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_67"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re174"
      uniprot "NA"
    ]
    graphics [
      x 1641.6104592494069
      y 1782.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P29466;urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292;urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M122_35"
      name "Caspase_minus_1_space_Tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa93"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 2467.9673850663708
      y 1690.2835095222708
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_72"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re180"
      uniprot "NA"
    ]
    graphics [
      x 2122.5144998161113
      y 1803.7316608476026
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:3606;urn:miriam:uniprot:Q14116;urn:miriam:hgnc.symbol:IL18"
      hgnc "HGNC_SYMBOL:IL18"
      map_id "M122_247"
      name "proIL_minus_18"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa463"
      uniprot "UNIPROT:Q14116"
    ]
    graphics [
      x 2321.476070226575
      y 1504.4199738205436
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_247"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:3606;urn:miriam:uniprot:Q14116;urn:miriam:hgnc.symbol:IL18"
      hgnc "HGNC_SYMBOL:IL18"
      map_id "M122_249"
      name "IL_minus_18"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa465"
      uniprot "UNIPROT:Q14116"
    ]
    graphics [
      x 877.9673850663706
      y 1583.2421714207787
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_249"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:3606;urn:miriam:uniprot:Q14116;urn:miriam:hgnc.symbol:IL18"
      hgnc "HGNC_SYMBOL:IL18"
      map_id "M122_245"
      name "proIL_minus_18"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa461"
      uniprot "UNIPROT:Q14116"
    ]
    graphics [
      x 2476.6857216474755
      y 2063.025207896084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_245"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_73"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re181"
      uniprot "NA"
    ]
    graphics [
      x 461.47607022657473
      y 1102.7632509475654
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:3606;urn:miriam:uniprot:Q14116;urn:miriam:hgnc.symbol:IL18"
      hgnc "HGNC_SYMBOL:IL18"
      map_id "M122_251"
      name "IL_minus_18"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa467"
      uniprot "UNIPROT:Q14116"
    ]
    graphics [
      x 131.47607022657485
      y 1244.967438350489
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_251"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "PUBMED:25770182;PUBMED:25847972;PUBMED:26331680;PUBMED:29789363;PUBMED:28356568;PUBMED:28741645"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_70"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re177"
      uniprot "NA"
    ]
    graphics [
      x 790.4146427238683
      y 2180.9038740205942
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:NLRP3"
      map_id "M122_223"
      name "NLRP3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa362"
      uniprot "UNIPROT:Q96P20"
    ]
    graphics [
      x 862.5144998161113
      y 1940.7683083415673
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_223"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:Q9H3M7;urn:miriam:hgnc:16952;urn:miriam:ncbigene:10628;urn:miriam:ensembl:ENSG00000265972;urn:miriam:ncbigene:10628;urn:miriam:uniprot:Q9H3M7;urn:miriam:refseq:NM_006472;urn:miriam:hgnc.symbol:TXNIP;urn:miriam:hgnc.symbol:TXNIP;urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:TXNIP;HGNC_SYMBOL:NLRP3"
      map_id "M122_28"
      name "TXNIP:NLRP3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa74"
      uniprot "UNIPROT:Q96P20;UNIPROT:Q9H3M7"
    ]
    graphics [
      x 367.96738506637075
      y 1804.266936905266
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P59637;urn:miriam:ncbigene:1489671;urn:miriam:hgnc.symbol:E"
      hgnc "HGNC_SYMBOL:E"
      map_id "M122_254"
      name "SARS_space_E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa471"
      uniprot "UNIPROT:P59637"
    ]
    graphics [
      x 1239.0311139321143
      y 2142.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_254"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0002221"
      hgnc "NA"
      map_id "M122_267"
      name "PAMPs"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa528"
      uniprot "NA"
    ]
    graphics [
      x 951.4543571843119
      y 1996.6019746441584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_267"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0002221"
      hgnc "NA"
      map_id "M122_266"
      name "DAMPs"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa527"
      uniprot "NA"
    ]
    graphics [
      x 1420.4075582174767
      y 2532.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_266"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:ncbigene:1489669"
      hgnc "NA"
      map_id "M122_253"
      name "SARS_space_Orf3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa469"
      uniprot "UNIPROT:P59632"
    ]
    graphics [
      x 1511.4760702265748
      y 1272.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_253"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A26523"
      hgnc "NA"
      map_id "M122_263"
      name "Reactive_space_Oxygen_space_Species"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa524"
      uniprot "NA"
    ]
    graphics [
      x 1901.4760702265748
      y 1356.2979076205993
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_263"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A46661;urn:miriam:obo.chebi:CHEBI%3A30563;urn:miriam:obo.chebi:CHEBI%3A16336"
      hgnc "NA"
      map_id "M122_258"
      name "NLRP3_space_Elicitor_space_Small_space_Molecules"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa494"
      uniprot "NA"
    ]
    graphics [
      x 1600.4146427238684
      y 2172.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_258"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc:16400;urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:Q96P20;urn:miriam:ensembl:ENSG00000162711;urn:miriam:ncbigene:351;urn:miriam:refseq:NM_004895;urn:miriam:uniprot:P05067;urn:miriam:hgnc.symbol:APP;urn:miriam:uniprot:P09616;urn:miriam:hgnc.symbol:hly;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:APP;HGNC_SYMBOL:hly;HGNC_SYMBOL:NLRP3"
      map_id "M122_259"
      name "NLRP3_space_Elicitor_space_Proteins"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa495"
      uniprot "UNIPROT:Q96P20;UNIPROT:P05067;UNIPROT:P09616"
    ]
    graphics [
      x 1115.6939429587637
      y 2397.2469542692015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_259"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "PUBMED:25770182;PUBMED:28356568"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_74"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re182"
      uniprot "NA"
    ]
    graphics [
      x 1541.4760702265748
      y 1362.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_262"
      name "s782"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa522"
      uniprot "NA"
    ]
    graphics [
      x 2171.476070226575
      y 1140.8495903009339
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_262"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17245"
      hgnc "NA"
      map_id "M122_217"
      name "CO"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa31"
      uniprot "NA"
    ]
    graphics [
      x 941.4760702265747
      y 1454.8275027840566
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_217"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_44"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re11"
      uniprot "NA"
    ]
    graphics [
      x 731.4760702265747
      y 1432.5713166070652
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30413"
      hgnc "NA"
      map_id "M122_194"
      name "Heme"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa21"
      uniprot "NA"
    ]
    graphics [
      x 1562.1817207292956
      y 1512.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_194"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15379"
      hgnc "NA"
      map_id "M122_204"
      name "O2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa24"
      uniprot "NA"
    ]
    graphics [
      x 997.9673850663706
      y 1679.2696849278589
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_204"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16474"
      hgnc "NA"
      map_id "M122_210"
      name "NADPH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa25"
      uniprot "NA"
    ]
    graphics [
      x 971.4760702265747
      y 1442.8472952948819
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_210"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000100292;urn:miriam:hgnc:5013;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:refseq:NM_002133;urn:miriam:uniprot:P09601;urn:miriam:ncbigene:3162;urn:miriam:ncbigene:3162;urn:miriam:ec-code:1.14.14.18"
      hgnc "HGNC_SYMBOL:HMOX1"
      map_id "M122_177"
      name "HMOX1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa168"
      uniprot "UNIPROT:P09601"
    ]
    graphics [
      x 1556.090149807047
      y 432.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_177"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17033"
      hgnc "NA"
      map_id "M122_200"
      name "Biliverdin"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa22"
      uniprot "NA"
    ]
    graphics [
      x 741.6104592494069
      y 1865.224839764994
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_200"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "M122_211"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa26"
      uniprot "NA"
    ]
    graphics [
      x 712.5144998161113
      y 2002.4666947456517
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_211"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29033"
      hgnc "NA"
      map_id "M122_213"
      name "Fe2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa28"
      uniprot "NA"
    ]
    graphics [
      x 1991.4760702265748
      y 1204.8841075166547
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_213"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18009"
      hgnc "NA"
      map_id "M122_212"
      name "NADP_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa27"
      uniprot "NA"
    ]
    graphics [
      x 1067.232625398789
      y 2071.8254692085234
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_212"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_113"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re81"
      uniprot "NA"
    ]
    graphics [
      x 1448.8536650413898
      y 552.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_46"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re111"
      uniprot "NA"
    ]
    graphics [
      x 1301.4760702265748
      y 1108.1356953218692
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "PUBMED:30692038;PUBMED:26794443"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_116"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re88"
      uniprot "NA"
    ]
    graphics [
      x 2302.5144998161113
      y 1887.4005697673038
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_96"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re61"
      uniprot "NA"
    ]
    graphics [
      x 1580.0074341427699
      y 2292.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_95"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re60"
      uniprot "NA"
    ]
    graphics [
      x 1150.4146427238684
      y 2169.0166269831952
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_37"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re100"
      uniprot "NA"
    ]
    graphics [
      x 802.5144998161113
      y 2037.6236763137715
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q9NP59;urn:miriam:obo.chebi:CHEBI%3A28694;urn:miriam:uniprot:Q9BQS7;urn:miriam:ncbigene:30061;urn:miriam:ncbigene:30061;urn:miriam:uniprot:Q9NP59;urn:miriam:uniprot:Q9NP59;urn:miriam:refseq:NM_014585;urn:miriam:hgnc:10909;urn:miriam:ensembl:ENSG00000138449;urn:miriam:hgnc.symbol:SLC40A1;urn:miriam:hgnc.symbol:SLC40A1;urn:miriam:ec-code:1.-.-.-;urn:miriam:ensembl:ENSG00000089472;urn:miriam:uniprot:Q9BQS7;urn:miriam:uniprot:Q9BQS7;urn:miriam:hgnc:4866;urn:miriam:hgnc.symbol:HEPH;urn:miriam:hgnc.symbol:HEPH;urn:miriam:ncbigene:9843;urn:miriam:refseq:NM_138737;urn:miriam:ncbigene:9843;urn:miriam:obo.chebi:CHEBI%3A29036"
      hgnc "HGNC_SYMBOL:SLC40A1;HGNC_SYMBOL:HEPH"
      map_id "M122_13"
      name "SLC40A1:HEPH:Cu2_plus_"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa22"
      uniprot "UNIPROT:Q9NP59;UNIPROT:Q9BQS7"
    ]
    graphics [
      x 667.9673850663706
      y 1589.691377665889
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29033"
      hgnc "NA"
      map_id "M122_150"
      name "Fe2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa135"
      uniprot "NA"
    ]
    graphics [
      x 892.1792674198781
      y 2393.3723708556545
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_150"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_97"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re62"
      uniprot "NA"
    ]
    graphics [
      x 761.4760702265747
      y 1457.1654913580128
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_38"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re101"
      uniprot "NA"
    ]
    graphics [
      x 1144.1474759714647
      y 2652.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "M122_196"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa212"
      uniprot "NA"
    ]
    graphics [
      x 1231.843839019008
      y 2352.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_196"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15379"
      hgnc "NA"
      map_id "M122_197"
      name "O2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa213"
      uniprot "NA"
    ]
    graphics [
      x 1329.0903364967949
      y 2412.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_197"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29034"
      hgnc "NA"
      map_id "M122_153"
      name "Fe3_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa138"
      uniprot "NA"
    ]
    graphics [
      x 2167.9673850663708
      y 1651.2387042684727
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_153"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "M122_195"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa211"
      uniprot "NA"
    ]
    graphics [
      x 1940.0074341427699
      y 2379.4435094575715
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_195"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_39"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re102"
      uniprot "NA"
    ]
    graphics [
      x 1181.4760702265748
      y 1038.8238749182315
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_112"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re79"
      uniprot "NA"
    ]
    graphics [
      x 1717.9673850663708
      y 1559.0814698311804
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A10545"
      hgnc "NA"
      map_id "M122_185"
      name "e_minus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa195"
      uniprot "NA"
    ]
    graphics [
      x 1447.9673850663708
      y 1692.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_185"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q53TN4;urn:miriam:obo.chebi:CHEBI%3A30413;urn:miriam:obo.chebi:CHEBI%3A30413;urn:miriam:ec-code:1.-.-.-;urn:miriam:hgnc.symbol:CYBRD1;urn:miriam:ncbigene:79901;urn:miriam:hgnc.symbol:CYBRD1;urn:miriam:ncbigene:79901;urn:miriam:ensembl:ENSG00000071967;urn:miriam:hgnc:20797;urn:miriam:uniprot:Q53TN4;urn:miriam:uniprot:Q53TN4;urn:miriam:refseq:NM_024843"
      hgnc "HGNC_SYMBOL:CYBRD1"
      map_id "M122_5"
      name "CYBRD1:Heme"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa13"
      uniprot "UNIPROT:Q53TN4"
    ]
    graphics [
      x 1396.6057727547209
      y 1002.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29033"
      hgnc "NA"
      map_id "M122_172"
      name "Fe2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa162"
      uniprot "NA"
    ]
    graphics [
      x 1571.4760702265748
      y 1002.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_172"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P02787;urn:miriam:hgnc.symbol:TF;urn:miriam:ncbigene:7018"
      hgnc "HGNC_SYMBOL:TF"
      map_id "M122_198"
      name "Transferrin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa214"
      uniprot "UNIPROT:P02787"
    ]
    graphics [
      x 907.9673850663706
      y 1549.8026825465765
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_198"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P02787;urn:miriam:obo.chebi:CHEBI%3A29034;urn:miriam:obo.chebi:CHEBI%3A29034;urn:miriam:uniprot:P02787;urn:miriam:hgnc.symbol:TF;urn:miriam:ncbigene:7018"
      hgnc "HGNC_SYMBOL:TF"
      map_id "M122_14"
      name "holoTF"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa23"
      uniprot "UNIPROT:P02787"
    ]
    graphics [
      x 1958.8536650413898
      y 688.373935738091
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_40"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re103"
      uniprot "NA"
    ]
    graphics [
      x 2078.85366504139
      y 564.8606125107739
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc.symbol:TFRC;urn:miriam:hgnc.symbol:TFRC;urn:miriam:uniprot:P02786;urn:miriam:hgnc:11763;urn:miriam:ensembl:ENSG00000072274;urn:miriam:ncbigene:7037;urn:miriam:ncbigene:7037;urn:miriam:refseq:NM_001128148"
      hgnc "HGNC_SYMBOL:TFRC"
      map_id "M122_199"
      name "TFRC"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa217"
      uniprot "UNIPROT:P02786"
    ]
    graphics [
      x 2141.476070226575
      y 1192.799581371932
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_199"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P02787;urn:miriam:uniprot:P02786;urn:miriam:obo.chebi:CHEBI%3A29034;urn:miriam:uniprot:P02787;urn:miriam:hgnc.symbol:TF;urn:miriam:ncbigene:7018;urn:miriam:obo.chebi:CHEBI%3A29034;urn:miriam:hgnc.symbol:TFRC;urn:miriam:hgnc.symbol:TFRC;urn:miriam:uniprot:P02786;urn:miriam:hgnc:11763;urn:miriam:ensembl:ENSG00000072274;urn:miriam:ncbigene:7037;urn:miriam:ncbigene:7037;urn:miriam:refseq:NM_001128148"
      hgnc "HGNC_SYMBOL:TF;HGNC_SYMBOL:TFRC"
      map_id "M122_15"
      name "TFRC:holoTF"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa27"
      uniprot "UNIPROT:P02787;UNIPROT:P02786"
    ]
    graphics [
      x 1658.8536650413898
      y 612.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_41"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re105"
      uniprot "NA"
    ]
    graphics [
      x 1601.4760702265748
      y 1392.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P02787;urn:miriam:uniprot:P02786;urn:miriam:obo.chebi:CHEBI%3A29034;urn:miriam:obo.chebi:CHEBI%3A29034;urn:miriam:uniprot:P02787;urn:miriam:hgnc.symbol:TF;urn:miriam:ncbigene:7018;urn:miriam:hgnc.symbol:TFRC;urn:miriam:hgnc.symbol:TFRC;urn:miriam:uniprot:P02786;urn:miriam:hgnc:11763;urn:miriam:ensembl:ENSG00000072274;urn:miriam:ncbigene:7037;urn:miriam:ncbigene:7037;urn:miriam:refseq:NM_001128148"
      hgnc "HGNC_SYMBOL:TF;HGNC_SYMBOL:TFRC"
      map_id "M122_16"
      name "TFRC:holoTF"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa29"
      uniprot "UNIPROT:P02787;UNIPROT:P02786"
    ]
    graphics [
      x 2002.5144998161113
      y 1962.7090387553942
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_42"
      name "NA"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "re106"
      uniprot "NA"
    ]
    graphics [
      x 1820.0074341427699
      y 2304.6594484469133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29034"
      hgnc "NA"
      map_id "M122_201"
      name "Fe3_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa229"
      uniprot "NA"
    ]
    graphics [
      x 1450.4146427238684
      y 2292.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_201"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P02787;urn:miriam:uniprot:P02786;urn:miriam:hgnc.symbol:TFRC;urn:miriam:hgnc.symbol:TFRC;urn:miriam:uniprot:P02786;urn:miriam:hgnc:11763;urn:miriam:ensembl:ENSG00000072274;urn:miriam:ncbigene:7037;urn:miriam:ncbigene:7037;urn:miriam:refseq:NM_001128148;urn:miriam:uniprot:P02787;urn:miriam:hgnc.symbol:TF;urn:miriam:ncbigene:7018"
      hgnc "HGNC_SYMBOL:TFRC;HGNC_SYMBOL:TF"
      map_id "M122_18"
      name "TFRC:TF"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa30"
      uniprot "UNIPROT:P02787;UNIPROT:P02786"
    ]
    graphics [
      x 1124.15756169643
      y 2254.336442634987
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re112"
      uniprot "NA"
    ]
    graphics [
      x 862.5144998161113
      y 1880.7683083415673
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P02787;urn:miriam:uniprot:P02786;urn:miriam:hgnc.symbol:TFRC;urn:miriam:hgnc.symbol:TFRC;urn:miriam:uniprot:P02786;urn:miriam:hgnc:11763;urn:miriam:ensembl:ENSG00000072274;urn:miriam:ncbigene:7037;urn:miriam:ncbigene:7037;urn:miriam:refseq:NM_001128148;urn:miriam:uniprot:P02787;urn:miriam:hgnc.symbol:TF;urn:miriam:ncbigene:7018"
      hgnc "HGNC_SYMBOL:TFRC;HGNC_SYMBOL:TF"
      map_id "M122_19"
      name "TFRC:TF"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa31"
      uniprot "UNIPROT:P02787;UNIPROT:P02786"
    ]
    graphics [
      x 1464.0407232757284
      y 1812.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_48"
      name "NA"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "re113"
      uniprot "NA"
    ]
    graphics [
      x 1447.9673850663708
      y 1662.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_43"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re109"
      uniprot "NA"
    ]
    graphics [
      x 967.9673850663706
      y 1719.4280235401561
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A10545"
      hgnc "NA"
      map_id "M122_207"
      name "e_minus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa242"
      uniprot "NA"
    ]
    graphics [
      x 637.9673850663706
      y 1530.670546069104
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_207"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:refseq:NM_018234;urn:miriam:uniprot:Q658P3;urn:miriam:ensembl:ENSG00000115107;urn:miriam:ec-code:1.16.1.-;urn:miriam:hgnc.symbol:STEAP3;urn:miriam:hgnc.symbol:STEAP3;urn:miriam:ncbigene:55240;urn:miriam:ncbigene:55240;urn:miriam:hgnc:24592"
      hgnc "HGNC_SYMBOL:STEAP3"
      map_id "M122_208"
      name "STEAP3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa243"
      uniprot "UNIPROT:Q658P3"
    ]
    graphics [
      x 502.5144998161113
      y 1961.9994816369422
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_208"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29033"
      hgnc "NA"
      map_id "M122_203"
      name "Fe2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa238"
      uniprot "NA"
    ]
    graphics [
      x 1987.9673850663708
      y 1607.606843746084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_203"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_45"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re110"
      uniprot "NA"
    ]
    graphics [
      x 2498.85366504139
      y 891.5822360918069
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q9GZU1;urn:miriam:hgnc:13356;urn:miriam:refseq:NM_020533;urn:miriam:ncbigene:57192;urn:miriam:ncbigene:57192;urn:miriam:hgnc.symbol:MCOLN1;urn:miriam:ensembl:ENSG00000090674;urn:miriam:hgnc.symbol:MCOLN1"
      hgnc "HGNC_SYMBOL:MCOLN1"
      map_id "M122_206"
      name "MCOLN1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa241"
      uniprot "UNIPROT:Q9GZU1"
    ]
    graphics [
      x 2708.85366504139
      y 1047.4402830714218
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_206"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29033"
      hgnc "NA"
      map_id "M122_205"
      name "Fe2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa240"
      uniprot "NA"
    ]
    graphics [
      x 2381.476070226575
      y 1076.9903152695113
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_205"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15379"
      hgnc "NA"
      map_id "M122_168"
      name "O2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa156"
      uniprot "NA"
    ]
    graphics [
      x 968.8536650413897
      y 913.8329489969877
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_168"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "M122_169"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa157"
      uniprot "NA"
    ]
    graphics [
      x 818.8536650413897
      y 1098.2723357350503
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_169"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q9NP59;urn:miriam:obo.chebi:CHEBI%3A29036;urn:miriam:uniprot:P00450;urn:miriam:ncbigene:30061;urn:miriam:ncbigene:30061;urn:miriam:uniprot:Q9NP59;urn:miriam:uniprot:Q9NP59;urn:miriam:refseq:NM_014585;urn:miriam:hgnc:10909;urn:miriam:ensembl:ENSG00000138449;urn:miriam:hgnc.symbol:SLC40A1;urn:miriam:hgnc.symbol:SLC40A1;urn:miriam:obo.chebi:CHEBI%3A29036;urn:miriam:hgnc:2295;urn:miriam:ensembl:ENSG00000047457;urn:miriam:ec-code:1.16.3.1;urn:miriam:hgnc.symbol:CP;urn:miriam:hgnc.symbol:CP;urn:miriam:refseq:NM_000096;urn:miriam:uniprot:P00450;urn:miriam:uniprot:P00450;urn:miriam:ncbigene:1356;urn:miriam:ncbigene:1356"
      hgnc "HGNC_SYMBOL:SLC40A1;HGNC_SYMBOL:CP"
      map_id "M122_4"
      name "SLC40A1:CP:Cu2_plus_"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa12"
      uniprot "UNIPROT:Q9NP59;UNIPROT:P00450"
    ]
    graphics [
      x 1494.0407232757284
      y 1812.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "M122_170"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa158"
      uniprot "NA"
    ]
    graphics [
      x 491.47607022657473
      y 1185.4907777518654
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_170"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_110"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re76"
      uniprot "NA"
    ]
    graphics [
      x 1927.9673850663708
      y 1660.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:30061;urn:miriam:uniprot:Q9NP59;urn:miriam:refseq:NM_014585;urn:miriam:hgnc:10909;urn:miriam:ensembl:ENSG00000138449;urn:miriam:hgnc.symbol:SLC40A1"
      hgnc "HGNC_SYMBOL:SLC40A1"
      map_id "M122_155"
      name "SLC40A1"
      node_subtype "RNA"
      node_type "species"
      org_id "sa140"
      uniprot "UNIPROT:Q9NP59"
    ]
    graphics [
      x 2081.476070226575
      y 1415.718626258103
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_155"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      annotation "PUBMED:30692038"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_98"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re63"
      uniprot "NA"
    ]
    graphics [
      x 1931.4760702265748
      y 1360.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:30061;urn:miriam:uniprot:Q9NP59;urn:miriam:refseq:NM_014585;urn:miriam:hgnc:10909;urn:miriam:ensembl:ENSG00000138449;urn:miriam:hgnc.symbol:SLC40A1"
      hgnc "HGNC_SYMBOL:SLC40A1"
      map_id "M122_154"
      name "SLC40A1"
      node_subtype "GENE"
      node_type "species"
      org_id "sa139"
      uniprot "UNIPROT:Q9NP59"
    ]
    graphics [
      x 1541.4760702265748
      y 1482.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_154"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q16236;urn:miriam:ncbigene:4780;urn:miriam:hgnc.symbol:NFE2L2"
      hgnc "HGNC_SYMBOL:NFE2L2"
      map_id "M122_241"
      name "NRF2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa43"
      uniprot "UNIPROT:Q16236"
    ]
    graphics [
      x 2122.5144998161113
      y 1943.3608844567657
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_241"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      annotation "PUBMED:12198130"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_90"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re51"
      uniprot "NA"
    ]
    graphics [
      x 2386.521593033102
      y 2288.239781923581
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      annotation "PUBMED:30692038"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_107"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re73"
      uniprot "NA"
    ]
    graphics [
      x 2261.476070226575
      y 1236.5080320248062
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      annotation "PUBMED:30692038"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_99"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re65"
      uniprot "NA"
    ]
    graphics [
      x 2170.4146427238684
      y 2166.028859407032
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      annotation "PUBMED:29717933;PUBMED:31827672"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_76"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re20"
      uniprot "NA"
    ]
    graphics [
      x 2827.9673850663708
      y 1545.2436458095376
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      annotation "PUBMED:30692038"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_106"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re72"
      uniprot "NA"
    ]
    graphics [
      x 1421.4760702265748
      y 1182.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      annotation "PUBMED:23766848;PUBMED:30692038"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_103"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re69"
      uniprot "NA"
    ]
    graphics [
      x 1179.0903364967949
      y 2382.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      annotation "PUBMED:30692038"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_100"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re66"
      uniprot "NA"
    ]
    graphics [
      x 2141.476070226575
      y 1300.2820890985126
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:2512;urn:miriam:hgnc.symbol:FTL;urn:miriam:refseq:NM_000146;urn:miriam:ensembl:ENSG00000087086;urn:miriam:uniprot:P02792;urn:miriam:hgnc:3999"
      hgnc "HGNC_SYMBOL:FTL"
      map_id "M122_158"
      name "FTL"
      node_subtype "GENE"
      node_type "species"
      org_id "sa143"
      uniprot "UNIPROT:P02792"
    ]
    graphics [
      x 1597.9673850663708
      y 1632.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_158"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:2512;urn:miriam:hgnc.symbol:FTL;urn:miriam:refseq:NM_000146;urn:miriam:ensembl:ENSG00000087086;urn:miriam:uniprot:P02792;urn:miriam:hgnc:3999"
      hgnc "HGNC_SYMBOL:FTL"
      map_id "M122_160"
      name "FTL"
      node_subtype "RNA"
      node_type "species"
      org_id "sa145"
      uniprot "UNIPROT:P02792"
    ]
    graphics [
      x 1532.112491134918
      y 732.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_160"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_102"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re68"
      uniprot "NA"
    ]
    graphics [
      x 1406.090149807047
      y 462.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P02794;urn:miriam:ncbigene:2512;urn:miriam:ncbigene:2512;urn:miriam:hgnc.symbol:FTL;urn:miriam:refseq:NM_000146;urn:miriam:hgnc.symbol:FTL;urn:miriam:ensembl:ENSG00000087086;urn:miriam:uniprot:P02792;urn:miriam:uniprot:P02792;urn:miriam:hgnc:3999;urn:miriam:ensembl:ENSG00000167996;urn:miriam:ec-code:1.16.3.1;urn:miriam:refseq:NM_002032;urn:miriam:hgnc:3976;urn:miriam:uniprot:P02794;urn:miriam:uniprot:P02794;urn:miriam:hgnc.symbol:FTH1;urn:miriam:hgnc.symbol:FTH1;urn:miriam:ncbigene:2495;urn:miriam:ncbigene:2495"
      hgnc "HGNC_SYMBOL:FTL;HGNC_SYMBOL:FTH1"
      map_id "M122_3"
      name "Ferritin"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa11"
      uniprot "UNIPROT:P02794;UNIPROT:P02792"
    ]
    graphics [
      x 1061.4760702265748
      y 1443.6936536421204
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_101"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re67"
      uniprot "NA"
    ]
    graphics [
      x 991.2861750633202
      y 2241.9577197196427
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      annotation "PUBMED:30692038"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_111"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re78"
      uniprot "NA"
    ]
    graphics [
      x 1292.112491134918
      y 762.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:8031;urn:miriam:refseq:NM_005437;urn:miriam:ncbigene:8031;urn:miriam:uniprot:Q13772;urn:miriam:ensembl:ENSG00000266412;urn:miriam:hgnc:7671;urn:miriam:hgnc.symbol:NCOA4;urn:miriam:hgnc.symbol:NCOA4"
      hgnc "HGNC_SYMBOL:NCOA4"
      map_id "M122_156"
      name "NCOA4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa141"
      uniprot "UNIPROT:Q13772"
    ]
    graphics [
      x 1118.8536650413898
      y 1020.5543133122671
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_156"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_171"
      name "Ferritin"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa161"
      uniprot "NA"
    ]
    graphics [
      x 848.8536650413897
      y 856.0055613274181
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_171"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000167996;urn:miriam:refseq:NM_002032;urn:miriam:hgnc:3976;urn:miriam:uniprot:P02794;urn:miriam:hgnc.symbol:FTH1;urn:miriam:ncbigene:2495"
      hgnc "HGNC_SYMBOL:FTH1"
      map_id "M122_159"
      name "FTH1"
      node_subtype "RNA"
      node_type "species"
      org_id "sa144"
      uniprot "UNIPROT:P02794"
    ]
    graphics [
      x 1732.9931034425704
      y 2565.0702640464583
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_159"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000066926;urn:miriam:refseq:NM_000140;urn:miriam:hgnc.symbol:FECH;urn:miriam:uniprot:P22830;urn:miriam:hgnc:3647;urn:miriam:ncbigene:2235"
      hgnc "HGNC_SYMBOL:FECH"
      map_id "M122_161"
      name "FECH"
      node_subtype "GENE"
      node_type "species"
      org_id "sa146"
      uniprot "UNIPROT:P22830"
    ]
    graphics [
      x 1101.6104592494069
      y 1786.8414134939999
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_161"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000066926;urn:miriam:refseq:NM_000140;urn:miriam:hgnc.symbol:FECH;urn:miriam:uniprot:P22830;urn:miriam:hgnc:3647;urn:miriam:ncbigene:2235"
      hgnc "HGNC_SYMBOL:FECH"
      map_id "M122_162"
      name "FECH"
      node_subtype "RNA"
      node_type "species"
      org_id "sa147"
      uniprot "UNIPROT:P22830"
    ]
    graphics [
      x 1299.0903364967949
      y 2412.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_162"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_104"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re70"
      uniprot "NA"
    ]
    graphics [
      x 1798.1783887978854
      y 2505.3131445153113
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000066926;urn:miriam:ec-code:4.99.1.1;urn:miriam:refseq:NM_000140;urn:miriam:uniprot:P22830;urn:miriam:hgnc.symbol:FECH;urn:miriam:hgnc.symbol:FECH;urn:miriam:hgnc:3647;urn:miriam:ncbigene:2235;urn:miriam:ncbigene:2235"
      hgnc "HGNC_SYMBOL:FECH"
      map_id "M122_268"
      name "FECH"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa53"
      uniprot "UNIPROT:P22830"
    ]
    graphics [
      x 1870.4146427238684
      y 2260.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_268"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_80"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re28"
      uniprot "NA"
    ]
    graphics [
      x 2587.9673850663708
      y 1769.921259165732
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15430"
      hgnc "NA"
      map_id "M122_269"
      name "PRIN9"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa54"
      uniprot "NA"
    ]
    graphics [
      x 2201.476070226575
      y 1140.5867359968995
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_269"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29033"
      hgnc "NA"
      map_id "M122_270"
      name "Fe2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa55"
      uniprot "NA"
    ]
    graphics [
      x 2621.476070226575
      y 1441.7464025950017
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_270"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A27889"
      hgnc "NA"
      map_id "M122_271"
      name "Pb2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa56"
      uniprot "NA"
    ]
    graphics [
      x 2321.476070226575
      y 1534.4199738205436
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_271"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30413"
      hgnc "NA"
      map_id "M122_202"
      name "Heme"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa23"
      uniprot "NA"
    ]
    graphics [
      x 2302.5144998161113
      y 2078.67202013781
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_202"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "M122_272"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa57"
      uniprot "NA"
    ]
    graphics [
      x 2021.4760702265748
      y 1252.73118595318
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_272"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      annotation "PUBMED:25446301"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_81"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re32"
      uniprot "NA"
    ]
    graphics [
      x 1987.9673850663708
      y 1547.606843746084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_50"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re12"
      uniprot "NA"
    ]
    graphics [
      x 1672.5144998161113
      y 1908.1917700814608
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc.symbol:FLVCR1;urn:miriam:uniprot:Q9Y5Y0;urn:miriam:ncbigene:28982"
      hgnc "HGNC_SYMBOL:FLVCR1"
      map_id "M122_216"
      name "FLVCR1_minus_2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa30"
      uniprot "UNIPROT:Q9Y5Y0"
    ]
    graphics [
      x 1991.4760702265748
      y 1427.606843746084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_216"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57292"
      hgnc "NA"
      map_id "M122_274"
      name "SUCC_minus_CoA"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa65"
      uniprot "NA"
    ]
    graphics [
      x 2501.476070226575
      y 1446.0524092004684
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_274"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57305"
      hgnc "NA"
      map_id "M122_276"
      name "Gly"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa68"
      uniprot "NA"
    ]
    graphics [
      x 1691.4760702265748
      y 1299.571063044361
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_276"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "M122_275"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa67"
      uniprot "NA"
    ]
    graphics [
      x 1901.4760702265748
      y 1450.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_275"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P22557;urn:miriam:uniprot:P13196;urn:miriam:hgnc.symbol:ALAS1;urn:miriam:hgnc.symbol:ALAS1;urn:miriam:ec-code:2.3.1.37;urn:miriam:ncbigene:211;urn:miriam:ensembl:ENSG00000023330;urn:miriam:ncbigene:211;urn:miriam:refseq:NM_000688;urn:miriam:uniprot:P13196;urn:miriam:uniprot:P13196;urn:miriam:hgnc:396;urn:miriam:hgnc.symbol:ALAS2;urn:miriam:hgnc.symbol:ALAS2;urn:miriam:ec-code:2.3.1.37;urn:miriam:ensembl:ENSG00000158578;urn:miriam:uniprot:P22557;urn:miriam:uniprot:P22557;urn:miriam:refseq:NM_000032;urn:miriam:ncbigene:212;urn:miriam:ncbigene:212;urn:miriam:hgnc:397"
      hgnc "HGNC_SYMBOL:ALAS1;HGNC_SYMBOL:ALAS2"
      map_id "M122_22"
      name "ALAS1:ALAS2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa5"
      uniprot "UNIPROT:P22557;UNIPROT:P13196"
    ]
    graphics [
      x 2557.9673850663708
      y 1516.8855856306147
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A50385"
      hgnc "NA"
      map_id "M122_189"
      name "Panhematin"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa20"
      uniprot "NA"
    ]
    graphics [
      x 2441.476070226575
      y 1282.3409576730342
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_189"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A356416"
      hgnc "NA"
      map_id "M122_277"
      name "dALA"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa69"
      uniprot "NA"
    ]
    graphics [
      x 551.4760702265747
      y 1189.4834151171974
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_277"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15346"
      hgnc "NA"
      map_id "M122_279"
      name "CoA_minus_SH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa71"
      uniprot "NA"
    ]
    graphics [
      x 2332.5144998161113
      y 1887.0529021005457
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_279"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16526"
      hgnc "NA"
      map_id "M122_278"
      name "CO2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa70"
      uniprot "NA"
    ]
    graphics [
      x 1417.9673850663708
      y 1632.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_278"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_91"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "re56"
      uniprot "NA"
    ]
    graphics [
      x 521.4760702265747
      y 1505.261006004971
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A356416"
      hgnc "NA"
      map_id "M122_280"
      name "dALA"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa72"
      uniprot "NA"
    ]
    graphics [
      x 928.6089070563612
      y 2457.124444056284
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_280"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_84"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re42"
      uniprot "NA"
    ]
    graphics [
      x 1204.3528008800558
      y 2652.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29105;urn:miriam:uniprot:P13716;urn:miriam:refseq:NM_001003945;urn:miriam:ec-code:4.2.1.24;urn:miriam:ensembl:ENSG00000148218;urn:miriam:hgnc:395;urn:miriam:ncbigene:210;urn:miriam:ncbigene:210;urn:miriam:hgnc.symbol:ALAD;urn:miriam:uniprot:P13716;urn:miriam:uniprot:P13716;urn:miriam:hgnc.symbol:ALAD;urn:miriam:obo.chebi:CHEBI%3A29105"
      hgnc "HGNC_SYMBOL:ALAD"
      map_id "M122_6"
      name "ALAD:Zn2_plus_"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa14"
      uniprot "UNIPROT:P13716"
    ]
    graphics [
      x 1672.5144998161113
      y 2043.3443958949754
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A58126"
      hgnc "NA"
      map_id "M122_127"
      name "PBG"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa101"
      uniprot "NA"
    ]
    graphics [
      x 1571.4760702265748
      y 1452.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_127"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "M122_129"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa104"
      uniprot "NA"
    ]
    graphics [
      x 1435.2508552528402
      y 2562.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_129"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "M122_128"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa103"
      uniprot "NA"
    ]
    graphics [
      x 889.3019167023177
      y 2463.1100032828062
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_85"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re43"
      uniprot "NA"
    ]
    graphics [
      x 2527.9673850663708
      y 1544.3931775949325
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "M122_131"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa106"
      uniprot "NA"
    ]
    graphics [
      x 2171.476070226575
      y 1428.7342851305455
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_131"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P08397;urn:miriam:obo.chebi:CHEBI%3A36319;urn:miriam:hgnc:4982;urn:miriam:uniprot:P08397;urn:miriam:uniprot:P08397;urn:miriam:ec-code:2.5.1.61;urn:miriam:hgnc.symbol:HMBS;urn:miriam:refseq:NM_000190;urn:miriam:ensembl:ENSG00000256269;urn:miriam:hgnc.symbol:HMBS;urn:miriam:ncbigene:3145;urn:miriam:ncbigene:3145"
      hgnc "HGNC_SYMBOL:HMBS"
      map_id "M122_7"
      name "HMBS:DIPY"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa15"
      uniprot "UNIPROT:P08397"
    ]
    graphics [
      x 2197.9673850663708
      y 1609.1063115500313
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57845"
      hgnc "NA"
      map_id "M122_130"
      name "HMBL"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa105"
      uniprot "NA"
    ]
    graphics [
      x 1792.5144998161113
      y 1947.1275183490286
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_130"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A28938"
      hgnc "NA"
      map_id "M122_132"
      name "NH4_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa107"
      uniprot "NA"
    ]
    graphics [
      x 2171.476070226575
      y 1177.799644405166
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_86"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re44"
      uniprot "NA"
    ]
    graphics [
      x 907.9673850663706
      y 1688.944364236203
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:refseq:NM_000375;urn:miriam:hgnc.symbol:UROS;urn:miriam:hgnc.symbol:UROS;urn:miriam:ensembl:ENSG00000188690;urn:miriam:ncbigene:7390;urn:miriam:ncbigene:7390;urn:miriam:uniprot:P10746;urn:miriam:hgnc:12592;urn:miriam:ec-code:4.2.1.75"
      hgnc "HGNC_SYMBOL:UROS"
      map_id "M122_134"
      name "UROS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa112"
      uniprot "UNIPROT:P10746"
    ]
    graphics [
      x 1132.5144998161113
      y 1947.7497955054962
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15437"
      hgnc "NA"
      map_id "M122_133"
      name "URO3"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa111"
      uniprot "NA"
    ]
    graphics [
      x 1663.75410768239
      y 1529.0814698311804
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_133"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_87"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re45"
      uniprot "NA"
    ]
    graphics [
      x 2651.476070226575
      y 1194.167455031551
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P06132;urn:miriam:refseq:NM_000374;urn:miriam:ensembl:ENSG00000126088;urn:miriam:hgnc.symbol:UROD;urn:miriam:hgnc.symbol:UROD;urn:miriam:ncbigene:7389;urn:miriam:ncbigene:7389;urn:miriam:ec-code:4.1.1.37;urn:miriam:hgnc:12591"
      hgnc "HGNC_SYMBOL:UROD"
      map_id "M122_136"
      name "UROD"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa114"
      uniprot "UNIPROT:P06132"
    ]
    graphics [
      x 2591.476070226575
      y 1291.2512245739242
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15439"
      hgnc "NA"
      map_id "M122_135"
      name "COPRO3"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa113"
      uniprot "NA"
    ]
    graphics [
      x 2501.476070226575
      y 1416.0524092004684
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_92"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "re57"
      uniprot "NA"
    ]
    graphics [
      x 2602.5144998161113
      y 1874.73668366986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15439"
      hgnc "NA"
      map_id "M122_137"
      name "COPRO3"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa115"
      uniprot "NA"
    ]
    graphics [
      x 2471.476070226575
      y 1482.474739435631
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_137"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_88"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re48"
      uniprot "NA"
    ]
    graphics [
      x 1301.4760702265748
      y 1138.1356953218692
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15379"
      hgnc "NA"
      map_id "M122_141"
      name "O2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa121"
      uniprot "NA"
    ]
    graphics [
      x 1601.4760702265748
      y 1002.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_141"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000080819;urn:miriam:hgnc:2321;urn:miriam:refseq:NM_000097;urn:miriam:hgnc.symbol:CPOX;urn:miriam:hgnc.symbol:CPOX;urn:miriam:ec-code:1.3.3.3;urn:miriam:ncbigene:1371;urn:miriam:ncbigene:1371;urn:miriam:uniprot:P36551"
      hgnc "HGNC_SYMBOL:CPOX"
      map_id "M122_140"
      name "CPOX"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa118"
      uniprot "UNIPROT:P36551"
    ]
    graphics [
      x 728.8536650413897
      y 755.2758499779999
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_140"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15435"
      hgnc "NA"
      map_id "M122_138"
      name "PPGEN9"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa116"
      uniprot "NA"
    ]
    graphics [
      x 2141.476070226575
      y 1263.4519686837696
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_138"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16526"
      hgnc "NA"
      map_id "M122_142"
      name "CO2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa122"
      uniprot "NA"
    ]
    graphics [
      x 667.9673850663706
      y 1679.691377665889
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_142"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16240"
      hgnc "NA"
      map_id "M122_143"
      name "H2O2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa123"
      uniprot "NA"
    ]
    graphics [
      x 1417.9673850663708
      y 1602.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_143"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_89"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re49"
      uniprot "NA"
    ]
    graphics [
      x 2321.476070226575
      y 1307.1308867541973
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15379"
      hgnc "NA"
      map_id "M122_144"
      name "O2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa124"
      uniprot "NA"
    ]
    graphics [
      x 1687.9673850663708
      y 1619.0814698311804
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_144"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P50336;urn:miriam:obo.chebi:CHEBI%3A16238;urn:miriam:obo.chebi:CHEBI%3A16238"
      hgnc "NA"
      map_id "M122_8"
      name "PPO:FAD"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa17"
      uniprot "UNIPROT:P50336"
    ]
    graphics [
      x 2711.476070226575
      y 1333.639974456575
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15430"
      hgnc "NA"
      map_id "M122_139"
      name "PRIN9"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa117"
      uniprot "NA"
    ]
    graphics [
      x 2621.476070226575
      y 1125.461916952124
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_139"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16240"
      hgnc "NA"
      map_id "M122_145"
      name "H2O2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa125"
      uniprot "NA"
    ]
    graphics [
      x 2741.476070226575
      y 1218.711321929379
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_145"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_93"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "re58"
      uniprot "NA"
    ]
    graphics [
      x 1838.8536650413898
      y 926.3123323415327
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:644;urn:miriam:hgnc.symbol:BLVRA;urn:miriam:refseq:NM_000712;urn:miriam:uniprot:P53004;urn:miriam:ensembl:ENSG00000106605;urn:miriam:hgnc:1062"
      hgnc "HGNC_SYMBOL:BLVRA"
      map_id "M122_163"
      name "BLVRA"
      node_subtype "GENE"
      node_type "species"
      org_id "sa148"
      uniprot "UNIPROT:P53004"
    ]
    graphics [
      x 2081.476070226575
      y 1221.7724443093296
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_163"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:644;urn:miriam:hgnc.symbol:BLVRA;urn:miriam:refseq:NM_000712;urn:miriam:uniprot:P53004;urn:miriam:ensembl:ENSG00000106605;urn:miriam:hgnc:1062"
      hgnc "HGNC_SYMBOL:BLVRA"
      map_id "M122_165"
      name "BLVRA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa150"
      uniprot "UNIPROT:P53004"
    ]
    graphics [
      x 1661.4760702265748
      y 1487.5725709100786
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_165"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_109"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re75"
      uniprot "NA"
    ]
    graphics [
      x 2452.5144998161113
      y 1873.8214117411178
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 167
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29805;urn:miriam:uniprot:P53004;urn:miriam:obo.chebi:CHEBI%3A29105;urn:miriam:ncbigene:644;urn:miriam:ncbigene:644;urn:miriam:ec-code:1.3.1.24;urn:miriam:hgnc.symbol:BLVRA;urn:miriam:hgnc.symbol:BLVRA;urn:miriam:refseq:NM_000712;urn:miriam:uniprot:P53004;urn:miriam:uniprot:P53004;urn:miriam:ensembl:ENSG00000106605;urn:miriam:hgnc:1062"
      hgnc "HGNC_SYMBOL:BLVRA"
      map_id "M122_12"
      name "BLVRA:Zn2_plus_"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa21"
      uniprot "UNIPROT:P53004"
    ]
    graphics [
      x 2347.9673850663708
      y 1741.500231752103
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 168
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_55"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re15"
      uniprot "NA"
    ]
    graphics [
      x 1072.5144998161113
      y 1882.29086407284
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 169
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16474"
      hgnc "NA"
      map_id "M122_219"
      name "NADPH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa33"
      uniprot "NA"
    ]
    graphics [
      x 997.9673850663706
      y 1649.2696849278589
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_219"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 170
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc:1063;urn:miriam:uniprot:P30043;urn:miriam:ec-code:1.5.1.30;urn:miriam:hgnc.symbol:BLVRB;urn:miriam:hgnc.symbol:BLVRB;urn:miriam:ec-code:1.3.1.24;urn:miriam:ncbigene:645;urn:miriam:ncbigene:645;urn:miriam:refseq:NM_000713;urn:miriam:ensembl:ENSG00000090013"
      hgnc "HGNC_SYMBOL:BLVRB"
      map_id "M122_221"
      name "BLVRB"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa36"
      uniprot "UNIPROT:P30043"
    ]
    graphics [
      x 1958.8536650413898
      y 902.6254878021133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_221"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 171
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16990"
      hgnc "NA"
      map_id "M122_218"
      name "Bilirubin"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa32"
      uniprot "NA"
    ]
    graphics [
      x 1365.2810327361162
      y 2442.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_218"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 172
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18009"
      hgnc "NA"
      map_id "M122_220"
      name "NADP_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa34"
      uniprot "NA"
    ]
    graphics [
      x 681.6104592494069
      y 1876.0041448208015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_220"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 173
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_64"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re17"
      uniprot "NA"
    ]
    graphics [
      x 1522.5144998161113
      y 2052.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 174
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P02768;urn:miriam:hgnc.symbol:ABCC1;urn:miriam:ensembl:ENSG00000103222;urn:miriam:hgnc.symbol:ABCC1;urn:miriam:ncbigene:213;urn:miriam:ncbigene:4363;urn:miriam:ncbigene:4363;urn:miriam:uniprot:P33527;urn:miriam:uniprot:P33527;urn:miriam:ec-code:7.6.2.2;urn:miriam:ec-code:7.6.2.3;urn:miriam:refseq:NM_004996;urn:miriam:hgnc:51;urn:miriam:hgnc.symbol:ALB"
      hgnc "HGNC_SYMBOL:ABCC1;HGNC_SYMBOL:ALB"
      map_id "M122_239"
      name "ABCC1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa39"
      uniprot "UNIPROT:P02768;UNIPROT:P33527"
    ]
    graphics [
      x 1990.4146427238684
      y 2147.606843746084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_239"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 175
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16990"
      hgnc "NA"
      map_id "M122_233"
      name "Bilirubin"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa38"
      uniprot "NA"
    ]
    graphics [
      x 1702.5144998161113
      y 2015.5046666730118
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_233"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 176
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_71"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re18"
      uniprot "NA"
    ]
    graphics [
      x 1400.0074341427699
      y 2412.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 177
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P02768;urn:miriam:ncbigene:213;urn:miriam:ensembl:ENSG00000163631;urn:miriam:ncbigene:213;urn:miriam:hgnc:399;urn:miriam:hgnc.symbol:ALB;urn:miriam:refseq:NM_000477;urn:miriam:hgnc.symbol:ALB"
      hgnc "HGNC_SYMBOL:ALB"
      map_id "M122_240"
      name "ALB"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa40"
      uniprot "UNIPROT:P02768"
    ]
    graphics [
      x 1267.9673850663708
      y 1572.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_240"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 178
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P02768;urn:miriam:obo.chebi:CHEBI%3A16990;urn:miriam:uniprot:P02768;urn:miriam:ncbigene:213;urn:miriam:ensembl:ENSG00000163631;urn:miriam:ncbigene:213;urn:miriam:hgnc:399;urn:miriam:hgnc.symbol:ALB;urn:miriam:refseq:NM_000477;urn:miriam:hgnc.symbol:ALB;urn:miriam:obo.chebi:CHEBI%3A16990"
      hgnc "HGNC_SYMBOL:ALB"
      map_id "M122_17"
      name "ALB_slash_BIL"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa3"
      uniprot "UNIPROT:P02768"
    ]
    graphics [
      x 1850.0074341427699
      y 2320.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 179
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_118"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re90"
      uniprot "NA"
    ]
    graphics [
      x 2707.9673850663708
      y 1660.357868060668
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 180
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P02768;urn:miriam:hgnc.symbol:ABCC1;urn:miriam:ensembl:ENSG00000103222;urn:miriam:hgnc.symbol:ABCC1;urn:miriam:ncbigene:213;urn:miriam:ncbigene:4363;urn:miriam:ncbigene:4363;urn:miriam:uniprot:P33527;urn:miriam:uniprot:P33527;urn:miriam:ec-code:7.6.2.2;urn:miriam:ec-code:7.6.2.3;urn:miriam:refseq:NM_004996;urn:miriam:hgnc:51;urn:miriam:hgnc.symbol:ALB"
      hgnc "HGNC_SYMBOL:ABCC1;HGNC_SYMBOL:ALB"
      map_id "M122_181"
      name "ABCC1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa172"
      uniprot "UNIPROT:P02768;UNIPROT:P33527"
    ]
    graphics [
      x 2947.9673850663708
      y 1585.3307515624979
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_181"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 181
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:taxonomy:2697049"
      hgnc "NA"
      map_id "M122_180"
      name "ORF9c"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa171"
      uniprot "NA"
    ]
    graphics [
      x 2211.6104592494066
      y 1790.5567968186347
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_180"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 182
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_108"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re74"
      uniprot "NA"
    ]
    graphics [
      x 1466.5256443239937
      y 582.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 183
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc:1063;urn:miriam:uniprot:P30043;urn:miriam:hgnc.symbol:BLVRB;urn:miriam:ncbigene:645;urn:miriam:refseq:NM_000713;urn:miriam:ensembl:ENSG00000090013"
      hgnc "HGNC_SYMBOL:BLVRB"
      map_id "M122_166"
      name "BLVRB"
      node_subtype "RNA"
      node_type "species"
      org_id "sa151"
      uniprot "UNIPROT:P30043"
    ]
    graphics [
      x 1898.8536650413898
      y 669.9135480338199
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_166"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 184
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:O75444;urn:miriam:ncbigene:4094;urn:miriam:ncbigene:4094;urn:miriam:hgnc:6776;urn:miriam:refseq:NM_001031804;urn:miriam:hgnc.symbol:MAF;urn:miriam:hgnc.symbol:MAF;urn:miriam:ensembl:ENSG00000178573"
      hgnc "HGNC_SYMBOL:MAF"
      map_id "M122_260"
      name "MAF"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa5"
      uniprot "UNIPROT:O75444"
    ]
    graphics [
      x 2497.9673850663708
      y 1758.8975095895871
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_260"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 185
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q16236;urn:miriam:ncbigene:4780;urn:miriam:hgnc.symbol:NFE2L2;urn:miriam:uniprot:O75444;urn:miriam:ncbigene:4094;urn:miriam:ncbigene:4094;urn:miriam:hgnc:6776;urn:miriam:refseq:NM_001031804;urn:miriam:hgnc.symbol:MAF;urn:miriam:hgnc.symbol:MAF;urn:miriam:ensembl:ENSG00000178573"
      hgnc "HGNC_SYMBOL:NFE2L2;HGNC_SYMBOL:MAF"
      map_id "M122_10"
      name "Nrf2_slash_Maf"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa2"
      uniprot "UNIPROT:Q16236;UNIPROT:O75444"
    ]
    graphics [
      x 2771.476070226575
      y 1160.1739336230257
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 186
    zlevel -1

    cd19dm [
      annotation "PUBMED:30692038;PUBMED:29717933;PUBMED:31827672;PUBMED:10473555"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_79"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re25"
      uniprot "NA"
    ]
    graphics [
      x 2591.476070226575
      y 1189.6268049430425
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 187
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000100292;urn:miriam:hgnc:5013;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:refseq:NM_002133;urn:miriam:ncbigene:3162;urn:miriam:uniprot:P09601"
      hgnc "HGNC_SYMBOL:HMOX1"
      map_id "M122_126"
      name "HMOX1"
      node_subtype "GENE"
      node_type "species"
      org_id "sa10"
      uniprot "UNIPROT:P09601"
    ]
    graphics [
      x 1931.4760702265748
      y 1082.6254878021132
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 188
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:O75444;urn:miriam:ncbigene:4094;urn:miriam:ncbigene:4094;urn:miriam:hgnc:6776;urn:miriam:refseq:NM_001031804;urn:miriam:hgnc.symbol:MAF;urn:miriam:hgnc.symbol:MAF;urn:miriam:ensembl:ENSG00000178573;urn:miriam:hgnc.symbol:BACH1;urn:miriam:hgnc.symbol:BACH1;urn:miriam:hgnc:935;urn:miriam:refseq:NM_206866;urn:miriam:uniprot:O14867;urn:miriam:ncbigene:571;urn:miriam:ensembl:ENSG00000156273;urn:miriam:ncbigene:571"
      hgnc "HGNC_SYMBOL:MAF;HGNC_SYMBOL:BACH1"
      map_id "M122_21"
      name "BACH1_slash_Maf"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa4"
      uniprot "UNIPROT:O75444;UNIPROT:O14867"
    ]
    graphics [
      x 2408.85366504139
      y 711.5175868173752
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 189
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000100292;urn:miriam:hgnc:5013;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:refseq:NM_002133;urn:miriam:ncbigene:3162;urn:miriam:uniprot:P09601"
      hgnc "HGNC_SYMBOL:HMOX1"
      map_id "M122_261"
      name "HMOX1"
      node_subtype "RNA"
      node_type "species"
      org_id "sa50"
      uniprot "UNIPROT:P09601"
    ]
    graphics [
      x 1178.8536650413898
      y 978.8238749182315
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_261"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 190
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_105"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re71"
      uniprot "NA"
    ]
    graphics [
      x 1121.4760702265748
      y 1248.9296873026296
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 191
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000100292;urn:miriam:hgnc:5013;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:refseq:NM_002133;urn:miriam:uniprot:P09601;urn:miriam:ncbigene:3162;urn:miriam:ncbigene:3162;urn:miriam:ec-code:1.14.14.18"
      hgnc "HGNC_SYMBOL:HMOX1"
      map_id "M122_214"
      name "HMOX1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa29"
      uniprot "UNIPROT:P09601"
    ]
    graphics [
      x 2051.476070226575
      y 1469.1206388522583
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_214"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 192
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_115"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re87"
      uniprot "NA"
    ]
    graphics [
      x 2228.85366504139
      y 846.8607521927382
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 193
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P0DTC3;urn:miriam:ncbigene:43740569;urn:miriam:taxonomy:2697049"
      hgnc "NA"
      map_id "M122_178"
      name "ORF3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa169"
      uniprot "UNIPROT:P0DTC3"
    ]
    graphics [
      x 1718.8536650413898
      y 619.9831351775329
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_178"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 194
    zlevel -1

    cd19dm [
      annotation "PUBMED:28082120"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_77"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re21"
      uniprot "NA"
    ]
    graphics [
      x 2591.476070226575
      y 1321.2512245739242
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 195
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc.symbol:BACH1;urn:miriam:hgnc.symbol:BACH1;urn:miriam:hgnc:935;urn:miriam:refseq:NM_206866;urn:miriam:uniprot:O14867;urn:miriam:ncbigene:571;urn:miriam:ensembl:ENSG00000156273;urn:miriam:ncbigene:571"
      hgnc "HGNC_SYMBOL:BACH1"
      map_id "M122_242"
      name "BACH1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa44"
      uniprot "UNIPROT:O14867"
    ]
    graphics [
      x 2471.476070226575
      y 1326.5248251244564
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_242"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 196
    zlevel -1

    cd19dm [
      annotation "PUBMED:21982894;PUBMED:28082120"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_78"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re24"
      uniprot "NA"
    ]
    graphics [
      x 1868.8536650413898
      y 774.8359718006143
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 197
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc.symbol:BACH1;urn:miriam:hgnc:935;urn:miriam:refseq:NM_206866;urn:miriam:uniprot:O14867;urn:miriam:ncbigene:571;urn:miriam:ensembl:ENSG00000156273"
      hgnc "HGNC_SYMBOL:BACH1"
      map_id "M122_256"
      name "BACH1"
      node_subtype "RNA"
      node_type "species"
      org_id "sa49"
      uniprot "UNIPROT:O14867"
    ]
    graphics [
      x 2021.4760702265748
      y 1060.9809222758117
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_256"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 198
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:406947"
      hgnc "NA"
      map_id "M122_255"
      name "miRNA_minus_155"
      node_subtype "RNA"
      node_type "species"
      org_id "sa48"
      uniprot "NA"
    ]
    graphics [
      x 1028.8536650413898
      y 861.5374917954795
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_255"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 199
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000167996;urn:miriam:refseq:NM_002032;urn:miriam:hgnc:3976;urn:miriam:uniprot:P02794;urn:miriam:hgnc.symbol:FTH1;urn:miriam:ncbigene:2495"
      hgnc "HGNC_SYMBOL:FTH1"
      map_id "M122_157"
      name "FTH1"
      node_subtype "GENE"
      node_type "species"
      org_id "sa142"
      uniprot "UNIPROT:P02794"
    ]
    graphics [
      x 1507.9673850663708
      y 1602.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_157"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 200
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc:1063;urn:miriam:uniprot:P30043;urn:miriam:hgnc.symbol:BLVRB;urn:miriam:ncbigene:645;urn:miriam:refseq:NM_000713;urn:miriam:ensembl:ENSG00000090013"
      hgnc "HGNC_SYMBOL:BLVRB"
      map_id "M122_164"
      name "BLVRB"
      node_subtype "GENE"
      node_type "species"
      org_id "sa149"
      uniprot "UNIPROT:P30043"
    ]
    graphics [
      x 2108.85366504139
      y 720.1099789381244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_164"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 201
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q16236;urn:miriam:ncbigene:4780;urn:miriam:hgnc.symbol:NFE2L2"
      hgnc "HGNC_SYMBOL:NFE2L2"
      map_id "M122_215"
      name "NRF2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa3"
      uniprot "UNIPROT:Q16236"
    ]
    graphics [
      x 1701.4292661560455
      y 2689.502768367194
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_215"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 202
    zlevel -1

    cd19dm [
      annotation "PUBMED:12198130;PUBMED:31692987"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_123"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re96"
      uniprot "NA"
    ]
    graphics [
      x 833.8849036758296
      y 2593.0299559304995
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 203
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q16236;urn:miriam:ncbigene:4780;urn:miriam:hgnc.symbol:NFE2L2"
      hgnc "HGNC_SYMBOL:NFE2L2"
      map_id "M122_273"
      name "NRF2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa6"
      uniprot "UNIPROT:Q16236"
    ]
    graphics [
      x 1465.2508552528402
      y 2532.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_273"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 204
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:PKC;urn:miriam:pubmed:12198130;urn:miriam:interpro:IPR012233"
      hgnc "NA"
      map_id "M122_184"
      name "PKC"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa190"
      uniprot "UNIPROT:PKC"
    ]
    graphics [
      x 1042.5144998161113
      y 2031.7460409926869
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_184"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 205
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_265"
      name "CK2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa526"
      uniprot "NA"
    ]
    graphics [
      x 457.7320695981414
      y 2292.886298375834
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_265"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 206
    zlevel -1

    cd19dm [
      annotation "PUBMED:15572695;PUBMED:15282312;PUBMED:32132672;PUBMED:31692987;PUBMED:20486766"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_82"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re35"
      uniprot "NA"
    ]
    graphics [
      x 1811.4760702265748
      y 1174.8138321341212
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 207
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:9817;urn:miriam:refseq:NM_012289;urn:miriam:ncbigene:9817;urn:miriam:uniprot:Q14145;urn:miriam:hgnc:23177;urn:miriam:ensembl:ENSG00000079999;urn:miriam:hgnc.symbol:KEAP1;urn:miriam:hgnc.symbol:KEAP1"
      hgnc "HGNC_SYMBOL:KEAP1"
      map_id "M122_281"
      name "KEAP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa78"
      uniprot "UNIPROT:Q14145"
    ]
    graphics [
      x 1748.8536650413898
      y 736.8134512202714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_281"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 208
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A76004"
      hgnc "NA"
      map_id "M122_146"
      name "Dimethly_space_fumarate"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa126"
      uniprot "NA"
    ]
    graphics [
      x 2231.476070226575
      y 1037.1432564937434
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_146"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 209
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q16236;urn:miriam:uniprot:Q14145;urn:miriam:uniprot:Q16236;urn:miriam:ncbigene:4780;urn:miriam:hgnc.symbol:NFE2L2;urn:miriam:ncbigene:9817;urn:miriam:refseq:NM_012289;urn:miriam:ncbigene:9817;urn:miriam:uniprot:Q14145;urn:miriam:hgnc:23177;urn:miriam:ensembl:ENSG00000079999;urn:miriam:hgnc.symbol:KEAP1;urn:miriam:hgnc.symbol:KEAP1"
      hgnc "HGNC_SYMBOL:NFE2L2;HGNC_SYMBOL:KEAP1"
      map_id "M122_25"
      name "NRF2:KEAP1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa7"
      uniprot "UNIPROT:Q16236;UNIPROT:Q14145"
    ]
    graphics [
      x 1901.4760702265748
      y 1088.6835654610654
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 210
    zlevel -1

    cd19dm [
      annotation "PUBMED:15572695;PUBMED:31692987;PUBMED:16449638;PUBMED:20486766"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_121"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re94"
      uniprot "NA"
    ]
    graphics [
      x 2032.5144998161113
      y 1910.4619322870956
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 211
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P62877;urn:miriam:uniprot:Q13618;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:hgnc.symbol:CUL3;urn:miriam:ensembl:ENSG00000036257;urn:miriam:uniprot:Q13618;urn:miriam:uniprot:Q13618;urn:miriam:hgnc.symbol:CUL3;urn:miriam:hgnc:2553;urn:miriam:ncbigene:8452;urn:miriam:ncbigene:8452;urn:miriam:refseq:NM_001257197"
      hgnc "HGNC_SYMBOL:RBX1;HGNC_SYMBOL:CUL3"
      map_id "M122_23"
      name "Neddylated_space_CUL3:RBX1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa6"
      uniprot "UNIPROT:P62877;UNIPROT:Q13618;UNIPROT:Q15843"
    ]
    graphics [
      x 1809.5426638225422
      y 2454.6594484469133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 212
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc.symbol:CAND1;urn:miriam:hgnc.symbol:CAND1;urn:miriam:uniprot:Q86VP6;urn:miriam:refseq:NM_018448;urn:miriam:ncbigene:55832;urn:miriam:ncbigene:55832;urn:miriam:hgnc:30688;urn:miriam:ensembl:ENSG00000111530"
      hgnc "HGNC_SYMBOL:CAND1"
      map_id "M122_183"
      name "CAND1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa189"
      uniprot "UNIPROT:Q86VP6"
    ]
    graphics [
      x 1612.5144998161113
      y 2082.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_183"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 213
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P62877;urn:miriam:uniprot:Q16236;urn:miriam:uniprot:Q13618;urn:miriam:uniprot:Q14145;urn:miriam:uniprot:Q15843;urn:miriam:uniprot:Q16236;urn:miriam:ncbigene:4780;urn:miriam:hgnc.symbol:NFE2L2;urn:miriam:ncbigene:9817;urn:miriam:refseq:NM_012289;urn:miriam:ncbigene:9817;urn:miriam:uniprot:Q14145;urn:miriam:hgnc:23177;urn:miriam:ensembl:ENSG00000079999;urn:miriam:hgnc.symbol:KEAP1;urn:miriam:hgnc.symbol:KEAP1;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:hgnc.symbol:CUL3;urn:miriam:ensembl:ENSG00000036257;urn:miriam:uniprot:Q13618;urn:miriam:uniprot:Q13618;urn:miriam:hgnc.symbol:CUL3;urn:miriam:hgnc:2553;urn:miriam:ncbigene:8452;urn:miriam:ncbigene:8452;urn:miriam:refseq:NM_001257197"
      hgnc "HGNC_SYMBOL:NFE2L2;HGNC_SYMBOL:KEAP1;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:CUL3"
      map_id "M122_11"
      name "Ubiquitin_space_Ligase_space_Complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa20"
      uniprot "UNIPROT:P62877;UNIPROT:Q16236;UNIPROT:Q13618;UNIPROT:Q14145;UNIPROT:Q15843"
    ]
    graphics [
      x 847.9673850663706
      y 1633.1690082783348
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 214
    zlevel -1

    cd19dm [
      annotation "PUBMED:15572695;PUBMED:31692987;PUBMED:20486766"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_122"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re95"
      uniprot "NA"
    ]
    graphics [
      x 847.9673850663706
      y 1515.0908463925657
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 215
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:interpro:IPR000608"
      hgnc "NA"
      map_id "M122_282"
      name "E2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa89"
      uniprot "NA"
    ]
    graphics [
      x 367.96738506637075
      y 1715.2725735465146
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_282"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 216
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P62877;urn:miriam:uniprot:Q16236;urn:miriam:uniprot:Q13618;urn:miriam:uniprot:Q14145;urn:miriam:uniprot:Q15843;urn:miriam:interpro:IPR000608;urn:miriam:pubmed:19940261;urn:miriam:uniprot:Q16236;urn:miriam:ncbigene:4780;urn:miriam:hgnc.symbol:NFE2L2;urn:miriam:interpro:IPR000608;urn:miriam:hgnc.symbol:CUL3;urn:miriam:ensembl:ENSG00000036257;urn:miriam:uniprot:Q13618;urn:miriam:uniprot:Q13618;urn:miriam:hgnc.symbol:CUL3;urn:miriam:hgnc:2553;urn:miriam:ncbigene:8452;urn:miriam:ncbigene:8452;urn:miriam:refseq:NM_001257197;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:ncbigene:9817;urn:miriam:refseq:NM_012289;urn:miriam:ncbigene:9817;urn:miriam:uniprot:Q14145;urn:miriam:hgnc:23177;urn:miriam:ensembl:ENSG00000079999;urn:miriam:hgnc.symbol:KEAP1;urn:miriam:hgnc.symbol:KEAP1"
      hgnc "HGNC_SYMBOL:NFE2L2;HGNC_SYMBOL:CUL3;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:KEAP1"
      map_id "M122_30"
      name "Ubiquitin_space_Ligase_space_Complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa8"
      uniprot "UNIPROT:P62877;UNIPROT:Q16236;UNIPROT:Q13618;UNIPROT:Q14145;UNIPROT:Q15843"
    ]
    graphics [
      x 1358.8536650413898
      y 882.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 217
    zlevel -1

    cd19dm [
      annotation "PUBMED:15572695;PUBMED:31692987;PUBMED:20486766"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_83"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re40"
      uniprot "NA"
    ]
    graphics [
      x 1511.4760702265748
      y 1032.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 218
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P62877;urn:miriam:uniprot:Q16236;urn:miriam:uniprot:Q13618;urn:miriam:uniprot:Q14145;urn:miriam:uniprot:P0CG48;urn:miriam:uniprot:Q15843;urn:miriam:interpro:IPR000608;urn:miriam:pubmed:19940261;urn:miriam:interpro:IPR000608;urn:miriam:hgnc.symbol:CUL3;urn:miriam:ensembl:ENSG00000036257;urn:miriam:uniprot:Q13618;urn:miriam:uniprot:Q13618;urn:miriam:hgnc.symbol:CUL3;urn:miriam:hgnc:2553;urn:miriam:ncbigene:8452;urn:miriam:ncbigene:8452;urn:miriam:refseq:NM_001257197;urn:miriam:uniprot:Q16236;urn:miriam:ncbigene:4780;urn:miriam:hgnc.symbol:NFE2L2;urn:miriam:ncbigene:9817;urn:miriam:refseq:NM_012289;urn:miriam:ncbigene:9817;urn:miriam:uniprot:Q14145;urn:miriam:hgnc:23177;urn:miriam:ensembl:ENSG00000079999;urn:miriam:hgnc.symbol:KEAP1;urn:miriam:hgnc.symbol:KEAP1;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387"
      hgnc "HGNC_SYMBOL:CUL3;HGNC_SYMBOL:NFE2L2;HGNC_SYMBOL:KEAP1;HGNC_SYMBOL:RBX1"
      map_id "M122_1"
      name "Ubiquitin_space_Ligase_space_Complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa10"
      uniprot "UNIPROT:P62877;UNIPROT:Q16236;UNIPROT:Q13618;UNIPROT:Q14145;UNIPROT:P0CG48;UNIPROT:Q15843"
    ]
    graphics [
      x 581.4760702265747
      y 1428.9871862993068
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 219
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_49"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re114"
      uniprot "NA"
    ]
    graphics [
      x 760.4146427238683
      y 2174.1961450381555
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 220
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P62877;urn:miriam:uniprot:Q16236;urn:miriam:uniprot:Q13618;urn:miriam:uniprot:Q14145;urn:miriam:uniprot:P0CG48;urn:miriam:uniprot:Q15843;urn:miriam:interpro:IPR000608;urn:miriam:pubmed:19940261;urn:miriam:interpro:IPR000608;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:ncbigene:9817;urn:miriam:refseq:NM_012289;urn:miriam:ncbigene:9817;urn:miriam:uniprot:Q14145;urn:miriam:hgnc:23177;urn:miriam:ensembl:ENSG00000079999;urn:miriam:hgnc.symbol:KEAP1;urn:miriam:hgnc.symbol:KEAP1;urn:miriam:hgnc.symbol:CUL3;urn:miriam:ensembl:ENSG00000036257;urn:miriam:uniprot:Q13618;urn:miriam:uniprot:Q13618;urn:miriam:hgnc.symbol:CUL3;urn:miriam:hgnc:2553;urn:miriam:ncbigene:8452;urn:miriam:ncbigene:8452;urn:miriam:refseq:NM_001257197"
      hgnc "HGNC_SYMBOL:RBX1;HGNC_SYMBOL:KEAP1;HGNC_SYMBOL:CUL3"
      map_id "M122_20"
      name "Ubiquitin_space_Ligase_space_Complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa32"
      uniprot "UNIPROT:P62877;UNIPROT:Q16236;UNIPROT:Q13618;UNIPROT:Q14145;UNIPROT:P0CG48;UNIPROT:Q15843"
    ]
    graphics [
      x 1540.4146427238684
      y 2292.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 221
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q16236;urn:miriam:ncbigene:4780;urn:miriam:hgnc.symbol:NFE2L2"
      hgnc "HGNC_SYMBOL:NFE2L2"
      map_id "M122_209"
      name "NRF2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa249"
      uniprot "UNIPROT:Q16236"
    ]
    graphics [
      x 1102.5144998161113
      y 1939.076155074359
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_209"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 222
    zlevel -1

    cd19dm [
      annotation "PUBMED:15572695;PUBMED:31692987;PUBMED:20486766"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_120"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re93"
      uniprot "NA"
    ]
    graphics [
      x 820.1566501106137
      y 2378.25130003279
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 223
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_182"
      name "NRF2"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa178"
      uniprot "NA"
    ]
    graphics [
      x 600.1640821091252
      y 2411.6451734338757
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_182"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 224
    zlevel -1

    cd19dm [
      annotation "PUBMED:31692987;PUBMED:20486766"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_119"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re92"
      uniprot "NA"
    ]
    graphics [
      x 922.5144998161113
      y 1936.6019746441584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 225
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P62877;urn:miriam:uniprot:Q13618;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:hgnc.symbol:CUL3;urn:miriam:ensembl:ENSG00000036257;urn:miriam:uniprot:Q13618;urn:miriam:uniprot:Q13618;urn:miriam:hgnc.symbol:CUL3;urn:miriam:hgnc:2553;urn:miriam:ncbigene:8452;urn:miriam:ncbigene:8452;urn:miriam:refseq:NM_001257197"
      hgnc "HGNC_SYMBOL:RBX1;HGNC_SYMBOL:CUL3"
      map_id "M122_9"
      name "CUL3:RBX1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa19"
      uniprot "UNIPROT:P62877;UNIPROT:Q13618"
    ]
    graphics [
      x 487.96738506637064
      y 1598.8328798263947
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 226
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15379"
      hgnc "NA"
      map_id "M122_151"
      name "O2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa136"
      uniprot "NA"
    ]
    graphics [
      x 1709.8077024699244
      y 2496.313882011814
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_151"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 227
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "M122_152"
      name "H_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa137"
      uniprot "NA"
    ]
    graphics [
      x 1487.7845707541828
      y 2082.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_152"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 228
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A78619"
      hgnc "NA"
      map_id "M122_167"
      name "Fe(3_plus_)O(OH)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa152"
      uniprot "NA"
    ]
    graphics [
      x 637.9673850663706
      y 1692.1662202339423
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_167"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 229
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_179"
      name "ROS"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa170"
      uniprot "NA"
    ]
    graphics [
      x 1840.4146427238684
      y 2181.214968968279
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_179"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 230
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A26523"
      hgnc "NA"
      map_id "M122_174"
      name "Reactive_space_Oxygen_space_Species"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa165"
      uniprot "NA"
    ]
    graphics [
      x 1701.6104592494069
      y 1754.5562612679125
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_174"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 231
    zlevel -1

    cd19dm [
      annotation "PUBMED:30692038;PUBMED:26794443"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_94"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re59"
      uniprot "NA"
    ]
    graphics [
      x 1550.0074341427699
      y 2382.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 232
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A26208"
      hgnc "NA"
      map_id "M122_148"
      name "Poly_minus_unsaturated_space_fatty_space_acid"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa128"
      uniprot "NA"
    ]
    graphics [
      x 940.4146427238683
      y 2206.6019746441584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_148"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 233
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A61051"
      hgnc "NA"
      map_id "M122_149"
      name "Lipid_space_Peroxide"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa130"
      uniprot "NA"
    ]
    graphics [
      x 1560.1504894156328
      y 1842.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_149"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 234
    zlevel -1

    cd19dm [
      annotation "PUBMED:30692038;PUBMED:26794443"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_117"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re89"
      uniprot "NA"
    ]
    graphics [
      x 1762.5144998161113
      y 1873.8597507391748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 235
    zlevel -1

    cd19dm [
      annotation "PUBMED:30692038;PUBMED:26794443"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_114"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re85"
      uniprot "NA"
    ]
    graphics [
      x 1641.6104592494069
      y 1722.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 236
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000167468;urn:miriam:ncbigene:2879;urn:miriam:ncbigene:2879;urn:miriam:hgnc:4556;urn:miriam:hgnc.symbol:GPX4;urn:miriam:hgnc.symbol:GPX4;urn:miriam:refseq:NM_002085;urn:miriam:uniprot:P36969;urn:miriam:ec-code:1.11.1.12"
      hgnc "HGNC_SYMBOL:GPX4"
      map_id "M122_176"
      name "GPX4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa167"
      uniprot "UNIPROT:P36969"
    ]
    graphics [
      x 1871.4760702265748
      y 1540.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_176"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 237
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A24026"
      hgnc "NA"
      map_id "M122_175"
      name "Lipid_space_alcohol"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa166"
      uniprot "NA"
    ]
    graphics [
      x 1987.9673850663708
      y 1487.606843746084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_175"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 238
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0097707"
      hgnc "NA"
      map_id "M122_147"
      name "Ferroptosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa127"
      uniprot "NA"
    ]
    graphics [
      x 1451.4760702265748
      y 1482.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_147"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 239
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000110911;urn:miriam:hgnc.symbol:SLC11A2;urn:miriam:hgnc.symbol:SLC11A2;urn:miriam:refseq:NM_000617;urn:miriam:uniprot:P49281;urn:miriam:hgnc:10908;urn:miriam:ncbigene:4891;urn:miriam:ncbigene:4891"
      hgnc "HGNC_SYMBOL:SLC11A2"
      map_id "M122_173"
      name "SLC11A2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa164"
      uniprot "UNIPROT:P49281"
    ]
    graphics [
      x 1508.8536650413898
      y 822.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_173"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 240
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_124"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re97"
      uniprot "NA"
    ]
    graphics [
      x 2134.9376042990107
      y 2405.542611957437
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_124"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 241
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_125"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re98"
      uniprot "NA"
    ]
    graphics [
      x 487.96738506637064
      y 1557.0918947654488
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_125"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 242
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "M122_193"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa206"
      uniprot "NA"
    ]
    graphics [
      x 1331.4760702265748
      y 1422.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_193"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 243
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30616"
      hgnc "NA"
      map_id "M122_190"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa203"
      uniprot "NA"
    ]
    graphics [
      x 1181.4760702265748
      y 1317.0237622123032
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_190"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 244
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000118777;urn:miriam:uniprot:Q9UNQ0;urn:miriam:hgnc.symbol:ABCG2;urn:miriam:hgnc.symbol:ABCG2;urn:miriam:hgnc:74;urn:miriam:ncbigene:9429;urn:miriam:ncbigene:9429;urn:miriam:refseq:NM_004827;urn:miriam:ec-code:7.6.2.2"
      hgnc "HGNC_SYMBOL:ABCG2"
      map_id "M122_188"
      name "ABCG2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa198"
      uniprot "UNIPROT:Q9UNQ0"
    ]
    graphics [
      x 1162.5144998161113
      y 1972.7078987280793
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_188"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 245
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30413"
      hgnc "NA"
      map_id "M122_187"
      name "Heme"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa197"
      uniprot "NA"
    ]
    graphics [
      x 1510.4146427238684
      y 2292.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_187"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 246
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "M122_192"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa205"
      uniprot "NA"
    ]
    graphics [
      x 161.47607022657485
      y 1421.0281475497238
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_192"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 247
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A456216"
      hgnc "NA"
      map_id "M122_191"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa204"
      uniprot "NA"
    ]
    graphics [
      x 217.96738506637075
      y 1730.5439669965854
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_191"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 248
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc.symbol:FLVCR1;urn:miriam:uniprot:Q9Y5Y0;urn:miriam:ncbigene:28982"
      hgnc "HGNC_SYMBOL:FLVCR1"
      map_id "M122_186"
      name "FLVCR1_minus_1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa196"
      uniprot "UNIPROT:Q9Y5Y0"
    ]
    graphics [
      x 2339.128176510712
      y 2465.5611580436293
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_186"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 249
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_59"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re157"
      uniprot "NA"
    ]
    graphics [
      x 911.4760702265747
      y 1111.8262641726626
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 250
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc:16952;urn:miriam:ncbigene:10628;urn:miriam:ensembl:ENSG00000265972;urn:miriam:ncbigene:10628;urn:miriam:uniprot:Q9H3M7;urn:miriam:refseq:NM_006472;urn:miriam:hgnc.symbol:TXNIP;urn:miriam:hgnc.symbol:TXNIP"
      hgnc "HGNC_SYMBOL:TXNIP"
      map_id "M122_224"
      name "TXNIP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa363"
      uniprot "UNIPROT:Q9H3M7"
    ]
    graphics [
      x 2081.476070226575
      y 1285.768995492461
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_224"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 251
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_51"
      name "NA"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "re146"
      uniprot "NA"
    ]
    graphics [
      x 2651.476070226575
      y 1427.119032575329
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 252
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_56"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re150"
      uniprot "NA"
    ]
    graphics [
      x 2271.6104592494066
      y 1765.676905472525
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 253
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:7295;urn:miriam:ncbigene:7295;urn:miriam:uniprot:P10599;urn:miriam:hgnc:12435;urn:miriam:ensembl:ENSG00000136810;urn:miriam:refseq:NM_001244938;urn:miriam:hgnc.symbol:TXN;urn:miriam:hgnc.symbol:TXN"
      hgnc "HGNC_SYMBOL:TXN"
      map_id "M122_225"
      name "TXN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa364"
      uniprot "UNIPROT:P10599"
    ]
    graphics [
      x 2617.9673850663708
      y 1608.994925936687
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_225"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 254
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P10599;urn:miriam:uniprot:Q9H3M7;urn:miriam:hgnc:16952;urn:miriam:ncbigene:10628;urn:miriam:ensembl:ENSG00000265972;urn:miriam:ncbigene:10628;urn:miriam:uniprot:Q9H3M7;urn:miriam:refseq:NM_006472;urn:miriam:hgnc.symbol:TXNIP;urn:miriam:hgnc.symbol:TXNIP;urn:miriam:ncbigene:7295;urn:miriam:ncbigene:7295;urn:miriam:uniprot:P10599;urn:miriam:hgnc:12435;urn:miriam:ensembl:ENSG00000136810;urn:miriam:refseq:NM_001244938;urn:miriam:hgnc.symbol:TXN;urn:miriam:hgnc.symbol:TXN"
      hgnc "HGNC_SYMBOL:TXNIP;HGNC_SYMBOL:TXN"
      map_id "M122_24"
      name "Thioredoxin:TXNIP"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa69"
      uniprot "UNIPROT:P10599;UNIPROT:Q9H3M7"
    ]
    graphics [
      x 2212.5144998161113
      y 1990.0174647178208
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 255
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re149"
      uniprot "NA"
    ]
    graphics [
      x 2092.5144998161113
      y 1943.3608844567657
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 256
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A26523"
      hgnc "NA"
      map_id "M122_257"
      name "Reactive_space_Oxygen_space_Species"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa493"
      uniprot "NA"
    ]
    graphics [
      x 1911.6104592494069
      y 1780.814956786337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_257"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 257
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:7295;urn:miriam:ncbigene:7295;urn:miriam:uniprot:P10599;urn:miriam:hgnc:12435;urn:miriam:ensembl:ENSG00000136810;urn:miriam:refseq:NM_001244938;urn:miriam:hgnc.symbol:TXN;urn:miriam:hgnc.symbol:TXN"
      hgnc "HGNC_SYMBOL:TXN"
      map_id "M122_226"
      name "TXN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa366"
      uniprot "UNIPROT:P10599"
    ]
    graphics [
      x 2231.476070226575
      y 1239.6082873047994
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_226"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 258
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_75"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re183"
      uniprot "NA"
    ]
    graphics [
      x 1132.5144998161113
      y 2031.9955575125548
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 259
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:P08238;urn:miriam:uniprot:Q9Y2Z0;urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548;urn:miriam:hgnc:16987;urn:miriam:ncbigene:10910;urn:miriam:ncbigene:10910;urn:miriam:uniprot:Q9Y2Z0;urn:miriam:ensembl:ENSG00000165416;urn:miriam:refseq:NM_001130912;urn:miriam:hgnc.symbol:SUGT1;urn:miriam:hgnc.symbol:SUGT1"
      hgnc "HGNC_SYMBOL:NLRP3;HGNC_SYMBOL:SUGT1"
      map_id "M122_26"
      name "NLRP3:SUGT1:HSP90"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa72"
      uniprot "UNIPROT:Q96P20;UNIPROT:P08238;UNIPROT:Q9Y2Z0"
    ]
    graphics [
      x 1901.4760702265748
      y 1231.4707703304423
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 260
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:P08238;urn:miriam:uniprot:Q9Y2Z0;urn:miriam:ncbigene:3326;urn:miriam:ncbigene:3326;urn:miriam:uniprot:P08238;urn:miriam:hgnc:5258;urn:miriam:refseq:NM_007355;urn:miriam:ensembl:ENSG00000096384;urn:miriam:hgnc.symbol:HSP90AB1;urn:miriam:hgnc.symbol:HSP90AB1;urn:miriam:hgnc:16987;urn:miriam:ncbigene:10910;urn:miriam:ncbigene:10910;urn:miriam:uniprot:Q9Y2Z0;urn:miriam:ensembl:ENSG00000165416;urn:miriam:refseq:NM_001130912;urn:miriam:hgnc.symbol:SUGT1;urn:miriam:hgnc.symbol:SUGT1"
      hgnc "HGNC_SYMBOL:HSP90AB1;HGNC_SYMBOL:SUGT1"
      map_id "M122_27"
      name "SUGT1:HSP90AB1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa73"
      uniprot "UNIPROT:P08238;UNIPROT:Q9Y2Z0"
    ]
    graphics [
      x 1751.4760702265748
      y 1525.9277406352314
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 261
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_57"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re155"
      uniprot "NA"
    ]
    graphics [
      x 2257.9673850663708
      y 1672.0251788532216
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 262
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_58"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re156"
      uniprot "NA"
    ]
    graphics [
      x 2291.476070226575
      y 1107.620335179428
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 263
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q96P20;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:NLRP3"
      map_id "M122_264"
      name "NLRP3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa525"
      uniprot "UNIPROT:Q96P20"
    ]
    graphics [
      x 1372.5144998161113
      y 1842.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_264"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 264
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_53"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re148"
      uniprot "NA"
    ]
    graphics [
      x 640.4146427238683
      y 2149.1677940838536
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 265
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:uniprot:Q96P20;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:NLRP3"
      map_id "M122_227"
      name "NLRP3"
      node_subtype "RNA"
      node_type "species"
      org_id "sa367"
      uniprot "UNIPROT:Q96P20"
    ]
    graphics [
      x 1133.4244474734132
      y 2592.0650283099844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_227"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 266
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_52"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re147"
      uniprot "NA"
    ]
    graphics [
      x 700.4146427238683
      y 2150.076054494584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 267
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:uniprot:Q96P20;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:NLRP3"
      map_id "M122_222"
      name "NLRP3"
      node_subtype "GENE"
      node_type "species"
      org_id "sa361"
      uniprot "UNIPROT:Q96P20"
    ]
    graphics [
      x 780.6438077512097
      y 2322.792762792765
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_222"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 268
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q00653;urn:miriam:uniprot:P19838;urn:miriam:uniprot:Q04206;urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998;urn:miriam:hgnc:7795;urn:miriam:refseq:NM_001077494;urn:miriam:hgnc.symbol:NFKB2;urn:miriam:ncbigene:4791;urn:miriam:hgnc.symbol:NFKB2;urn:miriam:ncbigene:4791;urn:miriam:ensembl:ENSG00000077150;urn:miriam:uniprot:Q00653;urn:miriam:uniprot:Q00653;urn:miriam:hgnc:9955;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ncbigene:5970;urn:miriam:ncbigene:5970;urn:miriam:refseq:NM_021975;urn:miriam:hgnc.symbol:RELA;urn:miriam:hgnc.symbol:RELA;urn:miriam:uniprot:Q04206;urn:miriam:uniprot:Q04206"
      hgnc "HGNC_SYMBOL:NFKB1;HGNC_SYMBOL:NFKB2;HGNC_SYMBOL:RELA"
      map_id "M122_2"
      name "Nf_minus_KB_space_Complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa101"
      uniprot "UNIPROT:Q00653;UNIPROT:P19838;UNIPROT:Q04206"
    ]
    graphics [
      x 232.5144998161113
      y 1902.3560207493151
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 269
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:ncbigene:3326;urn:miriam:ncbigene:3326;urn:miriam:uniprot:P08238;urn:miriam:hgnc:5258;urn:miriam:refseq:NM_007355;urn:miriam:ensembl:ENSG00000096384;urn:miriam:hgnc.symbol:HSP90AB1;urn:miriam:hgnc.symbol:HSP90AB1"
      hgnc "HGNC_SYMBOL:HSP90AB1"
      map_id "M122_229"
      name "HSP90AB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa374"
      uniprot "UNIPROT:P08238"
    ]
    graphics [
      x 2707.9673850663708
      y 1623.3355246456933
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_229"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 270
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc:16987;urn:miriam:ncbigene:10910;urn:miriam:ncbigene:10910;urn:miriam:uniprot:Q9Y2Z0;urn:miriam:ensembl:ENSG00000165416;urn:miriam:refseq:NM_001130912;urn:miriam:hgnc.symbol:SUGT1;urn:miriam:hgnc.symbol:SUGT1"
      hgnc "HGNC_SYMBOL:SUGT1"
      map_id "M122_228"
      name "SUGT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa373"
      uniprot "UNIPROT:Q9Y2Z0"
    ]
    graphics [
      x 2032.5144998161113
      y 2067.368179031944
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_228"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 271
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:O15553;urn:miriam:refseq:NM_000243;urn:miriam:hgnc:6998;urn:miriam:hgnc.symbol:MEFV;urn:miriam:hgnc.symbol:MEFV;urn:miriam:ncbigene:4210;urn:miriam:ncbigene:4210;urn:miriam:ensembl:ENSG00000103313;urn:miriam:uniprot:O15553;urn:miriam:uniprot:O15553"
      hgnc "HGNC_SYMBOL:MEFV"
      map_id "M122_33"
      name "Pyrin_space_trimer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa85"
      uniprot "UNIPROT:O15553"
    ]
    graphics [
      x 911.4760702265747
      y 1398.436020814792
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 272
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:Q9ULZ3;urn:miriam:uniprot:O15553;urn:miriam:refseq:NM_000243;urn:miriam:hgnc:6998;urn:miriam:hgnc.symbol:MEFV;urn:miriam:hgnc.symbol:MEFV;urn:miriam:ncbigene:4210;urn:miriam:ncbigene:4210;urn:miriam:ensembl:ENSG00000103313;urn:miriam:uniprot:O15553;urn:miriam:uniprot:O15553;urn:miriam:ncbigene:29108;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:uniprot:Q9ULZ3"
      hgnc "HGNC_SYMBOL:MEFV;HGNC_SYMBOL:PYCARD"
      map_id "M122_34"
      name "Pyrin_space_trimer:ASC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa88"
      uniprot "UNIPROT:Q9ULZ3;UNIPROT:O15553"
    ]
    graphics [
      x 131.47607022657485
      y 1417.3613227987366
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 273
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M122_63"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re165"
      uniprot "NA"
    ]
    graphics [
      x 322.5144998161113
      y 1963.537990215821
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 274
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:hgnc.symbol:PSTPIP1;urn:miriam:hgnc.symbol:PSTPIP1;urn:miriam:ensembl:ENSG00000140368;urn:miriam:hgnc:9580;urn:miriam:ncbigene:9051;urn:miriam:ncbigene:9051;urn:miriam:refseq:NM_003978;urn:miriam:uniprot:O43586"
      hgnc "HGNC_SYMBOL:PSTPIP1"
      map_id "M122_232"
      name "PSTPIP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa377"
      uniprot "UNIPROT:O43586"
    ]
    graphics [
      x 1102.5144998161113
      y 2016.784435371911
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_232"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 275
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:HMOX1 pathway"
      full_annotation "urn:miriam:uniprot:O15553;urn:miriam:uniprot:O43586;urn:miriam:refseq:NM_000243;urn:miriam:hgnc:6998;urn:miriam:hgnc.symbol:MEFV;urn:miriam:hgnc.symbol:MEFV;urn:miriam:ncbigene:4210;urn:miriam:ncbigene:4210;urn:miriam:ensembl:ENSG00000103313;urn:miriam:uniprot:O15553;urn:miriam:uniprot:O15553;urn:miriam:hgnc.symbol:PSTPIP1;urn:miriam:hgnc.symbol:PSTPIP1;urn:miriam:ensembl:ENSG00000140368;urn:miriam:hgnc:9580;urn:miriam:ncbigene:9051;urn:miriam:ncbigene:9051;urn:miriam:refseq:NM_003978;urn:miriam:uniprot:O43586"
      hgnc "HGNC_SYMBOL:MEFV;HGNC_SYMBOL:PSTPIP1"
      map_id "M122_32"
      name "PSTPIP1_space_trimer:Pyrin_space_trimer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa83"
      uniprot "UNIPROT:O15553;UNIPROT:O43586"
    ]
    graphics [
      x 461.0697174788761
      y 2249.7735398896766
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M122_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 276
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_230"
      target_id "M122_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 277
    source 1
    target 3
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_230"
      target_id "M122_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 278
    source 271
    target 2
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_33"
      target_id "M122_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 2
    target 272
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_62"
      target_id "M122_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 4
    target 3
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_252"
      target_id "M122_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 3
    target 5
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_60"
      target_id "M122_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 25
    target 4
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_70"
      target_id "M122_252"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 6
    target 5
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_65"
      target_id "M122_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 5
    target 7
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_31"
      target_id "M122_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 9
    target 6
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_29"
      target_id "M122_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 10
    target 6
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_234"
      target_id "M122_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 6
    target 11
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_65"
      target_id "M122_237"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 6
    target 12
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_65"
      target_id "M122_238"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 6
    target 13
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_65"
      target_id "M122_235"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 6
    target 14
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_65"
      target_id "M122_236"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 8
    target 7
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_231"
      target_id "M122_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 7
    target 9
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_61"
      target_id "M122_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 11
    target 15
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_237"
      target_id "M122_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 14
    target 15
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_236"
      target_id "M122_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 15
    target 16
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_66"
      target_id "M122_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 16
    target 17
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_36"
      target_id "M122_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 17
    target 18
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_67"
      target_id "M122_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 18
    target 19
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_35"
      target_id "M122_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 20
    target 19
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_247"
      target_id "M122_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 19
    target 21
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_72"
      target_id "M122_249"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 19
    target 22
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_72"
      target_id "M122_245"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 21
    target 23
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_249"
      target_id "M122_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 23
    target 24
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_73"
      target_id "M122_251"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 304
    source 26
    target 25
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_223"
      target_id "M122_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 305
    source 27
    target 25
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "TRIGGER"
      source_id "M122_28"
      target_id "M122_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 306
    source 28
    target 25
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M122_254"
      target_id "M122_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 307
    source 29
    target 25
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M122_267"
      target_id "M122_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 308
    source 30
    target 25
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M122_266"
      target_id "M122_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 309
    source 31
    target 25
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M122_253"
      target_id "M122_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 310
    source 32
    target 25
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M122_263"
      target_id "M122_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 311
    source 33
    target 25
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M122_258"
      target_id "M122_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 312
    source 34
    target 25
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_OR"
      source_id "M122_259"
      target_id "M122_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 313
    source 258
    target 26
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_75"
      target_id "M122_223"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 314
    source 26
    target 249
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_223"
      target_id "M122_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 315
    source 249
    target 27
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_59"
      target_id "M122_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 316
    source 35
    target 32
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_74"
      target_id "M122_263"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 317
    source 36
    target 35
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_262"
      target_id "M122_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 318
    source 37
    target 35
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "INHIBITION"
      source_id "M122_217"
      target_id "M122_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 319
    source 38
    target 37
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_44"
      target_id "M122_217"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 320
    source 39
    target 38
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_194"
      target_id "M122_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 321
    source 40
    target 38
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_204"
      target_id "M122_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 322
    source 41
    target 38
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_210"
      target_id "M122_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 323
    source 42
    target 38
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_177"
      target_id "M122_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 324
    source 38
    target 43
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_44"
      target_id "M122_200"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 325
    source 38
    target 44
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_44"
      target_id "M122_211"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 326
    source 38
    target 45
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_44"
      target_id "M122_213"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 327
    source 38
    target 46
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_44"
      target_id "M122_212"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 328
    source 122
    target 39
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_50"
      target_id "M122_194"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 329
    source 39
    target 240
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_194"
      target_id "M122_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 330
    source 39
    target 241
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_194"
      target_id "M122_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 331
    source 192
    target 42
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_115"
      target_id "M122_177"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 332
    source 43
    target 168
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_200"
      target_id "M122_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 333
    source 47
    target 45
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_113"
      target_id "M122_213"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 334
    source 48
    target 45
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_46"
      target_id "M122_213"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 335
    source 45
    target 49
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_213"
      target_id "M122_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 336
    source 45
    target 50
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_213"
      target_id "M122_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 337
    source 45
    target 51
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_213"
      target_id "M122_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 338
    source 45
    target 52
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_213"
      target_id "M122_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 339
    source 65
    target 47
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_172"
      target_id "M122_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 340
    source 239
    target 47
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_173"
      target_id "M122_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 341
    source 85
    target 48
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_205"
      target_id "M122_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 342
    source 229
    target 49
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_179"
      target_id "M122_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 343
    source 49
    target 230
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_116"
      target_id "M122_174"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 344
    source 88
    target 50
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_4"
      target_id "M122_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 345
    source 50
    target 54
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_96"
      target_id "M122_150"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 346
    source 226
    target 51
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_151"
      target_id "M122_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 347
    source 227
    target 51
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_152"
      target_id "M122_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 348
    source 105
    target 51
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_3"
      target_id "M122_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 349
    source 51
    target 228
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_95"
      target_id "M122_167"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 350
    source 53
    target 52
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_13"
      target_id "M122_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 351
    source 52
    target 54
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_37"
      target_id "M122_150"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 352
    source 53
    target 56
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_13"
      target_id "M122_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 353
    source 54
    target 55
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_150"
      target_id "M122_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 354
    source 54
    target 56
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_150"
      target_id "M122_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 355
    source 86
    target 55
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_168"
      target_id "M122_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 356
    source 87
    target 55
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_169"
      target_id "M122_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 357
    source 88
    target 55
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_4"
      target_id "M122_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 358
    source 55
    target 59
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_97"
      target_id "M122_153"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 359
    source 55
    target 89
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_97"
      target_id "M122_170"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 360
    source 57
    target 56
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_196"
      target_id "M122_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 361
    source 58
    target 56
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_197"
      target_id "M122_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 362
    source 56
    target 59
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_38"
      target_id "M122_153"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 363
    source 56
    target 60
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_38"
      target_id "M122_195"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 364
    source 59
    target 61
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_153"
      target_id "M122_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 365
    source 59
    target 62
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_153"
      target_id "M122_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 366
    source 66
    target 61
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_198"
      target_id "M122_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 367
    source 61
    target 67
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_39"
      target_id "M122_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 368
    source 63
    target 62
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_185"
      target_id "M122_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 369
    source 64
    target 62
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_5"
      target_id "M122_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 370
    source 62
    target 65
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_112"
      target_id "M122_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 371
    source 78
    target 66
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_48"
      target_id "M122_198"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 372
    source 67
    target 68
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_14"
      target_id "M122_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 373
    source 69
    target 68
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_199"
      target_id "M122_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 374
    source 68
    target 70
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_40"
      target_id "M122_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 375
    source 78
    target 69
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_48"
      target_id "M122_199"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 376
    source 70
    target 71
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_15"
      target_id "M122_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 377
    source 71
    target 72
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_41"
      target_id "M122_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 378
    source 72
    target 73
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_16"
      target_id "M122_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 379
    source 73
    target 74
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_42"
      target_id "M122_201"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 380
    source 73
    target 75
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_42"
      target_id "M122_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 381
    source 74
    target 79
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_201"
      target_id "M122_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 382
    source 75
    target 76
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_18"
      target_id "M122_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 383
    source 76
    target 77
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_47"
      target_id "M122_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 384
    source 77
    target 78
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_19"
      target_id "M122_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 385
    source 80
    target 79
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_207"
      target_id "M122_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 386
    source 81
    target 79
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_208"
      target_id "M122_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 387
    source 79
    target 82
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_43"
      target_id "M122_203"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 388
    source 82
    target 83
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_203"
      target_id "M122_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 389
    source 84
    target 83
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_206"
      target_id "M122_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 390
    source 83
    target 85
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_45"
      target_id "M122_205"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 391
    source 90
    target 88
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_110"
      target_id "M122_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 392
    source 91
    target 90
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_155"
      target_id "M122_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 393
    source 92
    target 91
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_98"
      target_id "M122_155"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 394
    source 93
    target 92
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_154"
      target_id "M122_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 395
    source 94
    target 92
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_241"
      target_id "M122_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 396
    source 95
    target 94
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_90"
      target_id "M122_241"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 397
    source 94
    target 96
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_241"
      target_id "M122_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 398
    source 94
    target 97
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_241"
      target_id "M122_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 399
    source 94
    target 98
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_241"
      target_id "M122_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 400
    source 94
    target 99
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_241"
      target_id "M122_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 401
    source 94
    target 100
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_241"
      target_id "M122_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 402
    source 94
    target 101
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_241"
      target_id "M122_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 403
    source 201
    target 95
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_215"
      target_id "M122_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 404
    source 200
    target 96
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_164"
      target_id "M122_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 405
    source 96
    target 183
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_107"
      target_id "M122_166"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 406
    source 199
    target 97
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_157"
      target_id "M122_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 407
    source 97
    target 110
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_99"
      target_id "M122_159"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 408
    source 184
    target 98
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_260"
      target_id "M122_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 409
    source 98
    target 185
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_76"
      target_id "M122_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 410
    source 164
    target 99
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_163"
      target_id "M122_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 411
    source 99
    target 165
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_106"
      target_id "M122_165"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 412
    source 111
    target 100
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_161"
      target_id "M122_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 413
    source 100
    target 112
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_103"
      target_id "M122_162"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 414
    source 102
    target 101
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_158"
      target_id "M122_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 415
    source 101
    target 103
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_100"
      target_id "M122_160"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 416
    source 103
    target 104
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_160"
      target_id "M122_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 417
    source 104
    target 105
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_102"
      target_id "M122_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 418
    source 106
    target 105
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_101"
      target_id "M122_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 419
    source 105
    target 107
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_3"
      target_id "M122_111"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 420
    source 110
    target 106
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_159"
      target_id "M122_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 421
    source 108
    target 107
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_156"
      target_id "M122_111"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 422
    source 107
    target 109
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_111"
      target_id "M122_171"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 423
    source 112
    target 113
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_162"
      target_id "M122_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 424
    source 113
    target 114
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_104"
      target_id "M122_268"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 425
    source 114
    target 115
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_268"
      target_id "M122_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 426
    source 116
    target 115
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_269"
      target_id "M122_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 427
    source 117
    target 115
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_270"
      target_id "M122_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 428
    source 118
    target 115
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "INHIBITION"
      source_id "M122_271"
      target_id "M122_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 429
    source 115
    target 119
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_80"
      target_id "M122_202"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 430
    source 115
    target 120
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_80"
      target_id "M122_272"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 431
    source 163
    target 116
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_93"
      target_id "M122_269"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 432
    source 119
    target 121
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "INHIBITION"
      source_id "M122_202"
      target_id "M122_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 433
    source 119
    target 122
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_202"
      target_id "M122_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 434
    source 124
    target 121
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_274"
      target_id "M122_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 435
    source 125
    target 121
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_276"
      target_id "M122_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 436
    source 126
    target 121
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_275"
      target_id "M122_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 437
    source 127
    target 121
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_22"
      target_id "M122_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 438
    source 128
    target 121
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "INHIBITION"
      source_id "M122_189"
      target_id "M122_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 439
    source 121
    target 129
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_81"
      target_id "M122_277"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 440
    source 121
    target 130
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_81"
      target_id "M122_279"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 441
    source 121
    target 131
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_81"
      target_id "M122_278"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 442
    source 123
    target 122
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_216"
      target_id "M122_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 443
    source 129
    target 132
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_277"
      target_id "M122_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 444
    source 132
    target 133
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_91"
      target_id "M122_280"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 445
    source 133
    target 134
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_280"
      target_id "M122_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 446
    source 135
    target 134
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_6"
      target_id "M122_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 447
    source 134
    target 136
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_84"
      target_id "M122_127"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 448
    source 134
    target 137
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_84"
      target_id "M122_129"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 449
    source 134
    target 138
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_84"
      target_id "M122_128"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 450
    source 136
    target 139
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_127"
      target_id "M122_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 451
    source 140
    target 139
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_131"
      target_id "M122_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 452
    source 141
    target 139
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_7"
      target_id "M122_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 453
    source 139
    target 142
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_85"
      target_id "M122_130"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 454
    source 139
    target 143
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_85"
      target_id "M122_132"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 455
    source 142
    target 144
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_130"
      target_id "M122_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 456
    source 145
    target 144
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_134"
      target_id "M122_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 457
    source 144
    target 146
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_86"
      target_id "M122_133"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 458
    source 146
    target 147
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_133"
      target_id "M122_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 459
    source 148
    target 147
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_136"
      target_id "M122_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 460
    source 147
    target 149
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_87"
      target_id "M122_135"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 461
    source 149
    target 150
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_135"
      target_id "M122_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 462
    source 150
    target 151
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_92"
      target_id "M122_137"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 463
    source 151
    target 152
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_137"
      target_id "M122_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 464
    source 153
    target 152
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_141"
      target_id "M122_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 465
    source 154
    target 152
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_140"
      target_id "M122_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 466
    source 152
    target 155
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_88"
      target_id "M122_138"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 467
    source 152
    target 156
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_88"
      target_id "M122_142"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 468
    source 152
    target 157
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_88"
      target_id "M122_143"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 469
    source 155
    target 158
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_138"
      target_id "M122_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 470
    source 159
    target 158
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_144"
      target_id "M122_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 471
    source 160
    target 158
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_8"
      target_id "M122_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 472
    source 158
    target 161
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_89"
      target_id "M122_139"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 473
    source 158
    target 162
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_89"
      target_id "M122_145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 474
    source 161
    target 163
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_139"
      target_id "M122_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 475
    source 165
    target 166
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_165"
      target_id "M122_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 476
    source 166
    target 167
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_109"
      target_id "M122_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 477
    source 167
    target 168
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_12"
      target_id "M122_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 478
    source 169
    target 168
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_219"
      target_id "M122_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 479
    source 170
    target 168
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_221"
      target_id "M122_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 480
    source 168
    target 171
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_55"
      target_id "M122_218"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 481
    source 168
    target 172
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_55"
      target_id "M122_220"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 482
    source 182
    target 170
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_108"
      target_id "M122_221"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 483
    source 171
    target 173
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_218"
      target_id "M122_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 484
    source 174
    target 173
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_239"
      target_id "M122_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 485
    source 173
    target 175
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_64"
      target_id "M122_233"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 486
    source 179
    target 174
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_118"
      target_id "M122_239"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 487
    source 175
    target 176
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_233"
      target_id "M122_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 488
    source 177
    target 176
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_240"
      target_id "M122_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 489
    source 176
    target 178
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_71"
      target_id "M122_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 490
    source 180
    target 179
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_181"
      target_id "M122_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 491
    source 181
    target 179
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "MODULATION"
      source_id "M122_180"
      target_id "M122_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 492
    source 183
    target 182
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_166"
      target_id "M122_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 493
    source 184
    target 194
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_260"
      target_id "M122_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 494
    source 185
    target 186
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_10"
      target_id "M122_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 495
    source 187
    target 186
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_126"
      target_id "M122_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 496
    source 188
    target 186
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "INHIBITION"
      source_id "M122_21"
      target_id "M122_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 497
    source 186
    target 189
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_79"
      target_id "M122_261"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 498
    source 194
    target 188
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_77"
      target_id "M122_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 499
    source 189
    target 190
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_261"
      target_id "M122_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 500
    source 190
    target 191
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_105"
      target_id "M122_214"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 501
    source 191
    target 192
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_214"
      target_id "M122_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 502
    source 193
    target 192
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "MODULATION"
      source_id "M122_178"
      target_id "M122_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 503
    source 195
    target 194
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_242"
      target_id "M122_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 504
    source 196
    target 195
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_78"
      target_id "M122_242"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 505
    source 197
    target 196
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_256"
      target_id "M122_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 506
    source 198
    target 196
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "INHIBITION"
      source_id "M122_255"
      target_id "M122_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 507
    source 202
    target 201
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_123"
      target_id "M122_215"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 508
    source 203
    target 202
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_273"
      target_id "M122_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 509
    source 204
    target 202
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_184"
      target_id "M122_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 510
    source 205
    target 202
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_265"
      target_id "M122_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 511
    source 203
    target 206
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_273"
      target_id "M122_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 512
    source 207
    target 206
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_281"
      target_id "M122_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 513
    source 208
    target 206
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "INHIBITION"
      source_id "M122_146"
      target_id "M122_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 514
    source 206
    target 209
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_82"
      target_id "M122_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 515
    source 209
    target 210
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_25"
      target_id "M122_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 516
    source 211
    target 210
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_23"
      target_id "M122_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 517
    source 212
    target 210
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "INHIBITION"
      source_id "M122_183"
      target_id "M122_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 518
    source 210
    target 213
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_121"
      target_id "M122_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 519
    source 224
    target 211
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_119"
      target_id "M122_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 520
    source 213
    target 214
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_11"
      target_id "M122_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 521
    source 215
    target 214
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_282"
      target_id "M122_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 522
    source 214
    target 216
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_122"
      target_id "M122_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 523
    source 216
    target 217
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_30"
      target_id "M122_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 524
    source 217
    target 218
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_83"
      target_id "M122_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 525
    source 218
    target 219
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_1"
      target_id "M122_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 526
    source 219
    target 220
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_49"
      target_id "M122_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 527
    source 219
    target 221
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_49"
      target_id "M122_209"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 528
    source 221
    target 222
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_209"
      target_id "M122_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 529
    source 222
    target 223
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_120"
      target_id "M122_182"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 530
    source 225
    target 224
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_9"
      target_id "M122_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 531
    source 230
    target 231
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_174"
      target_id "M122_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 532
    source 232
    target 231
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_148"
      target_id "M122_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 533
    source 231
    target 233
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_94"
      target_id "M122_149"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 534
    source 233
    target 234
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_149"
      target_id "M122_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 535
    source 233
    target 235
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_149"
      target_id "M122_114"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 536
    source 234
    target 238
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_117"
      target_id "M122_147"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 537
    source 236
    target 235
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_176"
      target_id "M122_114"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 538
    source 235
    target 237
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_114"
      target_id "M122_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 539
    source 248
    target 240
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_186"
      target_id "M122_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 540
    source 240
    target 245
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_124"
      target_id "M122_187"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 541
    source 242
    target 241
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_193"
      target_id "M122_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 542
    source 243
    target 241
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_190"
      target_id "M122_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 543
    source 244
    target 241
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_188"
      target_id "M122_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 544
    source 241
    target 245
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_125"
      target_id "M122_187"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 545
    source 241
    target 246
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_125"
      target_id "M122_192"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 546
    source 241
    target 247
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_125"
      target_id "M122_191"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 547
    source 250
    target 249
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_224"
      target_id "M122_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 548
    source 251
    target 250
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_51"
      target_id "M122_224"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 549
    source 250
    target 252
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_224"
      target_id "M122_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 550
    source 254
    target 251
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_24"
      target_id "M122_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 551
    source 256
    target 251
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "TRIGGER"
      source_id "M122_257"
      target_id "M122_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 552
    source 251
    target 257
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_51"
      target_id "M122_226"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 553
    source 253
    target 252
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_225"
      target_id "M122_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 554
    source 252
    target 254
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_56"
      target_id "M122_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 555
    source 253
    target 255
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_225"
      target_id "M122_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 556
    source 256
    target 255
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "TRIGGER"
      source_id "M122_257"
      target_id "M122_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 557
    source 255
    target 257
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_54"
      target_id "M122_226"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 558
    source 259
    target 258
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_26"
      target_id "M122_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 559
    source 258
    target 260
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_75"
      target_id "M122_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 560
    source 262
    target 259
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_58"
      target_id "M122_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 561
    source 261
    target 260
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_57"
      target_id "M122_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 562
    source 260
    target 262
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_27"
      target_id "M122_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 563
    source 269
    target 261
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_229"
      target_id "M122_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 564
    source 270
    target 261
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_228"
      target_id "M122_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 565
    source 263
    target 262
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_264"
      target_id "M122_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 566
    source 264
    target 263
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_53"
      target_id "M122_264"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 567
    source 265
    target 264
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_227"
      target_id "M122_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 568
    source 266
    target 265
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_52"
      target_id "M122_227"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 569
    source 267
    target 266
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_222"
      target_id "M122_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 570
    source 268
    target 266
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CATALYSIS"
      source_id "M122_2"
      target_id "M122_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 571
    source 271
    target 273
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_33"
      target_id "M122_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 572
    source 274
    target 273
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M122_232"
      target_id "M122_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 573
    source 273
    target 275
    cd19dm [
      diagram "C19DMap:HMOX1 pathway"
      edge_type "PRODUCTION"
      source_id "M122_63"
      target_id "M122_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
