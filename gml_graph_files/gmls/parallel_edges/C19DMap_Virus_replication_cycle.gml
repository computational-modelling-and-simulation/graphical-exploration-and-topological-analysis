# generated with VANTED V2.8.2 at Fri Mar 04 09:56:59 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:interpro:IPR002552"
      hgnc "NA"
      map_id "M18_188"
      name "S2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2188"
      uniprot "NA"
    ]
    graphics [
      x 1136.17031861124
      y 234.12996345076976
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_188"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:32047258"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_121"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re981"
      uniprot "NA"
    ]
    graphics [
      x 1357.7148264864115
      y 225.44115579088475
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:interpro:IPR002552"
      hgnc "NA"
      map_id "M18_164"
      name "S2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2052"
      uniprot "NA"
    ]
    graphics [
      x 1558.9490969795497
      y 316.2756131036913
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_164"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_173"
      name "Orf7a_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2105"
      uniprot "NA"
    ]
    graphics [
      x 1386.703318857706
      y 1802.1347420940956
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_173"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_44"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1025"
      uniprot "NA"
    ]
    graphics [
      x 1230.6729393286048
      y 1502.145973214872
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0070992"
      hgnc "NA"
      map_id "M18_14"
      name "Host_space_translation_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa428"
      uniprot "NA"
    ]
    graphics [
      x 790.137464813533
      y 1353.8508853447831
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740573;urn:miriam:uniprot:P0DTC7"
      hgnc "NA"
      map_id "M18_146"
      name "Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1875"
      uniprot "UNIPROT:P0DTC7"
    ]
    graphics [
      x 1547.9468895754126
      y 1375.9426924850416
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_146"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_135"
      name "N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1685"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 84.30766598244065
      y 1304.9463639287167
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "PUBMED:28720894"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_114"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re932"
      uniprot "NA"
    ]
    graphics [
      x 127.47118067985127
      y 1178.9839237231522
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_151"
      name "N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1887"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 248.86851891899596
      y 1249.2024324792806
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_151"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_174"
      name "Orf7b_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2106"
      uniprot "NA"
    ]
    graphics [
      x 561.5308030353891
      y 1773.7650938278464
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_174"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_50"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1031"
      uniprot "NA"
    ]
    graphics [
      x 541.8077215100773
      y 1538.402753703026
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740574;urn:miriam:uniprot:P0DTD8"
      hgnc "NA"
      map_id "M18_147"
      name "Orf7b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1876"
      uniprot "UNIPROT:P0DTD8"
    ]
    graphics [
      x 349.777807558124
      y 1564.6397839909505
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_147"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_3"
      name "Nucleocapsid"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa365"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 1569.4263085823186
      y 478.8249782290119
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "PUBMED:32047258;PUBMED:32142651"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_120"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re979"
      uniprot "NA"
    ]
    graphics [
      x 1714.6886699220613
      y 514.9519142891401
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570;urn:miriam:hgnc.symbol:E;urn:miriam:uniprot:P0DTC4"
      hgnc "HGNC_SYMBOL:E"
      map_id "M18_167"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2062"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 1573.754385417235
      y 622.5780785225915
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_167"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740573;urn:miriam:uniprot:P0DTC7"
      hgnc "NA"
      map_id "M18_165"
      name "Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2060"
      uniprot "UNIPROT:P0DTC7"
    ]
    graphics [
      x 1609.1264927736065
      y 567.6657646028602
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_165"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "M18_166"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2061"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 1782.8933158234236
      y 412.7129239660369
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_166"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_5"
      name "Nucleocapsid"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa369"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 1895.5204354250966
      y 841.2455215063646
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740573;urn:miriam:uniprot:P0DTC7"
      hgnc "NA"
      map_id "M18_182"
      name "Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2114"
      uniprot "UNIPROT:P0DTC7"
    ]
    graphics [
      x 1678.0448006389488
      y 662.7825261699192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_182"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570;urn:miriam:hgnc.symbol:E;urn:miriam:uniprot:P0DTC4"
      hgnc "HGNC_SYMBOL:E"
      map_id "M18_183"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2115"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 1618.7866755421805
      y 458.5701055214729
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_183"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "M18_184"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2116"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 1539.6398322666887
      y 560.12988355399
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_184"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725306"
      hgnc "NA"
      map_id "M18_192"
      name "Nsp10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2200"
      uniprot "NA"
    ]
    graphics [
      x 1153.7790891029895
      y 541.2037978876114
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_192"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      annotation "PUBMED:11907209;PUBMED:30632963"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_97"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1112"
      uniprot "NA"
    ]
    graphics [
      x 1051.3622608888993
      y 804.8834956166555
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725311"
      hgnc "NA"
      map_id "M18_191"
      name "Nsp16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2199"
      uniprot "NA"
    ]
    graphics [
      x 1143.0940424508908
      y 655.0750399413723
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_191"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725310"
      hgnc "NA"
      map_id "M18_197"
      name "Nsp15"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2205"
      uniprot "NA"
    ]
    graphics [
      x 1067.5530839582325
      y 523.7938338543113
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_197"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725309"
      hgnc "NA"
      map_id "M18_198"
      name "Nsp14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2206"
      uniprot "NA"
    ]
    graphics [
      x 1209.257986718133
      y 651.1753077634263
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_198"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725308"
      hgnc "NA"
      map_id "M18_190"
      name "Nsp13"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2198"
      uniprot "NA"
    ]
    graphics [
      x 1109.3525040688728
      y 584.1030886890469
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_190"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725307"
      hgnc "NA"
      map_id "M18_189"
      name "Nsp12"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2197"
      uniprot "NA"
    ]
    graphics [
      x 1087.9164261070475
      y 636.143666266707
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_189"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725305"
      hgnc "NA"
      map_id "M18_193"
      name "Nsp9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2201"
      uniprot "NA"
    ]
    graphics [
      x 1228.5538902155429
      y 713.1950539217596
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_193"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725304"
      hgnc "NA"
      map_id "M18_194"
      name "Nsp8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2202"
      uniprot "NA"
    ]
    graphics [
      x 1169.082595317168
      y 710.968854601846
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_194"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725303"
      hgnc "NA"
      map_id "M18_195"
      name "Nsp7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2203"
      uniprot "NA"
    ]
    graphics [
      x 1177.9007261395868
      y 600.4339189816712
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_195"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725302"
      hgnc "NA"
      map_id "M18_196"
      name "Nsp6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2204"
      uniprot "NA"
    ]
    graphics [
      x 1088.1005802897832
      y 727.131005301479
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_196"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725299;urn:miriam:uniprot:Nsp3"
      hgnc "NA"
      map_id "M18_203"
      name "Nsp3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2222"
      uniprot "UNIPROT:Nsp3"
    ]
    graphics [
      x 1099.7864489573485
      y 1064.0429125839407
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_203"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725300"
      hgnc "NA"
      map_id "M18_205"
      name "Nsp4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2228"
      uniprot "NA"
    ]
    graphics [
      x 1049.9447911620753
      y 1049.9345282922818
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_205"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725302"
      hgnc "NA"
      map_id "M18_281"
      name "Nsp6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2367"
      uniprot "NA"
    ]
    graphics [
      x 825.819274980847
      y 835.9341228410542
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_281"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725303"
      hgnc "NA"
      map_id "M18_277"
      name "Nsp7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2363"
      uniprot "NA"
    ]
    graphics [
      x 833.142058685236
      y 766.0166303624002
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_277"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725304"
      hgnc "NA"
      map_id "M18_276"
      name "Nsp8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2362"
      uniprot "NA"
    ]
    graphics [
      x 883.6618032176741
      y 715.2609476954573
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_276"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725305"
      hgnc "NA"
      map_id "M18_275"
      name "Nsp9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2361"
      uniprot "NA"
    ]
    graphics [
      x 803.6354423350847
      y 724.1652679105702
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_275"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725306"
      hgnc "NA"
      map_id "M18_274"
      name "Nsp10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2360"
      uniprot "NA"
    ]
    graphics [
      x 843.7233655004256
      y 664.7665991612741
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_274"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725298"
      hgnc "NA"
      map_id "M18_200"
      name "Nsp2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2217"
      uniprot "NA"
    ]
    graphics [
      x 1256.419344804629
      y 1127.3968640901044
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_200"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725298"
      hgnc "NA"
      map_id "M18_271"
      name "Nsp2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2357"
      uniprot "NA"
    ]
    graphics [
      x 1275.8847347833023
      y 776.0501775356522
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_271"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_12"
      name "Replication_space_transcription_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa410"
      uniprot "NA"
    ]
    graphics [
      x 1103.3046490493132
      y 1579.8152322592127
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_6"
      name "Nucleocapsid"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa374"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 1622.38732918676
      y 1008.8936793107268
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "PUBMED:32047258;PUBMED:32142651;PUBMED:32944968;PUBMED:32094589"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_109"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re855"
      uniprot "NA"
    ]
    graphics [
      x 1750.077271755637
      y 980.6298714661059
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570;urn:miriam:hgnc.symbol:E;urn:miriam:uniprot:P0DTC4"
      hgnc "HGNC_SYMBOL:E"
      map_id "M18_160"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2023"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 1867.3744697890365
      y 979.3831883820865
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_160"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "M18_161"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2024"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 1657.8388353893874
      y 1061.1032843735509
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_161"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740573;urn:miriam:uniprot:P0DTC7"
      hgnc "NA"
      map_id "M18_162"
      name "Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2025"
      uniprot "UNIPROT:P0DTC7"
    ]
    graphics [
      x 1864.6034582051652
      y 913.8759314558963
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_162"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:interpro:IPR002552"
      hgnc "NA"
      map_id "M18_132"
      name "S2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1601"
      uniprot "NA"
    ]
    graphics [
      x 1536.2184955451994
      y 1222.5232179280226
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16113"
      hgnc "NA"
      map_id "M18_286"
      name "cholesterol"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa2372"
      uniprot "NA"
    ]
    graphics [
      x 1753.6735171941455
      y 1126.1232284415644
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_286"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:interpro:IPR002551;urn:miriam:ncbigene:8829;urn:miriam:ncbigene:8829;urn:miriam:hgnc:8004;urn:miriam:uniprot:O14786;urn:miriam:uniprot:O14786;urn:miriam:ensembl:ENSG00000099250;urn:miriam:refseq:NM_001024628;urn:miriam:hgnc.symbol:NRP1;urn:miriam:hgnc.symbol:NRP1"
      hgnc "HGNC_SYMBOL:NRP1"
      map_id "M18_21"
      name "S1:NRP1_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa441"
      uniprot "UNIPROT:O14786"
    ]
    graphics [
      x 1705.1761923769504
      y 862.4974826491334
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570;urn:miriam:hgnc.symbol:E;urn:miriam:uniprot:P0DTC4"
      hgnc "HGNC_SYMBOL:E"
      map_id "M18_169"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2065"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 1711.9352232120543
      y 1085.7747861551168
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_169"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "M18_170"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2066"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 1831.9871921315691
      y 1083.5395545862514
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_170"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740573;urn:miriam:uniprot:P0DTC7"
      hgnc "NA"
      map_id "M18_171"
      name "Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2067"
      uniprot "UNIPROT:P0DTC7"
    ]
    graphics [
      x 1654.632189843832
      y 950.7915936217623
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_171"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_263"
      name "Orf8_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2349"
      uniprot "NA"
    ]
    graphics [
      x 693.3711988265379
      y 1891.731439636167
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_263"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_82"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1095"
      uniprot "NA"
    ]
    graphics [
      x 536.5041483819994
      y 1885.9956766273335
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_229"
      name "Orf8_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2299"
      uniprot "NA"
    ]
    graphics [
      x 725.05449887193
      y 2048.3883679203977
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_229"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_175"
      name "Orf8_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2107"
      uniprot "NA"
    ]
    graphics [
      x 460.9745282525431
      y 1630.326412926021
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_175"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725300;urn:miriam:ncbiprotein:YP_009725299"
      hgnc "NA"
      map_id "M18_213"
      name "Nsp3_minus_4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2243"
      uniprot "NA"
    ]
    graphics [
      x 1655.9694596503855
      y 1294.4545307138453
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_213"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_93"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1108"
      uniprot "NA"
    ]
    graphics [
      x 1332.4969283897549
      y 1202.5232296008614
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_237"
      name "Orf9b_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2320"
      uniprot "NA"
    ]
    graphics [
      x 1296.8845274803164
      y 1906.7989545927053
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_237"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_72"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1084"
      uniprot "NA"
    ]
    graphics [
      x 1156.840300835328
      y 1914.7631951675721
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_264"
      name "Orf9b_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2350"
      uniprot "NA"
    ]
    graphics [
      x 1165.674914233622
      y 2078.197916571862
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_264"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_102"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1120"
      uniprot "NA"
    ]
    graphics [
      x 827.2267168643048
      y 1528.9475128704455
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_11"
      name "Replication_space_transcription_space_complex:N_space_oligomer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa398"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 622.6251122942307
      y 1413.7023494442242
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "PUBMED:28720894"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_52"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1061"
      uniprot "NA"
    ]
    graphics [
      x 415.60000736168763
      y 1350.5876827915417
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:interpro:IPR002551"
      hgnc "NA"
      map_id "M18_129"
      name "S1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1539"
      uniprot "NA"
    ]
    graphics [
      x 1450.5894327589149
      y 924.5212300883181
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_129"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      annotation "PUBMED:33082294;PUBMED:33082293"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_104"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1124"
      uniprot "NA"
    ]
    graphics [
      x 1552.8829915351807
      y 824.3122177914277
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:8829;urn:miriam:ncbigene:8829;urn:miriam:hgnc:8004;urn:miriam:uniprot:O14786;urn:miriam:uniprot:O14786;urn:miriam:ensembl:ENSG00000099250;urn:miriam:refseq:NM_001024628;urn:miriam:hgnc.symbol:NRP1;urn:miriam:hgnc.symbol:NRP1"
      hgnc "HGNC_SYMBOL:NRP1"
      map_id "M18_285"
      name "NRP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2371"
      uniprot "UNIPROT:O14786"
    ]
    graphics [
      x 1627.7222188291614
      y 872.9684119841424
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_285"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570"
      hgnc "NA"
      map_id "M18_243"
      name "E_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2326"
      uniprot "NA"
    ]
    graphics [
      x 1309.1652205148837
      y 1811.5569845435662
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_243"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      annotation "PUBMED:11142;PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_66"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1078"
      uniprot "NA"
    ]
    graphics [
      x 1157.1140335021173
      y 1680.4141209256156
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_258"
      name "E_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2344"
      uniprot "NA"
    ]
    graphics [
      x 1025.3037371340786
      y 1521.1047193412312
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_258"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTD2"
      hgnc "NA"
      map_id "M18_149"
      name "Orf9b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1878"
      uniprot "UNIPROT:P0DTD2"
    ]
    graphics [
      x 627.0599121692092
      y 2014.010819254283
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_149"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_37"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1018"
      uniprot "NA"
    ]
    graphics [
      x 644.9179688265583
      y 2197.9619744356514
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTD2"
      hgnc "NA"
      map_id "M18_219"
      name "Orf9b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2249"
      uniprot "UNIPROT:P0DTD2"
    ]
    graphics [
      x 713.6929319988985
      y 2308.5795228193265
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_219"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_239"
      name "Orf3a_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2322"
      uniprot "NA"
    ]
    graphics [
      x 1036.9849637274579
      y 1618.8923070723088
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_239"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_70"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1082"
      uniprot "NA"
    ]
    graphics [
      x 916.9792278748042
      y 1492.403116027229
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_262"
      name "Orf3a_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2348"
      uniprot "NA"
    ]
    graphics [
      x 700.7354640576409
      y 1258.474932799699
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_262"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571"
      hgnc "NA"
      map_id "M18_244"
      name "M_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2327"
      uniprot "NA"
    ]
    graphics [
      x 1162.7203632776832
      y 1830.3132667423233
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_244"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_65"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1077"
      uniprot "NA"
    ]
    graphics [
      x 1304.2234385061035
      y 1724.0475752917662
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_257"
      name "M_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2343"
      uniprot "NA"
    ]
    graphics [
      x 1532.7614133877598
      y 1834.3421088165856
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_257"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      annotation "PUBMED:28484023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_96"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re1111"
      uniprot "NA"
    ]
    graphics [
      x 906.5715752866633
      y 477.86747385714773
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0071360"
      hgnc "NA"
      map_id "M18_222"
      name "cellular_space_response_space_to_space_exogenous_space_dsRNA"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa2291"
      uniprot "NA"
    ]
    graphics [
      x 985.2608102357436
      y 498.227557619265
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_222"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M18_214"
      name "pp1ab_space_Nsp3_minus_16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2244"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1891.7343148660018
      y 1096.7275003331365
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_214"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      annotation "PUBMED:21203998;PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_26"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1006"
      uniprot "NA"
    ]
    graphics [
      x 1768.8013366702653
      y 835.1865343791229
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M18_206"
      name "pp1ab_space_nsp6_minus_16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2229"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1516.4901445870128
      y 692.0821151508969
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_206"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725301"
      hgnc "NA"
      map_id "M18_211"
      name "Nsp5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2241"
      uniprot "NA"
    ]
    graphics [
      x 1567.969604738263
      y 683.717567793135
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_211"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725300;urn:miriam:ncbiprotein:YP_009725299"
      hgnc "NA"
      map_id "M18_212"
      name "Nsp3_minus_4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2242"
      uniprot "NA"
    ]
    graphics [
      x 1923.212450358209
      y 1030.0086515570936
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_212"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_225"
      name "S_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2295"
      uniprot "NA"
    ]
    graphics [
      x 1088.9547798744154
      y 1656.6738118115363
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_225"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_56"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1067"
      uniprot "NA"
    ]
    graphics [
      x 1335.117424262075
      y 1665.8607598950496
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_248"
      name "sa2295_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2332"
      uniprot "NA"
    ]
    graphics [
      x 1503.2417818237732
      y 1684.6696812440412
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_248"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740575"
      hgnc "NA"
      map_id "M18_267"
      name "N_space__space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2353"
      uniprot "NA"
    ]
    graphics [
      x 1009.3771685631365
      y 1825.1822706132725
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_267"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_86"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1099"
      uniprot "NA"
    ]
    graphics [
      x 857.6181512965645
      y 1762.1507028759634
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_268"
      name "N_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2354"
      uniprot "NA"
    ]
    graphics [
      x 608.2825955940444
      y 1837.8323510446835
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_268"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "M18_138"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1688"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 500.27102723534585
      y 1151.2616558880206
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_138"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_32"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1013"
      uniprot "NA"
    ]
    graphics [
      x 573.3080466790786
      y 1007.4644262564514
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "M18_154"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1893"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 751.0378517182138
      y 960.0602150827585
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_154"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_77"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1090"
      uniprot "NA"
    ]
    graphics [
      x 905.3696182725339
      y 1350.4790682292457
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570"
      hgnc "NA"
      map_id "M18_224"
      name "E_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2294"
      uniprot "NA"
    ]
    graphics [
      x 882.3340624009802
      y 1197.6556461496539
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_224"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_179"
      name "E_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2111"
      uniprot "NA"
    ]
    graphics [
      x 713.04530112973
      y 1364.1400923478718
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_179"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_240"
      name "Orf6_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2323"
      uniprot "NA"
    ]
    graphics [
      x 1235.053981676744
      y 1881.3194512707894
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_240"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_69"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1081"
      uniprot "NA"
    ]
    graphics [
      x 1244.4155062489576
      y 1684.8865335177236
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_261"
      name "Orf6_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2347"
      uniprot "NA"
    ]
    graphics [
      x 1322.3377822866962
      y 1522.0536455389408
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_261"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M18_140"
      name "pp1ab"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1790"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1600.8777768821167
      y 1458.4896037322378
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_140"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      annotation "PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_88"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1102"
      uniprot "NA"
    ]
    graphics [
      x 1425.1114770612007
      y 1472.3265038710017
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M18_202"
      name "pp1ab_space_Nsp3_minus_16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2221"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1675.3215722888697
      y 1441.4009113115185
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_202"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725297"
      hgnc "NA"
      map_id "M18_199"
      name "Nsp1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2216"
      uniprot "NA"
    ]
    graphics [
      x 1229.2463602250648
      y 1819.5102626523517
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_199"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740575"
      hgnc "NA"
      map_id "M18_157"
      name "N_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa1962"
      uniprot "NA"
    ]
    graphics [
      x 369.69359150535195
      y 1640.5580799420852
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_157"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_115"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re940"
      uniprot "NA"
    ]
    graphics [
      x 233.5917239130564
      y 1429.5158857239555
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0070992"
      hgnc "NA"
      map_id "M18_15"
      name "Host_space_translation_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa429"
      uniprot "NA"
    ]
    graphics [
      x 367.68392236856846
      y 1296.1383806465026
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0005783"
      hgnc "NA"
      map_id "M18_19"
      name "Endoplasmic_space_reticulum"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa439"
      uniprot "NA"
    ]
    graphics [
      x 942.5640916982857
      y 1042.8414941831825
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      annotation "PUBMED:23943763"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_123"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re999"
      uniprot "NA"
    ]
    graphics [
      x 963.8327512247231
      y 931.2988122246713
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0039718"
      hgnc "NA"
      map_id "M18_18"
      name "Double_minus_membrane_space_vesicle"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa438"
      uniprot "NA"
    ]
    graphics [
      x 831.2928161132141
      y 999.8065477044167
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_76"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1089"
      uniprot "NA"
    ]
    graphics [
      x 1679.366968837495
      y 1882.9273232298037
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571"
      hgnc "NA"
      map_id "M18_223"
      name "M_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2292"
      uniprot "NA"
    ]
    graphics [
      x 1860.2265951676031
      y 1951.6552636598462
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_223"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_181"
      name "M_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2113"
      uniprot "NA"
    ]
    graphics [
      x 1484.0803053780965
      y 1754.9624177874466
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_181"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_81"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1094"
      uniprot "NA"
    ]
    graphics [
      x 517.9904695246134
      y 1066.0130037918775
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_228"
      name "Orf3a_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2298"
      uniprot "NA"
    ]
    graphics [
      x 430.2322482568875
      y 874.7567458459521
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_228"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_172"
      name "Orf3a_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2104"
      uniprot "NA"
    ]
    graphics [
      x 597.0375408143248
      y 1155.1545761236403
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_172"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_20"
      name "Replication_space_transcription_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa440"
      uniprot "NA"
    ]
    graphics [
      x 890.2539741963627
      y 1641.7856141167345
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_101"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1119"
      uniprot "NA"
    ]
    graphics [
      x 984.4832844898225
      y 1655.1424554530302
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:refseq:NC_045512"
      hgnc "NA"
      map_id "M18_245"
      name "(_minus_)ss_space_gRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2328"
      uniprot "NA"
    ]
    graphics [
      x 1408.8232796667528
      y 1645.8371927666246
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_245"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_64"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1076"
      uniprot "NA"
    ]
    graphics [
      x 1298.0528388309763
      y 1583.3967486169258
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:refseq:NC_045512"
      hgnc "NA"
      map_id "M18_256"
      name "ds_space_gRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2342"
      uniprot "NA"
    ]
    graphics [
      x 1312.3754244113002
      y 1444.5199000108682
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_256"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_87"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1100"
      uniprot "NA"
    ]
    graphics [
      x 393.556228845839
      y 1855.5990161565942
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_269"
      name "N_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2355"
      uniprot "NA"
    ]
    graphics [
      x 405.48115776314353
      y 2028.3051765843343
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_269"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_83"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1096"
      uniprot "NA"
    ]
    graphics [
      x 1040.8243309785778
      y 2156.2753010732094
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_230"
      name "Orf9b_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2300"
      uniprot "NA"
    ]
    graphics [
      x 1244.27690540552
      y 2271.0684003869155
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_230"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_177"
      name "Orf9b_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2109"
      uniprot "NA"
    ]
    graphics [
      x 850.8000937741152
      y 2003.3084395452663
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_177"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC8;urn:miriam:ncbigene:43740577"
      hgnc "NA"
      map_id "M18_150"
      name "Orf8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1879"
      uniprot "UNIPROT:P0DTC8"
    ]
    graphics [
      x 300.36647721663144
      y 1353.1773257523762
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_150"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1017"
      uniprot "NA"
    ]
    graphics [
      x 159.2404545336251
      y 1295.5628542429695
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC8;urn:miriam:ncbigene:43740577"
      hgnc "NA"
      map_id "M18_218"
      name "Orf8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2248"
      uniprot "UNIPROT:P0DTC8"
    ]
    graphics [
      x 62.5
      y 1366.9713982472238
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_218"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_41"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1022"
      uniprot "NA"
    ]
    graphics [
      x 1221.5490953902126
      y 1625.3454712376674
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "M18_136"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1686"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 1455.701136185341
      y 1883.4785396586244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_133"
      name "N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1667"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 2201.4301455893474
      y 1178.4418107712531
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_133"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_113"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re908"
      uniprot "NA"
    ]
    graphics [
      x 2299.1516248397147
      y 1291.5408905747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_155"
      name "s2919"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa1920"
      uniprot "NA"
    ]
    graphics [
      x 2215.2734763442263
      y 1396.7281382274712
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_155"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:refseq:NC_045512"
      hgnc "NA"
      map_id "M18_134"
      name "(_plus_)ss_space_gRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa1675"
      uniprot "NA"
    ]
    graphics [
      x 1779.2272572820866
      y 1259.9522838108253
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_99"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1115"
      uniprot "NA"
    ]
    graphics [
      x 1797.0201233072025
      y 1386.582490914698
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0070992"
      hgnc "NA"
      map_id "M18_13"
      name "Host_space_translation_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa427"
      uniprot "NA"
    ]
    graphics [
      x 1820.1986937557685
      y 1200.5642229988043
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_238"
      name "Orf8_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2321"
      uniprot "NA"
    ]
    graphics [
      x 1033.9691043429132
      y 1902.9277767789354
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_238"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_71"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1083"
      uniprot "NA"
    ]
    graphics [
      x 888.9098066819225
      y 1825.0553477079666
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725300;urn:miriam:ncbiprotein:YP_009725299"
      hgnc "NA"
      map_id "M18_273"
      name "Nsp3_minus_4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2359"
      uniprot "NA"
    ]
    graphics [
      x 583.8521267016913
      y 874.215330590162
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_273"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      annotation "PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_91"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1106"
      uniprot "NA"
    ]
    graphics [
      x 666.3284868775344
      y 1017.2532701548544
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725300;urn:miriam:ncbiprotein:YP_009725299"
      hgnc "NA"
      map_id "M18_279"
      name "Nsp3_minus_4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2365"
      uniprot "NA"
    ]
    graphics [
      x 823.4010820128232
      y 1126.9108158311203
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_279"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      annotation "PUBMED:21203998"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_25"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1004"
      uniprot "NA"
    ]
    graphics [
      x 1850.7908230541225
      y 1328.3978317158171
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_231"
      name "Orf14_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2301"
      uniprot "NA"
    ]
    graphics [
      x 993.6172148559647
      y 389.8059099890397
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_231"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_62"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1074"
      uniprot "NA"
    ]
    graphics [
      x 1028.7321593541778
      y 195.07033112193972
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_254"
      name "sa2301_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2339"
      uniprot "NA"
    ]
    graphics [
      x 1030.9137312326757
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_254"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      annotation "PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_24"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1002"
      uniprot "NA"
    ]
    graphics [
      x 1932.8548376115905
      y 1255.79305487799
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_100"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1118"
      uniprot "NA"
    ]
    graphics [
      x 526.4621518983807
      y 2134.517389296735
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_282"
      name "sa2355_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2368"
      uniprot "NA"
    ]
    graphics [
      x 554.4729137101864
      y 2047.4798295637993
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_282"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M18_210"
      name "pp1a_space_Nsp3_minus_11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2240"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 749.3072334330195
      y 623.4697489513634
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_210"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      annotation "PUBMED:21203998;PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_122"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re989"
      uniprot "NA"
    ]
    graphics [
      x 559.7910401427663
      y 721.3833573368933
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTC1;urn:miriam:ec-code:3.4.22.-;urn:miriam:ncbigene:43740578"
      hgnc "NA"
      map_id "M18_204"
      name "pp1a_space_Nsp6_minus_11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2224"
      uniprot "UNIPROT:P0DTC1"
    ]
    graphics [
      x 478.97966605306306
      y 701.8585805590433
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_204"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725301"
      hgnc "NA"
      map_id "M18_280"
      name "Nsp5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2366"
      uniprot "NA"
    ]
    graphics [
      x 530.9743155945679
      y 833.1123487679384
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_280"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_42"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1023"
      uniprot "NA"
    ]
    graphics [
      x 631.1456540932095
      y 1488.8615015609196
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570;urn:miriam:hgnc.symbol:E;urn:miriam:uniprot:P0DTC4"
      hgnc "HGNC_SYMBOL:E"
      map_id "M18_137"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1687"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 529.8759118645036
      y 1606.5199666502094
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_137"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_30"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1011"
      uniprot "NA"
    ]
    graphics [
      x 1616.1492873992659
      y 2068.0800804835326
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "M18_152"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1891"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 1722.2483131813524
      y 2224.8969076844987
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_152"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      annotation "PUBMED:11907209"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_94"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1109"
      uniprot "NA"
    ]
    graphics [
      x 679.5047806549026
      y 719.9396864983064
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725312"
      hgnc "NA"
      map_id "M18_278"
      name "Nsp11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2364"
      uniprot "NA"
    ]
    graphics [
      x 575.6032419675352
      y 612.8433227103548
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_278"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_227"
      name "Orf6_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2297"
      uniprot "NA"
    ]
    graphics [
      x 1243.49071454971
      y 1401.109336634027
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_227"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_58"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1069"
      uniprot "NA"
    ]
    graphics [
      x 1113.6830889255255
      y 1426.1576872654637
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_250"
      name "sa2297_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2334"
      uniprot "NA"
    ]
    graphics [
      x 998.5142958642698
      y 1453.5243629224542
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_250"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570;urn:miriam:hgnc.symbol:E;urn:miriam:uniprot:P0DTC4"
      hgnc "HGNC_SYMBOL:E"
      map_id "M18_153"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1892"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 611.3050818842556
      y 1640.6979339138688
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_153"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 167
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_28"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1009"
      uniprot "NA"
    ]
    graphics [
      x 723.2406429669586
      y 1530.7455792944088
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 168
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740570;urn:miriam:hgnc.symbol:E;urn:miriam:uniprot:P0DTC4"
      hgnc "HGNC_SYMBOL:E"
      map_id "M18_142"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1858"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 777.6961294444617
      y 1425.0880120692834
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_142"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 169
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_226"
      name "Orf7a_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2296"
      uniprot "NA"
    ]
    graphics [
      x 1363.726141235105
      y 2298.118927072796
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_226"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 170
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_57"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1068"
      uniprot "NA"
    ]
    graphics [
      x 1254.1935800538818
      y 2411.4116888656117
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 171
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_249"
      name "sa2296_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2333"
      uniprot "NA"
    ]
    graphics [
      x 1109.6195379590563
      y 2321.2894505486165
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_249"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 172
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_39"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1020"
      uniprot "NA"
    ]
    graphics [
      x 217.35944433079146
      y 1557.2105951764136
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 173
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740574;urn:miriam:uniprot:P0DTD8"
      hgnc "NA"
      map_id "M18_221"
      name "Orf7b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2251"
      uniprot "UNIPROT:P0DTD8"
    ]
    graphics [
      x 308.5609348200891
      y 1490.4868003249308
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_221"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 174
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_178"
      name "Orf14_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2110"
      uniprot "NA"
    ]
    graphics [
      x 880.4187541025283
      y 798.7765280374947
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_178"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 175
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_49"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1030"
      uniprot "NA"
    ]
    graphics [
      x 719.3029618752139
      y 917.6291575829066
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 176
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTD3"
      hgnc "NA"
      map_id "M18_148"
      name "Orf14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1877"
      uniprot "UNIPROT:P0DTD3"
    ]
    graphics [
      x 616.2334129926309
      y 677.0252768020582
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_148"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 177
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_55"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1066"
      uniprot "NA"
    ]
    graphics [
      x 867.3014077307566
      y 1057.2965506984328
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 178
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_247"
      name "sa2294_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2331"
      uniprot "NA"
    ]
    graphics [
      x 854.2012395475952
      y 931.6204110985658
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_247"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 179
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S;urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:S;HGNC_SYMBOL:ACE2"
      map_id "M18_4"
      name "ACE2:SPIKE_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa368"
      uniprot "UNIPROT:P0DTC2;UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1488.4125150572995
      y 1188.0365233744697
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 180
    zlevel -1

    cd19dm [
      annotation "PUBMED:32142651;PUBMED:32362314"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_117"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re950"
      uniprot "NA"
    ]
    graphics [
      x 1321.9532550230674
      y 1056.4465506403214
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 181
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:hgnc:8568;urn:miriam:ensembl:ENSG00000140564;urn:miriam:ec-code:3.4.21.75;urn:miriam:uniprot:P09958;urn:miriam:uniprot:P09958;urn:miriam:ncbigene:5045;urn:miriam:ncbigene:5045;urn:miriam:hgnc.symbol:FURIN;urn:miriam:hgnc.symbol:FURIN;urn:miriam:refseq:NM_002569"
      hgnc "HGNC_SYMBOL:FURIN"
      map_id "M18_156"
      name "FURIN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1921"
      uniprot "UNIPROT:P09958"
    ]
    graphics [
      x 1194.583424457152
      y 972.9834055391186
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_156"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 182
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:hgnc:11876;urn:miriam:hgnc.symbol:TMPRSS2;urn:miriam:uniprot:O15393;urn:miriam:uniprot:O15393;urn:miriam:hgnc.symbol:TMPRSS2;urn:miriam:ncbigene:7113;urn:miriam:ncbigene:7113;urn:miriam:ec-code:3.4.21.-;urn:miriam:ensembl:ENSG00000184012;urn:miriam:refseq:NM_001135099"
      hgnc "HGNC_SYMBOL:TMPRSS2"
      map_id "M18_128"
      name "TMPRSS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1537"
      uniprot "UNIPROT:O15393"
    ]
    graphics [
      x 1266.7778232501223
      y 948.132764950401
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 183
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:pubchem.compound:2536"
      hgnc "NA"
      map_id "M18_130"
      name "Camostat_space_mesylate"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1544"
      uniprot "NA"
    ]
    graphics [
      x 1150.9039861877332
      y 1026.411776288512
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_130"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 184
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:interpro:IPR002552"
      hgnc "NA"
      map_id "M18_168"
      name "S2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2063"
      uniprot "NA"
    ]
    graphics [
      x 1255.8950488735686
      y 1215.5209345898293
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_168"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 185
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:ACE2"
      map_id "M18_209"
      name "ACE2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2239"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1327.2909217911563
      y 954.8167704480536
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_209"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 186
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:ACE2"
      map_id "M18_208"
      name "ACE2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2238"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1433.5589799201357
      y 1030.218467688139
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_208"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 187
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_236"
      name "Orf14_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2319"
      uniprot "NA"
    ]
    graphics [
      x 1148.2727457884957
      y 1522.972031107954
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_236"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 188
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_73"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1085"
      uniprot "NA"
    ]
    graphics [
      x 1082.1768364710288
      y 1311.2880063509162
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 189
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_265"
      name "Orf14_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2351"
      uniprot "NA"
    ]
    graphics [
      x 1021.4048113529152
      y 973.8654226346846
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_265"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 190
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S;urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:S;HGNC_SYMBOL:ACE2"
      map_id "M18_17"
      name "ACE2:SPIKE_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa431"
      uniprot "UNIPROT:P0DTC2;UNIPROT:Q9BYF1"
    ]
    graphics [
      x 776.7550063997736
      y 322.19451271692276
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 191
    zlevel -1

    cd19dm [
      annotation "PUBMED:32142651"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_107"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re843"
      uniprot "NA"
    ]
    graphics [
      x 897.1775638450272
      y 286.6839050809126
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 192
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ensembl:ENSG00000164733;urn:miriam:ncbigene:1508;urn:miriam:ncbigene:1508;urn:miriam:refseq:NM_147780;urn:miriam:uniprot:P07858;urn:miriam:uniprot:P07858;urn:miriam:hgnc:2527;urn:miriam:ec-code:3.4.22.1;urn:miriam:hgnc.symbol:CTSB;urn:miriam:hgnc.symbol:CTSB"
      hgnc "HGNC_SYMBOL:CTSB"
      map_id "M18_126"
      name "CTSB"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1524"
      uniprot "UNIPROT:P07858"
    ]
    graphics [
      x 828.6276265686215
      y 505.4010992456232
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 193
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.22.15;urn:miriam:hgnc.symbol:CTSL;urn:miriam:ncbigene:1514;urn:miriam:hgnc.symbol:CTSL;urn:miriam:ncbigene:1514;urn:miriam:uniprot:P07711;urn:miriam:uniprot:P07711;urn:miriam:ensembl:ENSG00000135047;urn:miriam:refseq:NM_001912;urn:miriam:hgnc:2537"
      hgnc "HGNC_SYMBOL:CTSL"
      map_id "M18_127"
      name "CTSL"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1525"
      uniprot "UNIPROT:P07711"
    ]
    graphics [
      x 949.2040068550779
      y 213.20185646674486
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_127"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 194
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:interpro:IPR002551"
      hgnc "NA"
      map_id "M18_125"
      name "S1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1516"
      uniprot "NA"
    ]
    graphics [
      x 875.933577516127
      y 385.9521776564395
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_125"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 195
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_232"
      name "Orf7b_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2302"
      uniprot "NA"
    ]
    graphics [
      x 805.0160031602688
      y 2165.695609809843
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_232"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 196
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_63"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1075"
      uniprot "NA"
    ]
    graphics [
      x 956.9607687722984
      y 2304.013892703149
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 197
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_255"
      name "sa2302_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2340"
      uniprot "NA"
    ]
    graphics [
      x 1139.6195379590563
      y 2340.8986174394113
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_255"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 198
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_33"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1014"
      uniprot "NA"
    ]
    graphics [
      x 1864.7135131229643
      y 1247.4399080952162
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 199
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740573;urn:miriam:uniprot:P0DTC7"
      hgnc "NA"
      map_id "M18_215"
      name "Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2245"
      uniprot "UNIPROT:P0DTC7"
    ]
    graphics [
      x 2092.310124847433
      y 1136.8108952297923
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_215"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 200
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_110"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re859"
      uniprot "NA"
    ]
    graphics [
      x 2010.6754553044161
      y 1096.2194643678463
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 201
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_75"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1088"
      uniprot "NA"
    ]
    graphics [
      x 1196.9761760299218
      y 1341.805510941243
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 202
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:refseq:NC_045512"
      hgnc "NA"
      map_id "M18_233"
      name "(_minus_)ss_space_gRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2314"
      uniprot "NA"
    ]
    graphics [
      x 1286.2651862955631
      y 1271.9161925295703
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_233"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 203
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:refseq:NC_045512"
      hgnc "NA"
      map_id "M18_185"
      name "(_plus_)ss_space_gRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2153"
      uniprot "NA"
    ]
    graphics [
      x 1025.620431260825
      y 1373.7743700561768
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_185"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 204
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_51"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1057"
      uniprot "NA"
    ]
    graphics [
      x 1175.80348917227
      y 1201.112469911567
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 205
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_234"
      name "sa2314_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2315"
      uniprot "NA"
    ]
    graphics [
      x 1196.8690965473966
      y 1063.827357753742
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_234"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 206
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTC1;urn:miriam:ec-code:3.4.22.-;urn:miriam:ncbigene:43740578"
      hgnc "NA"
      map_id "M18_139"
      name "pp1a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1789"
      uniprot "UNIPROT:P0DTC1"
    ]
    graphics [
      x 1659.3491804867372
      y 809.4208526851601
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_139"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 207
    zlevel -1

    cd19dm [
      annotation "PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_89"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1104"
      uniprot "NA"
    ]
    graphics [
      x 1464.282202831368
      y 713.417067901235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 208
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTC1;urn:miriam:ec-code:3.4.22.-;urn:miriam:ncbigene:43740578"
      hgnc "NA"
      map_id "M18_201"
      name "pp1a_space_Nsp3_minus_11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2220"
      uniprot "UNIPROT:P0DTC1"
    ]
    graphics [
      x 1281.0764151479427
      y 654.1557127581144
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_201"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 209
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbiprotein:YP_009725297"
      hgnc "NA"
      map_id "M18_270"
      name "Nsp1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2356"
      uniprot "NA"
    ]
    graphics [
      x 1719.8297387866523
      y 602.7541127943501
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_270"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 210
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_27"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1008"
      uniprot "NA"
    ]
    graphics [
      x 1792.217726570378
      y 2354.8347616465617
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 211
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "M18_141"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1857"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 1888.8678238006344
      y 2304.4413804098103
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_141"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 212
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_266"
      name "Orf7b_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2352"
      uniprot "NA"
    ]
    graphics [
      x 779.8115438677888
      y 1869.9160044894386
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_266"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 213
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_85"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1098"
      uniprot "NA"
    ]
    graphics [
      x 682.9979831394028
      y 1978.5174828240552
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 214
    zlevel -1

    cd19dm [
      annotation "PUBMED:23035226"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_90"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re1105"
      uniprot "NA"
    ]
    graphics [
      x 1892.3538303931678
      y 546.0431222055729
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 215
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0006412"
      hgnc "NA"
      map_id "M18_272"
      name "Host_space_translation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa2358"
      uniprot "NA"
    ]
    graphics [
      x 2005.6083452672447
      y 615.561314962241
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_272"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 216
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_80"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1093"
      uniprot "NA"
    ]
    graphics [
      x 1397.2046611138067
      y 1350.5440686879444
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 217
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_176"
      name "Orf6_space__space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2108"
      uniprot "NA"
    ]
    graphics [
      x 1360.2100651098635
      y 1162.5179826426713
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_176"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 218
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_38"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1019"
      uniprot "NA"
    ]
    graphics [
      x 535.840634313907
      y 495.54350000734735
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 219
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTD3"
      hgnc "NA"
      map_id "M18_220"
      name "Orf14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2250"
      uniprot "UNIPROT:P0DTD3"
    ]
    graphics [
      x 502.0539852339996
      y 367.49654164669914
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_220"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 220
    zlevel -1

    cd19dm [
      annotation "PUBMED:15564471"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_92"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1107"
      uniprot "NA"
    ]
    graphics [
      x 1034.265887788227
      y 1190.818656128598
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 221
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_241"
      name "Orf7a_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2324"
      uniprot "NA"
    ]
    graphics [
      x 1356.2780902129903
      y 1882.6335093131254
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_241"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 222
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_68"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1080"
      uniprot "NA"
    ]
    graphics [
      x 1269.50048289769
      y 1781.509003508159
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 223
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_260"
      name "Orf7a_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2346"
      uniprot "NA"
    ]
    graphics [
      x 1358.3526963029385
      y 1983.7778702689764
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_260"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 224
    zlevel -1

    cd19dm [
      annotation "PUBMED:32047258"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_118"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re952"
      uniprot "NA"
    ]
    graphics [
      x 1329.7053270624901
      y 1351.0185468132163
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 225
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_61"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1073"
      uniprot "NA"
    ]
    graphics [
      x 1429.614917822327
      y 2262.7263503582735
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 226
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_253"
      name "sa2300_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2338"
      uniprot "NA"
    ]
    graphics [
      x 1528.9206910684047
      y 2167.84292682935
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_253"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 227
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_79"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1092"
      uniprot "NA"
    ]
    graphics [
      x 1413.8477528440928
      y 2086.9798013800564
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 228
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_48"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1029"
      uniprot "NA"
    ]
    graphics [
      x 713.9541584588394
      y 1773.6041808212153
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 229
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1064"
      uniprot "NA"
    ]
    graphics [
      x 2012.851867115207
      y 1953.3171652717542
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 230
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_246"
      name "sa2292_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2329"
      uniprot "NA"
    ]
    graphics [
      x 2056.4681837435105
      y 1825.706252873973
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_246"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 231
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_1"
      name "Nucleocapsid"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa353"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 2024.7478948584735
      y 870.2479521590049
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 232
    zlevel -1

    cd19dm [
      annotation "PUBMED:32142651;PUBMED:32094589"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_106"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re841"
      uniprot "NA"
    ]
    graphics [
      x 1991.1605125561414
      y 1001.1211569803312
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 233
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S;urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:S;HGNC_SYMBOL:ACE2"
      map_id "M18_16"
      name "ACE2:SPIKE_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa430"
      uniprot "UNIPROT:P0DTC2;UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1922.0294840729057
      y 1310.483590820897
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 234
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_2"
      name "Nucleocapsid"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa357"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 1859.1181973572088
      y 842.7051073388602
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 235
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC3;urn:miriam:ncbigene:43740569"
      hgnc "NA"
      map_id "M18_144"
      name "Orf3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1873"
      uniprot "UNIPROT:P0DTC3"
    ]
    graphics [
      x 329.19269179448156
      y 1116.4161758213206
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_144"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 236
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_35"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1016"
      uniprot "NA"
    ]
    graphics [
      x 194.81157390801786
      y 1016.0747561674782
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 237
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC3;urn:miriam:ncbigene:43740569"
      hgnc "NA"
      map_id "M18_217"
      name "Orf3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2247"
      uniprot "UNIPROT:P0DTC3"
    ]
    graphics [
      x 94.55348259630534
      y 941.1335874550734
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_217"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 238
    zlevel -1

    cd19dm [
      annotation "PUBMED:8830530"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_103"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1123"
      uniprot "NA"
    ]
    graphics [
      x 1190.7864427238676
      y 1757.595404455003
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 239
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_235"
      name "Orf7b_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2318"
      uniprot "NA"
    ]
    graphics [
      x 1067.2392680793948
      y 1761.8851211246656
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_235"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 240
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_242"
      name "S_space_(_minus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2325"
      uniprot "NA"
    ]
    graphics [
      x 1085.992442589962
      y 1852.209261038538
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_242"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 241
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_98"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1113"
      uniprot "NA"
    ]
    graphics [
      x 1786.178610983562
      y 1040.5539013765256
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 242
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_9"
      name "Nucleocapsid"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa391"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 879.6517862277174
      y 1604.1256107283684
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 243
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_112"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re895"
      uniprot "NA"
    ]
    graphics [
      x 929.248091424137
      y 1865.9797813367782
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 244
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_8"
      name "Nucleocapsid"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa389"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 990.5216164882783
      y 2033.7571266964055
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 245
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:ACE2"
      map_id "M18_124"
      name "ACE2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1462"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1826.49950840061
      y 1692.9428816843867
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_124"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 246
    zlevel -1

    cd19dm [
      annotation "PUBMED:32970989;PUBMED:32142651;PUBMED:32094589"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_105"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re838"
      uniprot "NA"
    ]
    graphics [
      x 1872.0667922006403
      y 1564.3006278255032
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 247
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "M18_163"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2040"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 1938.5350710945122
      y 1667.0520030678786
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_163"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 248
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "M18_187"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2178"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 1748.2179584980568
      y 1588.506028282376
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_187"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 249
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A28815"
      hgnc "NA"
      map_id "M18_283"
      name "Heparan_space_sulfate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa2369"
      uniprot "NA"
    ]
    graphics [
      x 1705.4827623651609
      y 1632.639237166009
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_283"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 250
    zlevel -1

    cd19dm [
      annotation "PUBMED:11142;PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_67"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1079"
      uniprot "NA"
    ]
    graphics [
      x 990.4710823143812
      y 1739.6751193437221
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 251
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_259"
      name "S_space_ds_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2345"
      uniprot "NA"
    ]
    graphics [
      x 801.0564462519254
      y 1739.4566253734085
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_259"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 252
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_84"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1097"
      uniprot "NA"
    ]
    graphics [
      x 947.1221081812298
      y 664.2448466176006
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 253
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_111"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re894"
      uniprot "NA"
    ]
    graphics [
      x 1082.6280431091275
      y 2215.265101698327
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 254
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_7"
      name "Nucleocapsid"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa387"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 1219.159654201126
      y 2188.7324551174893
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 255
    zlevel -1

    cd19dm [
      annotation "PUBMED:23035226"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_22"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re1000"
      uniprot "NA"
    ]
    graphics [
      x 1061.5232567081753
      y 2046.1424675219805
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 256
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0006412"
      hgnc "NA"
      map_id "M18_207"
      name "Host_space_translation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa2237"
      uniprot "NA"
    ]
    graphics [
      x 947.8290681941753
      y 2180.467667422887
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_207"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 257
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.go:GO%3A0019013;urn:miriam:hgnc.symbol:N;urn:miriam:ncbigene:43740575;urn:miriam:uniprot:P0DTC9;urn:miriam:refseq:NC_045512"
      hgnc "HGNC_SYMBOL:N"
      map_id "M18_10"
      name "Nucleocapsid"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa397"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 865.8359731745788
      y 1302.258226326246
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 258
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_116"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re948"
      uniprot "NA"
    ]
    graphics [
      x 898.2100442869543
      y 1431.3366326032433
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 259
    zlevel -1

    cd19dm [
      annotation "PUBMED:21203998"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_23"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1001"
      uniprot "NA"
    ]
    graphics [
      x 1005.2977221513162
      y 640.5344174709505
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 260
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_53"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1063"
      uniprot "NA"
    ]
    graphics [
      x 1486.043533854168
      y 1500.5705118922983
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 261
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1026"
      uniprot "NA"
    ]
    graphics [
      x 1157.8379129164844
      y 1120.7386360813064
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 262
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740572;urn:miriam:uniprot:P0DTC6"
      hgnc "NA"
      map_id "M18_145"
      name "Orf6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1874"
      uniprot "UNIPROT:P0DTC6"
    ]
    graphics [
      x 1315.6006895875557
      y 857.0130145586188
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_145"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 263
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_29"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1010"
      uniprot "NA"
    ]
    graphics [
      x 953.1495567744022
      y 873.8014980977418
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 264
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "M18_143"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1859"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 1120.2333980579294
      y 866.9254259248456
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_143"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 265
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_40"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1021"
      uniprot "NA"
    ]
    graphics [
      x 2210.7845745467166
      y 987.7436946633185
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 266
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740573;urn:miriam:uniprot:P0DTC7"
      hgnc "NA"
      map_id "M18_158"
      name "Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1986"
      uniprot "UNIPROT:P0DTC7"
    ]
    graphics [
      x 2294.840734095472
      y 888.9806917037645
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_158"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 267
    zlevel -1

    cd19dm [
      annotation "PUBMED:28720894"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_119"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re972"
      uniprot "NA"
    ]
    graphics [
      x 836.8631984428005
      y 1448.286132724512
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 268
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1012"
      uniprot "NA"
    ]
    graphics [
      x 482.91113721491274
      y 1710.5525943818263
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 269
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "M18_159"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2009"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 1767.3693287238275
      y 1461.3668434702479
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_159"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 270
    zlevel -1

    cd19dm [
      annotation "PUBMED:32970989;PUBMED:32142651;PUBMED:32155444;PUBMED:32094589"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_108"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re852"
      uniprot "NA"
    ]
    graphics [
      x 1651.6582341462158
      y 1370.248931062499
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 271
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:ACE2"
      map_id "M18_131"
      name "ACE2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1545"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1740.010864869952
      y 1318.795753961015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_131"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 272
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "M18_186"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2173"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 1615.1634744600583
      y 1258.0353631960213
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_186"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 273
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A28815"
      hgnc "NA"
      map_id "M18_284"
      name "Heparan_space_sulfate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa2370"
      uniprot "NA"
    ]
    graphics [
      x 1656.3775372559303
      y 1518.2443518898258
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_284"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 274
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_78"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1091"
      uniprot "NA"
    ]
    graphics [
      x 802.7937780722649
      y 1626.5898646457372
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 275
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_180"
      name "S_space_(_plus_)ss_space_sgmRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa2112"
      uniprot "NA"
    ]
    graphics [
      x 686.7476417904691
      y 1465.3793296508472
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_180"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 276
    zlevel -1

    cd19dm [
      annotation "PUBMED:22438542"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_74"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1086"
      uniprot "NA"
    ]
    graphics [
      x 929.4274840849287
      y 1746.6401696976336
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 277
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_46"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1027"
      uniprot "NA"
    ]
    graphics [
      x 517.9840626235168
      y 1238.5697547341397
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 278
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1015"
      uniprot "NA"
    ]
    graphics [
      x 1381.5364814603925
      y 641.3878662913178
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 279
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "urn:miriam:ncbigene:43740572;urn:miriam:uniprot:P0DTC6"
      hgnc "NA"
      map_id "M18_216"
      name "Orf6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa2246"
      uniprot "UNIPROT:P0DTC6"
    ]
    graphics [
      x 1398.6827329949285
      y 496.0534258666696
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_216"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 280
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1028"
      uniprot "NA"
    ]
    graphics [
      x 492.47008374254403
      y 1431.887203330001
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 281
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_59"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1070"
      uniprot "NA"
    ]
    graphics [
      x 362.75400788760976
      y 705.8514668544833
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 282
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_251"
      name "sa2298_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2335"
      uniprot "NA"
    ]
    graphics [
      x 332.9288992325721
      y 578.4241387184319
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_251"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 283
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_60"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1072"
      uniprot "NA"
    ]
    graphics [
      x 922.5894557401933
      y 2120.261812332521
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 284
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_252"
      name "sa2299_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa2337"
      uniprot "NA"
    ]
    graphics [
      x 1104.3818754919885
      y 2142.3456097061126
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_252"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 285
    zlevel -1

    cd19dm [
      annotation "PUBMED:11907209"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_95"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1110"
      uniprot "NA"
    ]
    graphics [
      x 1277.4194459278729
      y 543.4776470792776
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 286
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:27712623"
      count 1
      diagram "C19DMap:Virus replication cycle"
      full_annotation "NA"
      hgnc "NA"
      map_id "M18_43"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re1024"
      uniprot "NA"
    ]
    graphics [
      x 602.0261289185637
      y 1305.3193526534758
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M18_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 287
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_188"
      target_id "M18_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 2
    target 3
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_121"
      target_id "M18_164"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 4
    target 5
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_173"
      target_id "M18_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 6
    target 5
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 5
    target 7
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_44"
      target_id "M18_146"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 8
    target 9
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_135"
      target_id "M18_114"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 9
    target 10
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_114"
      target_id "M18_151"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 11
    target 12
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_174"
      target_id "M18_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 6
    target 12
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 12
    target 13
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_50"
      target_id "M18_147"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 14
    target 15
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_3"
      target_id "M18_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 16
    target 15
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_167"
      target_id "M18_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 17
    target 15
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_165"
      target_id "M18_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 18
    target 15
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_166"
      target_id "M18_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 3
    target 15
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M18_164"
      target_id "M18_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 15
    target 19
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_120"
      target_id "M18_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 15
    target 20
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_120"
      target_id "M18_182"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 304
    source 15
    target 21
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_120"
      target_id "M18_183"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 305
    source 15
    target 22
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_120"
      target_id "M18_184"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 306
    source 23
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_192"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 307
    source 25
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_191"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 308
    source 26
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_197"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 309
    source 27
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_198"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 310
    source 28
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_190"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 311
    source 29
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_189"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 312
    source 30
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_193"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 313
    source 31
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_194"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 314
    source 32
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_195"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 315
    source 33
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_196"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 316
    source 34
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_203"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 317
    source 35
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_205"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 318
    source 36
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_281"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 319
    source 37
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_277"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 320
    source 38
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_276"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 321
    source 39
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_275"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 322
    source 40
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_274"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 323
    source 41
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_200"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 324
    source 42
    target 24
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_271"
      target_id "M18_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 325
    source 24
    target 43
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_97"
      target_id "M18_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 326
    source 44
    target 45
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_6"
      target_id "M18_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 327
    source 46
    target 45
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_160"
      target_id "M18_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 328
    source 47
    target 45
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_161"
      target_id "M18_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 329
    source 48
    target 45
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_162"
      target_id "M18_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 330
    source 49
    target 45
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M18_132"
      target_id "M18_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 331
    source 50
    target 45
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M18_286"
      target_id "M18_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 332
    source 51
    target 45
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M18_21"
      target_id "M18_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 333
    source 45
    target 19
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_109"
      target_id "M18_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 334
    source 45
    target 52
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_109"
      target_id "M18_169"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 335
    source 45
    target 53
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_109"
      target_id "M18_170"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 336
    source 45
    target 54
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_109"
      target_id "M18_171"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 337
    source 55
    target 56
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_263"
      target_id "M18_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 338
    source 56
    target 57
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_82"
      target_id "M18_229"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 339
    source 56
    target 58
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_82"
      target_id "M18_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 340
    source 59
    target 60
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_213"
      target_id "M18_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 341
    source 59
    target 60
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "M18_213"
      target_id "M18_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 342
    source 60
    target 34
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_93"
      target_id "M18_203"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 343
    source 60
    target 35
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_93"
      target_id "M18_205"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 344
    source 61
    target 62
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_237"
      target_id "M18_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 345
    source 43
    target 62
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 346
    source 62
    target 63
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_72"
      target_id "M18_264"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 347
    source 43
    target 64
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_12"
      target_id "M18_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 348
    source 64
    target 65
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_102"
      target_id "M18_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 349
    source 10
    target 66
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_151"
      target_id "M18_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 350
    source 66
    target 65
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_52"
      target_id "M18_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 351
    source 67
    target 68
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_129"
      target_id "M18_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 352
    source 69
    target 68
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_285"
      target_id "M18_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 353
    source 68
    target 51
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_104"
      target_id "M18_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 354
    source 70
    target 71
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_243"
      target_id "M18_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 355
    source 43
    target 71
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 356
    source 71
    target 72
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_66"
      target_id "M18_258"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 357
    source 73
    target 74
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_149"
      target_id "M18_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 358
    source 74
    target 75
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_37"
      target_id "M18_219"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 359
    source 76
    target 77
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_239"
      target_id "M18_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 360
    source 43
    target 77
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 361
    source 77
    target 78
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_70"
      target_id "M18_262"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 362
    source 79
    target 80
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_244"
      target_id "M18_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 363
    source 43
    target 80
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 364
    source 80
    target 81
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_65"
      target_id "M18_257"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 365
    source 26
    target 82
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_197"
      target_id "M18_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 366
    source 82
    target 83
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_96"
      target_id "M18_222"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 367
    source 84
    target 85
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_214"
      target_id "M18_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 368
    source 84
    target 85
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "M18_214"
      target_id "M18_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 369
    source 85
    target 86
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_26"
      target_id "M18_206"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 370
    source 85
    target 87
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_26"
      target_id "M18_211"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 371
    source 85
    target 88
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_26"
      target_id "M18_212"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 372
    source 89
    target 90
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_225"
      target_id "M18_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 373
    source 90
    target 91
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_56"
      target_id "M18_248"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 374
    source 92
    target 93
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_267"
      target_id "M18_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 375
    source 43
    target 93
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 376
    source 93
    target 94
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_86"
      target_id "M18_268"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 377
    source 95
    target 96
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_138"
      target_id "M18_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 378
    source 96
    target 97
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_32"
      target_id "M18_154"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 379
    source 72
    target 98
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_258"
      target_id "M18_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 380
    source 98
    target 99
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_77"
      target_id "M18_224"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 381
    source 98
    target 100
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_77"
      target_id "M18_179"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 382
    source 101
    target 102
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_240"
      target_id "M18_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 383
    source 43
    target 102
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 384
    source 102
    target 103
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_69"
      target_id "M18_261"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 385
    source 104
    target 105
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_140"
      target_id "M18_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 386
    source 104
    target 105
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "M18_140"
      target_id "M18_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 387
    source 105
    target 106
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_88"
      target_id "M18_202"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 388
    source 105
    target 41
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_88"
      target_id "M18_200"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 389
    source 105
    target 107
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_88"
      target_id "M18_199"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 390
    source 108
    target 109
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_157"
      target_id "M18_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 391
    source 110
    target 109
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_15"
      target_id "M18_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 392
    source 109
    target 8
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_115"
      target_id "M18_135"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 393
    source 111
    target 112
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_19"
      target_id "M18_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 394
    source 35
    target 112
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "MODULATION"
      source_id "M18_205"
      target_id "M18_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 395
    source 34
    target 112
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "MODULATION"
      source_id "M18_203"
      target_id "M18_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 396
    source 36
    target 112
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "MODULATION"
      source_id "M18_281"
      target_id "M18_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 397
    source 33
    target 112
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "MODULATION"
      source_id "M18_196"
      target_id "M18_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 398
    source 112
    target 113
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_123"
      target_id "M18_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 399
    source 81
    target 114
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_257"
      target_id "M18_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 400
    source 114
    target 115
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_76"
      target_id "M18_223"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 401
    source 114
    target 116
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_76"
      target_id "M18_181"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 402
    source 78
    target 117
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_262"
      target_id "M18_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 403
    source 117
    target 118
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_81"
      target_id "M18_228"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 404
    source 117
    target 119
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_81"
      target_id "M18_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 405
    source 120
    target 121
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_20"
      target_id "M18_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 406
    source 121
    target 43
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_101"
      target_id "M18_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 407
    source 122
    target 123
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_245"
      target_id "M18_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 408
    source 43
    target 123
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 409
    source 123
    target 124
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_64"
      target_id "M18_256"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 410
    source 94
    target 125
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_268"
      target_id "M18_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 411
    source 125
    target 126
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_87"
      target_id "M18_269"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 412
    source 125
    target 108
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_87"
      target_id "M18_157"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 413
    source 63
    target 127
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_264"
      target_id "M18_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 414
    source 127
    target 128
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_83"
      target_id "M18_230"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 415
    source 127
    target 129
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_83"
      target_id "M18_177"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 416
    source 130
    target 131
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_150"
      target_id "M18_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 417
    source 131
    target 132
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_36"
      target_id "M18_218"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 418
    source 116
    target 133
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_181"
      target_id "M18_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 419
    source 6
    target 133
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 420
    source 133
    target 134
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_41"
      target_id "M18_136"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 421
    source 135
    target 136
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_133"
      target_id "M18_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 422
    source 136
    target 137
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_113"
      target_id "M18_155"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 423
    source 138
    target 139
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_134"
      target_id "M18_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 424
    source 140
    target 139
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_13"
      target_id "M18_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 425
    source 139
    target 104
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_99"
      target_id "M18_140"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 426
    source 141
    target 142
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_238"
      target_id "M18_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 427
    source 43
    target 142
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 428
    source 142
    target 55
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_71"
      target_id "M18_263"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 429
    source 143
    target 144
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_273"
      target_id "M18_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 430
    source 144
    target 145
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_91"
      target_id "M18_279"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 431
    source 106
    target 146
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_202"
      target_id "M18_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 432
    source 146
    target 84
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_25"
      target_id "M18_214"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 433
    source 147
    target 148
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_231"
      target_id "M18_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 434
    source 148
    target 149
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_62"
      target_id "M18_254"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 435
    source 88
    target 150
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_212"
      target_id "M18_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 436
    source 150
    target 59
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_24"
      target_id "M18_213"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 437
    source 126
    target 151
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_269"
      target_id "M18_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 438
    source 151
    target 152
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_100"
      target_id "M18_282"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 439
    source 153
    target 154
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_210"
      target_id "M18_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 440
    source 153
    target 154
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "M18_210"
      target_id "M18_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 441
    source 154
    target 155
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_122"
      target_id "M18_204"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 442
    source 154
    target 143
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_122"
      target_id "M18_273"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 443
    source 154
    target 156
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_122"
      target_id "M18_280"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 444
    source 100
    target 157
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_179"
      target_id "M18_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 445
    source 6
    target 157
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 446
    source 157
    target 158
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_42"
      target_id "M18_137"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 447
    source 134
    target 159
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_136"
      target_id "M18_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 448
    source 159
    target 160
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_30"
      target_id "M18_152"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 449
    source 155
    target 161
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_204"
      target_id "M18_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 450
    source 156
    target 161
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "M18_280"
      target_id "M18_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 451
    source 161
    target 36
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_94"
      target_id "M18_281"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 452
    source 161
    target 37
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_94"
      target_id "M18_277"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 453
    source 161
    target 38
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_94"
      target_id "M18_276"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 454
    source 161
    target 39
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_94"
      target_id "M18_275"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 455
    source 161
    target 40
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_94"
      target_id "M18_274"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 456
    source 161
    target 162
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_94"
      target_id "M18_278"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 457
    source 163
    target 164
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_227"
      target_id "M18_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 458
    source 164
    target 165
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_58"
      target_id "M18_250"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 459
    source 166
    target 167
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_153"
      target_id "M18_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 460
    source 167
    target 168
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_28"
      target_id "M18_142"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 461
    source 169
    target 170
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_226"
      target_id "M18_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 462
    source 170
    target 171
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_57"
      target_id "M18_249"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 463
    source 13
    target 172
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_147"
      target_id "M18_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 464
    source 172
    target 173
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_39"
      target_id "M18_221"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 465
    source 174
    target 175
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_178"
      target_id "M18_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 466
    source 6
    target 175
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 467
    source 175
    target 176
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_49"
      target_id "M18_148"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 468
    source 99
    target 177
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_224"
      target_id "M18_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 469
    source 177
    target 178
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_55"
      target_id "M18_247"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 470
    source 179
    target 180
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_4"
      target_id "M18_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 471
    source 181
    target 180
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "M18_156"
      target_id "M18_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 472
    source 182
    target 180
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "M18_128"
      target_id "M18_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 473
    source 183
    target 180
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "INHIBITION"
      source_id "M18_130"
      target_id "M18_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 474
    source 180
    target 184
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_117"
      target_id "M18_168"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 475
    source 180
    target 185
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_117"
      target_id "M18_209"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 476
    source 180
    target 67
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_117"
      target_id "M18_129"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 477
    source 180
    target 186
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_117"
      target_id "M18_208"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 478
    source 187
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_236"
      target_id "M18_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 479
    source 43
    target 188
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 480
    source 188
    target 189
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_73"
      target_id "M18_265"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 481
    source 190
    target 191
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_17"
      target_id "M18_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 482
    source 192
    target 191
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "M18_126"
      target_id "M18_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 483
    source 193
    target 191
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "M18_127"
      target_id "M18_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 484
    source 191
    target 1
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_107"
      target_id "M18_188"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 485
    source 191
    target 194
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_107"
      target_id "M18_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 486
    source 195
    target 196
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_232"
      target_id "M18_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 487
    source 196
    target 197
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_63"
      target_id "M18_255"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 488
    source 7
    target 198
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_146"
      target_id "M18_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 489
    source 198
    target 199
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_33"
      target_id "M18_215"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 490
    source 19
    target 200
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_5"
      target_id "M18_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 491
    source 200
    target 138
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_110"
      target_id "M18_134"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 492
    source 200
    target 135
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_110"
      target_id "M18_133"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 493
    source 124
    target 201
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_256"
      target_id "M18_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 494
    source 201
    target 202
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_75"
      target_id "M18_233"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 495
    source 201
    target 203
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_75"
      target_id "M18_185"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 496
    source 202
    target 204
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_233"
      target_id "M18_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 497
    source 204
    target 205
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_51"
      target_id "M18_234"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 498
    source 206
    target 207
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_139"
      target_id "M18_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 499
    source 206
    target 207
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "M18_139"
      target_id "M18_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 500
    source 207
    target 208
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_89"
      target_id "M18_201"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 501
    source 207
    target 42
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_89"
      target_id "M18_271"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 502
    source 207
    target 209
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_89"
      target_id "M18_270"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 503
    source 160
    target 210
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_152"
      target_id "M18_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 504
    source 210
    target 211
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_27"
      target_id "M18_141"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 505
    source 212
    target 213
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_266"
      target_id "M18_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 506
    source 213
    target 195
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_85"
      target_id "M18_232"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 507
    source 213
    target 11
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_85"
      target_id "M18_174"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 508
    source 209
    target 214
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_270"
      target_id "M18_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 509
    source 214
    target 215
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_90"
      target_id "M18_272"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 510
    source 103
    target 216
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_261"
      target_id "M18_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 511
    source 216
    target 163
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_80"
      target_id "M18_227"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 512
    source 216
    target 217
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_80"
      target_id "M18_176"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 513
    source 176
    target 218
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_148"
      target_id "M18_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 514
    source 218
    target 219
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_38"
      target_id "M18_220"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 515
    source 145
    target 220
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_279"
      target_id "M18_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 516
    source 145
    target 220
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "M18_279"
      target_id "M18_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 517
    source 220
    target 35
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_92"
      target_id "M18_205"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 518
    source 220
    target 34
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_92"
      target_id "M18_203"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 519
    source 221
    target 222
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_241"
      target_id "M18_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 520
    source 43
    target 222
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 521
    source 222
    target 223
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_68"
      target_id "M18_260"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 522
    source 184
    target 224
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_168"
      target_id "M18_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 523
    source 224
    target 49
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_118"
      target_id "M18_132"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 524
    source 128
    target 225
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_230"
      target_id "M18_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 525
    source 225
    target 226
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_61"
      target_id "M18_253"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 526
    source 223
    target 227
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_260"
      target_id "M18_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 527
    source 227
    target 169
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_79"
      target_id "M18_226"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 528
    source 227
    target 4
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_79"
      target_id "M18_173"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 529
    source 129
    target 228
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_177"
      target_id "M18_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 530
    source 6
    target 228
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 531
    source 228
    target 73
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_48"
      target_id "M18_149"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 532
    source 115
    target 229
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_223"
      target_id "M18_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 533
    source 229
    target 230
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_54"
      target_id "M18_246"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 534
    source 231
    target 232
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_1"
      target_id "M18_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 535
    source 233
    target 232
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M18_16"
      target_id "M18_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 536
    source 232
    target 234
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_106"
      target_id "M18_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 537
    source 235
    target 236
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_144"
      target_id "M18_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 538
    source 236
    target 237
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_35"
      target_id "M18_217"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 539
    source 122
    target 238
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_245"
      target_id "M18_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 540
    source 43
    target 238
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 541
    source 238
    target 239
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_235"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 542
    source 238
    target 92
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_267"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 543
    source 238
    target 79
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_244"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 544
    source 238
    target 70
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_243"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 545
    source 238
    target 240
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_242"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 546
    source 238
    target 221
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_241"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 547
    source 238
    target 101
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_240"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 548
    source 238
    target 76
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_239"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 549
    source 238
    target 141
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_238"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 550
    source 238
    target 61
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_237"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 551
    source 238
    target 187
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_103"
      target_id "M18_236"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 552
    source 138
    target 241
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_134"
      target_id "M18_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 553
    source 140
    target 241
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_13"
      target_id "M18_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 554
    source 241
    target 206
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_98"
      target_id "M18_139"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 555
    source 242
    target 243
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_9"
      target_id "M18_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 556
    source 243
    target 244
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_112"
      target_id "M18_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 557
    source 245
    target 246
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_124"
      target_id "M18_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 558
    source 247
    target 246
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_163"
      target_id "M18_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 559
    source 248
    target 246
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_187"
      target_id "M18_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 560
    source 249
    target 246
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M18_283"
      target_id "M18_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 561
    source 246
    target 233
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_105"
      target_id "M18_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 562
    source 240
    target 250
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_242"
      target_id "M18_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 563
    source 43
    target 250
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 564
    source 250
    target 251
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_67"
      target_id "M18_259"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 565
    source 189
    target 252
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_265"
      target_id "M18_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 566
    source 252
    target 147
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_84"
      target_id "M18_231"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 567
    source 252
    target 174
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_84"
      target_id "M18_178"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 568
    source 244
    target 253
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_8"
      target_id "M18_111"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 569
    source 253
    target 254
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_111"
      target_id "M18_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 570
    source 107
    target 255
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_199"
      target_id "M18_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 571
    source 255
    target 256
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_22"
      target_id "M18_207"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 572
    source 257
    target 258
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_10"
      target_id "M18_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 573
    source 258
    target 242
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_116"
      target_id "M18_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 574
    source 208
    target 259
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_201"
      target_id "M18_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 575
    source 259
    target 153
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_23"
      target_id "M18_210"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 576
    source 138
    target 260
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_134"
      target_id "M18_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 577
    source 43
    target 260
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 578
    source 260
    target 122
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_53"
      target_id "M18_245"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 579
    source 217
    target 261
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_176"
      target_id "M18_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 580
    source 6
    target 261
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 581
    source 261
    target 262
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_45"
      target_id "M18_145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 582
    source 97
    target 263
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_154"
      target_id "M18_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 583
    source 263
    target 264
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_29"
      target_id "M18_143"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 584
    source 199
    target 265
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_215"
      target_id "M18_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 585
    source 265
    target 266
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_40"
      target_id "M18_158"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 586
    source 65
    target 267
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_11"
      target_id "M18_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 587
    source 203
    target 267
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_185"
      target_id "M18_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 588
    source 267
    target 257
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_119"
      target_id "M18_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 589
    source 267
    target 120
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_119"
      target_id "M18_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 590
    source 158
    target 268
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_137"
      target_id "M18_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 591
    source 268
    target 166
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_31"
      target_id "M18_153"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 592
    source 269
    target 270
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_159"
      target_id "M18_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 593
    source 271
    target 270
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_131"
      target_id "M18_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 594
    source 272
    target 270
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_186"
      target_id "M18_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 595
    source 273
    target 270
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M18_284"
      target_id "M18_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 596
    source 270
    target 179
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_108"
      target_id "M18_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 597
    source 251
    target 274
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_259"
      target_id "M18_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 598
    source 274
    target 89
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_78"
      target_id "M18_225"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 599
    source 274
    target 275
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_78"
      target_id "M18_180"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 600
    source 239
    target 276
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_235"
      target_id "M18_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 601
    source 43
    target 276
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_12"
      target_id "M18_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 602
    source 276
    target 212
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_74"
      target_id "M18_266"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 603
    source 119
    target 277
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_172"
      target_id "M18_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 604
    source 6
    target 277
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 605
    source 277
    target 235
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_46"
      target_id "M18_144"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 606
    source 262
    target 278
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_145"
      target_id "M18_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 607
    source 278
    target 279
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_34"
      target_id "M18_216"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 608
    source 58
    target 280
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_175"
      target_id "M18_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 609
    source 6
    target 280
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 610
    source 280
    target 130
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_47"
      target_id "M18_150"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 611
    source 118
    target 281
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_228"
      target_id "M18_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 612
    source 281
    target 282
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_59"
      target_id "M18_251"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 613
    source 57
    target 283
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_229"
      target_id "M18_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 614
    source 283
    target 284
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_60"
      target_id "M18_252"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 615
    source 86
    target 285
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_206"
      target_id "M18_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 616
    source 87
    target 285
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CATALYSIS"
      source_id "M18_211"
      target_id "M18_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 617
    source 285
    target 28
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_190"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 618
    source 285
    target 29
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_189"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 619
    source 285
    target 23
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_192"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 620
    source 285
    target 30
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_193"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 621
    source 285
    target 31
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_194"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 622
    source 285
    target 32
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_195"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 623
    source 285
    target 33
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_196"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 624
    source 285
    target 25
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_191"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 625
    source 285
    target 26
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_197"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 626
    source 285
    target 27
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_95"
      target_id "M18_198"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 627
    source 275
    target 286
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "CONSPUMPTION"
      source_id "M18_180"
      target_id "M18_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 628
    source 6
    target 286
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "TRIGGER"
      source_id "M18_14"
      target_id "M18_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 629
    source 286
    target 95
    cd19dm [
      diagram "C19DMap:Virus replication cycle"
      edge_type "PRODUCTION"
      source_id "M18_43"
      target_id "M18_138"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
