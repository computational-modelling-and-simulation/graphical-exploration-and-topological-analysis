# generated with VANTED V2.8.2 at Fri Mar 04 09:56:59 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023"
      hgnc "NA"
      map_id "M16_238"
      name "s67"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa79"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 1525.696221069314
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_238"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_77"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re33"
      uniprot "NA"
    ]
    graphics [
      x 176.19580414718428
      y 1451.453519383947
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:P52630;urn:miriam:uniprot:P52630;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc:11363;urn:miriam:ncbigene:6773;urn:miriam:ncbigene:6773;urn:miriam:refseq:NM_005419;urn:miriam:ensembl:ENSG00000170581;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:refseq:NM_007315;urn:miriam:hgnc.symbol:STAT1;urn:miriam:hgnc.symbol:STAT1;urn:miriam:uniprot:P42224;urn:miriam:uniprot:P42224;urn:miriam:ncbigene:6772;urn:miriam:ncbigene:6772;urn:miriam:hgnc:11362;urn:miriam:ensembl:ENSG00000115415;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q00978;urn:miriam:uniprot:Q00978;urn:miriam:refseq:NM_001385400;urn:miriam:ensembl:ENSG00000213928;urn:miriam:hgnc.symbol:IRF9;urn:miriam:ncbigene:10379;urn:miriam:hgnc.symbol:IRF9;urn:miriam:ncbigene:10379;urn:miriam:hgnc:6131"
      hgnc "HGNC_SYMBOL:STAT2;HGNC_SYMBOL:STAT1;HGNC_SYMBOL:IRF9"
      map_id "M16_31"
      name "ISRE"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa6"
      uniprot "UNIPROT:P52630;UNIPROT:P42224;UNIPROT:Q00978"
    ]
    graphics [
      x 400.74169803726784
      y 1618.4595358495949
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:OAS3;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q9Y6K5;urn:miriam:ensembl:ENSG00000111331;urn:miriam:refseq:NM_006187;urn:miriam:hgnc:8088;urn:miriam:ncbigene:4940"
      hgnc "HGNC_SYMBOL:OAS3"
      map_id "M16_236"
      name "OAS3"
      node_subtype "GENE"
      node_type "species"
      org_id "sa77"
      uniprot "UNIPROT:Q9Y6K5"
    ]
    graphics [
      x 136.0291202499261
      y 1583.91878971892
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_236"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:pubmed:24622840;urn:miriam:hgnc:6118;urn:miriam:uniprot:Q14653;urn:miriam:uniprot:Q14653;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661;urn:miriam:ncbigene:3661"
      hgnc "HGNC_SYMBOL:IRF3"
      map_id "M16_28"
      name "IRF3_underscore_homodimer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa46"
      uniprot "UNIPROT:Q14653"
    ]
    graphics [
      x 364.8186607247053
      y 1347.2275597803546
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:OAS3;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q9Y6K5;urn:miriam:ensembl:ENSG00000111331;urn:miriam:refseq:NM_006187;urn:miriam:hgnc:8088;urn:miriam:ncbigene:4940"
      hgnc "HGNC_SYMBOL:OAS3"
      map_id "M16_237"
      name "OAS3"
      node_subtype "RNA"
      node_type "species"
      org_id "sa78"
      uniprot "UNIPROT:Q9Y6K5"
    ]
    graphics [
      x 180.7315207625926
      y 1172.5509040848146
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_237"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868"
      hgnc "NA"
      map_id "M16_216"
      name "EIF2AK"
      node_subtype "RNA"
      node_type "species"
      org_id "sa48"
      uniprot "NA"
    ]
    graphics [
      x 832.7282712779727
      y 1468.319020685508
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_216"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_72"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re19"
      uniprot "NA"
    ]
    graphics [
      x 892.8933596377857
      y 1263.6410740809438
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868"
      hgnc "NA"
      map_id "M16_220"
      name "EIF2AK"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa51"
      uniprot "NA"
    ]
    graphics [
      x 887.6206533071928
      y 978.6744544255819
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_220"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:wikipathways:WP4868;urn:miriam:ncbigene:9641;urn:miriam:ncbigene:9641;urn:miriam:hgnc:14552;urn:miriam:refseq:NM_001193321;urn:miriam:pubmed:31226023;urn:miriam:uniprot:Q14164;urn:miriam:uniprot:Q14164;urn:miriam:pubmed:24622840;urn:miriam:ec-code:2.7.11.10;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:ensembl:ENSG00000263528"
      hgnc "HGNC_SYMBOL:IKBKE"
      map_id "M16_111"
      name "IKBKE"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa119"
      uniprot "UNIPROT:Q14164"
    ]
    graphics [
      x 1346.2732338578471
      y 827.6036101823856
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "PUBMED:18353649;PUBMED:31226023;PUBMED:25636800"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_82"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re52"
      uniprot "NA"
    ]
    graphics [
      x 1328.0372269293762
      y 969.0581114465414
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:ncbiprotein:YP_009725299;urn:miriam:uniprot:Nsp3"
      hgnc "NA"
      map_id "M16_114"
      name "Nsp3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa123"
      uniprot "UNIPROT:Nsp3"
    ]
    graphics [
      x 1178.247996904196
      y 1139.475623189978
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q9UHD2;urn:miriam:uniprot:Q9UHD2;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:pubmed:31226023;urn:miriam:pubmed:24622840;urn:miriam:ec-code:2.7.11.1;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110"
      hgnc "HGNC_SYMBOL:TBK1"
      map_id "M16_226"
      name "TBK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa61"
      uniprot "UNIPROT:Q9UHD2"
    ]
    graphics [
      x 1077.161565578685
      y 991.552074681571
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_226"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:wikipathways:WP4868;urn:miriam:ncbigene:9641;urn:miriam:ncbigene:9641;urn:miriam:hgnc:14552;urn:miriam:refseq:NM_001193321;urn:miriam:pubmed:31226023;urn:miriam:uniprot:Q14164;urn:miriam:uniprot:Q14164;urn:miriam:pubmed:24622840;urn:miriam:ec-code:2.7.11.10;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:ensembl:ENSG00000263528"
      hgnc "HGNC_SYMBOL:IKBKE"
      map_id "M16_110"
      name "IKBKE"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa116"
      uniprot "UNIPROT:Q14164"
    ]
    graphics [
      x 1403.782896792003
      y 1090.4527330306319
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:pubmed:24622840;urn:miriam:hgnc:6118;urn:miriam:uniprot:Q14653;urn:miriam:uniprot:Q14653;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661;urn:miriam:ncbigene:3661"
      hgnc "HGNC_SYMBOL:IRF3"
      map_id "M16_113"
      name "IRF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa121"
      uniprot "UNIPROT:Q14653"
    ]
    graphics [
      x 766.3609696604661
      y 919.7579692067085
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_55"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re129"
      uniprot "NA"
    ]
    graphics [
      x 380.9372049011454
      y 874.514217746744
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:pubmed:24622840;urn:miriam:hgnc:6118;urn:miriam:uniprot:Q14653;urn:miriam:uniprot:Q14653;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661;urn:miriam:ncbigene:3661"
      hgnc "HGNC_SYMBOL:IRF3"
      map_id "M16_178"
      name "IRF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa286"
      uniprot "UNIPROT:Q14653"
    ]
    graphics [
      x 259.21547379791093
      y 791.758398423785
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_178"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:pubmed:24622840;urn:miriam:hgnc:6118;urn:miriam:uniprot:Q14653;urn:miriam:uniprot:Q14653;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661;urn:miriam:ncbigene:3661"
      hgnc "HGNC_SYMBOL:IRF3"
      map_id "M16_27"
      name "IRF3_underscore_homodimer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa45"
      uniprot "UNIPROT:Q14653"
    ]
    graphics [
      x 182.84288051357657
      y 1036.7384223650522
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:OAS2;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:P29728;urn:miriam:ncbigene:4939;urn:miriam:hgnc:8087;urn:miriam:ensembl:ENSG00000111335;urn:miriam:refseq:NM_001032731"
      hgnc "HGNC_SYMBOL:OAS2"
      map_id "M16_233"
      name "OAS2"
      node_subtype "RNA"
      node_type "species"
      org_id "sa74"
      uniprot "UNIPROT:P29728"
    ]
    graphics [
      x 466.0796980385061
      y 1614.6662846799504
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_233"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_76"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re32"
      uniprot "NA"
    ]
    graphics [
      x 289.9003579635946
      y 1538.2942281979701
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:OAS2;urn:miriam:hgnc.symbol:OAS2;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:P29728;urn:miriam:uniprot:P29728;urn:miriam:ncbigene:4939;urn:miriam:ncbigene:4939;urn:miriam:hgnc:8087;urn:miriam:ensembl:ENSG00000111335;urn:miriam:refseq:NM_001032731;urn:miriam:ec-code:2.7.7.84"
      hgnc "HGNC_SYMBOL:OAS2"
      map_id "M16_127"
      name "OAS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa158"
      uniprot "UNIPROT:P29728"
    ]
    graphics [
      x 300.92914528122355
      y 1331.6374670726045
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_127"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:hgnc:5433;urn:miriam:ncbigene:3455;urn:miriam:ensembl:ENSG00000159110;urn:miriam:ncbigene:3455;urn:miriam:hgnc.symbol:IFNAR2;urn:miriam:refseq:NM_000874;urn:miriam:hgnc.symbol:IFNAR2;urn:miriam:uniprot:P48551;urn:miriam:uniprot:P48551;urn:miriam:hgnc:5432;urn:miriam:uniprot:P17181;urn:miriam:uniprot:P17181;urn:miriam:refseq:NM_000629;urn:miriam:ensembl:ENSG00000142166;urn:miriam:hgnc.symbol:IFNAR1;urn:miriam:hgnc.symbol:IFNAR1;urn:miriam:ncbigene:3454;urn:miriam:ncbigene:3454"
      hgnc "HGNC_SYMBOL:IFNAR2;HGNC_SYMBOL:IFNAR1"
      map_id "M16_1"
      name "IFNAR"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa1"
      uniprot "UNIPROT:P48551;UNIPROT:P17181"
    ]
    graphics [
      x 574.1474474242808
      y 1169.1440853424106
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:32913009;PUBMED:24362405"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_37"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re103"
      uniprot "NA"
    ]
    graphics [
      x 591.8036502970944
      y 1068.871396438171
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:ensembl:ENSG00000171855;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc:5434;urn:miriam:uniprot:P01574;urn:miriam:uniprot:P01574;urn:miriam:refseq:NM_002176;urn:miriam:ncbigene:3456;urn:miriam:ncbigene:3456"
      hgnc "HGNC_SYMBOL:IFNB1"
      map_id "M16_185"
      name "IFNB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa3"
      uniprot "UNIPROT:P01574"
    ]
    graphics [
      x 638.2494490166288
      y 959.9071072795552
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_185"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ensembl:ENSG00000171855;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc:5434;urn:miriam:uniprot:P01574;urn:miriam:uniprot:P01574;urn:miriam:refseq:NM_002176;urn:miriam:ncbigene:3456;urn:miriam:ncbigene:3456"
      hgnc "HGNC_SYMBOL:IFNB1"
      map_id "M16_174"
      name "IFNB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa27"
      uniprot "UNIPROT:P01574"
    ]
    graphics [
      x 465.8536344975815
      y 1264.4346459684891
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_174"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:hgnc:5433;urn:miriam:ncbigene:3455;urn:miriam:ensembl:ENSG00000159110;urn:miriam:ncbigene:3455;urn:miriam:hgnc.symbol:IFNAR2;urn:miriam:refseq:NM_000874;urn:miriam:hgnc.symbol:IFNAR2;urn:miriam:uniprot:P48551;urn:miriam:uniprot:P48551;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:ensembl:ENSG00000171855;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc:5434;urn:miriam:uniprot:P01574;urn:miriam:uniprot:P01574;urn:miriam:refseq:NM_002176;urn:miriam:ncbigene:3456;urn:miriam:ncbigene:3456;urn:miriam:hgnc:5432;urn:miriam:uniprot:P17181;urn:miriam:uniprot:P17181;urn:miriam:refseq:NM_000629;urn:miriam:ensembl:ENSG00000142166;urn:miriam:hgnc.symbol:IFNAR1;urn:miriam:hgnc.symbol:IFNAR1;urn:miriam:ncbigene:3454;urn:miriam:ncbigene:3454"
      hgnc "HGNC_SYMBOL:IFNAR2;HGNC_SYMBOL:IFNB1;HGNC_SYMBOL:IFNAR1"
      map_id "M16_12"
      name "IFNB1_underscore_IFNAR"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa3"
      uniprot "UNIPROT:P48551;UNIPROT:P01574;UNIPROT:P17181"
    ]
    graphics [
      x 815.3775311809499
      y 823.6137689293465
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:pubmed:24622840;urn:miriam:uniprot:Q9UHD2;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110"
      hgnc "HGNC_SYMBOL:TBK1"
      map_id "M16_225"
      name "TBK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa60"
      uniprot "UNIPROT:Q9UHD2"
    ]
    graphics [
      x 1195.5746306423425
      y 883.9007182489685
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_225"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      annotation "PUBMED:24622840;PUBMED:25636800;PUBMED:26631542;PUBMED:32979938;PUBMED:33337934;PUBMED:32733001"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_74"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re24"
      uniprot "NA"
    ]
    graphics [
      x 1250.9410392991645
      y 1063.4752952175545
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:hgnc.symbol:STING1;urn:miriam:hgnc.symbol:STING1;urn:miriam:pubmed:24622840;urn:miriam:uniprot:Q86WV6;urn:miriam:uniprot:Q86WV6;urn:miriam:ncbigene:340061;urn:miriam:ncbigene:340061;urn:miriam:hgnc:27962;urn:miriam:refseq:NM_198282;urn:miriam:ensembl:ENSG00000184584"
      hgnc "HGNC_SYMBOL:STING1"
      map_id "M16_137"
      name "STING1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa172"
      uniprot "UNIPROT:Q86WV6"
    ]
    graphics [
      x 1407.9784496753812
      y 1268.830687872843
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_137"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:uniprot:Q7Z434;urn:miriam:uniprot:Q7Z434;urn:miriam:pubmed:24622840;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:pubmed:19052324;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746"
      hgnc "HGNC_SYMBOL:MAVS"
      map_id "M16_122"
      name "MAVS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa151"
      uniprot "UNIPROT:Q7Z434"
    ]
    graphics [
      x 1083.3049328691902
      y 1200.358127204698
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:wikipathways:WP4868;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbiprotein:YP_009725308;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M16_132"
      name "Nsp13"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa167"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1263.600402973574
      y 883.5856843737942
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:ncbigene:29108;urn:miriam:ncbigene:29108;urn:miriam:refseq:NM_013258;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:ensembl:ENSG00000103490;urn:miriam:hgnc:16608;urn:miriam:uniprot:Q9ULZ3;urn:miriam:uniprot:Q9ULZ3;urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:Q96P20;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:PYCARD;HGNC_SYMBOL:CASP1;HGNC_SYMBOL:NLRP3"
      map_id "M16_14"
      name "NLRP3_underscore_inflammasome"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa31"
      uniprot "UNIPROT:Q9ULZ3;UNIPROT:P29466;UNIPROT:Q96P20"
    ]
    graphics [
      x 795.360281719487
      y 332.4761854904975
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "PUBMED:32133002;PUBMED:24265316;PUBMED:28531279"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_100"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re86"
      uniprot "NA"
    ]
    graphics [
      x 736.338529604174
      y 268.86712265348444
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ncbiprotein:BCD58755;urn:miriam:uniprot:E"
      hgnc "NA"
      map_id "M16_117"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa131"
      uniprot "UNIPROT:E"
    ]
    graphics [
      x 516.6507183076865
      y 455.3372252148638
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:hgnc:9955;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ncbigene:5970;urn:miriam:ncbigene:5970;urn:miriam:refseq:NM_021975;urn:miriam:hgnc.symbol:RELA;urn:miriam:hgnc.symbol:RELA;urn:miriam:uniprot:Q04206;urn:miriam:uniprot:Q04206;urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998"
      hgnc "HGNC_SYMBOL:RELA;HGNC_SYMBOL:NFKB1"
      map_id "M16_16"
      name "p50_underscore_p65"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa34"
      uniprot "UNIPROT:Q04206;UNIPROT:P19838"
    ]
    graphics [
      x 646.3374894936551
      y 459.75547516393397
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_157"
      name "MNS"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa237"
      uniprot "NA"
    ]
    graphics [
      x 923.9935512455592
      y 274.02952021904593
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_157"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292;urn:miriam:ncbigene:29108;urn:miriam:ncbigene:29108;urn:miriam:refseq:NM_013258;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:ensembl:ENSG00000103490;urn:miriam:hgnc:16608;urn:miriam:uniprot:Q9ULZ3;urn:miriam:uniprot:Q9ULZ3;urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:Q96P20;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:CASP1;HGNC_SYMBOL:PYCARD;HGNC_SYMBOL:NLRP3"
      map_id "M16_13"
      name "NLRP3_underscore_inflammasome"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa30"
      uniprot "UNIPROT:P29466;UNIPROT:Q9ULZ3;UNIPROT:Q96P20"
    ]
    graphics [
      x 870.8695193648816
      y 116.34105519988134
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:hgnc.symbol:STING1;urn:miriam:hgnc.symbol:STING1;urn:miriam:pubmed:24622840;urn:miriam:uniprot:Q86WV6;urn:miriam:uniprot:Q86WV6;urn:miriam:ncbigene:340061;urn:miriam:ncbigene:340061;urn:miriam:hgnc:27962;urn:miriam:refseq:NM_198282;urn:miriam:ensembl:ENSG00000184584"
      hgnc "HGNC_SYMBOL:STING1"
      map_id "M16_136"
      name "STING1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa171"
      uniprot "UNIPROT:Q86WV6"
    ]
    graphics [
      x 1374.8727129662345
      y 1007.4918052368951
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "PUBMED:24622840;PUBMED:22312431"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_92"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re72"
      uniprot "NA"
    ]
    graphics [
      x 1331.8045051002662
      y 1202.8640236457773
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:24622840;urn:miriam:ncbiprotein:YP_009724389"
      hgnc "NA"
      map_id "M16_138"
      name "pp1ab"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa184"
      uniprot "NA"
    ]
    graphics [
      x 1378.4651730772293
      y 1331.504794046808
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_138"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:hgnc:9955;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ncbigene:5970;urn:miriam:ncbigene:5970;urn:miriam:refseq:NM_021975;urn:miriam:hgnc.symbol:RELA;urn:miriam:hgnc.symbol:RELA;urn:miriam:uniprot:Q04206;urn:miriam:uniprot:Q04206;urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998"
      hgnc "HGNC_SYMBOL:RELA;HGNC_SYMBOL:NFKB1"
      map_id "M16_23"
      name "IkB_underscore_p50_underscore_p65"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa41"
      uniprot "UNIPROT:Q04206;UNIPROT:P19838"
    ]
    graphics [
      x 1698.450283801434
      y 1476.394764176213
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_51"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re121"
      uniprot "NA"
    ]
    graphics [
      x 1532.4983614937105
      y 1312.226222800813
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_22"
      name "IKKa_underscore_IKKb_underscore_NEMO"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa40"
      uniprot "NA"
    ]
    graphics [
      x 1669.238526190607
      y 1307.000283466942
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:hgnc:9955;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ncbigene:5970;urn:miriam:ncbigene:5970;urn:miriam:refseq:NM_021975;urn:miriam:hgnc.symbol:RELA;urn:miriam:hgnc.symbol:RELA;urn:miriam:uniprot:Q04206;urn:miriam:uniprot:Q04206;urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998"
      hgnc "HGNC_SYMBOL:RELA;HGNC_SYMBOL:NFKB1"
      map_id "M16_24"
      name "IkB_underscore_p50_underscore_p65"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa42"
      uniprot "UNIPROT:Q04206;UNIPROT:P19838"
    ]
    graphics [
      x 1253.2618755256487
      y 1282.4963138778812
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_87"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re66"
      uniprot "NA"
    ]
    graphics [
      x 476.89422195334146
      y 1085.259676759933
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ncbiprotein:YP_009725310"
      hgnc "NA"
      map_id "M16_116"
      name "Nsp15"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa129"
      uniprot "NA"
    ]
    graphics [
      x 710.1026160321422
      y 955.1431565110435
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:OAS2;urn:miriam:hgnc.symbol:OAS2;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:P29728;urn:miriam:uniprot:P29728;urn:miriam:ncbigene:4939;urn:miriam:ncbigene:4939;urn:miriam:hgnc:8087;urn:miriam:ensembl:ENSG00000111335;urn:miriam:refseq:NM_001032731;urn:miriam:ec-code:2.7.7.84"
      hgnc "HGNC_SYMBOL:OAS2"
      map_id "M16_235"
      name "OAS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa76"
      uniprot "UNIPROT:P29728"
    ]
    graphics [
      x 592.3749440177983
      y 901.355453447816
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_235"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:ncbiprotein:YP_009725299;urn:miriam:uniprot:Nsp3"
      hgnc "NA"
      map_id "M16_134"
      name "Nsp3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa169"
      uniprot "UNIPROT:Nsp3"
    ]
    graphics [
      x 1340.7037976956608
      y 1442.846390324072
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_91"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re71"
      uniprot "NA"
    ]
    graphics [
      x 1303.5109967863175
      y 1306.1453205393927
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      annotation "PUBMED:32133002"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_35"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re100"
      uniprot "NA"
    ]
    graphics [
      x 1009.8216458326
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:mesh:D007249"
      hgnc "NA"
      map_id "M16_144"
      name "Proinflammatory_space_cytokine_space_expression_underscore_Inflammation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa196"
      uniprot "NA"
    ]
    graphics [
      x 974.4309632442186
      y 182.0354847997744
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_144"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:refseq:NM_007315;urn:miriam:hgnc.symbol:STAT1;urn:miriam:hgnc.symbol:STAT1;urn:miriam:uniprot:P42224;urn:miriam:uniprot:P42224;urn:miriam:ncbigene:6772;urn:miriam:ncbigene:6772;urn:miriam:hgnc:11362;urn:miriam:ensembl:ENSG00000115415"
      hgnc "HGNC_SYMBOL:STAT1"
      map_id "M16_190"
      name "STAT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa305"
      uniprot "UNIPROT:P42224"
    ]
    graphics [
      x 1731.1535261989507
      y 919.3945657066381
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_190"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "PUBMED:32979938"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_63"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re136"
      uniprot "NA"
    ]
    graphics [
      x 1670.5631018850204
      y 846.8708718235541
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ncbiprotein:YP_009725297"
      hgnc "NA"
      map_id "M16_118"
      name "Nsp1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa132"
      uniprot "NA"
    ]
    graphics [
      x 1594.8291451921418
      y 909.9492955899312
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:32979938;urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:YP_009742613.1"
      hgnc "NA"
      map_id "M16_192"
      name "Nsp6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa307"
      uniprot "NA"
    ]
    graphics [
      x 1826.4360211230226
      y 845.6302041080882
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_192"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:wikipathways:WP4868;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbiprotein:YP_009725308;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M16_194"
      name "Nsp13"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa309"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1675.6739341409261
      y 962.9374837968556
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_194"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:ncbiprotein:YP_009725318.1"
      hgnc "NA"
      map_id "M16_193"
      name "Orf7b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa308"
      uniprot "NA"
    ]
    graphics [
      x 1616.2495460363896
      y 978.501600767412
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_193"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:ncbiprotein:YP_009724391.1;urn:miriam:pubmed:32979938"
      hgnc "NA"
      map_id "M16_142"
      name "Orf3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa194"
      uniprot "NA"
    ]
    graphics [
      x 1614.5791927443374
      y 736.2008866727344
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_142"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ncbiprotein:YP_009724393.1;urn:miriam:uniprot:M"
      hgnc "NA"
      map_id "M16_119"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa134"
      uniprot "UNIPROT:M"
    ]
    graphics [
      x 1520.001626938004
      y 837.8930510865996
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:refseq:NM_007315;urn:miriam:hgnc.symbol:STAT1;urn:miriam:hgnc.symbol:STAT1;urn:miriam:uniprot:P42224;urn:miriam:uniprot:P42224;urn:miriam:ncbigene:6772;urn:miriam:ncbigene:6772;urn:miriam:hgnc:11362;urn:miriam:ensembl:ENSG00000115415"
      hgnc "HGNC_SYMBOL:STAT1"
      map_id "M16_229"
      name "STAT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa68"
      uniprot "UNIPROT:P42224"
    ]
    graphics [
      x 1584.094697151886
      y 625.2681914836651
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_229"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_214"
      name "s36"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa46"
      uniprot "NA"
    ]
    graphics [
      x 524.5552686706937
      y 1853.0420301114345
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_214"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_69"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re16"
      uniprot "NA"
    ]
    graphics [
      x 507.0605923050864
      y 1717.830537197211
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:P00973;urn:miriam:ncbigene:4938;urn:miriam:refseq:NM_001032409;urn:miriam:hgnc.symbol:OAS1;urn:miriam:hgnc:8086;urn:miriam:ensembl:ENSG00000089127"
      hgnc "HGNC_SYMBOL:OAS1"
      map_id "M16_212"
      name "OAS1"
      node_subtype "GENE"
      node_type "species"
      org_id "sa44"
      uniprot "UNIPROT:P00973"
    ]
    graphics [
      x 684.4987098463146
      y 1721.0102678704338
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_212"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:P00973;urn:miriam:ncbigene:4938;urn:miriam:refseq:NM_001032409;urn:miriam:hgnc.symbol:OAS1;urn:miriam:hgnc:8086;urn:miriam:ensembl:ENSG00000089127"
      hgnc "HGNC_SYMBOL:OAS1"
      map_id "M16_213"
      name "OAS1"
      node_subtype "RNA"
      node_type "species"
      org_id "sa45"
      uniprot "UNIPROT:P00973"
    ]
    graphics [
      x 737.5997768811454
      y 1830.409818014371
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_213"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_71"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re18"
      uniprot "NA"
    ]
    graphics [
      x 903.0950316357337
      y 1742.9431454956762
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:P00973;urn:miriam:uniprot:P00973;urn:miriam:ncbigene:4938;urn:miriam:ncbigene:4938;urn:miriam:refseq:NM_001032409;urn:miriam:hgnc.symbol:OAS1;urn:miriam:hgnc.symbol:OAS1;urn:miriam:hgnc:8086;urn:miriam:ec-code:2.7.7.84;urn:miriam:ensembl:ENSG00000089127"
      hgnc "HGNC_SYMBOL:OAS1"
      map_id "M16_128"
      name "OAS1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa159"
      uniprot "UNIPROT:P00973"
    ]
    graphics [
      x 921.4774151317274
      y 1503.4521281991413
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:P52630;urn:miriam:uniprot:P52630;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc:11363;urn:miriam:ncbigene:6773;urn:miriam:ncbigene:6773;urn:miriam:refseq:NM_005419;urn:miriam:ensembl:ENSG00000170581"
      hgnc "HGNC_SYMBOL:STAT2"
      map_id "M16_191"
      name "STAT2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa306"
      uniprot "UNIPROT:P52630"
    ]
    graphics [
      x 1749.108970700781
      y 1096.5123954142532
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_191"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      annotation "PUBMED:32979938"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_64"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re137"
      uniprot "NA"
    ]
    graphics [
      x 1798.7429049713014
      y 959.7567857906915
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:ncbiprotein:YP_009724395.1"
      hgnc "NA"
      map_id "M16_143"
      name "Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa195"
      uniprot "NA"
    ]
    graphics [
      x 1899.4369357473938
      y 940.5546084202433
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_143"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:P52630;urn:miriam:uniprot:P52630;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc:11363;urn:miriam:ncbigene:6773;urn:miriam:ncbigene:6773;urn:miriam:refseq:NM_005419;urn:miriam:ensembl:ENSG00000170581"
      hgnc "HGNC_SYMBOL:STAT2"
      map_id "M16_230"
      name "STAT2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa69"
      uniprot "UNIPROT:P52630"
    ]
    graphics [
      x 1643.703574879033
      y 795.0132711971125
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_230"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:OAS3;urn:miriam:hgnc.symbol:OAS3;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q9Y6K5;urn:miriam:uniprot:Q9Y6K5;urn:miriam:ensembl:ENSG00000111331;urn:miriam:refseq:NM_006187;urn:miriam:ec-code:2.7.7.84;urn:miriam:hgnc:8088;urn:miriam:ncbigene:4940;urn:miriam:ncbigene:4940;urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:OAS2;urn:miriam:hgnc.symbol:OAS2;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:P29728;urn:miriam:uniprot:P29728;urn:miriam:ncbigene:4939;urn:miriam:ncbigene:4939;urn:miriam:hgnc:8087;urn:miriam:ensembl:ENSG00000111335;urn:miriam:refseq:NM_001032731;urn:miriam:ec-code:2.7.7.84;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:P00973;urn:miriam:uniprot:P00973;urn:miriam:ncbigene:4938;urn:miriam:ncbigene:4938;urn:miriam:refseq:NM_001032409;urn:miriam:hgnc.symbol:OAS1;urn:miriam:hgnc.symbol:OAS1;urn:miriam:hgnc:8086;urn:miriam:ec-code:2.7.7.84;urn:miriam:ensembl:ENSG00000089127"
      hgnc "HGNC_SYMBOL:OAS3;HGNC_SYMBOL:OAS2;HGNC_SYMBOL:OAS1"
      map_id "M16_15"
      name "OAS1_underscore_EIF2AK"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa32"
      uniprot "UNIPROT:Q9Y6K5;UNIPROT:P29728;UNIPROT:P00973"
    ]
    graphics [
      x 724.2573417802759
      y 594.2589489127713
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_107"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re97"
      uniprot "NA"
    ]
    graphics [
      x 784.2780055249514
      y 486.44301537361764
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:mesh:D007113"
      hgnc "NA"
      map_id "M16_240"
      name "ISG_space_expression_underscore_antiviral_space_response"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa81"
      uniprot "NA"
    ]
    graphics [
      x 934.7947262914693
      y 487.97168050588823
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_240"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_181"
      name "NAP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa296"
      uniprot "NA"
    ]
    graphics [
      x 779.0769733812896
      y 723.6439446249494
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_181"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_59"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re132"
      uniprot "NA"
    ]
    graphics [
      x 818.2685946102152
      y 879.2885506080534
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:refseq:NM_138554;urn:miriam:ncbigene:7099;urn:miriam:ncbigene:7099;urn:miriam:ec-code:3.2.2.6;urn:miriam:hgnc:11850;urn:miriam:uniprot:O00206;urn:miriam:uniprot:O00206;urn:miriam:hgnc.symbol:TLR4;urn:miriam:hgnc.symbol:TLR4;urn:miriam:ensembl:ENSG00000136869"
      hgnc "HGNC_SYMBOL:TLR4"
      map_id "M16_29"
      name "TLR4_underscore_TRIF_underscore_TRAM"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa47"
      uniprot "UNIPROT:O00206"
    ]
    graphics [
      x 571.4913082248844
      y 972.0997283790635
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_182"
      name "NAP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa297"
      uniprot "NA"
    ]
    graphics [
      x 1016.461389250774
      y 1054.5778606753047
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_182"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998;urn:miriam:hgnc:9955;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ncbigene:5970;urn:miriam:ncbigene:5970;urn:miriam:refseq:NM_021975;urn:miriam:hgnc.symbol:RELA;urn:miriam:hgnc.symbol:RELA;urn:miriam:uniprot:Q04206;urn:miriam:uniprot:Q04206"
      hgnc "HGNC_SYMBOL:NFKB1;HGNC_SYMBOL:RELA"
      map_id "M16_6"
      name "p50_underscore_p65"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa21"
      uniprot "UNIPROT:P19838;UNIPROT:Q04206"
    ]
    graphics [
      x 839.2439322597343
      y 1041.4634302646618
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_84"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re62"
      uniprot "NA"
    ]
    graphics [
      x 710.7799741150097
      y 767.0395971147381
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:24622840;urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q9UHD2;urn:miriam:uniprot:Q9UHD2;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:pubmed:31226023;urn:miriam:pubmed:24622840;urn:miriam:ec-code:2.7.11.1;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033;urn:miriam:wikipathways:WP4868;urn:miriam:ncbigene:9641;urn:miriam:ncbigene:9641;urn:miriam:hgnc:14552;urn:miriam:refseq:NM_001193321;urn:miriam:pubmed:31226023;urn:miriam:uniprot:Q14164;urn:miriam:uniprot:Q14164;urn:miriam:pubmed:24622840;urn:miriam:ec-code:2.7.11.10;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:ensembl:ENSG00000263528"
      hgnc "HGNC_SYMBOL:TBK1;HGNC_SYMBOL:TRAF3;HGNC_SYMBOL:IKBKE"
      map_id "M16_8"
      name "TRAF3_underscore_TBK1_underscore_IKBKE"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa25"
      uniprot "UNIPROT:Q9UHD2;UNIPROT:Q13114;UNIPROT:Q14164"
    ]
    graphics [
      x 1570.2436883467294
      y 1612.5365720918173
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      annotation "PUBMED:24622840"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_93"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re73"
      uniprot "NA"
    ]
    graphics [
      x 1432.2402546499081
      y 1519.5954579030658
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:24622840;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033;urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q9UHD2;urn:miriam:uniprot:Q9UHD2;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:pubmed:31226023;urn:miriam:pubmed:24622840;urn:miriam:ec-code:2.7.11.1;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110;urn:miriam:wikipathways:WP4868;urn:miriam:ncbigene:9641;urn:miriam:ncbigene:9641;urn:miriam:hgnc:14552;urn:miriam:refseq:NM_001193321;urn:miriam:pubmed:31226023;urn:miriam:uniprot:Q14164;urn:miriam:uniprot:Q14164;urn:miriam:pubmed:24622840;urn:miriam:ec-code:2.7.11.10;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:ensembl:ENSG00000263528"
      hgnc "HGNC_SYMBOL:TRAF3;HGNC_SYMBOL:TBK1;HGNC_SYMBOL:IKBKE"
      map_id "M16_9"
      name "TRAF3_underscore_TBK1_underscore_IKBKE"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa26"
      uniprot "UNIPROT:Q13114;UNIPROT:Q9UHD2;UNIPROT:Q14164"
    ]
    graphics [
      x 1309.0742691067983
      y 1621.6606874470772
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      annotation "PUBMED:33037393;PUBMED:19380580;PUBMED:18089727"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_98"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re82"
      uniprot "NA"
    ]
    graphics [
      x 1173.652410758848
      y 1550.8770169261002
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:ncbigene:10010;urn:miriam:ncbigene:10010;urn:miriam:ensembl:ENSG00000136560;urn:miriam:refseq:NM_133484;urn:miriam:uniprot:Q92844;urn:miriam:uniprot:Q92844;urn:miriam:hgnc:11562;urn:miriam:hgnc.symbol:TANK;urn:miriam:hgnc.symbol:TANK"
      hgnc "HGNC_SYMBOL:TANK"
      map_id "M16_151"
      name "TANK"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa209"
      uniprot "UNIPROT:Q92844"
    ]
    graphics [
      x 1092.70777309519
      y 1640.3907356400623
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_151"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:uniprot:O15455;urn:miriam:uniprot:O15455;urn:miriam:ensembl:ENSG00000164342;urn:miriam:refseq:NM_003265;urn:miriam:ncbigene:7098;urn:miriam:ncbigene:7098;urn:miriam:hgnc.symbol:TLR3;urn:miriam:hgnc.symbol:TLR3;urn:miriam:hgnc:11849"
      hgnc "HGNC_SYMBOL:TLR3"
      map_id "M16_18"
      name "TLR3_underscore_TRIF"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa37"
      uniprot "UNIPROT:O15455"
    ]
    graphics [
      x 1009.1422023523911
      y 1664.6768114776853
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ncbiprotein:YP_009724393.1;urn:miriam:uniprot:M"
      hgnc "NA"
      map_id "M16_154"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa226"
      uniprot "UNIPROT:M"
    ]
    graphics [
      x 1195.373311800306
      y 1686.8889815744074
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_154"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:wikipathways:WP4868;urn:miriam:ncbigene:9641;urn:miriam:ncbigene:9641;urn:miriam:hgnc:14552;urn:miriam:refseq:NM_001193321;urn:miriam:pubmed:31226023;urn:miriam:uniprot:Q14164;urn:miriam:uniprot:Q14164;urn:miriam:pubmed:24622840;urn:miriam:ec-code:2.7.11.10;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:ensembl:ENSG00000263528;urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q9UHD2;urn:miriam:uniprot:Q9UHD2;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:pubmed:31226023;urn:miriam:pubmed:24622840;urn:miriam:ec-code:2.7.11.1;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033;urn:miriam:ncbigene:10010;urn:miriam:ncbigene:10010;urn:miriam:ensembl:ENSG00000136560;urn:miriam:refseq:NM_133484;urn:miriam:uniprot:Q92844;urn:miriam:uniprot:Q92844;urn:miriam:hgnc:11562;urn:miriam:hgnc.symbol:TANK;urn:miriam:hgnc.symbol:TANK"
      hgnc "HGNC_SYMBOL:IKBKE;HGNC_SYMBOL:TBK1;HGNC_SYMBOL:TRAF3;HGNC_SYMBOL:TANK"
      map_id "M16_11"
      name "TRAF3_underscore_TANK_underscore_TBK1_underscore_IKKepsilon"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa29"
      uniprot "UNIPROT:Q14164;UNIPROT:Q9UHD2;UNIPROT:Q13114;UNIPROT:Q92844"
    ]
    graphics [
      x 1274.0148966514048
      y 1374.1122260944685
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000187608;urn:miriam:refseq:NM_005101;urn:miriam:ncbigene:9636;urn:miriam:hgnc.symbol:ISG15;urn:miriam:uniprot:P05161;urn:miriam:hgnc:4053"
      hgnc "HGNC_SYMBOL:ISG15"
      map_id "M16_184"
      name "ISG15"
      node_subtype "RNA"
      node_type "species"
      org_id "sa299"
      uniprot "UNIPROT:P05161"
    ]
    graphics [
      x 501.16069137768136
      y 1414.5035720778124
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_184"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      annotation "PUBMED:29769653"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_61"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re134"
      uniprot "NA"
    ]
    graphics [
      x 753.092888394316
      y 1321.9742206298629
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000187608;urn:miriam:refseq:NM_005101;urn:miriam:ncbigene:9636;urn:miriam:ncbigene:9636;urn:miriam:hgnc.symbol:ISG15;urn:miriam:hgnc.symbol:ISG15;urn:miriam:uniprot:P05161;urn:miriam:uniprot:P05161;urn:miriam:hgnc:4053"
      hgnc "HGNC_SYMBOL:ISG15"
      map_id "M16_188"
      name "ISG15"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa301"
      uniprot "UNIPROT:P05161"
    ]
    graphics [
      x 960.0039902315681
      y 1124.333479792674
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_188"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:OAS3;urn:miriam:hgnc.symbol:OAS3;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q9Y6K5;urn:miriam:uniprot:Q9Y6K5;urn:miriam:ensembl:ENSG00000111331;urn:miriam:refseq:NM_006187;urn:miriam:ec-code:2.7.7.84;urn:miriam:hgnc:8088;urn:miriam:ncbigene:4940;urn:miriam:ncbigene:4940"
      hgnc "HGNC_SYMBOL:OAS3"
      map_id "M16_126"
      name "OAS3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa157"
      uniprot "UNIPROT:Q9Y6K5"
    ]
    graphics [
      x 489.29812103723714
      y 773.4230169691618
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_86"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re65"
      uniprot "NA"
    ]
    graphics [
      x 641.9673759302185
      y 723.2641781584753
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:OAS3;urn:miriam:hgnc.symbol:OAS3;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q9Y6K5;urn:miriam:uniprot:Q9Y6K5;urn:miriam:ensembl:ENSG00000111331;urn:miriam:refseq:NM_006187;urn:miriam:ec-code:2.7.7.84;urn:miriam:hgnc:8088;urn:miriam:ncbigene:4940;urn:miriam:ncbigene:4940"
      hgnc "HGNC_SYMBOL:OAS3"
      map_id "M16_239"
      name "OAS3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa80"
      uniprot "UNIPROT:Q9Y6K5"
    ]
    graphics [
      x 647.8973423212699
      y 623.456039593028
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_239"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_47"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re117"
      uniprot "NA"
    ]
    graphics [
      x 350.08179675011434
      y 1412.2571685526987
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_168"
      name "type_space_I_space_IFN_space_response"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa260"
      uniprot "NA"
    ]
    graphics [
      x 437.0651202947315
      y 1337.7813183946334
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_168"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033"
      hgnc "HGNC_SYMBOL:TRAF3"
      map_id "M16_241"
      name "TRAF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa82"
      uniprot "UNIPROT:Q13114"
    ]
    graphics [
      x 1567.4452241919723
      y 1159.1945061692873
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_241"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:18089727"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_79"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re39"
      uniprot "NA"
    ]
    graphics [
      x 1436.055577493279
      y 1167.57826494976
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:pubmed:31226023;urn:miriam:ncbigene:4615;urn:miriam:ensembl:ENSG00000172936;urn:miriam:ncbigene:4615;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q99836;urn:miriam:uniprot:Q99836;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc:7562;urn:miriam:refseq:NM_002468"
      hgnc "HGNC_SYMBOL:MYD88"
      map_id "M16_5"
      name "MYD88_underscore_TRAM"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa20"
      uniprot "UNIPROT:Q99836"
    ]
    graphics [
      x 1311.424471681899
      y 1037.874830702164
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033"
      hgnc "HGNC_SYMBOL:TRAF3"
      map_id "M16_242"
      name "TRAF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa83"
      uniprot "UNIPROT:Q13114"
    ]
    graphics [
      x 1464.9742485841732
      y 1313.7926928804195
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_242"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:IFNA1;urn:miriam:hgnc.symbol:IFNA1;urn:miriam:ncbigene:3439;urn:miriam:ncbigene:3439;urn:miriam:refseq:NM_024013;urn:miriam:uniprot:P01562;urn:miriam:uniprot:P01562;urn:miriam:hgnc:5417;urn:miriam:ensembl:ENSG00000197919"
      hgnc "HGNC_SYMBOL:IFNA1"
      map_id "M16_186"
      name "IFNA1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa30"
      uniprot "UNIPROT:P01562"
    ]
    graphics [
      x 944.918728131465
      y 604.6816962507412
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_186"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_95"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re8"
      uniprot "NA"
    ]
    graphics [
      x 935.7540631035189
      y 947.6628211417969
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:IFNA1;urn:miriam:hgnc.symbol:IFNA1;urn:miriam:ncbigene:3439;urn:miriam:ncbigene:3439;urn:miriam:refseq:NM_024013;urn:miriam:uniprot:P01562;urn:miriam:uniprot:P01562;urn:miriam:hgnc:5417;urn:miriam:ensembl:ENSG00000197919"
      hgnc "HGNC_SYMBOL:IFNA1"
      map_id "M16_195"
      name "IFNA1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa31"
      uniprot "UNIPROT:P01562"
    ]
    graphics [
      x 756.5584228917662
      y 1177.9737092440066
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_195"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:hgnc:18873;urn:miriam:ncbigene:64135;urn:miriam:ncbigene:64135;urn:miriam:refseq:NM_022168;urn:miriam:ensembl:ENSG00000115267;urn:miriam:pubmed:19052324;urn:miriam:uniprot:Q9BYX4;urn:miriam:uniprot:Q9BYX4;urn:miriam:ec-code:3.6.4.13;urn:miriam:hgnc.symbol:IFIH1;urn:miriam:hgnc.symbol:IFIH1"
      hgnc "HGNC_SYMBOL:IFIH1"
      map_id "M16_145"
      name "IFIH1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa198"
      uniprot "UNIPROT:Q9BYX4"
    ]
    graphics [
      x 1137.103149808544
      y 1076.7194044364694
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_145"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      annotation "PUBMED:33348292;PUBMED:28158275"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_94"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re76"
      uniprot "NA"
    ]
    graphics [
      x 1124.1664667623324
      y 921.6943654888842
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ncbiprotein:YP_009725310"
      hgnc "NA"
      map_id "M16_155"
      name "Nsp15"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa227"
      uniprot "NA"
    ]
    graphics [
      x 988.5699942126306
      y 828.4321903829086
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_155"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A2955"
      hgnc "NA"
      map_id "M16_203"
      name "Azithromycin"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa320"
      uniprot "NA"
    ]
    graphics [
      x 1245.618319157239
      y 673.9045137765636
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_203"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:hgnc:18873;urn:miriam:ncbigene:64135;urn:miriam:ncbigene:64135;urn:miriam:refseq:NM_022168;urn:miriam:ensembl:ENSG00000115267;urn:miriam:pubmed:19052324;urn:miriam:uniprot:Q9BYX4;urn:miriam:uniprot:Q9BYX4;urn:miriam:ec-code:3.6.4.13;urn:miriam:hgnc.symbol:IFIH1;urn:miriam:hgnc.symbol:IFIH1"
      hgnc "HGNC_SYMBOL:IFIH1"
      map_id "M16_123"
      name "IFIH1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa154"
      uniprot "UNIPROT:Q9BYX4"
    ]
    graphics [
      x 961.9537816542446
      y 886.44550683541
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      annotation "PUBMED:12692549"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_103"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re92"
      uniprot "NA"
    ]
    graphics [
      x 829.9917289475713
      y 450.3057764478324
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:uniprot:O15455;urn:miriam:uniprot:O15455;urn:miriam:ensembl:ENSG00000164342;urn:miriam:refseq:NM_003265;urn:miriam:ncbigene:7098;urn:miriam:ncbigene:7098;urn:miriam:hgnc.symbol:TLR3;urn:miriam:hgnc.symbol:TLR3;urn:miriam:hgnc:11849"
      hgnc "HGNC_SYMBOL:TLR3"
      map_id "M16_159"
      name "TLR3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa239"
      uniprot "UNIPROT:O15455"
    ]
    graphics [
      x 1033.3427897921133
      y 1501.3512793122532
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_159"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      annotation "PUBMED:33348292"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_43"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re113"
      uniprot "NA"
    ]
    graphics [
      x 1024.5996468593667
      y 1386.8211050377124
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_160"
      name "TRIF"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa240"
      uniprot "NA"
    ]
    graphics [
      x 715.3403285925239
      y 1289.7408236076735
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_160"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:pubmed:19052324;urn:miriam:taxonomy:694009"
      hgnc "NA"
      map_id "M16_166"
      name "Viral_space_dsRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa259"
      uniprot "NA"
    ]
    graphics [
      x 1334.813809675415
      y 1124.893646605192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_166"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A2955"
      hgnc "NA"
      map_id "M16_135"
      name "Azithromycin"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa170"
      uniprot "NA"
    ]
    graphics [
      x 1165.0056512835545
      y 1342.0409719201389
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ncbigene:23586;urn:miriam:ncbigene:23586;urn:miriam:refseq:NM_014314;urn:miriam:ensembl:ENSG00000107201;urn:miriam:pubmed:19052324;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:uniprot:O95786;urn:miriam:hgnc:19102;urn:miriam:hgnc.symbol:DDX58;urn:miriam:hgnc.symbol:DDX58"
      hgnc "HGNC_SYMBOL:DDX58"
      map_id "M16_146"
      name "DDX58"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa200"
      uniprot "UNIPROT:O95786"
    ]
    graphics [
      x 1271.9213323216957
      y 323.17327489034994
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_146"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      annotation "PUBMED:25581309;PUBMED:33024073;PUBMED:33348292;PUBMED:28148787"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_96"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re80"
      uniprot "NA"
    ]
    graphics [
      x 1289.6530318322857
      y 457.5673019311155
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ncbiprotein:BCD58761;urn:miriam:ncbiprotein:YP_009724397.2"
      hgnc "NA"
      map_id "M16_121"
      name "N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa140"
      uniprot "NA"
    ]
    graphics [
      x 1167.5315894225412
      y 679.8237733240733
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:ncbigene:7706;urn:miriam:ensembl:ENSG00000121060;urn:miriam:ncbigene:7706;urn:miriam:hgnc.symbol:TRIM25;urn:miriam:hgnc.symbol:TRIM25;urn:miriam:ec-code:2.3.2.27;urn:miriam:hgnc:12932;urn:miriam:uniprot:Q14258;urn:miriam:uniprot:Q14258;urn:miriam:refseq:NM_005082"
      hgnc "HGNC_SYMBOL:TRIM25"
      map_id "M16_147"
      name "TRIM25"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa201"
      uniprot "UNIPROT:Q14258"
    ]
    graphics [
      x 1350.0982167212696
      y 324.96592972030953
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_147"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:uniprot:Q8IUD6;urn:miriam:uniprot:Q8IUD6;urn:miriam:hgnc:21158;urn:miriam:refseq:NM_032322;urn:miriam:hgnc.symbol:RNF135;urn:miriam:hgnc.symbol:RNF135;urn:miriam:ncbigene:84282;urn:miriam:ncbigene:84282;urn:miriam:ensembl:ENSG00000181481"
      hgnc "HGNC_SYMBOL:RNF135"
      map_id "M16_148"
      name "RNF135"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa202"
      uniprot "UNIPROT:Q8IUD6"
    ]
    graphics [
      x 1404.4160383437136
      y 379.45666824149816
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_148"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:ncbiprotein:YP_009725301"
      hgnc "NA"
      map_id "M16_162"
      name "Nsp5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa251"
      uniprot "NA"
    ]
    graphics [
      x 1419.983695433596
      y 522.1113460185722
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_162"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ncbigene:23586;urn:miriam:ncbigene:23586;urn:miriam:refseq:NM_014314;urn:miriam:ensembl:ENSG00000107201;urn:miriam:pubmed:19052324;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:uniprot:O95786;urn:miriam:hgnc:19102;urn:miriam:hgnc.symbol:DDX58;urn:miriam:hgnc.symbol:DDX58"
      hgnc "HGNC_SYMBOL:DDX58"
      map_id "M16_124"
      name "DDX58"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa155"
      uniprot "UNIPROT:O95786"
    ]
    graphics [
      x 1125.6525964439484
      y 493.2689644382923
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_124"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ec-code:2.7.11.24;urn:miriam:wikipathways:WP4868;urn:miriam:ensembl:ENSG00000112062;urn:miriam:hgnc:6876;urn:miriam:uniprot:Q16539;urn:miriam:uniprot:Q16539;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:refseq:NM_001315"
      hgnc "HGNC_SYMBOL:MAPK14"
      map_id "M16_228"
      name "MAPK14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa64"
      uniprot "UNIPROT:Q16539"
    ]
    graphics [
      x 576.4839700654193
      y 700.300602836294
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_228"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_38"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re104"
      uniprot "NA"
    ]
    graphics [
      x 627.2990918165756
      y 836.4238307715332
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ec-code:2.7.11.24;urn:miriam:refseq:NM_001278547;urn:miriam:wikipathways:WP4868;urn:miriam:ensembl:ENSG00000107643;urn:miriam:hgnc.symbol:MAPK8;urn:miriam:uniprot:P45983;urn:miriam:uniprot:P45983;urn:miriam:hgnc.symbol:MAPK8;urn:miriam:hgnc:6881;urn:miriam:ncbigene:5599;urn:miriam:ncbigene:5599"
      hgnc "HGNC_SYMBOL:MAPK8"
      map_id "M16_227"
      name "MAPK8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa63"
      uniprot "UNIPROT:P45983"
    ]
    graphics [
      x 518.8470914711313
      y 713.9910770020288
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_227"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:uniprot:O43318;urn:miriam:uniprot:O43318;urn:miriam:wikipathways:WP4868;urn:miriam:ensembl:ENSG00000135341;urn:miriam:refseq:NM_145331;urn:miriam:hgnc:6859;urn:miriam:ncbigene:6885;urn:miriam:ncbigene:6885;urn:miriam:hgnc.symbol:MAP3K7;urn:miriam:hgnc.symbol:MAP3K7;urn:miriam:ec-code:2.7.11.25"
      hgnc "HGNC_SYMBOL:MAP3K7"
      map_id "M16_244"
      name "MAP3K7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa86"
      uniprot "UNIPROT:O43318"
    ]
    graphics [
      x 615.965042320317
      y 1020.3693408128955
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_244"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:pubmed:31226023;urn:miriam:ec-code:2.7.11.24;urn:miriam:refseq:NM_001278547;urn:miriam:wikipathways:WP4868;urn:miriam:ensembl:ENSG00000107643;urn:miriam:hgnc.symbol:MAPK8;urn:miriam:uniprot:P45983;urn:miriam:uniprot:P45983;urn:miriam:hgnc.symbol:MAPK8;urn:miriam:hgnc:6881;urn:miriam:ncbigene:5599;urn:miriam:ncbigene:5599;urn:miriam:pubmed:31226023;urn:miriam:ec-code:2.7.11.24;urn:miriam:wikipathways:WP4868;urn:miriam:ensembl:ENSG00000112062;urn:miriam:hgnc:6876;urn:miriam:uniprot:Q16539;urn:miriam:uniprot:Q16539;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:hgnc.symbol:MAPK14;urn:miriam:ncbigene:1432;urn:miriam:refseq:NM_001315"
      hgnc "HGNC_SYMBOL:MAPK8;HGNC_SYMBOL:MAPK14"
      map_id "M16_32"
      name "MAPK8_slash_14"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa8"
      uniprot "UNIPROT:P45983;UNIPROT:Q16539"
    ]
    graphics [
      x 419.10821785821173
      y 746.5676726893291
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:uniprot:Q92985;urn:miriam:uniprot:Q92985;urn:miriam:ensembl:ENSG00000185507;urn:miriam:refseq:NM_001572;urn:miriam:hgnc.symbol:IRF7;urn:miriam:hgnc.symbol:IRF7;urn:miriam:hgnc:6122;urn:miriam:ncbigene:3665;urn:miriam:ncbigene:3665"
      hgnc "HGNC_SYMBOL:IRF7"
      map_id "M16_25"
      name "IRF7_underscore_homodimer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa43"
      uniprot "UNIPROT:Q92985"
    ]
    graphics [
      x 1359.3328002847172
      y 753.1597005458993
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_54"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re128"
      uniprot "NA"
    ]
    graphics [
      x 1064.152281508475
      y 554.0342671329697
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:uniprot:Q92985;urn:miriam:uniprot:Q92985;urn:miriam:ensembl:ENSG00000185507;urn:miriam:refseq:NM_001572;urn:miriam:hgnc.symbol:IRF7;urn:miriam:hgnc.symbol:IRF7;urn:miriam:hgnc:6122;urn:miriam:ncbigene:3665;urn:miriam:ncbigene:3665"
      hgnc "HGNC_SYMBOL:IRF7"
      map_id "M16_26"
      name "IRF7_underscore_homodimer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa44"
      uniprot "UNIPROT:Q92985"
    ]
    graphics [
      x 704.7622907598991
      y 451.5338435897479
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:refseq:NM_007315;urn:miriam:hgnc.symbol:STAT1;urn:miriam:hgnc.symbol:STAT1;urn:miriam:uniprot:P42224;urn:miriam:uniprot:P42224;urn:miriam:ncbigene:6772;urn:miriam:ncbigene:6772;urn:miriam:hgnc:11362;urn:miriam:ensembl:ENSG00000115415;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:P52630;urn:miriam:uniprot:P52630;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc:11363;urn:miriam:ncbigene:6773;urn:miriam:ncbigene:6773;urn:miriam:refseq:NM_005419;urn:miriam:ensembl:ENSG00000170581;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q00978;urn:miriam:uniprot:Q00978;urn:miriam:refseq:NM_001385400;urn:miriam:ensembl:ENSG00000213928;urn:miriam:hgnc.symbol:IRF9;urn:miriam:ncbigene:10379;urn:miriam:hgnc.symbol:IRF9;urn:miriam:ncbigene:10379;urn:miriam:hgnc:6131"
      hgnc "HGNC_SYMBOL:STAT1;HGNC_SYMBOL:STAT2;HGNC_SYMBOL:IRF9"
      map_id "M16_21"
      name "STAT1_slash_2_underscore_IRF9"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa4"
      uniprot "UNIPROT:P42224;UNIPROT:P52630;UNIPROT:Q00978"
    ]
    graphics [
      x 1079.8961316746663
      y 1070.1519529162197
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      annotation "PUBMED:32979938"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_65"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re138"
      uniprot "NA"
    ]
    graphics [
      x 738.5852931626187
      y 1412.6669163121583
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ncbiprotein:YP_009724394.1"
      hgnc "NA"
      map_id "M16_196"
      name "Orf6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa311"
      uniprot "NA"
    ]
    graphics [
      x 682.926685246591
      y 1545.2501707374522
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_196"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ncbigene:4615;urn:miriam:ensembl:ENSG00000172936;urn:miriam:ncbigene:4615;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q99836;urn:miriam:uniprot:Q99836;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc:7562;urn:miriam:refseq:NM_002468"
      hgnc "HGNC_SYMBOL:MYD88"
      map_id "M16_222"
      name "MYD88"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa57"
      uniprot "UNIPROT:Q99836"
    ]
    graphics [
      x 1682.2935133275555
      y 707.2115077985536
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_222"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:18089727"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_106"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re96"
      uniprot "NA"
    ]
    graphics [
      x 1570.734831015478
      y 831.097776270713
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_198"
      name "IRAK1_slash_4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa314"
      uniprot "NA"
    ]
    graphics [
      x 1731.8044085196193
      y 812.2875039590444
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_198"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000188056;urn:miriam:wikipathways:WP4868;urn:miriam:ncbigene:285852;urn:miriam:ncbigene:285852;urn:miriam:refseq:NM_198153;urn:miriam:hgnc.symbol:TREML4;urn:miriam:hgnc.symbol:TREML4;urn:miriam:uniprot:Q6UXN2;urn:miriam:uniprot:Q6UXN2;urn:miriam:hgnc:30807"
      hgnc "HGNC_SYMBOL:TREML4"
      map_id "M16_131"
      name "TREML4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa166"
      uniprot "UNIPROT:Q6UXN2"
    ]
    graphics [
      x 1559.4701560345356
      y 684.2947689557227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_131"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:uniprot:Q9NYK1;urn:miriam:uniprot:Q9NYK1;urn:miriam:hgnc:15631;urn:miriam:refseq:NM_016562;urn:miriam:hgnc.symbol:TLR7;urn:miriam:hgnc.symbol:TLR7;urn:miriam:ensembl:ENSG00000196664;urn:miriam:ncbigene:51284;urn:miriam:ncbigene:51284"
      hgnc "HGNC_SYMBOL:TLR7"
      map_id "M16_221"
      name "TLR7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa55"
      uniprot "UNIPROT:Q9NYK1"
    ]
    graphics [
      x 1645.6322747514566
      y 652.1715918171161
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_221"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:uniprot:Q9NR96;urn:miriam:uniprot:Q9NR96;urn:miriam:ncbigene:54106;urn:miriam:ncbigene:54106;urn:miriam:ensembl:ENSG00000239732;urn:miriam:hgnc.symbol:TLR9;urn:miriam:hgnc.symbol:TLR9;urn:miriam:refseq:NM_017442;urn:miriam:hgnc:15633"
      hgnc "HGNC_SYMBOL:TLR9"
      map_id "M16_158"
      name "TLR9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa238"
      uniprot "UNIPROT:Q9NR96"
    ]
    graphics [
      x 1728.3791568299844
      y 748.3286447027118
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_158"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:hgnc:18157;urn:miriam:refseq:NM_153497;urn:miriam:uniprot:Q15750;urn:miriam:uniprot:Q15750;urn:miriam:ensembl:ENSG00000100324;urn:miriam:hgnc.symbol:TAB1;urn:miriam:hgnc.symbol:TAB1;urn:miriam:ncbigene:10454;urn:miriam:ncbigene:10454"
      hgnc "HGNC_SYMBOL:TAB1"
      map_id "M16_163"
      name "TAB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa254"
      uniprot "UNIPROT:Q15750"
    ]
    graphics [
      x 804.8406538674652
      y 1653.0895989380065
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_163"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_45"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re115"
      uniprot "NA"
    ]
    graphics [
      x 947.6034737790982
      y 1612.6380464089148
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:hgnc.symbol:TAB2;urn:miriam:ensembl:ENSG00000055208;urn:miriam:hgnc.symbol:TAB2;urn:miriam:uniprot:Q9NYJ8;urn:miriam:uniprot:Q9NYJ8;urn:miriam:hgnc:17075;urn:miriam:ncbigene:23118;urn:miriam:refseq:NM_001292034;urn:miriam:ncbigene:23118"
      hgnc "HGNC_SYMBOL:TAB2"
      map_id "M16_164"
      name "TAB2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa255"
      uniprot "UNIPROT:Q9NYJ8"
    ]
    graphics [
      x 814.9231761166687
      y 1524.6979657026752
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_164"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7189;urn:miriam:ncbigene:7189;urn:miriam:ensembl:ENSG00000175104;urn:miriam:uniprot:Q9Y4K3;urn:miriam:uniprot:Q9Y4K3;urn:miriam:hgnc:12036;urn:miriam:refseq:NM_145803;urn:miriam:hgnc.symbol:TRAF6;urn:miriam:hgnc.symbol:TRAF6"
      hgnc "HGNC_SYMBOL:TRAF6"
      map_id "M16_224"
      name "TRAF6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa59"
      uniprot "UNIPROT:Q9Y4K3"
    ]
    graphics [
      x 826.6356483661806
      y 1363.2689479141516
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_224"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_165"
      name "TAK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa257"
      uniprot "NA"
    ]
    graphics [
      x 824.1615244624193
      y 1606.204889277145
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_165"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:uniprot:O15455;urn:miriam:uniprot:O15455;urn:miriam:ensembl:ENSG00000164342;urn:miriam:refseq:NM_003265;urn:miriam:ncbigene:7098;urn:miriam:ncbigene:7098;urn:miriam:hgnc.symbol:TLR3;urn:miriam:hgnc.symbol:TLR3;urn:miriam:hgnc:11849"
      hgnc "HGNC_SYMBOL:TLR3"
      map_id "M16_19"
      name "TLR3_underscore_TRIF_underscore_RIPK1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa38"
      uniprot "UNIPROT:O15455"
    ]
    graphics [
      x 810.1957684695631
      y 1778.6821427231646
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:hgnc.symbol:TAB2;urn:miriam:ensembl:ENSG00000055208;urn:miriam:hgnc.symbol:TAB2;urn:miriam:uniprot:Q9NYJ8;urn:miriam:uniprot:Q9NYJ8;urn:miriam:hgnc:17075;urn:miriam:ncbigene:23118;urn:miriam:refseq:NM_001292034;urn:miriam:ncbigene:23118;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7189;urn:miriam:ncbigene:7189;urn:miriam:ensembl:ENSG00000175104;urn:miriam:uniprot:Q9Y4K3;urn:miriam:uniprot:Q9Y4K3;urn:miriam:hgnc:12036;urn:miriam:refseq:NM_145803;urn:miriam:hgnc.symbol:TRAF6;urn:miriam:hgnc.symbol:TRAF6;urn:miriam:hgnc:18157;urn:miriam:refseq:NM_153497;urn:miriam:uniprot:Q15750;urn:miriam:uniprot:Q15750;urn:miriam:ensembl:ENSG00000100324;urn:miriam:hgnc.symbol:TAB1;urn:miriam:hgnc.symbol:TAB1;urn:miriam:ncbigene:10454;urn:miriam:ncbigene:10454"
      hgnc "HGNC_SYMBOL:TAB2;HGNC_SYMBOL:TRAF6;HGNC_SYMBOL:TAB1"
      map_id "M16_20"
      name "TAB1_slash_2_underscore_TRAF6_underscore_TAK1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa39"
      uniprot "UNIPROT:Q9NYJ8;UNIPROT:Q9Y4K3;UNIPROT:Q15750"
    ]
    graphics [
      x 1281.4134083378349
      y 1519.1544135155718
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7189;urn:miriam:ncbigene:7189;urn:miriam:ensembl:ENSG00000175104;urn:miriam:uniprot:Q9Y4K3;urn:miriam:uniprot:Q9Y4K3;urn:miriam:hgnc:12036;urn:miriam:refseq:NM_145803;urn:miriam:hgnc.symbol:TRAF6;urn:miriam:hgnc.symbol:TRAF6"
      hgnc "HGNC_SYMBOL:TRAF6"
      map_id "M16_223"
      name "TRAF6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa58"
      uniprot "UNIPROT:Q9Y4K3"
    ]
    graphics [
      x 702.2908712949179
      y 1089.5585232890198
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_223"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:25135833;PUBMED:18089727"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_73"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re23"
      uniprot "NA"
    ]
    graphics [
      x 891.5549756405836
      y 1119.323265227736
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:uniprot:Q96J02;urn:miriam:uniprot:Q96J02;urn:miriam:ec-code:2.3.2.26;urn:miriam:hgnc.symbol:ITCH;urn:miriam:hgnc.symbol:ITCH;urn:miriam:ensembl:ENSG00000078747;urn:miriam:refseq:NM_001257137;urn:miriam:ncbigene:83737;urn:miriam:ncbigene:83737;urn:miriam:hgnc:13890"
      hgnc "HGNC_SYMBOL:ITCH"
      map_id "M16_150"
      name "ITCH"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa204"
      uniprot "UNIPROT:Q96J02"
    ]
    graphics [
      x 437.66196083258325
      y 979.1970176248685
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_150"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:ncbigene:3725;urn:miriam:ncbigene:3725;urn:miriam:hgnc:6204;urn:miriam:uniprot:P05412;urn:miriam:uniprot:P05412;urn:miriam:hgnc.symbol:JUN;urn:miriam:hgnc.symbol:JUN;urn:miriam:ensembl:ENSG00000177606;urn:miriam:refseq:NM_002228"
      hgnc "HGNC_SYMBOL:JUN"
      map_id "M16_208"
      name "JUN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa36"
      uniprot "UNIPROT:P05412"
    ]
    graphics [
      x 432.57862478965217
      y 810.7799435869133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_208"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_39"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re105"
      uniprot "NA"
    ]
    graphics [
      x 371.92557610087897
      y 667.4222815590057
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:hgnc:3796;urn:miriam:ensembl:ENSG00000170345;urn:miriam:refseq:NM_005252;urn:miriam:uniprot:P01100;urn:miriam:uniprot:P01100;urn:miriam:ncbigene:2353;urn:miriam:ncbigene:2353;urn:miriam:hgnc.symbol:FOS;urn:miriam:hgnc.symbol:FOS"
      hgnc "HGNC_SYMBOL:FOS"
      map_id "M16_245"
      name "FOS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa87"
      uniprot "UNIPROT:P01100"
    ]
    graphics [
      x 451.25658653279135
      y 874.9131289813242
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_245"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:ncbigene:3725;urn:miriam:ncbigene:3725;urn:miriam:hgnc:6204;urn:miriam:uniprot:P05412;urn:miriam:uniprot:P05412;urn:miriam:hgnc.symbol:JUN;urn:miriam:hgnc.symbol:JUN;urn:miriam:ensembl:ENSG00000177606;urn:miriam:refseq:NM_002228;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:hgnc:3796;urn:miriam:ensembl:ENSG00000170345;urn:miriam:refseq:NM_005252;urn:miriam:uniprot:P01100;urn:miriam:uniprot:P01100;urn:miriam:ncbigene:2353;urn:miriam:ncbigene:2353;urn:miriam:hgnc.symbol:FOS;urn:miriam:hgnc.symbol:FOS"
      hgnc "HGNC_SYMBOL:JUN;HGNC_SYMBOL:FOS"
      map_id "M16_2"
      name "AP_minus_1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa10"
      uniprot "UNIPROT:P05412;UNIPROT:P01100"
    ]
    graphics [
      x 231.33404346674342
      y 510.1305028328012
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_187"
      name "s2661"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa300"
      uniprot "NA"
    ]
    graphics [
      x 340.58921651889113
      y 1278.7432326237308
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_187"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      annotation "PUBMED:29769653;PUBMED:32553163"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_60"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re133"
      uniprot "NA"
    ]
    graphics [
      x 286.9773902997699
      y 1455.7641548036424
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000187608;urn:miriam:refseq:NM_005101;urn:miriam:ncbigene:9636;urn:miriam:hgnc.symbol:ISG15;urn:miriam:uniprot:P05161;urn:miriam:hgnc:4053"
      hgnc "HGNC_SYMBOL:ISG15"
      map_id "M16_183"
      name "ISG15"
      node_subtype "GENE"
      node_type "species"
      org_id "sa298"
      uniprot "UNIPROT:P05161"
    ]
    graphics [
      x 262.02271207019487
      y 1288.33794567819
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_183"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      annotation "PUBMED:20457564"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_109"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re99"
      uniprot "NA"
    ]
    graphics [
      x 848.811054930796
      y 326.13257612752875
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_211"
      name "s27"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa40"
      uniprot "NA"
    ]
    graphics [
      x 471.81381706184567
      y 160.1074353824822
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_211"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_56"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re13"
      uniprot "NA"
    ]
    graphics [
      x 561.576713804845
      y 263.41298551116574
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:hgnc:3796;urn:miriam:ensembl:ENSG00000170345;urn:miriam:refseq:NM_005252;urn:miriam:uniprot:P01100;urn:miriam:uniprot:P01100;urn:miriam:ncbigene:2353;urn:miriam:ncbigene:2353;urn:miriam:hgnc.symbol:FOS;urn:miriam:hgnc.symbol:FOS;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:ncbigene:3725;urn:miriam:ncbigene:3725;urn:miriam:hgnc:6204;urn:miriam:uniprot:P05412;urn:miriam:uniprot:P05412;urn:miriam:hgnc.symbol:JUN;urn:miriam:hgnc.symbol:JUN;urn:miriam:ensembl:ENSG00000177606;urn:miriam:refseq:NM_002228"
      hgnc "HGNC_SYMBOL:FOS;HGNC_SYMBOL:JUN"
      map_id "M16_17"
      name "AP_minus_1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa35"
      uniprot "UNIPROT:P01100;UNIPROT:P05412"
    ]
    graphics [
      x 391.76087298213577
      y 368.13652080086695
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:IFNA1;urn:miriam:ncbigene:3439;urn:miriam:refseq:NM_024013;urn:miriam:uniprot:P01562;urn:miriam:hgnc:5417;urn:miriam:ensembl:ENSG00000197919"
      hgnc "HGNC_SYMBOL:IFNA1"
      map_id "M16_209"
      name "IFNA1"
      node_subtype "GENE"
      node_type "species"
      org_id "sa38"
      uniprot "UNIPROT:P01562"
    ]
    graphics [
      x 563.6808093737334
      y 126.98323704402321
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_209"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:IFNA1;urn:miriam:ncbigene:3439;urn:miriam:refseq:NM_024013;urn:miriam:uniprot:P01562;urn:miriam:hgnc:5417;urn:miriam:ensembl:ENSG00000197919"
      hgnc "HGNC_SYMBOL:IFNA1"
      map_id "M16_210"
      name "IFNA1"
      node_subtype "RNA"
      node_type "species"
      org_id "sa39"
      uniprot "UNIPROT:P01562"
    ]
    graphics [
      x 734.7623918185631
      y 200.44825599882597
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_210"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_66"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re139"
      uniprot "NA"
    ]
    graphics [
      x 974.1714971985607
      y 778.6388557188353
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:JAK1;urn:miriam:hgnc.symbol:JAK1;urn:miriam:wikipathways:WP4868;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc:6190;urn:miriam:ncbigene:3716;urn:miriam:ncbigene:3716;urn:miriam:ensembl:ENSG00000162434;urn:miriam:uniprot:P23458;urn:miriam:uniprot:P23458;urn:miriam:refseq:NM_002227"
      hgnc "HGNC_SYMBOL:JAK1"
      map_id "M16_246"
      name "JAK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa9"
      uniprot "UNIPROT:P23458"
    ]
    graphics [
      x 983.0582955234685
      y 703.5023631995316
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_246"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_105"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re95"
      uniprot "NA"
    ]
    graphics [
      x 1054.9119977771009
      y 672.6178195192144
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:ec-code:2.7.10.2;urn:miriam:ncbigene:7297;urn:miriam:ncbigene:7297;urn:miriam:ensembl:ENSG00000105397;urn:miriam:refseq:NM_001385197;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc:12440;urn:miriam:uniprot:P29597;urn:miriam:uniprot:P29597"
      hgnc "HGNC_SYMBOL:TYK2"
      map_id "M16_141"
      name "TYK2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa19"
      uniprot "UNIPROT:P29597"
    ]
    graphics [
      x 933.1016795625991
      y 729.8054795129217
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_141"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:hgnc:5432;urn:miriam:uniprot:P17181;urn:miriam:uniprot:P17181;urn:miriam:refseq:NM_000629;urn:miriam:ensembl:ENSG00000142166;urn:miriam:hgnc.symbol:IFNAR1;urn:miriam:hgnc.symbol:IFNAR1;urn:miriam:ncbigene:3454;urn:miriam:ncbigene:3454;urn:miriam:hgnc:5433;urn:miriam:ncbigene:3455;urn:miriam:ensembl:ENSG00000159110;urn:miriam:ncbigene:3455;urn:miriam:hgnc.symbol:IFNAR2;urn:miriam:refseq:NM_000874;urn:miriam:hgnc.symbol:IFNAR2;urn:miriam:uniprot:P48551;urn:miriam:uniprot:P48551;urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:IFNA1;urn:miriam:hgnc.symbol:IFNA1;urn:miriam:ncbigene:3439;urn:miriam:ncbigene:3439;urn:miriam:refseq:NM_024013;urn:miriam:uniprot:P01562;urn:miriam:uniprot:P01562;urn:miriam:hgnc:5417;urn:miriam:ensembl:ENSG00000197919"
      hgnc "HGNC_SYMBOL:IFNAR1;HGNC_SYMBOL:IFNAR2;HGNC_SYMBOL:IFNA1"
      map_id "M16_4"
      name "IFNA1_underscore_IFNAR"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa2"
      uniprot "UNIPROT:P17181;UNIPROT:P48551;UNIPROT:P01562"
    ]
    graphics [
      x 878.866824129287
      y 895.1750955678203
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:ec-code:2.7.10.2;urn:miriam:ncbigene:7297;urn:miriam:ncbigene:7297;urn:miriam:ensembl:ENSG00000105397;urn:miriam:refseq:NM_001385197;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc:12440;urn:miriam:uniprot:P29597;urn:miriam:uniprot:P29597;urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:JAK1;urn:miriam:hgnc.symbol:JAK1;urn:miriam:wikipathways:WP4868;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc:6190;urn:miriam:ncbigene:3716;urn:miriam:ncbigene:3716;urn:miriam:ensembl:ENSG00000162434;urn:miriam:uniprot:P23458;urn:miriam:uniprot:P23458;urn:miriam:refseq:NM_002227"
      hgnc "HGNC_SYMBOL:TYK2;HGNC_SYMBOL:JAK1"
      map_id "M16_33"
      name "JAK1_underscore_TYK2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa9"
      uniprot "UNIPROT:P29597;UNIPROT:P23458"
    ]
    graphics [
      x 1267.2719084419427
      y 600.9992592652346
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 167
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_46"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re116"
      uniprot "NA"
    ]
    graphics [
      x 561.3682164213426
      y 1263.781261623462
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 168
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:pubmed:24622840;urn:miriam:hgnc:6118;urn:miriam:uniprot:Q14653;urn:miriam:uniprot:Q14653;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661;urn:miriam:ncbigene:3661"
      hgnc "HGNC_SYMBOL:IRF3"
      map_id "M16_112"
      name "IRF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa120"
      uniprot "UNIPROT:Q14653"
    ]
    graphics [
      x 1279.4120144256235
      y 953.2998755592895
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 169
    zlevel -1

    cd19dm [
      annotation "PUBMED:17761676;PUBMED:25636800;PUBMED:17108024;PUBMED:32979938;PUBMED:29294448;PUBMED:14679297;PUBMED:31226023;PUBMED:24622840;PUBMED:25481026;PUBMED:33337934;PUBMED:18440553"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_83"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re54"
      uniprot "NA"
    ]
    graphics [
      x 1183.6758751345312
      y 1021.5253048095778
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 170
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:ncbiprotein:YP_009724396.1"
      hgnc "NA"
      map_id "M16_10"
      name "Orf8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa27"
      uniprot "NA"
    ]
    graphics [
      x 1252.6400361958038
      y 1127.5612457691966
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 171
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ncbiprotein:YP_009724394.1"
      hgnc "NA"
      map_id "M16_115"
      name "Orf6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa127"
      uniprot "NA"
    ]
    graphics [
      x 1378.9385676202646
      y 877.8026162586953
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 172
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:32979938;urn:miriam:taxonomy:2697049;urn:miriam:ncbiprotein:YP_009742613.1"
      hgnc "NA"
      map_id "M16_189"
      name "Nsp6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa304"
      uniprot "NA"
    ]
    graphics [
      x 1222.3588232951881
      y 935.3290566104231
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_189"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 173
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ncbiprotein:YP_009725310"
      hgnc "NA"
      map_id "M16_199"
      name "Nsp15"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa316"
      uniprot "NA"
    ]
    graphics [
      x 1078.90389833036
      y 834.6244420365508
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_199"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 174
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:32913009"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re102"
      uniprot "NA"
    ]
    graphics [
      x 759.8286726057023
      y 1094.1130414187517
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 175
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:IFNA1;urn:miriam:hgnc.symbol:IFNA1;urn:miriam:wikipathways:WP4868;urn:miriam:ncbigene:3439;urn:miriam:ncbigene:3439;urn:miriam:refseq:NM_024013;urn:miriam:uniprot:P01562;urn:miriam:uniprot:P01562;urn:miriam:hgnc:5417;urn:miriam:ensembl:ENSG00000197919"
      hgnc "HGNC_SYMBOL:IFNA1"
      map_id "M16_218"
      name "IFNA1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa5"
      uniprot "UNIPROT:P01562"
    ]
    graphics [
      x 649.4237916465518
      y 1197.8669719134284
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_218"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 176
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:pubmed:19052324;urn:miriam:taxonomy:694009"
      hgnc "NA"
      map_id "M16_140"
      name "Viral_space_dsRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa187"
      uniprot "NA"
    ]
    graphics [
      x 712.5027048419789
      y 1477.9156912858616
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_140"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 177
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:32665127"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_90"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re69"
      uniprot "NA"
    ]
    graphics [
      x 626.720300989165
      y 1304.2796014405953
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 178
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:pubmed:19052324;urn:miriam:mesh:D014779"
      hgnc "NA"
      map_id "M16_130"
      name "Viral_space_replication"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa162"
      uniprot "NA"
    ]
    graphics [
      x 597.4535312513428
      y 1444.4094699676214
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_130"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 179
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:pubmed:19052324;urn:miriam:taxonomy:694009"
      hgnc "NA"
      map_id "M16_139"
      name "Viral_space_dsRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa186"
      uniprot "NA"
    ]
    graphics [
      x 799.6858283530495
      y 981.1959261814801
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_139"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 180
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ensembl:ENSG00000171855;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc:5434;urn:miriam:uniprot:P01574;urn:miriam:uniprot:P01574;urn:miriam:refseq:NM_002176;urn:miriam:ncbigene:3456;urn:miriam:ncbigene:3456"
      hgnc "HGNC_SYMBOL:IFNB1"
      map_id "M16_167"
      name "IFNB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa26"
      uniprot "UNIPROT:P01574"
    ]
    graphics [
      x 654.9370287409556
      y 1344.1454014556398
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_167"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 181
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_102"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re9"
      uniprot "NA"
    ]
    graphics [
      x 546.9152334737021
      y 1376.5391339266996
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 182
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_234"
      name "s63"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa75"
      uniprot "NA"
    ]
    graphics [
      x 428.13513974129836
      y 1422.461612902076
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_234"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 183
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_75"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re31"
      uniprot "NA"
    ]
    graphics [
      x 351.88150351627496
      y 1556.0168402475085
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 184
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:hgnc.symbol:OAS2;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:P29728;urn:miriam:ncbigene:4939;urn:miriam:hgnc:8087;urn:miriam:ensembl:ENSG00000111335;urn:miriam:refseq:NM_001032731"
      hgnc "HGNC_SYMBOL:OAS2"
      map_id "M16_232"
      name "OAS2"
      node_subtype "GENE"
      node_type "species"
      org_id "sa73"
      uniprot "UNIPROT:P29728"
    ]
    graphics [
      x 480.98806710847856
      y 1520.1196083674663
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_232"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 185
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:pubmed:24622840;urn:miriam:wikipathways:WP4868;urn:miriam:ncbigene:9641;urn:miriam:ncbigene:9641;urn:miriam:hgnc:14552;urn:miriam:refseq:NM_001193321;urn:miriam:pubmed:31226023;urn:miriam:uniprot:Q14164;urn:miriam:uniprot:Q14164;urn:miriam:pubmed:24622840;urn:miriam:ec-code:2.7.11.10;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:hgnc.symbol:IKBKE;urn:miriam:ensembl:ENSG00000263528;urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q9UHD2;urn:miriam:uniprot:Q9UHD2;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:pubmed:31226023;urn:miriam:pubmed:24622840;urn:miriam:ec-code:2.7.11.1;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110"
      hgnc "HGNC_SYMBOL:IKBKE;HGNC_SYMBOL:TBK1"
      map_id "M16_3"
      name "TBK1_underscore_IKBKE"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa19"
      uniprot "UNIPROT:Q14164;UNIPROT:Q9UHD2"
    ]
    graphics [
      x 1414.9592501095099
      y 1386.9503401468705
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 186
    zlevel -1

    cd19dm [
      annotation "PUBMED:24622840"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_42"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re111"
      uniprot "NA"
    ]
    graphics [
      x 1538.4867155869222
      y 1475.119731822854
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 187
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:uniprot:Q92985;urn:miriam:uniprot:Q92985;urn:miriam:ensembl:ENSG00000185507;urn:miriam:refseq:NM_001572;urn:miriam:hgnc.symbol:IRF7;urn:miriam:hgnc.symbol:IRF7;urn:miriam:hgnc:6122;urn:miriam:ncbigene:3665;urn:miriam:ncbigene:3665"
      hgnc "HGNC_SYMBOL:IRF7"
      map_id "M16_177"
      name "IRF7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa281"
      uniprot "UNIPROT:Q92985"
    ]
    graphics [
      x 1750.3346221424306
      y 1047.4760143377757
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_177"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 188
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_53"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re127"
      uniprot "NA"
    ]
    graphics [
      x 1570.4974374113208
      y 1033.9869364923848
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 189
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:uniprot:Q92985;urn:miriam:uniprot:Q92985;urn:miriam:ensembl:ENSG00000185507;urn:miriam:refseq:NM_001572;urn:miriam:hgnc.symbol:IRF7;urn:miriam:hgnc.symbol:IRF7;urn:miriam:hgnc:6122;urn:miriam:ncbigene:3665;urn:miriam:ncbigene:3665"
      hgnc "HGNC_SYMBOL:IRF7"
      map_id "M16_152"
      name "IRF7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa211"
      uniprot "UNIPROT:Q92985"
    ]
    graphics [
      x 1591.8908611018537
      y 1247.0942638058816
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_152"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 190
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:32726355;PUBMED:19052324"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_41"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re110"
      uniprot "NA"
    ]
    graphics [
      x 937.2716397310006
      y 663.3143837806069
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 191
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ncbiprotein:YP_009724393.1;urn:miriam:uniprot:M"
      hgnc "NA"
      map_id "M16_156"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa232"
      uniprot "UNIPROT:M"
    ]
    graphics [
      x 976.4895266909825
      y 534.5929962754497
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_156"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 192
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:ncbiprotein:YP_009725306"
      hgnc "NA"
      map_id "M16_200"
      name "Nsp10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa317"
      uniprot "NA"
    ]
    graphics [
      x 852.4461929997298
      y 713.4681388143056
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_200"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 193
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:ncbiprotein:YP_009725309"
      hgnc "NA"
      map_id "M16_201"
      name "Nsp14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa318"
      uniprot "NA"
    ]
    graphics [
      x 829.9685691252663
      y 650.3128782906217
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_201"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 194
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:ncbiprotein:YP_009725309"
      hgnc "NA"
      map_id "M16_202"
      name "Nsp16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa319"
      uniprot "NA"
    ]
    graphics [
      x 880.6861821285986
      y 544.1570527193708
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_202"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 195
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:pubmed:19052324;urn:miriam:pubmed:31226023;urn:miriam:hgnc:18873;urn:miriam:ncbigene:64135;urn:miriam:ncbigene:64135;urn:miriam:refseq:NM_022168;urn:miriam:ensembl:ENSG00000115267;urn:miriam:pubmed:19052324;urn:miriam:uniprot:Q9BYX4;urn:miriam:uniprot:Q9BYX4;urn:miriam:ec-code:3.6.4.13;urn:miriam:hgnc.symbol:IFIH1;urn:miriam:hgnc.symbol:IFIH1;urn:miriam:pubmed:31226023;urn:miriam:ncbigene:23586;urn:miriam:ncbigene:23586;urn:miriam:refseq:NM_014314;urn:miriam:ensembl:ENSG00000107201;urn:miriam:pubmed:19052324;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:uniprot:O95786;urn:miriam:hgnc:19102;urn:miriam:hgnc.symbol:DDX58;urn:miriam:hgnc.symbol:DDX58"
      hgnc "HGNC_SYMBOL:IFIH1;HGNC_SYMBOL:DDX58"
      map_id "M16_7"
      name "RIG1_underscore_MDA5"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa23"
      uniprot "UNIPROT:Q9BYX4;UNIPROT:O95786"
    ]
    graphics [
      x 676.1624585889722
      y 806.0685091641612
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 196
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_108"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re98"
      uniprot "NA"
    ]
    graphics [
      x 1085.5585959191283
      y 763.7789100843697
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 197
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:hgnc:9955;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ncbigene:5970;urn:miriam:ncbigene:5970;urn:miriam:refseq:NM_021975;urn:miriam:hgnc.symbol:RELA;urn:miriam:hgnc.symbol:RELA;urn:miriam:uniprot:Q04206;urn:miriam:uniprot:Q04206"
      hgnc "HGNC_SYMBOL:RELA"
      map_id "M16_172"
      name "RELA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa268"
      uniprot "UNIPROT:Q04206"
    ]
    graphics [
      x 1981.422292395008
      y 1545.116569926156
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_172"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 198
    zlevel -1

    cd19dm [
      annotation "PUBMED:33139913;PUBMED:31426357"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_49"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re119"
      uniprot "NA"
    ]
    graphics [
      x 1876.8937876796713
      y 1604.1625323563171
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 199
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998"
      hgnc "HGNC_SYMBOL:NFKB1"
      map_id "M16_173"
      name "NFKB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa269"
      uniprot "UNIPROT:P19838"
    ]
    graphics [
      x 1778.871034075969
      y 1542.3620275301842
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_173"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 200
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_175"
      name "IkB"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa270"
      uniprot "NA"
    ]
    graphics [
      x 1894.9210490830608
      y 1475.5264562987536
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_175"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 201
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:uniprot:Q96J02;urn:miriam:uniprot:Q96J02;urn:miriam:ec-code:2.3.2.26;urn:miriam:hgnc.symbol:ITCH;urn:miriam:hgnc.symbol:ITCH;urn:miriam:ensembl:ENSG00000078747;urn:miriam:refseq:NM_001257137;urn:miriam:ncbigene:83737;urn:miriam:ncbigene:83737;urn:miriam:hgnc:13890"
      hgnc "HGNC_SYMBOL:ITCH"
      map_id "M16_149"
      name "ITCH"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa203"
      uniprot "UNIPROT:Q96J02"
    ]
    graphics [
      x 97.69129046757303
      y 727.9335212251889
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_149"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 202
    zlevel -1

    cd19dm [
      annotation "PUBMED:25135833"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_97"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re81"
      uniprot "NA"
    ]
    graphics [
      x 179.38327482472005
      y 870.859713825872
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 203
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:uniprot:P0DTD2;urn:miriam:ncbiprotein:ABI96969"
      hgnc "NA"
      map_id "M16_120"
      name "Orf9b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa139"
      uniprot "UNIPROT:P0DTD2"
    ]
    graphics [
      x 266.9124992622318
      y 957.2795643969524
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 204
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_67"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re14"
      uniprot "NA"
    ]
    graphics [
      x 908.2516642008271
      y 353.23665894919316
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 205
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:33348292;PUBMED:19380580"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_40"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re107"
      uniprot "NA"
    ]
    graphics [
      x 1249.2658180091346
      y 1211.6740797723894
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 206
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:refseq:NM_138554;urn:miriam:ncbigene:7099;urn:miriam:ncbigene:7099;urn:miriam:ec-code:3.2.2.6;urn:miriam:hgnc:11850;urn:miriam:uniprot:O00206;urn:miriam:uniprot:O00206;urn:miriam:hgnc.symbol:TLR4;urn:miriam:hgnc.symbol:TLR4;urn:miriam:ensembl:ENSG00000136869"
      hgnc "HGNC_SYMBOL:TLR4"
      map_id "M16_179"
      name "TLR4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa291"
      uniprot "UNIPROT:O00206"
    ]
    graphics [
      x 337.25418833887704
      y 1087.4600259063332
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_179"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 207
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_58"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re131"
      uniprot "NA"
    ]
    graphics [
      x 457.2373373550686
      y 1163.0775779997361
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 208
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_180"
      name "IRAK1_slash_4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa292"
      uniprot "NA"
    ]
    graphics [
      x 314.9309460090359
      y 1165.1704400792992
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_180"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 209
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:P00973;urn:miriam:uniprot:P00973;urn:miriam:ncbigene:4938;urn:miriam:ncbigene:4938;urn:miriam:refseq:NM_001032409;urn:miriam:hgnc.symbol:OAS1;urn:miriam:hgnc.symbol:OAS1;urn:miriam:hgnc:8086;urn:miriam:ec-code:2.7.7.84;urn:miriam:ensembl:ENSG00000089127"
      hgnc "HGNC_SYMBOL:OAS1"
      map_id "M16_219"
      name "OAS1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa50"
      uniprot "UNIPROT:P00973"
    ]
    graphics [
      x 750.6868443509011
      y 1011.8464957530207
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_219"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 210
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_104"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re94"
      uniprot "NA"
    ]
    graphics [
      x 756.4476042744122
      y 784.8421673564271
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 211
    zlevel -1

    cd19dm [
      annotation "PUBMED:33473130"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_62"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re135"
      uniprot "NA"
    ]
    graphics [
      x 1089.0393590317776
      y 1294.7714956550972
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 212
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:wikipathways:WP4868"
      hgnc "NA"
      map_id "M16_133"
      name "GRL0617"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa168"
      uniprot "NA"
    ]
    graphics [
      x 1090.9502943705106
      y 1427.8860747494489
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_133"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 213
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000187608;urn:miriam:refseq:NM_005101;urn:miriam:ncbigene:9636;urn:miriam:ncbigene:9636;urn:miriam:hgnc.symbol:ISG15;urn:miriam:hgnc.symbol:ISG15;urn:miriam:uniprot:P05161;urn:miriam:uniprot:P05161;urn:miriam:hgnc:4053;urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:ncbiprotein:YP_009725299;urn:miriam:uniprot:Nsp3"
      hgnc "HGNC_SYMBOL:ISG15"
      map_id "M16_30"
      name "ISG15_underscore_Nsp3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa48"
      uniprot "UNIPROT:P05161;UNIPROT:Nsp3"
    ]
    graphics [
      x 1159.4336418948808
      y 1420.5980268799453
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 214
    zlevel -1

    cd19dm [
      annotation "PUBMED:32979938"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_57"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re130"
      uniprot "NA"
    ]
    graphics [
      x 140.2813777432924
      y 1236.4272628986198
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 215
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ncbiprotein:YP_009724394.1"
      hgnc "NA"
      map_id "M16_197"
      name "Orf6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa312"
      uniprot "NA"
    ]
    graphics [
      x 78.47691268085725
      y 1329.0702813195107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_197"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 216
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_88"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re67"
      uniprot "NA"
    ]
    graphics [
      x 823.8329667596198
      y 1197.780361025842
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 217
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A2955"
      hgnc "NA"
      map_id "M16_204"
      name "Azithromycin"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa321"
      uniprot "NA"
    ]
    graphics [
      x 552.2816326268276
      y 408.1324029347983
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_204"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 218
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_68"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re140"
      uniprot "NA"
    ]
    graphics [
      x 726.6334639103595
      y 376.25834659302325
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 219
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ensembl:ENSG00000171855;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc:5434;urn:miriam:uniprot:P01574;urn:miriam:refseq:NM_002176;urn:miriam:ncbigene:3456"
      hgnc "HGNC_SYMBOL:IFNB1"
      map_id "M16_206"
      name "IFNB1"
      node_subtype "RNA"
      node_type "species"
      org_id "sa34"
      uniprot "UNIPROT:P01574"
    ]
    graphics [
      x 518.0038838754258
      y 856.5933845630672
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_206"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 220
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_50"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re12"
      uniprot "NA"
    ]
    graphics [
      x 618.1388058317042
      y 1121.6956464964667
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 221
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_52"
      name "NA"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "re122"
      uniprot "NA"
    ]
    graphics [
      x 1022.006368641837
      y 1220.551106926424
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 222
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_176"
      name "IkB"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa278"
      uniprot "NA"
    ]
    graphics [
      x 924.595117057724
      y 1202.4003799272773
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_176"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 223
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:33024073"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_101"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re89"
      uniprot "NA"
    ]
    graphics [
      x 1424.8933024139737
      y 702.9534298603626
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 224
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868;urn:miriam:uniprot:Q00978;urn:miriam:uniprot:Q00978;urn:miriam:refseq:NM_001385400;urn:miriam:ensembl:ENSG00000213928;urn:miriam:hgnc.symbol:IRF9;urn:miriam:ncbigene:10379;urn:miriam:hgnc.symbol:IRF9;urn:miriam:ncbigene:10379;urn:miriam:hgnc:6131"
      hgnc "HGNC_SYMBOL:IRF9"
      map_id "M16_231"
      name "IRF9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa70"
      uniprot "UNIPROT:Q00978"
    ]
    graphics [
      x 1505.0466389657852
      y 631.165309614925
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_231"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 225
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_78"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re34"
      uniprot "NA"
    ]
    graphics [
      x 307.32112354588605
      y 928.1893618633828
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 226
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:uniprot:Q92985;urn:miriam:uniprot:Q92985;urn:miriam:ensembl:ENSG00000185507;urn:miriam:refseq:NM_001572;urn:miriam:hgnc.symbol:IRF7;urn:miriam:hgnc.symbol:IRF7;urn:miriam:hgnc:6122;urn:miriam:ncbigene:3665;urn:miriam:ncbigene:3665"
      hgnc "HGNC_SYMBOL:IRF7"
      map_id "M16_153"
      name "IRF7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa213"
      uniprot "UNIPROT:Q92985"
    ]
    graphics [
      x 1494.5734079685121
      y 1592.0983694703914
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_153"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 227
    zlevel -1

    cd19dm [
      annotation "PUBMED:19380580"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_99"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re84"
      uniprot "NA"
    ]
    graphics [
      x 1481.9747715064434
      y 1438.0857224175115
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 228
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_81"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re42"
      uniprot "NA"
    ]
    graphics [
      x 236.6971540338734
      y 363.4750726425983
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 229
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_44"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re114"
      uniprot "NA"
    ]
    graphics [
      x 855.3713512762404
      y 1881.0320104728771
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 230
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_161"
      name "RIP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa247"
      uniprot "NA"
    ]
    graphics [
      x 833.972645106467
      y 2013.4737364173795
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_161"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 231
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:uniprot:O43318;urn:miriam:uniprot:O43318;urn:miriam:wikipathways:WP4868;urn:miriam:ensembl:ENSG00000135341;urn:miriam:refseq:NM_145331;urn:miriam:hgnc:6859;urn:miriam:ncbigene:6885;urn:miriam:ncbigene:6885;urn:miriam:hgnc.symbol:MAP3K7;urn:miriam:hgnc.symbol:MAP3K7;urn:miriam:ec-code:2.7.11.25"
      hgnc "HGNC_SYMBOL:MAP3K7"
      map_id "M16_243"
      name "MAP3K7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa85"
      uniprot "UNIPROT:O43318"
    ]
    graphics [
      x 811.8314551426542
      y 1290.004519190606
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_243"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 232
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_80"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re40"
      uniprot "NA"
    ]
    graphics [
      x 708.7423055047047
      y 1221.0400821652393
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 233
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_217"
      name "s39"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa49"
      uniprot "NA"
    ]
    graphics [
      x 823.792587410986
      y 1687.7742871895257
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_217"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 234
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_70"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re17"
      uniprot "NA"
    ]
    graphics [
      x 616.083465928389
      y 1611.4972284544924
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 235
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868"
      hgnc "NA"
      map_id "M16_215"
      name "EIF2AK"
      node_subtype "GENE"
      node_type "species"
      org_id "sa47"
      uniprot "NA"
    ]
    graphics [
      x 734.5542644838039
      y 1717.3373360051432
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_215"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 236
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_169"
      name "NEMO"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa265"
      uniprot "NA"
    ]
    graphics [
      x 1771.6073385931204
      y 1379.5835933726062
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_169"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 237
    zlevel -1

    cd19dm [
      annotation "PUBMED:33139913;PUBMED:31426357"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_48"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re118"
      uniprot "NA"
    ]
    graphics [
      x 1624.9584239340613
      y 1425.4763403089287
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 238
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_170"
      name "IKKa"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa266"
      uniprot "NA"
    ]
    graphics [
      x 1762.3147288079194
      y 1450.2106415580238
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_170"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 239
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_171"
      name "IKKb"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa267"
      uniprot "NA"
    ]
    graphics [
      x 1741.7431113569428
      y 1319.469959189036
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_171"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 240
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_207"
      name "s22"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa35"
      uniprot "NA"
    ]
    graphics [
      x 383.98853959220605
      y 497.4196120742821
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_207"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 241
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:33348292"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10"
      uniprot "NA"
    ]
    graphics [
      x 459.9345247718814
      y 612.5944879518536
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 242
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:ensembl:ENSG00000171855;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc:5434;urn:miriam:uniprot:P01574;urn:miriam:refseq:NM_002176;urn:miriam:ncbigene:3456"
      hgnc "HGNC_SYMBOL:IFNB1"
      map_id "M16_205"
      name "IFNB1"
      node_subtype "GENE"
      node_type "species"
      org_id "sa33"
      uniprot "UNIPROT:P01574"
    ]
    graphics [
      x 329.51677188584665
      y 560.1594309142268
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_205"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 243
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:uniprot:Q7Z434;urn:miriam:uniprot:Q7Z434;urn:miriam:pubmed:24622840;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:pubmed:19052324;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746"
      hgnc "HGNC_SYMBOL:MAVS"
      map_id "M16_125"
      name "MAVS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa156"
      uniprot "UNIPROT:Q7Z434"
    ]
    graphics [
      x 361.4266060282596
      y 997.2314254151055
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_125"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 244
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023;PUBMED:19052324;PUBMED:25135833"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_85"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re64"
      uniprot "NA"
    ]
    graphics [
      x 533.8401801863995
      y 1019.3382391947914
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 245
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "urn:miriam:pubmed:31226023;urn:miriam:wikipathways:WP4868"
      hgnc "NA"
      map_id "M16_129"
      name "EIF2AK"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa160"
      uniprot "NA"
    ]
    graphics [
      x 1136.884089580233
      y 804.2414176153168
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_129"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 246
    zlevel -1

    cd19dm [
      annotation "PUBMED:31226023"
      count 1
      diagram "C19DMap:Interferon 1 pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M16_89"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re68"
      uniprot "NA"
    ]
    graphics [
      x 1006.0788488762262
      y 902.6329321380033
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M16_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 247
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_238"
      target_id "M16_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 248
    source 3
    target 2
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "M16_31"
      target_id "M16_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 249
    source 4
    target 2
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "M16_236"
      target_id "M16_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 250
    source 5
    target 2
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "M16_28"
      target_id "M16_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 251
    source 3
    target 2
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M16_31"
      target_id "M16_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 252
    source 4
    target 2
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M16_236"
      target_id "M16_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 253
    source 5
    target 2
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M16_28"
      target_id "M16_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 254
    source 2
    target 6
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_77"
      target_id "M16_237"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 255
    source 7
    target 8
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_216"
      target_id "M16_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 256
    source 8
    target 9
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_72"
      target_id "M16_220"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 257
    source 10
    target 11
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_111"
      target_id "M16_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 258
    source 12
    target 11
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_114"
      target_id "M16_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 259
    source 13
    target 11
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_226"
      target_id "M16_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 260
    source 11
    target 14
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_82"
      target_id "M16_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 261
    source 15
    target 16
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_113"
      target_id "M16_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 262
    source 17
    target 16
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_178"
      target_id "M16_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 263
    source 16
    target 18
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_55"
      target_id "M16_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 264
    source 19
    target 20
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_233"
      target_id "M16_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 265
    source 20
    target 21
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_76"
      target_id "M16_127"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 266
    source 22
    target 23
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_1"
      target_id "M16_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 267
    source 24
    target 23
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_185"
      target_id "M16_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 268
    source 25
    target 23
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_174"
      target_id "M16_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 269
    source 23
    target 26
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_37"
      target_id "M16_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 270
    source 27
    target 28
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_225"
      target_id "M16_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 271
    source 14
    target 28
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_110"
      target_id "M16_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 272
    source 29
    target 28
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_137"
      target_id "M16_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 273
    source 30
    target 28
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_122"
      target_id "M16_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 274
    source 31
    target 28
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_132"
      target_id "M16_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 275
    source 28
    target 13
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_74"
      target_id "M16_226"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 276
    source 32
    target 33
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_14"
      target_id "M16_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 277
    source 34
    target 33
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_117"
      target_id "M16_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 278
    source 35
    target 33
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_16"
      target_id "M16_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 36
    target 33
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_157"
      target_id "M16_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 33
    target 37
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_100"
      target_id "M16_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 38
    target 39
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_136"
      target_id "M16_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 30
    target 39
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_122"
      target_id "M16_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 40
    target 39
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_138"
      target_id "M16_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 39
    target 29
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_92"
      target_id "M16_137"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 41
    target 42
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_23"
      target_id "M16_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 43
    target 42
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_22"
      target_id "M16_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 42
    target 44
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_51"
      target_id "M16_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 21
    target 45
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_127"
      target_id "M16_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 46
    target 45
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_116"
      target_id "M16_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 45
    target 47
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_87"
      target_id "M16_235"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 48
    target 49
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_134"
      target_id "M16_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 49
    target 12
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_91"
      target_id "M16_114"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 37
    target 50
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_13"
      target_id "M16_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 50
    target 51
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_35"
      target_id "M16_144"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 52
    target 53
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_190"
      target_id "M16_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 54
    target 53
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_118"
      target_id "M16_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 55
    target 53
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_192"
      target_id "M16_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 56
    target 53
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_194"
      target_id "M16_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 57
    target 53
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_193"
      target_id "M16_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 58
    target 53
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_142"
      target_id "M16_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 59
    target 53
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_119"
      target_id "M16_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 53
    target 60
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_63"
      target_id "M16_229"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 61
    target 62
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_214"
      target_id "M16_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 304
    source 3
    target 62
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "M16_31"
      target_id "M16_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 305
    source 63
    target 62
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "M16_212"
      target_id "M16_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 306
    source 5
    target 62
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "M16_28"
      target_id "M16_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 307
    source 3
    target 62
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M16_31"
      target_id "M16_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 308
    source 63
    target 62
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M16_212"
      target_id "M16_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 309
    source 5
    target 62
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M16_28"
      target_id "M16_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 310
    source 62
    target 64
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_69"
      target_id "M16_213"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 311
    source 64
    target 65
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_213"
      target_id "M16_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 312
    source 65
    target 66
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_71"
      target_id "M16_128"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 313
    source 67
    target 68
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_191"
      target_id "M16_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 314
    source 69
    target 68
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_143"
      target_id "M16_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 315
    source 57
    target 68
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_193"
      target_id "M16_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 316
    source 55
    target 68
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_192"
      target_id "M16_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 317
    source 56
    target 68
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_194"
      target_id "M16_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 318
    source 68
    target 70
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_64"
      target_id "M16_230"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 319
    source 71
    target 72
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_15"
      target_id "M16_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 320
    source 72
    target 73
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_107"
      target_id "M16_240"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 321
    source 74
    target 75
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_181"
      target_id "M16_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 322
    source 76
    target 75
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_29"
      target_id "M16_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 323
    source 75
    target 77
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_59"
      target_id "M16_182"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 324
    source 78
    target 79
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_6"
      target_id "M16_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 325
    source 79
    target 35
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_84"
      target_id "M16_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 326
    source 80
    target 81
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_8"
      target_id "M16_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 327
    source 29
    target 81
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_137"
      target_id "M16_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 328
    source 40
    target 81
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_138"
      target_id "M16_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 329
    source 81
    target 82
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_93"
      target_id "M16_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 330
    source 82
    target 83
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_9"
      target_id "M16_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 331
    source 84
    target 83
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_151"
      target_id "M16_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 332
    source 85
    target 83
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_18"
      target_id "M16_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 333
    source 86
    target 83
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_154"
      target_id "M16_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 334
    source 30
    target 83
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_122"
      target_id "M16_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 335
    source 83
    target 87
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_98"
      target_id "M16_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 336
    source 88
    target 89
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_184"
      target_id "M16_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 337
    source 89
    target 90
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_61"
      target_id "M16_188"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 338
    source 91
    target 92
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_126"
      target_id "M16_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 339
    source 46
    target 92
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_116"
      target_id "M16_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 340
    source 92
    target 93
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_86"
      target_id "M16_239"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 341
    source 25
    target 94
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_174"
      target_id "M16_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 342
    source 94
    target 95
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_47"
      target_id "M16_168"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 343
    source 96
    target 97
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_241"
      target_id "M16_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 344
    source 98
    target 97
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_5"
      target_id "M16_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 345
    source 12
    target 97
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_114"
      target_id "M16_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 346
    source 97
    target 99
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_79"
      target_id "M16_242"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 347
    source 100
    target 101
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_186"
      target_id "M16_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 348
    source 101
    target 102
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_95"
      target_id "M16_195"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 349
    source 103
    target 104
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_145"
      target_id "M16_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 350
    source 105
    target 104
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_155"
      target_id "M16_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 351
    source 106
    target 104
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_203"
      target_id "M16_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 352
    source 104
    target 107
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_94"
      target_id "M16_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 353
    source 35
    target 108
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_16"
      target_id "M16_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 354
    source 108
    target 73
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_103"
      target_id "M16_240"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 355
    source 109
    target 110
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_159"
      target_id "M16_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 356
    source 111
    target 110
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_160"
      target_id "M16_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 357
    source 112
    target 110
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "TRIGGER"
      source_id "M16_166"
      target_id "M16_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 358
    source 113
    target 110
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_135"
      target_id "M16_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 359
    source 110
    target 85
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_43"
      target_id "M16_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 360
    source 114
    target 115
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_146"
      target_id "M16_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 361
    source 116
    target 115
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_121"
      target_id "M16_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 362
    source 117
    target 115
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_147"
      target_id "M16_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 363
    source 118
    target 115
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_148"
      target_id "M16_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 364
    source 119
    target 115
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_162"
      target_id "M16_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 365
    source 106
    target 115
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_203"
      target_id "M16_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 366
    source 115
    target 120
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_96"
      target_id "M16_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 367
    source 121
    target 122
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_228"
      target_id "M16_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 368
    source 123
    target 122
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_227"
      target_id "M16_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 369
    source 13
    target 122
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_226"
      target_id "M16_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 370
    source 124
    target 122
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_244"
      target_id "M16_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 371
    source 122
    target 125
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_38"
      target_id "M16_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 372
    source 126
    target 127
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_25"
      target_id "M16_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 373
    source 127
    target 128
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_54"
      target_id "M16_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 374
    source 129
    target 130
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_21"
      target_id "M16_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 375
    source 131
    target 130
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_196"
      target_id "M16_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 376
    source 130
    target 3
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_65"
      target_id "M16_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 377
    source 132
    target 133
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_222"
      target_id "M16_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 378
    source 134
    target 133
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_198"
      target_id "M16_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 379
    source 135
    target 133
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_131"
      target_id "M16_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 380
    source 136
    target 133
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_221"
      target_id "M16_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 381
    source 137
    target 133
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_158"
      target_id "M16_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 382
    source 112
    target 133
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "TRIGGER"
      source_id "M16_166"
      target_id "M16_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 383
    source 133
    target 98
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_106"
      target_id "M16_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 384
    source 138
    target 139
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_163"
      target_id "M16_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 385
    source 140
    target 139
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_164"
      target_id "M16_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 386
    source 141
    target 139
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_224"
      target_id "M16_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 387
    source 142
    target 139
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_165"
      target_id "M16_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 388
    source 143
    target 139
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_19"
      target_id "M16_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 389
    source 139
    target 144
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_45"
      target_id "M16_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 390
    source 145
    target 146
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_223"
      target_id "M16_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 391
    source 98
    target 146
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_5"
      target_id "M16_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 392
    source 12
    target 146
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_114"
      target_id "M16_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 393
    source 147
    target 146
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_150"
      target_id "M16_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 394
    source 146
    target 141
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_73"
      target_id "M16_224"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 395
    source 148
    target 149
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_208"
      target_id "M16_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 396
    source 150
    target 149
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_245"
      target_id "M16_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 397
    source 34
    target 149
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_117"
      target_id "M16_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 398
    source 125
    target 149
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_32"
      target_id "M16_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 399
    source 149
    target 151
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_39"
      target_id "M16_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 400
    source 152
    target 153
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_187"
      target_id "M16_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 401
    source 154
    target 153
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "M16_183"
      target_id "M16_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 402
    source 3
    target 153
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "M16_31"
      target_id "M16_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 403
    source 154
    target 153
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M16_183"
      target_id "M16_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 404
    source 3
    target 153
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M16_31"
      target_id "M16_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 405
    source 153
    target 88
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_60"
      target_id "M16_184"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 406
    source 35
    target 155
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_16"
      target_id "M16_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 407
    source 155
    target 51
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_109"
      target_id "M16_144"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 408
    source 156
    target 157
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_211"
      target_id "M16_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 409
    source 158
    target 157
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "M16_17"
      target_id "M16_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 410
    source 159
    target 157
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "M16_209"
      target_id "M16_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 411
    source 35
    target 157
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "M16_16"
      target_id "M16_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 412
    source 128
    target 157
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "M16_26"
      target_id "M16_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 413
    source 158
    target 157
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M16_17"
      target_id "M16_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 414
    source 159
    target 157
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M16_209"
      target_id "M16_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 415
    source 35
    target 157
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M16_16"
      target_id "M16_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 416
    source 128
    target 157
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M16_26"
      target_id "M16_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 417
    source 157
    target 160
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_56"
      target_id "M16_210"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 418
    source 90
    target 161
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_188"
      target_id "M16_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 419
    source 161
    target 73
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_66"
      target_id "M16_240"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 420
    source 162
    target 163
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_246"
      target_id "M16_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 421
    source 164
    target 163
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_141"
      target_id "M16_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 422
    source 165
    target 163
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_4"
      target_id "M16_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 423
    source 26
    target 163
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_12"
      target_id "M16_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 424
    source 163
    target 166
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_105"
      target_id "M16_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 425
    source 102
    target 167
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_195"
      target_id "M16_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 426
    source 167
    target 95
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_46"
      target_id "M16_168"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 427
    source 168
    target 169
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_112"
      target_id "M16_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 428
    source 12
    target 169
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_114"
      target_id "M16_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 429
    source 116
    target 169
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_121"
      target_id "M16_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 430
    source 170
    target 169
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_10"
      target_id "M16_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 431
    source 171
    target 169
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_115"
      target_id "M16_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 432
    source 40
    target 169
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_138"
      target_id "M16_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 433
    source 30
    target 169
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_122"
      target_id "M16_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 434
    source 87
    target 169
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_11"
      target_id "M16_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 435
    source 172
    target 169
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_189"
      target_id "M16_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 436
    source 173
    target 169
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_199"
      target_id "M16_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 437
    source 169
    target 15
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_83"
      target_id "M16_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 438
    source 22
    target 174
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_1"
      target_id "M16_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 439
    source 175
    target 174
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_218"
      target_id "M16_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 440
    source 102
    target 174
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_195"
      target_id "M16_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 441
    source 174
    target 165
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_36"
      target_id "M16_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 442
    source 176
    target 177
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_140"
      target_id "M16_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 443
    source 178
    target 177
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "TRIGGER"
      source_id "M16_130"
      target_id "M16_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 444
    source 102
    target 177
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_195"
      target_id "M16_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 445
    source 25
    target 177
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_174"
      target_id "M16_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 446
    source 177
    target 179
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_90"
      target_id "M16_139"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 447
    source 180
    target 181
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_167"
      target_id "M16_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 448
    source 181
    target 25
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_102"
      target_id "M16_174"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 449
    source 182
    target 183
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_234"
      target_id "M16_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 450
    source 3
    target 183
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "M16_31"
      target_id "M16_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 451
    source 184
    target 183
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "M16_232"
      target_id "M16_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 452
    source 5
    target 183
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "M16_28"
      target_id "M16_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 453
    source 3
    target 183
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M16_31"
      target_id "M16_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 454
    source 184
    target 183
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M16_232"
      target_id "M16_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 455
    source 5
    target 183
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M16_28"
      target_id "M16_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 456
    source 183
    target 19
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_75"
      target_id "M16_233"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 457
    source 185
    target 186
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_3"
      target_id "M16_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 458
    source 99
    target 186
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_242"
      target_id "M16_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 459
    source 40
    target 186
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_138"
      target_id "M16_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 460
    source 186
    target 80
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_42"
      target_id "M16_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 461
    source 187
    target 188
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_177"
      target_id "M16_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 462
    source 189
    target 188
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_152"
      target_id "M16_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 463
    source 188
    target 126
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_53"
      target_id "M16_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 464
    source 120
    target 190
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_124"
      target_id "M16_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 465
    source 107
    target 190
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_123"
      target_id "M16_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 466
    source 179
    target 190
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_139"
      target_id "M16_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 467
    source 116
    target 190
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_121"
      target_id "M16_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 468
    source 105
    target 190
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_155"
      target_id "M16_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 469
    source 191
    target 190
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_156"
      target_id "M16_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 470
    source 173
    target 190
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_199"
      target_id "M16_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 471
    source 192
    target 190
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_200"
      target_id "M16_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 472
    source 193
    target 190
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_201"
      target_id "M16_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 473
    source 194
    target 190
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_202"
      target_id "M16_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 474
    source 190
    target 195
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_41"
      target_id "M16_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 475
    source 12
    target 196
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_114"
      target_id "M16_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 476
    source 196
    target 73
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_108"
      target_id "M16_240"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 477
    source 197
    target 198
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_172"
      target_id "M16_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 478
    source 199
    target 198
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_173"
      target_id "M16_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 479
    source 200
    target 198
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_175"
      target_id "M16_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 480
    source 198
    target 41
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_49"
      target_id "M16_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 481
    source 201
    target 202
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_149"
      target_id "M16_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 482
    source 203
    target 202
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_120"
      target_id "M16_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 483
    source 202
    target 147
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_97"
      target_id "M16_150"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 484
    source 160
    target 204
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_210"
      target_id "M16_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 485
    source 204
    target 100
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_67"
      target_id "M16_186"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 486
    source 13
    target 205
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_226"
      target_id "M16_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 487
    source 14
    target 205
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_110"
      target_id "M16_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 488
    source 99
    target 205
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_242"
      target_id "M16_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 489
    source 12
    target 205
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_114"
      target_id "M16_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 490
    source 77
    target 205
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_182"
      target_id "M16_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 491
    source 113
    target 205
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_135"
      target_id "M16_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 492
    source 205
    target 185
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_40"
      target_id "M16_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 493
    source 206
    target 207
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_179"
      target_id "M16_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 494
    source 208
    target 207
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_180"
      target_id "M16_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 495
    source 111
    target 207
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_160"
      target_id "M16_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 496
    source 207
    target 76
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_58"
      target_id "M16_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 497
    source 209
    target 210
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_219"
      target_id "M16_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 498
    source 93
    target 210
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_239"
      target_id "M16_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 499
    source 9
    target 210
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_220"
      target_id "M16_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 500
    source 47
    target 210
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_235"
      target_id "M16_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 501
    source 210
    target 71
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_104"
      target_id "M16_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 502
    source 90
    target 211
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_188"
      target_id "M16_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 503
    source 12
    target 211
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_114"
      target_id "M16_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 504
    source 212
    target 211
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_133"
      target_id "M16_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 505
    source 211
    target 213
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_62"
      target_id "M16_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 506
    source 18
    target 214
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_27"
      target_id "M16_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 507
    source 215
    target 214
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_197"
      target_id "M16_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 508
    source 214
    target 5
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_57"
      target_id "M16_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 509
    source 66
    target 216
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_128"
      target_id "M16_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 510
    source 46
    target 216
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_116"
      target_id "M16_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 511
    source 216
    target 209
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_88"
      target_id "M16_219"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 512
    source 217
    target 218
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_204"
      target_id "M16_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 513
    source 218
    target 73
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_68"
      target_id "M16_240"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 514
    source 219
    target 220
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_206"
      target_id "M16_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 515
    source 220
    target 180
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_50"
      target_id "M16_167"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 516
    source 44
    target 221
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_24"
      target_id "M16_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 517
    source 221
    target 78
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_52"
      target_id "M16_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 518
    source 221
    target 222
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_52"
      target_id "M16_176"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 519
    source 60
    target 223
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_229"
      target_id "M16_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 520
    source 224
    target 223
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_231"
      target_id "M16_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 521
    source 70
    target 223
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_230"
      target_id "M16_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 522
    source 171
    target 223
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_115"
      target_id "M16_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 523
    source 135
    target 223
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_131"
      target_id "M16_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 524
    source 166
    target 223
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_33"
      target_id "M16_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 525
    source 119
    target 223
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_162"
      target_id "M16_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 526
    source 223
    target 129
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_101"
      target_id "M16_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 527
    source 6
    target 225
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_237"
      target_id "M16_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 528
    source 225
    target 91
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_78"
      target_id "M16_126"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 529
    source 226
    target 227
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_153"
      target_id "M16_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 530
    source 87
    target 227
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_11"
      target_id "M16_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 531
    source 227
    target 189
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_99"
      target_id "M16_152"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 532
    source 151
    target 228
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_2"
      target_id "M16_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 533
    source 228
    target 158
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_81"
      target_id "M16_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 534
    source 85
    target 229
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_18"
      target_id "M16_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 535
    source 230
    target 229
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_161"
      target_id "M16_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 536
    source 229
    target 143
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_44"
      target_id "M16_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 537
    source 231
    target 232
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_243"
      target_id "M16_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 538
    source 141
    target 232
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_224"
      target_id "M16_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 539
    source 232
    target 124
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_80"
      target_id "M16_244"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 540
    source 233
    target 234
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_217"
      target_id "M16_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 541
    source 3
    target 234
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "M16_31"
      target_id "M16_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 542
    source 235
    target 234
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "M16_215"
      target_id "M16_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 543
    source 5
    target 234
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "M16_28"
      target_id "M16_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 544
    source 3
    target 234
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M16_31"
      target_id "M16_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 545
    source 235
    target 234
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M16_215"
      target_id "M16_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 546
    source 5
    target 234
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M16_28"
      target_id "M16_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 547
    source 234
    target 7
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_70"
      target_id "M16_216"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 548
    source 236
    target 237
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_169"
      target_id "M16_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 549
    source 238
    target 237
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_170"
      target_id "M16_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 550
    source 239
    target 237
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_171"
      target_id "M16_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 551
    source 144
    target 237
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_20"
      target_id "M16_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 552
    source 237
    target 43
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_48"
      target_id "M16_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 553
    source 240
    target 241
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_207"
      target_id "M16_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 554
    source 158
    target 241
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "M16_17"
      target_id "M16_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 555
    source 242
    target 241
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "M16_205"
      target_id "M16_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 556
    source 35
    target 241
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "M16_16"
      target_id "M16_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 557
    source 5
    target 241
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "M16_28"
      target_id "M16_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 558
    source 128
    target 241
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "M16_26"
      target_id "M16_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 559
    source 158
    target 241
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M16_17"
      target_id "M16_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 560
    source 242
    target 241
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M16_205"
      target_id "M16_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 561
    source 35
    target 241
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M16_16"
      target_id "M16_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 562
    source 5
    target 241
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M16_28"
      target_id "M16_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 563
    source 128
    target 241
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M16_26"
      target_id "M16_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 564
    source 217
    target 241
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_204"
      target_id "M16_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 565
    source 241
    target 219
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_34"
      target_id "M16_206"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 566
    source 243
    target 244
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_125"
      target_id "M16_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 567
    source 195
    target 244
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CATALYSIS"
      source_id "M16_7"
      target_id "M16_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 568
    source 203
    target 244
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_120"
      target_id "M16_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 569
    source 147
    target 244
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_150"
      target_id "M16_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 570
    source 244
    target 30
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_85"
      target_id "M16_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 571
    source 245
    target 246
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "CONSPUMPTION"
      source_id "M16_129"
      target_id "M16_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 572
    source 46
    target 246
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "INHIBITION"
      source_id "M16_116"
      target_id "M16_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 573
    source 246
    target 9
    cd19dm [
      diagram "C19DMap:Interferon 1 pathway"
      edge_type "PRODUCTION"
      source_id "M16_89"
      target_id "M16_220"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
