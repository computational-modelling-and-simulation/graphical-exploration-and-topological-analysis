# generated with VANTED V2.8.2 at Fri Mar 04 09:57:02 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:wikidata:Q582356"
      hgnc "NA"
      map_id "W21_18"
      name "Interferon_space_gamma_minus_1b"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "acad9"
      uniprot "NA"
    ]
    graphics [
      x 1521.073244529468
      y 1213.8217138741977
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306;PUBMED:15580300"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_132"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "f6ee1"
      uniprot "NA"
    ]
    graphics [
      x 1555.2782672559122
      y 1090.6559986091956
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000115129"
      hgnc "NA"
      map_id "W21_34"
      name "P53"
      node_subtype "GENE"
      node_type "species"
      org_id "b47f9"
      uniprot "NA"
    ]
    graphics [
      x 1516.502100190653
      y 941.7092747405428
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000131323;urn:miriam:ensembl:ENSG00000102871;urn:miriam:ensembl:ENSG00000127191;urn:miriam:ensembl:ENSG00000175104;urn:miriam:ensembl:ENSG00000082512;urn:miriam:ensembl:ENSG00000088888"
      hgnc "NA"
      map_id "W21_110"
      name "ea43c"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ea43c"
      uniprot "NA"
    ]
    graphics [
      x 154.84731724624112
      y 517.9211776661201
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_49"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "ba969"
      uniprot "NA"
    ]
    graphics [
      x 124.30449824052539
      y 405.2032725958536
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000136560"
      hgnc "NA"
      map_id "W21_116"
      name "TANK"
      node_subtype "GENE"
      node_type "species"
      org_id "ede74"
      uniprot "NA"
    ]
    graphics [
      x 236.0087566544289
      y 463.3939972173244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "W21_72"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "cedc7"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 1569.6609465339338
      y 557.6047667799232
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "PUBMED:33372174;PUBMED:33101306"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_87"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "db7f5"
      uniprot "NA"
    ]
    graphics [
      x 1466.0985761581244
      y 374.44192627139773
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ncbigene:424185"
      hgnc "NA"
      map_id "W21_35"
      name "MDA5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b4a31"
      uniprot "NA"
    ]
    graphics [
      x 1331.078647692997
      y 226.50421929888364
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ncbiprotein:YP_009742611"
      hgnc "NA"
      map_id "W21_117"
      name "NSP4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ee721"
      uniprot "NA"
    ]
    graphics [
      x 1837.79959397396
      y 1331.8772760803076
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_119"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "f012f"
      uniprot "NA"
    ]
    graphics [
      x 1792.1169763857113
      y 1196.96001084926
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000121060"
      hgnc "NA"
      map_id "W21_112"
      name "TRIM25"
      node_subtype "GENE"
      node_type "species"
      org_id "eb329"
      uniprot "NA"
    ]
    graphics [
      x 1725.3441037346943
      y 1009.4720105674622
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:wikidata:Q106020256"
      hgnc "NA"
      map_id "W21_53"
      name "S1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bcd14"
      uniprot "NA"
    ]
    graphics [
      x 1448.518191007284
      y 1575.876712195775
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_38"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "b66f3"
      uniprot "NA"
    ]
    graphics [
      x 1346.635273929207
      y 1654.7213287086633
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:P35613"
      hgnc "NA"
      map_id "W21_15"
      name "BSG"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "aa59a"
      uniprot "UNIPROT:P35613"
    ]
    graphics [
      x 1250.1485493263226
      y 1590.2265606215233
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:Q87917582"
      hgnc "NA"
      map_id "W21_17"
      name "NSP5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "abe5a"
      uniprot "UNIPROT:Q87917582"
    ]
    graphics [
      x 1099.6846850137244
      y 1729.4405139833946
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306;PUBMED:32353859"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_128"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "f6131"
      uniprot "NA"
    ]
    graphics [
      x 1142.524372170044
      y 1614.212759947118
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000196591"
      hgnc "NA"
      map_id "W21_151"
      name "HDAC2"
      node_subtype "GENE"
      node_type "species"
      org_id "fdaa0"
      uniprot "NA"
    ]
    graphics [
      x 1231.1901788241455
      y 1480.675781529685
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_151"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:pfam:PF00143"
      hgnc "NA"
      map_id "W21_47"
      name "IFN_minus_I"
      node_subtype "GENE"
      node_type "species"
      org_id "b9e39"
      uniprot "NA"
    ]
    graphics [
      x 396.55682000569186
      y 1120.3777072593618
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "PUBMED:17490702;PUBMED:33335518"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_157"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idde2be3e1"
      uniprot "NA"
    ]
    graphics [
      x 268.5616583668284
      y 1251.7922172063504
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_157"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:P0DTC9"
      hgnc "NA"
      map_id "W21_26"
      name "N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b0f05"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 254.9024316957607
      y 1382.1205402475343
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000109320"
      hgnc "NA"
      map_id "W21_88"
      name "NFKB"
      node_subtype "GENE"
      node_type "species"
      org_id "dcaa7"
      uniprot "NA"
    ]
    graphics [
      x 400.45196082437576
      y 1257.8988105576047
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:pfam:PF00143"
      hgnc "NA"
      map_id "W21_44"
      name "IFN_minus_I"
      node_subtype "GENE"
      node_type "species"
      org_id "b8a45"
      uniprot "NA"
    ]
    graphics [
      x 304.0247088598509
      y 1122.2102768597729
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000137203"
      hgnc "NA"
      map_id "W21_141"
      name "TFAP2A"
      node_subtype "GENE"
      node_type "species"
      org_id "fb0d0"
      uniprot "NA"
    ]
    graphics [
      x 1836.2796640320923
      y 859.1848430882038
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_141"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_135"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "f8c93"
      uniprot "NA"
    ]
    graphics [
      x 1664.9007814357115
      y 997.3406770727165
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_136"
      name "Regulation_space_of_space_MHC_space_II_space__br_expression_space_and_space_cytokine_space_production"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "f8f8f"
      uniprot "NA"
    ]
    graphics [
      x 1502.6627582981573
      y 1153.3172930977373
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000180871"
      hgnc "NA"
      map_id "W21_45"
      name "CXCR2"
      node_subtype "GENE"
      node_type "species"
      org_id "b9244"
      uniprot "NA"
    ]
    graphics [
      x 1338.37229071706
      y 1250.9383016666068
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_94"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "e021e"
      uniprot "NA"
    ]
    graphics [
      x 1445.0442957945352
      y 1443.4194743560056
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_9"
      name "NETosis"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "a5b14"
      uniprot "NA"
    ]
    graphics [
      x 1561.7232696932342
      y 1618.3024865847913
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:wikidata:Q22327223"
      hgnc "NA"
      map_id "W21_123"
      name "IgA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f3c02"
      uniprot "NA"
    ]
    graphics [
      x 1623.9311986069654
      y 1396.8993547954497
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "PUBMED:33391280"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_63"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "c830f"
      uniprot "NA"
    ]
    graphics [
      x 1534.247386126041
      y 1484.4365125320867
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_95"
      name "Cytotoxic_space_Function"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "e0973"
      uniprot "NA"
    ]
    graphics [
      x 1942.1887291023545
      y 433.95632905394405
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_67"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "cb2d0"
      uniprot "NA"
    ]
    graphics [
      x 1807.100824010232
      y 303.79493285544015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000117281"
      hgnc "NA"
      map_id "W21_106"
      name "CD16"
      node_subtype "GENE"
      node_type "species"
      org_id "e8ca6"
      uniprot "NA"
    ]
    graphics [
      x 1674.7319140013924
      y 299.83381120992135
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_73"
      name "NA"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "cef7e"
      uniprot "NA"
    ]
    graphics [
      x 1474.5756032250788
      y 2032.6975676502655
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306;PUBMED:33335518"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_91"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "de9d0"
      uniprot "NA"
    ]
    graphics [
      x 1404.8899824677765
      y 2126.0984790153148
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:P01375;urn:miriam:uniprot:P22301;urn:miriam:uniprot:B4DNV3;urn:miriam:ensembl:ENSG00000271503;urn:miriam:uniprot:P04141"
      hgnc "NA"
      map_id "W21_54"
      name "bd84b"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "bd84b"
      uniprot "UNIPROT:P01375;UNIPROT:P22301;UNIPROT:B4DNV3;UNIPROT:P04141"
    ]
    graphics [
      x 1347.279870112524
      y 2021.6333388910496
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000183735"
      hgnc "NA"
      map_id "W21_142"
      name "TBK1"
      node_subtype "GENE"
      node_type "species"
      org_id "fbafc"
      uniprot "NA"
    ]
    graphics [
      x 439.69562402489305
      y 728.497025344096
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_142"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_144"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "fc186"
      uniprot "NA"
    ]
    graphics [
      x 317.7430751054321
      y 705.682549641292
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_144"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000126456"
      hgnc "NA"
      map_id "W21_86"
      name "IRF3"
      node_subtype "GENE"
      node_type "species"
      org_id "db31f"
      uniprot "NA"
    ]
    graphics [
      x 415.58986201626647
      y 628.125429558864
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306;PUBMED:33335518"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_121"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "f12ae"
      uniprot "NA"
    ]
    graphics [
      x 1360.3535501399456
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:P0DTD2"
      hgnc "NA"
      map_id "W21_124"
      name "ORF9b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f4882"
      uniprot "UNIPROT:P0DTD2"
    ]
    graphics [
      x 1483.6711872717644
      y 112.6359968251196
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_124"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000088888"
      hgnc "NA"
      map_id "W21_24"
      name "MAVS"
      node_subtype "GENE"
      node_type "species"
      org_id "ae650"
      uniprot "NA"
    ]
    graphics [
      x 1414.988528106384
      y 151.51255998023396
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000105329"
      hgnc "NA"
      map_id "W21_120"
      name "TGFB1"
      node_subtype "GENE"
      node_type "species"
      org_id "f062c"
      uniprot "NA"
    ]
    graphics [
      x 1413.4979575871014
      y 1261.6439367763865
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "PUBMED:33391280"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_52"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "bcb8e"
      uniprot "NA"
    ]
    graphics [
      x 1546.0906035482894
      y 1310.7921841996272
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:P0DTC9"
      hgnc "NA"
      map_id "W21_41"
      name "N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b6d08"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 415.8319901049107
      y 1564.0334553825935
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_69"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "cc670"
      uniprot "NA"
    ]
    graphics [
      x 298.20228891676186
      y 1628.445304327155
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000155506"
      hgnc "NA"
      map_id "W21_43"
      name "LARP1"
      node_subtype "GENE"
      node_type "species"
      org_id "b7808"
      uniprot "NA"
    ]
    graphics [
      x 335.29230003023724
      y 1508.8836045438807
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_150"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "fd934"
      uniprot "NA"
    ]
    graphics [
      x 546.2382713413821
      y 662.5738445523684
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_150"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000126456"
      hgnc "NA"
      map_id "W21_98"
      name "IRF3"
      node_subtype "GENE"
      node_type "species"
      org_id "e366a"
      uniprot "NA"
    ]
    graphics [
      x 665.7803568036262
      y 696.4873539760079
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_68"
      name "dsRNA"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "cb56c"
      uniprot "NA"
    ]
    graphics [
      x 1110.172773025268
      y 452.60387764063614
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "PUBMED:26579391;PUBMED:33335518"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_100"
      name "NA"
      node_subtype "UNKNOWN_POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "e4ea3"
      uniprot "NA"
    ]
    graphics [
      x 1270.7923913426096
      y 511.73378659632476
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000107201"
      hgnc "NA"
      map_id "W21_6"
      name "DDX58"
      node_subtype "GENE"
      node_type "species"
      org_id "a397d"
      uniprot "NA"
    ]
    graphics [
      x 1410.3986239416813
      y 501.3336812039547
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_7"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "a4481"
      uniprot "NA"
    ]
    graphics [
      x 1985.2817124789262
      y 622.2108902830131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:A0N0L5"
      hgnc "NA"
      map_id "W21_12"
      name "IL_minus_6R"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a8797"
      uniprot "UNIPROT:A0N0L5"
    ]
    graphics [
      x 2046.8788947826326
      y 863.8787642530561
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000170581;urn:miriam:ensembl:ENSG00000115415"
      hgnc "NA"
      map_id "W21_75"
      name "cfa40"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "cfa40"
      uniprot "NA"
    ]
    graphics [
      x 1082.7006805916315
      y 1175.9708599381142
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      annotation "PUBMED:33335518"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_82"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "d6494"
      uniprot "NA"
    ]
    graphics [
      x 945.9031476076038
      y 1228.4976473320573
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:Q89226299;urn:miriam:uniprot:YP_009825053"
      hgnc "NA"
      map_id "W21_111"
      name "ea56d"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ea56d"
      uniprot "UNIPROT:Q89226299;UNIPROT:YP_009825053"
    ]
    graphics [
      x 914.8412247329197
      y 1110.5536593648928
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_19"
      name "ISGs"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "ad145"
      uniprot "NA"
    ]
    graphics [
      x 785.5555119855708
      y 1392.7994897162353
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000163737;urn:miriam:ncbigene:2920;urn:miriam:ensembl:ENSG00000169429;urn:miriam:ensembl:ENSG00000169248;urn:miriam:ensembl:ENSG00000163736;urn:miriam:ensembl:ENSG00000156234;urn:miriam:ensembl:ENSG00000275302;urn:miriam:ensembl:ENSG00000108691;urn:miriam:ensembl:ENSG00000107562;urn:miriam:ensembl:ENSG00000169245;urn:miriam:ensembl:ENSG00000163735;urn:miriam:ensembl:ENSG00000163734;urn:miriam:ncbigene:6372;urn:miriam:ensembl:ENSG00000163739;urn:miriam:ensembl:ENSG00000277632;urn:miriam:ensembl:ENSG00000138755"
      hgnc "NA"
      map_id "W21_147"
      name "fcec7"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "fcec7"
      uniprot "NA"
    ]
    graphics [
      x 1977.124589518875
      y 762.1518095927983
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_147"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_131"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "f692c"
      uniprot "NA"
    ]
    graphics [
      x 1856.703967151645
      y 700.7175880698006
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_131"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_101"
      name "Neutrophil_space_recruitment"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "e62ae"
      uniprot "NA"
    ]
    graphics [
      x 1716.029468302194
      y 719.0153814451644
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_76"
      name "NA"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "cfc78"
      uniprot "NA"
    ]
    graphics [
      x 2133.0376147577845
      y 795.2115208241584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "PUBMED:32416070;PUBMED:33335518"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_59"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "c290b"
      uniprot "NA"
    ]
    graphics [
      x 2117.11332916372
      y 700.8405687119832
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_5"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "a3844"
      uniprot "NA"
    ]
    graphics [
      x 1554.0730131663859
      y 846.1686522109193
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_21"
      name "Antiproliferative_space_activity"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "adf72"
      uniprot "NA"
    ]
    graphics [
      x 1509.6142571986813
      y 739.6532550546881
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000185507"
      hgnc "NA"
      map_id "W21_22"
      name "IRF7"
      node_subtype "GENE"
      node_type "species"
      org_id "ae0a7"
      uniprot "NA"
    ]
    graphics [
      x 740.1020309092338
      y 1062.2132067054413
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306;PUBMED:32529952"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_126"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "f5bcb"
      uniprot "NA"
    ]
    graphics [
      x 589.4726746457412
      y 903.1124547758941
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:P0DTC3;urn:miriam:uniprot:6ZSL;urn:miriam:uniprot:P0DTC5;urn:miriam:uniprot:P0DTD2;urn:miriam:uniprot:P0DTC9;urn:miriam:uniprot:Q87917579"
      hgnc "NA"
      map_id "W21_80"
      name "d3cf8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d3cf8"
      uniprot "UNIPROT:P0DTC3;UNIPROT:6ZSL;UNIPROT:P0DTC5;UNIPROT:P0DTD2;UNIPROT:P0DTC9;UNIPROT:Q87917579"
    ]
    graphics [
      x 702.453709674661
      y 797.6250532419267
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:B3KNW1"
      hgnc "NA"
      map_id "W21_153"
      name "AP_minus_1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "fdd5c"
      uniprot "UNIPROT:B3KNW1"
    ]
    graphics [
      x 629.7243331492402
      y 477.5025551747995
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_153"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_37"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "b5f9c"
      uniprot "NA"
    ]
    graphics [
      x 731.3754032539105
      y 405.5047231874622
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:B3KNW1"
      hgnc "NA"
      map_id "W21_4"
      name "AP_minus_1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a262e"
      uniprot "UNIPROT:B3KNW1"
    ]
    graphics [
      x 846.9494093343776
      y 367.64443255751235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000110713;urn:miriam:ensembl:ENSG00000101146"
      hgnc "NA"
      map_id "W21_118"
      name "ef9ac"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ef9ac"
      uniprot "NA"
    ]
    graphics [
      x 541.0754085855772
      y 183.6014574907249
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859;PUBMED:32529952"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_16"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "ab09f"
      uniprot "NA"
    ]
    graphics [
      x 564.9444231956685
      y 70.41285795567728
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:Q89226299"
      hgnc "NA"
      map_id "W21_62"
      name "ORF6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c6ec9"
      uniprot "UNIPROT:Q89226299"
    ]
    graphics [
      x 664.4090375733526
      y 131.4410616513086
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000162434"
      hgnc "NA"
      map_id "W21_93"
      name "e01fa"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e01fa"
      uniprot "NA"
    ]
    graphics [
      x 1382.153164456071
      y 1065.6855606440554
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_2"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "a0568"
      uniprot "NA"
    ]
    graphics [
      x 1223.4148783099288
      y 1117.6886306197327
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_129"
      name "Viral_space_particle"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "f62f1"
      uniprot "NA"
    ]
    graphics [
      x 1015.2253868218693
      y 900.8311335077707
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_129"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_20"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "adadb"
      uniprot "NA"
    ]
    graphics [
      x 935.7867739726395
      y 831.1769374772856
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000130234"
      hgnc "NA"
      map_id "W21_14"
      name "ACE2"
      node_subtype "GENE"
      node_type "species"
      org_id "a9be1"
      uniprot "NA"
    ]
    graphics [
      x 894.9502082033863
      y 930.1542735083931
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000159110;urn:miriam:ensembl:ENSG00000142166"
      hgnc "NA"
      map_id "W21_105"
      name "e8804"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e8804"
      uniprot "NA"
    ]
    graphics [
      x 1390.059477200577
      y 938.205322428501
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      annotation "PUBMED:33335518"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_8"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "a4b7d"
      uniprot "NA"
    ]
    graphics [
      x 1502.5571353215116
      y 1014.2776103018094
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306;PUBMED:33335518"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_57"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "c18f7"
      uniprot "NA"
    ]
    graphics [
      x 1454.2536443294707
      y 272.53501164625595
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000166949"
      hgnc "NA"
      map_id "W21_137"
      name "SMAD3"
      node_subtype "GENE"
      node_type "species"
      org_id "f99c6"
      uniprot "NA"
    ]
    graphics [
      x 2378.50849927414
      y 895.0955674750364
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_137"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_66"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "c94fa"
      uniprot "NA"
    ]
    graphics [
      x 2456.503084875303
      y 978.9467782576663
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000166949"
      hgnc "NA"
      map_id "W21_42"
      name "SMAD3"
      node_subtype "GENE"
      node_type "species"
      org_id "b7200"
      uniprot "NA"
    ]
    graphics [
      x 2349.5315251599914
      y 1024.0350291731406
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ncbiprotein:AAL65234"
      hgnc "NA"
      map_id "W21_130"
      name "NKG2A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f6900"
      uniprot "NA"
    ]
    graphics [
      x 714.5840962900525
      y 286.47054232106314
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_130"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_99"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "e3a24"
      uniprot "NA"
    ]
    graphics [
      x 773.8876917881857
      y 179.20014246533083
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ncbigene:84868;urn:miriam:ncbigene:3902"
      hgnc "NA"
      map_id "W21_40"
      name "b6cf9"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b6cf9"
      uniprot "NA"
    ]
    graphics [
      x 827.2988626604852
      y 81.55384003307336
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_28"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "b2c03"
      uniprot "NA"
    ]
    graphics [
      x 1856.4343661153794
      y 355.6654005099713
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000104970"
      hgnc "NA"
      map_id "W21_25"
      name "KIR"
      node_subtype "GENE"
      node_type "species"
      org_id "aea75"
      uniprot "NA"
    ]
    graphics [
      x 1774.6798547622454
      y 414.7660769595466
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:P0DTC3"
      hgnc "NA"
      map_id "W21_115"
      name "ORF3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ed8aa"
      uniprot "UNIPROT:P0DTC3"
    ]
    graphics [
      x 193.4249314998027
      y 987.1157396812816
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      annotation "PUBMED:32555321"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_83"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "d6cd5"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 1019.6325709203762
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:Q14790"
      hgnc "NA"
      map_id "W21_50"
      name "CASP8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bc51a"
      uniprot "UNIPROT:Q14790"
    ]
    graphics [
      x 115.74600326532209
      y 1131.1503207847998
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:YP_009742617"
      hgnc "NA"
      map_id "W21_155"
      name "NSP10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "fdf94"
      uniprot "UNIPROT:YP_009742617"
    ]
    graphics [
      x 2065.863618473466
      y 620.3274255266028
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_155"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_113"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "eb412"
      uniprot "NA"
    ]
    graphics [
      x 1959.2326802490795
      y 707.1382783898543
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_85"
      name "DMV"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "d806a"
      uniprot "NA"
    ]
    graphics [
      x 931.983053134175
      y 716.3508678367139
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306;PUBMED:33335518"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_127"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "f5be7"
      uniprot "NA"
    ]
    graphics [
      x 1000.9954080597167
      y 597.007299823282
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_127"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000171855"
      hgnc "NA"
      map_id "W21_89"
      name "IFNB1"
      node_subtype "GENE"
      node_type "species"
      org_id "dcfff"
      uniprot "NA"
    ]
    graphics [
      x 1355.8940923699615
      y 738.5751713410946
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      annotation "PUBMED:33335518"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_29"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "b2c4d"
      uniprot "NA"
    ]
    graphics [
      x 1307.4264957232174
      y 839.1182574579686
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:P0DTC9"
      hgnc "NA"
      map_id "W21_46"
      name "N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b9df2"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 1193.6750482192385
      y 806.1374638320399
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000189377;urn:miriam:ensembl:ENSG00000169429;urn:miriam:ensembl:ENSG00000081041"
      hgnc "NA"
      map_id "W21_77"
      name "d00c2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d00c2"
      uniprot "NA"
    ]
    graphics [
      x 1208.9703365381145
      y 887.3009567320589
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_96"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "e0d71"
      uniprot "NA"
    ]
    graphics [
      x 1269.220847011169
      y 1054.4065830250177
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000109320"
      hgnc "NA"
      map_id "W21_145"
      name "NFKB"
      node_subtype "GENE"
      node_type "species"
      org_id "fc824"
      uniprot "NA"
    ]
    graphics [
      x 646.3963432976941
      y 861.5228083716509
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_145"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_152"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "fdaed"
      uniprot "NA"
    ]
    graphics [
      x 512.8368738267544
      y 1049.3888985202943
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_152"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_32"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "b3aaf"
      uniprot "NA"
    ]
    graphics [
      x 473.68500580925297
      y 1450.4414681072683
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_114"
      name "Protein_space_synthesis"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "ec9ad"
      uniprot "NA"
    ]
    graphics [
      x 620.0118572889653
      y 1420.6297303930123
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ncbigene:8519"
      hgnc "NA"
      map_id "W21_13"
      name "IFITM1"
      node_subtype "GENE"
      node_type "species"
      org_id "a93c7"
      uniprot "NA"
    ]
    graphics [
      x 597.4516941047567
      y 1238.588364246966
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      annotation "PUBMED:33335518"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_30"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "b2f3d"
      uniprot "NA"
    ]
    graphics [
      x 654.2778718925226
      y 1345.1097836114864
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ncbigene:8519"
      hgnc "NA"
      map_id "W21_109"
      name "IFITM1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e997b"
      uniprot "NA"
    ]
    graphics [
      x 539.99050273768
      y 1347.4603020986328
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:B4DNV3"
      hgnc "NA"
      map_id "W21_146"
      name "IL_minus_6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "fcb4b"
      uniprot "UNIPROT:B4DNV3"
    ]
    graphics [
      x 2092.648450575766
      y 1295.788557575449
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_146"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_103"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "e782a"
      uniprot "NA"
    ]
    graphics [
      x 2083.5011882565695
      y 1080.3598289614665
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_84"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "d7cd0"
      uniprot "NA"
    ]
    graphics [
      x 525.6480385552672
      y 573.838530929008
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      annotation "PUBMED:33335518"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_108"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "e97bb"
      uniprot "NA"
    ]
    graphics [
      x 1183.8497256874969
      y 285.89097235256645
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000108771"
      hgnc "NA"
      map_id "W21_156"
      name "LGP2"
      node_subtype "GENE"
      node_type "species"
      org_id "ff178"
      uniprot "NA"
    ]
    graphics [
      x 1134.003978775638
      y 176.97727087753515
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_156"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_33"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "b47a6"
      uniprot "NA"
    ]
    graphics [
      x 1587.6090111758188
      y 762.7804097360026
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000137275;urn:miriam:ensembl:ENSG00000168040"
      hgnc "NA"
      map_id "W21_55"
      name "bf912"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "bf912"
      uniprot "NA"
    ]
    graphics [
      x 996.5126516305445
      y 536.3517676596963
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_36"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "b4cf0"
      uniprot "NA"
    ]
    graphics [
      x 796.6129369262956
      y 691.6930685245349
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:A0N0L5"
      hgnc "NA"
      map_id "W21_139"
      name "IL_minus_6R"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "fa884"
      uniprot "UNIPROT:A0N0L5"
    ]
    graphics [
      x 1916.7091062316413
      y 1593.127125953557
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_139"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_97"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "e32a1"
      uniprot "NA"
    ]
    graphics [
      x 1749.888132310697
      y 1656.5186730851892
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      annotation "PUBMED:33372174;PUBMED:33101306"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_48"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "ba78e"
      uniprot "NA"
    ]
    graphics [
      x 1472.5326451115745
      y 600.2575628871584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_143"
      name "Viral_space_particle"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "fbdf9"
      uniprot "NA"
    ]
    graphics [
      x 1229.3696831740897
      y 1350.755748185713
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_143"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_154"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "fdd8b"
      uniprot "NA"
    ]
    graphics [
      x 1326.9888237301616
      y 1475.5514986720818
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_154"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_92"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "df020"
      uniprot "NA"
    ]
    graphics [
      x 1438.6221097772093
      y 832.010407641054
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000166949;urn:miriam:ensembl:ENSG00000100393"
      hgnc "NA"
      map_id "W21_61"
      name "c69be"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c69be"
      uniprot "NA"
    ]
    graphics [
      x 2016.7871430952396
      y 211.07986118503732
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_149"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "fd17e"
      uniprot "NA"
    ]
    graphics [
      x 2049.935305185664
      y 92.5525835920331
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_149"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:wikipathways:WP3624"
      hgnc "NA"
      map_id "W21_79"
      name "Lung_space_fibrosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "d2969"
      uniprot "NA"
    ]
    graphics [
      x 1934.1698110380235
      y 124.85426669348522
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      annotation "PUBMED:32416070;PUBMED:31986264"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_58"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "c1910"
      uniprot "NA"
    ]
    graphics [
      x 2034.5620734972536
      y 1465.155872041304
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_90"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "de802"
      uniprot "NA"
    ]
    graphics [
      x 266.311183184818
      y 1161.8666587157672
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:wikipathways:WP254"
      hgnc "NA"
      map_id "W21_11"
      name "Apoptosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "a6ff9"
      uniprot "NA"
    ]
    graphics [
      x 413.2760643193776
      y 1174.3716748478498
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ncbigene:4599"
      hgnc "NA"
      map_id "W21_102"
      name "MX1"
      node_subtype "GENE"
      node_type "species"
      org_id "e7804"
      uniprot "NA"
    ]
    graphics [
      x 726.3664871769727
      y 1659.3902873303937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      annotation "PUBMED:33335518"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_104"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "e849d"
      uniprot "NA"
    ]
    graphics [
      x 756.4177533283776
      y 1549.3517723445443
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ncbigene:4599"
      hgnc "NA"
      map_id "W21_39"
      name "MX1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b67c2"
      uniprot "NA"
    ]
    graphics [
      x 863.7068459022232
      y 1570.032786099246
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:wikidata:Q94647436"
      hgnc "NA"
      map_id "W21_74"
      name "NSP12"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "cf31a"
      uniprot "NA"
    ]
    graphics [
      x 1749.8395606676165
      y 743.9070697380173
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_125"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "f5211"
      uniprot "NA"
    ]
    graphics [
      x 1628.240876209334
      y 640.4032339886049
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_125"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000137275"
      hgnc "NA"
      map_id "W21_134"
      name "RIPK1"
      node_subtype "GENE"
      node_type "species"
      org_id "f7e89"
      uniprot "NA"
    ]
    graphics [
      x 1455.1670526889236
      y 530.2115202502332
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:pfam:PF15177"
      hgnc "NA"
      map_id "W21_23"
      name "IFN_minus_III"
      node_subtype "GENE"
      node_type "species"
      org_id "ae4ce"
      uniprot "NA"
    ]
    graphics [
      x 486.42112417365047
      y 1520.9407730379544
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      annotation "PUBMED:17490702;PUBMED:33335518"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_3"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "a0f18"
      uniprot "NA"
    ]
    graphics [
      x 385.95447791131926
      y 1432.2663876424049
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:pfam:PF15177"
      hgnc "NA"
      map_id "W21_31"
      name "IFN_minus_III"
      node_subtype "GENE"
      node_type "species"
      org_id "b3038"
      uniprot "NA"
    ]
    graphics [
      x 444.51275165272136
      y 1596.1924629601751
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_64"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "c8785"
      uniprot "NA"
    ]
    graphics [
      x 870.5667436935266
      y 1220.2336364067812
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000185507"
      hgnc "NA"
      map_id "W21_81"
      name "IRF7"
      node_subtype "GENE"
      node_type "species"
      org_id "d524f"
      uniprot "NA"
    ]
    graphics [
      x 976.5628756331588
      y 1316.8147065710887
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306;PUBMED:19430490;PUBMED:33335518"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_27"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "b21db"
      uniprot "NA"
    ]
    graphics [
      x 1618.8454719594781
      y 1513.6482407623332
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000130234"
      hgnc "NA"
      map_id "W21_122"
      name "ACE2"
      node_subtype "GENE"
      node_type "species"
      org_id "f1b6b"
      uniprot "NA"
    ]
    graphics [
      x 1718.3647052948857
      y 1435.797718527378
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_51"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "bc934"
      uniprot "NA"
    ]
    graphics [
      x 2215.8649128744382
      y 1339.0240125569262
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:A0N0L5"
      hgnc "NA"
      map_id "W21_133"
      name "IL_minus_6R"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f7e47"
      uniprot "UNIPROT:A0N0L5"
    ]
    graphics [
      x 2159.3996736620034
      y 1237.411809367519
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_133"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_10"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "a69de"
      uniprot "NA"
    ]
    graphics [
      x 318.10553256442495
      y 587.8289901137541
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_148"
      name "NA"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "fd08c"
      uniprot "NA"
    ]
    graphics [
      x 1160.1930457022756
      y 577.4315099826879
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_148"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_71"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ce121"
      uniprot "NA"
    ]
    graphics [
      x 1168.485766581589
      y 719.3059320897614
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_78"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "d1890"
      uniprot "NA"
    ]
    graphics [
      x 1217.7264597502526
      y 479.2847591155764
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:wikidata:Q22327229"
      hgnc "NA"
      map_id "W21_107"
      name "IgM"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e94ab"
      uniprot "NA"
    ]
    graphics [
      x 1561.3008119608842
      y 1725.1717213441132
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      annotation "PUBMED:33391280"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_65"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "c8a5d"
      uniprot "NA"
    ]
    graphics [
      x 1657.5716762209677
      y 1673.5164819273755
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_60"
      name "RBD_br_"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "c59c3"
      uniprot "NA"
    ]
    graphics [
      x 1599.3343966733423
      y 1573.5499610316124
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ncbigene:3433"
      hgnc "NA"
      map_id "W21_140"
      name "IFIT2"
      node_subtype "GENE"
      node_type "species"
      org_id "facda"
      uniprot "NA"
    ]
    graphics [
      x 828.2825019912239
      y 1334.4574906407838
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_140"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      annotation "PUBMED:33335518"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_70"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ccae3"
      uniprot "NA"
    ]
    graphics [
      x 728.8361152265473
      y 1283.5116500856163
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ncbigene:3433"
      hgnc "NA"
      map_id "W21_138"
      name "IFIT2"
      node_subtype "GENE"
      node_type "species"
      org_id "f9fe4"
      uniprot "NA"
    ]
    graphics [
      x 782.7174074197644
      y 1188.6767790198373
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_138"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      annotation "PUBMED:33391280"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_56"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "bfa83"
      uniprot "NA"
    ]
    graphics [
      x 1250.2843348759957
      y 1215.5323174053988
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_1"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "a0131"
      uniprot "NA"
    ]
    graphics [
      x 1368.5191693648205
      y 1340.1778682743575
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 158
    source 1
    target 2
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_18"
      target_id "W21_132"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 159
    source 2
    target 3
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_132"
      target_id "W21_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 160
    source 4
    target 5
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_110"
      target_id "W21_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 161
    source 5
    target 6
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_49"
      target_id "W21_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 162
    source 7
    target 8
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_72"
      target_id "W21_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 163
    source 8
    target 9
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_87"
      target_id "W21_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 164
    source 10
    target 11
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_117"
      target_id "W21_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 165
    source 11
    target 12
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_119"
      target_id "W21_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 166
    source 13
    target 14
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_53"
      target_id "W21_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 167
    source 14
    target 15
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_38"
      target_id "W21_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 168
    source 16
    target 17
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_17"
      target_id "W21_128"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 169
    source 17
    target 18
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_128"
      target_id "W21_151"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 170
    source 19
    target 20
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_47"
      target_id "W21_157"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 171
    source 21
    target 20
    cd19dm [
      diagram "WP5039"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W21_26"
      target_id "W21_157"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 172
    source 22
    target 20
    cd19dm [
      diagram "WP5039"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W21_88"
      target_id "W21_157"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 173
    source 20
    target 23
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_157"
      target_id "W21_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 174
    source 24
    target 25
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_141"
      target_id "W21_135"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 175
    source 25
    target 26
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_135"
      target_id "W21_136"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 176
    source 27
    target 28
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_45"
      target_id "W21_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 177
    source 28
    target 29
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_94"
      target_id "W21_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 178
    source 30
    target 31
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_123"
      target_id "W21_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 179
    source 31
    target 13
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_63"
      target_id "W21_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 180
    source 32
    target 33
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_95"
      target_id "W21_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 181
    source 33
    target 34
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_67"
      target_id "W21_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 182
    source 35
    target 36
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_73"
      target_id "W21_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 36
    target 37
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_91"
      target_id "W21_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 38
    target 39
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_142"
      target_id "W21_144"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 39
    target 40
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_144"
      target_id "W21_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 9
    target 41
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_35"
      target_id "W21_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 42
    target 41
    cd19dm [
      diagram "WP5039"
      edge_type "INHIBITION"
      source_id "W21_124"
      target_id "W21_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 41
    target 43
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_121"
      target_id "W21_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 44
    target 45
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_120"
      target_id "W21_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 45
    target 30
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_52"
      target_id "W21_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 46
    target 47
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_41"
      target_id "W21_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 47
    target 48
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_69"
      target_id "W21_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 40
    target 49
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_86"
      target_id "W21_150"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 49
    target 50
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_150"
      target_id "W21_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 51
    target 52
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_68"
      target_id "W21_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 52
    target 53
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_100"
      target_id "W21_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 32
    target 54
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_95"
      target_id "W21_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 54
    target 55
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_7"
      target_id "W21_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 56
    target 57
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_75"
      target_id "W21_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 58
    target 57
    cd19dm [
      diagram "WP5039"
      edge_type "INHIBITION"
      source_id "W21_111"
      target_id "W21_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 57
    target 59
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_82"
      target_id "W21_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 60
    target 61
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_147"
      target_id "W21_131"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 61
    target 62
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_131"
      target_id "W21_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 63
    target 64
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_76"
      target_id "W21_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 64
    target 60
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_59"
      target_id "W21_147"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 3
    target 65
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_34"
      target_id "W21_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 65
    target 66
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_5"
      target_id "W21_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 67
    target 68
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_22"
      target_id "W21_126"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 69
    target 68
    cd19dm [
      diagram "WP5039"
      edge_type "INHIBITION"
      source_id "W21_80"
      target_id "W21_126"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 68
    target 38
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_126"
      target_id "W21_142"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 70
    target 71
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_153"
      target_id "W21_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 71
    target 72
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_37"
      target_id "W21_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 73
    target 74
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_118"
      target_id "W21_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 74
    target 75
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_16"
      target_id "W21_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 76
    target 77
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_93"
      target_id "W21_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 77
    target 56
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_2"
      target_id "W21_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 78
    target 79
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_129"
      target_id "W21_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 79
    target 80
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_20"
      target_id "W21_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 81
    target 82
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_105"
      target_id "W21_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 82
    target 76
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_8"
      target_id "W21_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 53
    target 83
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_6"
      target_id "W21_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 42
    target 83
    cd19dm [
      diagram "WP5039"
      edge_type "INHIBITION"
      source_id "W21_124"
      target_id "W21_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 83
    target 43
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_57"
      target_id "W21_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 84
    target 85
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_137"
      target_id "W21_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 85
    target 86
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_66"
      target_id "W21_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 87
    target 88
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_130"
      target_id "W21_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 88
    target 89
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_99"
      target_id "W21_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 32
    target 90
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_95"
      target_id "W21_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 90
    target 91
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_28"
      target_id "W21_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 92
    target 93
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_115"
      target_id "W21_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 93
    target 94
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_83"
      target_id "W21_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 95
    target 96
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_155"
      target_id "W21_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 96
    target 24
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_113"
      target_id "W21_141"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 97
    target 98
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_85"
      target_id "W21_127"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 98
    target 51
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_127"
      target_id "W21_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 99
    target 100
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_89"
      target_id "W21_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 101
    target 100
    cd19dm [
      diagram "WP5039"
      edge_type "INHIBITION"
      source_id "W21_46"
      target_id "W21_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 100
    target 81
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_29"
      target_id "W21_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 239
    source 102
    target 103
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_77"
      target_id "W21_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 240
    source 103
    target 27
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_96"
      target_id "W21_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 241
    source 104
    target 105
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_145"
      target_id "W21_152"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 242
    source 105
    target 22
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_152"
      target_id "W21_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 243
    source 48
    target 106
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_43"
      target_id "W21_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 244
    source 106
    target 107
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_32"
      target_id "W21_114"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 245
    source 108
    target 109
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_13"
      target_id "W21_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 246
    source 59
    target 109
    cd19dm [
      diagram "WP5039"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W21_19"
      target_id "W21_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 247
    source 109
    target 110
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_30"
      target_id "W21_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 248
    source 111
    target 112
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_146"
      target_id "W21_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 249
    source 112
    target 55
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_103"
      target_id "W21_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 250
    source 38
    target 113
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_142"
      target_id "W21_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 251
    source 113
    target 70
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_84"
      target_id "W21_153"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 252
    source 51
    target 114
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_68"
      target_id "W21_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 253
    source 115
    target 114
    cd19dm [
      diagram "WP5039"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W21_156"
      target_id "W21_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 254
    source 114
    target 9
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_108"
      target_id "W21_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 255
    source 12
    target 116
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_112"
      target_id "W21_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 256
    source 116
    target 53
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_33"
      target_id "W21_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 257
    source 117
    target 118
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_55"
      target_id "W21_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 258
    source 69
    target 118
    cd19dm [
      diagram "WP5039"
      edge_type "INHIBITION"
      source_id "W21_80"
      target_id "W21_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 259
    source 118
    target 104
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_36"
      target_id "W21_145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 260
    source 119
    target 120
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_139"
      target_id "W21_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 261
    source 120
    target 29
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_97"
      target_id "W21_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 262
    source 7
    target 121
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_72"
      target_id "W21_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 263
    source 121
    target 53
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_48"
      target_id "W21_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 264
    source 122
    target 123
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_143"
      target_id "W21_154"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 265
    source 123
    target 13
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_154"
      target_id "W21_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 266
    source 99
    target 124
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_89"
      target_id "W21_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 267
    source 124
    target 3
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_92"
      target_id "W21_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 268
    source 125
    target 126
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_61"
      target_id "W21_149"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 269
    source 126
    target 127
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_149"
      target_id "W21_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 270
    source 111
    target 128
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_146"
      target_id "W21_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 271
    source 128
    target 119
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_58"
      target_id "W21_139"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 272
    source 94
    target 129
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_50"
      target_id "W21_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 273
    source 129
    target 130
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_90"
      target_id "W21_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 274
    source 131
    target 132
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_102"
      target_id "W21_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 275
    source 59
    target 132
    cd19dm [
      diagram "WP5039"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W21_19"
      target_id "W21_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 276
    source 132
    target 133
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_104"
      target_id "W21_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 277
    source 134
    target 135
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_74"
      target_id "W21_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 278
    source 135
    target 136
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_125"
      target_id "W21_134"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 137
    target 138
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_23"
      target_id "W21_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 21
    target 138
    cd19dm [
      diagram "WP5039"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W21_26"
      target_id "W21_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 22
    target 138
    cd19dm [
      diagram "WP5039"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W21_88"
      target_id "W21_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 138
    target 139
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_3"
      target_id "W21_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 67
    target 140
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_22"
      target_id "W21_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 140
    target 141
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_64"
      target_id "W21_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 13
    target 142
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_53"
      target_id "W21_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 142
    target 143
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_27"
      target_id "W21_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 111
    target 144
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_146"
      target_id "W21_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 144
    target 145
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_51"
      target_id "W21_133"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 6
    target 146
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_116"
      target_id "W21_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 146
    target 38
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_10"
      target_id "W21_142"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 147
    target 148
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_148"
      target_id "W21_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 148
    target 102
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_71"
      target_id "W21_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 136
    target 149
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_134"
      target_id "W21_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 149
    target 117
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_78"
      target_id "W21_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 150
    target 151
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_107"
      target_id "W21_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 151
    target 152
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_65"
      target_id "W21_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 153
    target 154
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_140"
      target_id "W21_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 59
    target 154
    cd19dm [
      diagram "WP5039"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W21_19"
      target_id "W21_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 154
    target 155
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_70"
      target_id "W21_138"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 122
    target 156
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_143"
      target_id "W21_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 156
    target 44
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_56"
      target_id "W21_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 18
    target 157
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_151"
      target_id "W21_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 157
    target 26
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_1"
      target_id "W21_136"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
