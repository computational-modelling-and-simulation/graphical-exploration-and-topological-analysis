# generated with VANTED V2.8.2 at Fri Mar 04 09:56:59 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:uniprot:P59633;urn:miriam:wikidata:Q89458416"
      hgnc "NA"
      map_id "W9_32"
      name "ee4e9"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ee4e9"
      uniprot "UNIPROT:P59633"
    ]
    graphics [
      x 1151.5092750796057
      y 934.6809914650569
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_62"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id81131143"
      uniprot "NA"
    ]
    graphics [
      x 1146.6531285267022
      y 1108.5595622929413
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ncbigene:5595;urn:miriam:ncbigene:5594"
      hgnc "NA"
      map_id "W9_38"
      name "f9084"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f9084"
      uniprot "NA"
    ]
    graphics [
      x 1091.9687579527886
      y 1306.0383957910612
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ncbigene:5599;urn:miriam:ncbigene:5601;urn:miriam:ncbigene:5602"
      hgnc "NA"
      map_id "W9_17"
      name "ca580"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ca580"
      uniprot "NA"
    ]
    graphics [
      x 927.4520825214294
      y 269.0567162989537
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_44"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id2520366d"
      uniprot "NA"
    ]
    graphics [
      x 1030.126012140302
      y 531.6675913472462
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000170345;urn:miriam:ensembl:ENSG00000177606;urn:miriam:ncbigene:3726"
      hgnc "NA"
      map_id "W9_22"
      name "d382f"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d382f"
      uniprot "NA"
    ]
    graphics [
      x 1098.0058997796327
      y 828.4152218895781
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000085511;urn:miriam:ensembl:ENSG00000095015"
      hgnc "NA"
      map_id "W9_16"
      name "c8921"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c8921"
      uniprot "NA"
    ]
    graphics [
      x 661.4179504456479
      y 520.3303701319716
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_63"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id9808cc66"
      uniprot "NA"
    ]
    graphics [
      x 825.3306205704241
      y 512.5328108787232
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000065559"
      hgnc "NA"
      map_id "W9_13"
      name "MAP2K4"
      node_subtype "GENE"
      node_type "species"
      org_id "bed55"
      uniprot "NA"
    ]
    graphics [
      x 785.889159484494
      y 621.6513447246277
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000117676;urn:miriam:ensembl:ENSG00000177189;urn:miriam:ensembl:ENSG00000071242"
      hgnc "NA"
      map_id "W9_24"
      name "dd506"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "dd506"
      uniprot "NA"
    ]
    graphics [
      x 697.57499444043
      y 1384.0139303725236
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_68"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idc1c0f088"
      uniprot "NA"
    ]
    graphics [
      x 617.5501811949011
      y 1455.056800941114
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_23"
      name "Cell_space_survival_space__br_and_space_proliferation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "d8558"
      uniprot "NA"
    ]
    graphics [
      x 655.3256231413401
      y 1313.4201849838983
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_58"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id79ad2aef"
      uniprot "NA"
    ]
    graphics [
      x 653.4663240555149
      y 800.1184023305166
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ncbigene:5600;urn:miriam:ncbigene:6300;urn:miriam:ensembl:ENSG00000112062;urn:miriam:ncbigene:5603"
      hgnc "NA"
      map_id "W9_37"
      name "f53ff"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f53ff"
      uniprot "NA"
    ]
    graphics [
      x 567.676449861814
      y 994.6796494255487
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000185885"
      hgnc "NA"
      map_id "W9_21"
      name "IFITM1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "cf41c"
      uniprot "NA"
    ]
    graphics [
      x 818.1081867824461
      y 850.2589995445281
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_75"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ide8866e40"
      uniprot "NA"
    ]
    graphics [
      x 963.4870579746225
      y 864.4838004156153
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000185885"
      hgnc "NA"
      map_id "W9_9"
      name "IFITM1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b8cbc"
      uniprot "NA"
    ]
    graphics [
      x 856.4162942003388
      y 903.074310200743
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000142089"
      hgnc "NA"
      map_id "W9_36"
      name "IFITM3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f50e2"
      uniprot "NA"
    ]
    graphics [
      x 1071.0765031495296
      y 684.2992011778509
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_6"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ad2dd"
      uniprot "NA"
    ]
    graphics [
      x 1189.6831607537235
      y 731.1744758706218
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000142089"
      hgnc "NA"
      map_id "W9_33"
      name "IFITM3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f1794"
      uniprot "NA"
    ]
    graphics [
      x 1075.6921668312489
      y 763.8347716580548
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000171791"
      hgnc "NA"
      map_id "W9_27"
      name "BCL2"
      node_subtype "GENE"
      node_type "species"
      org_id "e21a3"
      uniprot "NA"
    ]
    graphics [
      x 561.715347068634
      y 174.25917879349458
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_61"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id81035407"
      uniprot "NA"
    ]
    graphics [
      x 456.5846638644766
      y 296.7572404665566
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_12"
      name "Apoptosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "be42e"
      uniprot "NA"
    ]
    graphics [
      x 401.9750534540717
      y 449.99193184842363
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:uniprot:P59635;urn:miriam:uniprot:P59632"
      hgnc "NA"
      map_id "W9_26"
      name "e11b1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e11b1"
      uniprot "UNIPROT:P59635;UNIPROT:P59632"
    ]
    graphics [
      x 204.56279582308957
      y 959.2857621606613
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_70"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idcac13c17"
      uniprot "NA"
    ]
    graphics [
      x 365.1225672139301
      y 987.7617145903887
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_28"
      name "SARS,_space_MERS,_space__br_229E_space_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "e5c80"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 764.3722050166178
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_78"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idfb2829ab"
      uniprot "NA"
    ]
    graphics [
      x 80.76691387678613
      y 891.613475837805
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000034152;urn:miriam:ensembl:ENSG00000108984"
      hgnc "NA"
      map_id "W9_30"
      name "e6b7b"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e6b7b"
      uniprot "NA"
    ]
    graphics [
      x 336.5783215580829
      y 703.3014951158532
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_42"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id198c85f"
      uniprot "NA"
    ]
    graphics [
      x 412.1858337464747
      y 855.8086365169742
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_25"
      name "SARS,_space_229E_br_IBV_space_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "e046c"
      uniprot "NA"
    ]
    graphics [
      x 1027.7466699860095
      y 281.6362746025271
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_76"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "ided0c8de7"
      uniprot "NA"
    ]
    graphics [
      x 889.7707975531977
      y 336.3448177861105
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000076984"
      hgnc "NA"
      map_id "W9_4"
      name "MAP2K7"
      node_subtype "GENE"
      node_type "species"
      org_id "ac39a"
      uniprot "NA"
    ]
    graphics [
      x 754.3503318519414
      y 399.89342142803525
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000185201"
      hgnc "NA"
      map_id "W9_5"
      name "IFITM2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "acf10"
      uniprot "NA"
    ]
    graphics [
      x 1407.6877115887423
      y 755.9957816158898
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ea117"
      uniprot "NA"
    ]
    graphics [
      x 1284.4296965570481
      y 764.5822490130689
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000185201"
      hgnc "NA"
      map_id "W9_35"
      name "IFITM2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f247c"
      uniprot "NA"
    ]
    graphics [
      x 1356.1299647555336
      y 663.2153581350049
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_46"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id352f2389"
      uniprot "NA"
    ]
    graphics [
      x 797.34143871879
      y 279.13256900070775
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_43"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id1b1215f4"
      uniprot "NA"
    ]
    graphics [
      x 181.42123572341598
      y 700.996934638256
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_49"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id44f6de9d"
      uniprot "NA"
    ]
    graphics [
      x 438.66813230550116
      y 1112.1605160040485
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000151247"
      hgnc "NA"
      map_id "W9_20"
      name "EIF4E"
      node_subtype "GENE"
      node_type "species"
      org_id "ce80a"
      uniprot "NA"
    ]
    graphics [
      x 317.74676981183256
      y 1203.0089990431543
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000175197"
      hgnc "NA"
      map_id "W9_1"
      name "DDIT3"
      node_subtype "GENE"
      node_type "species"
      org_id "a158c"
      uniprot "NA"
    ]
    graphics [
      x 462.63233799533134
      y 796.1053787932585
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_73"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "ide06bd151"
      uniprot "NA"
    ]
    graphics [
      x 411.64498275732166
      y 626.0757275022115
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_64"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "ida2a9cdb0"
      uniprot "NA"
    ]
    graphics [
      x 958.1707868040437
      y 1199.4479138739894
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000115966;urn:miriam:ensembl:ENSG00000170345"
      hgnc "NA"
      map_id "W9_10"
      name "b90b1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b90b1"
      uniprot "NA"
    ]
    graphics [
      x 852.1073696048305
      y 1053.8276828447729
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_14"
      name "SARS,_space_MERS,_space_229E_br_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "bf897"
      uniprot "NA"
    ]
    graphics [
      x 1332.1057059272098
      y 1461.368866570816
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_55"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id62eba7d3"
      uniprot "NA"
    ]
    graphics [
      x 1451.9452608380288
      y 1411.7265411355588
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000169032;urn:miriam:ensembl:ENSG00000126934"
      hgnc "NA"
      map_id "W9_29"
      name "e5ca8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e5ca8"
      uniprot "NA"
    ]
    graphics [
      x 1442.9240800855423
      y 1287.4607441321336
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_71"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idda26811d"
      uniprot "NA"
    ]
    graphics [
      x 495.09531902082045
      y 594.108189765861
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000132155"
      hgnc "NA"
      map_id "W9_41"
      name "RAF1"
      node_subtype "GENE"
      node_type "species"
      org_id "fd989"
      uniprot "NA"
    ]
    graphics [
      x 1597.6001128322687
      y 1060.1821262669039
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_66"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idbeaaca30"
      uniprot "NA"
    ]
    graphics [
      x 1554.6086656947666
      y 1187.5457259244986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_57"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id6fd5bed2"
      uniprot "NA"
    ]
    graphics [
      x 810.4802712572146
      y 947.3096135135261
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:wikidata:Q89458416;urn:miriam:uniprot:P59632;urn:miriam:uniprot:P59635;urn:miriam:uniprot:P59634;urn:miriam:uniprot:P59633;urn:miriam:wikidata:Q89457519"
      hgnc "NA"
      map_id "W9_7"
      name "afd54"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "afd54"
      uniprot "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59634;UNIPROT:P59633"
    ]
    graphics [
      x 806.4028844859315
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_65"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idac31cf2f"
      uniprot "NA"
    ]
    graphics [
      x 910.9916836847933
      y 120.88335980019929
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_52"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id559f5826"
      uniprot "NA"
    ]
    graphics [
      x 1126.2831561943337
      y 672.665620490304
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_18"
      name "Innate_br_immunity"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "cc6a0"
      uniprot "NA"
    ]
    graphics [
      x 971.4559338806419
      y 696.618643464769
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_53"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id589b86fe"
      uniprot "NA"
    ]
    graphics [
      x 641.968658090944
      y 1206.2508994002583
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_40"
      name "SARS_br_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "f9996"
      uniprot "NA"
    ]
    graphics [
      x 771.906715149113
      y 1307.9204284293
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_77"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "ideee7f970"
      uniprot "NA"
    ]
    graphics [
      x 890.6166660400729
      y 1377.9985035335826
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_47"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id392d909b"
      uniprot "NA"
    ]
    graphics [
      x 256.05947044378667
      y 1317.2530345524642
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_3"
      name "Protein_br_synthesis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "a5ffe"
      uniprot "NA"
    ]
    graphics [
      x 376.7994784069521
      y 1339.2231206572983
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_50"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id4b1d8b89"
      uniprot "NA"
    ]
    graphics [
      x 462.08549719108305
      y 81.46940212106472
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_19"
      name "Autophagy"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "cc8d3"
      uniprot "NA"
    ]
    graphics [
      x 350.35076081130995
      y 123.86812753657262
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_74"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "ide37bb3b6"
      uniprot "NA"
    ]
    graphics [
      x 737.8396864597298
      y 190.10010166877873
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_72"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "ide012a8a5"
      uniprot "NA"
    ]
    graphics [
      x 877.8211271605517
      y 441.2295300786342
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_56"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id63f652b7"
      uniprot "NA"
    ]
    graphics [
      x 521.0743246094914
      y 1349.91384743039
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ncbigene:684"
      hgnc "NA"
      map_id "W9_2"
      name "BST2"
      node_subtype "GENE"
      node_type "species"
      org_id "a24ed"
      uniprot "NA"
    ]
    graphics [
      x 1332.8891086640203
      y 1003.087714714195
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f1e34"
      uniprot "NA"
    ]
    graphics [
      x 1254.8777525329556
      y 909.1808215912113
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ncbigene:684"
      hgnc "NA"
      map_id "W9_8"
      name "BST2"
      node_subtype "GENE"
      node_type "species"
      org_id "b17a4"
      uniprot "NA"
    ]
    graphics [
      x 1376.9359403163203
      y 911.1343314954144
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_59"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id7c30bf1f"
      uniprot "NA"
    ]
    graphics [
      x 1197.8632116940128
      y 1419.880698285246
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ensembl:ENSG00000173327;urn:miriam:ensembl:ENSG00000130758;urn:miriam:ensembl:ENSG00000006432"
      hgnc "NA"
      map_id "W9_11"
      name "b9bb1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b9bb1"
      uniprot "NA"
    ]
    graphics [
      x 765.9436806279941
      y 742.055947018034
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_45"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id264ad72f"
      uniprot "NA"
    ]
    graphics [
      x 720.3281131705099
      y 577.9526185320677
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_48"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id3d6c0762"
      uniprot "NA"
    ]
    graphics [
      x 857.4816475139816
      y 795.8758741410705
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_67"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idc07c34c7"
      uniprot "NA"
    ]
    graphics [
      x 1274.7497117075754
      y 1305.0131674076501
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_60"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id80dca1f4"
      uniprot "NA"
    ]
    graphics [
      x 756.5863049271187
      y 1185.7989826044882
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_51"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id54d4a9a9"
      uniprot "NA"
    ]
    graphics [
      x 1034.0479770715694
      y 171.80977358166422
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_54"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id60fca41b"
      uniprot "NA"
    ]
    graphics [
      x 464.62564356188875
      y 963.6634236711112
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ncbigene:3429"
      hgnc "NA"
      map_id "W9_39"
      name "IFI27"
      node_subtype "GENE"
      node_type "species"
      org_id "f93d3"
      uniprot "NA"
    ]
    graphics [
      x 992.9173279953534
      y 1045.8358117275682
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4877"
      full_annotation "NA"
      hgnc "NA"
      map_id "W9_69"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idc72f872e"
      uniprot "NA"
    ]
    graphics [
      x 1082.1040347038875
      y 983.3203216068357
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4877"
      full_annotation "urn:miriam:ncbigene:3429"
      hgnc "NA"
      map_id "W9_15"
      name "IFI27"
      node_subtype "GENE"
      node_type "species"
      org_id "c5ac8"
      uniprot "NA"
    ]
    graphics [
      x 1055.9036739281376
      y 1109.3713970944423
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W9_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 79
    source 1
    target 2
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_32"
      target_id "W9_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 2
    target 3
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_62"
      target_id "W9_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 81
    source 4
    target 5
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_17"
      target_id "W9_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 82
    source 5
    target 6
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_44"
      target_id "W9_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 83
    source 7
    target 8
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_16"
      target_id "W9_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 84
    source 8
    target 9
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_63"
      target_id "W9_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 85
    source 10
    target 11
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_24"
      target_id "W9_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 86
    source 11
    target 12
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_68"
      target_id "W9_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 87
    source 9
    target 13
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_13"
      target_id "W9_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 88
    source 13
    target 14
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_58"
      target_id "W9_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 89
    source 15
    target 16
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_21"
      target_id "W9_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 90
    source 6
    target 16
    cd19dm [
      diagram "WP4877"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W9_22"
      target_id "W9_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 91
    source 16
    target 17
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_75"
      target_id "W9_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 92
    source 18
    target 19
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_36"
      target_id "W9_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 93
    source 6
    target 19
    cd19dm [
      diagram "WP4877"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W9_22"
      target_id "W9_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 94
    source 19
    target 20
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_6"
      target_id "W9_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 95
    source 21
    target 22
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_27"
      target_id "W9_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 96
    source 22
    target 23
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_61"
      target_id "W9_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 97
    source 24
    target 25
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_26"
      target_id "W9_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 98
    source 25
    target 14
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_70"
      target_id "W9_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 99
    source 26
    target 27
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_28"
      target_id "W9_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 100
    source 27
    target 24
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_78"
      target_id "W9_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 28
    target 29
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_30"
      target_id "W9_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 29
    target 14
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_42"
      target_id "W9_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 30
    target 31
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_25"
      target_id "W9_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 31
    target 32
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_76"
      target_id "W9_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 33
    target 34
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_5"
      target_id "W9_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 106
    source 6
    target 34
    cd19dm [
      diagram "WP4877"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W9_22"
      target_id "W9_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 107
    source 34
    target 35
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_31"
      target_id "W9_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 108
    source 32
    target 36
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_4"
      target_id "W9_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 109
    source 36
    target 4
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_46"
      target_id "W9_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 110
    source 26
    target 37
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_28"
      target_id "W9_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 111
    source 37
    target 28
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_43"
      target_id "W9_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 112
    source 14
    target 38
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_37"
      target_id "W9_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 113
    source 38
    target 39
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_49"
      target_id "W9_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 114
    source 40
    target 41
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_1"
      target_id "W9_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 41
    target 23
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_73"
      target_id "W9_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 3
    target 42
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_38"
      target_id "W9_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 42
    target 43
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_64"
      target_id "W9_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 44
    target 45
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_14"
      target_id "W9_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 45
    target 46
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_55"
      target_id "W9_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 7
    target 47
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_16"
      target_id "W9_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 47
    target 28
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_71"
      target_id "W9_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 48
    target 49
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_41"
      target_id "W9_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 49
    target 46
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_66"
      target_id "W9_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 14
    target 50
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_37"
      target_id "W9_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 50
    target 6
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_57"
      target_id "W9_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 126
    source 51
    target 52
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_7"
      target_id "W9_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 127
    source 52
    target 4
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_65"
      target_id "W9_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 128
    source 6
    target 53
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_22"
      target_id "W9_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 53
    target 54
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_52"
      target_id "W9_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 130
    source 14
    target 55
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_37"
      target_id "W9_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 131
    source 56
    target 55
    cd19dm [
      diagram "WP4877"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W9_40"
      target_id "W9_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 132
    source 55
    target 10
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_53"
      target_id "W9_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 133
    source 3
    target 57
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_38"
      target_id "W9_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 134
    source 56
    target 57
    cd19dm [
      diagram "WP4877"
      edge_type "INHIBITION"
      source_id "W9_40"
      target_id "W9_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 135
    source 57
    target 10
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_77"
      target_id "W9_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 136
    source 39
    target 58
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_20"
      target_id "W9_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 137
    source 58
    target 59
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_47"
      target_id "W9_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 138
    source 21
    target 60
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_27"
      target_id "W9_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 139
    source 60
    target 61
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_50"
      target_id "W9_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 4
    target 62
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_17"
      target_id "W9_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 62
    target 21
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_74"
      target_id "W9_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 9
    target 63
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_13"
      target_id "W9_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 63
    target 4
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_72"
      target_id "W9_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 144
    source 10
    target 64
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_24"
      target_id "W9_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 145
    source 64
    target 59
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_56"
      target_id "W9_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 146
    source 65
    target 66
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_2"
      target_id "W9_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 147
    source 6
    target 66
    cd19dm [
      diagram "WP4877"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W9_22"
      target_id "W9_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 148
    source 66
    target 67
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_34"
      target_id "W9_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 149
    source 44
    target 68
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_14"
      target_id "W9_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 150
    source 68
    target 3
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_59"
      target_id "W9_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 151
    source 69
    target 70
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_11"
      target_id "W9_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 152
    source 70
    target 32
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_45"
      target_id "W9_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 153
    source 43
    target 71
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_10"
      target_id "W9_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 154
    source 71
    target 54
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_48"
      target_id "W9_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 155
    source 46
    target 72
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_29"
      target_id "W9_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 156
    source 72
    target 3
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_67"
      target_id "W9_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 157
    source 43
    target 73
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_10"
      target_id "W9_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 158
    source 73
    target 12
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_60"
      target_id "W9_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 159
    source 30
    target 74
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_25"
      target_id "W9_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 160
    source 74
    target 4
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_51"
      target_id "W9_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 161
    source 14
    target 75
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_37"
      target_id "W9_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 162
    source 75
    target 40
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_54"
      target_id "W9_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 163
    source 76
    target 77
    cd19dm [
      diagram "WP4877"
      edge_type "CONSPUMPTION"
      source_id "W9_39"
      target_id "W9_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 164
    source 6
    target 77
    cd19dm [
      diagram "WP4877"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W9_22"
      target_id "W9_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 165
    source 77
    target 78
    cd19dm [
      diagram "WP4877"
      edge_type "PRODUCTION"
      source_id "W9_69"
      target_id "W9_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
