# generated with VANTED V2.8.2 at Fri Mar 04 09:57:01 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4860"
      full_annotation "urn:miriam:wikidata:Q25100575"
      hgnc "NA"
      map_id "W5_3"
      name "Pevonedistat"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "dda75"
      uniprot "NA"
    ]
    graphics [
      x 440.2807576500084
      y 328.7781779827884
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W5_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:22088887"
      count 1
      diagram "WP4860"
      full_annotation "NA"
      hgnc "NA"
      map_id "W5_6"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idc583d98b"
      uniprot "NA"
    ]
    graphics [
      x 452.401889819532
      y 218.81882292568667
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W5_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4860"
      full_annotation "urn:miriam:ensembl:ENSG00000144744;urn:miriam:ensembl:ENSG00000159593"
      hgnc "NA"
      map_id "W5_1"
      name "c466c"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c466c"
      uniprot "NA"
    ]
    graphics [
      x 543.748499343274
      y 157.43910591418003
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W5_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4860"
      full_annotation "NA"
      hgnc "NA"
      map_id "W5_2"
      name "Substrate"
      node_subtype "GENE"
      node_type "species"
      org_id "d1f64"
      uniprot "NA"
    ]
    graphics [
      x 98.09435920224809
      y 224.28988623899974
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W5_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "PUBMED:22088887"
      count 1
      diagram "WP4860"
      full_annotation "NA"
      hgnc "NA"
      map_id "W5_5"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id92c39589"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 118.48523425654186
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W5_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4860"
      full_annotation "urn:miriam:wikipathways:WP183"
      hgnc "NA"
      map_id "W5_4"
      name "Proteasome_space_degradation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "e5bba"
      uniprot "NA"
    ]
    graphics [
      x 158.9758718897416
      y 62.500000000000014
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W5_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 7
    source 1
    target 2
    cd19dm [
      diagram "WP4860"
      edge_type "CONSPUMPTION"
      source_id "W5_3"
      target_id "W5_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 8
    source 2
    target 3
    cd19dm [
      diagram "WP4860"
      edge_type "PRODUCTION"
      source_id "W5_6"
      target_id "W5_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 9
    source 4
    target 5
    cd19dm [
      diagram "WP4860"
      edge_type "CONSPUMPTION"
      source_id "W5_2"
      target_id "W5_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 10
    source 5
    target 6
    cd19dm [
      diagram "WP4860"
      edge_type "PRODUCTION"
      source_id "W5_5"
      target_id "W5_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
