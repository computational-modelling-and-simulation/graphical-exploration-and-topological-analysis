# generated with VANTED V2.8.2 at Fri Mar 04 09:57:02 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:O15455;urn:miriam:uniprot:O15455;urn:miriam:ensembl:ENSG00000164342;urn:miriam:refseq:NM_003265;urn:miriam:ncbigene:7098;urn:miriam:ncbigene:7098;urn:miriam:hgnc.symbol:TLR3;urn:miriam:hgnc.symbol:TLR3;urn:miriam:hgnc:11849"
      hgnc "HGNC_SYMBOL:TLR3"
      map_id "M116_75"
      name "TLR3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa45"
      uniprot "UNIPROT:O15455"
    ]
    graphics [
      x 642.234443673558
      y 978.7017219059509
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:23758787"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re67"
      uniprot "NA"
    ]
    graphics [
      x 700.8210654679291
      y 862.3200167185013
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_73"
      name "double_minus_stranded_space_RNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa41"
      uniprot "NA"
    ]
    graphics [
      x 706.8666473412582
      y 994.3246584132321
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:O15455;urn:miriam:uniprot:O15455;urn:miriam:ensembl:ENSG00000164342;urn:miriam:refseq:NM_003265;urn:miriam:ncbigene:7098;urn:miriam:ncbigene:7098;urn:miriam:hgnc.symbol:TLR3;urn:miriam:hgnc.symbol:TLR3;urn:miriam:hgnc:11849"
      hgnc "HGNC_SYMBOL:TLR3"
      map_id "M116_97"
      name "TLR3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa93"
      uniprot "UNIPROT:O15455"
    ]
    graphics [
      x 661.6678945424225
      y 704.3644776810598
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:refseq:NM_138554;urn:miriam:ncbigene:7099;urn:miriam:ncbigene:7099;urn:miriam:ec-code:3.2.2.6;urn:miriam:hgnc:11850;urn:miriam:uniprot:O00206;urn:miriam:uniprot:O00206;urn:miriam:hgnc.symbol:TLR4;urn:miriam:hgnc.symbol:TLR4;urn:miriam:ensembl:ENSG00000136869"
      hgnc "HGNC_SYMBOL:TLR4"
      map_id "M116_65"
      name "TLR4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa142"
      uniprot "UNIPROT:O00206"
    ]
    graphics [
      x 1317.7896957718099
      y 1425.7401883816106
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "PUBMED:28829373"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_27"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re112"
      uniprot "NA"
    ]
    graphics [
      x 1358.1423898344876
      y 1310.5418518713593
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:O14896;urn:miriam:uniprot:O14896;urn:miriam:refseq:NM_006147;urn:miriam:ensembl:ENSG00000117595;urn:miriam:hgnc:6121;urn:miriam:ncbigene:3664;urn:miriam:ncbigene:3664;urn:miriam:hgnc.symbol:IRF6;urn:miriam:hgnc.symbol:IRF6"
      hgnc "HGNC_SYMBOL:IRF6"
      map_id "M116_64"
      name "IRF6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa141"
      uniprot "UNIPROT:O14896"
    ]
    graphics [
      x 1212.499153953679
      y 1366.0916446692659
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ncbigene:4615;urn:miriam:ensembl:ENSG00000172936;urn:miriam:ncbigene:4615;urn:miriam:uniprot:Q99836;urn:miriam:uniprot:Q99836;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc:7562;urn:miriam:refseq:NM_002468"
      hgnc "HGNC_SYMBOL:MYD88"
      map_id "M116_66"
      name "MYD88"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa143"
      uniprot "UNIPROT:Q99836"
    ]
    graphics [
      x 1406.3053948546992
      y 1155.535006811399
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:11850;urn:miriam:hgnc:6121;urn:miriam:hgnc:7562;urn:miriam:uniprot:O14896;urn:miriam:uniprot:O14896;urn:miriam:refseq:NM_006147;urn:miriam:ensembl:ENSG00000117595;urn:miriam:hgnc:6121;urn:miriam:ncbigene:3664;urn:miriam:ncbigene:3664;urn:miriam:hgnc.symbol:IRF6;urn:miriam:hgnc.symbol:IRF6;urn:miriam:ncbigene:4615;urn:miriam:ensembl:ENSG00000172936;urn:miriam:ncbigene:4615;urn:miriam:uniprot:Q99836;urn:miriam:uniprot:Q99836;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc:7562;urn:miriam:refseq:NM_002468;urn:miriam:refseq:NM_138554;urn:miriam:ncbigene:7099;urn:miriam:ncbigene:7099;urn:miriam:ec-code:3.2.2.6;urn:miriam:hgnc:11850;urn:miriam:uniprot:O00206;urn:miriam:uniprot:O00206;urn:miriam:hgnc.symbol:TLR4;urn:miriam:hgnc.symbol:TLR4;urn:miriam:ensembl:ENSG00000136869"
      hgnc "HGNC_SYMBOL:IRF6;HGNC_SYMBOL:MYD88;HGNC_SYMBOL:TLR4"
      map_id "M116_16"
      name "LPS_slash_TLR4_slash_MYD88"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa8"
      uniprot "UNIPROT:O14896;UNIPROT:Q99836;UNIPROT:O00206"
    ]
    graphics [
      x 1233.5756443056825
      y 1275.6555638366235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_53"
      name "s187"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa124"
      uniprot "NA"
    ]
    graphics [
      x 994.832333037708
      y 424.7342446294279
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "PUBMED:32172672"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_46"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re97"
      uniprot "NA"
    ]
    graphics [
      x 1097.3717451654536
      y 367.75597395233046
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:P0DTC3;urn:miriam:ncbigene:43740569"
      hgnc "NA"
      map_id "M116_47"
      name "Orf3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1"
      uniprot "UNIPROT:P0DTC3"
    ]
    graphics [
      x 1256.3914307186008
      y 382.4904067791171
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "M116_71"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa34"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 998.771917210303
      y 307.2779035085507
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7189;urn:miriam:ncbigene:7189;urn:miriam:ensembl:ENSG00000175104;urn:miriam:uniprot:Q9Y4K3;urn:miriam:uniprot:Q9Y4K3;urn:miriam:hgnc:12036;urn:miriam:refseq:NM_145803;urn:miriam:hgnc.symbol:TRAF6;urn:miriam:hgnc.symbol:TRAF6"
      hgnc "HGNC_SYMBOL:TRAF6"
      map_id "M116_58"
      name "TRAF6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa135"
      uniprot "UNIPROT:Q9Y4K3"
    ]
    graphics [
      x 1048.5975496711499
      y 1183.4758600827395
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "PUBMED:23758787"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_30"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re32"
      uniprot "NA"
    ]
    graphics [
      x 1165.168877373212
      y 1180.1616043079314
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7189;urn:miriam:ncbigene:7189;urn:miriam:ensembl:ENSG00000175104;urn:miriam:uniprot:Q9Y4K3;urn:miriam:uniprot:Q9Y4K3;urn:miriam:hgnc:12036;urn:miriam:refseq:NM_145803;urn:miriam:hgnc.symbol:TRAF6;urn:miriam:hgnc.symbol:TRAF6"
      hgnc "HGNC_SYMBOL:TRAF6"
      map_id "M116_50"
      name "TRAF6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa101"
      uniprot "UNIPROT:Q9Y4K3"
    ]
    graphics [
      x 1066.8825163763054
      y 1086.9721963102888
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:9955;urn:miriam:hgnc:7794;urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998;urn:miriam:hgnc:9955;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ncbigene:5970;urn:miriam:ncbigene:5970;urn:miriam:refseq:NM_021975;urn:miriam:hgnc.symbol:RELA;urn:miriam:hgnc.symbol:RELA;urn:miriam:uniprot:Q04206;urn:miriam:uniprot:Q04206"
      hgnc "HGNC_SYMBOL:NFKB1;HGNC_SYMBOL:RELA"
      map_id "M116_15"
      name "P65_slash_P015"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa7"
      uniprot "UNIPROT:P19838;UNIPROT:Q04206"
    ]
    graphics [
      x 1482.3002303104117
      y 154.70447579961
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "PUBMED:32172672"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_22"
      name "NA"
      node_subtype "POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "re106"
      uniprot "NA"
    ]
    graphics [
      x 1497.7262574119081
      y 316.19859378821945
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:5992;urn:miriam:hgnc.symbol:IL1B;urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:uniprot:P01584;urn:miriam:refseq:NM_000576;urn:miriam:ncbigene:3553;urn:miriam:ncbigene:3553;urn:miriam:ensembl:ENSG00000125538"
      hgnc "HGNC_SYMBOL:IL1B"
      map_id "M116_54"
      name "IL1b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa127"
      uniprot "UNIPROT:P01584"
    ]
    graphics [
      x 1610.2404482808388
      y 354.1554653652541
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:5960;urn:miriam:hgnc:5961;urn:miriam:hgnc:1974;urn:miriam:hgnc:5960;urn:miriam:ensembl:ENSG00000104365;urn:miriam:ec-code:2.7.11.10;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:uniprot:O14920;urn:miriam:uniprot:O14920;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:ncbigene:3551;urn:miriam:ncbigene:3551;urn:miriam:refseq:NM_001190720;urn:miriam:hgnc.symbol:CHUK;urn:miriam:hgnc.symbol:CHUK;urn:miriam:refseq:NM_001278;urn:miriam:ec-code:2.7.11.10;urn:miriam:ensembl:ENSG00000213341;urn:miriam:hgnc:1974;urn:miriam:uniprot:O15111;urn:miriam:uniprot:O15111;urn:miriam:ncbigene:1147;urn:miriam:ncbigene:1147;urn:miriam:hgnc:5961;urn:miriam:ensembl:ENSG00000269335;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:refseq:NM_003639;urn:miriam:ncbigene:8517;urn:miriam:ncbigene:8517;urn:miriam:uniprot:Q9Y6K9;urn:miriam:uniprot:Q9Y6K9"
      hgnc "HGNC_SYMBOL:IKBKB;HGNC_SYMBOL:CHUK;HGNC_SYMBOL:IKBKG"
      map_id "M116_11"
      name "IKBKG_slash_IKBKB_slash_CHUK"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa22"
      uniprot "UNIPROT:O14920;UNIPROT:O15111;UNIPROT:Q9Y6K9"
    ]
    graphics [
      x 1282.8222848274436
      y 490.23461174794056
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "PUBMED:32172672"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re96"
      uniprot "NA"
    ]
    graphics [
      x 1185.2811388799744
      y 416.0748207998024
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:5991;urn:miriam:hgnc:6121;urn:miriam:hgnc:11916;urn:miriam:hgnc:5991;urn:miriam:refseq:NM_000575;urn:miriam:hgnc.symbol:IL1A;urn:miriam:hgnc.symbol:IL1A;urn:miriam:uniprot:P01583;urn:miriam:uniprot:P01583;urn:miriam:ensembl:ENSG00000115008;urn:miriam:ncbigene:3552;urn:miriam:ncbigene:3552;urn:miriam:ncbigene:7132;urn:miriam:ncbigene:7132;urn:miriam:refseq:NM_001065;urn:miriam:ensembl:ENSG00000067182;urn:miriam:uniprot:P19438;urn:miriam:uniprot:P19438;urn:miriam:hgnc.symbol:TNFRSF1A;urn:miriam:hgnc.symbol:TNFRSF1A;urn:miriam:hgnc:11916;urn:miriam:uniprot:O14896;urn:miriam:uniprot:O14896;urn:miriam:refseq:NM_006147;urn:miriam:ensembl:ENSG00000117595;urn:miriam:hgnc:6121;urn:miriam:ncbigene:3664;urn:miriam:ncbigene:3664;urn:miriam:hgnc.symbol:IRF6;urn:miriam:hgnc.symbol:IRF6"
      hgnc "HGNC_SYMBOL:IL1A;HGNC_SYMBOL:TNFRSF1A;HGNC_SYMBOL:IRF6"
      map_id "M116_12"
      name "LPS_slash_TNF_space__alpha__slash_IL_minus_1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa3"
      uniprot "UNIPROT:P01583;UNIPROT:P19438;UNIPROT:O14896"
    ]
    graphics [
      x 1203.9077948746983
      y 531.8538441692396
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:5960;urn:miriam:hgnc:5961;urn:miriam:hgnc:1974;urn:miriam:hgnc:5960;urn:miriam:ensembl:ENSG00000104365;urn:miriam:ec-code:2.7.11.10;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:uniprot:O14920;urn:miriam:uniprot:O14920;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:ncbigene:3551;urn:miriam:ncbigene:3551;urn:miriam:refseq:NM_001190720;urn:miriam:hgnc.symbol:CHUK;urn:miriam:hgnc.symbol:CHUK;urn:miriam:refseq:NM_001278;urn:miriam:ec-code:2.7.11.10;urn:miriam:ensembl:ENSG00000213341;urn:miriam:hgnc:1974;urn:miriam:uniprot:O15111;urn:miriam:uniprot:O15111;urn:miriam:ncbigene:1147;urn:miriam:ncbigene:1147;urn:miriam:hgnc:5961;urn:miriam:ensembl:ENSG00000269335;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:refseq:NM_003639;urn:miriam:ncbigene:8517;urn:miriam:ncbigene:8517;urn:miriam:uniprot:Q9Y6K9;urn:miriam:uniprot:Q9Y6K9"
      hgnc "HGNC_SYMBOL:IKBKB;HGNC_SYMBOL:CHUK;HGNC_SYMBOL:IKBKG"
      map_id "M116_13"
      name "IKBKG_slash_IKBKB_slash_CHUK"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa4"
      uniprot "UNIPROT:O14920;UNIPROT:O15111;UNIPROT:Q9Y6K9"
    ]
    graphics [
      x 1225.1144154645965
      y 248.88513470293833
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000166167;urn:miriam:uniprot:Q9Y297;urn:miriam:uniprot:Q9Y297;urn:miriam:hgnc:1144;urn:miriam:hgnc.symbol:BTRC;urn:miriam:hgnc.symbol:BTRC;urn:miriam:ncbigene:8945;urn:miriam:ncbigene:8945;urn:miriam:refseq:NM_033637"
      hgnc "HGNC_SYMBOL:BTRC"
      map_id "M116_59"
      name "BTRC"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa137"
      uniprot "UNIPROT:Q9Y297"
    ]
    graphics [
      x 791.5737972021025
      y 1263.0569390086962
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "PUBMED:21135871"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_21"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re105"
      uniprot "NA"
    ]
    graphics [
      x 658.3040522580463
      y 1325.2239461184038
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:16288;urn:miriam:ec-code:2.3.2.27;urn:miriam:ensembl:ENSG00000100505;urn:miriam:ncbigene:114088;urn:miriam:ncbigene:114088;urn:miriam:uniprot:Q9C026;urn:miriam:uniprot:Q9C026;urn:miriam:hgnc.symbol:TRIM9;urn:miriam:refseq:NM_015163;urn:miriam:hgnc.symbol:TRIM9"
      hgnc "HGNC_SYMBOL:TRIM9"
      map_id "M116_85"
      name "TRIM9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa74"
      uniprot "UNIPROT:Q9C026"
    ]
    graphics [
      x 804.6081005497416
      y 1330.4875101518796
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000166167;urn:miriam:uniprot:Q9Y297;urn:miriam:uniprot:Q9Y297;urn:miriam:hgnc:1144;urn:miriam:hgnc.symbol:BTRC;urn:miriam:hgnc.symbol:BTRC;urn:miriam:ncbigene:8945;urn:miriam:ncbigene:8945;urn:miriam:refseq:NM_033637"
      hgnc "HGNC_SYMBOL:BTRC"
      map_id "M116_86"
      name "BTRC"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa75"
      uniprot "UNIPROT:Q9Y297"
    ]
    graphics [
      x 494.20245836363904
      y 1286.284414015846
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:TICAM1;urn:miriam:uniprot:Q8IUC6;urn:miriam:uniprot:Q8IUC6;urn:miriam:hgnc.symbol:TICAM1;urn:miriam:ensembl:ENSG00000127666;urn:miriam:hgnc:18348;urn:miriam:refseq:NM_014261;urn:miriam:ncbigene:148022;urn:miriam:ncbigene:148022"
      hgnc "HGNC_SYMBOL:TICAM1"
      map_id "M116_98"
      name "TICAM1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa94"
      uniprot "UNIPROT:Q8IUC6"
    ]
    graphics [
      x 554.6268694968164
      y 578.7219574334181
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "PUBMED:22539786;PUBMED:23758787"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_35"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re68"
      uniprot "NA"
    ]
    graphics [
      x 657.9075063706389
      y 504.7845364265052
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:TRIM38;urn:miriam:hgnc.symbol:TRIM38;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:10475;urn:miriam:ncbigene:10475;urn:miriam:ensembl:ENSG00000112343;urn:miriam:refseq:NM_006355;urn:miriam:hgnc:10059;urn:miriam:uniprot:O00635;urn:miriam:uniprot:O00635"
      hgnc "HGNC_SYMBOL:TRIM38"
      map_id "M116_78"
      name "TRIM38"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa50"
      uniprot "UNIPROT:O00635"
    ]
    graphics [
      x 513.3565883733672
      y 513.732299280025
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:TICAM1;urn:miriam:uniprot:Q8IUC6;urn:miriam:uniprot:Q8IUC6;urn:miriam:hgnc.symbol:TICAM1;urn:miriam:ensembl:ENSG00000127666;urn:miriam:hgnc:18348;urn:miriam:refseq:NM_014261;urn:miriam:ncbigene:148022;urn:miriam:ncbigene:148022"
      hgnc "HGNC_SYMBOL:TICAM1"
      map_id "M116_76"
      name "TICAM1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa48"
      uniprot "UNIPROT:Q8IUC6"
    ]
    graphics [
      x 766.5791171073439
      y 354.69666864546576
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ncbigene:4615;urn:miriam:ensembl:ENSG00000172936;urn:miriam:ncbigene:4615;urn:miriam:uniprot:Q99836;urn:miriam:uniprot:Q99836;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc:7562;urn:miriam:refseq:NM_002468"
      hgnc "HGNC_SYMBOL:MYD88"
      map_id "M116_49"
      name "MYD88"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa100"
      uniprot "UNIPROT:Q99836"
    ]
    graphics [
      x 783.6738307384243
      y 1117.5229959649832
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "PUBMED:15361868"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_38"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re73"
      uniprot "NA"
    ]
    graphics [
      x 734.8067707788589
      y 1242.6507287382187
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:15631;urn:miriam:hgnc:15632;urn:miriam:hgnc:15633;urn:miriam:uniprot:Q9NYK1;urn:miriam:uniprot:Q9NYK1;urn:miriam:hgnc:15631;urn:miriam:refseq:NM_016562;urn:miriam:hgnc.symbol:TLR7;urn:miriam:hgnc.symbol:TLR7;urn:miriam:ensembl:ENSG00000196664;urn:miriam:ncbigene:51284;urn:miriam:ncbigene:51284;urn:miriam:uniprot:Q9NR96;urn:miriam:uniprot:Q9NR96;urn:miriam:ncbigene:54106;urn:miriam:ncbigene:54106;urn:miriam:ensembl:ENSG00000239732;urn:miriam:hgnc.symbol:TLR9;urn:miriam:hgnc.symbol:TLR9;urn:miriam:refseq:NM_017442;urn:miriam:hgnc:15633;urn:miriam:refseq:NM_016610;urn:miriam:uniprot:Q9NR97;urn:miriam:uniprot:Q9NR97;urn:miriam:hgnc:15632;urn:miriam:ncbigene:51311;urn:miriam:ncbigene:51311;urn:miriam:hgnc.symbol:TLR8;urn:miriam:ensembl:ENSG00000101916;urn:miriam:hgnc.symbol:TLR8"
      hgnc "HGNC_SYMBOL:TLR7;HGNC_SYMBOL:TLR9;HGNC_SYMBOL:TLR8"
      map_id "M116_6"
      name "TLR7_slash_8_slash_9"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa17"
      uniprot "UNIPROT:Q9NYK1;UNIPROT:Q9NR96;UNIPROT:Q9NR97"
    ]
    graphics [
      x 739.6849778523384
      y 1428.826601861857
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ncbigene:4615;urn:miriam:ensembl:ENSG00000172936;urn:miriam:ncbigene:4615;urn:miriam:uniprot:Q99836;urn:miriam:uniprot:Q99836;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc:7562;urn:miriam:refseq:NM_002468"
      hgnc "HGNC_SYMBOL:MYD88"
      map_id "M116_77"
      name "MYD88"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa49"
      uniprot "UNIPROT:Q99836"
    ]
    graphics [
      x 870.4349307245911
      y 1136.8458666046713
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:refseq:NM_172016;urn:miriam:hgnc.symbol:TRIM39;urn:miriam:hgnc.symbol:TRIM39;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:56658;urn:miriam:ncbigene:56658;urn:miriam:hgnc:10065;urn:miriam:uniprot:Q9HCM9;urn:miriam:uniprot:Q9HCM9;urn:miriam:ensembl:ENSG00000204599"
      hgnc "HGNC_SYMBOL:TRIM39"
      map_id "M116_87"
      name "TRIM39"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa76"
      uniprot "UNIPROT:Q9HCM9"
    ]
    graphics [
      x 165.43281824928886
      y 274.80488241672526
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "PUBMED:26999213"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_25"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re110"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 338.13523173807124
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:refseq:NM_172016;urn:miriam:hgnc.symbol:TRIM39;urn:miriam:hgnc.symbol:TRIM39;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:56658;urn:miriam:ncbigene:56658;urn:miriam:hgnc:10065;urn:miriam:uniprot:Q9HCM9;urn:miriam:uniprot:Q9HCM9;urn:miriam:ensembl:ENSG00000204599"
      hgnc "HGNC_SYMBOL:TRIM39"
      map_id "M116_61"
      name "TRIM39"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa139"
      uniprot "UNIPROT:Q9HCM9"
    ]
    graphics [
      x 74.75571544403158
      y 466.75897487063855
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M116_90"
      name "CASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa82"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 1727.4684895270375
      y 627.1405972849158
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_29"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re24"
      uniprot "NA"
    ]
    graphics [
      x 1735.337942327059
      y 518.2548220226058
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ncbigene:29108;urn:miriam:ncbigene:29108;urn:miriam:refseq:NM_013258;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:ensembl:ENSG00000103490;urn:miriam:pubmed:32172672;urn:miriam:hgnc:16608;urn:miriam:uniprot:Q9ULZ3;urn:miriam:uniprot:Q9ULZ3"
      hgnc "HGNC_SYMBOL:PYCARD"
      map_id "M116_62"
      name "PYCARD"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa14"
      uniprot "UNIPROT:Q9ULZ3"
    ]
    graphics [
      x 1545.0306121638737
      y 427.5723848546938
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:Q96P20;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:NLRP3"
      map_id "M116_48"
      name "NLRP3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa10"
      uniprot "UNIPROT:Q96P20"
    ]
    graphics [
      x 1789.7143012210445
      y 707.9189458025164
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ncbigene:834;urn:miriam:ncbigene:834;urn:miriam:ec-code:3.4.22.36;urn:miriam:hgnc:1499;urn:miriam:ensembl:ENSG00000137752;urn:miriam:uniprot:P29466;urn:miriam:uniprot:P29466;urn:miriam:hgnc.symbol:CASP1;urn:miriam:hgnc.symbol:CASP1;urn:miriam:refseq:NM_033292"
      hgnc "HGNC_SYMBOL:CASP1"
      map_id "M116_69"
      name "CASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa25"
      uniprot "UNIPROT:P29466"
    ]
    graphics [
      x 1787.673820477762
      y 367.07934904140956
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "PUBMED:32172672"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_23"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re107"
      uniprot "NA"
    ]
    graphics [
      x 1304.2350651708232
      y 88.98385442197196
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:E;urn:miriam:ncbiprotein:1796318600"
      hgnc "NA"
      map_id "M116_91"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa83"
      uniprot "UNIPROT:E"
    ]
    graphics [
      x 1163.3229618895764
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ncbigene:29108;urn:miriam:ncbigene:29108;urn:miriam:refseq:NM_013258;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:hgnc.symbol:PYCARD;urn:miriam:ensembl:ENSG00000103490;urn:miriam:pubmed:32172672;urn:miriam:hgnc:16608;urn:miriam:uniprot:Q9ULZ3;urn:miriam:uniprot:Q9ULZ3"
      hgnc "HGNC_SYMBOL:PYCARD"
      map_id "M116_55"
      name "PYCARD"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa131"
      uniprot "UNIPROT:Q9ULZ3"
    ]
    graphics [
      x 1379.5629702678611
      y 182.81505648785685
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "PUBMED:32172672"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_19"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re103"
      uniprot "NA"
    ]
    graphics [
      x 1337.762416169522
      y 300.6135310295798
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033"
      hgnc "HGNC_SYMBOL:TRAF3"
      map_id "M116_51"
      name "TRAF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa11"
      uniprot "UNIPROT:Q13114"
    ]
    graphics [
      x 1125.6779779906353
      y 234.48921256712322
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:P08697;urn:miriam:uniprot:P08697;urn:miriam:ensembl:ENSG00000167711;urn:miriam:ncbigene:5345;urn:miriam:ncbigene:5345;urn:miriam:hgnc:9075;urn:miriam:refseq:NM_000934;urn:miriam:hgnc.symbol:SERPINF2;urn:miriam:hgnc.symbol:SERPINF2"
      hgnc "HGNC_SYMBOL:SERPINF2"
      map_id "M116_92"
      name "SERPINF2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa86"
      uniprot "UNIPROT:P08697"
    ]
    graphics [
      x 130.83002184048757
      y 1328.581785117612
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      annotation "PUBMED:17706453"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_42"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re90"
      uniprot "NA"
    ]
    graphics [
      x 234.57950389563234
      y 1276.2384202027308
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:6859;urn:miriam:hgnc:30681;urn:miriam:hgnc:17075;urn:miriam:hgnc.symbol:TAB2;urn:miriam:ensembl:ENSG00000055208;urn:miriam:hgnc.symbol:TAB2;urn:miriam:uniprot:Q9NYJ8;urn:miriam:uniprot:Q9NYJ8;urn:miriam:hgnc:17075;urn:miriam:ncbigene:23118;urn:miriam:refseq:NM_001292034;urn:miriam:ncbigene:23118;urn:miriam:uniprot:O43318;urn:miriam:uniprot:O43318;urn:miriam:ensembl:ENSG00000135341;urn:miriam:refseq:NM_145331;urn:miriam:hgnc:6859;urn:miriam:ncbigene:6885;urn:miriam:ncbigene:6885;urn:miriam:hgnc.symbol:MAP3K7;urn:miriam:hgnc.symbol:MAP3K7;urn:miriam:ec-code:2.7.11.25;urn:miriam:ensembl:ENSG00000157625;urn:miriam:ncbigene:257397;urn:miriam:ncbigene:257397;urn:miriam:hgnc.symbol:TAB3;urn:miriam:hgnc.symbol:TAB3;urn:miriam:uniprot:Q8N5C8;urn:miriam:uniprot:Q8N5C8;urn:miriam:hgnc:30681;urn:miriam:refseq:NM_152787"
      hgnc "HGNC_SYMBOL:TAB2;HGNC_SYMBOL:MAP3K7;HGNC_SYMBOL:TAB3"
      map_id "M116_1"
      name "TAB2_slash_TAB3_slash_TAK1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa10"
      uniprot "UNIPROT:Q9NYJ8;UNIPROT:O43318;UNIPROT:Q8N5C8"
    ]
    graphics [
      x 343.9777572628441
      y 1085.395061873653
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:P08697;urn:miriam:uniprot:P08697;urn:miriam:ensembl:ENSG00000167711;urn:miriam:ncbigene:5345;urn:miriam:ncbigene:5345;urn:miriam:hgnc:9075;urn:miriam:refseq:NM_000934;urn:miriam:hgnc.symbol:SERPINF2;urn:miriam:hgnc.symbol:SERPINF2"
      hgnc "HGNC_SYMBOL:SERPINF2"
      map_id "M116_93"
      name "SERPINF2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa87"
      uniprot "UNIPROT:P08697"
    ]
    graphics [
      x 207.9091499582944
      y 1447.1368810111085
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000171855;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc:5434;urn:miriam:uniprot:P01574;urn:miriam:uniprot:P01574;urn:miriam:refseq:NM_002176;urn:miriam:ncbigene:3456;urn:miriam:ncbigene:3456"
      hgnc "HGNC_SYMBOL:IFNB1"
      map_id "M116_94"
      name "IFNB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa88"
      uniprot "UNIPROT:P01574"
    ]
    graphics [
      x 323.503488541259
      y 1575.081169066485
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      annotation "PUBMED:17706453"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_33"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re64"
      uniprot "NA"
    ]
    graphics [
      x 212.63504290145738
      y 1599.9585254304457
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000171855;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc:5434;urn:miriam:uniprot:P01574;urn:miriam:uniprot:P01574;urn:miriam:refseq:NM_002176;urn:miriam:ncbigene:3456;urn:miriam:ncbigene:3456"
      hgnc "HGNC_SYMBOL:IFNB1"
      map_id "M116_68"
      name "IFNB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa147"
      uniprot "UNIPROT:P01574"
    ]
    graphics [
      x 339.37287377751676
      y 1659.4023324363552
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ensembl:ENSG00000131323;urn:miriam:hgnc.symbol:TRAF3;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7187;urn:miriam:ncbigene:7187;urn:miriam:refseq:NM_145725;urn:miriam:uniprot:Q13114;urn:miriam:uniprot:Q13114;urn:miriam:hgnc:12033"
      hgnc "HGNC_SYMBOL:TRAF3"
      map_id "M116_52"
      name "TRAF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa116"
      uniprot "UNIPROT:Q13114"
    ]
    graphics [
      x 851.8415090578317
      y 132.67013365086962
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      annotation "PUBMED:28829373"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_43"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re92"
      uniprot "NA"
    ]
    graphics [
      x 911.5675089255984
      y 237.78211429991575
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ncbigene:4615;urn:miriam:ensembl:ENSG00000172936;urn:miriam:ncbigene:4615;urn:miriam:uniprot:Q99836;urn:miriam:uniprot:Q99836;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc.symbol:MYD88;urn:miriam:hgnc:7562;urn:miriam:refseq:NM_002468"
      hgnc "HGNC_SYMBOL:MYD88"
      map_id "M116_67"
      name "MYD88"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa144"
      uniprot "UNIPROT:Q99836"
    ]
    graphics [
      x 1319.267167667481
      y 940.9885645022239
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      annotation "PUBMED:22588174"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_28"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re113"
      uniprot "NA"
    ]
    graphics [
      x 1410.316035731877
      y 985.2938663782519
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:TRIM59;urn:miriam:hgnc.symbol:TRIM59;urn:miriam:uniprot:Q8IWR1;urn:miriam:uniprot:Q8IWR1;urn:miriam:ensembl:ENSG00000213186;urn:miriam:pubmed:22588174;urn:miriam:hgnc:30834;urn:miriam:ncbigene:286827;urn:miriam:refseq:NM_173084;urn:miriam:ncbigene:286827"
      hgnc "HGNC_SYMBOL:TRIM59"
      map_id "M116_70"
      name "TRIM59"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa3"
      uniprot "UNIPROT:Q8IWR1"
    ]
    graphics [
      x 1337.425454026547
      y 803.3406509288968
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:TRIM59;urn:miriam:hgnc.symbol:TRIM59;urn:miriam:uniprot:Q8IWR1;urn:miriam:uniprot:Q8IWR1;urn:miriam:ensembl:ENSG00000213186;urn:miriam:pubmed:22588174;urn:miriam:hgnc:30834;urn:miriam:ncbigene:286827;urn:miriam:refseq:NM_173084;urn:miriam:ncbigene:286827"
      hgnc "HGNC_SYMBOL:TRIM59"
      map_id "M116_99"
      name "TRIM59"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa96"
      uniprot "UNIPROT:Q8IWR1"
    ]
    graphics [
      x 1121.900887481232
      y 632.307266715361
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "PUBMED:28829373"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re70"
      uniprot "NA"
    ]
    graphics [
      x 1246.790070560946
      y 624.9457843599116
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:9955;urn:miriam:hgnc:7794;urn:miriam:hgnc:9955;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ncbigene:5970;urn:miriam:ncbigene:5970;urn:miriam:refseq:NM_021975;urn:miriam:hgnc.symbol:RELA;urn:miriam:hgnc.symbol:RELA;urn:miriam:uniprot:Q04206;urn:miriam:uniprot:Q04206;urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998"
      hgnc "HGNC_SYMBOL:RELA;HGNC_SYMBOL:NFKB1"
      map_id "M116_14"
      name "P65_slash_P015"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa6"
      uniprot "UNIPROT:Q04206;UNIPROT:P19838"
    ]
    graphics [
      x 1166.6360259433545
      y 164.20021594425134
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780;PUBMED:32172672"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re100"
      uniprot "NA"
    ]
    graphics [
      x 1288.5856479939416
      y 184.8260129271357
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998"
      hgnc "HGNC_SYMBOL:NFKB1"
      map_id "M116_89"
      name "NFKB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa81"
      uniprot "UNIPROT:P19838"
    ]
    graphics [
      x 200.81378310201626
      y 960.9001353596659
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "PUBMED:26999213"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_44"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re93"
      uniprot "NA"
    ]
    graphics [
      x 75.83202893259943
      y 922.5363161361423
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:7797;urn:miriam:refseq:NM_020529;urn:miriam:ensembl:ENSG00000100906;urn:miriam:uniprot:P25963;urn:miriam:uniprot:P25963;urn:miriam:ncbigene:4792;urn:miriam:ncbigene:4792;urn:miriam:hgnc.symbol:NFKBIA;urn:miriam:hgnc.symbol:NFKBIA"
      hgnc "HGNC_SYMBOL:NFKBIA"
      map_id "M116_82"
      name "NFKBIA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa65"
      uniprot "UNIPROT:P25963"
    ]
    graphics [
      x 180.24407755107939
      y 1084.1229968503058
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:CACTIN;urn:miriam:hgnc.symbol:CACTIN;urn:miriam:uniprot:Q8WUQ7;urn:miriam:uniprot:Q8WUQ7;urn:miriam:ensembl:ENSG00000105298;urn:miriam:refseq:NM_001080543;urn:miriam:hgnc:29938;urn:miriam:ncbigene:58509;urn:miriam:ncbigene:58509"
      hgnc "HGNC_SYMBOL:CACTIN"
      map_id "M116_60"
      name "CACTIN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa138"
      uniprot "UNIPROT:Q8WUQ7"
    ]
    graphics [
      x 78.45581412818524
      y 751.5990170289798
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:7797;urn:miriam:refseq:NM_020529;urn:miriam:ensembl:ENSG00000100906;urn:miriam:uniprot:P25963;urn:miriam:uniprot:P25963;urn:miriam:ncbigene:4792;urn:miriam:ncbigene:4792;urn:miriam:hgnc.symbol:NFKBIA;urn:miriam:hgnc.symbol:NFKBIA;urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998"
      hgnc "HGNC_SYMBOL:NFKBIA;HGNC_SYMBOL:NFKB1"
      map_id "M116_10"
      name "NFKB1:NFKNIA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa21"
      uniprot "UNIPROT:P25963;UNIPROT:P19838"
    ]
    graphics [
      x 100.7473098971177
      y 1034.9776757014329
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      annotation "PUBMED:15361868;PUBMED:22539786;PUBMED:20724660"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_39"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re75"
      uniprot "NA"
    ]
    graphics [
      x 931.5877105506477
      y 1015.8955577554016
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:TRIM38;urn:miriam:hgnc.symbol:TRIM38;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:10475;urn:miriam:ncbigene:10475;urn:miriam:ensembl:ENSG00000112343;urn:miriam:refseq:NM_006355;urn:miriam:hgnc:10059;urn:miriam:uniprot:O00635;urn:miriam:uniprot:O00635"
      hgnc "HGNC_SYMBOL:TRIM38"
      map_id "M116_79"
      name "TRIM38"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa52"
      uniprot "UNIPROT:O00635"
    ]
    graphics [
      x 1041.7005112976935
      y 980.2907940977747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:uniprot:P36406;urn:miriam:uniprot:P36406;urn:miriam:ensembl:ENSG00000113595;urn:miriam:hgnc.symbol:TRIM23;urn:miriam:hgnc.symbol:TRIM23;urn:miriam:hgnc:660;urn:miriam:ncbigene:373;urn:miriam:refseq:NM_001656;urn:miriam:ncbigene:373"
      hgnc "HGNC_SYMBOL:TRIM23"
      map_id "M116_84"
      name "TRIM23"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa70"
      uniprot "UNIPROT:P36406"
    ]
    graphics [
      x 982.6180597560217
      y 899.2474478321872
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:7189;urn:miriam:ncbigene:7189;urn:miriam:ensembl:ENSG00000175104;urn:miriam:uniprot:Q9Y4K3;urn:miriam:uniprot:Q9Y4K3;urn:miriam:hgnc:12036;urn:miriam:refseq:NM_145803;urn:miriam:hgnc.symbol:TRAF6;urn:miriam:hgnc.symbol:TRAF6"
      hgnc "HGNC_SYMBOL:TRAF6"
      map_id "M116_72"
      name "TRAF6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa38"
      uniprot "UNIPROT:Q9Y4K3"
    ]
    graphics [
      x 767.4999108995605
      y 1000.1092173931074
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:CACTIN;urn:miriam:hgnc.symbol:CACTIN;urn:miriam:uniprot:Q8WUQ7;urn:miriam:uniprot:Q8WUQ7;urn:miriam:ensembl:ENSG00000105298;urn:miriam:refseq:NM_001080543;urn:miriam:hgnc:29938;urn:miriam:ncbigene:58509;urn:miriam:ncbigene:58509"
      hgnc "HGNC_SYMBOL:CACTIN"
      map_id "M116_88"
      name "CACTIN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa77"
      uniprot "UNIPROT:Q8WUQ7"
    ]
    graphics [
      x 301.76952552116455
      y 555.1402528352075
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "PUBMED:26999213"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_24"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re109"
      uniprot "NA"
    ]
    graphics [
      x 135.16159978708845
      y 593.8985556654135
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:7797;urn:miriam:refseq:NM_020529;urn:miriam:ensembl:ENSG00000100906;urn:miriam:uniprot:P25963;urn:miriam:uniprot:P25963;urn:miriam:ncbigene:4792;urn:miriam:ncbigene:4792;urn:miriam:hgnc.symbol:NFKBIA;urn:miriam:hgnc.symbol:NFKBIA"
      hgnc "HGNC_SYMBOL:NFKBIA"
      map_id "M116_81"
      name "NFKBIA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa64"
      uniprot "UNIPROT:P25963"
    ]
    graphics [
      x 394.5469806595982
      y 1300.089860606126
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      annotation "PUBMED:21135871"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re50"
      uniprot "NA"
    ]
    graphics [
      x 331.0645273789612
      y 1199.5002921587031
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:5960;urn:miriam:hgnc:5961;urn:miriam:hgnc:1974;urn:miriam:hgnc.symbol:CHUK;urn:miriam:hgnc.symbol:CHUK;urn:miriam:refseq:NM_001278;urn:miriam:ec-code:2.7.11.10;urn:miriam:ensembl:ENSG00000213341;urn:miriam:hgnc:1974;urn:miriam:uniprot:O15111;urn:miriam:uniprot:O15111;urn:miriam:ncbigene:1147;urn:miriam:ncbigene:1147;urn:miriam:hgnc:5961;urn:miriam:ensembl:ENSG00000269335;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:refseq:NM_003639;urn:miriam:ncbigene:8517;urn:miriam:ncbigene:8517;urn:miriam:uniprot:Q9Y6K9;urn:miriam:uniprot:Q9Y6K9;urn:miriam:hgnc:5960;urn:miriam:ensembl:ENSG00000104365;urn:miriam:ec-code:2.7.11.10;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:uniprot:O14920;urn:miriam:uniprot:O14920;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:ncbigene:3551;urn:miriam:ncbigene:3551;urn:miriam:refseq:NM_001190720"
      hgnc "HGNC_SYMBOL:CHUK;HGNC_SYMBOL:IKBKG;HGNC_SYMBOL:IKBKB"
      map_id "M116_3"
      name "NEMO_slash_IKKA_slash_IKKB"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa12"
      uniprot "UNIPROT:O15111;UNIPROT:Q9Y6K9;UNIPROT:O14920"
    ]
    graphics [
      x 411.91870661154155
      y 1075.3392800768568
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      annotation "PUBMED:31034780"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_32"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re61"
      uniprot "NA"
    ]
    graphics [
      x 1686.3145727174401
      y 234.01202534594358
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:E;urn:miriam:ncbiprotein:1796318600"
      hgnc "NA"
      map_id "M116_95"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa90"
      uniprot "UNIPROT:E"
    ]
    graphics [
      x 1640.627113425522
      y 140.57928503307664
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:5992;urn:miriam:hgnc.symbol:IL1B;urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:uniprot:P01584;urn:miriam:refseq:NM_000576;urn:miriam:ncbigene:3553;urn:miriam:ncbigene:3553;urn:miriam:ensembl:ENSG00000125538"
      hgnc "HGNC_SYMBOL:IL1B"
      map_id "M116_96"
      name "IL1b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa92"
      uniprot "UNIPROT:P01584"
    ]
    graphics [
      x 1701.4367244257319
      y 353.03085352221126
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:16400;urn:miriam:refseq:NM_004895;urn:miriam:uniprot:Q96P20;urn:miriam:uniprot:Q96P20;urn:miriam:ensembl:ENSG00000162711;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:hgnc.symbol:NLRP3;urn:miriam:ncbigene:114548;urn:miriam:ncbigene:114548"
      hgnc "HGNC_SYMBOL:NLRP3"
      map_id "M116_57"
      name "NLRP3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa134"
      uniprot "UNIPROT:Q96P20"
    ]
    graphics [
      x 1741.877700249469
      y 1026.0112769955063
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      annotation "PUBMED:31231549"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_20"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re104"
      uniprot "NA"
    ]
    graphics [
      x 1785.9856941943144
      y 882.1350803295361
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ncbiprotein:BCD58760"
      hgnc "NA"
      map_id "M116_56"
      name "ORF8b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa132"
      uniprot "NA"
    ]
    graphics [
      x 1690.0057010208866
      y 959.6321899299924
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_63"
      name "s197"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa140"
      uniprot "NA"
    ]
    graphics [
      x 1425.9376991586887
      y 303.78016588042874
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      annotation "PUBMED:15316659;PUBMED:17715238;PUBMED:25375324;PUBMED:19590927"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_26"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re111"
      uniprot "NA"
    ]
    graphics [
      x 1379.7072842056286
      y 407.8773725013669
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:14583;urn:miriam:uniprot:J9TC74;urn:miriam:hgnc:5013;urn:miriam:hgnc:20266;urn:miriam:hgnc:24048;urn:miriam:hgnc:20593;urn:miriam:ncbigene:23155;urn:miriam:ncbigene:23155;urn:miriam:hgnc:29675;urn:miriam:refseq:NM_015127;urn:miriam:hgnc.symbol:CLCC1;urn:miriam:uniprot:Q96S66;urn:miriam:uniprot:Q96S66;urn:miriam:hgnc.symbol:CLCC1;urn:miriam:ensembl:ENSG00000121940;urn:miriam:uniprot:Q9UH99;urn:miriam:uniprot:Q9UH99;urn:miriam:hgnc:14210;urn:miriam:ensembl:ENSG00000100242;urn:miriam:refseq:NM_001199579;urn:miriam:ncbigene:25777;urn:miriam:ncbigene:25777;urn:miriam:hgnc.symbol:SUN2;urn:miriam:hgnc.symbol:SUN2;urn:miriam:ncbigene:151188;urn:miriam:ncbigene:151188;urn:miriam:hgnc:24048;urn:miriam:hgnc.symbol:ARL6IP6;urn:miriam:hgnc.symbol:ARL6IP6;urn:miriam:uniprot:Q8N6S5;urn:miriam:uniprot:Q8N6S5;urn:miriam:refseq:NM_152522;urn:miriam:ensembl:ENSG00000177917;urn:miriam:ensembl:ENSG00000100292;urn:miriam:hgnc:5013;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:hgnc.symbol:HMOX1;urn:miriam:refseq:NM_002133;urn:miriam:ncbigene:3162;urn:miriam:uniprot:P09601;urn:miriam:uniprot:P09601;urn:miriam:ncbigene:3162;urn:miriam:ec-code:1.14.14.18;urn:miriam:uniprot:Q96JC1;urn:miriam:uniprot:Q96JC1;urn:miriam:hgnc.symbol:VPS39;urn:miriam:refseq:NM_015289;urn:miriam:hgnc.symbol:VPS39;urn:miriam:ensembl:ENSG00000166887;urn:miriam:ncbigene:23339;urn:miriam:ncbigene:23339;urn:miriam:hgnc:20593;urn:miriam:hgnc:14583;urn:miriam:ncbigene:55823;urn:miriam:refseq:NM_021729;urn:miriam:ncbigene:55823;urn:miriam:ensembl:ENSG00000160695;urn:miriam:hgnc.symbol:VPS11;urn:miriam:hgnc.symbol:VPS11;urn:miriam:uniprot:Q9H270;urn:miriam:uniprot:Q9H270;urn:miriam:hgnc.symbol:ALG5;urn:miriam:hgnc.symbol:ALG5;urn:miriam:ensembl:ENSG00000120697;urn:miriam:hgnc:20266;urn:miriam:ncbigene:29880;urn:miriam:ncbigene:29880;urn:miriam:uniprot:Q9Y673;urn:miriam:uniprot:Q9Y673;urn:miriam:ec-code:2.4.1.117;urn:miriam:refseq:NM_013338"
      hgnc "HGNC_SYMBOL:CLCC1;HGNC_SYMBOL:SUN2;HGNC_SYMBOL:ARL6IP6;HGNC_SYMBOL:HMOX1;HGNC_SYMBOL:VPS39;HGNC_SYMBOL:VPS11;HGNC_SYMBOL:ALG5"
      map_id "M116_8"
      name "Hops_space_Complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa2"
      uniprot "UNIPROT:J9TC74;UNIPROT:Q96S66;UNIPROT:Q9UH99;UNIPROT:Q8N6S5;UNIPROT:P09601;UNIPROT:Q96JC1;UNIPROT:Q9H270;UNIPROT:Q9Y673"
    ]
    graphics [
      x 1440.7443345165457
      y 504.8439005012464
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:15631;urn:miriam:hgnc:15632;urn:miriam:hgnc:15633;urn:miriam:refseq:NM_016610;urn:miriam:uniprot:Q9NR97;urn:miriam:uniprot:Q9NR97;urn:miriam:hgnc:15632;urn:miriam:ncbigene:51311;urn:miriam:ncbigene:51311;urn:miriam:hgnc.symbol:TLR8;urn:miriam:ensembl:ENSG00000101916;urn:miriam:hgnc.symbol:TLR8;urn:miriam:uniprot:Q9NR96;urn:miriam:uniprot:Q9NR96;urn:miriam:ncbigene:54106;urn:miriam:ncbigene:54106;urn:miriam:ensembl:ENSG00000239732;urn:miriam:hgnc.symbol:TLR9;urn:miriam:hgnc.symbol:TLR9;urn:miriam:refseq:NM_017442;urn:miriam:hgnc:15633;urn:miriam:uniprot:Q9NYK1;urn:miriam:uniprot:Q9NYK1;urn:miriam:hgnc:15631;urn:miriam:refseq:NM_016562;urn:miriam:hgnc.symbol:TLR7;urn:miriam:hgnc.symbol:TLR7;urn:miriam:ensembl:ENSG00000196664;urn:miriam:ncbigene:51284;urn:miriam:ncbigene:51284"
      hgnc "HGNC_SYMBOL:TLR8;HGNC_SYMBOL:TLR9;HGNC_SYMBOL:TLR7"
      map_id "M116_17"
      name "TLR7_slash_8_slash_9"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa9"
      uniprot "UNIPROT:Q9NR97;UNIPROT:Q9NR96;UNIPROT:Q9NYK1"
    ]
    graphics [
      x 852.4109968809282
      y 1623.6412105925633
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      annotation "PUBMED:21782231"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_37"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re72"
      uniprot "NA"
    ]
    graphics [
      x 745.9598438573247
      y 1576.8193397780424
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_74"
      name "ssRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa42"
      uniprot "NA"
    ]
    graphics [
      x 729.3190599075494
      y 1692.3734340677486
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:6859;urn:miriam:hgnc:30681;urn:miriam:hgnc:17075;urn:miriam:hgnc.symbol:TAB2;urn:miriam:ensembl:ENSG00000055208;urn:miriam:hgnc.symbol:TAB2;urn:miriam:uniprot:Q9NYJ8;urn:miriam:uniprot:Q9NYJ8;urn:miriam:hgnc:17075;urn:miriam:ncbigene:23118;urn:miriam:refseq:NM_001292034;urn:miriam:ncbigene:23118;urn:miriam:uniprot:O43318;urn:miriam:uniprot:O43318;urn:miriam:ensembl:ENSG00000135341;urn:miriam:refseq:NM_145331;urn:miriam:hgnc:6859;urn:miriam:ncbigene:6885;urn:miriam:ncbigene:6885;urn:miriam:hgnc.symbol:MAP3K7;urn:miriam:hgnc.symbol:MAP3K7;urn:miriam:ec-code:2.7.11.25;urn:miriam:ensembl:ENSG00000157625;urn:miriam:ncbigene:257397;urn:miriam:ncbigene:257397;urn:miriam:hgnc.symbol:TAB3;urn:miriam:hgnc.symbol:TAB3;urn:miriam:uniprot:Q8N5C8;urn:miriam:uniprot:Q8N5C8;urn:miriam:hgnc:30681;urn:miriam:refseq:NM_152787"
      hgnc "HGNC_SYMBOL:TAB2;HGNC_SYMBOL:MAP3K7;HGNC_SYMBOL:TAB3"
      map_id "M116_7"
      name "TAB2_slash_TAB3_slash_TAK1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa19"
      uniprot "UNIPROT:Q9NYJ8;UNIPROT:O43318;UNIPROT:Q8N5C8"
    ]
    graphics [
      x 482.7748701159832
      y 968.1208702733168
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      annotation "PUBMED:18345001;PUBMED:25172371;PUBMED:23758787"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_40"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re88"
      uniprot "NA"
    ]
    graphics [
      x 548.426930823847
      y 1062.4970859343748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:uniprot:P15533;urn:miriam:hgnc:10059;urn:miriam:hgnc.symbol:TRIM38;urn:miriam:hgnc.symbol:TRIM38;urn:miriam:ec-code:2.3.2.27;urn:miriam:ncbigene:10475;urn:miriam:ncbigene:10475;urn:miriam:ensembl:ENSG00000112343;urn:miriam:refseq:NM_006355;urn:miriam:hgnc:10059;urn:miriam:uniprot:O00635;urn:miriam:uniprot:O00635"
      hgnc "HGNC_SYMBOL:TRIM38"
      map_id "M116_2"
      name "TRIM30a_slash_TRIM38"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa11"
      uniprot "UNIPROT:P15533;UNIPROT:O00635"
    ]
    graphics [
      x 436.88634138279673
      y 1017.8486054044365
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:5960;urn:miriam:hgnc:5961;urn:miriam:hgnc:1974;urn:miriam:hgnc.symbol:CHUK;urn:miriam:hgnc.symbol:CHUK;urn:miriam:refseq:NM_001278;urn:miriam:ec-code:2.7.11.10;urn:miriam:ensembl:ENSG00000213341;urn:miriam:hgnc:1974;urn:miriam:uniprot:O15111;urn:miriam:uniprot:O15111;urn:miriam:ncbigene:1147;urn:miriam:ncbigene:1147;urn:miriam:hgnc:5961;urn:miriam:ensembl:ENSG00000269335;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:hgnc.symbol:IKBKG;urn:miriam:refseq:NM_003639;urn:miriam:ncbigene:8517;urn:miriam:ncbigene:8517;urn:miriam:uniprot:Q9Y6K9;urn:miriam:uniprot:Q9Y6K9;urn:miriam:hgnc:5960;urn:miriam:ensembl:ENSG00000104365;urn:miriam:ec-code:2.7.11.10;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:uniprot:O14920;urn:miriam:uniprot:O14920;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:ncbigene:3551;urn:miriam:ncbigene:3551;urn:miriam:refseq:NM_001190720"
      hgnc "HGNC_SYMBOL:CHUK;HGNC_SYMBOL:IKBKG;HGNC_SYMBOL:IKBKB"
      map_id "M116_9"
      name "NEMO_slash_IKKA_slash_IKKB"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa20"
      uniprot "UNIPROT:O15111;UNIPROT:Q9Y6K9;UNIPROT:O14920"
    ]
    graphics [
      x 372.07728491644446
      y 782.1162298883241
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      annotation "PUBMED:27695001;PUBMED:26358190;PUBMED:23408607;PUBMED:23758787;PUBMED:24379373;PUBMED:20724660"
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M116_41"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re89"
      uniprot "NA"
    ]
    graphics [
      x 432.677912722993
      y 883.396131701074
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:hgnc:16379;urn:miriam:uniprot:Q8IYM9;urn:miriam:uniprot:Q8IYM9;urn:miriam:ensembl:ENSG00000132274;urn:miriam:refseq:NM_006074;urn:miriam:ncbigene:10346;urn:miriam:ncbigene:10346;urn:miriam:hgnc.symbol:TRIM22;urn:miriam:hgnc.symbol:TRIM22"
      hgnc "HGNC_SYMBOL:TRIM22"
      map_id "M116_80"
      name "TRIM22"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa56"
      uniprot "UNIPROT:Q8IYM9"
    ]
    graphics [
      x 544.5163547012485
      y 900.9610663613189
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:ncbigene:23650;urn:miriam:ncbigene:23650;urn:miriam:hgnc.symbol:TRIM29;urn:miriam:refseq:NM_012101;urn:miriam:uniprot:Q14134;urn:miriam:uniprot:Q14134;urn:miriam:hgnc.symbol:TRIM29;urn:miriam:ensembl:ENSG00000137699;urn:miriam:hgnc:17274"
      hgnc "HGNC_SYMBOL:TRIM29"
      map_id "M116_83"
      name "TRIM29"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa67"
      uniprot "UNIPROT:Q14134"
    ]
    graphics [
      x 566.6040983621668
      y 828.8088967179835
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:16283;urn:miriam:hgnc:660;urn:miriam:uniprot:Q14142;urn:miriam:uniprot:Q14142;urn:miriam:hgnc.symbol:TRIM14;urn:miriam:ensembl:ENSG00000106785;urn:miriam:hgnc.symbol:TRIM14;urn:miriam:ncbigene:9830;urn:miriam:refseq:NM_014788;urn:miriam:ncbigene:9830;urn:miriam:hgnc:16283;urn:miriam:ec-code:2.3.2.27;urn:miriam:uniprot:P36406;urn:miriam:uniprot:P36406;urn:miriam:ensembl:ENSG00000113595;urn:miriam:hgnc.symbol:TRIM23;urn:miriam:hgnc.symbol:TRIM23;urn:miriam:hgnc:660;urn:miriam:ncbigene:373;urn:miriam:refseq:NM_001656;urn:miriam:ncbigene:373"
      hgnc "HGNC_SYMBOL:TRIM14;HGNC_SYMBOL:TRIM23"
      map_id "M116_5"
      name "TRIM14_slash_TRIM23"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa15"
      uniprot "UNIPROT:Q14142;UNIPROT:P36406"
    ]
    graphics [
      x 534.4936830716265
      y 757.334423538615
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Orf3a protein interactions"
      full_annotation "urn:miriam:hgnc:9975;urn:miriam:hgnc:11312;urn:miriam:hgnc.symbol:TRIM27;urn:miriam:ensembl:ENSG00000204713;urn:miriam:hgnc.symbol:TRIM27;urn:miriam:hgnc:9975;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_030950;urn:miriam:ncbigene:5987;urn:miriam:uniprot:P14373;urn:miriam:uniprot:P14373;urn:miriam:ncbigene:5987;urn:miriam:uniprot:P19474;urn:miriam:uniprot:P19474;urn:miriam:ncbigene:6737;urn:miriam:ncbigene:6737;urn:miriam:ec-code:2.3.2.27;urn:miriam:hgnc:11312;urn:miriam:refseq:NM_003141;urn:miriam:hgnc.symbol:TRIM21;urn:miriam:hgnc.symbol:TRIM21;urn:miriam:ensembl:ENSG00000132109"
      hgnc "HGNC_SYMBOL:TRIM27;HGNC_SYMBOL:TRIM21"
      map_id "M116_4"
      name "TRIM27_slash_TRIM21"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa13"
      uniprot "UNIPROT:P14373;UNIPROT:P19474"
    ]
    graphics [
      x 442.220647373341
      y 760.0868199317059
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M116_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 100
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_75"
      target_id "M116_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 3
    target 2
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_73"
      target_id "M116_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 2
    target 4
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_34"
      target_id "M116_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 5
    target 6
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_65"
      target_id "M116_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 7
    target 6
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_64"
      target_id "M116_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 8
    target 6
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_66"
      target_id "M116_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 106
    source 6
    target 9
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_27"
      target_id "M116_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 107
    source 10
    target 11
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_53"
      target_id "M116_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 108
    source 12
    target 11
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_47"
      target_id "M116_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 109
    source 11
    target 13
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_46"
      target_id "M116_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 110
    source 14
    target 15
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_58"
      target_id "M116_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 111
    source 9
    target 15
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CATALYSIS"
      source_id "M116_16"
      target_id "M116_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 112
    source 15
    target 16
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_30"
      target_id "M116_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 113
    source 17
    target 18
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_15"
      target_id "M116_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 114
    source 18
    target 19
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_22"
      target_id "M116_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 20
    target 21
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_11"
      target_id "M116_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 22
    target 21
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CATALYSIS"
      source_id "M116_12"
      target_id "M116_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 21
    target 23
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_45"
      target_id "M116_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 24
    target 25
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_59"
      target_id "M116_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 26
    target 25
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "INHIBITION"
      source_id "M116_85"
      target_id "M116_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 25
    target 27
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_21"
      target_id "M116_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 28
    target 29
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_98"
      target_id "M116_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 4
    target 29
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_97"
      target_id "M116_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 30
    target 29
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "INHIBITION"
      source_id "M116_78"
      target_id "M116_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 29
    target 31
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_35"
      target_id "M116_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 32
    target 33
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_49"
      target_id "M116_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 126
    source 34
    target 33
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_6"
      target_id "M116_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 127
    source 33
    target 35
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_38"
      target_id "M116_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 128
    source 36
    target 37
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_87"
      target_id "M116_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 37
    target 38
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_25"
      target_id "M116_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 130
    source 39
    target 40
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_90"
      target_id "M116_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 131
    source 41
    target 40
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_62"
      target_id "M116_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 132
    source 42
    target 40
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_48"
      target_id "M116_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 133
    source 40
    target 43
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_29"
      target_id "M116_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 134
    source 23
    target 44
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_13"
      target_id "M116_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 135
    source 45
    target 44
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_91"
      target_id "M116_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 136
    source 44
    target 17
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_23"
      target_id "M116_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 137
    source 46
    target 47
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_55"
      target_id "M116_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 138
    source 48
    target 47
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CATALYSIS"
      source_id "M116_51"
      target_id "M116_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 139
    source 12
    target 47
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_47"
      target_id "M116_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 47
    target 41
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_19"
      target_id "M116_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 49
    target 50
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_92"
      target_id "M116_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 51
    target 50
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_1"
      target_id "M116_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 50
    target 52
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_42"
      target_id "M116_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 144
    source 53
    target 54
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_94"
      target_id "M116_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 145
    source 52
    target 54
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M116_93"
      target_id "M116_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 146
    source 54
    target 55
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_33"
      target_id "M116_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 147
    source 56
    target 57
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_52"
      target_id "M116_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 148
    source 31
    target 57
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_76"
      target_id "M116_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 149
    source 57
    target 48
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_43"
      target_id "M116_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 150
    source 58
    target 59
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_67"
      target_id "M116_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 151
    source 60
    target 59
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "INHIBITION"
      source_id "M116_70"
      target_id "M116_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 152
    source 59
    target 8
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_28"
      target_id "M116_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 153
    source 61
    target 62
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_99"
      target_id "M116_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 154
    source 12
    target 62
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_47"
      target_id "M116_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 155
    source 62
    target 60
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_36"
      target_id "M116_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 156
    source 63
    target 64
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_14"
      target_id "M116_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 157
    source 48
    target 64
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CATALYSIS"
      source_id "M116_51"
      target_id "M116_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 158
    source 23
    target 64
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CATALYSIS"
      source_id "M116_13"
      target_id "M116_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 159
    source 12
    target 64
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_47"
      target_id "M116_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 160
    source 64
    target 17
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_18"
      target_id "M116_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 161
    source 65
    target 66
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_89"
      target_id "M116_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 162
    source 67
    target 66
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_82"
      target_id "M116_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 163
    source 68
    target 66
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "INHIBITION"
      source_id "M116_60"
      target_id "M116_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 164
    source 66
    target 69
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_44"
      target_id "M116_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 165
    source 16
    target 70
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_50"
      target_id "M116_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 166
    source 35
    target 70
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_77"
      target_id "M116_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 167
    source 71
    target 70
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "INHIBITION"
      source_id "M116_79"
      target_id "M116_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 168
    source 72
    target 70
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_84"
      target_id "M116_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 169
    source 70
    target 73
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_39"
      target_id "M116_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 170
    source 74
    target 75
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_88"
      target_id "M116_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 171
    source 38
    target 75
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_61"
      target_id "M116_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 172
    source 75
    target 68
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_24"
      target_id "M116_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 173
    source 76
    target 77
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_81"
      target_id "M116_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 174
    source 27
    target 77
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CATALYSIS"
      source_id "M116_86"
      target_id "M116_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 175
    source 78
    target 77
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CATALYSIS"
      source_id "M116_3"
      target_id "M116_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 176
    source 77
    target 67
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_31"
      target_id "M116_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 177
    source 19
    target 79
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_54"
      target_id "M116_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 178
    source 43
    target 79
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CATALYSIS"
      source_id "M116_69"
      target_id "M116_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 179
    source 80
    target 79
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CATALYSIS"
      source_id "M116_95"
      target_id "M116_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 180
    source 17
    target 79
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M116_15"
      target_id "M116_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 181
    source 79
    target 81
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_32"
      target_id "M116_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 182
    source 82
    target 83
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_57"
      target_id "M116_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 84
    target 83
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_56"
      target_id "M116_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 83
    target 42
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_20"
      target_id "M116_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 85
    target 86
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_63"
      target_id "M116_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 12
    target 86
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_47"
      target_id "M116_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 86
    target 87
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_26"
      target_id "M116_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 88
    target 89
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_17"
      target_id "M116_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 90
    target 89
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_74"
      target_id "M116_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 89
    target 34
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_37"
      target_id "M116_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 91
    target 92
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_7"
      target_id "M116_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 93
    target 92
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "INHIBITION"
      source_id "M116_2"
      target_id "M116_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 73
    target 92
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_72"
      target_id "M116_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 92
    target 51
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_40"
      target_id "M116_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 94
    target 95
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M116_9"
      target_id "M116_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 51
    target 95
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_1"
      target_id "M116_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 96
    target 95
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "INHIBITION"
      source_id "M116_80"
      target_id "M116_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 97
    target 95
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "INHIBITION"
      source_id "M116_83"
      target_id "M116_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 98
    target 95
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "TRIGGER"
      source_id "M116_5"
      target_id "M116_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 99
    target 95
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "INHIBITION"
      source_id "M116_4"
      target_id "M116_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 95
    target 78
    cd19dm [
      diagram "C19DMap:Orf3a protein interactions"
      edge_type "PRODUCTION"
      source_id "M116_41"
      target_id "M116_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
