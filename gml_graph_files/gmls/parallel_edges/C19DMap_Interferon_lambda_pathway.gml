# generated with VANTED V2.8.2 at Fri Mar 04 09:57:02 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_9"
      name "p38_minus_NFkB"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa20"
      uniprot "NA"
    ]
    graphics [
      x 1605.837720559597
      y 1520.3901558075072
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:25045870"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_51"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re43"
      uniprot "NA"
    ]
    graphics [
      x 1714.5440564526489
      y 1496.127835840838
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:Q14653;urn:miriam:hgnc:6118;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661;urn:miriam:ncbigene:3661"
      hgnc "HGNC_SYMBOL:IRF3"
      map_id "M120_117"
      name "IRF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa89"
      uniprot "UNIPROT:Q14653"
    ]
    graphics [
      x 1627.275956335698
      y 1398.8938225859054
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:Q14653;urn:miriam:hgnc:6118;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661;urn:miriam:ncbigene:3661"
      hgnc "HGNC_SYMBOL:IRF3"
      map_id "M120_20"
      name "p38_minus_NFkB"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa41"
      uniprot "UNIPROT:Q14653"
    ]
    graphics [
      x 1708.5951303934085
      y 1343.2588902417497
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:hgnc.symbol:OAS2;urn:miriam:hgnc.symbol:OAS2;urn:miriam:uniprot:P29728;urn:miriam:uniprot:P29728;urn:miriam:ncbigene:4939;urn:miriam:ncbigene:4939;urn:miriam:hgnc:8087;urn:miriam:ensembl:ENSG00000111335;urn:miriam:refseq:NM_001032731;urn:miriam:ec-code:2.7.7.84;urn:miriam:hgnc.symbol:OAS3;urn:miriam:hgnc.symbol:OAS3;urn:miriam:uniprot:Q9Y6K5;urn:miriam:uniprot:Q9Y6K5;urn:miriam:ensembl:ENSG00000111331;urn:miriam:refseq:NM_006187;urn:miriam:ec-code:2.7.7.84;urn:miriam:hgnc:8088;urn:miriam:ncbigene:4940;urn:miriam:ncbigene:4940;urn:miriam:ensembl:ENSG00000185745;urn:miriam:hgnc:5407;urn:miriam:hgnc.symbol:IFIT1;urn:miriam:uniprot:P09914;urn:miriam:uniprot:P09914;urn:miriam:hgnc.symbol:IFIT1;urn:miriam:refseq:NM_001548;urn:miriam:ncbigene:3434;urn:miriam:ncbigene:3434;urn:miriam:refseq:NM_080657;urn:miriam:hgnc:30908;urn:miriam:ncbigene:91543;urn:miriam:ncbigene:91543;urn:miriam:hgnc.symbol:RSAD2;urn:miriam:ensembl:ENSG00000134321;urn:miriam:hgnc.symbol:RSAD2;urn:miriam:uniprot:Q8WXG1;urn:miriam:uniprot:Q8WXG1;urn:miriam:uniprot:P00973;urn:miriam:uniprot:P00973;urn:miriam:ncbigene:4938;urn:miriam:ncbigene:4938;urn:miriam:refseq:NM_001032409;urn:miriam:hgnc.symbol:OAS1;urn:miriam:hgnc.symbol:OAS1;urn:miriam:hgnc:8086;urn:miriam:ec-code:2.7.7.84;urn:miriam:ensembl:ENSG00000089127;urn:miriam:ensembl:ENSG00000126709;urn:miriam:ncbigene:2537;urn:miriam:ncbigene:2537;urn:miriam:refseq:NM_022873;urn:miriam:uniprot:P09912;urn:miriam:uniprot:P09912;urn:miriam:hgnc.symbol:IFI6;urn:miriam:hgnc.symbol:IFI6;urn:miriam:hgnc:4054;urn:miriam:uniprot:Q92985;urn:miriam:uniprot:Q92985;urn:miriam:ensembl:ENSG00000185507;urn:miriam:refseq:NM_001572;urn:miriam:hgnc.symbol:IRF7;urn:miriam:hgnc.symbol:IRF7;urn:miriam:hgnc:6122;urn:miriam:ncbigene:3665;urn:miriam:ncbigene:3665;urn:miriam:ncbigene:3659;urn:miriam:ncbigene:3659;urn:miriam:hgnc:6116;urn:miriam:hgnc.symbol:IRF1;urn:miriam:hgnc.symbol:IRF1;urn:miriam:refseq:NM_002198;urn:miriam:uniprot:P10914;urn:miriam:uniprot:P10914;urn:miriam:ensembl:ENSG00000125347;urn:miriam:hgnc:9437;urn:miriam:ec-code:2.7.11.1;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc.symbol:EIF2AK2;urn:miriam:hgnc.symbol:EIF2AK2;urn:miriam:uniprot:P19525;urn:miriam:uniprot:P19525;urn:miriam:refseq:NM_002759;urn:miriam:ncbigene:5610;urn:miriam:ensembl:ENSG00000055332;urn:miriam:ncbigene:5610"
      hgnc "HGNC_SYMBOL:OAS2;HGNC_SYMBOL:OAS3;HGNC_SYMBOL:IFIT1;HGNC_SYMBOL:RSAD2;HGNC_SYMBOL:OAS1;HGNC_SYMBOL:IFI6;HGNC_SYMBOL:IRF7;HGNC_SYMBOL:IRF1;HGNC_SYMBOL:EIF2AK2"
      map_id "M120_26"
      name "antiviral_space_proteins"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa58"
      uniprot "UNIPROT:P29728;UNIPROT:Q9Y6K5;UNIPROT:P09914;UNIPROT:Q8WXG1;UNIPROT:P00973;UNIPROT:P09912;UNIPROT:Q92985;UNIPROT:P10914;UNIPROT:P19525"
    ]
    graphics [
      x 186.46846632987422
      y 1056.600921608691
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_31"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re11"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 1091.5434481423972
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_27"
      name "antiviral_space_proteins"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa59"
      uniprot "NA"
    ]
    graphics [
      x 113.05956697615102
      y 1198.163799487654
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_94"
      name "ISGs"
      node_subtype "RNA"
      node_type "species"
      org_id "sa15"
      uniprot "NA"
    ]
    graphics [
      x 610.4934651089384
      y 847.9732351558606
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_74"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re7"
      uniprot "NA"
    ]
    graphics [
      x 500.3926172482337
      y 902.204206702652
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_92"
      name "ISGs"
      node_subtype "RNA"
      node_type "species"
      org_id "sa14"
      uniprot "NA"
    ]
    graphics [
      x 407.72686330446675
      y 994.867877462406
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:Q14653;urn:miriam:hgnc:6118;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661;urn:miriam:ncbigene:3661"
      hgnc "HGNC_SYMBOL:IRF3"
      map_id "M120_115"
      name "IRF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa87"
      uniprot "UNIPROT:Q14653"
    ]
    graphics [
      x 1924.7674160746838
      y 961.7026932316919
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "PUBMED:25636800"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_40"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re28"
      uniprot "NA"
    ]
    graphics [
      x 1927.3422768983082
      y 816.9255889873913
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:hgnc.symbol:CHUK;urn:miriam:ec-code:2.7.11.10;urn:miriam:ec-code:2.7.11.1;urn:miriam:uniprot:O14920;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:ncbigene:3551;urn:miriam:uniprot:O15111;urn:miriam:ncbigene:1147;urn:miriam:ncbigene:23586;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:hgnc.symbol:DDX58;urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110;urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746"
      hgnc "HGNC_SYMBOL:CHUK;HGNC_SYMBOL:IKBKB;HGNC_SYMBOL:DDX58;HGNC_SYMBOL:TBK1;HGNC_SYMBOL:MAVS"
      map_id "M120_7"
      name "RIG_minus_I_minus_MAVS"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa17"
      uniprot "UNIPROT:O14920;UNIPROT:O15111;UNIPROT:O95786;UNIPROT:Q9UHD2;UNIPROT:Q7Z434"
    ]
    graphics [
      x 1978.0659749986476
      y 706.33598021081
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:Q14653;urn:miriam:hgnc:6118;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661;urn:miriam:ncbigene:3661"
      hgnc "HGNC_SYMBOL:IRF3"
      map_id "M120_116"
      name "IRF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa88"
      uniprot "UNIPROT:Q14653"
    ]
    graphics [
      x 1798.3926478889825
      y 860.3987983924394
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ncbigene:163702;urn:miriam:ncbigene:163702;urn:miriam:hgnc.symbol:IFNLR1;urn:miriam:uniprot:Q8IU57;urn:miriam:uniprot:Q8IU57;urn:miriam:hgnc.symbol:IFNLR1;urn:miriam:ensembl:ENSG00000185436;urn:miriam:hgnc:18584;urn:miriam:refseq:NM_170743;urn:miriam:hgnc.symbol:JAK1;urn:miriam:hgnc.symbol:JAK1;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc:6190;urn:miriam:ncbigene:3716;urn:miriam:ncbigene:3716;urn:miriam:ensembl:ENSG00000162434;urn:miriam:uniprot:P23458;urn:miriam:uniprot:P23458;urn:miriam:refseq:NM_002227;urn:miriam:ec-code:2.7.10.2;urn:miriam:ncbigene:7297;urn:miriam:ncbigene:7297;urn:miriam:ensembl:ENSG00000105397;urn:miriam:refseq:NM_001385197;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc:12440;urn:miriam:uniprot:P29597;urn:miriam:uniprot:P29597"
      hgnc "HGNC_SYMBOL:IFNLR1;HGNC_SYMBOL:JAK1;HGNC_SYMBOL:TYK2"
      map_id "M120_1"
      name "receptor_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa10"
      uniprot "UNIPROT:Q8IU57;UNIPROT:P23458;UNIPROT:P29597"
    ]
    graphics [
      x 684.9685810635546
      y 204.82054186517223
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_71"
      name "NA"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "re66"
      uniprot "NA"
    ]
    graphics [
      x 782.7558334740384
      y 69.37651342389972
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:Q8IZI9;urn:miriam:uniprot:Q8IZI9;urn:miriam:refseq:NM_172139;urn:miriam:ncbigene:282617;urn:miriam:ncbigene:282617;urn:miriam:hgnc.symbol:IFNL3;urn:miriam:hgnc.symbol:IFNL3;urn:miriam:hgnc:18365;urn:miriam:ensembl:ENSG00000197110;urn:miriam:refseq:NM_172138;urn:miriam:hgnc.symbol:IFNL2;urn:miriam:uniprot:Q8IZJ0;urn:miriam:uniprot:Q8IZJ0;urn:miriam:hgnc.symbol:IFNL2;urn:miriam:hgnc:18364;urn:miriam:ncbigene:282616;urn:miriam:ensembl:ENSG00000183709;urn:miriam:ncbigene:282616;urn:miriam:uniprot:Q8IU54;urn:miriam:uniprot:Q8IU54;urn:miriam:ncbigene:282618;urn:miriam:refseq:NM_172140;urn:miriam:ncbigene:282618;urn:miriam:ensembl:ENSG00000182393;urn:miriam:hgnc.symbol:IFNL1;urn:miriam:hgnc.symbol:IFNL1;urn:miriam:hgnc:18363;urn:miriam:ensembl:ENSG00000272395;urn:miriam:hgnc.symbol:IFNL4;urn:miriam:hgnc.symbol:IFNL4;urn:miriam:hgnc:44480;urn:miriam:ncbigene:101180976;urn:miriam:uniprot:K9M1U5;urn:miriam:uniprot:K9M1U5;urn:miriam:ncbigene:101180976;urn:miriam:refseq:NM_001276254"
      hgnc "HGNC_SYMBOL:IFNL3;HGNC_SYMBOL:IFNL2;HGNC_SYMBOL:IFNL1;HGNC_SYMBOL:IFNL4"
      map_id "M120_28"
      name "Ifn_space_lambda"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa60"
      uniprot "UNIPROT:Q8IZI9;UNIPROT:Q8IZJ0;UNIPROT:Q8IU54;UNIPROT:K9M1U5"
    ]
    graphics [
      x 755.8160517025879
      y 197.34396000111576
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:hgnc.symbol:JAK1;urn:miriam:hgnc.symbol:JAK1;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc:6190;urn:miriam:ncbigene:3716;urn:miriam:ncbigene:3716;urn:miriam:ensembl:ENSG00000162434;urn:miriam:uniprot:P23458;urn:miriam:uniprot:P23458;urn:miriam:refseq:NM_002227;urn:miriam:ec-code:2.7.10.2;urn:miriam:ncbigene:7297;urn:miriam:ncbigene:7297;urn:miriam:ensembl:ENSG00000105397;urn:miriam:refseq:NM_001385197;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc:12440;urn:miriam:uniprot:P29597;urn:miriam:uniprot:P29597;urn:miriam:ncbigene:163702;urn:miriam:ncbigene:163702;urn:miriam:hgnc.symbol:IFNLR1;urn:miriam:uniprot:Q8IU57;urn:miriam:uniprot:Q8IU57;urn:miriam:hgnc.symbol:IFNLR1;urn:miriam:ensembl:ENSG00000185436;urn:miriam:hgnc:18584;urn:miriam:refseq:NM_170743"
      hgnc "HGNC_SYMBOL:JAK1;HGNC_SYMBOL:TYK2;HGNC_SYMBOL:IFNLR1"
      map_id "M120_29"
      name "receptor_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa9"
      uniprot "UNIPROT:P23458;UNIPROT:P29597;UNIPROT:Q8IU57"
    ]
    graphics [
      x 931.6787836777394
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ncbigene:3659;urn:miriam:ncbigene:3659;urn:miriam:hgnc:6116;urn:miriam:hgnc.symbol:IRF1;urn:miriam:hgnc.symbol:IRF1;urn:miriam:refseq:NM_002198;urn:miriam:uniprot:P10914;urn:miriam:uniprot:P10914;urn:miriam:ensembl:ENSG00000125347"
      hgnc "HGNC_SYMBOL:IRF1"
      map_id "M120_122"
      name "IRF1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa94"
      uniprot "UNIPROT:P10914"
    ]
    graphics [
      x 622.406864075579
      y 444.6284055883083
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "PUBMED:25045870"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_43"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re32"
      uniprot "NA"
    ]
    graphics [
      x 708.3194708470319
      y 556.0904284248802
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ncbigene:23586;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:hgnc.symbol:DDX58;urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746"
      hgnc "HGNC_SYMBOL:DDX58;HGNC_SYMBOL:MAVS"
      map_id "M120_10"
      name "RIG_minus_I_minus_MAVS"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa21"
      uniprot "UNIPROT:O95786;UNIPROT:Q7Z434"
    ]
    graphics [
      x 641.3765596322819
      y 505.86897135757283
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ncbigene:3659;urn:miriam:ncbigene:3659;urn:miriam:hgnc:6116;urn:miriam:hgnc.symbol:IRF1;urn:miriam:hgnc.symbol:IRF1;urn:miriam:refseq:NM_002198;urn:miriam:uniprot:P10914;urn:miriam:uniprot:P10914;urn:miriam:ensembl:ENSG00000125347"
      hgnc "HGNC_SYMBOL:IRF1"
      map_id "M120_123"
      name "IRF1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa95"
      uniprot "UNIPROT:P10914"
    ]
    graphics [
      x 847.3181328741717
      y 692.9726787043899
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_56"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re48"
      uniprot "NA"
    ]
    graphics [
      x 1003.1375022551498
      y 809.8518894623592
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ncbigene:3659;urn:miriam:ncbigene:3659;urn:miriam:hgnc:6116;urn:miriam:hgnc.symbol:IRF1;urn:miriam:hgnc.symbol:IRF1;urn:miriam:refseq:NM_002198;urn:miriam:uniprot:P10914;urn:miriam:uniprot:P10914;urn:miriam:ensembl:ENSG00000125347"
      hgnc "HGNC_SYMBOL:IRF1"
      map_id "M120_86"
      name "IRF1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa127"
      uniprot "UNIPROT:P10914"
    ]
    graphics [
      x 1157.8950561769404
      y 940.6977882297858
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_22"
      name "ribosomal_space_60S_space_subunit"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa47"
      uniprot "NA"
    ]
    graphics [
      x 658.4730659457025
      y 1219.1445257061428
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "PUBMED:32680882"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_59"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re52"
      uniprot "NA"
    ]
    graphics [
      x 683.8938528027658
      y 1117.0214501674911
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_23"
      name "ribosomal_space_40S_space_subunit"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa48"
      uniprot "NA"
    ]
    graphics [
      x 861.9365706646822
      y 1165.4471034536034
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_21"
      name "80S_space_ribosome"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa44"
      uniprot "NA"
    ]
    graphics [
      x 545.818072917308
      y 1065.2923154670232
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:P52630;urn:miriam:uniprot:P52630;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc:11363;urn:miriam:ncbigene:6773;urn:miriam:ncbigene:6773;urn:miriam:refseq:NM_005419;urn:miriam:ensembl:ENSG00000170581;urn:miriam:refseq:NM_007315;urn:miriam:hgnc.symbol:STAT1;urn:miriam:hgnc.symbol:STAT1;urn:miriam:uniprot:P42224;urn:miriam:uniprot:P42224;urn:miriam:ncbigene:6772;urn:miriam:ncbigene:6772;urn:miriam:hgnc:11362;urn:miriam:ensembl:ENSG00000115415"
      hgnc "HGNC_SYMBOL:STAT2;HGNC_SYMBOL:STAT1"
      map_id "M120_3"
      name "ISGF3_space_precursor"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa12"
      uniprot "UNIPROT:P52630;UNIPROT:P42224"
    ]
    graphics [
      x 553.3444928433386
      y 1699.6962761244358
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_62"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re55"
      uniprot "NA"
    ]
    graphics [
      x 518.5740932496735
      y 1866.7695791111269
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000168610;urn:miriam:ncbigene:6774;urn:miriam:refseq:NM_139276;urn:miriam:uniprot:P40763;urn:miriam:uniprot:P40763;urn:miriam:ncbigene:6774;urn:miriam:hgnc:11364;urn:miriam:refseq:NM_003150;urn:miriam:hgnc.symbol:STAT3;urn:miriam:hgnc.symbol:STAT3"
      hgnc "HGNC_SYMBOL:STAT3"
      map_id "M120_83"
      name "STAT3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa11"
      uniprot "UNIPROT:P40763"
    ]
    graphics [
      x 608.915194771082
      y 1940.3175518575758
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:refseq:NM_007315;urn:miriam:hgnc.symbol:STAT1;urn:miriam:hgnc.symbol:STAT1;urn:miriam:uniprot:P42224;urn:miriam:uniprot:P42224;urn:miriam:ncbigene:6772;urn:miriam:ncbigene:6772;urn:miriam:hgnc:11362;urn:miriam:ensembl:ENSG00000115415;urn:miriam:uniprot:P52630;urn:miriam:uniprot:P52630;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc:11363;urn:miriam:ncbigene:6773;urn:miriam:ncbigene:6773;urn:miriam:refseq:NM_005419;urn:miriam:ensembl:ENSG00000170581;urn:miriam:ensembl:ENSG00000168610;urn:miriam:ncbigene:6774;urn:miriam:refseq:NM_139276;urn:miriam:uniprot:P40763;urn:miriam:uniprot:P40763;urn:miriam:ncbigene:6774;urn:miriam:hgnc:11364;urn:miriam:refseq:NM_003150;urn:miriam:hgnc.symbol:STAT3;urn:miriam:hgnc.symbol:STAT3"
      hgnc "HGNC_SYMBOL:STAT1;HGNC_SYMBOL:STAT2;HGNC_SYMBOL:STAT3"
      map_id "M120_4"
      name "Inhibited_space_ISGF3_space_precursor"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa14"
      uniprot "UNIPROT:P42224;UNIPROT:P52630;UNIPROT:P40763"
    ]
    graphics [
      x 477.6066034714951
      y 2009.9681783878616
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:refseq:NM_007315;urn:miriam:hgnc.symbol:STAT1;urn:miriam:hgnc.symbol:STAT1;urn:miriam:uniprot:P42224;urn:miriam:uniprot:P42224;urn:miriam:ncbigene:6772;urn:miriam:ncbigene:6772;urn:miriam:hgnc:11362;urn:miriam:ensembl:ENSG00000115415;urn:miriam:uniprot:Q00978;urn:miriam:uniprot:Q00978;urn:miriam:refseq:NM_001385400;urn:miriam:ensembl:ENSG00000213928;urn:miriam:hgnc.symbol:IRF9;urn:miriam:ncbigene:10379;urn:miriam:hgnc.symbol:IRF9;urn:miriam:ncbigene:10379;urn:miriam:hgnc:6131;urn:miriam:uniprot:P52630;urn:miriam:uniprot:P52630;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc:11363;urn:miriam:ncbigene:6773;urn:miriam:ncbigene:6773;urn:miriam:refseq:NM_005419;urn:miriam:ensembl:ENSG00000170581"
      hgnc "HGNC_SYMBOL:STAT1;HGNC_SYMBOL:IRF9;HGNC_SYMBOL:STAT2"
      map_id "M120_13"
      name "ISGF3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa3"
      uniprot "UNIPROT:P42224;UNIPROT:Q00978;UNIPROT:P52630"
    ]
    graphics [
      x 811.7261251987824
      y 1443.1507529106486
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_30"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re1"
      uniprot "NA"
    ]
    graphics [
      x 840.8723965996579
      y 1283.025202087319
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:P52630;urn:miriam:uniprot:P52630;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc:11363;urn:miriam:ncbigene:6773;urn:miriam:ncbigene:6773;urn:miriam:refseq:NM_005419;urn:miriam:ensembl:ENSG00000170581;urn:miriam:uniprot:Q00978;urn:miriam:uniprot:Q00978;urn:miriam:refseq:NM_001385400;urn:miriam:ensembl:ENSG00000213928;urn:miriam:hgnc.symbol:IRF9;urn:miriam:ncbigene:10379;urn:miriam:hgnc.symbol:IRF9;urn:miriam:ncbigene:10379;urn:miriam:hgnc:6131;urn:miriam:refseq:NM_007315;urn:miriam:hgnc.symbol:STAT1;urn:miriam:hgnc.symbol:STAT1;urn:miriam:uniprot:P42224;urn:miriam:uniprot:P42224;urn:miriam:ncbigene:6772;urn:miriam:ncbigene:6772;urn:miriam:hgnc:11362;urn:miriam:ensembl:ENSG00000115415"
      hgnc "HGNC_SYMBOL:STAT2;HGNC_SYMBOL:IRF9;HGNC_SYMBOL:STAT1"
      map_id "M120_19"
      name "ISGF3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa4"
      uniprot "UNIPROT:P52630;UNIPROT:Q00978;UNIPROT:P42224"
    ]
    graphics [
      x 768.9034420283132
      y 1127.895869176902
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:Q14653;urn:miriam:hgnc:6118;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661;urn:miriam:ncbigene:3661"
      hgnc "HGNC_SYMBOL:IRF3"
      map_id "M120_85"
      name "IRF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa126"
      uniprot "UNIPROT:Q14653"
    ]
    graphics [
      x 1616.0364720192583
      y 1098.5965477485167
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "PUBMED:25636800"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_55"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re47"
      uniprot "NA"
    ]
    graphics [
      x 1572.9166213617414
      y 1261.6086431040196
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ncbigene:23586;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:hgnc.symbol:DDX58"
      hgnc "HGNC_SYMBOL:DDX58"
      map_id "M120_17"
      name "RIG_minus_I:dsRNA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa37"
      uniprot "UNIPROT:O95786"
    ]
    graphics [
      x 1714.2561678709185
      y 471.4504880551498
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "PUBMED:25045870"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_73"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re69"
      uniprot "NA"
    ]
    graphics [
      x 1867.160739376126
      y 423.458878074934
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746"
      hgnc "HGNC_SYMBOL:MAVS"
      map_id "M120_80"
      name "MAVS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa100"
      uniprot "UNIPROT:Q7Z434"
    ]
    graphics [
      x 1801.4502901875935
      y 506.84474383183954
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746;urn:miriam:ncbigene:23586;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:hgnc.symbol:DDX58"
      hgnc "HGNC_SYMBOL:MAVS;HGNC_SYMBOL:DDX58"
      map_id "M120_12"
      name "RIG_minus_I_minus_mitoMAVS"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa23"
      uniprot "UNIPROT:Q7Z434;UNIPROT:O95786"
    ]
    graphics [
      x 1978.4001822074238
      y 521.3356636208712
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ncbigene:23586;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:hgnc.symbol:DDX58"
      hgnc "HGNC_SYMBOL:DDX58"
      map_id "M120_16"
      name "RIG_minus_I:dsRNA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa36"
      uniprot "UNIPROT:O95786"
    ]
    graphics [
      x 1557.9412969737282
      y 748.6618833097812
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "PUBMED:22390971"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_75"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re70"
      uniprot "NA"
    ]
    graphics [
      x 1592.0135999376714
      y 553.136273963385
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:uniprot:Q8IUD6;urn:miriam:uniprot:Q8IUD6;urn:miriam:hgnc:21158;urn:miriam:refseq:NM_032322;urn:miriam:hgnc.symbol:RNF135;urn:miriam:hgnc.symbol:RNF135;urn:miriam:ncbigene:84282;urn:miriam:ncbigene:84282;urn:miriam:ensembl:ENSG00000181481;urn:miriam:ncbigene:7706;urn:miriam:ensembl:ENSG00000121060;urn:miriam:ncbigene:7706;urn:miriam:hgnc.symbol:TRIM25;urn:miriam:hgnc.symbol:TRIM25;urn:miriam:ec-code:2.3.2.27;urn:miriam:hgnc:12932;urn:miriam:uniprot:Q14258;urn:miriam:uniprot:Q14258;urn:miriam:refseq:NM_005082"
      hgnc "HGNC_SYMBOL:RNF135;HGNC_SYMBOL:TRIM25"
      map_id "M120_18"
      name "Riplet:TRIM25"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa38"
      uniprot "UNIPROT:Q8IUD6;UNIPROT:Q14258"
    ]
    graphics [
      x 1466.7310647516106
      y 533.5567559885875
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_105"
      name "viral_space_RNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa75"
      uniprot "NA"
    ]
    graphics [
      x 1186.6031537225147
      y 2161.2326684137506
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re24"
      uniprot "NA"
    ]
    graphics [
      x 1331.2792368644095
      y 2164.974220096062
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:hgnc:18873;urn:miriam:ncbigene:64135;urn:miriam:ncbigene:64135;urn:miriam:refseq:NM_022168;urn:miriam:ensembl:ENSG00000115267;urn:miriam:uniprot:Q9BYX4;urn:miriam:uniprot:Q9BYX4;urn:miriam:ec-code:3.6.4.13;urn:miriam:hgnc.symbol:IFIH1;urn:miriam:hgnc.symbol:IFIH1"
      hgnc "HGNC_SYMBOL:IFIH1"
      map_id "M120_108"
      name "IFIH1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa78"
      uniprot "UNIPROT:Q9BYX4"
    ]
    graphics [
      x 1331.162288727826
      y 2049.026036703509
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_109"
      name "sa75_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa79"
      uniprot "NA"
    ]
    graphics [
      x 1421.0688950596095
      y 2060.2856472328394
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:hgnc.symbol:JAK1;urn:miriam:hgnc.symbol:JAK1;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc:6190;urn:miriam:ncbigene:3716;urn:miriam:ncbigene:3716;urn:miriam:ensembl:ENSG00000162434;urn:miriam:uniprot:P23458;urn:miriam:uniprot:P23458;urn:miriam:refseq:NM_002227;urn:miriam:ec-code:2.7.10.2;urn:miriam:ncbigene:7297;urn:miriam:ncbigene:7297;urn:miriam:ensembl:ENSG00000105397;urn:miriam:refseq:NM_001385197;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc:12440;urn:miriam:uniprot:P29597;urn:miriam:uniprot:P29597;urn:miriam:ncbigene:163702;urn:miriam:ncbigene:163702;urn:miriam:hgnc.symbol:IFNLR1;urn:miriam:uniprot:Q8IU57;urn:miriam:uniprot:Q8IU57;urn:miriam:hgnc.symbol:IFNLR1;urn:miriam:ensembl:ENSG00000185436;urn:miriam:hgnc:18584;urn:miriam:refseq:NM_170743;urn:miriam:uniprot:O15524;urn:miriam:uniprot:O15524;urn:miriam:ncbigene:8651;urn:miriam:ncbigene:8651;urn:miriam:ensembl:ENSG00000185338;urn:miriam:hgnc:19383;urn:miriam:hgnc.symbol:SOCS1;urn:miriam:hgnc.symbol:SOCS1;urn:miriam:refseq:NM_003745"
      hgnc "HGNC_SYMBOL:JAK1;HGNC_SYMBOL:TYK2;HGNC_SYMBOL:IFNLR1;HGNC_SYMBOL:SOCS1"
      map_id "M120_14"
      name "receptor_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa34"
      uniprot "UNIPROT:P23458;UNIPROT:P29597;UNIPROT:Q8IU57;UNIPROT:O15524"
    ]
    graphics [
      x 911.772979029325
      y 311.58765039215893
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_67"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re61"
      uniprot "NA"
    ]
    graphics [
      x 880.7633772985326
      y 222.587392914995
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_89"
      name "s226"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa137"
      uniprot "NA"
    ]
    graphics [
      x 989.5895318714823
      y 234.83713633563707
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:doi:10.1101/2020.05.18.102467;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ncbiprotein:YP_009725297;urn:miriam:uniprot:P0C6X7;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M120_5"
      name "inhibited_space_ribosomal_space_40S_space_SU"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa15"
      uniprot "UNIPROT:P0C6X7"
    ]
    graphics [
      x 1147.234172596051
      y 1274.9208330784832
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "PUBMED:32680882"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_70"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re65"
      uniprot "NA"
    ]
    graphics [
      x 1283.609443744299
      y 1355.894230411056
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_93"
      name "csa15_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa141"
      uniprot "NA"
    ]
    graphics [
      x 1397.7271981602562
      y 1430.1162406816009
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_25"
      name "IFNs"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa54"
      uniprot "NA"
    ]
    graphics [
      x 569.3241750571987
      y 1249.0747403720313
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_63"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re56"
      uniprot "NA"
    ]
    graphics [
      x 674.2617084238948
      y 1347.0279315273492
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_24"
      name "IFNs"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa53"
      uniprot "NA"
    ]
    graphics [
      x 784.1071828994825
      y 1353.853107671293
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ncbigene:23586;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:hgnc.symbol:DDX58;urn:miriam:uniprot:Q7Z434;urn:miriam:hgnc.symbol:MAVS;urn:miriam:hgnc.symbol:MAVS;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ncbigene:57506;urn:miriam:ncbigene:57506;urn:miriam:hgnc:29233;urn:miriam:refseq:NM_020746"
      hgnc "HGNC_SYMBOL:DDX58;HGNC_SYMBOL:MAVS"
      map_id "M120_11"
      name "RIG_minus_I_minus_MAVS"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa22"
      uniprot "UNIPROT:O95786;UNIPROT:Q7Z434"
    ]
    graphics [
      x 932.5355662360765
      y 410.39228792251663
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      annotation "PUBMED:25636800"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_42"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re31"
      uniprot "NA"
    ]
    graphics [
      x 816.8548895561988
      y 447.0009747260127
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_121"
      name "dsRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa93"
      uniprot "NA"
    ]
    graphics [
      x 920.3917434700953
      y 504.2226099767872
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_37"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re25"
      uniprot "NA"
    ]
    graphics [
      x 1047.1867694837524
      y 2102.0046263014683
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ncbiprotein:YP_009725309"
      hgnc "NA"
      map_id "M120_110"
      name "nsp14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa80"
      uniprot "NA"
    ]
    graphics [
      x 1119.241106371295
      y 2019.5641054469997
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_106"
      name "viral_space_RNA_plus_N_minus_methyl_minus_Guanine"
      node_subtype "RNA"
      node_type "species"
      org_id "sa76"
      uniprot "NA"
    ]
    graphics [
      x 953.2557531988292
      y 1972.1811132359303
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "PUBMED:25554382"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_76"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re71"
      uniprot "NA"
    ]
    graphics [
      x 1680.5834573529492
      y 621.0494660389405
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M120_81"
      name "PLPro"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa101"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 1577.5253215884804
      y 645.2620029280513
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_65"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re59"
      uniprot "NA"
    ]
    graphics [
      x 789.0147919797953
      y 346.9578851615969
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:O15524;urn:miriam:uniprot:O15524;urn:miriam:ncbigene:8651;urn:miriam:ncbigene:8651;urn:miriam:ensembl:ENSG00000185338;urn:miriam:hgnc:19383;urn:miriam:hgnc.symbol:SOCS1;urn:miriam:hgnc.symbol:SOCS1;urn:miriam:refseq:NM_003745"
      hgnc "HGNC_SYMBOL:SOCS1"
      map_id "M120_99"
      name "SOCS1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa45"
      uniprot "UNIPROT:O15524"
    ]
    graphics [
      x 772.5375397027177
      y 492.668560749477
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:refseq:NM_007315;urn:miriam:hgnc.symbol:STAT1;urn:miriam:hgnc.symbol:STAT1;urn:miriam:uniprot:P42224;urn:miriam:uniprot:P42224;urn:miriam:ncbigene:6772;urn:miriam:ncbigene:6772;urn:miriam:hgnc:11362;urn:miriam:ensembl:ENSG00000115415"
      hgnc "HGNC_SYMBOL:STAT1"
      map_id "M120_98"
      name "STAT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa4"
      uniprot "UNIPROT:P42224"
    ]
    graphics [
      x 458.3088097477488
      y 1460.5519518024983
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_41"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re3"
      uniprot "NA"
    ]
    graphics [
      x 371.64440108574763
      y 1394.1858350530201
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:hgnc.symbol:JAK1;urn:miriam:hgnc.symbol:JAK1;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc:6190;urn:miriam:ncbigene:3716;urn:miriam:ncbigene:3716;urn:miriam:ensembl:ENSG00000162434;urn:miriam:uniprot:P23458;urn:miriam:uniprot:P23458;urn:miriam:refseq:NM_002227"
      hgnc "HGNC_SYMBOL:JAK1"
      map_id "M120_96"
      name "JAK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa36"
      uniprot "UNIPROT:P23458"
    ]
    graphics [
      x 506.20834211227316
      y 1337.3573859721864
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:refseq:NM_007315;urn:miriam:hgnc.symbol:STAT1;urn:miriam:hgnc.symbol:STAT1;urn:miriam:uniprot:P42224;urn:miriam:uniprot:P42224;urn:miriam:ncbigene:6772;urn:miriam:ncbigene:6772;urn:miriam:hgnc:11362;urn:miriam:ensembl:ENSG00000115415"
      hgnc "HGNC_SYMBOL:STAT1"
      map_id "M120_101"
      name "STAT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa5"
      uniprot "UNIPROT:P42224"
    ]
    graphics [
      x 342.1842244102345
      y 1524.6635556128467
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:O14543;urn:miriam:uniprot:O14543;urn:miriam:hgnc:19391;urn:miriam:ncbigene:9021;urn:miriam:ncbigene:9021;urn:miriam:ensembl:ENSG00000184557;urn:miriam:refseq:NM_001378932;urn:miriam:hgnc.symbol:SOCS3;urn:miriam:hgnc.symbol:SOCS3;urn:miriam:ec-code:2.7.10.2;urn:miriam:ncbigene:7297;urn:miriam:ncbigene:7297;urn:miriam:ensembl:ENSG00000105397;urn:miriam:refseq:NM_001385197;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc:12440;urn:miriam:uniprot:P29597;urn:miriam:uniprot:P29597;urn:miriam:hgnc.symbol:JAK1;urn:miriam:hgnc.symbol:JAK1;urn:miriam:ec-code:2.7.10.2;urn:miriam:hgnc:6190;urn:miriam:ncbigene:3716;urn:miriam:ncbigene:3716;urn:miriam:ensembl:ENSG00000162434;urn:miriam:uniprot:P23458;urn:miriam:uniprot:P23458;urn:miriam:refseq:NM_002227;urn:miriam:ncbigene:163702;urn:miriam:ncbigene:163702;urn:miriam:hgnc.symbol:IFNLR1;urn:miriam:uniprot:Q8IU57;urn:miriam:uniprot:Q8IU57;urn:miriam:hgnc.symbol:IFNLR1;urn:miriam:ensembl:ENSG00000185436;urn:miriam:hgnc:18584;urn:miriam:refseq:NM_170743"
      hgnc "HGNC_SYMBOL:SOCS3;HGNC_SYMBOL:TYK2;HGNC_SYMBOL:JAK1;HGNC_SYMBOL:IFNLR1"
      map_id "M120_2"
      name "receptor_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa11"
      uniprot "UNIPROT:O14543;UNIPROT:P29597;UNIPROT:P23458;UNIPROT:Q8IU57"
    ]
    graphics [
      x 400.9086441037798
      y 377.98829086414844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_68"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re62"
      uniprot "NA"
    ]
    graphics [
      x 324.2904165152464
      y 504.313472086043
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_90"
      name "s227"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa138"
      uniprot "NA"
    ]
    graphics [
      x 314.1840094325174
      y 633.5094481806296
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_32"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re12"
      uniprot "NA"
    ]
    graphics [
      x 858.2881893560864
      y 152.41381770099747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      annotation "PUBMED:25636800"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_54"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re46"
      uniprot "NA"
    ]
    graphics [
      x 1699.1797738028781
      y 973.1851546573564
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_84"
      name "dsRNA_underscore_vesicle"
      node_subtype "RNA"
      node_type "species"
      org_id "sa125"
      uniprot "NA"
    ]
    graphics [
      x 1288.0138356274902
      y 1568.7813446378816
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_53"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re45"
      uniprot "NA"
    ]
    graphics [
      x 1324.4071817485828
      y 1444.2553254900681
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_112"
      name "dsRNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa82"
      uniprot "NA"
    ]
    graphics [
      x 1286.0946804424716
      y 1277.8607100020445
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_66"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re60"
      uniprot "NA"
    ]
    graphics [
      x 405.96680598392913
      y 2116.995235423896
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_88"
      name "s225"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa136"
      uniprot "NA"
    ]
    graphics [
      x 358.35405436283213
      y 2010.5388421582306
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "M120_82"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa103"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 1302.3039870470448
      y 572.7226970169905
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      annotation "PUBMED:32626922"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_72"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re68"
      uniprot "NA"
    ]
    graphics [
      x 1358.8244846513035
      y 685.0977772534744
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ncbigene:23586;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:hgnc.symbol:DDX58"
      hgnc "HGNC_SYMBOL:DDX58"
      map_id "M120_126"
      name "RIG_minus_I"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa99"
      uniprot "UNIPROT:O95786"
    ]
    graphics [
      x 1405.6228156659981
      y 821.5837070183316
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ncbigene:43740571;urn:miriam:uniprot:P0DTC5;urn:miriam:ncbigene:23586;urn:miriam:ec-code:3.6.4.13;urn:miriam:uniprot:O95786;urn:miriam:hgnc.symbol:DDX58"
      hgnc "HGNC_SYMBOL:DDX58"
      map_id "M120_15"
      name "RIG_minus_1_space_M_minus_Protein"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa35"
      uniprot "UNIPROT:P0DTC5;UNIPROT:O95786"
    ]
    graphics [
      x 1249.256456275369
      y 646.0116483799992
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_52"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re44"
      uniprot "NA"
    ]
    graphics [
      x 1249.378536255572
      y 1444.3008709263454
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:hgnc.symbol:CHUK;urn:miriam:ec-code:2.7.11.10;urn:miriam:ec-code:2.7.11.1;urn:miriam:uniprot:O14920;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:ncbigene:3551;urn:miriam:uniprot:O15111;urn:miriam:ncbigene:1147"
      hgnc "HGNC_SYMBOL:CHUK;HGNC_SYMBOL:IKBKB"
      map_id "M120_113"
      name "IKK"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa85"
      uniprot "UNIPROT:O14920;UNIPROT:O15111"
    ]
    graphics [
      x 1851.575112584585
      y 1040.9537074800874
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "PUBMED:25636800"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_39"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re27"
      uniprot "NA"
    ]
    graphics [
      x 1935.9704440391854
      y 908.3858805567596
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110"
      hgnc "HGNC_SYMBOL:TBK1"
      map_id "M120_114"
      name "TBK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa86"
      uniprot "UNIPROT:Q9UHD2"
    ]
    graphics [
      x 1816.6933892781285
      y 966.1359912788633
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:hgnc.symbol:CHUK;urn:miriam:ec-code:2.7.11.10;urn:miriam:ec-code:2.7.11.1;urn:miriam:uniprot:O14920;urn:miriam:hgnc.symbol:IKBKB;urn:miriam:ncbigene:3551;urn:miriam:uniprot:O15111;urn:miriam:ncbigene:1147;urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:hgnc.symbol:TBK1;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110;urn:miriam:ncbigene:29110"
      hgnc "HGNC_SYMBOL:CHUK;HGNC_SYMBOL:IKBKB;HGNC_SYMBOL:TBK1"
      map_id "M120_8"
      name "IKK_minus_TBK1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa18"
      uniprot "UNIPROT:O14920;UNIPROT:O15111;UNIPROT:Q9UHD2"
    ]
    graphics [
      x 2053.3650094314735
      y 796.025912895749
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_33"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re14"
      uniprot "NA"
    ]
    graphics [
      x 388.29747225911365
      y 1651.9253220429655
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:P52630;urn:miriam:uniprot:P52630;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc:11363;urn:miriam:ncbigene:6773;urn:miriam:ncbigene:6773;urn:miriam:refseq:NM_005419;urn:miriam:ensembl:ENSG00000170581"
      hgnc "HGNC_SYMBOL:STAT2"
      map_id "M120_103"
      name "STAT2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa7"
      uniprot "UNIPROT:P52630"
    ]
    graphics [
      x 231.53159569953823
      y 1622.5057726151667
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      annotation "PUBMED:32680882"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_61"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re54"
      uniprot "NA"
    ]
    graphics [
      x 766.2309447913545
      y 1018.4468697041959
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ncbiprotein:YP_009725297;urn:miriam:uniprot:P0C6X7;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M120_104"
      name "Nsp1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa70"
      uniprot "UNIPROT:P0C6X7"
    ]
    graphics [
      x 917.0253102006822
      y 1081.7831899756363
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ncbiprotein:YP_009725297;urn:miriam:uniprot:P0C6X7;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M120_6"
      name "inhibited_space_80S_space_ribosome"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa16"
      uniprot "UNIPROT:P0C6X7"
    ]
    graphics [
      x 904.2730188117076
      y 956.1628803280329
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      annotation "PUBMED:32680882"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_60"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re53"
      uniprot "NA"
    ]
    graphics [
      x 1020.2481472717806
      y 1187.0700554934099
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_38"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re26"
      uniprot "NA"
    ]
    graphics [
      x 878.1857470574798
      y 1820.5044134804973
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ncbiprotein:YP_009725311"
      hgnc "NA"
      map_id "M120_111"
      name "nsp16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa81"
      uniprot "NA"
    ]
    graphics [
      x 857.4025715924456
      y 1681.940583052693
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_107"
      name "5'cap_minus_viral_minus_RNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa77"
      uniprot "NA"
    ]
    graphics [
      x 783.2433649641222
      y 1716.873934324632
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      annotation "PUBMED:25636800"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_50"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re40"
      uniprot "NA"
    ]
    graphics [
      x 2070.918608435205
      y 641.661433432509
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_77"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re8"
      uniprot "NA"
    ]
    graphics [
      x 452.3154445224411
      y 1139.3730248870515
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_118"
      name "IFN_minus_III"
      node_subtype "GENE"
      node_type "species"
      org_id "sa90"
      uniprot "NA"
    ]
    graphics [
      x 1726.9716741751176
      y 1226.789849282977
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_46"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re36"
      uniprot "NA"
    ]
    graphics [
      x 1605.744370576967
      y 1190.6004473598766
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ncbigene:3659;urn:miriam:ncbigene:3659;urn:miriam:hgnc:6116;urn:miriam:hgnc.symbol:IRF1;urn:miriam:hgnc.symbol:IRF1;urn:miriam:refseq:NM_002198;urn:miriam:uniprot:P10914;urn:miriam:uniprot:P10914;urn:miriam:ensembl:ENSG00000125347"
      hgnc "HGNC_SYMBOL:IRF1"
      map_id "M120_120"
      name "IRF1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa92"
      uniprot "UNIPROT:P10914"
    ]
    graphics [
      x 1446.1614167891382
      y 1167.8604802056889
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_119"
      name "IFN_minus_III"
      node_subtype "RNA"
      node_type "species"
      org_id "sa91"
      uniprot "NA"
    ]
    graphics [
      x 1474.938595264994
      y 1045.6407629382907
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:P52630;urn:miriam:uniprot:P52630;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc.symbol:STAT2;urn:miriam:hgnc:11363;urn:miriam:ncbigene:6773;urn:miriam:ncbigene:6773;urn:miriam:refseq:NM_005419;urn:miriam:ensembl:ENSG00000170581"
      hgnc "HGNC_SYMBOL:STAT2"
      map_id "M120_102"
      name "STAT2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa6"
      uniprot "UNIPROT:P52630"
    ]
    graphics [
      x 257.1790610484102
      y 1688.7406975964545
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_49"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re4"
      uniprot "NA"
    ]
    graphics [
      x 141.30417930320368
      y 1724.1269548200453
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:ec-code:2.7.10.2;urn:miriam:ncbigene:7297;urn:miriam:ncbigene:7297;urn:miriam:ensembl:ENSG00000105397;urn:miriam:refseq:NM_001385197;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc.symbol:TYK2;urn:miriam:hgnc:12440;urn:miriam:uniprot:P29597;urn:miriam:uniprot:P29597"
      hgnc "HGNC_SYMBOL:TYK2"
      map_id "M120_97"
      name "TYK2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa37"
      uniprot "UNIPROT:P29597"
    ]
    graphics [
      x 262.4565565916064
      y 1765.624622207343
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_78"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re9"
      uniprot "NA"
    ]
    graphics [
      x 335.6552476342207
      y 1066.957011972356
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_57"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re49"
      uniprot "NA"
    ]
    graphics [
      x 1300.1599030649904
      y 1064.253288193343
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_34"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re15"
      uniprot "NA"
    ]
    graphics [
      x 722.0584957005512
      y 1566.847474200377
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:Q00978;urn:miriam:uniprot:Q00978;urn:miriam:refseq:NM_001385400;urn:miriam:ensembl:ENSG00000213928;urn:miriam:hgnc.symbol:IRF9;urn:miriam:ncbigene:10379;urn:miriam:hgnc.symbol:IRF9;urn:miriam:ncbigene:10379;urn:miriam:hgnc:6131"
      hgnc "HGNC_SYMBOL:IRF9"
      map_id "M120_79"
      name "IRF9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa10"
      uniprot "UNIPROT:Q00978"
    ]
    graphics [
      x 842.7091355691925
      y 1555.331980257251
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_64"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re58"
      uniprot "NA"
    ]
    graphics [
      x 509.0662336114105
      y 240.76823810156975
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "urn:miriam:uniprot:O14543;urn:miriam:uniprot:O14543;urn:miriam:hgnc:19391;urn:miriam:ncbigene:9021;urn:miriam:ncbigene:9021;urn:miriam:ensembl:ENSG00000184557;urn:miriam:refseq:NM_001378932;urn:miriam:hgnc.symbol:SOCS3;urn:miriam:hgnc.symbol:SOCS3"
      hgnc "HGNC_SYMBOL:SOCS3"
      map_id "M120_100"
      name "SOCS3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa46"
      uniprot "UNIPROT:O14543"
    ]
    graphics [
      x 404.6173145531727
      y 280.5832172071191
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_95"
      name "IFN_minus_sensitive_minus_response_minus_element"
      node_subtype "GENE"
      node_type "species"
      org_id "sa16"
      uniprot "NA"
    ]
    graphics [
      x 588.0590014809326
      y 935.0392429794349
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      annotation "PUBMED:30936491"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_35"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re2"
      uniprot "NA"
    ]
    graphics [
      x 687.2720782208903
      y 967.8593606762497
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_44"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re34"
      uniprot "NA"
    ]
    graphics [
      x 1313.417549823155
      y 905.2285741488138
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_124"
      name "IFN_minus_III"
      node_subtype "RNA"
      node_type "species"
      org_id "sa96"
      uniprot "NA"
    ]
    graphics [
      x 1149.1624845512767
      y 778.5339671924442
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_124"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_87"
      name "RNA"
      node_subtype "RNA"
      node_type "species"
      org_id "sa128"
      uniprot "NA"
    ]
    graphics [
      x 1036.0942121553776
      y 1533.5138366644744
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_58"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re50"
      uniprot "NA"
    ]
    graphics [
      x 1149.5209645741925
      y 1414.98746718222
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_125"
      name "IFN_minus_III"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa97"
      uniprot "NA"
    ]
    graphics [
      x 790.118008675797
      y 588.0683630660974
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_125"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_47"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re37"
      uniprot "NA"
    ]
    graphics [
      x 724.8151583631011
      y 381.7113309279532
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      annotation "PUBMED:32680882"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_69"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re63"
      uniprot "NA"
    ]
    graphics [
      x 1052.7183309795253
      y 949.7912427933262
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_91"
      name "s228"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa139"
      uniprot "NA"
    ]
    graphics [
      x 1136.9635302786319
      y 1035.761273871896
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_45"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "re35"
      uniprot "NA"
    ]
    graphics [
      x 969.5064248579627
      y 682.8785731298799
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      annotation "PUBMED:25045870"
      count 1
      diagram "C19DMap:Interferon lambda pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M120_48"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re38"
      uniprot "NA"
    ]
    graphics [
      x 1420.4704432921635
      y 973.0046964850594
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M120_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 127
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_9"
      target_id "M120_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 128
    source 3
    target 2
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_117"
      target_id "M120_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 2
    target 4
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_51"
      target_id "M120_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 130
    source 5
    target 6
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_26"
      target_id "M120_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 131
    source 6
    target 7
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_31"
      target_id "M120_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 132
    source 8
    target 9
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_94"
      target_id "M120_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 133
    source 9
    target 10
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_74"
      target_id "M120_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 134
    source 11
    target 12
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_115"
      target_id "M120_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 135
    source 13
    target 12
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M120_7"
      target_id "M120_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 136
    source 12
    target 14
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_40"
      target_id "M120_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 137
    source 15
    target 16
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_1"
      target_id "M120_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 138
    source 16
    target 17
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_71"
      target_id "M120_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 139
    source 16
    target 18
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_71"
      target_id "M120_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 19
    target 20
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_122"
      target_id "M120_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 21
    target 20
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CATALYSIS"
      source_id "M120_10"
      target_id "M120_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 20
    target 22
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_43"
      target_id "M120_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 22
    target 23
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_123"
      target_id "M120_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 144
    source 23
    target 24
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_56"
      target_id "M120_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 145
    source 25
    target 26
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_22"
      target_id "M120_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 146
    source 27
    target 26
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_23"
      target_id "M120_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 147
    source 26
    target 28
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_59"
      target_id "M120_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 148
    source 29
    target 30
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_3"
      target_id "M120_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 149
    source 31
    target 30
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_83"
      target_id "M120_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 150
    source 30
    target 32
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_62"
      target_id "M120_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 151
    source 33
    target 34
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_13"
      target_id "M120_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 152
    source 34
    target 35
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_30"
      target_id "M120_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 153
    source 36
    target 37
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_85"
      target_id "M120_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 154
    source 37
    target 3
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_55"
      target_id "M120_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 155
    source 38
    target 39
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_17"
      target_id "M120_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 156
    source 40
    target 39
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_80"
      target_id "M120_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 157
    source 39
    target 41
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_73"
      target_id "M120_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 158
    source 42
    target 43
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_16"
      target_id "M120_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 159
    source 44
    target 43
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CATALYSIS"
      source_id "M120_18"
      target_id "M120_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 160
    source 43
    target 38
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_75"
      target_id "M120_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 161
    source 45
    target 46
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_105"
      target_id "M120_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 162
    source 47
    target 46
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CATALYSIS"
      source_id "M120_108"
      target_id "M120_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 163
    source 46
    target 48
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_36"
      target_id "M120_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 164
    source 49
    target 50
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_14"
      target_id "M120_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 165
    source 50
    target 51
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_67"
      target_id "M120_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 166
    source 52
    target 53
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_5"
      target_id "M120_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 167
    source 53
    target 54
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_70"
      target_id "M120_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 168
    source 55
    target 56
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_25"
      target_id "M120_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 169
    source 56
    target 57
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_63"
      target_id "M120_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 170
    source 58
    target 59
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_11"
      target_id "M120_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 171
    source 60
    target 59
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "TRIGGER"
      source_id "M120_121"
      target_id "M120_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 172
    source 59
    target 21
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_42"
      target_id "M120_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 173
    source 45
    target 61
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_105"
      target_id "M120_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 174
    source 62
    target 61
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CATALYSIS"
      source_id "M120_110"
      target_id "M120_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 175
    source 61
    target 63
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_37"
      target_id "M120_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 176
    source 38
    target 64
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_17"
      target_id "M120_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 177
    source 65
    target 64
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CATALYSIS"
      source_id "M120_81"
      target_id "M120_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 178
    source 64
    target 42
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_76"
      target_id "M120_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 179
    source 15
    target 66
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_1"
      target_id "M120_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 180
    source 67
    target 66
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_99"
      target_id "M120_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 181
    source 66
    target 49
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_65"
      target_id "M120_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 182
    source 68
    target 69
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_98"
      target_id "M120_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 70
    target 69
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CATALYSIS"
      source_id "M120_96"
      target_id "M120_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 69
    target 71
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_41"
      target_id "M120_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 72
    target 73
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_2"
      target_id "M120_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 73
    target 74
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_68"
      target_id "M120_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 18
    target 75
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_29"
      target_id "M120_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 17
    target 75
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M120_28"
      target_id "M120_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 75
    target 15
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_32"
      target_id "M120_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 14
    target 76
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_116"
      target_id "M120_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 76
    target 36
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_54"
      target_id "M120_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 77
    target 78
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_84"
      target_id "M120_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 78
    target 79
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_53"
      target_id "M120_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 32
    target 80
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_4"
      target_id "M120_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 80
    target 81
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_66"
      target_id "M120_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 82
    target 83
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_82"
      target_id "M120_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 84
    target 83
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_126"
      target_id "M120_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 83
    target 85
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_72"
      target_id "M120_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 79
    target 86
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_112"
      target_id "M120_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 86
    target 77
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_52"
      target_id "M120_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 87
    target 88
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_113"
      target_id "M120_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 89
    target 88
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_114"
      target_id "M120_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 88
    target 90
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_39"
      target_id "M120_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 71
    target 91
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_101"
      target_id "M120_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 92
    target 91
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_103"
      target_id "M120_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 91
    target 29
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_33"
      target_id "M120_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 28
    target 93
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_21"
      target_id "M120_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 94
    target 93
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_104"
      target_id "M120_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 93
    target 95
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_61"
      target_id "M120_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 27
    target 96
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_23"
      target_id "M120_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 94
    target 96
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_104"
      target_id "M120_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 96
    target 52
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_60"
      target_id "M120_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 63
    target 97
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_106"
      target_id "M120_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 98
    target 97
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CATALYSIS"
      source_id "M120_111"
      target_id "M120_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 97
    target 99
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_38"
      target_id "M120_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 41
    target 100
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_12"
      target_id "M120_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 90
    target 100
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_8"
      target_id "M120_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 100
    target 13
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_50"
      target_id "M120_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 10
    target 101
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_92"
      target_id "M120_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 28
    target 101
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CATALYSIS"
      source_id "M120_21"
      target_id "M120_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 101
    target 55
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_77"
      target_id "M120_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 102
    target 103
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_118"
      target_id "M120_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 4
    target 103
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M120_20"
      target_id "M120_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 104
    target 103
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M120_120"
      target_id "M120_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 103
    target 105
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_46"
      target_id "M120_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 106
    target 107
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_102"
      target_id "M120_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 108
    target 107
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CATALYSIS"
      source_id "M120_97"
      target_id "M120_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 107
    target 92
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_49"
      target_id "M120_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 10
    target 109
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_92"
      target_id "M120_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 28
    target 109
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CATALYSIS"
      source_id "M120_21"
      target_id "M120_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 109
    target 5
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_78"
      target_id "M120_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 24
    target 110
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_86"
      target_id "M120_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 110
    target 104
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_57"
      target_id "M120_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 29
    target 111
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_3"
      target_id "M120_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 112
    target 111
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_79"
      target_id "M120_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 111
    target 33
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_34"
      target_id "M120_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 15
    target 113
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_1"
      target_id "M120_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 114
    target 113
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_100"
      target_id "M120_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 239
    source 113
    target 72
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_64"
      target_id "M120_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 240
    source 115
    target 116
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_95"
      target_id "M120_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 241
    source 35
    target 116
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "TRIGGER"
      source_id "M120_19"
      target_id "M120_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 242
    source 116
    target 8
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_35"
      target_id "M120_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 243
    source 105
    target 117
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_119"
      target_id "M120_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 244
    source 117
    target 118
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_44"
      target_id "M120_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 245
    source 119
    target 120
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_87"
      target_id "M120_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 246
    source 120
    target 79
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_58"
      target_id "M120_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 247
    source 121
    target 122
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_125"
      target_id "M120_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 248
    source 122
    target 17
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_47"
      target_id "M120_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 249
    source 95
    target 123
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_6"
      target_id "M120_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 250
    source 123
    target 124
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_69"
      target_id "M120_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 251
    source 118
    target 125
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_124"
      target_id "M120_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 252
    source 125
    target 121
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_45"
      target_id "M120_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 253
    source 84
    target 126
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "CONSPUMPTION"
      source_id "M120_126"
      target_id "M120_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 254
    source 79
    target 126
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "TRIGGER"
      source_id "M120_112"
      target_id "M120_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 255
    source 126
    target 42
    cd19dm [
      diagram "C19DMap:Interferon lambda pathway"
      edge_type "PRODUCTION"
      source_id "M120_48"
      target_id "M120_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
