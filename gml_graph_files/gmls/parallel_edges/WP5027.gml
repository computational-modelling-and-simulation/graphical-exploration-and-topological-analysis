# generated with VANTED V2.8.2 at Fri Mar 04 09:57:02 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5027"
      full_annotation "urn:miriam:wikidata:Q90038952"
      hgnc "NA"
      map_id "W4_8"
      name "nsp1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e6110"
      uniprot "NA"
    ]
    graphics [
      x 83.49059754661147
      y 498.54575256746364
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_19"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idad839e9d"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 387.5350910667011
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5027"
      full_annotation "urn:miriam:ncbigene:8669"
      hgnc "NA"
      map_id "W4_10"
      name "EIF3J"
      node_subtype "GENE"
      node_type "species"
      org_id "efe7f"
      uniprot "NA"
    ]
    graphics [
      x 176.90520150452227
      y 385.96018722292274
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5027"
      full_annotation "urn:miriam:ncbigene:1983;urn:miriam:ncbigene:3646;urn:miriam:obo.chebi:CHEBI%3A13145;urn:miriam:ncbigene:1964;urn:miriam:ncbigene:8668;urn:miriam:ncbigene:8669;urn:miriam:ncbigene:10209;urn:miriam:ncbigene:8667;urn:miriam:ncbigene:8666;urn:miriam:ncbigene:8663;urn:miriam:ncbigene:8664;urn:miriam:ncbigene:8662;urn:miriam:ncbigene:8894;urn:miriam:ncbigene:1965;urn:miriam:ncbigene:8661;urn:miriam:ncbigene:1968;https://identifiers.org/complexportal:CPX-6036;urn:miriam:ncbigene:8665"
      hgnc "NA"
      map_id "W4_1"
      name "40S_space_cytosolic_space_small_space__br_ribosomal_space_subunit"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b327e"
      uniprot "NA"
    ]
    graphics [
      x 734.3346923009269
      y 130.385769274346
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_12"
      name "NA"
      node_subtype "UNKNOWN_POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "id19d08a66"
      uniprot "NA"
    ]
    graphics [
      x 631.1469518286962
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_7"
      name "Load_space_mRNA_space_and_space_start_space_translation"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "dfc42"
      uniprot "NA"
    ]
    graphics [
      x 521.1134352062534
      y 106.69474873504282
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5027"
      full_annotation "urn:miriam:ncbigene:8668;urn:miriam:ncbigene:8669;urn:miriam:ncbigene:8664;urn:miriam:ncbigene:8662;urn:miriam:ncbigene:3646;urn:miriam:ncbigene:8661;urn:miriam:ncbigene:8663;urn:miriam:ncbigene:8665;urn:miriam:ncbigene:8667;urn:miriam:ncbigene:8666"
      hgnc "NA"
      map_id "W4_2"
      name "Eukaryotic_space_translation_space_initiation_br_factor_space_3_space_complex_br_"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b97c5"
      uniprot "NA"
    ]
    graphics [
      x 547.4570089385876
      y 481.4254168848583
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_14"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id4b83f12a"
      uniprot "NA"
    ]
    graphics [
      x 636.9765745422916
      y 552.0691921085734
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5027"
      full_annotation "https://identifiers.org/complexportal:CPX-5223"
      hgnc "NA"
      map_id "W4_6"
      name "40S_space_cytosolic_space_small_space__br_ribosomal_space_subunit"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "dc35b"
      uniprot "NA"
    ]
    graphics [
      x 731.7335712624136
      y 638.8619155227226
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_16"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id78120f40"
      uniprot "NA"
    ]
    graphics [
      x 797.767883542143
      y 505.3786349665636
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5027"
      full_annotation "urn:miriam:ncbigene:8665;urn:miriam:ncbigene:10209;urn:miriam:ncbigene:3646;urn:miriam:ncbigene:1983;urn:miriam:ncbigene:8669;urn:miriam:ncbigene:8666;https://identifiers.org/complexportal:CPX-6036;urn:miriam:ncbigene:8667;urn:miriam:ncbigene:8661;urn:miriam:ncbigene:8668;urn:miriam:ncbigene:1964;urn:miriam:ncbigene:8664;urn:miriam:ncbigene:8663;urn:miriam:ncbigene:8662"
      hgnc "NA"
      map_id "W4_9"
      name "40S_space_cytosolic_space_small_space__br_ribosomal_space_subunit"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ed8b6"
      uniprot "NA"
    ]
    graphics [
      x 841.4381298244494
      y 368.3437788598978
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_20"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idb76f4fdf"
      uniprot "NA"
    ]
    graphics [
      x 802.2514785447836
      y 239.27142102267499
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5027"
      full_annotation "urn:miriam:ncbigene:1964"
      hgnc "NA"
      map_id "W4_3"
      name "EIF1A"
      node_subtype "GENE"
      node_type "species"
      org_id "ca957"
      uniprot "NA"
    ]
    graphics [
      x 527.0727321655665
      y 751.9634677256058
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_18"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id8da43876"
      uniprot "NA"
    ]
    graphics [
      x 660.5248832984157
      y 755.5032935613344
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5027"
      full_annotation "urn:miriam:ncbigene:10209"
      hgnc "NA"
      map_id "W4_11"
      name "EIF1"
      node_subtype "GENE"
      node_type "species"
      org_id "fea09"
      uniprot "NA"
    ]
    graphics [
      x 844.8205274604525
      y 849.893635691518
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_15"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id573935d6"
      uniprot "NA"
    ]
    graphics [
      x 816.6016176022717
      y 737.986416936761
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5027"
      full_annotation "urn:miriam:ncbigene:1965;urn:miriam:ncbigene:1968;urn:miriam:obo.chebi:CHEBI%3A13145;urn:miriam:ncbigene:8894"
      hgnc "NA"
      map_id "W4_4"
      name "Ternary_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d6fe1"
      uniprot "NA"
    ]
    graphics [
      x 1019.5225833632339
      y 477.66895028813997
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_13"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id2d00e6ed"
      uniprot "NA"
    ]
    graphics [
      x 964.8000684479489
      y 377.3307990475233
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5027"
      full_annotation "urn:miriam:ncbigene:1983"
      hgnc "NA"
      map_id "W4_5"
      name "EIF5"
      node_subtype "GENE"
      node_type "species"
      org_id "db339"
      uniprot "NA"
    ]
    graphics [
      x 567.8307485652215
      y 813.4558211547019
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5027"
      full_annotation "NA"
      hgnc "NA"
      map_id "W4_17"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id8b1ce7b7"
      uniprot "NA"
    ]
    graphics [
      x 605.0705670571408
      y 687.7879345697033
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W4_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 21
    source 1
    target 2
    cd19dm [
      diagram "WP5027"
      edge_type "CONSPUMPTION"
      source_id "W4_8"
      target_id "W4_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 22
    source 2
    target 3
    cd19dm [
      diagram "WP5027"
      edge_type "PRODUCTION"
      source_id "W4_19"
      target_id "W4_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 23
    source 4
    target 5
    cd19dm [
      diagram "WP5027"
      edge_type "CONSPUMPTION"
      source_id "W4_1"
      target_id "W4_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 24
    source 5
    target 6
    cd19dm [
      diagram "WP5027"
      edge_type "PRODUCTION"
      source_id "W4_12"
      target_id "W4_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 25
    source 7
    target 8
    cd19dm [
      diagram "WP5027"
      edge_type "CONSPUMPTION"
      source_id "W4_2"
      target_id "W4_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 26
    source 8
    target 9
    cd19dm [
      diagram "WP5027"
      edge_type "PRODUCTION"
      source_id "W4_14"
      target_id "W4_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 27
    source 9
    target 10
    cd19dm [
      diagram "WP5027"
      edge_type "CONSPUMPTION"
      source_id "W4_6"
      target_id "W4_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 28
    source 10
    target 11
    cd19dm [
      diagram "WP5027"
      edge_type "PRODUCTION"
      source_id "W4_16"
      target_id "W4_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 29
    source 11
    target 12
    cd19dm [
      diagram "WP5027"
      edge_type "CONSPUMPTION"
      source_id "W4_9"
      target_id "W4_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 30
    source 12
    target 4
    cd19dm [
      diagram "WP5027"
      edge_type "PRODUCTION"
      source_id "W4_20"
      target_id "W4_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 31
    source 13
    target 14
    cd19dm [
      diagram "WP5027"
      edge_type "CONSPUMPTION"
      source_id "W4_3"
      target_id "W4_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 32
    source 14
    target 9
    cd19dm [
      diagram "WP5027"
      edge_type "PRODUCTION"
      source_id "W4_18"
      target_id "W4_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 33
    source 15
    target 16
    cd19dm [
      diagram "WP5027"
      edge_type "CONSPUMPTION"
      source_id "W4_11"
      target_id "W4_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 34
    source 16
    target 9
    cd19dm [
      diagram "WP5027"
      edge_type "PRODUCTION"
      source_id "W4_15"
      target_id "W4_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 35
    source 17
    target 18
    cd19dm [
      diagram "WP5027"
      edge_type "CONSPUMPTION"
      source_id "W4_4"
      target_id "W4_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 36
    source 18
    target 11
    cd19dm [
      diagram "WP5027"
      edge_type "PRODUCTION"
      source_id "W4_13"
      target_id "W4_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 37
    source 19
    target 20
    cd19dm [
      diagram "WP5027"
      edge_type "CONSPUMPTION"
      source_id "W4_5"
      target_id "W4_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 38
    source 20
    target 9
    cd19dm [
      diagram "WP5027"
      edge_type "PRODUCTION"
      source_id "W4_17"
      target_id "W4_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
