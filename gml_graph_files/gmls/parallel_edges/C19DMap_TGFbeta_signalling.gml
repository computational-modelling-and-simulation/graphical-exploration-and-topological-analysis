# generated with VANTED V2.8.2 at Fri Mar 04 09:57:01 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:hgnc:10436;urn:miriam:uniprot:P23443;urn:miriam:uniprot:P23443;urn:miriam:ec-code:2.7.11.1;urn:miriam:ncbigene:6198;urn:miriam:ncbigene:6198;urn:miriam:ensembl:ENSG00000108443;urn:miriam:hgnc.symbol:RPS6KB1;urn:miriam:hgnc.symbol:RPS6KB1;urn:miriam:refseq:NM_003161"
      hgnc "HGNC_SYMBOL:RPS6KB1"
      map_id "M19_29"
      name "RPS6KB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa41"
      uniprot "UNIPROT:P23443"
    ]
    graphics [
      x 175.36158458355953
      y 693.765117894159
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_12"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10"
      uniprot "NA"
    ]
    graphics [
      x 144.5710672187344
      y 580.7321828958102
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:ncbigene:5518;urn:miriam:uniprot:P30153;urn:miriam:uniprot:P30153;urn:miriam:ncbigene:5518;urn:miriam:refseq:NM_014225;urn:miriam:hgnc:9302;urn:miriam:hgnc.symbol:PPP2R1A;urn:miriam:hgnc.symbol:PPP2R1A;urn:miriam:ensembl:ENSG00000105568;urn:miriam:ncbigene:5515;urn:miriam:ncbigene:5515;urn:miriam:ensembl:ENSG00000113575;urn:miriam:refseq:NM_002715;urn:miriam:ec-code:3.1.3.16;urn:miriam:uniprot:P67775;urn:miriam:uniprot:P67775;urn:miriam:hgnc:9299;urn:miriam:hgnc.symbol:PPP2CA;urn:miriam:hgnc.symbol:PPP2CA"
      hgnc "HGNC_SYMBOL:PPP2R1A;HGNC_SYMBOL:PPP2CA"
      map_id "M19_5"
      name "PP2A"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa16"
      uniprot "UNIPROT:P30153;UNIPROT:P67775"
    ]
    graphics [
      x 273.6074289687632
      y 528.5394652088653
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:hgnc:10436;urn:miriam:uniprot:P23443;urn:miriam:uniprot:P23443;urn:miriam:ec-code:2.7.11.1;urn:miriam:ncbigene:6198;urn:miriam:ncbigene:6198;urn:miriam:ensembl:ENSG00000108443;urn:miriam:hgnc.symbol:RPS6KB1;urn:miriam:hgnc.symbol:RPS6KB1;urn:miriam:refseq:NM_003161"
      hgnc "HGNC_SYMBOL:RPS6KB1"
      map_id "M19_28"
      name "RPS6KB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa40"
      uniprot "UNIPROT:P23443"
    ]
    graphics [
      x 62.5
      y 664.1675250205915
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:ensembl:ENSG00000175387;urn:miriam:hgnc:6768;urn:miriam:ncbigene:4087;urn:miriam:uniprot:Q15796;urn:miriam:uniprot:Q15796;urn:miriam:ncbigene:4087;urn:miriam:hgnc.symbol:SMAD2;urn:miriam:hgnc.symbol:SMAD2;urn:miriam:refseq:NM_005901;urn:miriam:hgnc:6769;urn:miriam:ncbigene:4088;urn:miriam:ncbigene:4088;urn:miriam:uniprot:P84022;urn:miriam:uniprot:P84022;urn:miriam:hgnc.symbol:SMAD3;urn:miriam:refseq:NM_005902;urn:miriam:hgnc.symbol:SMAD3;urn:miriam:ensembl:ENSG00000166949"
      hgnc "HGNC_SYMBOL:SMAD2;HGNC_SYMBOL:SMAD3"
      map_id "M19_9"
      name "SMAD2_slash_3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa5"
      uniprot "UNIPROT:Q15796;UNIPROT:P84022"
    ]
    graphics [
      x 893.828107312794
      y 628.8197952320089
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_14"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "re14"
      uniprot "NA"
    ]
    graphics [
      x 1002.9378714378547
      y 730.116318250439
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:obo.go:GO%3A0000074"
      hgnc "NA"
      map_id "M19_32"
      name "Modulation_space_of_space_cell_space_cycle"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa55"
      uniprot "NA"
    ]
    graphics [
      x 1134.037672090596
      y 741.071475358833
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:refseq:NM_000660;urn:miriam:ncbigene:7040;urn:miriam:ncbigene:7040;urn:miriam:hgnc:11766;urn:miriam:hgnc.symbol:TGFB1;urn:miriam:uniprot:P01137;urn:miriam:uniprot:P01137;urn:miriam:hgnc.symbol:TGFB1;urn:miriam:ensembl:ENSG00000105329"
      hgnc "HGNC_SYMBOL:TGFB1"
      map_id "M19_23"
      name "TGFB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa10"
      uniprot "UNIPROT:P01137"
    ]
    graphics [
      x 759.1195681278505
      y 229.20893648009547
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_17"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re32"
      uniprot "NA"
    ]
    graphics [
      x 666.2902037270039
      y 192.63945867798844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:ec-code:2.7.11.30;urn:miriam:refseq:NM_001024847;urn:miriam:hgnc:11773;urn:miriam:ensembl:ENSG00000163513;urn:miriam:ncbigene:7048;urn:miriam:ncbigene:7048;urn:miriam:uniprot:P37173;urn:miriam:uniprot:P37173;urn:miriam:hgnc.symbol:TGFBR2;urn:miriam:hgnc.symbol:TGFBR2;urn:miriam:ensembl:ENSG00000106799;urn:miriam:uniprot:P36897;urn:miriam:uniprot:P36897;urn:miriam:ncbigene:7046;urn:miriam:ncbigene:7046;urn:miriam:ec-code:2.7.11.30;urn:miriam:hgnc:11772;urn:miriam:hgnc.symbol:TGFBR1;urn:miriam:hgnc.symbol:TGFBR1;urn:miriam:refseq:NM_001130916"
      hgnc "HGNC_SYMBOL:TGFBR2;HGNC_SYMBOL:TGFBR1"
      map_id "M19_3"
      name "TGFBR"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa13"
      uniprot "UNIPROT:P37173;UNIPROT:P36897"
    ]
    graphics [
      x 743.9042147195664
      y 84.50220774214802
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:refseq:NM_206943;urn:miriam:hgnc.symbol:LTBP1;urn:miriam:ncbigene:4052;urn:miriam:hgnc.symbol:LTBP1;urn:miriam:ncbigene:4052;urn:miriam:uniprot:Q14766;urn:miriam:uniprot:Q14766;urn:miriam:hgnc:6714;urn:miriam:ensembl:ENSG00000049323"
      hgnc "HGNC_SYMBOL:LTBP1"
      map_id "M19_37"
      name "LTBP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa7"
      uniprot "UNIPROT:Q14766"
    ]
    graphics [
      x 781.3565847502351
      y 143.44219366073315
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:uniprot:Q13145;urn:miriam:uniprot:Q13145;urn:miriam:refseq:NM_012342;urn:miriam:ensembl:ENSG00000095739;urn:miriam:hgnc.symbol:BAMBI;urn:miriam:ncbigene:25805;urn:miriam:hgnc.symbol:BAMBI;urn:miriam:ncbigene:25805;urn:miriam:hgnc:30251"
      hgnc "HGNC_SYMBOL:BAMBI"
      map_id "M19_26"
      name "BAMBI"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa33"
      uniprot "UNIPROT:Q13145"
    ]
    graphics [
      x 845.0822007112529
      y 285.7376924933993
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:uniprot:Q80H93;urn:miriam:uniprot:Q7TFA0;urn:miriam:ncbigene:1489677;urn:miriam:ncbigene:1489676"
      hgnc "NA"
      map_id "M19_39"
      name "Orf8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa72"
      uniprot "UNIPROT:Q80H93;UNIPROT:Q7TFA0"
    ]
    graphics [
      x 685.5373491585359
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:refseq:NM_000660;urn:miriam:ncbigene:7040;urn:miriam:ncbigene:7040;urn:miriam:hgnc:11766;urn:miriam:hgnc.symbol:TGFB1;urn:miriam:uniprot:P01137;urn:miriam:uniprot:P01137;urn:miriam:hgnc.symbol:TGFB1;urn:miriam:ensembl:ENSG00000105329"
      hgnc "HGNC_SYMBOL:TGFB1"
      map_id "M19_1"
      name "TGFB_slash_TGFBR"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa11"
      uniprot "UNIPROT:P01137"
    ]
    graphics [
      x 560.1707462807788
      y 333.45116648778634
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:hgnc:667;urn:miriam:refseq:NM_001664;urn:miriam:ensembl:ENSG00000067560;urn:miriam:ec-code:3.6.5.2;urn:miriam:ncbigene:387;urn:miriam:ncbigene:387;urn:miriam:uniprot:P61586;urn:miriam:uniprot:P61586;urn:miriam:hgnc.symbol:RHOA;urn:miriam:hgnc.symbol:RHOA"
      hgnc "HGNC_SYMBOL:RHOA"
      map_id "M19_22"
      name "RHOA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1"
      uniprot "UNIPROT:P61586"
    ]
    graphics [
      x 520.7709105046812
      y 82.11339193323079
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_20"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re7"
      uniprot "NA"
    ]
    graphics [
      x 462.39962787569436
      y 195.06976221274698
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:uniprot:Q7TFA1;urn:miriam:ncbigene:1489675"
      hgnc "NA"
      map_id "M19_40"
      name "Nsp7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa76"
      uniprot "UNIPROT:Q7TFA1"
    ]
    graphics [
      x 350.96707435287925
      y 146.21352392708752
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:hgnc:667;urn:miriam:refseq:NM_001664;urn:miriam:ensembl:ENSG00000067560;urn:miriam:ec-code:3.6.5.2;urn:miriam:ncbigene:387;urn:miriam:ncbigene:387;urn:miriam:uniprot:P61586;urn:miriam:uniprot:P61586;urn:miriam:hgnc.symbol:RHOA;urn:miriam:hgnc.symbol:RHOA"
      hgnc "HGNC_SYMBOL:RHOA"
      map_id "M19_27"
      name "RHOA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa38"
      uniprot "UNIPROT:P61586"
    ]
    graphics [
      x 424.53976829258636
      y 72.78075223148778
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:ncbiprotein:BCD58762"
      hgnc "NA"
      map_id "M19_42"
      name "Orf10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa92"
      uniprot "NA"
    ]
    graphics [
      x 585.0011071857393
      y 1192.7480135815968
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_18"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re33"
      uniprot "NA"
    ]
    graphics [
      x 483.3982817085446
      y 1258.9087663856703
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094"
      hgnc "HGNC_SYMBOL:ELOB;HGNC_SYMBOL:RBX1;HGNC_SYMBOL:ELOC;HGNC_SYMBOL:CUL2"
      map_id "M19_2"
      name "E3_space_ubiquitin_space_ligase_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa12"
      uniprot "UNIPROT:Q15370;UNIPROT:P62877;UNIPROT:Q15369;UNIPROT:Q13617"
    ]
    graphics [
      x 588.4871463374105
      y 1306.0693342740933
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:uniprot:Q13617;urn:miriam:uniprot:Q13617;urn:miriam:hgnc:2552;urn:miriam:hgnc.symbol:CUL2;urn:miriam:hgnc.symbol:CUL2;urn:miriam:ncbigene:8453;urn:miriam:ncbigene:8453;urn:miriam:refseq:NM_003591;urn:miriam:ensembl:ENSG00000108094;urn:miriam:refseq:NM_007108;urn:miriam:hgnc.symbol:ELOB;urn:miriam:hgnc.symbol:ELOB;urn:miriam:ncbigene:6923;urn:miriam:ncbigene:6923;urn:miriam:ensembl:ENSG00000103363;urn:miriam:hgnc:11619;urn:miriam:uniprot:Q15370;urn:miriam:uniprot:Q15370;urn:miriam:hgnc.symbol:ELOC;urn:miriam:hgnc.symbol:ELOC;urn:miriam:uniprot:Q15369;urn:miriam:uniprot:Q15369;urn:miriam:ncbigene:6921;urn:miriam:ensembl:ENSG00000154582;urn:miriam:ncbigene:6921;urn:miriam:hgnc:11617;urn:miriam:refseq:NM_005648;urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387;urn:miriam:ncbiprotein:BCD58762"
      hgnc "HGNC_SYMBOL:CUL2;HGNC_SYMBOL:ELOB;HGNC_SYMBOL:ELOC;HGNC_SYMBOL:RBX1"
      map_id "M19_11"
      name "E3_space_ubiquitin_space_ligase_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa9"
      uniprot "UNIPROT:Q13617;UNIPROT:Q15370;UNIPROT:Q15369;UNIPROT:P62877"
    ]
    graphics [
      x 489.86094736135334
      y 1139.064199464426
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_15"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re20"
      uniprot "NA"
    ]
    graphics [
      x 792.3582873466094
      y 374.2985427901497
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:ncbigene:1489669"
      hgnc "NA"
      map_id "M19_34"
      name "Orf3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa65"
      uniprot "UNIPROT:P59632"
    ]
    graphics [
      x 908.2381151294678
      y 447.17618691634885
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_35"
      name "sa44_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa66"
      uniprot "NA"
    ]
    graphics [
      x 669.7542155157925
      y 367.73859735703036
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:hgnc:6767;urn:miriam:ncbigene:4086;urn:miriam:uniprot:Q15797;urn:miriam:uniprot:Q15797;urn:miriam:ncbigene:4086;urn:miriam:ensembl:ENSG00000170365;urn:miriam:hgnc.symbol:SMAD1;urn:miriam:refseq:NM_005900;urn:miriam:hgnc.symbol:SMAD1;urn:miriam:uniprot:Q99717;urn:miriam:uniprot:Q99717;urn:miriam:hgnc.symbol:SMAD5;urn:miriam:hgnc.symbol:SMAD5;urn:miriam:refseq:NM_005903;urn:miriam:hgnc:6771;urn:miriam:ncbigene:4090;urn:miriam:ncbigene:4090;urn:miriam:ensembl:ENSG00000113658;urn:miriam:hgnc:6774;urn:miriam:ncbigene:4093;urn:miriam:ncbigene:4093;urn:miriam:hgnc.symbol:SMAD9;urn:miriam:ensembl:ENSG00000120693;urn:miriam:hgnc.symbol:SMAD9;urn:miriam:refseq:NM_005905;urn:miriam:uniprot:O15198;urn:miriam:uniprot:O15198"
      hgnc "HGNC_SYMBOL:SMAD1;HGNC_SYMBOL:SMAD5;HGNC_SYMBOL:SMAD9"
      map_id "M19_6"
      name "SMAD1_slash_5_slash_8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa2"
      uniprot "UNIPROT:Q15797;UNIPROT:Q99717;UNIPROT:O15198"
    ]
    graphics [
      x 1140.5498422188807
      y 319.18331898823186
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_16"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re3"
      uniprot "NA"
    ]
    graphics [
      x 1021.1807835098964
      y 335.0536673611631
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:hgnc:169;urn:miriam:hgnc.symbol:ACTR2;urn:miriam:hgnc.symbol:ACTR2;urn:miriam:uniprot:P61160;urn:miriam:uniprot:P61160;urn:miriam:ncbigene:10097;urn:miriam:ncbigene:10097;urn:miriam:ensembl:ENSG00000138071;urn:miriam:refseq:NM_001005386;urn:miriam:ncbigene:659;urn:miriam:ncbigene:659;urn:miriam:ensembl:ENSG00000204217;urn:miriam:hgnc:1078;urn:miriam:ec-code:2.7.11.30;urn:miriam:uniprot:Q13873;urn:miriam:uniprot:Q13873;urn:miriam:refseq:NM_001204;urn:miriam:hgnc.symbol:BMPR2;urn:miriam:hgnc.symbol:BMPR2"
      hgnc "HGNC_SYMBOL:ACTR2;HGNC_SYMBOL:BMPR2"
      map_id "M19_10"
      name "BMPR1_slash_2_slash_ACTR2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa7"
      uniprot "UNIPROT:P61160;UNIPROT:Q13873"
    ]
    graphics [
      x 1025.4384332736176
      y 453.9737089443919
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:ec-code:2.7.11.24;urn:miriam:ensembl:ENSG00000102882;urn:miriam:hgnc:6877;urn:miriam:hgnc.symbol:MAPK3;urn:miriam:hgnc.symbol:MAPK3;urn:miriam:refseq:NM_001040056;urn:miriam:ncbigene:5595;urn:miriam:ncbigene:5595;urn:miriam:uniprot:P27361;urn:miriam:uniprot:P27361"
      hgnc "HGNC_SYMBOL:MAPK3"
      map_id "M19_25"
      name "MAPK3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa18"
      uniprot "UNIPROT:P27361"
    ]
    graphics [
      x 899.2738849767732
      y 373.60656246781065
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:uniprot:Q7TFA1;urn:miriam:ncbigene:1489675"
      hgnc "NA"
      map_id "M19_38"
      name "Nsp7b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa70"
      uniprot "UNIPROT:Q7TFA1"
    ]
    graphics [
      x 1106.942844520785
      y 410.0230933565083
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:uniprot:P59637;urn:miriam:ncbigene:1489671;urn:miriam:hgnc.symbol:E"
      hgnc "HGNC_SYMBOL:E"
      map_id "M19_36"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa69"
      uniprot "UNIPROT:P59637"
    ]
    graphics [
      x 1074.8469365996943
      y 225.9883873616899
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:hgnc:6767;urn:miriam:ncbigene:4086;urn:miriam:uniprot:Q15797;urn:miriam:uniprot:Q15797;urn:miriam:ncbigene:4086;urn:miriam:ensembl:ENSG00000170365;urn:miriam:hgnc.symbol:SMAD1;urn:miriam:refseq:NM_005900;urn:miriam:hgnc.symbol:SMAD1;urn:miriam:hgnc:6774;urn:miriam:ncbigene:4093;urn:miriam:ncbigene:4093;urn:miriam:hgnc.symbol:SMAD9;urn:miriam:ensembl:ENSG00000120693;urn:miriam:hgnc.symbol:SMAD9;urn:miriam:refseq:NM_005905;urn:miriam:uniprot:O15198;urn:miriam:uniprot:O15198;urn:miriam:uniprot:Q99717;urn:miriam:uniprot:Q99717;urn:miriam:hgnc.symbol:SMAD5;urn:miriam:hgnc.symbol:SMAD5;urn:miriam:refseq:NM_005903;urn:miriam:hgnc:6771;urn:miriam:ncbigene:4090;urn:miriam:ncbigene:4090;urn:miriam:ensembl:ENSG00000113658"
      hgnc "HGNC_SYMBOL:SMAD1;HGNC_SYMBOL:SMAD9;HGNC_SYMBOL:SMAD5"
      map_id "M19_7"
      name "SMAD1_slash_5_slash_8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa3"
      uniprot "UNIPROT:Q15797;UNIPROT:O15198;UNIPROT:Q99717"
    ]
    graphics [
      x 1143.3929545891838
      y 487.12936197717886
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:ensembl:ENSG00000175387;urn:miriam:hgnc:6768;urn:miriam:ncbigene:4087;urn:miriam:uniprot:Q15796;urn:miriam:uniprot:Q15796;urn:miriam:ncbigene:4087;urn:miriam:hgnc.symbol:SMAD2;urn:miriam:hgnc.symbol:SMAD2;urn:miriam:refseq:NM_005901;urn:miriam:hgnc:6769;urn:miriam:ncbigene:4088;urn:miriam:ncbigene:4088;urn:miriam:uniprot:P84022;urn:miriam:uniprot:P84022;urn:miriam:hgnc.symbol:SMAD3;urn:miriam:refseq:NM_005902;urn:miriam:hgnc.symbol:SMAD3;urn:miriam:ensembl:ENSG00000166949"
      hgnc "HGNC_SYMBOL:SMAD2;HGNC_SYMBOL:SMAD3"
      map_id "M19_8"
      name "SMAD2_slash_3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa4"
      uniprot "UNIPROT:Q15796;UNIPROT:P84022"
    ]
    graphics [
      x 651.90716645846
      y 457.84182371632085
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_19"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re6"
      uniprot "NA"
    ]
    graphics [
      x 771.1990159868319
      y 469.6796639255697
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387"
      hgnc "HGNC_SYMBOL:RBX1"
      map_id "M19_24"
      name "RBX1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa11"
      uniprot "UNIPROT:P62877"
    ]
    graphics [
      x 654.5607676767668
      y 531.7865214419384
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:ncbigene:90;urn:miriam:ncbigene:90;urn:miriam:hgnc:171;urn:miriam:ec-code:2.7.11.30;urn:miriam:uniprot:Q04771;urn:miriam:uniprot:Q04771;urn:miriam:hgnc.symbol:ACVR1;urn:miriam:refseq:NM_001105;urn:miriam:hgnc.symbol:ACVR1;urn:miriam:ensembl:ENSG00000115170"
      hgnc "HGNC_SYMBOL:ACVR1"
      map_id "M19_30"
      name "ACVR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa44"
      uniprot "UNIPROT:Q04771"
    ]
    graphics [
      x 767.5935745346835
      y 603.8421673494513
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:hgnc.symbol:ACVR1B;urn:miriam:uniprot:P36896;urn:miriam:uniprot:P36896;urn:miriam:hgnc.symbol:ACVR1B;urn:miriam:ec-code:2.7.11.30;urn:miriam:ncbigene:91;urn:miriam:ncbigene:91;urn:miriam:hgnc:172;urn:miriam:ensembl:ENSG00000135503;urn:miriam:refseq:NM_020328"
      hgnc "HGNC_SYMBOL:ACVR1B"
      map_id "M19_31"
      name "ACVR1B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa45"
      uniprot "UNIPROT:P36896"
    ]
    graphics [
      x 700.3119905044352
      y 584.7708124629436
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:uniprot:Q7TFA1;urn:miriam:ncbigene:1489675"
      hgnc "NA"
      map_id "M19_33"
      name "Nsp7b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa64"
      uniprot "UNIPROT:Q7TFA1"
    ]
    graphics [
      x 833.2947268822938
      y 563.3469636856366
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_13"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "re13"
      uniprot "NA"
    ]
    graphics [
      x 1192.8176425772162
      y 623.4523304637192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:ncbigene:5518;urn:miriam:uniprot:P30153;urn:miriam:uniprot:P30153;urn:miriam:ncbigene:5518;urn:miriam:refseq:NM_014225;urn:miriam:hgnc:9302;urn:miriam:hgnc.symbol:PPP2R1A;urn:miriam:hgnc.symbol:PPP2R1A;urn:miriam:ensembl:ENSG00000105568;urn:miriam:ncbigene:5515;urn:miriam:ncbigene:5515;urn:miriam:ensembl:ENSG00000113575;urn:miriam:refseq:NM_002715;urn:miriam:ec-code:3.1.3.16;urn:miriam:uniprot:P67775;urn:miriam:uniprot:P67775;urn:miriam:hgnc:9299;urn:miriam:hgnc.symbol:PPP2CA;urn:miriam:hgnc.symbol:PPP2CA"
      hgnc "HGNC_SYMBOL:PPP2R1A;HGNC_SYMBOL:PPP2CA"
      map_id "M19_4"
      name "PP2A"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa15"
      uniprot "UNIPROT:P30153;UNIPROT:P67775"
    ]
    graphics [
      x 282.83490134461783
      y 421.8017223216722
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_21"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re8"
      uniprot "NA"
    ]
    graphics [
      x 401.27298987621657
      y 433.92000571749463
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:uniprot:P59635;urn:miriam:ncbigene:1489674"
      hgnc "NA"
      map_id "M19_41"
      name "Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa84"
      uniprot "UNIPROT:P59635"
    ]
    graphics [
      x 331.05677561234904
      y 338.70151383559653
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 43
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "M19_29"
      target_id "M19_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 44
    source 3
    target 2
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CATALYSIS"
      source_id "M19_5"
      target_id "M19_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 45
    source 2
    target 4
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PRODUCTION"
      source_id "M19_12"
      target_id "M19_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 46
    source 5
    target 6
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "M19_9"
      target_id "M19_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 47
    source 6
    target 7
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PRODUCTION"
      source_id "M19_14"
      target_id "M19_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 48
    source 8
    target 9
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "M19_23"
      target_id "M19_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 49
    source 10
    target 9
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "M19_3"
      target_id "M19_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 50
    source 11
    target 9
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "INHIBITION"
      source_id "M19_37"
      target_id "M19_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 51
    source 12
    target 9
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "INHIBITION"
      source_id "M19_26"
      target_id "M19_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 52
    source 13
    target 9
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M19_39"
      target_id "M19_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 53
    source 9
    target 14
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PRODUCTION"
      source_id "M19_17"
      target_id "M19_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 54
    source 15
    target 16
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "M19_22"
      target_id "M19_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 55
    source 14
    target 16
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M19_1"
      target_id "M19_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 56
    source 17
    target 16
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M19_40"
      target_id "M19_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 57
    source 16
    target 18
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PRODUCTION"
      source_id "M19_20"
      target_id "M19_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 58
    source 19
    target 20
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "M19_42"
      target_id "M19_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 59
    source 21
    target 20
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "M19_2"
      target_id "M19_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 60
    source 20
    target 22
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PRODUCTION"
      source_id "M19_18"
      target_id "M19_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 61
    source 12
    target 23
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "M19_26"
      target_id "M19_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 62
    source 24
    target 23
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M19_34"
      target_id "M19_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 63
    source 23
    target 25
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PRODUCTION"
      source_id "M19_15"
      target_id "M19_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 64
    source 26
    target 27
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "M19_6"
      target_id "M19_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 12
    target 27
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "INHIBITION"
      source_id "M19_26"
      target_id "M19_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 28
    target 27
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CATALYSIS"
      source_id "M19_10"
      target_id "M19_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 29
    target 27
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "INHIBITION"
      source_id "M19_25"
      target_id "M19_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 24
    target 27
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M19_34"
      target_id "M19_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 69
    source 30
    target 27
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M19_38"
      target_id "M19_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 70
    source 31
    target 27
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M19_36"
      target_id "M19_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 71
    source 27
    target 32
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PRODUCTION"
      source_id "M19_16"
      target_id "M19_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 72
    source 33
    target 34
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "M19_8"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 73
    source 12
    target 34
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "INHIBITION"
      source_id "M19_26"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 74
    source 35
    target 34
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "INHIBITION"
      source_id "M19_24"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 75
    source 14
    target 34
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M19_1"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 76
    source 36
    target 34
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CATALYSIS"
      source_id "M19_30"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 77
    source 37
    target 34
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CATALYSIS"
      source_id "M19_31"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 78
    source 29
    target 34
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "INHIBITION"
      source_id "M19_25"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 79
    source 24
    target 34
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M19_34"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 38
    target 34
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M19_33"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 81
    source 34
    target 5
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PRODUCTION"
      source_id "M19_19"
      target_id "M19_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 82
    source 32
    target 39
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "M19_7"
      target_id "M19_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 83
    source 39
    target 7
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PRODUCTION"
      source_id "M19_13"
      target_id "M19_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 84
    source 40
    target 41
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "M19_4"
      target_id "M19_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 85
    source 14
    target 41
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M19_1"
      target_id "M19_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 86
    source 42
    target 41
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M19_41"
      target_id "M19_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 87
    source 41
    target 3
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PRODUCTION"
      source_id "M19_21"
      target_id "M19_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
