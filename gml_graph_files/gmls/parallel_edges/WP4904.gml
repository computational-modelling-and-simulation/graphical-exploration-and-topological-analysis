# generated with VANTED V2.8.2 at Fri Mar 04 09:56:59 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:ensembl:ENSG00000267690"
      hgnc "NA"
      map_id "W23_7"
      name "LDLRAD4_minus_AS1"
      node_subtype "RNA"
      node_type "species"
      org_id "c9042"
      uniprot "NA"
    ]
    graphics [
      x 192.08240442575834
      y 224.4256208212453
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:32111819"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_27"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "idfb4f20fd"
      uniprot "NA"
    ]
    graphics [
      x 217.2710177812516
      y 336.8738008935977
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:ensembl:ENSG00000168675"
      hgnc "NA"
      map_id "W23_14"
      name "LDLRAD4"
      node_subtype "GENE"
      node_type "species"
      org_id "e9bfd"
      uniprot "NA"
    ]
    graphics [
      x 311.5480829916645
      y 435.75367913381575
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "PUBMED:31819986"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_28"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "idfdf4dec2"
      uniprot "NA"
    ]
    graphics [
      x 258.83686145424014
      y 614.6433678492833
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:mirbase.mature:MIMAT0000075"
      hgnc "NA"
      map_id "W23_15"
      name "hsa_minus_miR_minus_20a_minus_5p"
      node_subtype "RNA"
      node_type "species"
      org_id "f25b2"
      uniprot "NA"
    ]
    graphics [
      x 283.69945015237516
      y 773.924235230329
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:ensembl:ENSG00000069869"
      hgnc "NA"
      map_id "W23_5"
      name "NEDD4"
      node_subtype "GENE"
      node_type "species"
      org_id "c276a"
      uniprot "NA"
    ]
    graphics [
      x 635.7174415430782
      y 259.67550169901676
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_22"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id71ecd0f3"
      uniprot "NA"
    ]
    graphics [
      x 725.0369321867249
      y 162.5725081350693
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:wikipathways:WP4217"
      hgnc "NA"
      map_id "W23_3"
      name "Ebola_space_virus_br_pathway_space_in_space_host"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "b8756"
      uniprot "NA"
    ]
    graphics [
      x 782.5640777299016
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:ensembl:ENSG00000163513;urn:miriam:ensembl:ENSG00000106799"
      hgnc "NA"
      map_id "W23_12"
      name "e3254"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e3254"
      uniprot "NA"
    ]
    graphics [
      x 916.3225081331652
      y 499.31714308648753
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "PUBMED:29945215;PUBMED:24627487;PUBMED:24438557"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_24"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ide33a2255"
      uniprot "NA"
    ]
    graphics [
      x 1018.2183394435332
      y 554.4000952119053
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:ensembl:ENSG00000124225;urn:miriam:ensembl:ENSG00000168675"
      hgnc "NA"
      map_id "W23_2"
      name "b0004"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b0004"
      uniprot "NA"
    ]
    graphics [
      x 915.4027485410621
      y 614.9303487507119
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:ensembl:ENSG00000175387"
      hgnc "NA"
      map_id "W23_10"
      name "df78d"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "df78d"
      uniprot "NA"
    ]
    graphics [
      x 1036.46221849513
      y 665.3402602962711
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_1"
      name "Autophagosome_br_formation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "a5486"
      uniprot "NA"
    ]
    graphics [
      x 713.4433022227869
      y 1002.1197502550442
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "PUBMED:26165754"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_19"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id694a53fa"
      uniprot "NA"
    ]
    graphics [
      x 593.1963239295744
      y 1032.8448943650246
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:ensembl:ENSG00000085978"
      hgnc "NA"
      map_id "W23_16"
      name "ATG16L1"
      node_subtype "GENE"
      node_type "species"
      org_id "f314c"
      uniprot "NA"
    ]
    graphics [
      x 466.21931347541954
      y 991.6692107959882
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_11"
      name "Neurodevelopmental_br_disorders"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "e1533"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 445.6968351571569
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "PUBMED:31602316"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_21"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id6fa109c9"
      uniprot "NA"
    ]
    graphics [
      x 176.02706004920964
      y 461.24678833980937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_26"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idf54d419"
      uniprot "NA"
    ]
    graphics [
      x 430.20661950955906
      y 483.2468846824778
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:ensembl:ENSG00000168675"
      hgnc "NA"
      map_id "W23_8"
      name "LDLRAD4"
      node_subtype "GENE"
      node_type "species"
      org_id "ca96b"
      uniprot "NA"
    ]
    graphics [
      x 522.1883322090816
      y 556.5251242698905
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_23"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id7947a72f"
      uniprot "NA"
    ]
    graphics [
      x 340.4700381383841
      y 307.50962830103845
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:ensembl:ENSG00000168675"
      hgnc "NA"
      map_id "W23_9"
      name "LDLRAD4"
      node_subtype "GENE"
      node_type "species"
      org_id "d98b8"
      uniprot "NA"
    ]
    graphics [
      x 390.57915849982487
      y 202.04450440568723
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "PUBMED:28888937"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_20"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id6cc9fb0d"
      uniprot "NA"
    ]
    graphics [
      x 481.0449587839293
      y 337.1069011352182
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:wikipathways:WP3874"
      hgnc "NA"
      map_id "W23_4"
      name "TGF_minus_B_space_signaling_br_pathway"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "bab41"
      uniprot "NA"
    ]
    graphics [
      x 583.168176969067
      y 388.736273980058
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_13"
      name "Blood_br_pressure"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "e3ffd"
      uniprot "NA"
    ]
    graphics [
      x 385.9877569076656
      y 658.5783619148013
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "PUBMED:19430479"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_25"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "idef60a322"
      uniprot "NA"
    ]
    graphics [
      x 335.64203267266157
      y 554.4205075452671
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "PUBMED:26165754"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_17"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id3b8d16d5"
      uniprot "NA"
    ]
    graphics [
      x 359.3878355813091
      y 901.3138822694511
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4904"
      full_annotation "NA"
      hgnc "NA"
      map_id "W23_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id4fe61d16"
      uniprot "NA"
    ]
    graphics [
      x 774.6174708321619
      y 284.51889013057564
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4904"
      full_annotation "urn:miriam:ensembl:ENSG00000069869"
      hgnc "NA"
      map_id "W23_6"
      name "NEDD4"
      node_subtype "GENE"
      node_type "species"
      org_id "c628c"
      uniprot "NA"
    ]
    graphics [
      x 899.1808343475443
      y 309.0406990850687
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W23_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 29
    source 1
    target 2
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "W23_7"
      target_id "W23_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 30
    source 2
    target 3
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_27"
      target_id "W23_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 31
    source 3
    target 4
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "W23_14"
      target_id "W23_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 32
    source 4
    target 5
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_28"
      target_id "W23_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 33
    source 6
    target 7
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "W23_5"
      target_id "W23_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 34
    source 7
    target 8
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_22"
      target_id "W23_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 35
    source 9
    target 10
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "W23_12"
      target_id "W23_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 36
    source 11
    target 10
    cd19dm [
      diagram "WP4904"
      edge_type "INHIBITION"
      source_id "W23_2"
      target_id "W23_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 37
    source 10
    target 12
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_24"
      target_id "W23_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 38
    source 13
    target 14
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "W23_1"
      target_id "W23_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 39
    source 14
    target 15
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_19"
      target_id "W23_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 40
    source 16
    target 17
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "W23_11"
      target_id "W23_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 41
    source 17
    target 3
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_21"
      target_id "W23_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 42
    source 3
    target 18
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "W23_14"
      target_id "W23_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 43
    source 18
    target 19
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_26"
      target_id "W23_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 44
    source 3
    target 20
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "W23_14"
      target_id "W23_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 45
    source 20
    target 21
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_23"
      target_id "W23_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 46
    source 3
    target 22
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "W23_14"
      target_id "W23_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 47
    source 22
    target 6
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_20"
      target_id "W23_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 48
    source 22
    target 23
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_20"
      target_id "W23_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 49
    source 24
    target 25
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "W23_13"
      target_id "W23_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 50
    source 25
    target 3
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_25"
      target_id "W23_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 51
    source 5
    target 26
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "W23_15"
      target_id "W23_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 52
    source 26
    target 15
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_17"
      target_id "W23_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 53
    source 6
    target 27
    cd19dm [
      diagram "WP4904"
      edge_type "CONSPUMPTION"
      source_id "W23_5"
      target_id "W23_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 54
    source 27
    target 28
    cd19dm [
      diagram "WP4904"
      edge_type "PRODUCTION"
      source_id "W23_18"
      target_id "W23_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
