# generated with VANTED V2.8.2 at Fri Mar 04 09:56:59 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 1
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:ncbigene:43740572"
      hgnc "NA"
      map_id "W1_109"
      name "ORF6"
      node_subtype "GENE"
      node_type "species"
      org_id "fc02d"
      uniprot "NA"
    ]
    graphics [
      x 1104.40439815035
      y 925.7311355822145
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_144"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ida427ca0"
      uniprot "NA"
    ]
    graphics [
      x 1133.5732592714105
      y 1028.301035822104
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_144"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q89226299"
      hgnc "NA"
      map_id "W1_32"
      name "ORF6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bce0b"
      uniprot "NA"
    ]
    graphics [
      x 1232.8495510191337
      y 1007.4521362736982
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_62"
      name "Endocytosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "d77e4"
      uniprot "NA"
    ]
    graphics [
      x 1583.4653859661325
      y 1626.6751303050212
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_128"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id5dadb7eb"
      uniprot "NA"
    ]
    graphics [
      x 1477.090712751663
      y 1627.0066045746094
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_3"
      name "NA"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "a479d"
      uniprot "NA"
    ]
    graphics [
      x 1373.4236481177186
      y 1664.9633824393234
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:ncbigene:43740578"
      hgnc "NA"
      map_id "W1_15"
      name "orf1"
      node_subtype "GENE"
      node_type "species"
      org_id "a9a90"
      uniprot "NA"
    ]
    graphics [
      x 1329.0386453601188
      y 765.298909478113
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_124"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id53ccbca1"
      uniprot "NA"
    ]
    graphics [
      x 1435.481845762858
      y 719.6367538942113
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_124"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:uniprot:P0DTD1"
      hgnc "NA"
      map_id "W1_52"
      name "orf1ab"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "cda8f"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1516.4171752510954
      y 789.3205943179673
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:uniprot:P0DTC2"
      hgnc "NA"
      map_id "W1_36"
      name "trimer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c25c7"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 1263.5639746702734
      y 2106.832201076045
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_159"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "idd6e4d05b"
      uniprot "NA"
    ]
    graphics [
      x 1296.114341426789
      y 1992.6732101134423
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_159"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:uniprot:Q9BYF1"
      hgnc "NA"
      map_id "W1_96"
      name "recognition_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f1172"
      uniprot "UNIPROT:P0DTC2;UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1359.8207791380169
      y 1865.0632936817249
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:uniprot:P0DTD1"
      hgnc "NA"
      map_id "W1_59"
      name "orf1ab"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d244b"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 769.8842185110352
      y 1246.357531726648
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_97"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f262e"
      uniprot "NA"
    ]
    graphics [
      x 775.6989491184409
      y 1004.2679355509065
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q87917572"
      hgnc "NA"
      map_id "W1_70"
      name "nsp10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "de394"
      uniprot "NA"
    ]
    graphics [
      x 821.144458872388
      y 763.9691160350635
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:uniprot:P0DTC3"
      hgnc "NA"
      map_id "W1_49"
      name "ORF3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "cb0cc"
      uniprot "UNIPROT:P0DTC3"
    ]
    graphics [
      x 2191.6359392414247
      y 1051.1206123811355
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_129"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id5ff07442"
      uniprot "NA"
    ]
    graphics [
      x 2286.2474451530156
      y 1117.6997861645748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_129"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikipathways:WP4876"
      hgnc "NA"
      map_id "W1_108"
      name "Activation_space_of_space__br_NLRP3_br_inflammasome"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "fb6cf"
      uniprot "NA"
    ]
    graphics [
      x 2197.274236228859
      y 1191.217829208533
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_10"
      name "Membrane_br_fusion"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "a7d58"
      uniprot "NA"
    ]
    graphics [
      x 1113.8810202612383
      y 628.5055186440563
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_121"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id3d108ea2"
      uniprot "NA"
    ]
    graphics [
      x 1215.6888825831727
      y 709.5569770676019
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:ncbigene:43740578"
      hgnc "NA"
      map_id "W1_54"
      name "orf1"
      node_subtype "GENE"
      node_type "species"
      org_id "ce268"
      uniprot "NA"
    ]
    graphics [
      x 1199.4666878555208
      y 861.8866341985683
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:uniprot:P0DTC2"
      hgnc "NA"
      map_id "W1_82"
      name "surface_br_glycoprotein_space_S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e7798"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 354.3074807271156
      y 596.5125951268045
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "PUBMED:32142651;PUBMED:32917722;PUBMED:32125455;PUBMED:32132184"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_126"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id596b2488"
      uniprot "NA"
    ]
    graphics [
      x 214.54084389284048
      y 793.5503618841012
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16393"
      hgnc "NA"
      map_id "W1_114"
      name "sphingosine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "ffcdb"
      uniprot "NA"
    ]
    graphics [
      x 280.371268259927
      y 908.2466075357296
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:uniprot:Q9BYF1"
      hgnc "NA"
      map_id "W1_74"
      name "ACE2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e154d"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 217.6272448001656
      y 938.1009400959296
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_22"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "aec54"
      uniprot "NA"
    ]
    graphics [
      x 869.0353674560598
      y 1036.20260794786
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q88659350"
      hgnc "NA"
      map_id "W1_39"
      name "nsp8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c43c8"
      uniprot "NA"
    ]
    graphics [
      x 925.9199239228466
      y 830.416246170081
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q90038952;urn:miriam:wikidata:Q109653022"
      hgnc "NA"
      map_id "W1_41"
      name "40S"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c60ca"
      uniprot "NA"
    ]
    graphics [
      x 452.5006207143184
      y 1352.7583005445686
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "PUBMED:32680882"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_116"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id1db68f7d"
      uniprot "NA"
    ]
    graphics [
      x 560.0897968795703
      y 1343.6972313862464
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_99"
      name "Protein_space_expression"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "f6e0c"
      uniprot "NA"
    ]
    graphics [
      x 693.0515356233932
      y 1313.9300834427343
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:ncbigene:43740576"
      hgnc "NA"
      map_id "W1_31"
      name "ORF10"
      node_subtype "GENE"
      node_type "species"
      org_id "bc696"
      uniprot "NA"
    ]
    graphics [
      x 2158.908449221569
      y 718.3259806481146
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_145"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ida46f2e34"
      uniprot "NA"
    ]
    graphics [
      x 2189.0467027764535
      y 606.1646597512649
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_145"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q89227548"
      hgnc "NA"
      map_id "W1_53"
      name "ORF10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "cdb2d"
      uniprot "NA"
    ]
    graphics [
      x 2072.9550627205044
      y 624.7012254216947
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q87917579"
      hgnc "NA"
      map_id "W1_12"
      name "nsp15"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a9205"
      uniprot "NA"
    ]
    graphics [
      x 602.4596074037836
      y 858.8318458589351
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_115"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id126968be"
      uniprot "NA"
    ]
    graphics [
      x 573.2297311191214
      y 677.8026488383226
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikipathways:WP4861"
      hgnc "NA"
      map_id "W1_8"
      name "Integrative_space_stress_br_response"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "a679a"
      uniprot "NA"
    ]
    graphics [
      x 609.3429896072087
      y 538.5462489045718
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16113"
      hgnc "NA"
      map_id "W1_1"
      name "cholesterol"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "a1c2c"
      uniprot "NA"
    ]
    graphics [
      x 1900.8367513283717
      y 873.9445584458038
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "PUBMED:32944968"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_166"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "idff4da966"
      uniprot "NA"
    ]
    graphics [
      x 1820.932338595846
      y 955.3247325948148
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_166"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:ensembl:ENSG00000075239"
      hgnc "NA"
      map_id "W1_95"
      name "ACAT"
      node_subtype "GENE"
      node_type "species"
      org_id "f0ad5"
      uniprot "NA"
    ]
    graphics [
      x 1685.4578549144162
      y 1022.6351351339638
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16113"
      hgnc "NA"
      map_id "W1_23"
      name "cholesterol"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "b41ce"
      uniprot "NA"
    ]
    graphics [
      x 1813.0897178408281
      y 837.2283907465678
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "PUBMED:32438371"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_160"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "idd97096a5"
      uniprot "NA"
    ]
    graphics [
      x 966.313714689729
      y 627.3653175227512
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_160"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q88659350"
      hgnc "NA"
      map_id "W1_79"
      name "nsp8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e5b7f"
      uniprot "NA"
    ]
    graphics [
      x 1022.7660458746514
      y 494.6989543807663
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_165"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ide4f56776"
      uniprot "NA"
    ]
    graphics [
      x 1481.0214457830089
      y 1758.4300259710167
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_165"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q87917581"
      hgnc "NA"
      map_id "W1_84"
      name "PL2_minus_PRO"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e9d37"
      uniprot "NA"
    ]
    graphics [
      x 289.4540842386692
      y 1308.2086464284864
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "PUBMED:23943763"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_155"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "idc71222d4"
      uniprot "NA"
    ]
    graphics [
      x 138.63741786241917
      y 1318.2701163492075
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_155"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q87917581"
      hgnc "NA"
      map_id "W1_93"
      name "PL2_minus_PRO"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f03df"
      uniprot "NA"
    ]
    graphics [
      x 62.499999999999886
      y 1230.6427497243687
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_151"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "idb8ba3d51"
      uniprot "NA"
    ]
    graphics [
      x 723.4394775057076
      y 715.9678194912303
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_151"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q87917572"
      hgnc "NA"
      map_id "W1_14"
      name "nsp10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a99b9"
      uniprot "NA"
    ]
    graphics [
      x 623.3633517520223
      y 757.3526857384453
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:ncbigene:43740573"
      hgnc "NA"
      map_id "W1_28"
      name "ORF7a"
      node_subtype "GENE"
      node_type "species"
      org_id "bafb8"
      uniprot "NA"
    ]
    graphics [
      x 1430.612101843077
      y 1908.4073383051161
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_127"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id5c4ff7f0"
      uniprot "NA"
    ]
    graphics [
      x 1531.4923294794662
      y 1953.3902659745108
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_127"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q88658500"
      hgnc "NA"
      map_id "W1_112"
      name "ORF7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ff49a"
      uniprot "NA"
    ]
    graphics [
      x 1583.1496314384372
      y 1855.5924434034625
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q89686805;urn:miriam:pubmed:32592996"
      hgnc "NA"
      map_id "W1_9"
      name "nsp9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a7c94"
      uniprot "NA"
    ]
    graphics [
      x 458.0949319415962
      y 868.8636232191379
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_134"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id7a78fc75"
      uniprot "NA"
    ]
    graphics [
      x 344.72747878571863
      y 726.07119460496
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q89686805"
      hgnc "NA"
      map_id "W1_56"
      name "nsp9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "cf2d5"
      uniprot "NA"
    ]
    graphics [
      x 191.46644610193232
      y 733.709335294104
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_63"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "d78c0"
      uniprot "NA"
    ]
    graphics [
      x 505.55405351637944
      y 1264.1969618723697
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_45"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "c849c"
      uniprot "NA"
    ]
    graphics [
      x 864.682849702428
      y 1389.7877047631687
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:uniprot:P0DTD1"
      hgnc "NA"
      map_id "W1_34"
      name "orf1ab"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c185d"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 950.7132859752235
      y 1488.8569130437897
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_103"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f8ee8"
      uniprot "NA"
    ]
    graphics [
      x 884.4023630645833
      y 1324.9036391065324
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q87917582;urn:miriam:pubmed:32529116"
      hgnc "NA"
      map_id "W1_80"
      name "3CL_minus_PRO"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e5b99"
      uniprot "NA"
    ]
    graphics [
      x 996.7298081894453
      y 1398.031792847729
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_38"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "c3f71"
      uniprot "NA"
    ]
    graphics [
      x 609.1428523908473
      y 1425.7173143413836
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q90038956"
      hgnc "NA"
      map_id "W1_72"
      name "nsp4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e019b"
      uniprot "NA"
    ]
    graphics [
      x 477.90685269951007
      y 1557.885468254197
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:ncbigene:43740575"
      hgnc "NA"
      map_id "W1_17"
      name "nucleocapsid_br_phosphoprotein"
      node_subtype "GENE"
      node_type "species"
      org_id "aabef"
      uniprot "NA"
    ]
    graphics [
      x 1827.547343788725
      y 1980.1780191386488
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_119"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id254c7db4"
      uniprot "NA"
    ]
    graphics [
      x 1833.0893406705811
      y 2094.721206678008
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:pubmed:32159237;urn:miriam:uniprot:P0DTC9"
      hgnc "NA"
      map_id "W1_111"
      name "nucleocapsid_br_protein"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ff3a1"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 1728.7197164335066
      y 2050.1760622601405
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:obo.chebi:CHEBI%3A39025"
      hgnc "NA"
      map_id "W1_26"
      name "b76b3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b76b3"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 2030.3804646429971
      y 1386.337232506828
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "PUBMED:33244168"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_135"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id7acf7b3"
      uniprot "NA"
    ]
    graphics [
      x 1894.4983299057099
      y 1352.484274189078
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:ensembl:ENSG00000073060;urn:miriam:obo.chebi:CHEBI%3A39025;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:P0DTC2"
      hgnc "NA"
      map_id "W1_106"
      name "recognition_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f9872"
      uniprot "UNIPROT:Q9BYF1;UNIPROT:P0DTC2"
    ]
    graphics [
      x 1731.8409580135353
      y 1346.3798840136092
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:ncbigene:43740569"
      hgnc "NA"
      map_id "W1_67"
      name "ORF3a"
      node_subtype "GENE"
      node_type "species"
      org_id "dc5e7"
      uniprot "NA"
    ]
    graphics [
      x 1996.6715739177648
      y 995.5300034961065
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_154"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idc3f0c926"
      uniprot "NA"
    ]
    graphics [
      x 2045.9717660639303
      y 1095.8378240705092
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_154"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:uniprot:P0DTC3"
      hgnc "NA"
      map_id "W1_19"
      name "ORF3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ac4ba"
      uniprot "UNIPROT:P0DTC3"
    ]
    graphics [
      x 1928.5002040039913
      y 1118.8759018362496
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_20"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "aceb2"
      uniprot "NA"
    ]
    graphics [
      x 861.1293649744771
      y 1172.0874840567756
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q89006922"
      hgnc "NA"
      map_id "W1_43"
      name "nsp2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c6991"
      uniprot "NA"
    ]
    graphics [
      x 970.9897041170806
      y 1143.1751093740095
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      annotation "PUBMED:32511376"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_150"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "idb2e3b478"
      uniprot "NA"
    ]
    graphics [
      x 876.9560718131174
      y 641.9008622367335
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_150"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q87917572"
      hgnc "NA"
      map_id "W1_11"
      name "nsp10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a8b3c"
      uniprot "NA"
    ]
    graphics [
      x 987.2103363476477
      y 564.4146997752638
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "PUBMED:32142651"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_130"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id66bc3866"
      uniprot "NA"
    ]
    graphics [
      x 502.55996476768416
      y 573.7452229800017
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_130"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:pubmed:32142651;urn:miriam:pubmed:32662421;urn:miriam:uniprot:O15393"
      hgnc "NA"
      map_id "W1_57"
      name "TMPRSS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "cf321"
      uniprot "UNIPROT:O15393"
    ]
    graphics [
      x 429.2341092344491
      y 466.40355423694746
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q106020384"
      hgnc "NA"
      map_id "W1_33"
      name "S2_space_subunit"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bdee3"
      uniprot "NA"
    ]
    graphics [
      x 748.059477923342
      y 618.8296666977508
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q94648393;urn:miriam:pubmed:32283108"
      hgnc "NA"
      map_id "W1_90"
      name "ExoN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "eed95"
      uniprot "NA"
    ]
    graphics [
      x 678.4375429855227
      y 1656.0559047325078
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      annotation "PUBMED:32938769"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_163"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "ide22650b2"
      uniprot "NA"
    ]
    graphics [
      x 641.1613777366995
      y 1800.187477054919
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_163"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_48"
      name "Viral_space_RNA_space_synthesis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "ca04b"
      uniprot "NA"
    ]
    graphics [
      x 598.3991337425334
      y 1910.6211481233759
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:uniprot:P0DTC2"
      hgnc "NA"
      map_id "W1_44"
      name "surface_br_glycoprotein_space_S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c8192"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 1629.1659712331416
      y 1180.7507711982478
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      annotation "PUBMED:32376634"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_136"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id7d45bc8b"
      uniprot "NA"
    ]
    graphics [
      x 1714.938617222519
      y 1101.559985659399
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:uniprot:P09958;urn:miriam:pubmed:32376634"
      hgnc "NA"
      map_id "W1_89"
      name "FURIN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ee829"
      uniprot "UNIPROT:P09958"
    ]
    graphics [
      x 1701.3203888665776
      y 1219.3875256619522
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q106020384"
      hgnc "NA"
      map_id "W1_110"
      name "S2_space_subunit"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "fdfbe"
      uniprot "NA"
    ]
    graphics [
      x 1704.0412124977734
      y 943.3271069290221
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:ncbigene:43740570"
      hgnc "NA"
      map_id "W1_76"
      name "envelope_br_protein"
      node_subtype "GENE"
      node_type "species"
      org_id "e1b6f"
      uniprot "NA"
    ]
    graphics [
      x 463.26933103323563
      y 1161.4329641086003
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_142"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id95aba954"
      uniprot "NA"
    ]
    graphics [
      x 336.60908268076685
      y 1154.221215623159
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_142"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:pubmed:32159237;urn:miriam:uniprot:P0DTC4"
      hgnc "NA"
      map_id "W1_16"
      name "envelope_br_protein"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "aa466"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 415.8821700252969
      y 1076.2166005719544
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "PUBMED:32706371"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_161"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "iddb77d7c7"
      uniprot "NA"
    ]
    graphics [
      x 1293.9969624059836
      y 966.3233960612912
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_161"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:uniprot:Q9NYK1"
      hgnc "NA"
      map_id "W1_60"
      name "TLR7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d516b"
      uniprot "UNIPROT:Q9NYK1"
    ]
    graphics [
      x 1235.5291383485069
      y 1108.59248696091
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:ncbigene:43740574"
      hgnc "NA"
      map_id "W1_94"
      name "ORF7b"
      node_subtype "GENE"
      node_type "species"
      org_id "f092f"
      uniprot "NA"
    ]
    graphics [
      x 878.0449034845542
      y 2257.400715862661
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_105"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f9791"
      uniprot "NA"
    ]
    graphics [
      x 946.6128195095952
      y 2350.1316786265234
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q88089438"
      hgnc "NA"
      map_id "W1_77"
      name "ORF7b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e487d"
      uniprot "NA"
    ]
    graphics [
      x 999.890978223436
      y 2244.9044354686366
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A39025"
      hgnc "NA"
      map_id "W1_85"
      name "HDL"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "e9f11"
      uniprot "NA"
    ]
    graphics [
      x 332.5420255788482
      y 786.6572789436677
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      annotation "PUBMED:33244168"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_162"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "iddc9f49d1"
      uniprot "NA"
    ]
    graphics [
      x 252.32311572339222
      y 706.1373807240653
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_162"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:ensembl:ENSG00000073060"
      hgnc "NA"
      map_id "W1_6"
      name "SCARB1"
      node_subtype "GENE"
      node_type "species"
      org_id "a59d5"
      uniprot "NA"
    ]
    graphics [
      x 274.3253692123882
      y 830.9333976002396
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:ncbigene:43740571"
      hgnc "NA"
      map_id "W1_64"
      name "membrane_br_glycoprotein"
      node_subtype "GENE"
      node_type "species"
      org_id "d7b3e"
      uniprot "NA"
    ]
    graphics [
      x 550.6205206422957
      y 2144.31240351823
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_156"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idc79adab4"
      uniprot "NA"
    ]
    graphics [
      x 437.3130784802373
      y 2140.1220853000214
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_156"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:pubmed:32159237;urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "W1_61"
      name "membrane_br_glycoprotein"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d614c"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 477.90207362500973
      y 2033.5637242516739
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_55"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "cec01"
      uniprot "NA"
    ]
    graphics [
      x 582.6677335768079
      y 1060.728958403009
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:ncbigene:43740568"
      hgnc "NA"
      map_id "W1_27"
      name "surface_br_glycoprotein"
      node_subtype "GENE"
      node_type "species"
      org_id "b920a"
      uniprot "NA"
    ]
    graphics [
      x 1830.8308940626112
      y 1464.6288656252982
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_143"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id9d2699b4"
      uniprot "NA"
    ]
    graphics [
      x 1870.3502427393018
      y 1564.2089266028224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_143"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:pubmed:32155444;urn:miriam:pubmed:32159237"
      hgnc "NA"
      map_id "W1_50"
      name "surface_br_glycoprotein"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "cc4b9"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 1785.4447589181354
      y 1626.2150338180527
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      annotation "PUBMED:32653452;PUBMED:32970989"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_139"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id8acd3f8f"
      uniprot "NA"
    ]
    graphics [
      x 536.5815991668292
      y 516.6013307724481
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_139"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A28815"
      hgnc "NA"
      map_id "W1_107"
      name "heparan_space_sulfate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "fa9de"
      uniprot "NA"
    ]
    graphics [
      x 714.4086808310544
      y 529.9372630867073
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_122"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id4f170add"
      uniprot "NA"
    ]
    graphics [
      x 838.1022664662995
      y 570.1199703955602
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikipathways:WP4860"
      hgnc "NA"
      map_id "W1_91"
      name "Hijack_space_of_br_ubiquitination"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "ef7e5"
      uniprot "NA"
    ]
    graphics [
      x 900.4478604072092
      y 457.08785547793934
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q87917579"
      hgnc "NA"
      map_id "W1_40"
      name "nsp16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c4ba4"
      uniprot "NA"
    ]
    graphics [
      x 869.9078644669341
      y 1578.4769146200672
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      annotation "PUBMED:32511376"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_125"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id54006fd1"
      uniprot "NA"
    ]
    graphics [
      x 968.4770514712334
      y 1669.353452034944
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_125"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q87917579"
      hgnc "NA"
      map_id "W1_18"
      name "nsp16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "abf4b"
      uniprot "NA"
    ]
    graphics [
      x 1076.6522156746657
      y 1692.0659367406865
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_88"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ec0be"
      uniprot "NA"
    ]
    graphics [
      x 639.5074209567292
      y 1492.9380480621037
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q88656943"
      hgnc "NA"
      map_id "W1_83"
      name "nsp6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e7ad1"
      uniprot "NA"
    ]
    graphics [
      x 525.9984595953517
      y 1660.501715356009
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_25"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "b6f1b"
      uniprot "NA"
    ]
    graphics [
      x 720.9848730969926
      y 1473.6902512380325
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "c9b9f"
      uniprot "NA"
    ]
    graphics [
      x 661.3765266812349
      y 1254.4066867104477
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q94648377"
      hgnc "NA"
      map_id "W1_37"
      name "nsp13"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c38dc"
      uniprot "NA"
    ]
    graphics [
      x 613.5245345964908
      y 1195.687029736678
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:ncbigene:43740578"
      hgnc "NA"
      map_id "W1_100"
      name "orf1"
      node_subtype "GENE"
      node_type "species"
      org_id "f7787"
      uniprot "NA"
    ]
    graphics [
      x 1328.272151893352
      y 560.5810233295738
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_138"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id88a323b4"
      uniprot "NA"
    ]
    graphics [
      x 1292.1525168153846
      y 686.9174272558164
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_138"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_152"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idba2d7d98"
      uniprot "NA"
    ]
    graphics [
      x 996.9134468551837
      y 1260.5275915352063
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_152"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q90038952;urn:miriam:pubmed:32680882"
      hgnc "NA"
      map_id "W1_29"
      name "nsp1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bb606"
      uniprot "NA"
    ]
    graphics [
      x 1190.2286237304597
      y 1295.8506932231305
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:uniprot:P0DTC2"
      hgnc "NA"
      map_id "W1_7"
      name "surface_br_glycoprotein_space_S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a6335"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 2027.5572558762478
      y 1831.1720588126343
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      annotation "PUBMED:33244168"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_141"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id94e29422"
      uniprot "NA"
    ]
    graphics [
      x 2144.0234876535696
      y 1864.3216833505967
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_141"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A39025"
      hgnc "NA"
      map_id "W1_73"
      name "HDL"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "e1398"
      uniprot "NA"
    ]
    graphics [
      x 2120.425505212491
      y 1751.1500339865393
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A42977"
      hgnc "NA"
      map_id "W1_46"
      name "25HC"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "c9b05"
      uniprot "NA"
    ]
    graphics [
      x 1480.383667185732
      y 1230.4240406582428
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      annotation "PUBMED:32944968"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_140"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id912daad6"
      uniprot "NA"
    ]
    graphics [
      x 1561.5917283417975
      y 1108.1377414773185
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_140"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:ncbigene:43740577"
      hgnc "NA"
      map_id "W1_69"
      name "ORF8"
      node_subtype "GENE"
      node_type "species"
      org_id "dd961"
      uniprot "NA"
    ]
    graphics [
      x 716.794500719383
      y 98.77480062852385
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_131"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id68553d54"
      uniprot "NA"
    ]
    graphics [
      x 824.0680431754695
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_131"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q89225654"
      hgnc "NA"
      map_id "W1_42"
      name "ORF8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c6537"
      uniprot "NA"
    ]
    graphics [
      x 836.0822505947017
      y 186.62527550487403
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:uniprot:P0DTC2"
      hgnc "NA"
      map_id "W1_101"
      name "surface_br_glycoprotein_space_S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f7af7"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 1537.7117533257467
      y 2093.117175313607
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      annotation "PUBMED:33244168"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_133"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id76a29895"
      uniprot "NA"
    ]
    graphics [
      x 1570.4053309232074
      y 2216.7731580273394
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_133"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:uniprot:Q9BYF1"
      hgnc "NA"
      map_id "W1_113"
      name "ACE2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ffb2b"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1459.1781338604844
      y 2206.3937762304013
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q90038963"
      hgnc "NA"
      map_id "W1_66"
      name "nsp7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "db214"
      uniprot "NA"
    ]
    graphics [
      x 850.6308199707532
      y 1685.1606019313733
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      annotation "PUBMED:32438371"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_137"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id858197a5"
      uniprot "NA"
    ]
    graphics [
      x 897.666653127985
      y 1823.8839048005175
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_137"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q90038963"
      hgnc "NA"
      map_id "W1_81"
      name "nsp7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e64da"
      uniprot "NA"
    ]
    graphics [
      x 840.4065897461044
      y 1920.0680900984462
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      annotation "PUBMED:33082293"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_146"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "idacdc1203"
      uniprot "NA"
    ]
    graphics [
      x 1689.3023480806978
      y 813.3488915206437
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_146"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:uniprot:O14786;urn:miriam:pubmed:33082293"
      hgnc "NA"
      map_id "W1_86"
      name "NRP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ea33c"
      uniprot "UNIPROT:O14786"
    ]
    graphics [
      x 1655.0739906118363
      y 704.6616303460726
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      annotation "PUBMED:32944968"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_149"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idb04f1828"
      uniprot "NA"
    ]
    graphics [
      x 955.6794583889578
      y 678.6304132559289
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_149"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      annotation "PUBMED:32706371"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_2"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "a273b"
      uniprot "NA"
    ]
    graphics [
      x 1172.1214367343441
      y 1227.8438978529407
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikipathways:WP4868"
      hgnc "NA"
      map_id "W1_13"
      name "Type_space_I_space_interferon_br_induction_space_and_br_signaling"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "a989f"
      uniprot "NA"
    ]
    graphics [
      x 1164.4754469318807
      y 1370.6957322907915
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q87917572;urn:miriam:wikidata:Q89686805"
      hgnc "NA"
      map_id "W1_51"
      name "cd41e"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "cd41e"
      uniprot "NA"
    ]
    graphics [
      x 1709.5567841925877
      y 523.726441900733
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_158"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idd68bc555"
      uniprot "NA"
    ]
    graphics [
      x 1755.1402483105508
      y 423.8413283141864
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_158"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikipathways:WP4884"
      hgnc "NA"
      map_id "W1_30"
      name "Pathogenesis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "bc193"
      uniprot "NA"
    ]
    graphics [
      x 1665.0569914574157
      y 358.97269790622386
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_148"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idaf9c6f8"
      uniprot "NA"
    ]
    graphics [
      x 306.88475529850723
      y 1428.0139413758507
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_148"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikipathways:WP4868"
      hgnc "NA"
      map_id "W1_75"
      name "Type_space_I_space_interferon_br_induction_space_and_br_signaling"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "e1943"
      uniprot "NA"
    ]
    graphics [
      x 458.1763308335394
      y 1434.5902712435254
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      annotation "PUBMED:33244168"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_118"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id230f84bb"
      uniprot "NA"
    ]
    graphics [
      x 1628.0008875791416
      y 1467.6577552203807
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:uniprot:P0DTC2"
      hgnc "NA"
      map_id "W1_5"
      name "a4fdf"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "a4fdf"
      uniprot "UNIPROT:P0DTC2"
    ]
    graphics [
      x 1964.1803983069235
      y 1571.4352579446277
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      annotation "PUBMED:33244168"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_157"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idce2a065d"
      uniprot "NA"
    ]
    graphics [
      x 2057.2250253451284
      y 1500.2766845652145
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_157"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_147"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idaf62af2b"
      uniprot "NA"
    ]
    graphics [
      x 632.0844632387949
      y 1355.4816661873112
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_147"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_21"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ae185"
      uniprot "NA"
    ]
    graphics [
      x 919.6082182140883
      y 1259.3814018683493
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q87917582"
      hgnc "NA"
      map_id "W1_24"
      name "nsp5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b5d9f"
      uniprot "NA"
    ]
    graphics [
      x 1057.9315228474302
      y 1308.2840558924618
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_65"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "d8831"
      uniprot "NA"
    ]
    graphics [
      x 796.1743250542376
      y 1420.8555784825444
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_98"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f30e4"
      uniprot "NA"
    ]
    graphics [
      x 993.2228396140817
      y 1043.5783674461907
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      annotation "PUBMED:23943763"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_123"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id52141a70"
      uniprot "NA"
    ]
    graphics [
      x 358.6110351597887
      y 1654.6807870476864
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q90038956"
      hgnc "NA"
      map_id "W1_78"
      name "nsp4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e5589"
      uniprot "NA"
    ]
    graphics [
      x 352.54485142306487
      y 1553.8966860662235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_117"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id1f86b6c5"
      uniprot "NA"
    ]
    graphics [
      x 525.5117182157865
      y 730.3669923321585
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q89686805"
      hgnc "NA"
      map_id "W1_71"
      name "nsp9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "de7fa"
      uniprot "NA"
    ]
    graphics [
      x 636.5428805219028
      y 635.6913090255441
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      annotation "PUBMED:32680882"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_164"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "ide3b4c27b"
      uniprot "NA"
    ]
    graphics [
      x 1326.233969327532
      y 1265.2497987907632
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_164"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q90038952"
      hgnc "NA"
      map_id "W1_104"
      name "nsp1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f9094"
      uniprot "NA"
    ]
    graphics [
      x 1383.9410690292445
      y 1161.1192754105407
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:pubmed:32321524;urn:miriam:pubmed:32529116;urn:miriam:pubmed:32283108;urn:miriam:wikidata:Q94647436"
      hgnc "NA"
      map_id "W1_58"
      name "nsp12"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d0526"
      uniprot "NA"
    ]
    graphics [
      x 811.0052221246665
      y 945.7989555222468
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      annotation "PUBMED:32438371"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_132"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id768dd6a5"
      uniprot "NA"
    ]
    graphics [
      x 721.842861105251
      y 872.340604064214
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:pubmed:32321524;urn:miriam:pubmed:32529116;urn:miriam:pubmed:32283108;urn:miriam:wikidata:Q94647436"
      hgnc "NA"
      map_id "W1_4"
      name "nsp12"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a4ef4"
      uniprot "NA"
    ]
    graphics [
      x 624.1802367865369
      y 932.302088815424
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_120"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id31b492b0"
      uniprot "NA"
    ]
    graphics [
      x 1180.0763788581792
      y 768.797804432215
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:uniprot:P0DTC1;urn:miriam:pubmed:32169673"
      hgnc "NA"
      map_id "W1_87"
      name "orf1a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "eb8ab"
      uniprot "UNIPROT:P0DTC1"
    ]
    graphics [
      x 1055.3146231854012
      y 805.7969178255887
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_102"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f81a2"
      uniprot "NA"
    ]
    graphics [
      x 818.9302245865699
      y 1494.4919463397634
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      annotation "PUBMED:23943763"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_153"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "idbb7865a7"
      uniprot "NA"
    ]
    graphics [
      x 414.7221209715234
      y 1777.622639162525
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_153"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4846"
      full_annotation "urn:miriam:wikidata:Q88656943"
      hgnc "NA"
      map_id "W1_35"
      name "nsp6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c2256"
      uniprot "NA"
    ]
    graphics [
      x 299.7525308431325
      y 1827.7718408900364
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_92"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "efcb8"
      uniprot "NA"
    ]
    graphics [
      x 775.799520374306
      y 1099.981295462307
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4846"
      full_annotation "NA"
      hgnc "NA"
      map_id "W1_68"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "dd252"
      uniprot "NA"
    ]
    graphics [
      x 672.3029173505224
      y 1053.1692577603376
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W1_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 167
    source 1
    target 2
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_109"
      target_id "W1_144"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 168
    source 2
    target 3
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_144"
      target_id "W1_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 169
    source 4
    target 5
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_62"
      target_id "W1_128"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 170
    source 5
    target 6
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_128"
      target_id "W1_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 171
    source 7
    target 8
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_15"
      target_id "W1_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 172
    source 8
    target 9
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_124"
      target_id "W1_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 173
    source 10
    target 11
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_36"
      target_id "W1_159"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 174
    source 11
    target 12
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_159"
      target_id "W1_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 175
    source 13
    target 14
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_59"
      target_id "W1_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 176
    source 14
    target 15
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_97"
      target_id "W1_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 177
    source 16
    target 17
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_49"
      target_id "W1_129"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 178
    source 17
    target 18
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_129"
      target_id "W1_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 179
    source 19
    target 20
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_10"
      target_id "W1_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 180
    source 20
    target 21
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_121"
      target_id "W1_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 181
    source 22
    target 23
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_82"
      target_id "W1_126"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 182
    source 24
    target 23
    cd19dm [
      diagram "WP4846"
      edge_type "INHIBITION"
      source_id "W1_114"
      target_id "W1_126"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 23
    target 25
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_126"
      target_id "W1_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 13
    target 26
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_59"
      target_id "W1_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 26
    target 27
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_22"
      target_id "W1_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 28
    target 29
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_41"
      target_id "W1_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 29
    target 30
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_116"
      target_id "W1_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 31
    target 32
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_31"
      target_id "W1_145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 32
    target 33
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_145"
      target_id "W1_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 34
    target 35
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_12"
      target_id "W1_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 35
    target 36
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_115"
      target_id "W1_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 37
    target 38
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_1"
      target_id "W1_166"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 39
    target 38
    cd19dm [
      diagram "WP4846"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W1_95"
      target_id "W1_166"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 38
    target 40
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_166"
      target_id "W1_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 27
    target 41
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_39"
      target_id "W1_160"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 41
    target 42
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_160"
      target_id "W1_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 12
    target 43
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_96"
      target_id "W1_165"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 43
    target 4
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_165"
      target_id "W1_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 44
    target 45
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_84"
      target_id "W1_155"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 45
    target 46
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_155"
      target_id "W1_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 15
    target 47
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_70"
      target_id "W1_151"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 47
    target 48
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_151"
      target_id "W1_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 49
    target 50
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_28"
      target_id "W1_127"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 50
    target 51
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_127"
      target_id "W1_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 52
    target 53
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_9"
      target_id "W1_134"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 53
    target 54
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_134"
      target_id "W1_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 13
    target 55
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_59"
      target_id "W1_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 55
    target 44
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_63"
      target_id "W1_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 13
    target 56
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_59"
      target_id "W1_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 56
    target 57
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_45"
      target_id "W1_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 13
    target 58
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_59"
      target_id "W1_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 58
    target 59
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_103"
      target_id "W1_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 13
    target 60
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_59"
      target_id "W1_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 60
    target 61
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_38"
      target_id "W1_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 62
    target 63
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_17"
      target_id "W1_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 63
    target 64
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_119"
      target_id "W1_111"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 65
    target 66
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_26"
      target_id "W1_135"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 66
    target 67
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_135"
      target_id "W1_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 68
    target 69
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_67"
      target_id "W1_154"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 69
    target 70
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_154"
      target_id "W1_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 13
    target 71
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_59"
      target_id "W1_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 71
    target 72
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_20"
      target_id "W1_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 15
    target 73
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_70"
      target_id "W1_150"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 73
    target 74
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_150"
      target_id "W1_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 22
    target 75
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_82"
      target_id "W1_130"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 76
    target 75
    cd19dm [
      diagram "WP4846"
      edge_type "CATALYSIS"
      source_id "W1_57"
      target_id "W1_130"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 75
    target 77
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_130"
      target_id "W1_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 78
    target 79
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_90"
      target_id "W1_163"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 79
    target 80
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_163"
      target_id "W1_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 81
    target 82
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_44"
      target_id "W1_136"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 83
    target 82
    cd19dm [
      diagram "WP4846"
      edge_type "CATALYSIS"
      source_id "W1_89"
      target_id "W1_136"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 82
    target 84
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_136"
      target_id "W1_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 85
    target 86
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_76"
      target_id "W1_142"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 86
    target 87
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_142"
      target_id "W1_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 21
    target 88
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_54"
      target_id "W1_161"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 88
    target 89
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_161"
      target_id "W1_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 90
    target 91
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_94"
      target_id "W1_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 91
    target 92
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_105"
      target_id "W1_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 239
    source 93
    target 94
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_85"
      target_id "W1_162"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 240
    source 94
    target 95
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_162"
      target_id "W1_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 241
    source 96
    target 97
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_64"
      target_id "W1_156"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 242
    source 97
    target 98
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_156"
      target_id "W1_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 243
    source 13
    target 99
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_59"
      target_id "W1_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 244
    source 99
    target 52
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_55"
      target_id "W1_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 245
    source 100
    target 101
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_27"
      target_id "W1_143"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 246
    source 101
    target 102
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_143"
      target_id "W1_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 247
    source 22
    target 103
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_82"
      target_id "W1_139"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 248
    source 103
    target 104
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_139"
      target_id "W1_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 249
    source 15
    target 105
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_70"
      target_id "W1_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 250
    source 105
    target 106
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_122"
      target_id "W1_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 251
    source 107
    target 108
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_40"
      target_id "W1_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 252
    source 108
    target 109
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_125"
      target_id "W1_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 253
    source 13
    target 110
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_59"
      target_id "W1_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 254
    source 110
    target 111
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_88"
      target_id "W1_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 255
    source 13
    target 112
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_59"
      target_id "W1_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 256
    source 112
    target 78
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_25"
      target_id "W1_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 257
    source 13
    target 113
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_59"
      target_id "W1_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 258
    source 113
    target 114
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_47"
      target_id "W1_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 259
    source 115
    target 116
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_100"
      target_id "W1_138"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 260
    source 116
    target 21
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_138"
      target_id "W1_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 261
    source 13
    target 117
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_59"
      target_id "W1_152"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 262
    source 117
    target 118
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_152"
      target_id "W1_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 263
    source 119
    target 120
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_7"
      target_id "W1_141"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 264
    source 120
    target 121
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_141"
      target_id "W1_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 265
    source 122
    target 123
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_46"
      target_id "W1_140"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 266
    source 123
    target 39
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_140"
      target_id "W1_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 267
    source 124
    target 125
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_69"
      target_id "W1_131"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 268
    source 125
    target 126
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_131"
      target_id "W1_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 269
    source 127
    target 128
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_101"
      target_id "W1_133"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 270
    source 128
    target 129
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_133"
      target_id "W1_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 271
    source 130
    target 131
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_66"
      target_id "W1_137"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 272
    source 131
    target 132
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_137"
      target_id "W1_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 273
    source 84
    target 133
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_110"
      target_id "W1_146"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 274
    source 133
    target 134
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_146"
      target_id "W1_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 275
    source 77
    target 135
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_33"
      target_id "W1_149"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 276
    source 135
    target 19
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_149"
      target_id "W1_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 277
    source 89
    target 136
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_60"
      target_id "W1_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 278
    source 136
    target 137
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_2"
      target_id "W1_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 138
    target 139
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_51"
      target_id "W1_158"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 139
    target 140
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_158"
      target_id "W1_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 44
    target 141
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_84"
      target_id "W1_148"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 141
    target 142
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_148"
      target_id "W1_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 67
    target 143
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_106"
      target_id "W1_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 143
    target 4
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_118"
      target_id "W1_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 144
    target 145
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_5"
      target_id "W1_157"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 145
    target 65
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_157"
      target_id "W1_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 114
    target 146
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_37"
      target_id "W1_147"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 146
    target 142
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_147"
      target_id "W1_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 13
    target 147
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_59"
      target_id "W1_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 147
    target 148
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_21"
      target_id "W1_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 13
    target 149
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_59"
      target_id "W1_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 149
    target 107
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_65"
      target_id "W1_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 21
    target 150
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_54"
      target_id "W1_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 150
    target 13
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_98"
      target_id "W1_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 61
    target 151
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_72"
      target_id "W1_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 151
    target 152
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_123"
      target_id "W1_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 52
    target 153
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_9"
      target_id "W1_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 153
    target 154
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_117"
      target_id "W1_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 118
    target 155
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_29"
      target_id "W1_164"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 155
    target 156
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_164"
      target_id "W1_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 157
    target 158
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_58"
      target_id "W1_132"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 158
    target 159
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_132"
      target_id "W1_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 7
    target 160
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_15"
      target_id "W1_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 304
    source 160
    target 161
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_120"
      target_id "W1_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 305
    source 13
    target 162
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_59"
      target_id "W1_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 306
    source 162
    target 130
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_102"
      target_id "W1_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 307
    source 111
    target 163
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_83"
      target_id "W1_153"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 308
    source 163
    target 164
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_153"
      target_id "W1_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 309
    source 13
    target 165
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_59"
      target_id "W1_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 310
    source 165
    target 157
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_92"
      target_id "W1_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 311
    source 13
    target 166
    cd19dm [
      diagram "WP4846"
      edge_type "CONSPUMPTION"
      source_id "W1_59"
      target_id "W1_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 312
    source 166
    target 34
    cd19dm [
      diagram "WP4846"
      edge_type "PRODUCTION"
      source_id "W1_68"
      target_id "W1_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
